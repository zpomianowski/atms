#!/usr/bin/env python
# -*- coding: utf-8 -*-
import csv
import re
from subprocess import Popen, PIPE

LAB_NET = '10.17.199.0/24'
START_IP = 20

r_ip = re.compile(r"^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$")
r_mac = re.compile(r'([0-9A-F]{2}[:-]){5}([0-9A-F]{2})', re.IGNORECASE)


def detect(path):
    known = dict()

    with open(path, 'rb') as f:
        csv_reader = csv.reader(f)
        for row in csv_reader:
            if r_ip.match(row[2]) and r_mac.match(row[1]):
                known[row[2].strip().split('.')[-1]] = dict(mac=row[1])

    ping = Popen('nmap --privileged -sP %s' % LAB_NET,
                 stdout=PIPE, shell=True).communicate()[0]

    for f in re.finditer(
            r'(([0-9A-F]{2}[:-]){5}([0-9A-F]{2}))' +
            r'.*\n.*?' +
            r'(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$)', ping, re.MULTILINE):

        ip_ending = f.group(4).strip().split('.')[-1]
        if ip_ending not in known and int(ip_ending) >= START_IP:
            print('%s %s' % (f.group(1), f.group(4)))


if __name__ == '__main__':
    detect('/tmp/lab.csv')
