#!/usr/bin/env python
# -*- coding: utf-8 -*-
import csv
import re

r_ip = re.compile(r"^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$")
r_mac = re.compile(r'([0-9A-F]{2}[:-]){5}([0-9A-F]{2})', re.IGNORECASE)


def generate(path):
    with open(path, 'rb') as f:
        csv_reader = csv.reader(f)
        for row in csv_reader:
            if r_ip.match(row[2]) and r_mac.match(row[1]):
                if int(row[2].split('.')[-1]) <= 50:
                    continue
                print 'add address=%s client-id=%s mac-address=%s' % (
                    row[2], row[3], row[1].upper())

if __name__ == '__main__':
    generate('/tmp/lab.csv')
