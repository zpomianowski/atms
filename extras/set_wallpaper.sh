#!/bin/bash

# Cron won't be able to connect to your Gnome session without this
sessionfile=`find "${HOME}/.dbus/session-bus/" -type f`
export `grep "DBUS_SESSION_BUS_ADDRESS" "${sessionfile}" | sed '/^#/d'`

# This changes the wallpaper setting: it updates immediately
gsettings set org.gnome.desktop.background picture-uri $1
