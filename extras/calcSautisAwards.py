# -*- coding: utf-8 -*-

SAUT = 'sautis_issues_reported'

db2 = DAL(
    'mysql://saut:papasmerflubijagody?2011@192.168.81.93/saut_bugtracker')

db2.define_table(
    'mantis_user_table',
    Field('email', 'string'),
    migrate=False)

# SAUT projects
db2.define_table(
    'mantis_project_table',
    Field('name', writable=False),
    Field('description', 'text', writable=False),
    Field('status', writable=False),
    Field('enabled', writable=False),
    Field('view_state', writable=False),
    Field('category_id', writable=False),
    Field('inherit_global', writable=False),
    format='%(name)s',
    migrate=False)

db2.define_table(
    'mantis_bug_table',
    Field('project_id', writable=False),
    Field('reporter_id', writable=False),
    Field('handler_id', writable=False),
    Field('duplicate_id', writable=False),
    Field('priority', writable=False),
    Field('severity', writable=False),
    Field('reproducibility', writable=False),
    Field('status', writable=False),
    Field('resolution', writable=False),
    Field('projection', writable=False),
    Field('bug_text_id', writable=False),
    Field('version', writable=False),
    Field('fixed_in_version', writable=False),
    Field('build', writable=False),
    Field('profile_id', writable=False),
    Field('view_state', writable=False),
    Field('summary', writable=False),
    Field('sponsorship_total', writable=False),
    Field('sticky', writable=False),
    Field('target_version', writable=False),
    Field('category_id', writable=False),
    Field('date_submitted', writable=False),
    Field('due_date', writable=False),
    Field('last_updated', writable=False),
    Field('saut_closed_by_id', writable=False),
    Field('saut_issue_type', writable=False),
    migrate=False)

db2.define_table(
    'mantis_bug_history_table',
    Field('user_id', writable=False),
    Field('bug_id', writable=False),
    Field('field_name', writable=False),
    Field('old_value', writable=False),
    Field('new_value', writable=False),
    Field('type', writable=False),
    Field('date_modified', writable=False),
    migrate=False)

db2.define_table(
    'mantis_project_version_table',
    Field('project_id', writable=False),
    Field('version', writable=False),
    Field('description', writable=False),
    Field('released', writable=False),
    Field('obsolete', writable=False),
    Field('date_order', writable=False),
    migrate=False)


def calcMantisAwards(date, divider):
        try:
            import time
            start_ts = int(time.mktime(date.timetuple()))
            stop_ts = int(time.mktime(
                (date + timedelta(days=divider)).timetuple()))

            action = SAUT

            query = (db.atms_auth_user.sautis_user_id ==
                     db.mantis_bug_table.reporter_id) & \
                (db.atms_auth_user.is_active == True) & \
                (db.mantis_bug_table.date_submitted >= start_ts) & \
                (db.mantis_bug_table.date_submitted < stop_ts)

            issues_sev = db.mantis_bug_table.severity.sum()

            rows = db(query).select(
                db.atms_auth_user.id,
                issues_sev,
                groupby=db.mantis_bug_table.reporter_id,
                orderby=~issues_sev)
            for r in rows:
                db.atms_exp.update_or_insert(
                    ((db.atms_exp.timestamp == date) &
                     (db.atms_exp.user == r.atms_auth_user.id) &
                     (db.atms_exp.action == action)),
                    timestamp=date,
                    user=r.atms_auth_user.id,
                    action=action,
                    exp=int(r[issues_sev] / 40))
            db.commit()
        except:
            pass

if __name__ == '__main__':
    """
    Can be launched only in VLAN533 network.

    LAB network cannot access VLAN533's hosts, but LAB is accessible from
    VLAN533.
    """
    import time
    db(db.atms_exp.action == SAUT).delete()

    START = datetime.date(2011, 12, 1)
    STOP = datetime.date.today()
    DIVIDER = 7

    def daterange(start_date, end_date, divider=DIVIDER):
        end_date = end_date + datetime.timedelta(days=divider)
        for n in range(int((end_date - start_date).days / divider)):
            yield start_date + datetime.timedelta(n * divider)

    musers = db2(db2.mantis_user_table.id > 0).select(
        db2.mantis_user_table.id,
        db2.mantis_user_table.email)
    for d in daterange(START, STOP):
        for u in musers:
            user = db.atms_auth_user(email=u.email)
            if not user:
                continue
            start_ts = int(time.mktime(d.timetuple()))
            stop_ts = int(time.mktime(
                (d + datetime.timedelta(days=DIVIDER)).timetuple()))

            issues_sev = db2.mantis_bug_table.severity.sum()
            rows = db2(
                (db2.mantis_bug_table.reporter_id == u.id) &
                (db2.mantis_bug_table.date_submitted >= start_ts) &
                (db2.mantis_bug_table.date_submitted < stop_ts)).select(
                issues_sev,
                groupby=db2.mantis_bug_table.reporter_id)
            if not rows:
                continue
            exp_pts = int(rows.first()[issues_sev] / 10)

            print('%s -> %s' % (d, STOP), u.email, exp_pts)
            db.atms_exp.update_or_insert(
                ((db.atms_exp.timestamp == d) &
                 (db.atms_exp.user == user.id) &
                 (db.atms_exp.action == SAUT)),
                timestamp=d, user=user.id, action=SAUT, exp=exp_pts)
        db.commit()

    task_calculateAwards(full=True)
