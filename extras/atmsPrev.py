#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import sys
import socket
import locale
import gettext
from threading import Timer
from subprocess import check_output
import gi
gi.require_version('Gst', '1.0')
gi.require_version('GstVideo', '1.0')
gi.require_version('Gtk', '3.0')
gi.require_version('Gdk', '3.0')
gi.require_version('GdkX11', '3.0')
from gi.repository import GLib
from gi.repository import Gdk
from gi.repository import Gst
from gi.repository import Gtk
from gi.repository import GdkX11    # for window.get_xid()
from gi.repository import GstVideo  # for sink.set_window_handle()

DEBUG = False
if DEBUG:
    os.environ["GST_DEBUG"] = '*:2'
    # os.environ["GST_DEBUG"] = '*:2,*scope*:4'

Gdk.init([])
Gtk.init([])
Gst.init(None)

TARGET_HOSTNAME = socket.gethostname()
SERVER = 'http://atms.lab.redefine.pl:8080/hls'
TYPE_ATMS = 0x01
TYPE_CAPT = 0x02

ATMS_PIPELINE = """
souphttpsrc location="%(server)s/%(host)s-%(slot)s.m3u8" ! hlsdemux !
    decodebin ! videoconvert ! xvimagesink name=vsink
"""

CAPT_PIPELINE = """
v4l2src device=/dev/video%(slot)s ! video/x-raw,width=640,height=360 !
videoconvert !
textoverlay text=Slot:%(slot)s
    shaded-background=true font-desc="Sans, 24"
    halignment=right valignment=top deltay=-20 !
xvimagesink name=vsink
"""


class Main(object):
    def __init__(self):
        abspath = os.path.abspath(__file__)
        dname = os.path.dirname(abspath)
        os.chdir(dname)

        locale.setlocale(locale.LC_ALL, '')
        gettext.bindtextdomain('atmsPrev', '')
        gettext.textdomain('atmsPrev')
        _ = gettext.gettext

        builder = Gtk.Builder()
        builder.add_from_file("atms_prev.glade")

        self.__slots = 4
        panned_position = 0

        try:
            nb = int(check_output(['mwcap-info', '-l']).split(
                '\n')[0].split(':')[1])
            if nb == 0:
                raise Exception
            self.capture_widgets = [
                Channel(TYPE_CAPT, builder.get_object("m%d" % i), i)
                for i in range(self.__slots)]
            panned_position = Gdk.Screen.get_default().get_width() / 2
        except:
            pass

        self.atms_widgets = [
            Channel(TYPE_ATMS, builder.get_object("a%d" % i), i)
            for i in range(self.__slots)]

        self.window = builder.get_object("MainWin")
        self.window.set_title(_('ATMS Previewer'))
        self.window.connect('delete-event', self.on_delete_event)
        self.window.show_all()
        self.window.maximize()

        builder.get_object('paned').set_position(panned_position)

    def on_delete_event(self, widget, event):
        Gtk.main_quit()


class Channel(object):
    def __init__(self, type, video_target, slot):
        self.__slot = slot
        self.__type = type
        self.vtarget = video_target
        self.vtarget.set_double_buffered(False)
        self.vtarget.connect("realize", self.on_realize)
        self.vtarget.connect("draw", self.on_draw)

        pipe_desc = 'videotestsrc ! xvimagesink name=vsink'
        if type == TYPE_ATMS:
            pipe_desc = ATMS_PIPELINE
        elif type == TYPE_CAPT:
            pipe_desc = CAPT_PIPELINE

        self.state = Gst.State.NULL

        settings = dict(
            server=SERVER,
            host=TARGET_HOSTNAME,
            slot=slot)
        self.pl = Gst.parse_launch(pipe_desc % settings)

        bus = self.pl.get_bus()
        bus.add_signal_watch()
        bus.enable_sync_message_emission()
        bus.connect("message::error", self.on_error)
        bus.connect("message::eos", self.on_eos)
        bus.connect("message::state-changed", self.on_state_changed)
        bus.connect("sync-message::element", self.on_sync_message)

        self.sink = self.pl.get_by_name('vsink')

    def start(self):
        self.pl.set_state(Gst.State.PLAYING)

    def pause(self):
        self.pl.set_state(Gst.State.PAUSED)

    def stop(self):
        self.pl.set_state(Gst.State.NULL)

    def on_realize(self, widget):
        self.pl.set_state(Gst.State.READY)
        self.start()

    def _on_recovery(self):
        self._recovery_timer.cancel()
        print("Trying to recover the source.")
        self.pl.set_state(Gst.State.NULL)
        self.pl.set_state(Gst.State.READY)
        self.pl.set_state(Gst.State.PLAYING)

    def on_draw(self, widget, cr):
        if self.state < Gst.State.PAUSED:
            allocation = widget.get_allocation()

            cr.set_source_rgb(0, 0, 0)
            cr.rectangle(0, 0, allocation.width, allocation.height)
            cr.fill()

        return False

    def on_state_changed(self, bus, msg):
        old, new, pending = msg.parse_state_changed()
        if not msg.src == self.pl:
            # not from the playbin, ignore
            return

        self.state = new

    def on_error(self, bus, msg):
        err, dbg = msg.parse_error()
        print("ERROR:", msg.src.get_name(), ":", err.message)
        if dbg:
            print("Debug info:", dbg)

        interval = 3
        if self.__type == TYPE_ATMS:
            interval = 10
        self._recovery_timer = Timer(interval, self._on_recovery)
        self._recovery_timer.start()

    def on_eos(self, bus, msg):
        print("End-Of-Stream reached")
        self.pl.set_state(Gst.State.READY)

    def on_sync_message(self, bus, message):
        def func():
            imagesink = message.src
            imagesink.set_property("force-aspect-ratio", True)
            imagesink.set_window_handle(
                self.vtarget.get_window().get_xid())

        if message.get_structure().get_name() == 'prepare-window-handle':
            GLib.idle_add(func)

if __name__ == '__main__':
    if len(sys.argv) > 1:
        TARGET_HOSTNAME = sys.argv[1]
    main = Main()
    Gtk.main()
