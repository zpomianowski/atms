c = get_config()

c.InteractiveShell.autoindent = True
c.InteractiveShell.confirm_exit = False
c.InteractiveShell.deep_reload = True
c.TerminalIPythonApp.display_banner = False
c.InteractiveShell.editor = 'nano'
c.InteractiveShellApp.exec_lines = [
    'from IPython.utils.coloransi import TermColors as color',
    'from time import sleep',
    'from datetime import datetime, timedelta',
    'from tester.lab import LabFactory',
    'factory = LabFactory()',
    'print color.LightGreen',
    'print "from time import sleep"',
    'print "from datetime import datetime, timedelta"',
    'print "from tester.lab import LabFactory"',
    'print "factory = LabFactory()"',
    'print color.LightCyan',
    'print "Example: dev = factory(model=\'" + color.Yellow + "wkl" + color.LightCyan + "\')"',
    'print "         dev.temp = 45"',
    'print "         dev.humi_control = True"',
    'print "         dev.humi = 55"',
    'print "         dev.on()"',
    'print "\\n   Help: " + color.Yellow + "http://atms.lab.redefine.pl/atms/static/docs/tester/tester.lab.html" + color.Normal'
    ]
