#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os

"""
Insert into cron:
0 4 * 1 *         /bin/sh -c "/home/nginx/venv/bin/python /home/nginx/web2py/web2py.py -S atms -M -R applications/atms/extras/fixDBk.py"
"""


def main():
    root = 'applications/atms/uploads'

    # fix user user accounts
    for invalid_user in db(
            (db.atms_auth_user.identicon == None) |
            (db.atms_auth_user.color == None) |
            (db.atms_auth_user.color == '')).iterselect():
        invalid_user.color = 'XXXXX'
        invalid_user.update_record()
        print('Identicon and color generated for %s %s' %
              (invalid_user.first_name, invalid_user.last_name))
        db.commit()

    return
    # Part below needs tests!

    # find redundant files
    for filename in os.listdir(root):
        if not db(db.atms_attachments.filename == filename).select():
            print("File '%s' no longer referenced in DB!" % filename)
            os.unlink(os.path.join(root, filename))
        if not db(db.atms_auth_user.identicon == filename).select():
            print("File '%s' no longer referenced in DB!" % filename)
            os.unlink(os.path.join(root, filename))

    # find errors in db
    for row in db(db.atms_attachments.id > 0).iterselect(
            db.atms_attachments.id,
            db.atms_attachments.filename):
        if not os.path.exists(os.path.join(root, row.filename)):
            print("Broken entry '%s'. File does not exist!" % row.filename)
            db(db.atms_attachments.id == row.id).delete()
            db.commit()

if __name__ == '__main__':
    if settings.host != settings.host_master:
        print('Script can be executed only on the master machine!')
    else:
        main()
