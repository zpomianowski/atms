#!/usr/bin/env python
# -*- coding:utf-8 -*-
from gluon.storage import Storage
from tester.stb import TestCase, Bundle
from tester.errors import FAILURE
from tester.lab import LabFactory
from datetime import datetime

# Forces very fast execution of the test scanarios
DEBUG = False

STANDBY_ON_CMDS = ['standby']
STANDBY_OFF_CMDS = []

# Settings for power_on_off, long_term
T_MIN = 10
T_MAX = 35
V_MIN = 220
V_MAX = 240
F_MIN = 50
F_MAX = 60

# STB specific
STB_WAIT = 60

MINUTE = 60
M5 = 5 * MINUTE
M10 = 10 * MINUTE
H = 60 * MINUTE
H24 = 24 * 60 * MINUTE

if DEBUG:
    MINUTE = 1
    M5 = 2
    M10 = 3
    H = 5
    H24 = 10


class TestCase(TestCase):
    """
    Poniżej spis testów, które można wykonać w ramach tego skryptu.

    Potrzebne urządzenia:
        Komora klim, źródło ster IT7321, STB oraz opcjonalnie keysight
        z termoparami (thermalmapping).

    Wykonanie planujemy poprzez kopiowanie nazw funkcji do pola tekstowego
    ``Lista funkcji testujących`` np.: `baseline, op_temp_low, cold_start`

    Wszystkie metody testujące:
        baseline ~ 2d 1h 15m
        op_temp_low, op_temp_high, op_temp_change ~ 3d 1h 15m
        op_rh_high ~ 4d 30m
        cold_start ~ 1d 1h 15m
        hot_start ~ 1d 1h 15m
        abs_max_temp ~ 12h 30m
        power_on_off ~ 3d 12h 30m
        long_term ~ 2d 30m

    baseline
        1.1 (3.5RVT)Baseline Test

    op_temp_low
        4.1.1 (3.12RVT) Operating Temp Low

    op_temp_high
        4.1.2(3.12RVT) Operating Temp High

    op_temp_change
        4.1.3(3.12RVT) Operating Temp Change

    op_rh_high
        4.1.4(3.12RVT) Operating RH High

    cold_start
        6.1 (3.15RVT) Cold Start

    hot_start
        6.2 (3.16RVT) Hot start

    power_on_off
        3.2TRD Power ON /OFF

    long_term
        3.4 Long Term Stability

    abs_max_temp
        6.3 (3.17RVT) Abs Max Temp Rating
    """
    def setUp(self):
        factory = LabFactory(self)
        self.wkl = factory(subtype='chamber')
        self.pwr = factory(model='it7321')

        self.wkl.temp = 25

        self.debug_stamp('TEST ENV SETUP')
        self.pwr.debug('TEST ENV SETUP - power off')

        self.pwr.off()
        self.pwr.volts = 230
        self.pwr.freq = 50

        if not DEBUG:
            self.pwr.debug(
                'TEST ENV SETUP - power on and wait %ds ' % STB_WAIT +
                'until STBs are ready')
            self.pwr.on()
            self.sleep(STB_WAIT)

        self.stbs = Bundle(self, subtype='stb')
        self.stbs.apply('get_info')
        self.stbs.apply('bind', self.wkl, self.pwr)
        self.stbs.apply('start_log')

        if self._testMethodName == 'thermal_mapping':
            self.thermo = factory(subtype='thermo')

    def tearDown(self):
        self.wkl.release()
        self.pwr.release()

        if self._testMethodName == 'thermal_mapping':
            self.thermo.release()
        self.mail_current_logs(
            ['jklawczynski@cyfrowypolsat.pl',
             'mlachowicz@cyfrowypolsat.pl',
             'kchominski@cyfrowypolsat.pl'])

        self.stbs.is_active = False
        self.stbs.apply('stop_log')
        self.stbs.apply('release')


        self.sleep(5)

    def baseline(self):
        HOURS = 48
        TARGET_TEMP = 50
        TARGET_HUMI = 45
        STOP = self.get_stop_time(hours=HOURS)
        COOLDOWN_HOURS = 1

        stbs = self.stbs
        wkl = self.wkl
        stbs.apply('debug', '*** BASELINE START ***')

        stbs.apply('debug', 'Setting climate chamber')
        wkl.control_humi = True
        wkl.temp = TARGET_TEMP
        wkl.humi = TARGET_HUMI
        wkl.on()
        stbs.apply('debug', 'Waiting for %d°C' % TARGET_TEMP)
        if not DEBUG:
            wkl.wait(temp=True, humi=True)
        stbs.apply('debug', 'T = %d°C is set' % TARGET_TEMP)

        while datetime.now() < STOP:
            stbs.apply(
                'debug',
                '%d°C - %dh left' % (TARGET_TEMP, self.get_left_hours(STOP)))
            self.sleep(M10)
            stbs.apply('check')
            # STOP IF TOO MUCH ERRORS
            map(lambda dut: self.assertLess(dut.errors_count(), 20), self.stbs)

        wkl.control_humi = False
        wkl.temp = 25
        if not DEBUG:
            stbs.apply('debug', 'Finishing test: waiting for 25°C')
            wkl.wait(temp=True, humi=False)

        # stbs.apply('debug', '25°C for %d hours' % COOLDOWN_HOURS)
        # self.sleep(hours=COOLDOWN_HOURS)
        stbs.apply('debug', '*** BASELINE STOP ***')

    def op_temp_low(self):
        TARGET_TEMP = -5
        STOP = self.get_stop_time(seconds=H24)

        stbs = self.stbs
        wkl = self.wkl
        stbs.apply('debug', '*** OPERATING TEMP LOW START ***')

        wkl.control_humi = False
        wkl.temp = TARGET_TEMP
        wkl.on()
        stbs.apply('debug', 'Waiting for %d°C' % TARGET_TEMP)
        if not DEBUG:
            wkl.wait(temp=True, humi=False)

        while datetime.now() < STOP:
            stbs.apply(
                'debug',
                '%d°C - %dh left' % (TARGET_TEMP, self.get_left_hours(STOP)))

            self.sleep(M10)
            stbs.apply('check')
            # STOP IF TOO MUCH ERRORS
            map(lambda dut: self.assertLess(dut.errors_count(), 20), self.stbs)

        wkl.temp = 25
        if not DEBUG:
            stbs.apply('debug', 'Finishing test: waiting for 25°C')
            wkl.wait(temp=True, humi=False)
        stbs.apply('debug', '*** OPERATING TEMP LOW STOP ***')

    def op_temp_high(self):
        TARGET_TEMP = 45
        STOP = self.get_stop_time(seconds=H24)

        stbs = self.stbs
        wkl = self.wkl
        stbs.apply('debug', '*** OPERATING TEMP HIGH START ***')

        wkl.control_humi = False
        wkl.temp = TARGET_TEMP
        wkl.on()
        stbs.apply('debug', 'Waiting for %d°C' % TARGET_TEMP)
        if not DEBUG:
            wkl.wait(temp=True, humi=False)

        while datetime.now() < STOP:
            stbs.apply(
                'debug',
                '%d°C - %dh left' % (TARGET_TEMP, self.get_left_hours(STOP)))

            self.sleep(M10)
            stbs.apply('check')
            # STOP IF TOO MUCH ERRORS
            map(lambda dut: self.assertLess(dut.errors_count(), 20), self.stbs)

        wkl.temp = 25
        if not DEBUG:
            stbs.apply('debug', 'Finishing test: waiting for 25°C')
            wkl.wait(temp=True, humi=False)
        stbs.apply('debug', '*** OPERATING TEMP HIGH STOP ***')

    def op_temp_change(self):
        TEMP1 = 20
        TEMP2 = 50
        TIME_INTERVAL = H * 3
        CYCLES = 4

        stbs = self.stbs
        wkl = self.wkl
        stbs.apply('debug', '*** OPERATING TEMP CHANGE START ***')

        wkl.control_humi = False
        wkl.on()

        for i in range(CYCLES):
            wkl.temp = TEMP1
            msg1 = 'Waiting for %d°C - cycle [%d/%d]' % (TEMP1, i + 1, CYCLES)
            stbs.apply('debug', msg1)
            self.log_stamp(msg1)
            if not DEBUG:
                wkl.wait(temp=True, humi=False)
            stbs.apply('debug', '%d°C is set for %dh' % (
                TEMP1, TIME_INTERVAL / 3600))
            self.sleep(TIME_INTERVAL)
            stbs.apply('check')

            wkl.temp = TEMP2
            stbs.apply(
                'debug',
                'Waiting for %d°C - cycle [%d/%d]' % (TEMP2, i + 1, CYCLES))
            if not DEBUG:
                wkl.wait(temp=True, humi=False)
            msg2 = '%d°C is set for %dh' % (TEMP2, TIME_INTERVAL / 3600)
            stbs.apply('debug', msg2)
            self.log_stamp(msg2)
            self.sleep(TIME_INTERVAL)
            stbs.apply('check')
            # STOP IF TOO MUCH ERRORS
            map(lambda dut: self.assertLess(dut.errors_count(), 20), self.stbs)

        wkl.control_humi = False
        wkl.temp = 25
        if not DEBUG:
            msg = 'Finishing test: waiting for 25°C'
            stbs.apply('debug', msg)
            self.log_stamp(msg)
            wkl.wait(temp=True, humi=False)
        stbs.apply('debug', '*** OPERATING TEMP CHANGE STOP ***')

    def op_rh_high(self):
        TARGET_TEMP = 40
        TARGET_HUMI = 95
        DAYS = 4

        stbs = self.stbs
        wkl = self.wkl
        stbs.apply('debug', '*** OPERATING RELATIVE HUMIDITY - HIGH START ***')

        wkl.on()
        wkl.control_humi = True
        wkl.temp = TARGET_TEMP
        wkl.humi = TARGET_HUMI
        stbs.apply('debug', 'Waiting for %d°C %dRH' % (
            TARGET_TEMP, TARGET_HUMI))
        if not DEBUG:
            wkl.wait(temp=True, humi=True)
        stbs.apply('debug', '%d°C %dRH are set for %dh' % (
            TARGET_TEMP, TARGET_HUMI, DAYS * 24))

        STOP = self.get_stop_time(days=DAYS)

        def cycle():
            msg = 'Cycle %d°C %dRH, %dh left' % (
                TARGET_TEMP, TARGET_HUMI, self.get_left_hours(STOP))
            stbs.apply('debug', msg)
            self.log_stamp(msg)

            stbs.apply('check')
            # STOP IF TOO MUCH ERRORS
            map(lambda dut: self.assertLess(dut.errors_count(), 20), self.stbs)

        self.repeat_func_until(cycle, _days=DAYS)

        wkl.control_humi = False
        wkl.temp = 25
        if not DEBUG:
            stbs.apply('debug', 'Finishing test: waiting for 25°C')
            wkl.wait(temp=True, humi=False)
        stbs.apply('debug', '*** OPERATING RELATIVE HUMIDITY - HIGH STOP ***')

    def hot_start(self):
        HOURS = 24
        TARGET_TEMP = 50
        INTERVAL1 = M10
        INTERVAL2 = M5
        COOLDOWN_HOURS = 1

        stbs = self.stbs
        pwr = self.pwr
        wkl = self.wkl
        stbs.apply('debug', '*** HOT START ***')

        wkl.temp = TARGET_TEMP
        wkl.control_humi = False
        wkl.on()
        stbs.apply('debug', 'POWER ON - waiting for %d°C' % TARGET_TEMP)
        if not DEBUG:
            wkl.wait(temp=True, humi=False)

        STOP = self.get_stop_time(hours=HOURS)
        while datetime.now() < STOP:
            msg = 'Cycle %d°C, %dh left' % (
                TARGET_TEMP, self.get_left_hours(STOP))
            stbs.apply('debug', msg)
            self.log_stamp(msg)

            stbs.apply('debug', 'POWER ON - %d min' % (INTERVAL1 / 60))
            self.pwr.on()
            # wait until STB is ready
            self.sleep(STB_WAIT)
            stbs.apply('debug', 'STB should be booted and ready')
            stbs.paused = False
            stbs.apply('start_log')
            stbs.apply('check')
            self.sleep(INTERVAL1)

            stbs.paused = True
            stbs.apply('stop_log')
            stbs.apply('debug', 'POWER OFF - %d min' % (INTERVAL2 / 60))
            self.pwr.off()

            stbs.apply('check')
            self.sleep(INTERVAL2)
            stbs.apply('check')

            # STOP IF TOO MUCH ERRORS
            map(lambda dut: self.assertLess(dut.errors_count(), 20), self.stbs)

        TARGET_TEMP = 25
        wkl.temp = TARGET_TEMP
        stbs.apply('debug', 'POWER ON - recovery to %d°C' % TARGET_TEMP)
        pwr.on()
        # wait until STB is ready
        self.sleep(STB_WAIT)
        stbs.apply('debug', 'STB should be booted and ready')
        stbs.paused = False
        stbs.apply('start_log')
        if not DEBUG:
            stbs.apply('debug', 'Finishing test: waiting for 25°C')
            wkl.wait(temp=True, humi=False)
        # stbs.apply('debug', '25°C for %d hours' % COOLDOWN_HOURS)
        # self.sleep(hours=COOLDOWN_HOURS)
        stbs.apply('debug', '*** HOT STOP ***')

    def cold_start(self):
        HOURS = 24
        TARGET_TEMP = -5
        INTERVAL1 = M5
        INTERVAL2 = M10
        COOLDOWN_HOURS = 1

        stbs = self.stbs
        pwr = self.pwr
        wkl = self.wkl
        stbs.apply('debug', '*** COLD START ***')

        wkl.temp = TARGET_TEMP
        wkl.control_humi = False
        wkl.on()
        stbs.apply('debug', 'POWER ON - waiting for %d°C' % TARGET_TEMP)
        if not DEBUG:
            wkl.wait(temp=True, humi=False)

        STOP = self.get_stop_time(hours=HOURS)
        while datetime.now() < STOP:
            msg = 'Cycle %d°C, %dh left' % (
                TARGET_TEMP, self.get_left_hours(STOP))
            stbs.apply('debug', msg)
            self.log_stamp(msg)

            stbs.apply('debug', 'POWER ON - %d min' % (INTERVAL1 / 60))
            self.pwr.on()
            # wait until STB is ready
            self.sleep(STB_WAIT)
            stbs.apply('debug', 'STB should be booted and ready')
            stbs.paused = False
            stbs.apply('start_log')
            stbs.apply('check')
            self.sleep(INTERVAL1)

            stbs.paused = True
            stbs.apply('stop_log')
            stbs.apply('debug', 'POWER OFF - %d min' % (INTERVAL2 / 60))
            self.pwr.off()

            stbs.apply('check')
            self.sleep(INTERVAL2)
            stbs.apply('check')

            # STOP IF TOO MUCH ERRORS
            map(lambda dut: self.assertLess(dut.errors_count(), 20), self.stbs)

        TARGET_TEMP = 25
        wkl.temp = TARGET_TEMP
        stbs.apply('debug', 'POWER ON - recovery to %d°C' % TARGET_TEMP)
        pwr.on()
        # wait until STB is ready
        self.sleep(STB_WAIT)
        stbs.apply('debug', 'STB should be booted and ready')
        stbs.paused = False
        stbs.apply('start_log')
        if not DEBUG:
            stbs.apply('debug', 'Finishing test: waiting for 25°C')
            wkl.wait(temp=True, humi=False)
        # stbs.apply('debug', '25°C for %d hours' % COOLDOWN_HOURS)
        # self.sleep(hours=COOLDOWN_HOURS)
        stbs.apply('debug', '*** COLD STOP ***')

    def abs_max_temp(self):
        TARGET_TEMP = 60
        HOURS = 10
        COOLDOWN_HOURS = 2

        stbs = self.stbs
        wkl = self.wkl
        stbs.apply('debug', '*** ABSOLUTE MAX TEMPERATURE RATING START ***')

        wkl.temp = 25
        wkl.control_humi = False
        wkl.on()
        stbs.apply('debug', 'Waiting for 25°C')
        if not DEBUG:
            wkl.wait(temp=True, humi=False)

        wkl.temp = TARGET_TEMP
        stbs.apply('debug', 'Waiting for %d°C' % TARGET_TEMP)
        if not DEBUG:
            wkl.wait(temp=True, humi=False)

        STOP = self.get_stop_time(hours=HOURS)

        def cycle():
            msg = 'Cycle %d°C, %dh left' % (
                TARGET_TEMP, self.get_left_hours(STOP))
            stbs.apply('debug', msg)
            self.log_stamp(msg)

            stbs.apply('check')
            # STOP IF TOO MUCH ERRORS
            map(lambda dut: self.assertLess(dut.errors_count(), 20), self.stbs)

        self.repeat_func_until(cycle, _hours=HOURS)

        wkl.temp = 25
        if not DEBUG:
            stbs.apply('debug', 'Finishing test: waiting for 25°C')
            wkl.wait(temp=True, humi=False)

        # stbs.apply('debug', '25°C for %d hours' % COOLDOWN_HOURS)
        # self.sleep(hours=COOLDOWN_HOURS)

        stbs.apply('debug', '*** ABSOLUTE MAX TEMPERATURE RATING STOP ***')

    def long_term(self):
        DAYS = 2
        STOP = self.get_stop_time(days=DAYS)

        wkl = self.wkl
        stbs = self.stbs
        stbs.apply('debug', '*** LONG TERM STABILITY START ***')

        wkl.on()

        def cycle():
            msg = 'Long term cycle, %dh left' % self.get_left_hours(STOP)
            stbs.apply('debug', msg)
            self.log_stamp(msg)

            wkl.temp = T_MIN - 5
            if not DEBUG:
                stbs.apply('debug', 'Waiting for %d°C' % (T_MIN - 5))
                wkl.wait(temp=True, humi=False)
            stbs.apply('check')

            wkl.temp = T_MAX + 5
            if not DEBUG:
                stbs.apply('debug', 'Waiting for %d°C' % (T_MAX + 5))
                wkl.wait(temp=True, humi=False)
            stbs.apply('check')

            stbs.apply('debug', 'Dwell for 3h')
            self.sleep(H * 3)
            stbs.apply('check')

            wkl.temp = T_MIN - 5
            if not DEBUG:
                stbs.apply('debug', 'Waiting for %d°C' % (T_MIN - 5))
                wkl.wait(temp=True, humi=False)

            stbs.apply('debug', 'Dwell for 3h')
            self.sleep(H * 3)
            stbs.apply('check')
            # STOP IF TOO MUCH ERRORS
            map(lambda dut: self.assertLess(dut.errors_count(), 20), self.stbs)

        self.repeat_func_until(cycle, _days=DAYS)

        wkl.temp = 25
        if not DEBUG:
            stbs.apply('debug', 'Finishing test: waiting for 25°C')
            wkl.wait(temp=True, humi=False)
        self.sleep(hours=1)
        stbs.apply('debug', '*** LONG TERM STABILITY STOP ***')

    def power_on_off(self):
        wkl = self.wkl
        pwr = self.pwr
        stbs = self.stbs
        stbs.apply('bind', wkl, pwr)
        stbs.apply('stop_log')

        stbs.apply('debug', '*** POWER ON/OFF START ***')
        pwr.warn('Make sure the STB is connected directly to ACPWR (IT7321)!')

        CYCLES = Storage(A=500, B=500, C=500, D=500, E=500,
                         CA=10, CB=10, CC=10,
                         COUNTER=1)

        def csum():
            return sum([v for k, v in CYCLES.items() if k != 'COUNTER'])

        def cycle(name, toff):
            cnb = '%d/%d' % (CYCLES.COUNTER, csum())
            msg = 'Stage "%s" Power on/off cycle... [%s]' % (name, cnb)
            stbs.apply('debug', msg)
            self.log_stamp(msg)

            stbs.paused = True
            pwr.off()
            self.sleep(toff)

            stbs.apply('check')
            self.assertTrue(int(pwr.curr) == 0)

            pwr.on()
            self.sleep(STB_WAIT)
            stbs.paused = False

            stbs.apply('check')

            CYCLES.COUNTER += 1
            # STOP IF TOO MUCH ERRORS
            map(lambda dut: self.assertLess(
                dut.errors_count(FAILURE.AV), 50), self.stbs)

        # A & B
        wkl.temp = T_MIN - 5
        wkl.control_humi = False
        wkl.on()
        if not DEBUG:
            stbs.apply('debug', 'Waiting for %d°C' % (T_MIN - 5))
            wkl.wait(temp=True, humi=False)

        # A
        pwr.volts = int(V_MIN - 0.1 * V_MIN)
        pwr.freq = F_MIN - 3
        self.repeat_func_until(cycle, 'A', 60, _cycles=CYCLES.A, _interval=1)

        # B
        pwr.volts = int(V_MAX + 0.1 * V_MAX)
        pwr.freq = F_MAX + 3
        self.repeat_func_until(cycle, 'B', 60, _cycles=CYCLES.B, _interval=1)

        # C & D
        wkl.temp = T_MAX + 5
        if not DEBUG:
            stbs.apply('debug', 'Waiting for %d°C' % (T_MAX + 5))
            wkl.wait(temp=True, humi=False)

        # C
        pwr.volts = int(V_MIN - 0.1 * V_MIN)
        pwr.freq = F_MIN - 3
        self.repeat_func_until(cycle, 'C', 1, _cycles=CYCLES.C, _interval=1)

        # D
        pwr.volts = int(V_MAX + 0.1 * V_MAX)
        pwr.freq = F_MAX + 3
        self.repeat_func_until(cycle, 'D', 1, _cycles=CYCLES.D, _interval=1)

        # E
        pwr.volts = 230
        pwr.freq = 50
        wkl.temp = 25
        if not DEBUG:
            stbs.apply('debug', 'Waiting for 25°C')
            wkl.wait(temp=True, humi=False)
        self.repeat_func_until(cycle, 'E', 1, _cycles=CYCLES.E, _interval=1)

        # COLD RESTART

        # A & B & C
        wkl.temp = T_MIN - 5
        wkl.control_humi = False
        if not DEBUG:
            stbs.apply('debug', 'Waiting for %d°C' % (T_MIN - 5))
            wkl.wait(temp=True, humi=False)

        # A
        pwr.volts = int(V_MIN - 0.1 * V_MIN)
        pwr.freq = F_MIN - 3
        self.repeat_func_until(
            cycle, 'Cold Restart A', 600, _cycles=CYCLES.CA, _interval=1)

        # B
        pwr.volts = int(V_MAX + 0.1 * V_MAX)
        pwr.freq = F_MAX + 3
        self.repeat_func_until(
            cycle, 'Cold Restart B', 600, _cycles=CYCLES.CB, _interval=1)

        # C
        pwr.volts = 230
        pwr.freq = 50
        self.repeat_func_until(
            cycle, 'Cold Restart C', 600, _cycles=CYCLES.CC, _interval=1)

        # DEEP STANDBY
        if len(STANDBY_ON_CMDS) == 0 or len(STANDBY_OFF_CMDS) == 0:
            stbs.apply(
                'warn',
                'DEEP STANDBY ommited - '
                'lack of STANDBY_ON_CMDS/STANDBY_OFF_CMDS')
            stbs.apply('debug', 'Waiting for 25°C')
            wkl.wait(temp=True, humi=False)
            stbs.apply('debug', '*** POWER ON/OFF STOP ***')
            return

        CYCLES = 500

        def cycle_ds(name, tw):
            stbs.apply('debug', 'Deep standby cycle...')
            stbs.paused = True
            for stb in stbs:
                map(lambda cmd: stb.driver.query(cmd), STANDBY_ON_CMDS)
            self.sleep(tw)

            for stb in stbs:
                map(lambda cmd: stb.driver.query(cmd), STANDBY_OFF_CMDS)
            self.sleep(STB_WAIT)

            stbs.paused = False
            self.sleep(15)
            stbs.apply('check')

        # A
        wkl.temp = T_MIN - 5
        wkl.control_humi = False
        if not DEBUG:
            stbs.apply('debug', 'Waiting for %d°C' % (T_MIN - 5))
            wkl.wait(temp=True, humi=False)
        self.repeat_func_until(
            cycle_ds, 'Deep standby A', 60, _cycles=CYCLES, _interval=1)

        # B
        wkl.temp = T_MAX + 5
        wkl.control_humi = False
        if not DEBUG:
            stbs.apply('debug', 'Waiting for %d°C' % (T_MAX + 5))
            wkl.wait(temp=True, humi=False)
        self.repeat_func_until(
            cycle_ds, 'Deep standby B', 60, _cycles=CYCLES, _interval=1)

        # C
        wkl.temp = 25
        wkl.control_humi = False
        if not DEBUG:
            stbs.apply('debug', 'Waiting for 25°C')
            wkl.wait(temp=True, humi=False)
        self.repeat_func_until(
            cycle_ds, 'Deep standby C', 60, _cycles=CYCLES, _interval=1)

        stbs.apply('debug', '*** POWER ON/OFF STOP ***')

    def thermal_mapping(self):
        TARGET_TEMP = T_MAX
        HOURS = 3

        stbs = self.stbs
        wkl = self.wkl
        thermo = self.thermo
        self.stbs.apply('bind', wkl, thermo)
        stbs.apply('debug', '*** THERMAL MAPPING START ***')

        wkl.control_humi = False
        wkl.temp = TARGET_TEMP
        wkl.on()
        wkl.wait(temp=True, humi=False)

        STOP = self.get_stop_time(hours=HOURS)

        def cycle():
            msg = 'Cycle %.0f°C, %dh left' % (
                TARGET_TEMP, self.get_left_hours(STOP))
            stbs.apply('debug', msg)
            self.log_stamp(msg)

            stbs.apply('check')
            # STOP IF TOO MUCH ERRORS
            map(lambda dut: self.assertLess(dut.errors_count(), 20), self.stbs)

        self.repeat_func_until(cycle, _hours=HOURS)

        wkl.control_humi = False
        wkl.temp = 25
        stbs.apply('debug', 'Waiting for 25°C - finishing test')
        if not DEBUG:
            wkl.wait(temp=True, humi=False)
        stbs.apply('debug', '*** THERMAL MAPPING STOP ***')
