#!/usr/bin/env python
# -*- coding:utf-8 -*-
from tester.stb import TestCase
from tester.lab import LabFactory
from tester.lab.remote import VECTRA
from time import sleep
from datetime import datetime
from datetime import timedelta

MINUTE = 60
M5 = 5 * MINUTE
M10 = 10 * MINUTE
H = 60 * MINUTE
H24 = 24 * 60 * MINUTE


class TestCase(TestCase):
    def setUp(self):
        self.factory = LabFactory(self)
        self.ird = self.factory(model='irduino')
        self.ird.set_type(VECTRA)
        self.mfi = self.factory(model='mfi')
        self.ipcamsrc(
            'rtsp://admin:test1234@10.17.199.46:554/Streaming/Channels/101')

    def tearDown(self):
        self.ird.release()
        self.mfi.release()

    def test(self):
        ird = self.ird
        mfi = self.mfi
        mfi.off(1, 2, 3, 4, 5, 6)

        def tformat():
            return datetime.now().strftime('%Y.%m.%d_%H.%M.%S')

        STOP = datetime.now() + timedelta(seconds=H24)
        while datetime.now() < STOP:
            mfi.off(1, 2, 3, 4, 5, 6)
            sleep(MINUTE)
            mfi.on(1, 2, 3, 4, 5, 6)
            sleep(4)
            self.snapshot(tformat())
            cycle = 10
            for n in range(cycle):
                self.snapshot(tformat())
                sleep(M5)
                ird.send(1, ird.codes.DISPLAY)
                ird.send(1, ird.codes.DISPLAY)
                sleep(10)
                self.snapshot(tformat())

        mfi.off(1, 2, 3, 4, 5, 6)
