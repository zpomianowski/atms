#!/usr/bin/env python
# -*- coding:utf-8 -*-
from time import sleep
from tester.stb import TestCase
from tester.lab import LabFactory
from tester.errors import FAILURE


class TestCase(TestCase):
    def setUp(self):
        factory = LabFactory(self)
        self.stb = factory(subtype='stb')
        self.wkl = factory(subtype='chamber')
        self.wkl.start_log()
        self.stb.start_log()

    def tearDown(self):
        self.wkl.stop_log()
        self.stb.stop_log()
        self.wkl.release()
        self.stb.release()

    def test(self):
        """
        Tu implementujesz scenariusz testowy
        metoda tej klasy zostanie wynonana jedynie,
        gdy zostanie wypisana w odpowienim polu funkcji testwowych do wykonania
        (pole w panelu automatyzacji test case'a).
        Jako rozbudowany przykład możesz traktować tempalte `stb_rvt_trd.py`

        Przykład:
            >>> def test(self):
            >>>     wkl = self.wkl
            >>>     wkl.temp = -30
            >>>     wkl.wait()
            >>>     wkl.temp = 60
            >>>     wkl.wait()
            >>>     sleep(60)
        """
        raise NotImplementedError("Write your damn test scenario!!!")
