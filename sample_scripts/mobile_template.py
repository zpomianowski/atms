#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
Więcej info znajdziesz tu:
http://appium.io/slate/en/master/?python#
https://github.com/appium/python-client

Dla cwaniaków:
    Wykonanie tego skryptu jest wykonywane w sandboxie. Założenie jest by móc maksymalnie elastyczne narzędzie
    więc możliwe jest praktyczne użycie prawie każdej paczki i modułu.
    Mimo to nie próbujcie niczego psuć, nie jest to w niczyim interesie.
    Wykonywanie skryptów na serwerze głównym jest zablokowane.
"""

from tester.mobile.utils import TestCase
from appium.webdriver.common.touch_action import TouchAction
from appium.webdriver.mobilecommand import MobileCommand as Command


class TestCase(TestCase):
    """
    bziuuuum
    Ważna metoda inicjująca test
    Opis parametrów: http://appium.io/slate/en/master/?python#appium-server-capabilities

    Note:
        To find package and activity:
        `pm list packages -f | grep spotify`
        `pm dump com.spotify.music | grep -A 1 MAIN`
    """
    def setUp(self):
        caps = {
            'platformName': 'Android',
            'appPackage': 'com.spotify.music',
            'appActivity': 'com.spotify.music.MainActivity',
            'fullReset': False,
            'noReset': True
            }

        # grab selenium based web driver
        self.wd = self.get_driver(caps)

    def tearDown(self):
        pass

    def test(self):
        self.snapshot('test_search')

        el = self.wd.find_element_by_id(
            'com.spotify.music:id/search_tab')
        self.assertIsNotNone(el)
        el.click()

        self.wd.find_element_by_id(
            'com.spotify.music:id/search_toolbar').click()
        el = self.wd.find_element_by_id(
            'com.spotify.music:id/query')
        self.assertIsNotNone(el)
        el.send_keys('ni mom hektara')

        self.snapshot('test_hektar')

        el = self.wd.find_element_by_android_uiautomator(
            'new UiSelector().text("Ni mom hektara")')
        self.assertIsNotNone(el)
        el.click()

        for i in range(3):
            self.wd.find_element_by_id(
                'com.spotify.music:id/playPause').click()
            self.sleep(1)

        self.snapshot('test_hehe')

        el = self.wd.find_element_by_id(
            'com.spotify.music:id/search_tab').click()
        el = self.wd.find_element_by_id('com.spotify.music:id/query')
        self.assertIsNotNone(el)
        el.clear()
        el.send_keys('bodies')

        self.wd.find_element_by_android_uiautomator(
            'new UiSelector().text("Bodies")').click()

        self.snapshot('test_bodies')

        self.sleep(15)
