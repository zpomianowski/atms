#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""Test konwerterów + naklejek.

Cykl: 4h bardzo gorąco, 4h bardzo zimno.
Podczas testu dodatkowo wysoka wilgotność w celu
sprawdzenia wytrzymałości naklejek.

Prosty test, oparty jedynie na sterowaniu komory.
Weryfikacja poprzez ręczne oględziny stanu urządzeń po
wykonaniu wszystkich cykli.
"""
from tester.lab import LabFactory
from tester.stb import TestCase
from time import sleep
from datetime import datetime
from datetime import timedelta

MINUTE = 60
M5 = 5 * MINUTE
M10 = 10 * MINUTE
H = 60 * MINUTE
H24 = 24 * 60 * MINUTE


class TestCase(TestCase):
    def setUp(self):
        factory = LabFactory(self)
        self.wkl = factory(model='wkl')

    def tearDown(self):
        wkl.stop_log()
        self.wkl.release()

    def test(self):
        wkl = self.wkl
        wkl.on()
        wkl.control_humi = True
        wkl.humi = 75
        wkl.temp = -20

        wkl.debug('Waiting for -20°C 75RH')
        wkl.wait(temp=True, humi=True)
        wkl.debug('Go! Logging started')
        wkl.start_log()

        STOP = datetime.now() + timedelta(seconds=H24 * 4)
        while datetime.now() < STOP:
            wkl.humi = 75
            wkl.temp = -20
            wkl.debug('Waiting for -20°C 75RH')
            wkl.wait(temp=True, humi=True)
            wkl.debug('Go!')

            sleep(H * 4)

            wkl.humi = 75
            wkl.temp = 50
            wkl.debug('Waiting for 50°C 75RH')
            wkl.wait(temp=True, humi=True)
            wkl.debug('Go!')

            sleep(H * 4)

        wkl.control_humi = False
        wkl.off()
