# ATMS
Created by Zbigniew Pomianowski <zpomianowski@cyfrowypolsat.pl>
Tested with Ubuntu 16.04.

Thanks for contrubution for Piotr Szklanko, Michał Bartczak

`ATMS` is abbreviation for `Awesome Test Management System`. At the moment the main features are:
- lab devices manager
  - job dependency graph for physical devices management and conflict avoidance during multiple tests execution
- abstraction layer to unify remote control over lab devices
  - generic interfaces
- automation for long lasting STB environmental tests
  - integration with measuring apparatus like climate chambers, controlled power supplies etc.
  - telemetry based on grafana and influxdb
  - simple continuous video / audio analysis via gstreamer
- automation for mobile and web applications
  - via appium and selenium
- report generation
- UE Capabilities extraction and UE database
- WiFi Capabilities extraction and WiFi device database
- ticketing system for test management and task ordering
- simple wiki to collect team knowledge
- timesheet dashboard

__Complete solution stack includes__
(just few key words to enlight the environment config):
- _Languages_: Python, Javascript, Go, C/C++, Java 
- _Software_: Linux, nginx (with nginx-rtmp and nginx-vod),
GStreamer, OpenCV, Python, web2py, angularjs, qooxdoo, go (grafana), MySQL, InfluxDB, Redis, C/C++ (python wrappers - gst elements in development), GObject Introspection, nodejs, Wireshark components
- _Hardware_: HDMI capture cards based on v4l2 drivers, Ethernet/USB/Serial apparatus controlled via VISA

### Screenshots
###### Management desktop
![Alt text](_readme_resources/screenshot.png?raw=true "ATMS panel")
###### Telemetric dashboard
![Alt text](_readme_resources/atms_telem.png?raw=true "ATMS telemetry panel")

### Architecture


### Dependencies
```bash
sudo apt update
sudo apt install culr build-essential virtualenv libssl-dev libffi-dev python-dev
curl -sL https://deb.nodesource.com/setup_9.x | sudo -E bash -
sudo apt-get install -y nodejs
sudo npm install -g bower
```

Now setup somewhere _qooxdoo_ environment:

```bash
wget https://github.com/qooxdoo/qooxdoo/releases/download/release_5_0_2/qooxdoo-5.0.2-sdk.zip
unzip qooxdoo-5.0.2-sdk.zip & rm qooxdoo-5.0.2-sdk.zip
mv qooxdoo-* qooxdoo & cd qooxdoo
# clone libs from atms/static/js/atms/config.json library section
```

Go back to `<atms_path>/static/js/atms` and create symlink to the QX framework (example):

```bash
ln -s ../../../../../../qooxdoo qooxdoo
```

Make sure virtualenv is setup and packages from `requirements/dev.txt` are properly installed (via pip)
You might need _SASS_ preprocessor and bower (to manage js packages in static assets):
```
sudo apt-get install ruby
sudo su -c "gem install sass"
sudo apt-get install nodejs
sudo apt-get install npm
```

### Peripherals
Magewell capture cards. Download the [driver for Linux](http://www.magewell.com/files/ProCaptureForLinux_2531.tar.gz). Cd to the driver folder and execute:
```bash
sudo ./install.sh
```
Make sure the devices are ready. There should appear new video devices in `dev` folder: `/dev/video0`, `/dev/video1`, `/dev/videoX`.


### Development environment
+ Configure virtualenv path in `*run.sh` scripts
+ Prepare config file: `cp private/appconfig.ini.example private/appconfig.ini`
+ Server: `bash server_run.sh`
+ Scheduler workers: `bash worker_run.sh`
+ Celery workers: `bash worker_stb_run.sh start/stop`, `bash worker_mobile_run.sh start/stop` - not used, bute maybe some day.


### Production environment
+ copy `atms_install_or_update.py` to the top level folder
+ copy `appconfig.ini` to the top level folder
+ run

```bash
sudo python atms_install_or_update.py
```
+ install bower components: cd to `static/js`:
```bash
bower install
```
+ build qooxdoo apps: cd to `static/js/atms` and create symlink to _qooxdoo_ framework root directory:
```bash
python generate source
python generate build
```

Please study: `atms_install_or_update.py` and `web2py-*.conf` files

**Warning:** startup scripts are made for _upstart_ init system. For _systemd_ you need to change them.


### Production environment - master web server only
```bash
sudo apt install pep8 htop nodejs npm nginx uwsgi uwsgi-plugin-python unzip python-dev python-virtualenv libssl-dev libffi-dev build-essential libxml2-dev redis-server mysql-server
sudo adduser nginx
sudo npm install -g bower
cd /usr/bin && sudo mv nodejs node
cd /home/nginx && su nginx
virtualenv venv
wget http://www.web2py.com/examples/static/web2py_src.zip && unzip web2py_src.zip && rm web2py_src.zip
wget http://downloads.sourceforge.net/project/qooxdoo/qooxdoo-current/5.0.1/qooxdoo-5.0.1-sdk.zip?r=http%3A%2F%2Fwww.qooxdoo.org%2F&ts=1476104573&use_mirror=heanet
mv qooxdoo* qooxdoo.zip && unzip qooxdoo.zip && rm qooxdoo.zip && mv qooxdoo* qooxdoo
cd web2py/applications
git clone https://saut@bitbucket.org/zpomianowski/atms.git -b zpomianowski --single-branch
cd atms && source /home/nginx/venv/bin/activate
pip install -r requirements/prod.txt
cd static/js && bower install
cd atms
# fix missing packages from config.json
python generate build
# configure nginx and uwsgi according to config files in systemd directory
# set services
```




### Troubleshooting
- `E: Sub-process /usr/bin/dpkg returned an error code (1)`
  - `dpkg --list | grep linux-image`
  - `sudo dpkg --force-all -P linux-image-X.X.X-X-generic` (choose 1-2 old kernels to remove)
  - `sudo purge-old-kernels`
  - `sudo apt update && sudo apt upgrade`

### TODOs
- każde miejsce w kodzie oznaczone jako _# TODO_
- review kodu i refactoring kontrolera _manager_
- poprawa modeli i kontolerów wg reguły _DRY_
- szybka edycja caseów w TODOs usera
- motion detetection i pattern recognition z użyciem _AKAZE_ w `tester.lab.gst.analyser`
- lepszy preview Magewella, via macierz 2x2
- Wifi Caps - kontynuacja rozwoju koncepcji
  - Veriwave API
  - iXCharriot API
- gst-dektec
- obsługa termopar
- obsuga kamery termowizyjnej
