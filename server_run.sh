# venv/bin/python ~/web2py/applications/atms/modules/websocket_messaging.py &
venv/bin/python ~/web2py/web2py.py -a t -i "0.0.0.0" -p 8000 -K atms:`hostname` -X
# venv/bin/uwsgi --http :8000 -w wsgihandler:application --chdir ~/web2py --master --processes 4 --enable-threads --spooler /tmp/atmspool --spooler-import applications.atms.modules.uwsgitasks --spooler-frequency 2
