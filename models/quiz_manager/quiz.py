db.define_table(
    'atms_quiz_question',
    Field('category', 'string', length=255),
    Field('title', 'string', length=255),
    Field('text', 'text', default=None),
    Field('text_html', 'text',
          compute=lambda r: get_render(r.text),
          writable=False),
    auth.signature,
    format='[%(id)d] %(category)s')

# db.atms_quiz_question.category.requires = IS_IN_SET([
#     ('telecommunication', T('Telecommunication')),
#     ('it', T('IT'))], multiple=False)

db.define_table(
    'atms_quiz_option',
    Field('ref_quiz_question', 'reference %s' % db.atms_quiz_question),
    Field('correct', 'boolean', default=False),
    Field('text', 'text', default=None),
    Field('text_html', 'text',
          compute=lambda r: get_render(r.text),
          writable=False),
    format='%(id)d - %(correct)s')


db.define_table(
    'atms_quiz_question_done',
    Field('ref_question', 'reference %s' % db.atms_quiz_question),
    auth.signature)
