# -*- coding: utf-8 -*-
import logging
import logging.handlers
import socket
from fs.opener import fsopendir
from gluon.storage import Storage
from gluon.contrib.appconfig import AppConfig

response.delimiters = ('{!', '!}')

VERSION = '4.01.21'

logger = logging.getLogger("web2py.app.atms")
# handler = logging.handlers.RotatingFileHandler(
#     '/tmp/atms.log', maxBytes=52428800, backupCount=2)
# handler.setLevel(logging.DEBUG)
# logger.addHandler(handler)
logger.setLevel(logging.DEBUG)

myconf = AppConfig(reload=True)

settings = Storage()
settings.main_group = 'main'
tag = myconf.take('main.config')
settings.config = tag
if tag != 'prod':
    settings.main_group = 'zbh'
    from gluon.custom_import import track_changes
    track_changes(True)

settings.version = tag + '-' + VERSION

settings.host = socket.gethostname()
settings.migrate = True if myconf.take('main.migrate') == 'True' else False

settings.host_master = myconf.take(tag + '.host_master')
settings.host_imgproc = myconf.take(tag + '.host_imgproc')
settings.security_key = myconf.take(tag + '.security_key')
settings.title = 'ATMS TestLAB %s' % VERSION
settings.subtitle = 'powered by web2py'
settings.author = 'Zbigniew Pomianowski'
settings.author_email = 'zpomianowski@cyfrowypolsat.pl'
settings.keywords = ''
settings.description = ''

settings.email_server = myconf.take(tag + '.email_server')
settings.email_sender = myconf.take(tag + '.email_sender')
settings.email_login = myconf.take(tag + '.email_login')

settings.heartbeat = 7

settings.ws_server = myconf.take(tag + '.ws_server')
settings.ws_client = myconf.take(tag + '.ws_client')
settings.database_uri = myconf.take(tag + '.database_uri')
# settings.influxdb = myconf.take(tag + '.influxdb')
settings.redis_url = myconf.take(tag + '.redis_url')
settings.influxdb = myconf.take(tag + '.influxdb')
settings.influxdb_name = 'atms_' + tag
settings.bonus_default = 0.01

fs = None
if settings.host != settings.host_master:
    fs = fsopendir(myconf.take(tag + '.fs'))

response.title = settings.title
response.subtitle = settings.subtitle
response.meta.author = '%(author)s <%(author_email)s>' % settings
response.meta.keywords = settings.keywords
response.meta.description = settings.description

response.menu = [(T('News'), False, URL('default', 'index'), [])]
response.menu.append([T('Timesheet'), False, URL('timesheet', 'index'), []])
response.menu.append(['Dashboard', False,
                     URL('manager', 'index'), []])
