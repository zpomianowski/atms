# we prepend t_ to tablenames and f_ to fieldnames for disambiguity
# wiki init
import datetime
import saut_wiki as wiki
import unittest
import re
from plugin_mptt import MPTT
import json
from json import loads, dumps


auth._wiki = wiki.Wiki(
    auth, db, render='markmin',
    manage_permissions=False,
    force_prefix='',
    restrict_search=False,
    extra=None,
    menu_groups=None,
    templates=None)

db.wiki_media.widget = helpers.UploadWidget2.widget

if auth.has_membership(auth.id_group('wiki_editor')):
    response.menu += auth._wiki.menu('default', 'wiki')

# NODES
atms_nodes = MPTT(db)
atms_nodes.settings.table_node_name = 'atms_nodes'
atms_nodes.settings.extra_fields = {
    'atms_nodes': [
        Field('name', 'string'),
        Field('description', 'text', default=""),
        auth.signature,
        ]}
atms_nodes.define_tables()

def create_thumb(row):
    scheduler.queue_task(
        'webthumb',
        timeout=2000,
        # node has to support Xvfb
        pvars=dict(lang=request.env.http_accept_language),
        group_name=settings.host_imgproc,
        immediate=True,
        sync_output=5)
    db.commit()
    return None


# LOGS - GENERAL PURPOSE
db.define_table(
    'atms_logs',
    Field('timestamp', 'datetime', default=datetime.datetime.now),
    Field('level', 'string', length=255),
    Field('path', 'string', length=255),
    Field('msg', 'text'))


db.atms_logs.level.requires = IS_IN_SET([
    ('debug', 'DEBUG'),
    ('info', 'INFO'),
    ('warning', 'WARNING'),
    ('error', 'ERROR')], multiple=False)


# NEWS
db.define_table(
    'atms_news',
    Field('title', 'string', label=T('Title'), required=True),
    Field('link', 'list:string', label=T('Link')),
    Field('description', 'text', label=T('Description')),
    Field('filename', 'upload', default=None, uploadfs=fs,
          autodelete=True, compute=create_thumb),
    auth.signature,
    format='%(title)s')

db.atms_news.title.requires = IS_NOT_EMPTY()
db.atms_news.description.requires = IS_NOT_EMPTY()
db.atms_news.link.requires = IS_LIST_OF(IS_URL())

# EXP
db.define_table(
    'atms_awards',
    Field('timestamp', 'date', label=T('Timestamp'), required=True),
    Field('user', 'reference %s' % db.atms_auth_user, required=True),
    Field('type', 'string', length=255),
    Field('count', 'integer', default=0),
    format='%(timestamp)s - %(type)s')

db.define_table(
    'atms_exp',
    Field('timestamp', 'date', label=T('Timestamp'), required=True),
    Field('user', 'reference %s' % db.atms_auth_user, required=True),
    Field('exp', 'integer', default=0, label=T('Exp')),
    Field('action', 'string', default=None, label=T('Related action')),
    format='%(timestamp)s - %(exp)s exp')

db.define_table(
    'atms_devices',
    Field('type', 'string', length=255),
    Field('host', 'string', length=255, default='shared'),
    Field('udid', 'string', length=255),
    Field('subtype', 'string', length=255),
    Field('model', 'string', length=255),
    Field('manufacturer', 'string', length=255),
    Field('android_version', 'string', length=255),
    Field('sdk', 'string', length=255),
    Field('state', 'string', length=255, default='unknown'),
    Field('connected', 'boolean', default=False),
    Field('settings', 'text', default=dict()),
    format="(%(host)s) %(model)s")


db.atms_devices.type.requires = IS_IN_SET([
    ('lab', T('LAB instrument')),
    ('dut', 'DUT (Device Under Test)'),
    ('ios', 'iOS'),
    ('android', 'Android'),
    ('windows', 'Windows')], multiple=False, zero=None)
db.atms_devices.state.requires = IS_IN_SET([
    ('adb console', T('ADB console')),
    ('offline', T('Offline')),
    ('unknown', T('Unknown')),
    ('rebooting', T('Rebooting')),
    ('busy', T('Busy')),
    ('idle', T('Idle'))],
    multiple=False, zero=None)
db.atms_devices.settings.filter_in = \
    lambda obj, dumps=dumps: dumps_data(obj)
db.atms_devices.settings.filter_out = \
    lambda txt, loads=loads: load_data(txt)

# CASES TEMPLATES
db.define_table(
    'atms_tcases',
    Field('node', 'reference %s' % db.atms_nodes,
          writable=False, readable=False),
    Field('name', default='Name for case'),
    Field('procedure_text', 'text', default=None),
    Field('procedure_text_html', 'text',
          compute=lambda r: get_render(r.procedure_text),
          writable=False),
    Field('expected_result', 'text', default=None),
    Field('expected_result_html', 'text',
          compute=lambda r: get_render(r.expected_result),
          writable=False),
    Field('expected_time', 'integer', default=5),
    Field('result_time', 'integer', writable=False, readable=False),
    Field('timeout', 'integer'),
    Field('weight', 'integer', default=1),
    Field('is_automated', 'boolean', default=False),
    Field('auto_type', 'list:string', default='android'),
    Field('auto_bin', 'integer', default=None),
    Field('auto_script', 'integer', default=None),
    Field('auto_funcs', 'list:string'),
    Field('auto_devs', 'list:reference atms_devices', ondelete="SET NULL"),
    Field.Virtual('path', lambda row: get_tcase_path(row)),
    auth.signature,
    format='%(name)s')


def get_tcase_path(row):
    if not row.atms_tcases.node:
        return ""
    path = atms_nodes.ancestors_from_node(row.atms_tcases.node, True).select(
        db.atms_nodes.name,
        orderby=db.atms_nodes.level)
    return str.join('/', [x.name for x in path])

db.atms_tcases.name.requires = IS_NOT_EMPTY()
db.atms_tcases.auto_devs.requires = IS_EMPTY_OR(IS_IN_DB(
    db, 'atms_devices.id', db.atms_devices._format, multiple=True))

# TESTS
db.define_table(
    'atms_tests',
    Field('name', 'string'),
    Field('project', 'integer'),
    Field('description', 'text'),
    Field('description_html', 'text',
          compute=lambda r: get_render(r.description),
          writable=False),
    Field('start', 'datetime', default=request.now,
          writable=False),
    Field('stop', 'datetime', writable=False),
    Field('duration', 'datetime', writable=False),
    Field('summary', 'text'),
    Field('summary_html', 'text',
          compute=lambda r: get_render(r.summary),
          writable=False),
    Field('status', 'list:string', default='pending'),
    auth.signature,
    format='%(name)s')

db.atms_tests.status.requires = IS_IN_SET([
    ('waiting', T('Waiting')),
    ('pending', T('Pending')),
    ('aborted', T('Aborted')),
    ('finished', T('Finished'))], multiple=False)

db.define_table(
    'atms_ttests',
    Field('name', 'string'),
    auth.signature,
    format='%(name)s')

db.define_table(
    'atms_ttests_rel',
    Field('ttest', 'reference %s' % db.atms_ttests,
          writable=False, readable=False, notnull=True),
    Field('tcase', 'reference %s' % db.atms_tcases,
          writable=False, readable=False, notnull=True))

# CASES
db.define_table(
    'atms_cases',
    Field('node', db.atms_nodes, ondelete="SET NULL"),
    Field('parent', db.atms_tcases, ondelete="SET NULL"),
    Field('user', db.atms_auth_user, ondelete="SET NULL"),
    Field('test', db.atms_tests, ondelete="SET NULL"),
    Field('name'),
    Field('path', 'text'),
    Field('procedure_text', 'text', default=None),
    Field('procedure_text_html', 'text',
          compute=lambda r: get_render(r.procedure_text),
          writable=False),
    Field('expected_result', 'text', default=None),
    Field('expected_result_html', 'text',
          compute=lambda r: get_render(r.expected_result),
          writable=False),
    Field('result_notes', 'text'),
    Field('result_notes_html', 'text',
          compute=lambda r: get_render(r.result_notes),
          writable=False),
    Field('expected_time', 'integer', default=5),
    Field('result_time', 'integer'),
    Field('status', 'string', default='new'),
    Field('weight', 'integer', default=1),
    Field('doneBefore', 'integer', default=0),
    Field('auto_report', 'reference %s' % db.wiki_page,
          ondelete="SET NULL"),
    Field('is_automated', 'boolean', default=False),
    Field('auto_type', 'list:string', default='android'),
    Field('auto_bin', 'integer', default=None),
    Field('auto_script', 'integer', default=None),
    Field('auto_funcs', 'list:string'),
    Field('auto_devs', 'list:reference atms_devices', ondelete="SET NULL"),
    Field('auto_timeout', 'integer'),
    Field('auto_execution_time', 'integer'),
    auth.signature,
    format='%(name)s')

db.atms_cases.node.requires = IS_EMPTY_OR(IS_IN_DB(db, 'atms_nodes.id'))
db.atms_cases.parent.requires = IS_EMPTY_OR(IS_IN_DB(db, 'atms_tcases.id'))
db.atms_cases.user.requires = IS_EMPTY_OR(IS_IN_DB(db, 'atms_auth_user.id'))
db.atms_cases.test.requires = IS_EMPTY_OR(IS_IN_DB(db, 'atms_tests.id'))
db.atms_cases.auto_report.requires = IS_EMPTY_OR(IS_IN_DB(db, 'wiki_page.id'))
db.atms_cases.name.requires = IS_NOT_EMPTY()
db.atms_cases.status.requires = IS_IN_SET([
    ('unknown', T('Unknown')),
    ('new', T('New')),
    ('pending', T('Pending')),
    ('aborted', T('Aborted')),
    ('impossible', T('Impossible')),
    ('failed', T('Failed')),
    ('passed', T('Passed'))], multiple=False)
db.atms_cases.auto_devs.requires = IS_EMPTY_OR(IS_IN_DB(
    db, 'atms_devices.id', db.atms_devices._format, multiple=True))


# ATMS ATTACHEMENTS
db.define_table(
    'atms_attachments',
    Field('test', 'reference %s' % db.atms_tests,
          writable=False, readable=False, ondelete="SET NULL"),
    Field('tcase', 'reference %s' % db.atms_tcases,
          writable=False, readable=False, ondelete="SET NULL"),
    Field('refcase', 'reference %s' % db.atms_cases,
          writable=False, readable=False, ondelete="SET NULL"),
    Field('refuecap', 'reference %s' % db.atms_uecap,
          writable=False, readable=False),
    Field('refwificap', 'reference %s' % db.atms_wificap,
          writable=False, readable=False),
    Field('task', 'string', default=None),
    Field('title', required=True),
    Field('type'),
    Field('size', 'integer'),
    Field('filename', 'upload', autodelete=True, uploadfs=fs),
    auth.signature,
    format='%(filename)s')


db.atms_attachments.test.requires = IS_EMPTY_OR(IS_IN_DB(
    db, 'atms_tests.id'))
db.atms_attachments.tcase.requires = IS_EMPTY_OR(IS_IN_DB(
    db, 'atms_tcases.id'))
db.atms_attachments.refcase.requires = IS_EMPTY_OR(IS_IN_DB(
    db, 'atms_cases.id'))
db.atms_attachments.refuecap.requires = IS_EMPTY_OR(IS_IN_DB(
    db, 'atms_uecap.id'))


# TIMESHEET
db.define_table(
    'atms_timesheet_project',
    Field('name', 'string', length=255),
    Field('description', 'text', length=1024),
    Field('visible', 'boolean', default=True),
    Field('status', 'string', length=255),
    Field('color', 'string', length=255, default='blue2'),
    Field('background', 'string', length=255, default='white'),
    format='(%(id)s) %(name)s')


db.define_table(
    'atms_timesheet_task_type',
    Field('name', 'string', length=255),
    Field('description', 'text', length=1024),
    Field('visible', 'boolean', default=True),
    format='(%(id)s) %(name)s')


db.define_table(
    'atms_timesheet_task',
    Field('description', 'text', default=None),
    Field('project_id',
          "reference atms_timesheet_project", ondelete='NO ACTION'),
    Field('task_type_id',
          "reference atms_timesheet_task_type", ondelete='NO ACTION'),
    Field('user_id', "reference atms_auth_user", ondelete='NO ACTION'),
    Field('start', 'datetime'),
    Field('stop', 'datetime'),
    Field('duration'),
    Field('visible', 'boolean', default=True))


db.define_table(
    'atms_timesheet_project_task_type',
    Field('project_id',
          "reference atms_timesheet_project", ondelete='NO ACTION'),
    Field('task_type_id',
          "reference atms_timesheet_task_type", ondelete='NO ACTION'),
    Field('visible', 'boolean', default=True),
    format='%(project_id)s %(task_type_id)s')


db.define_table(
    'atms_servers',
    Field('hostname', 'string', length=255),
    Field('description', 'text'),
    Field('addr', 'list:string', length=255),
    Field('status', 'list:string', length=255),
    Field('adb_path', 'string', length=255),
    Field('adb_version', 'string', length=255),
    Field('node_version', 'string', length=255),
    Field('appium_version', 'string', length=255),
    Field('atms_version', 'string', length=255),
    Field('is_master', 'boolean', default=False,
          writable=False),
    format='%(hostname)s')

db.atms_servers.addr.requires = IS_IPV4()
db.atms_servers.status.requires = IS_IN_SET([
    ('unknown', T('Unknown')),
    ('down', T('Down')),
    ('up', T('Up')),
    ], multiple=False)

db.define_table(
    'atms_reports',
    Field('dev_id', 'reference %s' % db.atms_devices,
          writable=False, readable=False),
    Field('case_id', 'reference %s' % db.atms_cases,
          writable=False, readable=False),
    Field('files', 'text', default=[]),
    Field('wiki_page', 'reference %s' % db.wiki_page),
    Field('status', 'string', length=255, default='unknown'),
    Field('modified_on', 'datetime', default=datetime.datetime.now))

db.atms_reports.status.requires = IS_IN_SET([
    ('downloading', T('Downloading')),
    ('unknown', T('Unknown')),
    ('new', T('New')),
    ('pending', T('Pending')),
    ('aborted', T('Aborted')),
    ('postponed', T('Postponed')),
    ('impossible', T('Impossible')),
    ('failed', T('Failed')),
    ('passed', T('Passed'))], multiple=False)

db.define_table(
    'atms_comments',
    Field('channel', 'string', length=255),
    Field('msg', 'text', default=None),
    Field('msg_html', 'text',
          compute=lambda r: get_render(r.msg),
          writable=False),
    Field('members', 'list:reference %s' % db.atms_auth_user,
          ondelete="SET NULL"),
    Field('read_by', 'list:reference %s' % db.atms_auth_user,
          ondelete="SET NULL"),
    auth.signature,
    format='%(id)s %(channel)s')

db.define_table(
    'atms_accounts',
    Field('service', 'string', length=255, default='IPLA'),
    Field('dev_id', 'reference %s' % db.atms_devices, ondelete="SET NULL"),
    Field('email', 'string', length=255),
    Field('login', 'string', length=255, default="test"),
    Field('password', 'string', readable=False, length=255, default="1234"),
    Field('type', 'string', length=255, default='none'),
    format='%(type)s %(login)s')

db.define_table(
    'atms_fejms',
    Field('user_id', 'reference %s' % db.atms_auth_user),
    Field('code', 'string', default=None),
    Field('expire_date', 'datetime', default=datetime.datetime.now),
    format='%(fejm)s - %(user_id)s')

db.atms_fejms.code.requires = IS_IN_SET([
    ('milord', 'Milord'),
    ('birthday', 'birthday'),
    ('ladymilord', 'Milordowa')], multiple=False)

db.define_table(
    'atms_rates',
    Field('code', 'string', default=None),
    Field('current_rate', 'float', default=0.01),
    Field('target', 'float', default=10),
    Field('factor', 'float', default=1.5),
    Field('tends_up', 'integer', default=0),
    Field('rate_min', 'float', default=0.01),
    Field('rate_max', 'float', default=2.00),
    format='%(code)s')

db.define_table(
    'atms_history',
    Field('main_key', 'string', default=None),
    Field('data_key', 'string', default=None),
    Field('value', 'text', default=dict()),
    Field('timestamp', 'datetime', default=datetime.datetime.now))

db.atms_history.value.filter_in = \
    lambda obj, dumps=dumps: dumps(obj, cls=AtmsEncoder)
db.atms_history.value.filter_out = \
    lambda txt, loads=loads: load_data(txt)

db.atms_accounts.dev_id.requires = IS_EMPTY_OR(IS_IN_DB(
    db, 'atms_devices.id', db.atms_devices._format, multiple=False))
db.atms_accounts.email.requires = IS_EMAIL()
db.atms_accounts.service.requires = IS_IN_SET([
    ('IPLA', 'IPLA')], multiple=False)
db.atms_accounts.type.requires = IS_IN_SET([
    ('full', T('Full plan')),
    ('none', T('No plan'))], multiple=False)


db.atms_reports.files.filter_in = \
    lambda obj: dumps(obj)
db.atms_reports.files.filter_out = \
    lambda txt: load_data(txt)


def load_data(txt):
    try:
        return loads(txt)
    except:
        return []


def dumps_data(obj):
    return dumps(obj)


class AtmsEncoder(json.JSONEncoder):
    def default(self, obj):
        if not isinstance(obj, unittest.runner.TextTestResult):
            try:
                return super(AtmsEncoder, self).default(obj)
            except:
                return str(obj)
        return {k: v for (k, v) in obj.__dict__.items()
                if not re.match(r"^(_|dots|stream|buffer)+.*", k)}


def addExp(value, action=None, user_id=None):
    import datetime
    uid = user_id
    if auth.user and not user_id:
        uid = auth.user.id
    if uid:
        dt = datetime.datetime.now()
        date = datetime.date(dt.year, dt.month, dt.day)
        query = ((db.atms_exp.timestamp == date) &
                 (db.atms_exp.action == action) &
                 (db.atms_exp.user == uid))
        row = db(query).select(db.atms_exp.exp).first()
        if row is None:
            exp = 0
        else:
            exp = row.exp
        bonuses = db(
            (db.atms_fejms.user_id == uid) &
            (db.atms_fejms.code.contains("exp_boost")) &
            (db.atms_fejms.expire_date > datetime.datetime.now())).select(
            db.atms_fejms.code,
            distinct=True)
        for bonus in bonuses:
            if bonus.code == 'exp_boost_2x':
                value *= 2
            if bonus.code == 'exp_boost_3x':
                value *= 3
        db.atms_exp.update_or_insert(
            query,
            action=action,
            exp=exp + value,
            timestamp=date,
            user=uid)
        db.commit()
        addCoins(uid, value, action)


def addCoins(user_id, exp, action=None):
    import math
    if not user_id or exp < 0:
        return
    user = db.atms_auth_user[user_id]
    rate = settings.bonus_default
    special_rate = db.atms_rates(code=action)
    if special_rate:
        rate = special_rate.current_rate
    user.coins += exp * rate
    user.coins = math.ceil(user.coins * 100) / 100
    db(db.atms_auth_user.id == user_id).update(
        coins=user.coins)
    db.commit()


def get_render(v):
    return auth._wiki.markmin_base(v, True, True)


def getTableDescriptor(table_name):
    descriptor = {}
    for f in db[table_name]:
        validator = None

        vname = f.requires.__class__.__name__
        if f.name == 'f_ca_search_index':
            help = T("""
            Examples:<br>
            "1,3" - searches CA_1-3<br>
            "3,1" - searches CA_1-3<br>
            "1,3+" - searches CA_1-3-X..X<br>
            "1,3; 4,8+" - combines two search criterias
            """)
            validator = dict(extra={
                'help': help})
        elif 'reference' in f.type or vname == 'IS_IN_SET':
            validator = dict(
                type='id',
                extra={
                    'uri': '/atms/manager/call/json/store/' + str(f).replace(
                        '.', '/')
                    })
        elif vname == 'IS_INT_IN_RANGE':
            validator = dict(
                type=vname,
                extra={
                    'min': f.requires.minimum,
                    'max': f.requires.maximum
                    })
        descriptor[f.name] = dict(
            type=f.type,
            label=f.label)
        if (validator):
            descriptor[f.name]['validator'] = validator

    return descriptor


def flatten(d, parent_key=''):
    import collections
    items = []
    for k, v in d.items():
        new_key = parent_key + '_' + k if parent_key else k
        if isinstance(v, collections.MutableMapping):
            items.extend(flatten(v, new_key).items())
        else:
            items.append((new_key, v))
    return dict(items)
