import sys
import os
import subprocess
import uuid
import shlex
import datetime
from gluon import current
from websocket_messaging import websocket_send
from atms_scheduler import Scheduler
from gluon.scheduler import JobGraph
from gluon.tools import Mail
import tester.mobile.tasks as mt
import tester.base as test
from tester.lab import task_scanlab
from atms_parser import toTcases
import gamification as game


def task_webThumb(lang='en'):
    import traceback
    import string
    from time import sleep
    import os
    rows = db(db.atms_news.filename == None).select(
        db.atms_news.created_by,
        db.atms_news.title,
        db.atms_news.description,
        db.atms_news.link,
        db.atms_news.id)
    if len(rows) == 0:
        return
    valid_chars = "-_()%s%s" % (string.ascii_letters, string.digits)

    os.environ["DISPLAY"] = ":99"
    xvfb = subprocess.Popen(['Xvfb', ':99'])

    for idx, r in enumerate(rows):
        sleep(1)
        # thumb only for first link
        link = r.link[0]
        absfilename = '/tmp/atms_news.filename.%s.jpg' % uuid.uuid4()
        filename = ''.join(c if c in valid_chars else ''
                           for c in link[:75]) + '.jpg'

        # sys.exe is in venv - PySide must be installed!
        proc = subprocess.Popen([
            sys.executable,
            "applications/atms/modules/web_screenshot.py",
            link,
            absfilename],
            stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = proc.communicate()

        # wait and be sure that process is ended properly
        sleep(1)
        if os.path.exists(absfilename):
            r.filename = db.atms_news.filename.store(
                open(absfilename, 'rb'), filename)
            r.update_record()
            db.commit()
        # print '!clear!----%d%%' % int((idx + 1) * 100 / len(rows))
        os.unlink(absfilename)

        # link = UL()
        # if hasattr(r.link, '__iter__'):
        #     for l in r.link:
        #         link.append(LI(A(l, _href=l)))
        # else:
        #     link.append(LI(A(r.link, _href=r.link)))
        # emails = [u.email for u in
        #           db(db.atms_auth_user.is_active == True).select(
        #               db.atms_auth_user.email)]

        # creator = db.atms_auth_user[r.created_by]
        # print creator
        # if creator is None:
        #     continue

        # T = current.T
        # T.force(lang)
        # context = dict(
        #     title='SAUT NEWS',
        #     title_href=URL('atms', 'default', 'index',
        #                    scheme=True, host=True),
        #     msg_title=r.title,
        #     msg_href=URL('atms', 'default', 'index',
        #                  vars=dict(query=r.title),
        #                  scheme=True, host=True),
        #     msg_note=r.description,
        #     msg_body=link,
        #     user='%s %s' % (creator.first_name, creator.last_name),
        #     username='%s %s' % (creator.first_name, creator.last_name),
        #     useremail=creator.email)
        # message = response.render('mail_message.html', context)
        # if 'prod' not in settings.config:
        #     emails = 'zpomianowski@cyfrowypolsat.pl'
        # mail.send(
        #     creator.email,
        #     '[SAUT NEWS] "%s\"' % r.title,
        #     message, cc=emails)

        # addExp(2, 'news', creator.id)

    xvfb.kill()


def task_addTcases():
    files2parse = db(db.atms_attachments.task == 'addTcases').select()
    for f in files2parse:
        filename = request.folder + "/uploads/%s" % f.filename

        p = toTcases(atms_nodes, db, filename)
        exp = p.parse()
        f.delete_record()
        db.commit()
        addExp(exp, f.created_by)
    return 0


def task_checkServers():
    machines = db(db.atms_servers.id > 0).select()
    for m in machines:
        import socket
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            s.connect((m.addr, 22))
            status = 'up'
        except socket.error:
            status = 'down'
        m.update_record(status=status)
        db.commit()
        s.close()
    websocket_send(settings.ws_server, '',
                   settings.security_key, 'atms_servers')


def task_rebootHost():
    """
    Do reboot slave server.

    To have this feature you need to allow reboot
    command executed by a user without the password
    Type: sudo visudo
    Add lines:
        <user> ALL=(ALL) NOPASSWD: /sbin/reboot
        <user> ALL=(ALL) NOPASSWD: /sbin/poweroff
    """
    import socket
    from misc import say
    from subprocess import Popen

    say('Warning! Server is going to be rebooted in 3 2 1')

    row = db(db.atms_servers.hostname == socket.gethostname()).select().first()
    row.status = 'rebooting'
    row.update_record()
    db(db.atms_devices.host == socket.gethostname()).update(
        state='offline', connected=False)
    db.commit()
    websocket_send(settings.ws_server, '',
                   settings.security_key, 'atms_servers')

    Popen(['sudo reboot'], shell=True)


def task_restartATMS():
    """
    Do restart ATMS service.

    To have this feature you need to allow reboot
    command executed by a user without the password
    Type: sudo visudo
    Add lines:
        <user> ALL=(ALL) NOPASSWD: /sbin/restart web2py-scheduler
    """
    from subprocess import Popen
    import socket

    if socket.gethostname() == settings.host_master:
        f = open(os.path.join(os.getcwd(), "../restart"), "wb")
        f.close()

    scheduler.queue_task(
        'reportServerStatus',
        group_name=settings.host,
        timeout=90)
    scheduler.queue_task(
        'restartAdb',
        group_name=settings.host,
        timeout=300)
    db.commit()
    p = Popen(['sudo restart web2py-scheduler'], shell=True)
    p.communicate()


def task_restartAdb():
    """
    Do restart ADB server.

    adb for some reason do not send the RSA fingerprint
    (probably needs to refer to a key file which is visible for root)
    cd /home/cp/android-sdk-linux/platform-tools
    sudo chown root adb
    sudo chmod 4750 adb
    """
    # import socket
    import subprocess

    # host = socket.gethostname()
    proc = subprocess.Popen(shlex.split("adb kill-server"),
                            stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out1, err1 = proc.communicate()
    proc = subprocess.Popen(shlex.split("adb start-server"),
                            stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out2, err2 = proc.communicate()
    # print out1, out2, err1, err2

    mt.task_listAndroidDevices()
    return "OK"


def task_reportServerStatus(ip=None):
    import re
    import socket
    import subprocess
    from netifaces import AF_INET, AF_INET6, AF_LINK, AF_PACKET, AF_BRIDGE
    import netifaces as ni

    adb_v, node_v, appium_v = "n/a", "n/a", "n/a"
    try:
        rev = re.compile(r'.*?(\d.*\d).*?')
        proc = subprocess.Popen(shlex.split("adb version"),
                                stdout=subprocess.PIPE)
        adb_v, err = proc.communicate()
        proc = subprocess.Popen(shlex.split("node -v"),
                                stdout=subprocess.PIPE)
        node_v, err = proc.communicate()
        proc = subprocess.Popen(shlex.split("appium -v"),
                                stdout=subprocess.PIPE)
        appium_v, err = proc.communicate()

        adb_v = rev.match(adb_v).group(1)
        node_v = rev.match(node_v).group(1)
        appium_v = rev.match(appium_v).group(1)
    except:
        pass

    addrs = []
    for ifname in ni.interfaces():
        try:
            a = ni.ifaddresses(ifname)[AF_INET][0]['addr']
            if a != '127.0.0.1':
                addrs.append(a)
        except KeyError:
            pass

    db.atms_servers.update_or_insert(
        db.atms_servers.hostname == socket.gethostname(),
        hostname=socket.gethostname(),
        addr=addrs,
        adb_version=adb_v,
        node_version=node_v,
        appium_version=appium_v,
        atms_version=settings.version,
        status='up')
    db.commit()
    websocket_send(settings.ws_server, '',
                   settings.security_key, 'atms_servers')


def task_dumpTcases(user):
    from atms_parser import dump_tcases
    dump_tcases(db, user)
    websocket_send(settings.ws_server, '',
                   settings.security_key, 'atms_attachments')


def task_recalcRates():
    from datetime import date
    from datetime import datetime
    from datetime import timedelta
    from random import uniform
    from random import gauss
    timestamp = datetime.now()
    if timestamp.hour >= 17 or timestamp.hour < 9:
        db(db.atms_rates.id > 0).update(
            tends_up=0)
        db.commit()
        return
    exp_sum = db.atms_exp.exp.sum()
    rows = db(db.atms_rates.id > 0).select(
        db.atms_rates.code,
        exp_sum,
        db.atms_rates.ALL,
        left=db.atms_exp.on(
            (db.atms_rates.code == db.atms_exp.action) &
            (db.atms_exp.timestamp > date.today() - timedelta(days=1))),
        groupby=db.atms_rates.code)
    for r in rows:
        gathered = r[exp_sum] or 0
        target = r.atms_rates.target
        factor = r.atms_rates.factor
        current_rate = r.atms_rates.current_rate
        rate_min = r.atms_rates.rate_min
        rate_max = r.atms_rates.rate_max
        base = target - gathered
        if base < 0:
            base -= float(gathered) * (current_rate - rate_min) / rate_min
            factor = uniform(5.0, 15.0) * factor
        else:
            factor = uniform(1.0, 3.0) * factor
        inc = factor * base * settings.bonus_default
        new_rate = current_rate + inc + gauss(0, abs(inc) + 0.5)
        new_rate = min(max(new_rate, rate_min), rate_max)
        tends_up = 0
        if current_rate > new_rate:
            tends_up = -1
        if current_rate < new_rate:
            tends_up = 1
        db(db.atms_rates.code == r.atms_rates.code).update(
            current_rate=new_rate,
            tends_up=tends_up)
        db.atms_history.insert(
            main_key='atms_rates',
            data_key=r.atms_rates.code,
            value={'value': new_rate},
            timestamp=timestamp)
    db.commit()


def task_calculateAwards(full=False):
    from datetime import timedelta

    if full:
        db.atms_awards.truncate()

    exp_max = db.atms_exp.exp.max()
    exp_min = db.atms_exp.exp.min()
    exp_sum = db.atms_exp.exp.sum()
    exp_avg = db.atms_exp.exp.avg()
    toSelect = [
        db.atms_auth_user.id,
        db.atms_exp.action,
        exp_sum,
        exp_avg,
        exp_min,
        exp_max]

    def daterange(start_date, end_date, divider):
        end_date = end_date + timedelta(days=divider)
        for n in range(int((end_date - start_date).days / divider)):
            yield start_date + timedelta(n * divider)

    start = db(db.atms_exp.id > 0).select(
        orderby=db.atms_exp.id, limitby=(0, 1)).first().timestamp

    stop = db(db.atms_exp.id > 0).select(
        orderby=~db.atms_exp.id, limitby=(0, 1)).first().timestamp

    if not full:
        start = [d for d in daterange(start, stop, 30)][-2]

    def deg(value, deg):
        new_value = int(value - deg)
        return new_value if new_value > 0 else 1

    def calcAwards(date, divider, cat='general'):
        query = (db.atms_exp.user == db.atms_auth_user.id) & \
            (db.atms_exp.timestamp >= date) & \
            (db.atms_exp.timestamp < date + timedelta(days=divider))
        groupby = db.atms_exp.user
        if cat != 'general':
            groupby = db.atms_exp.user | db.atms_exp.action
            query &= (db.atms_exp.action == cat)
        try:
            pMax = db(query).select(
                exp_max,
                groupby=db.atms_exp.user, orderby=~exp_max).first()[exp_max]

            pAvg = db(query).select(exp_avg).first()[exp_avg]

            pSumBestRows = db(query).select(
                exp_sum,
                groupby=db.atms_exp.user, orderby=~exp_sum)
            pSumBest = pSumBestRows.first()[exp_sum]
            if len(pSumBestRows) > 1:
                if pSumBestRows[0][exp_sum] == pSumBestRows[1][exp_sum]:
                    pSumBest = 99999

            pSumWorstRows = db(query).select(
                exp_sum,
                groupby=db.atms_exp.user, orderby=exp_sum)
            pSumWorst = pSumWorstRows.first()[exp_sum]
            if len(pSumWorstRows) > 1:
                if pSumWorstRows[0][exp_sum] == pSumWorstRows[1][exp_sum]:
                    pSumWorst = -99999
        except:
            return

        name = ''
        th = 100
        if divider == 7:
            name = 'week'
            th = 30
        if divider == 30:
            name = 'month'
            th = 150

        rows = db(query).select(
            *toSelect,
            groupby=groupby, orderby=~exp_sum)
        users = db(db.atms_auth_user.id > 0).select(
            db.atms_auth_user.email,
            db.atms_auth_user.id)
        users_indexes = [d.atms_auth_user.id for d in rows]
        if len(rows) < len(users):
            pSumWorst = min(0, pSumWorst)
            pAvg = float(pAvg) / (
                1 + float(len(users) - len(rows)) / len(rows))
        for user in users:
            if user.id in users_indexes:
                user.max = rows[users_indexes.index(user.id)][exp_max]
                user.avg = rows[users_indexes.index(user.id)][exp_avg]
                user.sum = rows[users_indexes.index(user.id)][exp_sum]
                pSumWorst = min(user.sum, pSumWorst)
            else:
                continue

            d = {}
            d["user"] = user.id
            d["awards"] = []

            if cat == 'general':
                if user.max == pMax and user.sum > th:
                    d["awards"].append(dict(name=name + '_best_day', count=1))
                if user.avg > pAvg and pAvg > th:
                    d["awards"].append(dict(name=name + '_above_avg', count=1))
                if user.sum <= pSumWorst:
                    d["awards"].append(dict(name=name + '_worst', count=1))
                if user.sum == pSumBest and pSumBest > th:
                    d["awards"].append(dict(name=name + '_best', count=1))
            elif user.sum == pSumBest and pSumBest > th:
                if cat == 'wiki':
                    d["awards"].append(
                        dict(name=name + '_best_wiki', count=1))
                if cat == 'meal':
                    d["awards"].append(
                        dict(name=name + '_best_eater', count=1))
                if cat == 'news':
                    d["awards"].append(
                        dict(name=name + '_best_news', count=1))
                if cat == 'test':
                    d["awards"].append(
                        dict(name=name + '_best_tester', count=1))
                if cat == 'mng_test':
                    d["awards"].append(
                        dict(name=name + '_best_manager', count=1))
                if cat == 'timesheet':
                    d["awards"].append(
                        dict(name=name + '_best_timesheet', count=1))
                if cat == 'code':
                    d["awards"].append(
                        dict(name=name + '_best_coder', count=1))
                if cat == 'sautis_issues_reported':
                    d["awards"].append(
                        dict(name=name + '_sautis_satan', count=1))
                if cat == 'chat':
                    d["awards"].append(
                        dict(name=name + '_chatterbox', count=1))

            # print d, pSumBest
            for a in d["awards"]:
                db.atms_awards.update_or_insert(
                    ((db.atms_awards.timestamp == date) &
                     (db.atms_awards.type == a["name"]) &
                     (db.atms_awards.user == d["user"])),
                    timestamp=date,
                    user=d["user"],
                    type=a["name"],
                    count=a["count"])
            db.commit()
    _current = 0.
    _all = (sum(1 for _ in daterange(start, stop, 7)) +
            sum(1 for _ in daterange(start, stop, 30)))

    def print_progress():
        print('!clear!----%.2f%%' % float((_current / _all) * 100))

    print_progress()
    # "WEEK"
    divider = 7
    for d in daterange(start, stop, divider):
        _current += 1
        calcAwards(d, divider)
        calcAwards(d, divider, 'chat')
        calcAwards(d, divider, 'code')
        calcAwards(d, divider, 'wiki')
        calcAwards(d, divider, 'meal')
        calcAwards(d, divider, 'news')
        calcAwards(d, divider, 'test')
        calcAwards(d, divider, 'mng_test')
        calcAwards(d, divider, 'timesheet')
        calcAwards(d, divider, 'sautis_issues_reported')
        print_progress()
    # "MONTH"
    divider = 30
    for d in daterange(start, stop, divider):
        _current += 1
        calcAwards(d, divider)
        calcAwards(d, divider, 'chat')
        calcAwards(d, divider, 'code')
        calcAwards(d, divider, 'wiki')
        calcAwards(d, divider, 'meal')
        calcAwards(d, divider, 'news')
        calcAwards(d, divider, 'test')
        calcAwards(d, divider, 'mng_test')
        calcAwards(d, divider, 'timesheet')
        calcAwards(d, divider, 'sautis_issues_reported')
        print_progress()
    db.commit()


def task_notifyUser(id=None, lang='en'):
    import re
    replaceLocalHost = re.compile(r'127\.0\.0\.1(:\d+)?')
    users = []
    T.force(lang)
    if int(id) == 0:
        users = db(db.atms_auth_user.id > 0)(
            db.atms_auth_user.is_active == True).select()
    else:
        users = [db.atms_auth_user[id]]

    for user in users:
        rows = db((db.atms_cases.user == user.id) &
                  (db.atms_cases.status.belongs(['new', 'pending']))).select()
        query = db.atms_comments.members.contains(user.id)
        query &= ((~db.atms_comments.read_by.contains(user.id)) |
                  (db.atms_comments.read_by == None))
        count = db.atms_comments.id.count()
        msgs = db(query).select(count).first()[count]

        if not len(rows) and msgs == 0:
            continue
        ul = UL()
        for r in rows:
            ul.append(LI("[%d] %s (%s)" % (r.id, r.name, T(r.status))))
        if msgs > 0:
            ul.append(LI(T("You have %d messages to read") % msgs))
        context = dict(
            title='SAUT ATMS',
            title_href=URL('atms', 'manager', 'index',
                           scheme=True, host=True),
            msg_title=T("There is job to do"),
            msg_href=URL('atms', 'manager', 'index',
                         scheme=True, host=True, extension="html"),
            msg_note=(T('Try to do your best %s %s.') % (
                user.first_name,
                user.last_name)),
            msg_body=ul,
            user='%s %s' % (user.first_name, user.last_name),
            username=user.first_name,
            useremail=user.email)
        message = response.render('mail_message.html', context)
        message = replaceLocalHost.sub('saut-test.polsatc', message)
        mail.send(
            user.email,
            T('[SAUT ATMS] New %d tasks and %d messages for %s appeared') % (
                len(rows), msgs, user.first_name),
            message)


# zadanie odpowiedzialne za wysylanie wiadomosci
# przypominajacych uzytkownikom o regularnym wypelnianiu timesheeta


def task_timesheetReminder():

    '''
    OPIS URUCHOMIENIA REMINDERA
    1. uruchomic server_run.sh
       trzeba pamietac o ustawieniu nazwy hosta -K atms:NAZWAHOSTA w pliku
    2. odkomentowac currentDate, zeby ustawiac sobie date z palca
       i mail.send() wpisujac swojego meila, zeby nie spamowac ludziom
    3. w webowym panelu admina web2py przechodzimy
       do atms i do zakladki 'database administration'
    4. dodajemy record w tabeli scheduler_task:
        - application name: atms/timesheet
        - task name: hidden_NAZWATASKA np. hidden_reminderTest
        - group name: NAZWAHOSTA (czyli na przyklad nasz
          komp sie nazywa 'praca' to bedzie 'praca')
        - function name: timesheetReminder
        - mozna tez ustawic na przyklad repeats i period
          zeby powtarzalo sie codziennie robimy
          repeats: 0
          period: 86400 (60 * 60 * 24)
          prevent drift: true
    '''
    import re
    replaceLocalHost = re.compile(r'127\.0\.0\.1(:\d+)?')

    months = {
        1: 'stycznia',
        2: 'lutego',
        3: 'marca',
        4: 'kwietnia',
        5: 'maja',
        6: 'czerwca',
        7: 'lipca',
        8: 'sierpnia',
        9: 'września',
        10: 'października',
        11: 'listopada',
        12: 'grudnia'}

    currentDate = datetime.datetime.now().date()
    # currentDate = datetime.datetime(2016, 3, 14).date() # TESTY
    currentYear = currentDate.year
    currentDay = currentDate.day
    previousMonth = currentDate.month - 1
    monday = 1
    workday = 8
    firstDayOfMonth = 1
    lowerDateLimit = currentDate.replace(day=1)

    daygenerator = (
        lowerDateLimit + datetime.timedelta(x + 1)
        for x in xrange((currentDate - lowerDateLimit).days))
    workingDays = sum(1 for day in daygenerator if day.weekday() < 5)
    workingHours = workingDays * workday

    msgTitle = 'Rozliczenie Timesheet od początku miesiąca'

    if currentDay == firstDayOfMonth:
        lowerDateLimit = datetime.datetime(
            currentYear, previousMonth, firstDayOfMonth).date()
        msgTitle = "Podsumowanie %s" % (months[previousMonth])

    if currentDate.isoweekday() == monday or currentDay == firstDayOfMonth:
        users = {}
        usersMap = {}
        # wyszukuje wszystkich aktywnych uzytkownikow w bazie
        # tworzy slownik - username -> czas pracy
        # tworzy slownik - id -> username
        for row in db(db.atms_auth_user.is_active == True).select():
            fullName = row.first_name + ' ' + row.last_name
            users[fullName] = 0
            usersMap[row.id] = fullName

        # wyszukuje wszystkie wpisy nowsze od lowerDateLimit
        # przypisuje liczbe godzin do odpowiednich uzytkownikow
        for row in db((db.atms_timesheet_task.start >= lowerDateLimit) &
                      (db.atms_timesheet_task.start <= currentDate)).select():
            try:
                users[usersMap[row.user_id]] += float(row.duration)
            except:
                pass

        ul = TABLE()
        # wyswietlamy statystki kazdego uzytkownika
        # imie nazwisko     liczba godzin
        # jesli uzytkownik nie wypelnil odpowiedniej liczby godzin,
        # to zostaje to zaznaczone

        for user in sorted(users.items(), key=lambda x: x[1], reverse=True):
            if user[1] < workingHours * 0.6:
                ul.append(TR(TD("%s" % (user[0]), TD("%.1f h" % (user[1]))),
                             _style="color:red;"))
            elif user[1] < workingHours * 0.8:
                ul.append(TR(TD("%s" % (user[0]), TD("%.1f h" % (user[1]))),
                             _style="color:orange;"))
            else:
                ul.append(TR(TD("%s" % (user[0]), TD("%.1f h" % (user[1]))),
                             _style="color:green;"))

        context = dict(
            title='RAPORT TIMESHEET',
            title_href=URL('index',
                           scheme=True, host=True),
            msg_title=T(msgTitle),
            msg_href=URL('index',
                         scheme=True, host=True),
            msg_note=(T(
                'Każda osoba zaznaczona na czerwono '
                'powinna uzupełnić Timesheet')),
            msg_body=ul)

        context['useremail'] = T("")
        context['user'] = T("")
        message = response.render('mail_message.html', context)
        message = replaceLocalHost.sub('saut-test.polsatc', message)
        mail = Mail()
        mail.settings.server = myconf.take(tag + '.email_server')
        mail.settings.sender = myconf.take(tag + '.email_sender')
        mail.settings.login = myconf.take(tag + '.email_login')
        # mail.send('pszklanko@cyfrowypolsat.pl',
        mail.send('puk_dauk@cyfrowypolsat.pl',
                  '[TIMESHEET] Przypomnienie o ewidencji czasu pracy',
                  message)


def task_meal(stage=1):
    from gluon.fileutils import read_file
    from gluon.restricted import compile2
    path = 'applications/atms/models/meal/db.py'
    ccode = compile2(read_file(path), path)
    # ugly solution - there is a need to impove it!
    try:
        exec ccode in current.globalenv
    except:
        pass
    _group_rate_set_mealorders(stage)


scheduler = Scheduler(
    db,
    heartbeat=settings.heartbeat,
    migrate=settings.migrate,
    ticker_group='main',
    tasks=dict(
        meal=task_meal,
        reportServerStatus=task_reportServerStatus,
        webthumb=task_webThumb,
        addTcases=task_addTcases,
        apktool=mt.task_apktool,
        listAndroidDevices=mt.task_listAndroidDevices,
        runTest=test.task_runTest,
        reboot=mt.task_reboot,
        rebootHost=task_rebootHost,
        restartATMS=task_restartATMS,
        restartAdb=task_restartAdb,
        scanLab=task_scanlab,
        # makeWikiReport=mt.task_makeWikiReport,
        # makePartialWikiReport=mt.task_makePartialWikiReport,
        # makePDFReport=mt.task_makePDFReport,
        adbConsole=mt.task_adbConsole,
        calculateAwards=task_calculateAwards,
        dumpTcases=task_dumpTcases,
        notifyUser=task_notifyUser,
        recalcRates=task_recalcRates,
        performGameAction=game.task_performGameAction,
        parseUECap=task_parseUECap,
        parseWiFiCap=task_parseWiFiCap,
        timesheetReminder=task_timesheetReminder))
# stb_runTest=stb.task_runTest,
# stb_listMeasDevices=stb.task_listMeasDevices))


def task_status_changed(s, f):
    websocket_send(settings.ws_server, '',
                   settings.security_key, 'scheduler_task')
    websocket_send(settings.ws_server, '',
                   settings.security_key, 'atms_cases')


db.scheduler_task._after_update.append(
    lambda s, f: task_status_changed(s, f))
db.scheduler_task._after_insert.append(
    lambda f, id: task_status_changed(f, id))

current.db = db
current.settings = settings
current.scheduler = scheduler
current.logger = logger
current.fs = fs

# example: myjob.add_deps(pants.id, undershorts.id)
dep_graph = JobGraph(db, 'job_0')


def queue_task_dep(*args, **kwargs):
    GLOBAL_BLOCKING_TASKS = [
        'rebootHost', 'restartAdb', 'restartATMS', 'webthumb', 'scanLab']

    exclude_ids = []

    def excludeq():
        return (~db.scheduler_task.id.belongs(exclude_ids))

    baseq = db.scheduler_task.status.contains(
        ['QUEUED', 'ASSIGNED', 'RUNNING'], all=False)

    # prevent from having multiple general tasks in a queue
    count = db(
        (baseq) &
        (db.scheduler_task.function_name == args[0]) &
        (db.scheduler_task.function_name.belongs(GLOBAL_BLOCKING_TASKS +
         ['listAndroidDevices', 'webthumb', 'scanLab'])) &
        (db.scheduler_task.group_name == kwargs['group_name'])).count()
    if count > 0:
        return

    # sequential order for tasks for devices
    query = baseq
    if args[0] in ['runTest', 'reboot', 'adbConsole']:
        # if 'pvars' in kwargs:
        #     pvars = kwargs['pvars']
            # if 'dev_id' in pvars:
            #     query &= db.scheduler_task.vars.contains(
            #         "dev_id\": \"%s\"" % str(pvars['dev_id']))
        if 'devices' in kwargs:
            query &= db.scheduler_task.devices.contains(
                kwargs['devices'], all=False)

        task_id = scheduler.queue_task(*args, **kwargs)
        exclude_ids.append(task_id)

        row = db(query)(excludeq()).select(db.scheduler_task.id)
        if row:
            row = row.last()
            exclude_ids.append(row.id)
            dep_graph.add_deps(task_id, row.id)
    else:
        task_id = scheduler.queue_task(*args, **kwargs)
        exclude_ids.append(task_id)

    # place current BLOCKING task after pending tasks
    if args[0] in GLOBAL_BLOCKING_TASKS:
        rows = db(
            (baseq) &
            (db.scheduler_task.group_name == kwargs['group_name']) &
            (excludeq())).select(db.scheduler_task.id)
        for r in rows:
            exclude_ids.append(r.id)
            dep_graph.add_deps(task_id, r.id)

    # place current after BLOCKING tasks
    rows = db(
        (baseq) &
        (db.scheduler_task.group_name == kwargs['group_name']) &
        (db.scheduler_task.function_name.belongs(GLOBAL_BLOCKING_TASKS)) &
        (excludeq())).select(db.scheduler_task.id)
    for r in rows:
        exclude_ids.append(r.id)
        dep_graph.add_deps(task_id, r.id)

    db.commit()
    return task_id

current.queue_task_dep = queue_task_dep
