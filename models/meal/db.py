# -*- coding: utf-8 -*-
from gluon.storage import Storage
import datetime
from misc import is_holiday
STAGE1_HOUR = datetime.datetime.now().replace(
    hour=10, minute=0, second=0, microsecond=0)
STAGE2_HOUR = datetime.datetime.now().replace(
    hour=11, minute=50, second=0, microsecond=0)
DCOUNT_IMPORTANCE_FACTOR = 1


db.define_table(
    'atms_canteen',
    Field('f_name', 'string'),
    Field('f_contact', 'string'),
    Field('f_desc', 'text'),
    Field('f_atplace', 'boolean', default=False),
    format='%(f_name)s')

db.define_table(
    'atms_order',
    Field('f_date', 'date', default=datetime.date.today(), readable=False),
    Field('f_1', 'reference %s' % db.atms_canteen),
    Field('f_2', 'reference %s' % db.atms_canteen),
    Field('f_3', 'reference %s' % db.atms_canteen),
    Field('f_final', 'reference %s' % db.atms_canteen),
    Field('f_name', 'text', default='np. Kurczak po wietnamsku'),
    Field('f_cost', 'float', default=16),
    Field('f_cash', 'float', default=0),
    Field('f_not_interested', 'boolean', default=False),
    # Field.Virtual('debt',
    #               lambda row: row.atms_order.f_cost - row.atms_order.f_cash),
    auth.signature)

db.define_table(
    'atms_settlement',
    Field('f_date', 'date', default=datetime.date.today(), readable=False),
    Field('f_cost2share', 'float', default=0),
    Field('f_notes', 'text', default=''),
    auth.signature)

f_validator = IS_EMPTY_OR(IS_IN_DB(
    db, 'atms_canteen.id', db.atms_canteen._format, multiple=False))
db.atms_order.f_1.requires = f_validator
db.atms_order.f_2.requires = f_validator
db.atms_order.f_3.requires = f_validator
db.atms_order.f_final.requires = f_validator


def _is_before_stage(stage_hour):
    return stage_hour > datetime.datetime.now()


def _is_after_stage(stage_hour):
    return ~_is_before_stage(stage_hour)


def _group_rate_set_mealorders(stage=2):
    from urlparse import urlparse
    from collections import defaultdict
    # there is no need to calculate anything before STAGE1
    if _is_before_stage(STAGE1_HOUR):
        return None

    # do nothing if weekend
    if is_holiday(datetime.datetime.now()):
        return None

    COUNT_TH = 2
    WAGES = dict(f_1=4, f_2=2, f_3=1)
    ORDERS_NB = 0

    baseq = ((db.atms_order.f_not_interested == False) &
             (db.atms_order.f_date == datetime.date.today()))
    count = db.atms_order.id.count()
    rows = db(baseq)(
        (db.atms_order.f_1 != None) |
        (db.atms_order.f_2 != None) |
        (db.atms_order.f_3 != None)).select(count)
    if rows:
        ORDERS_NB = rows.first()[count]

    rank = Storage()
    for w in WAGES:
        rows = db(db.atms_order[w] != None)(baseq).select(
            db.atms_order[w],
            count, groupby=db.atms_order[w])
        for r in rows:
            if r.atms_order[w] not in rank:
                rank[r.atms_order[w]] = Storage(count=0, points=0)
            rank[r.atms_order[w]].count += r[count]
            rank[r.atms_order[w]].points += r[count] * WAGES[w]

    # filter
    rank = sorted([(k, v.count, v.points) for k, v
                   in rank.items() if v.count >= COUNT_TH],
                  key=lambda r: r[2])

    # set final options
    for r in rank:
        db(((db.atms_order.f_1 == r[0]) |
            (db.atms_order.f_2 == r[0]) |
            (db.atms_order.f_3 == r[0])) &
            (db.atms_order.f_not_interested == False) &
            (db.atms_order.f_date == datetime.date.today())).update(
                f_final=r[0])
        db.commit()

    # remove orders that count is too small
    to_clean = db((db.atms_order.f_date == datetime.date.today()) &
                  (db.atms_order.f_final != None)).select(
        db.atms_order.f_final,
        count,
        groupby=db.atms_order.f_final,
        having=count < COUNT_TH)
    db((db.atms_order.f_date == datetime.date.today()) &
       (db.atms_order.f_final.belongs(
        [r.atms_order.f_final for r in to_clean]))).update(f_final=None)
    db.commit()

    sys_user = db.atms_auth_user(email='saut@cyfrowypolsat.pl')
    if _is_before_stage(STAGE2_HOUR) or stage == 1:
        time_left = STAGE2_HOUR - datetime.datetime.now()
        hours_left = time_left.seconds // 3600
        minutes_left = (time_left.seconds // 60) % 60
        msg_body = 'Do tej pory:\n'
        for r in sorted(rank, key=lambda r: r[2], reverse=True):
            msg_body += '- zaznaczono %d opcje na jedzenie z %s\n' % (
                r[1], db.atms_canteen[r[0]].f_name)
        if len(rank) == 0:
            msg_body += '- Za mało zamówień by stworzyć choćby jedną grupę' + \
                ' (min %d osoby z podobnymi preferencjami)' % (COUNT_TH)
        if ORDERS_NB:
            msg_body += (
                '\n- ``%d - liczba osób, które ' % ORDERS_NB +
                'wpisało swoje dzisiejsze preferencje``:color[red]')

        host = urlparse(settings.ws_server).hostname
        context = dict(
            title='Obiad - raport chwilowy',
            title_href=None,
            msg_title='Kliknij tutaj by zmienić swoje zamówienie',
            msg_href=URL('manager', 'index',
                         args=['meal', 'update', 0],
                         scheme=True, host=host),
            msg_note='Popraw swoje zamówienie na bazie poniższych danych. ' +
                     'Masz %dh:%dm na rezygnację lub korektę. ' % (
                         hours_left, minutes_left) +
                     'Ostatecznie wygrają najpopularniejsze opcje ' +
                     'z uwzględnieniem jak bardzo są pożądane!',
            msg_body=auth._wiki.markmin_base(msg_body),
            user='%s %s' % (sys_user.first_name, sys_user.last_name),
            username=sys_user.first_name,
            useremail=sys_user.email)

        message = response.render('mail_message.html', context)
        mail = Mail()
        mail.settings.server = settings.email_server
        mail.settings.sender = settings.email_sender
        mail.settings.login = settings.email_login

        target_email = 'puk_dauk_saut@cyfrowypolsat.pl'
        if 'prod' not in settings.config:
            target_email = 'zpomianowski@cyfrowypolsat.pl'
        mail.send(
            target_email,
            '[OBIAD] Raport chwilowy ' +
            str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M')),
            message)

        return None

    # pick responsible people
    # abort if too small number of orders
    if len(rank) == 0:
        return None

    # responsible users already set
    if (db.atms_auth_user(duty_last_date=datetime.date.today()) is not None and
            'prod' in settings.config):
        return None

    # clear duties
    db(db.atms_auth_user.id > 0).update(duty_canteen=None)
    db.commit()

    responsible_users = []
    for r in rank:
        baseq = (((db.atms_order.f_final == r[0])) &
                 (db.atms_order.f_not_interested == False) &
                 (db.atms_order.f_date == datetime.date.today()))
        rows = db(baseq)(
            db.atms_auth_user.id == db.atms_order.created_by)(
            db.atms_order.f_final == db.atms_canteen.id).select(
            db.atms_order.f_final,
            db.atms_canteen.f_atplace,
            db.atms_auth_user.id,
            db.atms_auth_user.email,
            db.atms_auth_user.duty_canteen,
            db.atms_auth_user.duty_count,
            db.atms_auth_user.duty_last_date)

        if not rows:
            continue

        # choose justly responsible users
        rows = sorted(
            rows,
            key=lambda row: (
                (datetime.date.today() -
                 (row.atms_auth_user.duty_last_date or datetime.date.today())
                 ).days - (DCOUNT_IMPORTANCE_FACTOR *
                           row.atms_auth_user.duty_count)),
            reverse=True)

        if rows[0].atms_canteen.f_atplace == True:
            continue

        # ruser - responsible user
        ruser = db.atms_auth_user[rows[0].atms_auth_user.id]
        # if 'dev' in settings.config:
        #     ruser = db.atms_auth_user[9]
        if ruser.duty_last_date != datetime.date.today():
            ruser.duty_last_date = datetime.date.today()
            ruser.duty_count = ruser.duty_count + 1 \
                if ruser.duty_count is not None else 0
        ruser.duty_canteen = r[0]
        ruser.update_record()
        db.commit()
        responsible_users.append(ruser)

    # final emails
    interested_users_emails = []
    baseq = ((db.atms_order.f_not_interested == False) &
             (db.atms_order.f_date == datetime.date.today()))
    rows = db(baseq)(db.atms_auth_user.id == db.atms_order.created_by).select(
        db.atms_auth_user.first_name,
        db.atms_auth_user.last_name,
        db.atms_auth_user.email,
        db.atms_order.f_name,
        db.atms_order.f_cost,
        db.atms_order.f_cash,
        db.atms_canteen.f_name,
        left=db.atms_canteen.on(db.atms_order.f_final == db.atms_canteen.id))

    none_key = 'Osoby wykluczone'

    summary = Storage()
    cost_summary = defaultdict(lambda: [0, 0, 0])
    for r in rows:
        interested_users_emails.append(r.atms_auth_user.email)
        key = r.atms_canteen.f_name
        key = key if key else none_key
        if key not in summary:
            summary[key] = []
        if key == none_key:
            summary[key].append('%s %s' %
                                (r.atms_auth_user.first_name,
                                 r.atms_auth_user.last_name))
            continue
        summary[key].append('--%s %s - %.2f (%.2f) PLN - **%s**' % (
            r.atms_auth_user.first_name,
            r.atms_auth_user.last_name,
            r.atms_order.f_cost,
            r.atms_order.f_cash,
            r.atms_order.f_name.replace('\n', '[[NEWLINE]]')))

    for r in rows:
        key = r.atms_canteen.f_name
        key = key if key else none_key
        cost_summary[key][0] += r.atms_order.f_cost
        cost_summary[key][1] += r.atms_order.f_cash
        cost_summary[key][2] += r.atms_order.f_cash - r.atms_order.f_cost

    msg_body = "Podsumowanie zamówień z uwzględnieniem preferencji:\n"
    for k, v in summary.items():
        value = '\n'.join(v)
        cost_info = '``%.2f / **%.2f** / %.2f``:%s PLN' % (
            cost_summary[k][1],
            cost_summary[k][0],
            cost_summary[k][2],
            'blue' if cost_summary[k][2] >= 0 else 'red')
        if k == none_key:
            cost_info = ''
            value = '--' + ', '.join(v)
        msg_body += '- **%s** %s\n%s\n' % (k, cost_info, value)

    rmsg_body = 'Osoby odpowiedzialne za zamówienie:\n'
    for user in responsible_users:
        canteen = db.atms_canteen[user.duty_canteen]
        rmsg_body += "-**%s - ''%s %s''**\n--''%s''\n--''%s''\n" % (
            canteen.f_name,
            user.first_name,
            user.last_name,
            canteen.f_contact,
            canteen.f_desc.replace('\n', '[[NEWLINE]]'))

    msg_body = rmsg_body + msg_body
    T.force('pl')
    context = dict(
        title='Obiad - raport finalny',
        title_href=None,
        msg_title='Szykujcie żołądki...',
        msg_href=None,
        msg_note='Raport finalny. Już nie można robić korekty zamówienia.',
        msg_body=auth._wiki.markmin_base(msg_body),
        user='%s %s' % (sys_user.first_name, sys_user.last_name),
        username=sys_user.first_name,
        useremail=sys_user.email)

    message = response.render('mail_message.html', context)
    mail = Mail()
    mail.settings.server = settings.email_server
    mail.settings.sender = settings.email_sender
    mail.settings.login = settings.email_login

    if 'prod' not in settings.config:
        interested_users_emails = 'zpomianowski@cyfrowypolsat.pl'
    mail.send(
        interested_users_emails,
        '[OBIAD] Raport finalny ' +
        str(datetime.date.today()),
        message)

    return summary


def _settle(settlement_id):
    s = db.atms_settlement[settlement_id]
    uresponsible = s.created_by
    final_order = db.atms_auth_user(id=uresponsible).duty_canteen

    # to change!
    query = (db.atms_order.f_final == final_order)
    query &= (db.atms_order.f_date == datetime.date.today())
    query &= (db.atms_order.created_by == db.atms_auth_user.id)
    rows = db(query).select(
        db.atms_auth_user.id,
        db.atms_auth_user.first_name,
        db.atms_auth_user.last_name,
        db.atms_auth_user.email,
        db.atms_auth_user.account_nb,
        db.atms_order.f_name,
        db.atms_order.f_cost,
        db.atms_order.f_cash)

    borrowers = []
    lenders = []

    overflow = sum([r.atms_order.f_cash - r.atms_order.f_cost for r in rows])
    cost2share = round(s.f_cost2share / len(rows), 2)
    for r in rows:
        owes = r.atms_order.f_cost - r.atms_order.f_cash + cost2share
        if uresponsible == r.atms_auth_user.id:
            owes += overflow - s.f_cost2share
        x = Storage(dict(
            user_id=r.atms_auth_user.id,
            first_name=r.atms_auth_user.first_name,
            last_name=r.atms_auth_user.last_name,
            email=r.atms_auth_user.email,
            account_nb=r.atms_auth_user.account_nb,
            owns=owes))
        if owes > 0:
            borrowers.append(x)
        elif owes < 0:
            lenders.append(x)

    borrowers = sorted(borrowers, key=lambda i: i.owns, reverse=True)
    lenders = sorted(lenders, key=lambda i: i.owns, reverse=True)

    msg_body = 'Opis długów:\n'
    if s.f_notes:
        msg_body = 'Uwagi:\n- %s\n' % s.f_notes + msg_body
    for b in iter(borrowers):
        for l in iter(lenders):
            cash = min(b.owns, -l.owns)
            b.owns -= cash
            l.owns += cash
            if cash > 0:
                msg_body += (
                    "- ``%s``:red powinien zapłacić ``%s``:blue" +
                    " **%.2f** PLN\n") % (
                    b.first_name + ' ' + b.last_name,
                    l.first_name + ' ' + l.last_name,
                    cash)
                if l.account_nb:
                    msg_body += '---- Nr konta: ``%s``\n' % l.account_nb

    sys_user = db.atms_auth_user(email='saut@cyfrowypolsat.pl')
    main_title = 'rozliczenie kasy: %s ' % \
        db.atms_canteen[final_order].f_name

    T.force('pl')
    context = dict(
        title='Obiad - ' + main_title,
        title_href=None,
        msg_title='Rozliczenie uwzględnia dodatkowy %s PLN do podziału' %
                  ('koszt: %.2f' % s.f_cost2share if s.f_cost2share >= 0
                   else 'rabat: %.2f' % -s.f_cost2share),
        msg_href=None,
        msg_note='Wiadomość tą dostają jedynie osoby ' +
                 'z nieuregulowanym rachunkiem oraz dyżurny zamówienia.',
        msg_body=auth._wiki.markmin_base(msg_body),
        user='%s %s' % (sys_user.first_name, sys_user.last_name),
        username=sys_user.first_name,
        useremail=sys_user.email)

    message = response.render('mail_message.html', context)
    mail = Mail()
    mail.settings.server = settings.email_server
    mail.settings.sender = settings.email_sender
    mail.settings.login = settings.email_login

    interested_users_emails = [i.email for i in borrowers + lenders]
    if len(interested_users_emails) == 0:
        # no need to send any spam messages
        return

    if 'prod' not in settings.config:
        interested_users_emails = 'zpomianowski@cyfrowypolsat.pl'
    mail.send(
        interested_users_emails,
        '[OBIAD] ' + main_title.title() + str(datetime.date.today()),
        message, cc=uresponsible.email)
    addExp(len(interested_users_emails) * 10, 'meal')
