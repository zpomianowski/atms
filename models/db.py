# -*- coding: utf-8 -*-

#########################################################################
# This scaffolding model makes your app work on Google App Engine too
# File is released under public domain and you can use without limitations
#########################################################################

# if SSL/HTTPS is properly configured and you want all HTTP requests to
# be redirected to HTTPS, uncomment the line below:
# request.requires_https()
import helpers
from gluon.contrib.redis_utils import RConn
from gluon.contrib.redis_cache import RedisCache
from gluon.contrib.redis_session import RedisSession
from gluon.tools import Auth, Crud, Service, PluginManager

rconn = RConn(settings.redis_url, 6379)
cache.redis = RedisCache(redis_conn=rconn, debug=True)


if settings.host in ['zbh']:
    import os
    from gluon.custom_import import track_changes
    settings.migrate = True
    track_changes(True)
    INSTALL_PATH = '/atms'
    os.environ["PATH"] = os.environ["PATH"] + (
        ':%(h)s/nodejs/bin/:%(h)s/android-sdk-linux/tools' +
        ':%(h)s/android-sdk-linux/tools/bin' +
        ':%(h)s/android-sdk-linux/platform-tools') % dict(
        h=INSTALL_PATH)

# set global innodb_lock_wait_timeout = 180;
# set global innodb_deadlock_detect=0;
db = DAL(settings.database_uri,
         migrate_enabled=settings.migrate,
         lazy_tables=True,
        #  lazy_tables=False,
        #  fake_migrate_all=settings.migrate,
        #  fake_migrate=True,
         pool_size=10
         )

session.connect(request, response,
                db=RedisSession(redis_conn=rconn, session_expiry=False))
# or store session in Memcache, Redis, etc.
# from gluon.contrib.memdb import MEMDB
# from google.appengine.api.memcache import Client
# session.connect(request, response, db = MEMDB(Client()))

# by default give a view/generic.extension to all actions from localhost
# none otherwise. a pattern can be 'controller/function.extension'
response.generic_patterns = ['*'] if request.is_local else []

# (optional) optimize handling of static files
# response.optimize_css = 'concat,minify,inline'
# response.optimize_js = 'concat,minify,inline'

auth = Auth(db)
crud, service, plugins = Crud(db), Service(), PluginManager()
crud.settings.auth = auth

auth.settings.expiration = 3600 * 24
auth.settings.long_expiration = 3600 * 24 * 30
auth.settings.remember_me_form = True

auth.settings.allow_basic_login = True

auth.settings.table_user_name = 'atms_auth_user'
auth.settings.table_group_name = 'atms_auth_group'
auth.settings.table_membership_name = 'atms_auth_membership'
auth.settings.table_permission_name = 'atms_auth_permission'
auth.settings.table_event_name = 'atms_auth_event'
auth.settings.table_cas_name = 'atms_auth_cas'
auth.define_signature()


def atms_auth_user_changed(uset, fields):
    import urllib
    import urllib2
    import hashlib
    import dominant_colors as dc
    default = "identicon"
    size = 100

    if 'email' not in fields:
        return

    if uset:
        old_email = uset.select(
            db.atms_auth_user.email).first()[db.atms_auth_user.email]
        new_email = fields['email']
        if old_email == new_email:
            return

    username = fields['email'].split('@')[0]

    gravatar_url = "http://www.gravatar.com/avatar/" + \
        hashlib.md5(fields['email'].lower()).hexdigest() + "?"
    gravatar_url += urllib.urlencode({'d': default, 's': str(size)})

    filename = "atms_auth_user.identicon.%s_100" % username

    def dlIdenticon(filename, proxies=None):
        if proxies:
            proxy = urllib2.ProxyHandler(proxies)
            opener = urllib2.build_opener(proxy)
        else:
            opener = urllib2.build_opener()
        res = opener.open(gravatar_url)
        data = res.read()
        if data[:4] == '\xff\xd8\xff\xe0':
            filename += ".jpg"
        else:
            filename += ".png"

        file_handler = open('/tmp/%s' % filename, 'wb')
        file_handler.write(data)
        file_handler.close()
        return filename

    filename = dlIdenticon(filename)

    new_color = dc.colorz('/tmp/%s' % filename, 5)[0]

    aux = db.atms_auth_user.identicon.store(
        open('/tmp/%s' % filename, 'rb'), filename)

    # modify form
    fields['color'] = new_color
    fields['identicon'] = aux

    os.unlink('/tmp/%s' % filename)


auth.settings.extra_fields[auth.settings.table_user_name] = [
    Field('sautis_user_id', 'integer', writable=False, readable=False),
    Field('identicon', 'upload', default=None, uploadfs=fs,
          autodelete=True, writable=False, readable=True,
          label=A('Gravatar', _href="https://gravatar.com/")),
    Field('color', 'string', default='#ff0000',
          writable=False, readable=False),
    Field('is_active', 'boolean', default=True,
          writable=False, readable=False),
    Field('coins', 'float', default=0, writable=False, label=T('Coins')),
    Field('duty_count', 'integer', default=0, writable=False, readable=False),
    Field('duty_last_date', 'date',
          default=None, writable=False, readable=False),
    Field('duty_canteen', 'integer',
          default=None, writable=False, readable=False),
    Field('account_nb', 'string', default=None,
          label=T('Account nb to pay for meals')),
    Field('registration_date', 'date', default=request.now.date(),
          writable=False, readable=False)]


def string_to_int(s):
    return sum(map(lambda x: ord(x), s))


def removeNonAscii(s):
    return "".join(i for i in s if ord(i) < 128)


# create all tables needed by auth if not custom tables

auth.define_tables(username=False)
db.atms_auth_user._before_update.append(
    lambda s, f: atms_auth_user_changed(s, f))
db.atms_auth_user._before_insert.append(
    lambda f: atms_auth_user_changed(None, f))

# configure email
mail = auth.settings.mailer

mail.settings.server = settings.email_server
mail.settings.sender = settings.email_sender
mail.settings.login = settings.email_login
mail.settings.tls = True

# configure auth policy
auth.enable_record_versioning(db)
# auth.settings.actions_disabled += (
#     'register', 'retrieve_username', 'request_reset_password')
auth.settings.registration_requires_verification = False
# auth.settings.registration_requires_approval = True
auth.settings.reset_password_requires_verification = True

# after defining tables, uncomment below to enable auditing
auth.enable_record_versioning(db)

response.generic_patterns = ['*']

auth.navbar(mode='bare')
welcome = T('User settings') if auth.bar['user'] is None \
    else auth.bar['prefix'] + auth.bar['user']
response.menu = [[welcome, False, None, []]] + response.menu

for k, v in auth.bar.items():
    if k == 'prefix' or k == 'user':
        continue
    response.menu[0][3] += [(k, False, v, [])]


def get_extra(data, table_name, extra):
    if table_name == 'atms_devices':
        dev_ids = None
        if 'tcase_id' in extra:
            dev_ids = db.atms_tcases[extra.tcase_id].auto_devs or []
        if 'case_id' in extra:
            dev_ids = db.atms_cases[extra.case_id].auto_devs or []

        if dev_ids:
            for r in data:
                r.check = True if r.id in dev_ids else False
                task_query = (db.scheduler_task.vars.contains(
                    "dev_id\": \"%s\"" % str(r.id))) & \
                    (db.scheduler_task.status.contains(
                        ['QUEUED', 'ASSIGNED', 'RUNNING'], all=False))
                r.queue = db(task_query).count()
                r.runs = 0
                r.last_run = None
                r.report_status = None
                if extra.case_id:
                    r.runs = db(
                        (db.scheduler_task.vars.contains(
                            "dev_id\": \"%s\"" % str(r.id))) &
                        (db.scheduler_task.vars.contains(
                            "case_id\": \"%s\"" % str(extra.case_id))) &
                        (db.scheduler_task.status.contains(
                            ['COMPLETED', 'FAILED', 'TIMEDOUT'],
                            all=False))
                        ).count()
                    report_query = (
                        (db.atms_reports.case_id == extra.case_id) &
                        (db.atms_reports.dev_id == r.id))
                    last_run = db(report_query).select(
                        db.atms_reports.modified_on).last()
                    if last_run:
                        r.last_run = last_run[db.atms_reports.modified_on]
                    report_status = db(report_query).select(
                        db.atms_reports.status).last()
                    if report_status:
                        r.report_status = report_status[
                            db.atms_reports.status]
            return data
    return data
