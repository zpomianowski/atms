from json import loads, dumps

db.define_table(
    'atms_wificap',
    Field('ref_cbs', 'reference cbs_device', ondelete='SET NULL'),
    Field('f_sn', 'string', length=255, default=None),
    Field('f_sw_version', 'string', length=255),
    Field('f_model', 'string', length=255),
    Field('f_company', 'string', length=255),
    Field('f_chipset', 'string', length=255),
    Field('f_caps_json', 'text', default=[], readable=False),
    Field('ref_pcap24', 'integer', default=None),
    Field('ref_pcap5', 'integer', default=None),
    Field('f_processed', 'boolean', default=False),
    # indexes
    Field('f_cat', 'string', default=None),
    Field('f_maxrate24', 'float', default=0),
    Field('f_maxrate5', 'float', default=0),
    auth.signature,
    format='%(f_company)s %(f_model)s %(f_sn)s')

db.atms_wificap.f_caps_json.filter_in = \
    lambda obj, dumps=dumps: dumps(obj)
db.atms_wificap.f_caps_json.filter_out = \
    lambda txt, loads=loads: load_data(txt)


def task_parseWiFiCap(id, type='pdml'):
    if id is None or id == 'null':
        for row in db(db.atms_wificap.id > 0).iterselect(
                db.atms_wificap.id):
            task_parseWiFiCap(row.id, type)
        return

    import subprocess
    from wificap_parser import (xml2caps_data, get_wifi_cat, get_class,
                                CAT_5GHZ, CAT_24GHZ)

    wifi_entry = db(db.atms_wificap.id == id).select().first()

    def extract(filename_id, type=type):
        try:
            filename = db.atms_attachments[filename_id].filename
        except AttributeError:
            return
        file_path = os.path.join(
            os.getcwd(), 'applications/atms/uploads', filename)
        if fs:
            file_path = os.path.join('/tmp', filename)
            with open(file_path, 'wb') as f:
                f.write(fs.open(filename, 'rb').read())
        cmd = 'tshark -r %s -T %s &>1' % (file_path, type)

        process = subprocess.Popen(
            cmd,
            shell=True,
            stdout=subprocess.PIPE)
        value = process.communicate()[0]

        return value

    data = {}

    xml2caps_data(data, extract(
        wifi_entry.ref_pcap24), CAT_24GHZ)
    xml2caps_data(data, extract(
        wifi_entry.ref_pcap5), CAT_5GHZ)
    
    get_wifi_cat(data, CAT_24GHZ, 'ht')
    get_wifi_cat(data, CAT_5GHZ, 'ht')
    get_wifi_cat(data, CAT_5GHZ, 'vht')
    get_class(data)

    wifi_entry.f_processed = True
    wifi_entry.f_cat = data['class']
    wifi_entry.f_caps_json = data
    wifi_entry.f_maxrate24 = max(
        data.get(CAT_24GHZ, {}).get('ht', {}).get('maxrate', 0),
        data.get(CAT_24GHZ, {}).get('vht', {}).get('maxrate', 0))
    wifi_entry.f_maxrate5 = max(
        data.get(CAT_5GHZ, {}).get('ht', {}).get('maxrate', 0),
        data.get(CAT_5GHZ, {}).get('vht', {}).get('maxrate', 0))
    wifi_entry.update_record()
    db.commit()

    websocket_send(settings.ws_server, '',
                   settings.security_key, 'atms_wificap')
