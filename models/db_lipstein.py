from json import loads, dumps

db.define_table(
    'cbs_company',
    Field('name', 'string', length=255),
    format="%(name)s",
    migrate=False)


db.define_table(
    'cbs_device_model',
    Field('name', 'string', length=255),
    format="%(name)s",
    migrate=False)


db.define_table(
    'cbs_device',
    Field('model_id', 'reference cbs_device_model',
          ondelete='NO ACTION'),
    Field('imei', default=None),
    Field('sn', default=None),
    Field('company_id', 'reference cbs_company',
          ondelete='NO ACTION'),
    Field('visible', 'boolean', default=True),
    migrate=False)

db.cbs_device.model_id.requires = IS_IN_DB(
    db, 'cbs_device_model.id', db.cbs_device_model._format, multiple=False)
db.cbs_device.company_id.requires = IS_IN_DB(
    db, 'cbs_company.id', db.cbs_company._format, multiple=False)


db.define_table(
    'atms_uecap',
    Field('ref_cbs', 'reference cbs_device', ondelete='SET NULL'),
    Field('f_imei', 'string', length=255, default=None),
    Field('f_sw_version', 'string', length=255),
    Field('f_model', 'string', length=255),
    Field('f_company', 'string', length=255),
    Field('f_chipset', 'string', length=255),
    Field('f_uecap_hex', 'text'),
    Field('f_uecap_size', 'integer',
          compute=lambda r: len(
              r['f_uecap_hex'].replace(' ', '').strip()) / 2),
    Field('f_ca_json', 'text', default=[]),
    Field('f_caps_json', 'text', default={}),
    Field('f_attach_hex', 'text'),
    Field('f_uecap_hex_txtdump', 'text', readable=False),
    Field('f_attach_hex_txtdump', 'text', readable=False),
    Field('f_uecap_hex_pdmldump', 'text', readable=False),
    Field('f_attach_hex_pdmldump', 'text', readable=False),
    Field('f_processed', 'boolean', default=False),
    # helpers
    Field('f_bands', 'list:integer', default=[]),
    Field('f_category_lte', 'list:integer', default=[]),
    Field('f_ca_search_index', 'string'),
    Field('f_pdn_type', 'string'),
    Field('f_dl_mimo4l', 'list:integer', default=[]),
    Field('f_dl_qam256', 'list:integer', default=[]),
    Field('f_ul_qam64', 'list:integer', default=[]),
    # Field('f_mimoUL', 'integer', default=None),
    auth.signature,
    format='%(f_company)s %(f_model)s %(f_imei)s')

db.atms_uecap.f_ca_json.filter_in = \
    lambda obj, dumps=dumps: dumps(obj)
db.atms_uecap.f_ca_json.filter_out = \
    lambda txt, loads=loads: load_data(txt)
db.atms_uecap.f_caps_json.filter_in = \
    lambda obj, dumps=dumps: dumps(obj)
db.atms_uecap.f_caps_json.filter_out = \
    lambda txt, loads=loads: load_data(txt)


def task_parseUECap(id, type='pdml', gen_xlsx=True, created_by=None):
    """
    Install prerequisites:
    sudo add-apt-repository ppa:wireshark-dev/stable -y
    sudo apt install wireshark-common tshark
    """

    # db(db.atms_uecap.id.belongs([745])).update(f_processed=False)
    # db.commit()
    DEBUG = False

    query = db.atms_uecap.f_processed == False
    limitby = (0, 20)
    if id is None or id == 'null':
        db(db.atms_uecap.id > 0).update(f_processed=False)
        db.commit()
        limitby = None
        gen_xlsx = False

    from uecap_parser import xml2ca_data
    from uecap_parser import xml2caps_data
    from uecap_parser import generate_xlsx
    import subprocess
    import tempfile
    import os
    import time

    start_time = time.time()

    headers = {
        'f_uecap_hex': 'lte-rrc.ul.dcch',
        'f_attach_hex': 'nas-eps'
        }

    def parse(field, r, type):
        dump = tempfile.mktemp(prefix='hexdump_')
        pcap = tempfile.mktemp(prefix='pcap_')
        if DEBUG:
            dump = '/tmp/hexdump'
            pcap = '/tmp/pcap'

        # make hexdump
        cmd = 'echo \"%s\" | xxd -p -r | hd -v > %s' % (r[field], dump)
        process = subprocess.Popen(
            cmd,
            shell=True,
            stdout=subprocess.PIPE, stderr=subprocess.PIPE,
            close_fds=True)
        process.communicate()

        # prepare pcap
        cmd = 'text2pcap -D -l 147 %s %s' % (dump, pcap)
        process = subprocess.Popen(
            cmd,
            shell=True,
            stdout=subprocess.PIPE, stderr=subprocess.PIPE,
            close_fds=True)
        process.communicate()

        cmd = 'tshark -r %s -T %s ' % (pcap, type) + \
            '-V -o "uat:user_dlts:\\"User 0 (DLT=147)\\",' + \
            '\\"%s\\",\\"0\\",\\"\\",\\"0\\",\\"\\"" &>1' % headers[field]

        process = subprocess.Popen(
            cmd,
            shell=True,
            stdout=subprocess.PIPE,
            close_fds=True)
        value = process.communicate()[0]

        os.unlink(dump)
        os.unlink(pcap)
        return value

    for r in db(query).iterselect(orderby=~db.atms_uecap.id, limitby=limitby):
        # r = db(db.atms_uecap.id == id).select().first()
        for k in headers.keys():
            r[k + '_txtdump'] = parse(k, r, 'text')
            r[k + '_pdmldump'] = parse(k, r, 'pdml')

            if (k == 'f_uecap_hex'):
                r.f_ca_json = xml2ca_data(parse(k, r, 'pdml'))
                ca = r.f_ca_json
                idx = []
                for s in ca:
                    bands = []
                    for i in s['items']:
                        bands.append(int(i['bandEUTRA']))
                    idx.append('-'.join([str(b) for b in sorted(bands)]))
                r.f_ca_search_index = '|%s|' % '|'.join(idx)

            parsed_caps = xml2caps_data(parse(k, r, 'pdml'))
            if 'lte_cats' in parsed_caps:
                c1 = parsed_caps['lte_cats']
                c2 = parsed_caps.get('lte_cats_dl', [])
                c3 = parsed_caps.get('lte_cats_ul', [])
                r.f_category_lte = list(set(c1 + c2 + c3))
            if 'pdn_type' in parsed_caps:
                r.f_pdn_type = parsed_caps['pdn_type']
            if 'mimoDL' in parsed_caps:
                r.f_mimoDL = parsed_caps['mimoDL']
            if 'mimoUL' in parsed_caps:
                r.f_pdn_type = parsed_caps['mimoUL']
            if 'eutran_bands' in parsed_caps:
                r.f_bands = parsed_caps['eutran_bands']
            if 'extra_mcs' in parsed_caps:
                r.f_dl_mimo4l = [x['band'] for x in parsed_caps['extra_mcs']
                                 if x['mimoLayers'] == 4]
                r.f_dl_qam256 = [x['band'] for x in parsed_caps['extra_mcs']
                                 if x['DL-256QAM']]
                r.f_ul_qam64 = [x['band'] for x in parsed_caps['extra_mcs']
                                if x['UL-64QAM']]

            if not isinstance(r.f_caps_json, dict):
                r.f_caps_json = {}
            r.f_caps_json.update(parsed_caps)

        if DEBUG:
            from json import dumps
            logger.debug(dumps(r.f_caps_json, indent=4))
            with open('/tmp/uecap_dump.json', 'w') as f:
                f.write(dumps(r.f_caps_json, indent=2))
                f.write('\n' + 80 * '-' + '\n')
                f.write(dumps(r.f_ca_json, indent=2))
            continue

        r.f_processed = True
        r.update_record()
        db.commit()

        if gen_xlsx:
            generate_xlsx(id, created_by=created_by)

        elapsedm = time.time() - start_time
        m, s = divmod(elapsedm, 60)
        h, m = divmod(m, 60)
        logger.info(
            'UECAP rebuild for %d... %d:%02d:%02d passed' % (r.id, h, m, s))

        websocket_send(settings.ws_server, '',
                       settings.security_key, 'atms_uecap')
