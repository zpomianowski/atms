# -*- coding: utf-8 -*-
from misc import pack

GAME_ACTIONS = [
    {'id': 'steal', 'label': T("Steal cash! 3$/victim"), 'price': 3}
]

GAME_SPECIALS = [
    {'id': 'attack_dice', 'days': 7,
     'label': T("Attack dice! 500$/week"), 'price': 500},
    {'id': 'defence_dice', 'days': 7,
     'label': T("Defence dice! 100$/week"), 'price': 100},
    {'id': 'show_attacker', 'days': 7,
     'label': T("Can see who attacked you! 50$/week"), 'price': 50},
    {'id': 'trap', 'days': 7,
     'label': T("Set a trap for an attacker! 100$/week"), 'price': 100}
]


def actions():
    response.view = 'generic.' + request.extension
    return dict(content=GAME_ACTIONS)


def specials():
    response.view = 'generic.' + request.extension
    return dict(content=GAME_SPECIALS)


@auth.requires_login()
def play():
    pvars = pack(request.post_vars)
    victims = pvars.victims
    actions = pvars.actions
    specials = pvars.specials
    if (not victims or not actions) and not specials:
        raise HTTP(401, T("You did not provide all data"))
    cash_needed = 0
    actions = actions if actions else []
    specials = specials if specials else []
    victims = victims if victims else []
    for a in actions:
        for g in GAME_ACTIONS:
            if g['id'] == a:
                cash_needed += g['price']
    cash_needed *= len(victims)
    for s in specials:
        for g in GAME_SPECIALS:
            if g['id'] == s:
                cash_needed += g['price']
    user = db.atms_auth_user[auth.user_id]
    cash_available = user.coins
    if cash_available < cash_needed:
        raise HTTP(401, T("You cannot afford it, punk!"))
    user.coins -= cash_needed
    user.update_record()
    params = {
        'attacker': str(auth.user_id),
        'victims': victims,
        'actions': actions
    }
    try:
        system = db.atms_auth_user(last_name='ATMS')
        system.coins += cash_needed
        system.update_record()
    except:
        pass
    scheduler.queue_task('performGameAction',
                         task_name="hidden_performGameAction",
                         pvars=params)
    for code in specials:
        db.atms_fejms.insert(
            user_id=auth.user_id,
            code=code,
            expire_date=datetime.datetime.now() + datetime.delta(days=7))
