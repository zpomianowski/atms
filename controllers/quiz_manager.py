# -*- coding: utf-8 -*-
from gluon.contrib.simplejson import loads


@auth.requires_login()
@request.restful()
def api():
    response.view = 'generic.' + request.extension

    def GET(id):
        row = db((db.atms_quiz_question.id == id)).select(
            db.atms_quiz_question.id,
            db.atms_quiz_question.title,
            db.atms_quiz_question.category,
            db.atms_quiz_question.text,
            db.atms_quiz_question.text_html).first()

        row.options = db(
            db.atms_quiz_option.ref_quiz_question == row.id
            ).select().as_list()
        return dict(content=[row])

    def POST(**kwargs):
        data = loads(request.body.read())
        options = data['options']
        del data['options']
        if "id" in data:
            del data["id"]

        if len(options) < 2:
            raise HTTP(500, 'More options needed!')

        result = db.atms_quiz_question.validate_and_insert(**data)
        if not result.id:
            return

        for o in options:
            o['ref_quiz_question'] = result.id
            if "id" in o:
                del o["id"]
            db.atms_quiz_option.validate_and_insert(**o)
        addExp(2, 'quiz')

    def PUT(**kwargs):
        data = loads(request.body.read())
        db(db.atms_quiz_question.id == data['id']).validate_and_update(
            **{k: v for (k, v) in data.items()
               if k not in ['id', 'options', 'text_html']})

        ids2keep = [x['id'] for x in data['options']]
        db(
            (db.atms_quiz_option.ref_quiz_question == data['id']) &
            (~db.atms_quiz_option.id.belongs(ids2keep))).delete()

        if len(data['options']) < 2:
            raise HTTP(500, 'More options needed!')

        for o in data['options']:
            option_id = o['id']
            del o['id']
            if option_id:
                db(db.atms_quiz_option.id == option_id).validate_and_update(
                    ref_quiz_question=data['id'],
                    **o)
            else:
                db.atms_quiz_option.validate_and_insert(
                    ref_quiz_question=data['id'],
                    **o)

    def DELETE(ids):
        ids = [int(x) for x in ids.split('_')]
        db(db.atms_quiz_question.id.belongs(ids)).delete()
        addExp(2*len(ids), 'quiz')

    return locals()


@auth.requires_login()
def get_quizq_count():
    query = (db.atms_quiz_question.id > 0) & \
        (db.atms_quiz_question.created_by == auth.user.id)
    if request.vars:
        for k, v in request.vars.items():
            v = None if v == "None" else v
            query &= db.atms_quiz_question[k] == v

    result = db(query).count()
    return dict(content=result)


@auth.requires_login()
def get_quizq():
    response.view = 'generic.' + request.extension

    qFrom = int(request.vars['from'])
    qTo = int(request.vars['to'])
    qSort = request.vars['sortOrder']
    qCol = request.vars['sortIndex']
    project = request.vars['project']

    del request.vars['from']
    del request.vars['to']
    del request.vars['sortOrder']
    del request.vars['sortIndex']

    query = (db.atms_quiz_question.id > 0) & \
        (db.atms_quiz_question.created_by == auth.user.id)

    if request.vars:
        for k, v in request.vars.items():
            v = None if v == "None" else v
            query &= db.atms_quiz_question[k] == v

    options = dict(limitby=(qFrom, qTo))
    if qCol != 'null':
        qCol = 'category' if qCol == 'prev' else qCol
        options['orderby'] = '%s %s' % (qCol, qSort)

    rows = db(query).select(
        db.atms_quiz_question._id,
        db.atms_quiz_question.category,
        db.atms_quiz_question.title,
        **options)

    for r in rows:
        if r.category and r.title:
            r.prev = r.category + ": " + r.title[:25]
        else:
            r.prev = "???"
    return dict(content=rows)


@auth.requires_login()
def get_quiz():
    from datetime import datetime
    from datetime import timedelta

    db(db.atms_quiz_question_done.created_on <
       datetime.now() - timedelta(days=1)).delete()

    questions = db(
        (db.atms_quiz_question.id > 0) &
        ((db.atms_quiz_question_done.id == None) |
         (db.atms_quiz_question_done.created_by != auth.user.id))).select(
        db.atms_quiz_question.id,
        left=db.atms_quiz_question_done.on(
            db.atms_quiz_question.id ==
            db.atms_quiz_question_done.ref_question),
        orderby='<random>',
        limitby=(0, 5))

    return dict(content=questions)


@auth.requires_login()
def verify_quiz():
    data = loads(request.body.read())
    bonus = 0
    if sum([x['result'] for x in data]) == len(data):
        bonus = 10

    for qid in [x['question'] for x in data]:
        db.atms_quiz_question_done.validate_and_insert(
            ref_question=qid)

    addExp(len(data)+bonus, 'quiz')
