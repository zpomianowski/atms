# -*- coding: utf-8 -*-


def index():
    return dict()


def get_logs():
    response.view = 'generic.' + request.extension
    data = db(db.atms_logs.id > 0).select(
        orderby=~db.atms_logs.id,
        limitby=(0, 1000))
    return dict(content=data)
