#!/usr/bin/env python
# -*- coding: utf-8 -*-
import re


def show():
    if request.extension == 'json':
        response.view = 'generic.json'
    HLS_SERVER = 'http://atms.lab.redefine.pl:8080/hls/%s-%d.m3u8'
    stb_id = request.args[0]
    if not stb_id.isdigit():
        return response.render('stb/error.html')
    stb = db(db.atms_devices.id == stb_id).select(
        db.atms_devices.settings,
        db.atms_devices.host).first()
    url = HLS_SERVER % (stb.host, int(re.sub(
        '[a-zA-Z/]', '', stb.settings[0]['videosrc'])))
    return dict(content=url,
                title='STB[ID:%s]~connstr=%s videosrc=%s' % (
                    stb_id,
                    stb.settings[0]['connstr'],
                    stb.settings[0]['videosrc']))
