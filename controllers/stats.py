# -*- coding: utf-8 -*-
from misc import pack

levels = [
    {'level': 1,  'th': 50,     'name': 'Jesteś nikim...'},
    {'level': 2,  'th': 100,    'name': 'Majtek'},
    {'level': 3,  'th': 150,    'name': 'Marynarz'},
    {'level': 4,  'th': 200,    'name': 'Starszy marynarz'},
    {'level': 5,  'th': 300,    'name': 'Mat'},
    {'level': 6,  'th': 500,    'name': 'Starszy mat'},
    {'level': 7,  'th': 750,    'name': 'Bosmanmat'},
    {'level': 8,  'th': 1000,   'name': 'Bosman'},
    {'level': 9,  'th': 1500,   'name': 'Starszy bosman'},
    {'level': 10, 'th': 2000,   'name': 'Młodszy chorąży'},
    {'level': 11, 'th': 3000,   'name': 'Chorąży'},
    {'level': 12, 'th': 3500,   'name': 'Chorąży Torpeda'},
    {'level': 13, 'th': 4000,   'name': 'Starszy chorąży'},
    {'level': 14, 'th': 5000,   'name': 'Starszy chorąży sztabowy'},
    {'level': 15, 'th': 6000,   'name': 'Podporucznik'},
    {'level': 16, 'th': 7000,   'name': 'Porucznik'},
    {'level': 17, 'th': 8000,   'name': 'Kapitan'},
    {'level': 18, 'th': 8500,   'name': 'Kapitan Bomba'},
    {'level': 19, 'th': 9000,   'name': 'Komandor podporucznik'},
    {'level': 20, 'th': 10000,  'name': 'Komandor porucznik'},
    {'level': 21, 'th': 12000,  'name': 'Komandor'},
    {'level': 22, 'th': 14000,  'name': 'Kontradmirał'},
    {'level': 23, 'th': 16000,  'name': 'Wiceadmirał'},
    {'level': 24, 'th': 20000,  'name': 'Admirał floty'},
    {'level': 25, 'th': 30000,  'name': 'Admirał gwiezdnej floty'},
    {'level': 26, 'th': 40000,  'name': 'Admirał'},
    {'level': 27, 'th': 50000,  'name': 'Marszałek'},
    {'level': 28, 'th': 60000,  'name': 'Nadludź'}]


@auth.requires_login()
def call():
    return service()


@service.json
def user_levels(rank_type='global'):
    from datetime import date
    from datetime import timedelta

    exp_sum = db.atms_exp.exp.sum()
    award_sum = db.atms_awards.count.sum()
    toSelect = [
        db.atms_auth_user.id,
        db.atms_auth_user.first_name,
        db.atms_auth_user.last_name,
        db.atms_auth_user.identicon,
        db.atms_auth_user.color,
        db.atms_auth_user.registration_date,
        exp_sum]

    def deg(value, deg):
        new_value = int(value - deg)
        return new_value if new_value > 0 else 1

    def flatten(name, pDate):
        import math
        result = []
        query = (db.atms_exp.user == db.atms_auth_user.id) & \
            (db.atms_exp.timestamp > pDate) & \
            (db.atms_auth_user.is_active == True)

        rows = db(query).select(*toSelect,
                                groupby=db.atms_exp.user, orderby=~exp_sum)
        counter = 1
        for idx, r in enumerate(rows):
            d = {}
            d["place"] = counter
            d["color"] = r.atms_auth_user.color
            d["label"] = "%s %s" % (
                r.atms_auth_user.first_name,
                r.atms_auth_user.last_name)
            d["icon"] = r.atms_auth_user.identicon
            d["awards"] = []
            # special awards - bought ;)
            if rank_type == 'global':
                milords = db((db.atms_fejms.user_id == r.atms_auth_user.id) &
                             (db.atms_fejms.code == 'milord')).count()
                d["awards"].append(dict(name='milord', count=milords))

            awardsq = (db.atms_awards.user == r.atms_auth_user.id) & \
                (db.atms_awards.timestamp > pDate)
            if name == "week":
                awardsq &= ~(db.atms_awards.type.startswith("month"))

            awards_rows = db(awardsq).select(
                db.atms_awards.type, award_sum,
                groupby=db.atms_awards.type)

            for ar in awards_rows:
                count = ar[award_sum]
                if rank_type == 'global':
                    if ar.atms_awards.type in ["week_worst", "month_worst"]:
                        anti_dicks_nb = db(
                            (db.atms_fejms.user_id == r.atms_auth_user.id) &
                            (db.atms_fejms.code == 'anti_' +
                             ar.atms_awards.type)).count()
                        count -= anti_dicks_nb
                d["awards"].append(dict(
                    name=ar.atms_awards.type, count=count))

            # fallback if no users's registration_date
            if not r.atms_auth_user.registration_date:
                registration_date = datetime.date.today()
                try:
                    registration_date = db(
                        db.atms_exp.user == r.atms_auth_user.id).select(
                        db.atms_exp.timestamp,
                        orderby=db.atms_exp.timestamp).first()[
                            db.atms_exp.timestamp]
                except:
                    pass
                db(db.atms_auth_user.id == r.atms_auth_user.id).update(
                    registration_date=registration_date)
                db.commit()
                r.atms_auth_user.registration_date = registration_date

            bonus_m = 100
            bonus_w = 20
            penalty = 4
            bonus_exp = ((
                datetime.date.today() -
                r.atms_auth_user.registration_date).days +
                bonus_m * len([i for i in d['awards']
                               if 'month_best' in i['name']]) +
                bonus_w * len([i for i in d['awards']
                               if 'month_best' in i['name']]) -
                bonus_m * penalty * len([i for i in d['awards']
                                         if 'month_worst' in i['name']]) -
                bonus_w * penalty * len([i for i in d['awards']
                                         if 'week_worst' in i['name']]))

            badge3 = int(math.log(deg(r[exp_sum], 10000), 30))
            d["awards"].append(dict(name='badge3', count=badge3))
            badge2 = int(math.log(deg(r[exp_sum], 1000), 10))
            d["awards"].append(dict(name='badge2', count=badge2))
            badge1 = int(math.log(deg(r[exp_sum], 0), 2))
            badge1 = -11
            d["awards"].append(dict(name='badge1', count=badge1))
            d["level"] = levels[0]["level"]
            d["rank"] = levels[0]["name"]
            next_level = 0
            for i in range(len(levels)):
                if levels[i]["th"] < r[exp_sum] + bonus_exp:
                    d["level"] = levels[i]["level"]
                    next_level = levels[i + 1]["th"]
                    d["rank"] = \
                        "%(lvl)s (%(exp)s + bonus %(bonus)d)/%(next)d" % dict(
                        lvl=levels[i]["name"],
                        exp=r[exp_sum],
                        next=next_level,
                        bonus=bonus_exp)
            if name == 'global':
                d["label"] = "%s lvl. %d" % (d["label"], d["level"])
            else:
                d["rank"] = "%s: %d" % (unichr(0x03A3), r[exp_sum])

            result.append(d)
            counter += 1
        return result

    if rank_type == 'global':
        shift = 99999
    if rank_type == 'year':
        shift = 365
    if rank_type == 'half-year':
        shift = 183
    if rank_type == 'quarter':
        shift = 91
    if rank_type == 'month':
        shift = 30
    if rank_type == 'week':
        shift = 10

    bonuses = get_bonuses()['content']

    system_coins = 0
    try:
        system_coins = db.atms_auth_user(last_name='ATMS').coins
    except:
        pass
    return dict(system=dict(system_coins=system_coins),
                content=flatten(rank_type,
                                date.today() - timedelta(days=shift)),
                user=dict(coins=db.atms_auth_user[auth.user_id].coins,
                          bonuses=bonuses))


@service.json
def get_bonuses():
    from random import sample
    from random import randint
    bonuses = db((db.atms_rates.id > 0) &
                 (db.atms_rates.current_rate > settings.bonus_default)).select(
        db.atms_rates.current_rate,
        db.atms_rates.code,
        db.atms_rates.tends_up,
        cacheable=True,
        cache=(cache.disk, 60))
    bonuses = sample(bonuses, randint(1, len(bonuses)))

    def trend(value):
        tplt = '<span style="color:%s;font-weight: \
            bold;font-size:16px;">%s</span>'
        values = {
            -1: tplt % ('red', unichr(8600)),
            0: tplt % ('black', unichr(8605)),
            1: tplt % ('green', unichr(8599))}
        return values[value]

    bonuses = ", ".join(["%s basic: %.2f [$/exp]" % (trend(0),
                                                     settings.bonus_default)] +
                        ["%s&nbsp;%s:&nbsp;%.3f&nbsp;[$/exp]" % (
                            trend(x.tends_up), x.code, x.current_rate) for x
                         in bonuses])

    return dict(content=bonuses)


@service.json
def user_exp():
    pvars = pack(request.post_vars)
    users = pvars.user
    dfrom = pvars.dfrom
    dto = pvars.dto

    data = Storage()
    qusers = (
        (db.atms_auth_user.id > 0) &
        (db.atms_auth_user.is_active == True))
    if users:
        qusers &= db.atms_auth_user.id.belongs(users)

    users = db(qusers).select(
        db.atms_auth_user.id,
        db.atms_auth_user.first_name,
        db.atms_auth_user.last_name,
        db.atms_auth_user.color)

    import datetime
    dmin = datetime.datetime.strptime(dfrom[0].split(' ')[0],
                                      "%Y-%m-%d").date()
    dmax = datetime.datetime.strptime(dto[0].split(' ')[0],
                                      "%Y-%m-%d").date()

    from datetime import timedelta

    def daterange(start_date, end_date):
        for n in range(int((end_date - start_date).days + 1)):
            yield start_date + timedelta(n)

    data.x = []
    for d in daterange(dmin, dmax):
        data.x.append(str(d))

    data.layers = []
    for u in users:
        item = Storage()
        item.label = "%s %s" % (u.first_name, u.last_name)
        item.color = u.color
        rows = db(db.atms_exp.user == u.id).select(
            db.atms_exp.exp,
            db.atms_exp.timestamp).as_dict(key='timestamp')

        item.data = []
        for date in daterange(dmin, dmax):
            if date in rows:
                value = rows[date]['exp']
            else:
                value = 0
            item.data.append(dict(
                value=value,
                desc="Gówno panie!"))

        data.layers.append(item)

    return dict(content=data)


@service.json
def cases():
    pvars = pack(request.post_vars)
    users = pvars.user
    tests = pvars.test
    groupby = pvars.groupby or []
    layers = pvars.layer or []
    statuses = pvars.status
    aggr = pvars.aggr[0]

    qcases = db.atms_cases.id > 0
    if users:
        qcases &= db.atms_cases.user.belongs(users)
    if tests:
        qcases &= db.atms_cases.test.belongs(tests)
    if statuses:
        qcases &= db.atms_cases.status.contains(statuses)

    rows = db(
        (qcases) &
        (db.atms_auth_user.is_active == True) &
        (db.atms_auth_user.id == db.atms_cases.user) &
        (db.atms_tests.id == db.atms_cases.test)).select(
        db.atms_auth_user.first_name,
        db.atms_auth_user.last_name,
        db.atms_auth_user.id,
        db.atms_tests.name,
        db.atms_cases.status,
        db.atms_cases.weight,
        aggr,
        groupby=','.join(set(groupby + layers)))

    export = convert2LayeredBars(rows, groupby, layers, aggr)
    return dict(content=export)


@service.json
def rates():
    MAX_SAMPLES = 48
    NTHSELECTOR = 1
    pvars = pack(request.post_vars)
    dfrom = pvars.dfrom
    dto = pvars.dto
    layers = pvars.layer or []

    dmin = datetime.datetime.strptime(dfrom[0],
                                      "%Y-%m-%d %H:%M:%S")
    dmax = datetime.datetime.strptime(dto[0],
                                      "%Y-%m-%d %H:%M:%S")
    query = (db.atms_history.main_key == 'atms_rates') & \
        (db.atms_history.timestamp >= dmin) & \
        (db.atms_history.timestamp <= dmax) & \
        (db.atms_history.data_key.belongs(layers))

    labels = db(query).select(db.atms_history.timestamp, distinct=True)

    # convert for export
    export = Storage()
    if len(labels) > MAX_SAMPLES:
        NTHSELECTOR = len(labels) * 2 / MAX_SAMPLES
    export.x = [x.timestamp.strftime("%Y.%m.%d %H:%M")
                for ind, x in enumerate(labels) if ind % NTHSELECTOR == 0]
    layers = db(query)(db.atms_history.timestamp.belongs(
        [x.timestamp
         for ind, x in enumerate(labels) if ind % NTHSELECTOR == 0])) \
        .select(
            db.atms_history.timestamp,
            db.atms_history.data_key,
            db.atms_history.value,
            orderby=db.atms_history.data_key | db.atms_history.timestamp)

    preexport = {}
    for r in layers:
        if r.data_key not in preexport:
            preexport[r.data_key] = {}
            preexport[r.data_key]['desc'] = ''
            preexport[r.data_key]['label'] = r.data_key
            preexport[r.data_key]['data'] = []
        preexport[r.data_key]['data'].append(
            {'value': r.value['value'], 'desc': 'deta'})
    export.layers = [v for k, v in preexport.items()]
    return dict(content=export)


def convert2LayeredBars(rows, groupby, layers, aggr_func):
    # group data
    data = Storage()
    for r in rows:
        labelA = []
        for g in groupby:
            if g == 'atms_auth_user.id':
                labelA.append('%s %s' % (
                    r[db.atms_auth_user.first_name],
                    r[db.atms_auth_user.last_name]))
            elif g == 'atms_cases.weight':
                labelA.append('W:' + str(r[g]))
            else:
                labelA.append(str(r[g]))

        layerA = []
        for g in layers:
            if g == 'atms_auth_user.id':
                layerA.append('%s %s' % (
                    r[db.atms_auth_user.first_name],
                    r[db.atms_auth_user.last_name]))
            elif g == 'atms_cases.weight':
                layerA.append('W:' + str(r[g]))
            else:
                layerA.append(str(r[g]))

        xlabel = ' '.join(labelA)
        layer = ' '.join(layerA)

        if xlabel not in data:
            data[xlabel] = {}

        if layer not in data[xlabel]:
            item = Storage()
            item.label = layer
            item.desc = ''
            item.data = {}
            data[xlabel][layer] = item

        if xlabel not in data[xlabel][layer].data:
            data[xlabel][layer].data[xlabel] = r[aggr_func]
        else:
            data[xlabel][layer].data[xlabel] += r[aggr_func]

    # convert for export
    export = Storage()
    export.x = data.keys()
    lkeys = []
    for vd in data.values():
        for v in vd:
            lkeys.append(v)

    export.layers = []

    for lk in set(lkeys):
        item = Storage()
        item.label = lk
        item.desc = ''
        item.data = []
        for x in export.x:
            value = 0
            if x in data:
                if lk in data[x]:
                    v = data[x][lk].data.values()[0]
                    value += v if v else 0
            item.data.append({
                'value': value,
                'desc': ''})
        export.layers.append(item)

    return export


@auth.requires_login()
@request.restful()
def fejmapi():
    response.view = 'generic.' + request.extension
    FEJMS = {
        'fejm_milord_1d': {
            'code': 'milord', 'value': 10, 'days': 1,
            'desc': T("Milord title - 1 day for 10$")},
        'fejm_milord_7d': {
            'code': 'milord', 'value': 30, 'days': 7,
            'desc': T("Milord title - 7 days for 30$")},
        'fejm_milord_14d': {
            'code': 'milord', 'value': 50, 'days': 14,
            'desc': T("Milord title - 14 days for 50$")},
        'fejm_ladymilord_1d': {
            'code': 'ladymilord', 'value': 10, 'days': 1,
            'desc': T("ladymilord title - 1 day for 10$")},
        'fejm_ladymilord_7d': {
            'code': 'ladymilord', 'value': 30, 'days': 7,
            'desc': T("ladymilord title - 7 days for 30$")},
        'fejm_ladymilord_14d': {
            'code': 'ladymilord', 'value': 50, 'days': 14,
            'desc': T("ladymilord title - 14 days for 50$")},
        'fejm_anti_week_worst': {
            'code': 'anti_week_worst', 'value': 1, 'days': 0,
            'desc': T("Hide one small penal dick for 1$")},
        'fejm_anti_month_worst': {
            'code': 'anti_month_worst', 'value': 10, 'days': 0,
            'desc': T("Hide one big penal dick for 10$")},
        'fejm_exp_boost_2x': {
            'code': 'exp_boost_2x', 'value': 10, 'days': 1,
            'desc': T("Bonus 2x exp - 24h for 10$")},
        'fejm_exp_boost_3x': {
            'code': 'exp_boost_3x', 'value': 50, 'days': 1,
            'desc': T("Bonus 3x exp - 24h for 50$")}
        }

    def GET(*args, **vars):
        fejms = []
        for k, v in FEJMS.items():
            fejms.append({'id': k, 'label': v['desc']})
        return dict(content=fejms)

    def POST(code):
        from datetime import datetime
        from datetime import timedelta
        fejm = FEJMS.get(code, None)
        if not fejm:
            return None
        user = db.atms_auth_user[auth.user_id]
        if user.coins < fejm['value']:
            raise HTTP(401, T('Go home you little poor thing...'))
        # overriding
        if 'milord' in fejm['code']:
            db(((db.atms_fejms.code == fejm['code']) |
                (db.atms_fejms.code == 'lady' + fejm['code'])) &
               (db.atms_fejms.expire_date > datetime.now())).update(
                expire_date=datetime.now())
        db.atms_fejms.insert(
            user_id=auth.user_id,
            code=fejm['code'],
            expire_date=datetime.now() + timedelta(days=fejm['days']))
        user.coins = user.coins - fejm['value']
        try:
            system = db.atms_auth_user(last_name='ATMS')
            system.coins += fejm['value']
            system.update_record()
        except:
            pass
        user.update_record()

    return locals()
