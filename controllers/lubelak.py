from websocket_messaging import websocket_send


def call():
    return service()


@request.restful()
# @auth.requires_login()
def tapi():
    response.view = 'generic.' + request.extension

    def GET(table_name, action, **vars):
        query = (db[table_name].id > 0)
        extra = Storage()
        # advanced filtering
        if 'csfilter' in vars:
            import base64
            import gluon.contrib.simplejson as simplejson
            b64 = base64.b64decode(vars['csfilter'])
            for k, v in simplejson.loads(b64).items():
                if isinstance(v, dict):
                    if 'min' in v:
                        query &= (db[table_name][k] >= v['min'])
                    if 'max' in v:
                        query &= (db[table_name][k] <= v['max'])
                elif isinstance(v, list):
                    if 'ref' in k or k in ['created_by', 'modified_by']:
                        query &= (db[table_name][k].belongs(
                            [int(x) for x in v]))
                    else:
                        query &= (db[table_name][k].contains(
                            [x for x in v]))
                elif isinstance(v, bool):
                    query &= (db[table_name][k] == v)
                else:
                    query &= (db[table_name][k].like(
                              '%%%s%%' % v, case_sensitive=False))

        if action == 'count':
            return dict(
                content=db(query).count(),
                descriptor=getTableDescriptor(table_name))

        select_params = Storage()
        if 'orderby' in vars:
            select_params.orderby = '%s %s' % (
                str(db[table_name][vars['orderby']]),
                vars['asc'])
        else:
            select_params.orderby = ~db[table_name].id
        if 'to' in vars and 'from' in vars:
            lfrom = int(vars['from'])
            lto = int(vars['to']) + 1
            if lto - lfrom > 5000:
                raise HTTP(503, 'Requested volume of data too big!')
            select_params.limitby = (lfrom, lto)

        data2refactor = False
        refactorRules = Storage()
        toSelect = []
        lefts = {}

        exportCols = None
        if 'exportCols' in vars:
            exportCols = [str(f) for f in vars['exportCols']]
        for f in db[table_name]:
            rule = Storage()
            # handle references
            if exportCols:
                if str(f).split('.')[-1] not in exportCols:
                    continue
            if 'reference' in f.type:
                data2refactor = True
                second_table = f.type.split(" ")[1]
                alias = db[second_table].with_alias(f.name)
                lefts[f.name] = db[second_table].with_alias(f.name).on(
                    db[table_name][f.name] == alias.id)

                rule.ref = f.name
                # special handling
                if second_table == 'atms_auth_user':
                    toSelect += [alias.first_name,
                                 alias.last_name]
                    rule.format = "%(first_name)s %(last_name)s"

            # hide text fields as redundant for table widgets
            if f.type != 'text':
                toSelect.append(f)

            if rule.func is not None or rule.format is not None:
                refactorRules[f.name] = rule

        select_params.left = lefts.values()
        data = db(query).select(
            *toSelect,
            **select_params)

        # extra parameters
        data = get_extra(data, table_name, extra)

        if not data2refactor:
            return dict(content=data.as_list())

        refactoredData = []
        for r in data:
            try:
                direct = r[table_name]
            except:
                direct = r
            for key in refactorRules:
                rule = refactorRules[key]
                if 'func' in rule:
                    direct[key] = rule.func(r)
                else:
                    direct[key] = rule.format % r[rule.ref]
                    if 'None' in direct[key]:
                        direct[key] = ''
            refactoredData.append(direct)
        return dict(content=refactoredData)

    return locals()


@auth.requires_login()
@request.restful()
def api():
    response.view = 'generic.' + request.extension

    def GET(*args, **vars):
        if len(args) > 2:
            if args[2] in ['null', 'None']:
                return dict(content=[
                    {k: None for k in db[args[0]].fields
                     if k not in auth.signature and
                     db[args[0]][k].writable and
                     db[args[0]][k].readable}])
        patterns = 'auto'
        patterns = [
            '/atms_wificap[atms_wificap]',
            '/atms_wificap/id/{atms_wificap.id}',
            '/atms_wificap/id/{atms_wificap.id}/:field',
            '/cbs_device/id/{cbs_device.id}',
            '/cbs_device/id/{cbs_device.id}/:field',
            '/cbs_device[cbs_device]/id/{cbs_device.id}' +
            '/model[cbs_device_model.id]/:field',
            '/cbs_device[cbs_device]/id/{cbs_device.id}' +
            '/company[cbs_company.id]/:field']
        parser = db.parse_as_rest(patterns, args, vars)
        if parser.status == 200:
            return dict(content=parser.response)
        else:
            raise HTTP(parser.status, parser.error)

    def POST(table_name, id, **vars):
        import gluon.contrib.simplejson as simplejson
        # if not auth.has_permission('create', db[table_name], 0):
        #     raise HTTP(401, T('Unauthorized'))
        data = simplejson.loads(vars['content'])
        data['f_processed'] = False
        if 'f_category_lte' in data:
            del data['f_category_lte']
        if 'output_format' in data:
            del data['output_format']
        for k, v in data.items():
            data[k] = None if v == 'None' else v
        addExp(1 if id else 10, 'test')
        if id != 'null':
            db(db[table_name]._id == id).update(**data)
        else:
            id = db[table_name].validate_and_insert(**data).id
        queue_task_dep(
            'parseWiFiCap',
            [id],
            group_name=settings.main_group,
            immediate=True,
            timeout=10800,
            sync_output=5)

        websocket_send(settings.ws_server, '',
                       settings.security_key, table_name)
        return dict(content=id)

    def DELETE(table_name, _id):
        # if not auth.has_permission('delete', db[table_name], _id):
        #     raise HTTP(401, 'Unauthorized')
        db(db[table_name]._id == _id).delete()
        websocket_send(settings.ws_server, '',
                       settings.security_key, table_name)

    return locals()


@auth.requires_login()
@request.restful()
def txt_api():
    response.view = 'generic.' + request.extension

    def GET(field, id):
        from json import dumps
        data = db(db.atms_wificap.id == id).select(
            db.atms_wificap[field]).first()[field]
        if field == 'f_caps_json':
            return dumps(data, sort_keys=True,
                         indent=4, separators=(',', ': '))
        return data

    return locals()


@service.json
def chart_classes():
    from wificap_parser import CLASSES
    classes = CLASSES + [T('Unknown')]
    export = Storage(layers=[], x=classes)
    export.layers.append(Storage(color="#3eaed7",
                                 label=T("Number of devices of the class")))

    data = [{"value": 0, "desc": ""}] * len(classes)
    cnt = db.atms_wificap.id.count()
    for item in db(db.atms_wificap.id > 0).select(
            db.atms_wificap.f_cat,
            cnt,
            groupby=db.atms_wificap.f_cat):
        try:
            item.atms_wificap.f_cat
            index = classes.index(item.atms_wificap.f_cat)
            data[index] = {"value": item[cnt], "desc": ""}
        except AttributeError:
            data[-1]["value"] += 1
    export.layers[0].data = data
    return dict(content=export)


@service.json
def chart_rates():
    items = db(db.atms_wificap.id > 0).select(
        db.atms_wificap.f_company,
        db.atms_wificap.f_model,
        db.atms_wificap.f_maxrate24,
        db.atms_wificap.f_maxrate5,
        orderby=db.atms_wificap.f_maxrate5)

    export = Storage(layers=[], x=[
        "%s %s" % (d.f_company, d.f_model) for d in items])
    export.layers.append(Storage(label="2.4GHz", color="#75cd63", data=[]))
    export.layers.append(Storage(label="5GHz", color="#e68845", data=[]))
    l1 = export.layers[0].data
    l2 = export.layers[1].data
    for i in items:
        l1.append({"value": i.f_maxrate24, "desc": ""})
        l2.append({"value": i.f_maxrate5, "desc": ""})

    return dict(content=export)
