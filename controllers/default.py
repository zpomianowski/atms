# -*- coding: utf-8 -*-
import gluon.contrib.simplejson
import random


@cache.action(session=True, cache_model=cache.redis)
@auth.requires_login()
@service.json
def getperm(b64):
    """
    {
    "ids": "[]",
    "rname": ""
    }

    Service address
    /atms/default/call/json/getperm?b64={b64 json}
    """
    import base64
    import gluon.contrib.simplejson as simplejson
    b64 = simplejson.loads(base64.b64decode(b64))
    ids = b64['ids']
    rname = b64['rname']

    p = {
        'read': True,
        'create': True,
        'update': True,
        'del': True
    }
    # only for debuging !!!!
    return p

    if len(ids) == 0:
        p = {k: False for k in p}
        p['create'] = auth.has_permission('create', rname, 0)
        p['update'] = False

    for i in ids:
        mod = False
        if 'modified_by' in db[rname].fields:
            mod = db(
                (db[rname].id == i) &
                ((db[rname].created_by == auth.user_id) |
                 (db[rname].created_by == auth.user_id))).select(
                db[rname].created_by,
                db[rname].modified_by).first()
        perm = True if mod else False
        p['read'] &= auth.has_permission('read', rname, i) or perm
        p['create'] &= auth.has_permission('create', rname, i) or perm
        p['update'] &= auth.has_permission('update', rname, i) or perm
        p['del'] &= auth.has_permission('delete', rname, i)
    return p


def user():
    """
    user.html view controller
    """
    help_string = P(T('Move the mouse to the top-left corner.'))
    return dict(form=auth(), help_string=help_string)


def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)


def call():
    """
    exposes services. for example:
    http://..../[app]/default/call/jsonrpc
    decorate with @services.jsonrpc the functions to expose
    supports xml, json, xmlrpc, jsonrpc, amfrpc, rss, csv
    """
    return service()


def editRecord():
    """
    Edits record from a database.

    Kwargs:
        table (str): tablename with record to edit.
        id (str): record id.
        data (json): data to edit.

    Returns:
        dict. The return code::

            0 -- Failure
            1 -- Success

    For specified datastructures check in models/db_wizard.py
    """
    response.view = 'generic.' + request.extension
    data = gluon.contrib.simplejson.loads(json.dumps(request.vars))
    resp = db(db[data['table']].id == int(data['id'])).update(**data['data'])
    return dict(resp=resp)


def addRecord():
    """
    Adds record to a database.

    Kwargs:
        table (str): tablename where to add record.
        data (dict): data to add.

    Returns:
        dict. The return code::

            record id -- Success
            -1 -- Failure (HINT check tablename and fieldnames)

    For specified datastructures check in models/db_wizard.py
    """
    response.view = 'generic.' + request.extension
    data = gluon.contrib.simplejson.loads(json.dumps(request.vars))
    try:
        resp = db[data['table']].insert(**data['data'])
        db.commit()
        if data['table'] == 'atms_news':
            r = db.atms_news[resp]
            link = UL()
            if hasattr(r.link, '__iter__'):
                for l in r.link:
                    link.append(LI(A(l, _href=l)))
            else:
                link.append(LI(A(r.link, _href=r.link)))
            emails = [u.email for u in
                    db(db.atms_auth_user.is_active == True).select(
                        db.atms_auth_user.email)]

            creator = db.atms_auth_user[r.created_by]
            if creator:
                context = dict(
                    title='SAUT NEWS',
                    title_href=URL('atms', 'default', 'index',
                                scheme=True, host=True),
                    msg_title=r.title,
                    msg_href=URL('atms', 'default', 'index',
                                vars=dict(query=r.title),
                                scheme=True, host=True),
                    msg_note=r.description,
                    msg_body=link,
                    user='%s %s' % (creator.first_name, creator.last_name),
                    username='%s %s' % (creator.first_name, creator.last_name),
                    useremail=creator.email)
                message = response.render('mail_message.html', context)
                if 'prod' not in settings.config:
                    emails = 'zpomianowski@cyfrowypolsat.pl'
                mail.send(
                    creator.email,
                    '[SAUT NEWS] "%s\"' % r.title,
                    message, cc=emails)

                addExp(2, 'news', creator.id)
    except (AttributeError, SyntaxError, KeyError):
        return dict(resp=-1)
    return dict(resp=resp)


def delRecord():
    """Deletes record from a database.

    Kwargs:
        table (str): tablename with record to delete.
        id (str): record id.

    Returns:
        dict. The return code::

            1 -- Success
            -1 -- Failure (HINT check tablename and fieldnames)

    For specified datastructures check in models/db_wizard.py
    """
    response.view = 'generic.' + request.extension
    data = gluon.contrib.simplejson.loads(json.dumps(request.vars))
    try:
        resp = db(db[data["table"]].id == int(data["id"])).delete()
    except (AttributeError, SyntaxError, KeyError):
        return dict(resp=-1)
    try:
        if table == "atms_timesheet_task":
            addExp(-3, 'timesheet')
    except NameError:
        pass
    return dict(resp=resp)


@auth.requires_login()
def index():
    """
    index.html view controller
    """
    return dict()


def data():
    """
    http://..../[app]/default/data/tables
    http://..../[app]/default/data/create/[table]
    http://..../[app]/default/data/read/[table]/[id]
    http://..../[app]/default/data/update/[table]/[id]
    http://..../[app]/default/data/delete/[table]/[id]
    http://..../[app]/default/data/select/[table]
    http://..../[app]/default/data/search/[table]
    but URLs must be signed, i.e. linked with
      A('table',_href=URL('data/tables',user_signature=True))
    or with the signed load operator
      LOAD('default','data.load',args='tables',ajax=True,user_signature=True)
    """
    response.view = "generic." + request.extension
    form = crud()
    return dict(form=form)


@auth.requires_login()
def wiki():
    """
    wiki.html view controller
    """
    response.menu += auth._wiki.menu('default', 'wiki', True)
    return auth._wiki()


def setup():
    if not auth.id_group('wiki_editor'):
        auth.add_group('wiki_editor',
                       'Can create and edit all pages.')
        auth.add_group('wiki_author',
                       'Can create and edit self created pages.')
        # db.atms_nodes.insert(name=new_node, l=1, r=2)

    if not auth.id_group('atms_tester'):
        at = auth.add_group('atms_tester',  'Contributes in tests')

    if not auth.id_group('atms_manager'):
        am = auth.add_group('atms_manager', 'Manages testers and tests')

    if not auth.id_group('atms_admin'):
        ad = auth.add_group('atms_admin', 'Can manage users\' roles')

    at = auth.id_group('atms_tester')
    am = auth.id_group('atms_manager')
    ad = auth.id_group('atms_admin')

    db.atms_auth_permission.truncate()
    db.commit()

    # ## permissions for manager
    auth.add_permission(am, 'read',   db.atms_nodes)
    auth.add_permission(am, 'create', db.atms_nodes)
    auth.add_permission(am, 'update', db.atms_nodes)
    auth.add_permission(am, 'select', db.atms_nodes)
    auth.add_permission(am, 'delete', db.atms_nodes)

    auth.add_permission(am, 'read',   db.atms_tcases)
    auth.add_permission(am, 'create', db.atms_tcases)
    auth.add_permission(am, 'update', db.atms_tcases)
    auth.add_permission(am, 'select', db.atms_tcases)
    auth.add_permission(am, 'delete', db.atms_tcases)

    auth.add_permission(am, 'read',   db.atms_tests)
    auth.add_permission(am, 'create', db.atms_tests)
    auth.add_permission(am, 'update', db.atms_tests)
    auth.add_permission(am, 'select', db.atms_tests)
    auth.add_permission(am, 'delete', db.atms_tests)

    auth.add_permission(am, 'read',   db.atms_ttests)
    auth.add_permission(am, 'create', db.atms_ttests)
    auth.add_permission(am, 'update', db.atms_ttests)
    auth.add_permission(am, 'select', db.atms_ttests)
    auth.add_permission(am, 'delete', db.atms_ttests)

    auth.add_permission(am, 'read',   db.atms_cases)
    auth.add_permission(am, 'create', db.atms_cases)
    auth.add_permission(am, 'update', db.atms_cases)
    auth.add_permission(am, 'assign', db.atms_cases)
    auth.add_permission(am, 'do',     db.atms_cases)
    auth.add_permission(am, 'select', db.atms_cases)
    auth.add_permission(am, 'delete', db.atms_cases)

    auth.add_permission(am, 'read',   db.atms_attachments)
    auth.add_permission(am, 'create', db.atms_attachments)
    auth.add_permission(am, 'update', db.atms_attachments)
    auth.add_permission(am, 'select', db.atms_attachments)
    auth.add_permission(am, 'delete', db.atms_attachments)

    auth.add_permission(am, 'read',   db.atms_news)
    auth.add_permission(am, 'create', db.atms_news)
    auth.add_permission(am, 'update', db.atms_news)

    auth.add_permission(am, 'read',   db.atms_auth_user)
    auth.add_permission(am, 'assign', db.atms_auth_user)

    auth.add_permission(am, 'read',   db.atms_devices)
    auth.add_permission(am, 'create', db.atms_devices)
    auth.add_permission(am, 'update', db.atms_devices)
    auth.add_permission(am, 'delete', db.atms_devices)

    auth.add_permission(am, 'read',   db.wiki_page)

    auth.add_permission(am, 'read',   db.scheduler_task)
    auth.add_permission(am, 'create', db.scheduler_task)
    auth.add_permission(am, 'update', db.scheduler_task)
    auth.add_permission(am, 'select', db.scheduler_task)
    auth.add_permission(am, 'delete', db.scheduler_task)

    # ## permissions for testers
    auth.add_permission(at, 'read',   db.atms_nodes)
    auth.add_permission(at, 'select', db.atms_nodes)

    auth.add_permission(at, 'read',   db.atms_tcases)
    auth.add_permission(at, 'create', db.atms_tcases)
    auth.add_permission(at, 'select', db.atms_tcases)

    auth.add_permission(at, 'read',   db.atms_cases)
    auth.add_permission(at, 'do_case', db.atms_cases)
    auth.add_permission(at, 'select', db.atms_tcases)

    auth.add_permission(at, 'read',   db.atms_attachments)

    auth.add_permission(at, 'read',   db.atms_news)
    auth.add_permission(at, 'create', db.atms_news)
    auth.add_permission(at, 'update', db.atms_news)

    auth.add_permission(at, 'read',   db.atms_auth_user)

    auth.add_permission(at, 'read',   db.atms_devices)

    auth.add_permission(at, 'read',   db.wiki_page)

    # permissions for atms admin
    auth.add_permission(ad, 'delete', db.atms_auth_user)

    return 'Setup done!'


# NEWS_API
@auth.requires_login()
@request.restful()
def getNews():
    """Gets news and wiki articles from database.

    Returns:
        dict with a list of news and wiki articles dicts.

    For specified datastructures check in models/db_wizard.py
    """
    response.view = 'generic.json'

    def GET(*args):
        # newsy z atms_news
        news = db(db.atms_news).select(
            orderby=~db.atms_news.modified_on,
            limitby=(0, 10)
        ).as_list()

        # najnowsze artykuly wiki
        news += db(db.wiki_page).select(
            db.wiki_page.slug,
            db.wiki_page.title,
            db.wiki_page.modified_on,
            orderby=~db.wiki_page.modified_on,
            limitby=(0, 5)
        )

        # losowe najstarsze wiki
        wiki_old = db(db.wiki_page).select(
            db.wiki_page.slug,
            db.wiki_page.title,
            db.wiki_page.modified_on,
            orderby=db.wiki_page.modified_on,
            limitby=(0, 20)
        )
        try:
            wiki_old = random.sample(wiki_old, 5)
        except:
            pass

        news += wiki_old

        random.shuffle(news)
        return dict(news_items=news)
    return locals()


@auth.requires_login()
def search():
    """Gets news and wiki articles from database by keywords. Finds articles
    containg ALL words from keywords list.

    Kwargs:
        keywords (list): search keywords

    Returns:
        dict with a list of news and wiki articles dicts
        containing given keywords.
    """
    response.view = 'generic.' + request.extension
    data = gluon.contrib.simplejson.loads(json.dumps(request.vars))
    keys = data['keywords']
    # wyszukuje slowa kluczowe w tytule, opisie i linku newsa
    query_news = (db.atms_news.title.contains(keys, all=True))
    query_news |= (db.atms_news.description.contains(keys, all=True))
    # query_news |= (db.atms_news.link.like(keys))

    # newsy do wyswietlenia
    news = db(query_news).select(
        orderby=~db.atms_news.modified_on
    ).as_list()

    # wyszukuje slowa kluczowe w tytule i zawartosci wiki
    query_wiki = (db.wiki_page.title.contains(keys, all=True))
    query_wiki |= (db.wiki_page.body.contains(keys, all=True))

    # newsy i wiki do wyswietlenia
    news += db(query_wiki).select(
        orderby=~db.wiki_page.modified_on
    )
    return dict(news_items=news)
