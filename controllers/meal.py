# -*- coding: utf-8 -*-
def call():
    return service()


def test():
    row = db.atms_auth_user[9]
    row.coins = 1242
    row.update_record()
    db(db.atms_order.id < 0).update(
        f_final=2,
        f_date=datetime.date.today() - datetime.timedelta(days=0))
    # db.atms_order.truncate()
    # 9 - Zbych, 4 - Lubelakm 8 - Chominski, 30 - szklanko, 14 - Lipa
    # 1 - chinol, 2 - biesiadowo, 3 - kebab
    # db(db.atms_auth_user.id > 0).update(
    #     duty_last_date=datetime.date.today() - datetime.timedelta(days=100),
    #     duty_count=0)
    # row = db.atms_auth_user[9]
    # row.duty_last_date = None
    # row.duty_count = -9
    # row.duty_last_date = datetime.date.today() - datetime.timedelta(days=10)
    # row.update_record()
    # row = db.atms_auth_user[4]
    # row.duty_last_date = datetime.date.today() - datetime.timedelta(days=10)
    # row.duty_count = 10
    # row.update_record()
    # row = db.atms_auth_user[14]
    # row.duty_last_date = datetime.date.today() - datetime.timedelta(days=10)
    # row.duty_count = 0
    # row.update_record()
    # db.atms_order.insert(
    #     created_by=14, f_cost=13, f_cash=14, f_1=1, f_2=2, f_3=3, f_name='Kebab XXL')
    # db.atms_order.insert(
    #     created_by=9, f_cost=12.5, f_cash=17.5, f_1=1, f_name='W cieście')
    # db.atms_order.insert(
    #     created_by=4, f_cost=14, f_cash=11.5, f_1=1, f_2=3, f_name='Po wietnamsku')
    # db.atms_order.insert(
    #     created_by=8, f_cost=14, f_cash=10, f_1=1, f_2=2, f_name='Po syczuansku')
    # db.atms_order.insert(
    #     created_by=30, f_cost=17, f_cash=7, f_1=2, f_2=3, f_name='Mafioso')
    # db.atms_order.insert(
    #     created_by=31, f_cost=17, f_cash=2, f_1=2, f_name='Na ostrym XXL')
    # db.atms_order.insert(
    #     created_by=27, f_cost=17, f_cash=8, f_2=2, f_name='Na grubym łagądny XXL')
    # db.atms_order.insert(
    #     created_by=26, f_cost=33, f_cash=10, f_1=3, f_name='Widły w plecach')
    return dict(content='OK')


@auth.requires_login()
def duty():
    duty_canteen = db.atms_auth_user[auth.user_id].duty_canteen
    return dict(content=duty_canteen is not None)


@auth.requires_login()
@request.restful()
def uapi():
    response.view = 'generic.' + request.extension

    def GET():
        excluded = [r.created_by for r in
            db(db.atms_order.f_date == datetime.date.today())(
                db.atms_order.f_final != None).select(
                db.atms_order.created_by)]
        rows = db(db.atms_auth_user.is_active == True)(
            ~db.atms_auth_user.id.belongs(excluded)).select(
            db.atms_auth_user.id,
            db.atms_auth_user.first_name,
            db.atms_auth_user.last_name)
        return dict(content=[
            dict(id=r.id, label='%(first_name)s %(last_name)s' % r)
            for r in rows])

    def PUT(**vars):
        canteen = db.atms_auth_user[auth.user.id].duty_canteen
        db.atms_order.update_or_insert(
            ((db.atms_order.f_final == canteen) &
             (db.atms_order.f_date == datetime.date.today()) &
             (db.atms_order.created_by == vars['id'])),
            f_final=db.atms_auth_user[auth.user.id].duty_canteen,
            f_date=datetime.date.today(),
            created_by=vars['id'])
        return dict()

    def DELETE(**vars):
        db(db.atms_order.id == vars['id']).delete()
        return dict()

    return locals()


@auth.requires_login()
@request.restful()
def api():
    response.view = 'generic.' + request.extension

    def GET(*args, **vars):
        create_new = False
        if args[2] == 'null':
            create_new = True
            args = args[:-2] + (
                'f_date', str(datetime.date.today()),
                'created_by', str(auth.user.id))
        if _is_before_stage(STAGE2_HOUR):
            db.atms_order.f_final.readable = False
        else:
            db.atms_order.f_not_interested.readable = False
            db.atms_order.f_1.readable = False
            db.atms_order.f_2.readable = False
            db.atms_order.f_3.readable = False

        patterns = [
            '/atms_settlement/id/{atms_settlement.id}',
            '/atms_settlement/f_date/{atms_settlement.f_date}' +
            '/created_by/{atms_settlement.created_by}',
            '/atms_canteen/id/{atms_canteen.id}',
            '/atms_order/id/{atms_order.id}',
            '/atms_order/f_date/{atms_order.f_date}' +
            '/created_by/{atms_order.created_by}']
        parser = db.parse_as_rest(patterns, args, vars)
        if parser.status == 200:
            if len(parser.response) == 0 and create_new:
                new_id = db[args[0]].insert()
                parser.response = db(db[args[0]].id == new_id).select(
                    *[field for field in db[args[0]] if field.readable])
                addExp(1, 'meal')
            return dict(content=parser.response)
        else:
            raise HTTP(parser.status, parser.error)

    def PUT(table_name, **vars):
        import gluon.contrib.simplejson as simplejson
        data = simplejson.loads(vars['content'])[0]
        if table_name == 'atms_order' and 'f_1' in data:
            # anti cheat action -f_* cannot be the same
            from collections import defaultdict
            keys = ['f_1', 'f_2', 'f_3']
            D = defaultdict(list)
            for i, item in enumerate(keys):
                D[data[item]].append(i)
            for v in [v[1:] for v in D.values() if len(v) > 1]:
                for i in v:
                    data[keys[i]] = None
        for d in data:
            data[d] = None if data[d] == 'None' else data[d]
        fff = len([k for k, v in data.items()
                   if k in ['f_1', 'f_2', 'f_3'] and v is not None])
        addExp(fff, 'meal')
        updated = db(db[table_name]._id == vars["id"]).validate_and_update(
            **data)
        if len(updated.errors.keys()) > 0:
            raise HTTP(500, 'Data not valid!')

        if table_name == 'atms_settlement':
            _settle(vars["id"])

        websocket_send(settings.ws_server, '',
                       settings.security_key, table_name)
        return dict()

    def POST(table_name, **vars):
        import gluon.contrib.simplejson as simplejson
        data = simplejson.loads(vars['content'])[0]
        if 'id' in data:
            del data['id']
        db[table_name].validate_and_insert(**data)

        websocket_send(settings.ws_server, '',
                       settings.security_key, table_name)

    def DELETE(table_name, **vars):
        if table_name == 'atms_canteen':
            db(
                (db.atms_order.f_1 == vars['id']) |
                (db.atms_order.f_2 == vars['id']) |
                (db.atms_order.f_3 == vars['id']) |
                (db.atms_order.f_final == vars['id'])).delete()
            db.commit()
        db(db[table_name]._id == vars['id']).delete()
        websocket_send(settings.ws_server, '',
                       settings.security_key, table_name)
        return dict()
    return locals()


@auth.requires_login()
@request.restful()
def tapi():
    response.view = 'generic.' + request.extension

    def GET(table_name, action, **vars):
        query = (db[table_name].id > 0)
        if table_name == 'atms_order':
            query &= (db.atms_order.f_final ==
                      db.atms_auth_user[auth.user_id].duty_canteen)
            query &= (db.atms_order.f_date == datetime.date.today())

        # advanced filtering
        if 'csfilter' in vars:
            import base64
            import gluon.contrib.simplejson as simplejson
            b64 = base64.b64decode(vars['csfilter'])
            for k, v in simplejson.loads(b64).items():
                if isinstance(v, dict):
                    if 'min' in v:
                        query &= (db[table_name][k] >= v['min'])
                    if 'max' in v:
                        query &= (db[table_name][k] <= v['max'])
                elif isinstance(v, list):
                    if 'ref' in k or k in ['created_by', 'updated_by']:
                        query &= (db[table_name][k].belongs(
                            [int(x) for x in v]))
                    else:
                        query &= (db[table_name][k].contains(
                            [x for x in v]))
                elif isinstance(v, bool):
                    query &= (db[table_name][k] == v)
                else:
                    query &= (db[table_name][k].like(
                              '%%%s%%' % v, case_sensitive=False))

        if action == 'count':
            return dict(
                content=db(query).count(),
                descriptor=getTableDescriptor(table_name))

        select_params = Storage()
        if 'orderby' in vars:
            select_params.orderby = '%s %s' % (
                str(db[table_name][vars['orderby']]),
                vars['asc'])
        if 'to' in vars and 'from' in vars:
            lfrom = int(vars['from'])
            lto = int(vars['to']) + 1
            if lto - lfrom > 5000:
                raise HTTP(503, 'Requested volume of data too big!')
            select_params.limitby = (lfrom, lto)

        data2refactor = False
        refactorRules = Storage()
        toSelect = []
        lefts = {}

        for f in db[table_name]:
            rule = Storage()
            # handle references
            if 'list:reference' in f.type:
                continue
            if 'reference' in f.type:
                data2refactor = True
                second_table = f.type.split(" ")[1]
                alias = db[second_table].with_alias(f.name)
                lefts[f.name] = db[second_table].with_alias(f.name).on(
                    db[table_name][f.name] == alias.id)
                rule.ref = f.name

                # special handling
                if second_table == 'atms_auth_user':
                    toSelect += [alias.first_name,
                                 alias.last_name]
                    rule.format = "%(first_name)s %(last_name)s"
                if second_table == 'atms_canteen':
                    toSelect += [alias.f_name]
                    rule.format = "%(f_name)s"

            toSelect.append(f)

            if rule.func is not None or rule.format is not None:
                refactorRules[f.name] = rule

        select_params.left = lefts.values()
        data = db(query).select(
            *toSelect,
            **select_params)

        if not data2refactor:
            return dict(content=data.as_list())

        refactoredData = []
        for r in data:
            direct = r[table_name]
            for key in refactorRules:
                rule = refactorRules[key]
                if 'func' in rule:
                    direct[key] = rule.func(r)
                else:
                    direct[key] = rule.format % r[rule.ref]
                    if 'None' in direct[key]:
                        direct[key] = ''
            refactoredData.append(direct)
        return dict(content=refactoredData)

    return locals()


@auth.requires_login()
@service.json
def store(t_name, f_name, query=None, extra=None):
    if extra:
        if not isinstance(extra, list):
            extra = [extra]
        else:
            extra = extra
    else:
        extra = []

    # cast args -> ID, belongs func requires it
    if db[t_name][f_name].type in ['id', 'integer']:
        extra = [int(x) for x in extra]

    if db[t_name][f_name].requires.__class__.__name__ == 'IS_IN_SET':
        options = db[t_name][f_name].requires.options()
        if len(options) != 0:
            data = []
            for o in db[t_name][f_name].requires.options():
                data.append(dict(
                    value=True if o[0] in extra else False,
                    id=o[0],
                    label=o[1]))
            return data

    tablemap = dict(
        f_1={'tn': 'atms_canteen',
             'id_path': 'id', 'format': '%(f_name)s',
             'extraToSelect': [db.atms_canteen.f_name]})

    if f_name not in tablemap:
        raise HTTP(503, "This store is not supported")

    table_name = tablemap[f_name]['tn']
    format = tablemap[f_name]['format']
    id_path = tablemap[f_name]['id_path']

    q = (db[table_name].id > 0)
    if 'joins' in tablemap[f_name]:
        for j in tablemap[f_name]['joins']:
            q &= j
    if query != 'null':
        qq = None
        for f in tablemap[f_name]['extraToSelect']:
            x = str(f).split('.')
            qqq = (db[x[0]][x[1]].like(
                '%%%s%%' % query, case_sensitive=False))
            if not qq:
                qq = qqq
            else:
                qq |= qqq
        q &= qq

    rows = db(q).select(
        db[table_name][id_path],
        *tablemap[f_name]['extraToSelect'],
        limitby=(0, 2000)).as_list()

    if len(extra) > 0:
        if extra[0] != '_':
            rows += db(q)(db[table_name][id_path].belongs(extra)).select(
                db[table_name][id_path],
                *tablemap[f_name]['extraToSelect']).as_list()

    data = []
    for r in rows:
        data.append(flatten(r))
    rows = [dict(y) for y in set(tuple(x.items()) for x in data)]
    if 'joins' in tablemap[f_name]:
        id_path = "%s_%s" % (
            tablemap[f_name]['tn'], tablemap[f_name]['id_path'])
    data = []
    for r in rows:
        preselect = True if str(r[id_path]) in extra else False
        label = format % r
        data.append(dict(
            value=preselect,
            id=r[id_path],
            label=label))
    return data
