# -*- coding: utf-8 -*-
from websocket_messaging import websocket_send
from gluon.contrib import simplejson
from datetime import timedelta
from datetime import datetime

MAX_HOURS = 4


@auth.requires_login()
def html_render():
    v = request.post_vars.content
    return dict(content=auth._wiki.markmin_base(v, True, True))


@auth.requires_login()
@request.restful()
def api():
    def GET(channel, members, reverse, last_id):
        if members == "_":
            members = []
        else:
            members = [long(x) for x in members.split("_")] + [auth.user.id]
            members = [x for x in set(members)]
        reverse = bool(int(reverse))
        if not reverse:
            query = (db.atms_comments._id > last_id)
        else:
            query = (db.atms_comments._id < last_id)
        query &= (db.atms_comments.channel == channel)
        if len(members) > 1:
            query &= (
                db.atms_comments.members.contains(members, all=True))
            # warning - next query tyical for mysql,
            # may not work with other dbs
            query &= (
                db.atms_comments.members.regexp(
                    "^(\|[[:digit:]]+){%d}\|$" % len(members)))
        rows = db(query)(
            db.atms_comments.created_by == db.atms_auth_user.id).select(
            db.atms_comments.id,
            db.atms_comments.msg_html,
            db.atms_auth_user.identicon,
            db.atms_auth_user.first_name,
            db.atms_auth_user.last_name,
            db.atms_comments.members,
            db.atms_comments.read_by,
            db.atms_comments.created_on,
            db.atms_comments.created_by,
            orderby=~db.atms_comments.created_on,
            limitby=(0, 8))
        data = []

        for r in rows:
            print r
            if r.atms_comments.read_by is None:
                read_by = []
            else:
                read_by = r.atms_comments.read_by
            data.append({
                'id': r.atms_comments.id,
                'icon': URL('atms/default', 'download',
                            r.atms_auth_user.identicon),
                'user': "%s %s" % (r.atms_auth_user.first_name,
                                   r.atms_auth_user.last_name),
                'msg': r.atms_comments.msg_html,
                'timestamp': r.atms_comments.created_on,
                'deletable': (
                    r.atms_comments.created_on > datetime.now() -
                    timedelta(hours=MAX_HOURS)
                    and (r.atms_comments.created_by == auth.user.id)),
                'read': auth.user.id in read_by
            })
            db(db.atms_comments.id == r.atms_comments.id).update(
                read_by=[x for x in set(read_by + [auth.user.id])])
        return dict(content=data, reverse=reverse)

    def POST(**vars):
        if vars['members'] == "_":
            vars['members'] = []
        elif len(vars['members']) > 0:
            vars['members'] = [x for x in set(vars['members'] +
                               [auth.user.id])]
        vars['read_by'] = [auth.user.id]
        post_id = db[db.atms_comments].insert(**vars)
        addExp(1, 'chat')
        websocket_send(settings.ws_server, '',
                       settings.security_key, vars['channel'])
        additional_members = None
        if vars['channel'].startswith('atms_cases'):
            nb = vars['channel'].split('_')[2]
            row = db(db.atms_cases.id == long(nb)).select(
                db.atms_cases.created_by,
                db.atms_cases.user).first()
            additional_members = [row.user, row.created_by]
        group = db(db.atms_auth_group.role == vars['channel']).select(
            db.atms_auth_group.id).first()
        if group:
            additional_members = [x.user_id for x in db(
                db.atms_auth_membership.group_id == group.id).select(
                db.atms_auth_membership.user_id)]
        if additional_members:
            db(db.atms_comments.id == post_id).update(
                members=[x for x in set(additional_members)])
        db.commit()
        notify()

    def PUT(comment_id, **vars):
        db(db.atms_comments._id == comment_id).validate_and_update(**vars)
        websocket_send(settings.ws_server, '',
                       settings.security_key, vars['channel'])

    def DELETE(comment_id):
        comment = db.atms_comments[comment_id]
        if comment.created_on > datetime.now() - timedelta(hours=MAX_HOURS):
            db(db.atms_comments._id == comment_id).delete()
            addExp(-2, 'chat')
    return locals()


@auth.requires_login()
def notify():
    response.view = 'generic.' + request.extension
    data = {}
    users = db(db.atms_auth_user.id > 0).select(
        db.atms_auth_user.id,
        db.atms_auth_user.first_name)

    captions = {
        'atms_tester': str(T('All registered testers')),
        'atms_manager': str(T('All registered managers')),
        'wiki_editor': str(T('All registered Wiki editors'))
    }

    def bonusChannels(data, user_id):
        rows = db(db.atms_auth_group.id > 0).select(
            db.atms_auth_group.role)
        gs = [x.role for x in rows]
        for group in gs:
            if auth.has_membership(None, user_id, group):
                query = (db.atms_comments.channel == group)
                query &= ((~db.atms_comments.read_by.contains(user_id)) |
                          (db.atms_comments.read_by == None))
                count = db(query).count()
                if captions.get(group, None):
                    user_data.append({
                        'channel': group,
                        'members': [],
                        'count': count,
                        'caption': captions.get(group, None)
                    })

    def monitoredChannels(data, user_id):
        rows = db(
            db.atms_comments.members.contains(user_id) &
            db.atms_comments.read_by.contains(user_id) &
            (db.atms_comments.created_on > datetime.now() -
             timedelta(days=70))).select(
            db.atms_comments.channel,
            db.atms_comments.members,
            groupby=db.atms_comments.channel | db.atms_comments.members,
            cache=(cache.disk, 84000))
        for r in rows:
            caption = ", ".join([x.first_name for x in db(
                db.atms_auth_user.id.belongs(
                    r.members)).select(
                db.atms_auth_user.first_name,
                cache=(cache.disk, 84000),
                cacheable=True)])
            data.append({
                'channel': r.channel,
                'members': r.members,
                'count': 0,
                'caption': caption
            })

    for u in users:
        query = db.atms_comments.members.contains(u.id)
        query &= ((~db.atms_comments.read_by.contains(u.id)) |
                  (db.atms_comments.read_by == None))
        count = db.atms_comments.id.count()
        rows = db(query).select(
            count,
            db.atms_comments.channel,
            db.atms_comments.members,
            groupby=db.atms_comments.channel | db.atms_comments.members)
        user_data = []
        bonusChannels(user_data, u.id)
        for r in rows:
            caption = ", ".join([x.first_name for x in db(
                db.atms_auth_user.id.belongs(
                    r.atms_comments.members)).select(
                db.atms_auth_user.first_name,
                cache=(cache.disk, 84000),
                cacheable=True)])
            user_data.append({
                'channel': r.atms_comments.channel,
                'members': r.atms_comments.members,
                'count': r[count],
                'caption': caption
            })
        # monitoredChannels(user_data, u.id)
        data[u.id] = user_data

    websocket_send(settings.ws_server, simplejson.dumps(data),
                   settings.security_key, 'broadcast')
    return dict(data=data)
