from websocket_messaging import websocket_send


def call():
    return service()


@request.restful()
# @auth.requires_login()
def tapi():
    response.view = 'generic.' + request.extension

    def GET(table_name, action, **vars):
        query = (db[table_name].id > 0)
        extra = Storage()
        # advanced filtering
        if 'csfilter' in vars:
            import base64
            import gluon.contrib.simplejson as simplejson
            b64 = base64.b64decode(vars['csfilter'])
            for k, v in simplejson.loads(b64).items():
                if isinstance(v, basestring):
                    try:
                        query_data = int(v)
                        v = [query_data]
                    except:
                        query_data = [d.strip() for d in v.split(',')
                                      if d.strip() != '']
                        if len(query_data) > 1:
                            v = query_data
                if isinstance(v, dict):
                    if 'min' in v:
                        query &= (db[table_name][k] >= v['min'])
                    if 'max' in v:
                        query &= (db[table_name][k] <= v['max'])
                elif isinstance(v, list):
                    if 'ref' in k or k in ['created_by', 'modified_by']:
                        query &= (db[table_name][k].belongs(
                            [int(x) for x in v]))
                    else:
                        if 'list:' in db[table_name][k].type:
                            for x in v:
                                query &= (db[table_name][k].contains(x))
                        else:
                            query &= (db[table_name][k].contains(
                                [x for x in v]))
                elif isinstance(v, bool):
                    query &= (db[table_name][k] == v)
                else:
                    query &= (db[table_name][k].like(
                              '%%%s%%' % v, case_sensitive=False))
                # special support for CA (only for this field!)
                if k == 'f_ca_search_index':
                    from itertools import permutations
                    caquery = ','.join([str(i) for i in v])
                    subqs = [s.strip().replace(' ', '').replace('\-', ',')
                             for s in caquery.split(';')]
                    dbq = db.atms_uecap.id > 0
                    dbq_idx = 0
                    for sub in subqs:
                        openq = sub.find('+')
                        bands = []
                        if openq > 0:
                            sub = sub.replace('+', '')
                            bands += permutations(sub.split(',') + ['#*'])
                        bands.append(
                            [str(x) for x in sorted(
                                [int(c) for c in sub.split(',')])])
                        for b in bands:
                            b = '-'.join(b)
                            exp = db.atms_uecap.f_ca_search_index.regexp(
                                '\|%s\|' % b)
                            if dbq_idx == 0:
                                dbq = exp
                            else:
                                dbq |= exp
                            dbq_idx += 1
                    query &= dbq
                    continue

        if action == 'count':
            return dict(
                content=db(query).count(),
                descriptor=getTableDescriptor(table_name))

        select_params = Storage()
        if 'orderby' in vars:
            select_params.orderby = '%s %s' % (
                str(db[table_name][vars['orderby']]),
                vars['asc'])
        else:
            select_params.orderby = ~db[table_name].id
        if 'to' in vars and 'from' in vars:
            lfrom = int(vars['from'])
            lto = int(vars['to']) + 1
            if lto - lfrom > 5000:
                raise HTTP(503, 'Requested volume of data too big!')
            select_params.limitby = (lfrom, lto)

        data2refactor = False
        refactorRules = Storage()
        toSelect = []
        lefts = {}

        exportCols = None
        if 'exportCols' in vars:
            exportCols = [str(f) for f in vars['exportCols']]
        for f in db[table_name]:
            rule = Storage()
            # handle references
            if exportCols:
                if str(f).split('.')[-1] not in exportCols:
                    continue
            if 'reference' in f.type:
                data2refactor = True
                second_table = f.type.split(" ")[1]
                alias = db[second_table].with_alias(f.name)
                lefts[f.name] = db[second_table].with_alias(f.name).on(
                    db[table_name][f.name] == alias.id)

                rule.ref = f.name
                # special handling
                if second_table == 'atms_auth_user':
                    toSelect += [alias.first_name,
                                 alias.last_name]
                    rule.format = "%(first_name)s %(last_name)s"

            # hide text fields as redundant for table widgets
            if f.type != 'text':
                toSelect.append(f)

            if rule.func is not None or rule.format is not None:
                refactorRules[f.name] = rule

        select_params.left = lefts.values()
        data = db(query).select(
            *toSelect,
            **select_params)

        # extra parameters
        data = get_extra(data, table_name, extra)

        if not data2refactor:
            return dict(content=data.as_list())

        refactoredData = []
        for r in data:
            try:
                direct = r[table_name]
            except:
                direct = r
            for key in refactorRules:
                rule = refactorRules[key]
                if 'func' in rule:
                    direct[key] = rule.func(r)
                else:
                    direct[key] = rule.format % r[rule.ref]
                    if 'None' in direct[key]:
                        direct[key] = ''
            refactoredData.append(direct)
        return dict(content=refactoredData)

    return locals()


@auth.requires_login()
@request.restful()
def api():
    response.view = 'generic.' + request.extension

    def GET(*args, **vars):
        if len(args) > 2:
            if args[2] in ['null', 'None']:
                return dict(content=[
                    {k: None for k in db[args[0]].fields
                     if k not in auth.signature and
                     db[args[0]][k].writable and
                     db[args[0]][k].readable}])
        patterns = 'auto'
        patterns = [
            '/atms_uecap[atms_uecap]',
            '/atms_uecap/id/{atms_uecap.id}',
            '/atms_uecap/id/{atms_uecap.id}/:field',
            '/cbs_device/id/{cbs_device.id}',
            '/cbs_device/id/{cbs_device.id}/:field',
            '/cbs_device[cbs_device]/id/{cbs_device.id}' +
            '/model[cbs_device_model.id]/:field',
            '/cbs_device[cbs_device]/id/{cbs_device.id}' +
            '/company[cbs_company.id]/:field']
        parser = db.parse_as_rest(patterns, args, vars)
        if parser.status == 200:
            return dict(content=parser.response)
        else:
            raise HTTP(parser.status, parser.error)

    def POST(table_name, id, **vars):
        import gluon.contrib.simplejson as simplejson
        # if not auth.has_permission('create', db[table_name], 0):
        #     raise HTTP(401, T('Unauthorized'))
        data = simplejson.loads(vars['content'])
        data['f_processed'] = False
        if 'f_category_lte' in data:
            del data['f_category_lte']
        if 'output_format' in data:
            del data['output_format']
        if 'f_uecap_size' in data:
            del data['f_uecap_size']
        for k, v in data.items():
            data[k] = None if v == 'None' else v
        addExp(1 if id else 10, 'test')
        if id != 'null':
            db(db[table_name]._id == id).update(**data)
        else:
            id = db[table_name].validate_and_insert(**data).id
        queue_task_dep(
            'parseUECap',
            [id],
            dict(created_by=auth.user.id),
            group_name=settings.main_group,
            immediate=True,
            timeout=10800,
            sync_output=5)

        websocket_send(settings.ws_server, '',
                       settings.security_key, table_name)

    def DELETE(table_name, _id):
        # if not auth.has_permission('delete', db[table_name], _id):
        #     raise HTTP(401, 'Unauthorized')
        db(db[table_name]._id == _id).delete()
        websocket_send(settings.ws_server, '',
                       settings.security_key, table_name)

    return locals()


def caps(id):
    caps = [
        'release',
        'voice_usage',
        'voice_domain',
        'rohc_profiles',
        'lte_cats',
        'lte_cats_dl',
        'lte_cats_ul',
        'eutran_bands',
        'utran_bands_fdd',
        'utran_bands_tdd',
        'geran_bands',
        'hsdpa_cats',
        'hsupa_cats',
        'pdn_type'
        ]
    lang = {
        'release': T('3GPP release'),
        'rohc_profiles': T('ROHC profiles'),
        'voice_usage': T('Voice usage'),
        'voice_domain': T('Voice domain'),
        'lte_cats': T('LTE categories'),
        'lte_cats_dl': T('LTE category DL'),
        'lte_cats_ul': T('LTE category UL'),
        'eutran_bands': T('EUTRAN bands'),
        'utran_bands_fdd': T('UTRAN bands FDD'),
        'utran_bands_tdd': T('UTRAN bands TDD'),
        'geran_bands': T('GERAN bands'),
        'hsdpa_cats': T('HSDPA categories'),
        'hsupa_cats': T('HSUPA categories'),
        'pdn_type': T('PDN type')
        }

    rows = db(db.atms_uecap.id == id).select(db.atms_uecap.f_caps_json)
    table = TABLE(
        _class='table-hover table table-striped table-bordered',
        _style="overflow-y: auto;")
    data = rows[0].f_caps_json
    tdstl = 'text-align:center; vertical-align:middle;'
    table.append(TR(
        TH(T('Basic info'), _colspan=2, _style=tdstl), _class='success'))
    if len(caps) == 0:
        return P(T('No CAPS data.'))
    for c in caps:
        try:
            value = (', '.join([str(x) for x in data[c]])
                     if isinstance(data[c], list) else data[c])
            table.append(TR(TD(B(lang[c]), TD(value, _style=tdstl))))
        except Exception:
            pass
    return table


def band_extras(id, bands2filter=None):
    do_filter = bool(len(bands2filter))
    rows = db(db.atms_uecap.id == id).select(db.atms_uecap.f_caps_json)
    data = rows[0].f_caps_json.get('extra_mcs', [])

    if do_filter:
        data = filter(lambda d: d['band'] in bands2filter, data)

    if len(data) == 0:
        return P(T('No additional band data.'))

    lang = [
        dict(id='band', tr=T('Band')),
        dict(id='DL-256QAM', tr='DL-256QAM'),
        dict(id='UL-64QAM', tr='UL-64QAM'),
        dict(id='mimoLayers', tr=T('MIMO layers'))
        ]

    table = TABLE(
        _class='table-hover table table-striped table-bordered',
        _style="overflow-y: auto;")
    tdstl = 'text-align:center; vertical-align:middle;'
    if do_filter:
        tdstl += 'color: #be0000; font-weight: bold;'
    thstl = 'text-align:center; vertical-align:middle; color: white;' + \
        'background-color: #1db4c8'
    # table name
    table.append(TR(
        TH(T('Additional band info'),
           _colspan=len(lang), _style=thstl), _class='success'))

    first_tr = TR()
    for l in lang:
        first_tr.append(TH(l['tr'], _style=thstl))
    table.append(first_tr)

    base_url = 'static/js/atms/source/resource/atms'
    for d in data:
        row = TR(_style=tdstl)
        for l in lang:
            value = d[l['id']]
            if value == True and isinstance(value, bool):
                value = SPAN(IMG(_src=URL(base_url, 'thumbs-o-up.png')))
            if value == False and isinstance(value, bool):
                value = SPAN(IMG(_src=URL(base_url, 'thumbs-o-down.png')))
            row.append(TD(value, _style=tdstl))
        table.append(row)

    return table


def ca(id, bands2filter=None, filterType=False):
    do_filter = bool(len(bands2filter))
    lang_inter = {
        'interband': T('INTERBAND'),
        'intraband': T('INTRABAND Non contiguous carriers'),
        'intrabandc': T('INTRABAND Contiguous carriers')
        }

    data = db(db.atms_uecap.id == id).select(
        db.atms_uecap.f_ca_json)[0].f_ca_json
    t = TABLE(
        _class='table-hover table table-striped '
        'table-bordered table-condensed',
        _style="overflow-y: auto;")
    tdstl = 'text-align:center; vertical-align:middle;'
    if do_filter:
        tdstl += 'color: #be0000; font-weight: bold;'
    tthstl = 'text-align:center; vertical-align:middle; color: white;'

    group = None
    if not data:
        return P(T('No CA data found.'))
    for d in data:
        if do_filter:
            item_bands = [int(i['bandEUTRA']) for i in d['items']]
            cross = filter(lambda x: x in bands2filter, item_bands)
            if filterType == 1:
                if len(set(cross)) != len(bands2filter):
                    continue
            elif filterType == 2:
                if len(set(item_bands) - set(bands2filter)) != 0:
                    continue
            else:
                if len(cross) == 0:
                    continue

        rowSpan = len(d['items'])
        inter = d['group'] == 'interband'
        moreSpan = 0 if inter else 1

        if group != d['group']:
            # build headers
            if d['group'] == 'intraband':
                bC = "#DC572E"
            elif d['group'] == 'interband':
                bC = "#BF1E4B"
            elif d['group'] == 'intrabandc':
                bC = "#00A600"
            thstl = tthstl + 'background-color: %s;' % bC

            t.append(TR(TH(lang_inter[d['group']], _colspan=5, _style=thstl)))
            t.append(TR(
                TH(T('Configuration'), _colspan=3, _style=thstl),
                TH(T('BCSs'), _colspan=1, _rowspan=2, _style=thstl),
                TH(T('MIMO DL layers'), _colspan=1, _rowspan=2, _style=thstl)))
            tr = TR(
                TH(T('Bands'), _colspan=1, _style=thstl),
                TH(T('Classes'), _style=thstl, _colspan=1 + moreSpan))
            if inter:
                tr.append(TH(T('Uplink'), _colspan=1, _style=thstl))
            t.append(tr)
        group = d['group']

        uplink_td_flag = False
        bcs_td_flag = False

        for i in d['items']:
            desc = []
            if 'dl_cls' in i:
                desc.append('DL_' + i['dl_cls'])
            if 'ul_cls' in i and not inter:
                desc.append('UL_' + i['ul_cls'])
            tr = TR(
                TD(i['bandEUTRA'], _style=tdstl),
                TD(', '.join(desc), _style=tdstl, _colspan=1 + moreSpan))
            if inter and not uplink_td_flag:
                tr.append(TD(', '.join(d['uplink']),
                          _style=tdstl, _rowspan=rowSpan))
                uplink_td_flag = True
            if not bcs_td_flag:
                tr.append(TD(', '.join([str(x) for x in d['bcs']]),
                          _style=tdstl, _rowspan=rowSpan))
                bcs_td_flag = True
            tr.append(TD(i.get('mimoLayersDL', 'n/a'),
                      _style=tdstl))
            t.append(tr)
    return t


def summary():
    response.view = 'generic.load'
    id = request.args[0]
    bands2filter = [int(b) for b in request.args[1:]]

    filterType = None
    caption = ''
    if len(bands2filter) > 0:
        caption = DIV(
            B(T('Warning!')), ' ',
            T('Band filters are active.'),
            BUTTON(u'\u274C', **{'_type': 'button', '_class': 'close',
                                 '_data-dismiss': 'alert'}),
            _class='alert')
        filterType = int(request.vars['filterType'])
    t1 = caps(id)
    t2 = band_extras(id, bands2filter=bands2filter)
    t3 = ca(id, bands2filter=bands2filter, filterType=filterType)

    return dict(content=TAG[''](caption, t1, t2, t3))


@auth.requires_login()
@request.restful()
def txt_api():
    response.view = 'generic.' + request.extension

    def GET(field, id):
        data = db(db.atms_uecap.id == id).select(
            db.atms_uecap[field]).first()[field]
        return data

    return locals()


@service.json
def chart_max_class():
    items = db(db.atms_uecap.id > 0).select(
        db.atms_uecap.f_company,
        db.atms_uecap.f_category_lte)

    companies = [d.f_company.upper() for d in db(db.atms_uecap.id > 0).select(
        db.atms_uecap.f_company,
        distinct=True)]

    export = Storage(layers=[], x=companies)
    export.layers.append(Storage(
        color="#3eaed7",
        label=T("Vendor's max category")))

    data = [{"value": 0, "desc": ""}] * len(companies)
    maxs = Storage()
    for item in items:
        try:
            tmp = maxs.get(item.f_company.upper(), [0])
            tmp += item.f_category_lte
            maxs[item.f_company.upper()] = [max(tmp)]
            data[companies.index(item.f_company.upper())] = {
                "value": maxs[item.f_company.upper()][0], "desc": ""}
        except:
            pass

    export.layers[0].data = data
    return dict(content=export)
