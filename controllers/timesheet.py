﻿import json
import gluon.contrib.simplejson
import datetime
from datetime import date, timedelta
from time import gmtime, strftime
import time
import csv


@auth.requires_login()
def index():
    """
    index.html view controller
    """
    return dict()


@auth.requires_login()
def calendar():
    """
    calendar.html view controller
    """
    return dict()


@auth.requires_login()
def statistics():
    """
    statistics.html view controller
    """
    return dict()


@auth.requires_login()
def summation():
    """
    summation.html view controller
    """
    return dict()


@auth.requires_login()
def getUserInfo():
    '''
    Returs info about currently logged in user.
    '''
    response.view = 'generic.' + request.extension
    data = False
    if auth.user:
        role = db((db.atms_auth_group.id == db.atms_auth_membership.group_id) &
                  (db.atms_auth_membership.user_id == auth.user.id) &
                  (db.atms_auth_group.role == "TimesheetAdmin")
                  ).select()
        if len(role) > 0:
            privileges_n = role.first().atms_auth_group.role
            privileges_id = role.first().atms_auth_group.id
        else:
            privileges_n = ""
            privileges_id = 0
        data = {
            'id': auth.user.id,
            'firstname': auth.user.first_name,
            'email': auth.user.email,
            'lastname':  auth.user.last_name,
            'username': auth.user.email,
            'privileges': {'name': privileges_n, 'id': 1},  #, 'id': privileges_id
        }
    return dict(data=data)


@auth.requires_login()
def getProjects():
    '''
    Returs list of timesheet projects.
    '''
    response.view = 'generic.' + request.extension
    data = []
    for i in db(db.atms_timesheet_project).select():
        data.append({
            'id': i.id,
            'name': i.name,
            'description': i.description,
            'visible': i.visible,
            'status': i.status,
            'color': i.color,
            'background': i.background})
    return dict(data=data)


@auth.requires_login()
def getTaskType():
    '''
    Returs list of tasks types.
    '''
    response.view = 'generic.' + request.extension
    data = []
    for i in db(db.atms_timesheet_task_type).select():
        data.append({
            'id': i.id,
            'name': i.name,
            'visible': i.visible,
            'description': i.description})
    return dict(data=data)


@auth.requires_login()
def getProjectTaskType():
    '''
    Returs list of tasks types for a project.
    '''
    response.view = 'generic.' + request.extension
    data = []
    for i in db(db.atms_timesheet_project_task_type.id > 0).select():
        data.append({
            'id': i.id,
            'project_id': i.project_id,
            'task_type_id': i.task_type_id,
            'visible': i.visible})
    return dict(data=data)


@auth.requires_login()
def getEvents():
    '''
    Returs list of timesheet events.
    '''
    response.view = 'generic.' + request.extension
    if auth.has_membership("TimesheetAdmin"):
        query = (db.atms_timesheet_task.user_id > 0)
    else:
        query = (db.atms_timesheet_task.user_id == auth.user.id)

    if request.post_vars.user_id:
        if request.post_vars.user_id != 0:
            query = (db.atms_timesheet_task.user_id ==
                     int(request.post_vars.user_id))
        else:
            query = (db.atms_timesheet_task.user_id == auth.user.id)

    query_data = (db.atms_timesheet_task.id > 0)
    if request.post_vars.month:
        query_data = (db.atms_timesheet_task.start.month() ==
                      request.post_vars.month)
        query_data &= (db.atms_timesheet_task.start.year() ==
                       request.post_vars.year)

    data = []

    for i in db(
            (query) &
            (db.atms_timesheet_task.visible == True) &
            (query_data)).select():
        data.append({
            'id': i.id,
            'project_id': i.project_id,
            'task_type_id': i.task_type_id,
            'visible': i.visible,
            'description': i.description,
            'user_id': i.user_id,
            'start': str(i.start),
            'end': str(i.stop),
            'duration': i.duration})

    return dict(data=data)


@auth.requires_login()
def getUsers():
    '''
    Returs list of tasks types for a project.
    '''
    response.view = 'generic.' + request.extension
    data = []
    for i in db().select(
                orderby=db.atms_auth_user.email):
        data.append(
            {
                'id': i.id,
                'first_name': i.first_name,
                'last_name': i.last_name,
                'email': i.email,
                'username': i.email,
                'is_active': i.is_active})
    return dict(data=data)


def editRecord():
    """
    Edits record from a database.

    Kwargs:
        table (str): tablename with record to edit.
        id (str): record id.
        data (json): data to edit.

    Returns:
        dict. The return code::

            0 -- Failure
            1 -- Success

    For specified datastructures check in models/db_wizard.py
    """
    response.view = 'generic.' + request.extension
    data = gluon.contrib.simplejson.loads(json.dumps(request.vars))
    resp = db(db[data['table']].id == int(data['id'])).update(**data['data'])
    return dict(data=resp)


@auth.requires_login()
def addRecord():
    """
    Adds record to a database.

    Kwargs:
        table (str): tablename where to add record.
        data (dict): data to add.

    Returns:
        dict. The return code::

            record id -- Success
            -1 -- Failure (HINT check tablename and fieldnames)

    For specified datastructures check in models/db_wizard.py
    """
    response.view = 'generic.' + request.extension
    data = gluon.contrib.simplejson.loads(json.dumps(request.vars))
    try:
        resp = db[data['table']].insert(**data['data'])
    except (AttributeError, SyntaxError, KeyError):
        return dict(data=-1)
    if data['table'] == "atms_timesheet_task":
        dt = datetime.datetime.now()
        today_date = str(datetime.date(dt.year, dt.month, dt.day))
        start_date = str(db(
            db.atms_timesheet_task.id == resp).select(
            ).first().start).split()[0]
        if start_date == today_date:
            query = (db.atms_timesheet_task.start.month() == dt.month)
            query &= (db.atms_timesheet_task.start.year() == dt.year)
            query &= (db.atms_timesheet_task.start.day() == dt.day)
            query &= (db.atms_timesheet_task.user_id == auth.user.id)
            if len(db(query).select()) == 1:
                addExp(3, 'timesheet')
        else:
            query = (db.atms_timesheet_task.start.month() ==
                     int(start_date.split("-")[1]))
            query &= (db.atms_timesheet_task.start.year() ==
                      int(start_date.split("-")[0]))
            query &= (db.atms_timesheet_task.start.day() ==
                      int(start_date.split("-")[2]))
            query &= (db.atms_timesheet_task.user_id == auth.user.id)
            if len(db(query).select()) == 1:
                addExp(1, 'timesheet')
    return dict(data=resp)


def delRecord():
    """Deletes record from a database.

    Kwargs:
        table (str): tablename with record to delete.
        id (str): record id.

    Returns:
        dict. The return code::

            1 -- Success
            -1 -- Failure (HINT check tablename and fieldnames)

    For specified datastructures check in models/db_wizard.py
    """
    response.view = 'generic.' + request.extension
    data = gluon.contrib.simplejson.loads(json.dumps(request.vars))
    try:
        resp = db(db[data["table"]].id == int(data["id"])).delete()
    except (AttributeError, SyntaxError, KeyError):
        return dict(resp=-1)
    if data["table"] == "atms_timesheet_task":
        addExp(-3, 'timesheet')
    return dict(data=resp)


def resetRelations():
    """Resets relations between projects and tasks.

    Kwargs:
        target (str): tablename with record to delete.
        id (str): record id.

    For specified datastructures check in models/db_wizard.py
    """
    response.view = 'generic.' + request.extension
    if request.vars.target == "project":
        db(db.atms_timesheet_project_task_type.project_id ==
           request.vars.id).update(visible=False)
    else:
        db(db.atms_timesheet_project_task_type.task_type_id ==
           request.vars.id).update(visible=False)
    return dict()

def setRelations():
    response.view = 'generic.' + request.extension
    data = gluon.contrib.simplejson.loads(request.body.read())
    resp = db.atms_timesheet_project_task_type.update_or_insert(
        (db.atms_timesheet_project_task_type.project_id ==
         data["data"]['project_id']) &
        (db.atms_timesheet_project_task_type.task_type_id ==
         data["data"]['task_type_id']),
        project_id=data["data"]['project_id'],
        task_type_id=data["data"]['task_type_id'],
        visible=True)
    return dict(data=resp)
