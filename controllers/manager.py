# -*- coding: utf-8 -*-
from websocket_messaging import websocket_send
from gluon.contrib import simplejson


def user():
    return dict(form=auth())


def test_conv():
    rows = db(db.atms_tcases.procedure_text.startswith("----")).select()
    for r in rows:
        r.procedure_text = r.procedure_text.replace("[[NEWLINE]]", "")
        r.expected_result = r.expected_result.replace("[[NEWLINE]]", "")
        r.update_record()


@auth.requires_login()
def download():
    addExp(1, 'download')
    table = request.args(0)
    index = request.args(1)
    att = db[table](index)
    request.args = [att.filename]
    return response.download(request, db, download_filename=att.title)


@auth.requires_login()
def uploadFile():
    import os
    table = request.args[0]
    V = request.vars
    for k, v in V.items():
        V[k] = None if v == "None" else v
        if v == 'null':
            del V[k]
        if k == 'mode':
            del V[k]
    fileApp = request.post_vars.myFile
    if 'refcase' in V:
        if 'tcase' in V:
            del V['tcase']
        if 'test' in V:
            del V['test']

    filename = fileApp.filename.replace(' ', '')
    aux = db[table].filename.store(fileApp.file, filename)
    del V["myFile"]
    V.filename = aux
    V.title = filename
    V.type = fileApp.type + '/' + os.path.splitext(filename)[1][1:]

    fileApp.file.seek(0, 2)
    V.size = fileApp.file.tell()
    db[table].insert(**V)

    try:
        general = V.type.split('/')[0]
        ext = V.type.split('/')[2]
        if ext == 'py':
            addExp(1, 'code')
        thumbPath = request.folder + "static/images/" + \
            general + "_" + ext + ".png"
        if os.path.isfile(thumbPath):
            return

        # import PIL
        color = "%s" % (general + ext).encode("hex")[::-1][:6]

        r = float(int("0x%s" % color[0:2], 0)) / 255
        g = float(int("0x%s" % color[2:4], 0)) / 255
        b = float(int("0x%s" % color[4:6], 0)) / 255
        from colorsys import rgb_to_hls
        from colorsys import hls_to_rgb
        hls = rgb_to_hls(r, g, b)
        light = hls[1] if hls[1] < 0.9 else 0.9
        light = hls[1] if hls[1] > 0.2 else 0.2
        hls = (hls[0], light, hls[2])
        rgb = hls_to_rgb(*hls)
        RGB = (int(rgb[0] * 255), int(rgb[1] * 255), int(rgb[2] * 255))
        from PIL import Image
        from PIL import ImageDraw
        from PIL import ImageFont

        im = Image.new("RGBA", (512, 512), color=RGB)

        draw = ImageDraw.Draw(im)
        draw.line((25, 0, 25, im.size[1]),
                  fill=(0, 0, 0, 0), width=50)
        draw.line((im.size[0] - 25, 0, im.size[0] - 25, im.size[1]),
                  fill=(0, 0, 0, 0), width=50)
        draw.line((im.size[0] - 200, -10, im.size[0], 100),
                  fill=(0, 0, 0, 0), width=25)

        font_size = 300
        font = ImageFont.truetype(
            request.folder + "static/fonts/old_school_united.ttf",
            font_size)

        while draw.textsize(ext, font=font)[0] > im.size[0] - 100:
            font_size = font_size - 10
            font = ImageFont.truetype(
                request.folder + "static/fonts/old_school_united.ttf",
                font_size)

        w2, h2 = draw.textsize(ext, font=font)
        draw.text(((im.size[0] - w2) / 2, im.size[0] - h2 - 10),
                  ext, fill="white", font=font)

        while draw.textsize(general, font=font)[0] > im.size[0] - 100:
            font_size = font_size - 10
            font = ImageFont.truetype(
                request.folder + "static/fonts/old_school_united.ttf",
                font_size)

        w1, h1 = draw.textsize(general, font=font)
        draw.text(((im.size[0] - w1) / 2, 100),
                  general, fill=(0, 0, 0, 0), font=font)

        im.thumbnail((128, 128), Image.ANTIALIAS)
        im.save(thumbPath, "PNG")
    except Exception as er:
        logger.debug(er)


@auth.requires_login()
def call():
    return service()


def all_cases():
    redirect(URL('index'))


@auth.requires_login()
def index():
    build = 'build'
    if settings.host in ['zbh']:
        build = 'source'
    return dict(scheme=request.env.get('wsgi_url_scheme', 'http').lower()
                if request else 'http',
                host=request.env.http_host,
                build=build, version=settings.version)


@auth.requires_login()
def get_table_permissions(table):
    tpermissions = {}
    tpermissions['assign'] = auth.has_permission(
        'assign', db[table])
    tpermissions['read'] = auth.has_permission(
        'read', db[table])
    tpermissions['create'] = auth.has_permission(
        'create', db[table])
    tpermissions['update'] = auth.has_permission(
        'update', db[table])
    tpermissions['exe'] = auth.has_permission(
        'do', db[table])
    tpermissions['del'] = auth.has_permission(
        'delete', db[table])
    return tpermissions


@auth.requires_login()
def get_rows_permissions():
    response.view = 'generic.' + request.extension
    table = request.vars.table
    ids = request.vars.ids
    ids = request.vars.ids if isinstance(ids, list) else [ids]
    return get_perm(table, ids)


def get_perm(table, ids):
    if ids[0] is None:
        return None
    p = {
        'read': True,
        'create': True,
        'update': True,
        'exe': True,
        'del': True,
        'assign': True
        }
    for i in ids:
        mod = True
        if 'created_by' in db[table].fields:
            mod = db(
                (db[table].id == i) &
                ((db[table].created_by == auth.user_id) |
                 (db[table].created_by == auth.user_id))).select(
                db[table].created_by,
                db[table].modified_by).first()
        perm = True if mod else False
        p['read'] &= auth.has_permission('read', table, i) or perm
        p['create'] &= auth.has_permission('create', table, i) or perm
        p['update'] &= auth.has_permission('update', table, i) or perm
        p['exe'] &= auth.has_permission('do', table, i)
        p['del'] &= auth.has_permission('delete', table, i)
        p['assign'] &= auth.has_permission('assign', table, i)

        if table == 'atms_nodes' and p['del']:
            root = db.atms_nodes[i]
            folders = atms_nodes.descendants_from_node(root).select(
                db.atms_nodes.id)
            for f in folders:
                p['del'] &= auth.has_permission('delete', table, f.id)
            for tc in db(db.atms_tcases.node.belongs(
                    [f.id for f in folders] + [i])).select(db.atms_tcases.id):
                p['del'] &= auth.has_permission('delete', 'atms_tcases', tc.id)
    return p


@auth.requires_login()
def get_basic_permissions():
    permissions = {
        'nodes': get_table_permissions(db.atms_nodes),
        'tcases': get_table_permissions(db.atms_tcases),
        'tests': get_table_permissions(db.atms_tests),
        'ttests': get_table_permissions(db.atms_ttests),
        'cases': get_table_permissions(db.atms_cases),
        'users': get_table_permissions(db.atms_auth_user),
        'my_id': auth.user.id
        }
    return dict(content=permissions)


@auth.requires_login()
@service.json
def get_nodes(node_id=None, root=True, recur=False, table_name='atms_tcases',
              _all='', name='', relatedTest=None):
    query = None
    if not isinstance(name, list):
        query = [query]
    data = []
    items = None

    mainqN = auth.accessible_query('read', db.atms_nodes)
    mainqTC = auth.accessible_query('read', db.atms_tcases)
    if name == '':
        nodes = None
        if node_id is None:
            nodesq = (db.atms_nodes.lft == 1)
        else:
            nodesq = (db.atms_nodes.parent == node_id)
        itemsq = (db[table_name].node == node_id) & (mainqTC)

        if relatedTest:
            nodes = []
            itemsq = ((itemsq) &
                      (db.atms_tcases.id == db.atms_ttests_rel.tcase) &
                      (relatedTest == db.atms_ttests_rel.ttest))

            dp_nodes = set([x.id for x in db(
                           (relatedTest == db.atms_ttests_rel.ttest) &
                           (db.atms_tcases.id == db.atms_ttests_rel.tcase) &
                           (db.atms_tcases.node == db.atms_nodes._id)).select(
                db.atms_nodes.id)])

            ids = []
            for n in dp_nodes:
                node_lindex = 0 if node_id is None \
                    else db.atms_nodes[node_id].lft
                ids += [x.id for x in
                        atms_nodes.ancestors_from_node(n, True)(
                            db.atms_nodes.lft > node_lindex).select(
                            db.atms_nodes.id)]

            nodesq = db.atms_nodes.id.belongs(set(ids))
            if node_id:
                r = db(db.atms_nodes[node_id])
                r = db.atms_nodes[node_id]
                nodesq &= ((db.atms_nodes.lft > r.lft) &
                           (db.atms_nodes.rgt < r.rgt) &
                           (db.atms_nodes.tree_id == r.tree_id))

            temp_nodes = db(nodesq)(mainqN).select(orderby=db.atms_nodes.level)
            if temp_nodes:
                level0 = temp_nodes[0].level
                for n in temp_nodes:
                    if level0 == n.level:
                        nodes.append(n)
                    else:
                        break

        else:
            nodes = db(nodesq)(mainqN).select(
                db.atms_nodes.id,
                db.atms_nodes.name)

        items = db(itemsq)(mainqTC).select(db.atms_tcases.ALL)

        for node in nodes:
            children = [dict(label="Loading", icon="loading",
                             loaded=False, id=None)]
            if recur:
                children = get_nodes(
                    node_id=node['id'],
                    root=False,
                    recur=recur,
                    table_name=table_name,
                    _all=_all,
                    name=name,
                    relatedTest=relatedTest)

            _data = dict(
                label=node['name'],
                children=children,
                icon="folder",
                loaded=recur,
                id=node['id'])

            data.append(_data)
    else:
        q = db[table_name].name.contains(name)
        q |= db[table_name].procedure_text.contains(name)
        q |= db[table_name].expected_result.contains(name)
        if relatedTest:
            q = ((q) &
                 (db.atms_tcases.id == db.atms_ttests_rel.tcase) &
                 (relatedTest == db.atms_ttests_rel.ttest))
        items = db(q)(mainqTC).select(db.atms_tcases.ALL)

    if items:
        for i in items:
            data.append(dict(
                # label=str(i.id) + " " + i.name,
                label=i.name,
                icon="tcase",
                automated=i.is_automated,
                loaded=False,
                id=i.id))

    if root:
        return dict(label='Root',
                    children=data,
                    icon="folder",
                    loaded=False,
                    id=None)
    else:
        return data


@auth.requires_login()
def create_from_nodes():
    addExp(2, 'mng_test')
    mainq = auth.accessible_query('read', db.atms_tcases)

    nodes = request.vars.nodes
    tcases = request.vars.cases

    if 'user' in request.vars:
        if request.vars['user'] == 'me':
            request.vars['user'] = auth.user_id

    toSelect = [
        db.atms_tcases.ALL
        ]

    result = []
    if tcases:
        if not isinstance(tcases, list):
            tcases = [tcases]
        result += db(db.atms_tcases.id.belongs(tcases))(mainq).select(
            *toSelect)
    if nodes:
        if not isinstance(nodes, list):
            nodes = [nodes]
        descendants = []
        for n in nodes:
            descendants += [item.id for item in
                            atms_nodes.descendants_from_node(n, True).select(
                                db.atms_nodes.id)]
        result += db(db.atms_tcases.node.belongs(descendants))(mainq).select(
            *toSelect)
    noDups = {}
    for r in result:
        if r.node:
            path = atms_nodes.ancestors_from_node(r.node, True).select(
                db.atms_nodes.name,
                orderby=db.atms_nodes.level)
            r.path = str.join('/', [x.name for x in path])
        noDups[r.id] = r
    for case in noDups.values():
        case.parent = case.id
        del case.id
        case.user = request.vars.user
        if 'test' in request.vars:
            case.test = request.vars.test
        db.atms_cases.insert(**case.as_dict())


@auth.requires_login()
def get_node_path():
    node = request.args[0]
    path = atms_nodes.ancestors_from_node(node, True).select(
        db.atms_nodes.name,
        orderby=db.atms_nodes.level)
    return dict(content=str.join('/', [x.name for x in path]))


@auth.requires_login()
def assign_test2user():
    addExp(1, 'mng_test')
    mainq = auth.accessible_query('update', db.atms_cases)

    test = request.vars.test
    user = request.vars.user

    db(db.atms_cases.test == test)(mainq).update(user=user)


@auth.requires_login()
def assign_cases():
    if not auth.has_permission('assign', 'atms_cases', 0):
        raise HTTP(401, 'Unauthorized')

    addExp(1, 'mng_test')
    mainq = auth.accessible_query('update', db.atms_cases)

    test = request.vars.test
    user = request.vars.user
    cases = request.vars.cases
    if not isinstance(cases, list):
        cases = [cases]

    kwargs = dict()
    if test:
        kwargs['test'] = test
    if user:
        kwargs['user'] = user
    if len(kwargs):
        db(db.atms_cases.id.belongs(cases))(mainq).update(**kwargs)
        db(db.atms_cases.id.belongs(cases))(mainq).select(
            db.atms_cases.user).first()
    return dict(content=None)


@auth.requires_login()
def move_nodes():
    q_update = auth.accessible_query('update', db.atms_tcases)

    import gluon.contrib.simplejson as simplejson
    data = simplejson.loads(request.body.read())
    copy = True if request.args[0] == 'copy' else False
    for k, v in data.iteritems():
        if k == 'nodes':
            for i in v:
                if copy:
                    copy_nodes(i, data['moveTo'])
                else:
                    if auth.has_permission('update', db.atms_nodes, i):
                        atms_nodes.move_node(i, data['moveTo'])
        if k == 'tcases':
            for i in v:
                if copy:
                    fields = db(db.atms_tcases.id == i).select(
                        db.atms_tcases.name,
                        db.atms_tcases.procedure_text,
                        db.atms_tcases.expected_result,
                        db.atms_tcases.expected_time,
                        db.atms_tcases.result_time,
                        db.atms_tcases.weight).first().as_dict()
                    if auth.has_permission('create', db.atms_tcases):
                        db.atms_tcases.insert(node=data['moveTo'], **fields)
                else:
                    db(db.atms_tcases.id == i)(q_update).update(
                        node=data['moveTo'])


@auth.requires_login()
def copy2Ttest():
    import gluon.contrib.simplejson as simplejson
    data = simplejson.loads(request.body.read())

    perm = get_table_permissions(db.atms_ttests)
    if not perm['create']:
        return

    ids = []
    for n in data['nodes']:
        parent = db.atms_nodes[n]
        ids += [x.id for x in db((db.atms_nodes.lft >= parent.lft) &
                (db.atms_nodes.rgt <= parent.rgt) &
                (db.atms_tcases.node == db.atms_nodes._id) &
                (db.atms_nodes.tree_id == parent.tree_id)).select(
            db.atms_tcases.id)]

    ids += data['tcases']

    for i in set(ids):
        db.atms_ttests_rel.update_or_insert(
            ttest=request.args[0],
            tcase=i)
    return dict()


@auth.requires_login()
def copy_nodes(target, copyTo):
    qnr = auth.accessible_query('read', db.atms_nodes)
    qtr = auth.accessible_query('read', db.atms_tcases)

    fields = db(db.atms_nodes.id == target)(qnr).select(
        db.atms_nodes.id,
        db.atms_nodes.name,
        db.atms_nodes.description).first().as_dict()
    if not fields:
        return

    del fields['id']
    if fields is None:
        return
    if auth.has_permission('create', db.atms_nodes):
        newNodeId = atms_nodes.insert_node(copyTo, **fields)

    tcases = db(db.atms_tcases.node == target)(qtr).select(
        db.atms_tcases.name,
        db.atms_tcases.procedure_text,
        db.atms_tcases.expected_result,
        db.atms_tcases.expected_time,
        db.atms_tcases.result_time,
        db.atms_tcases.weight)

    for tc in tcases:
        fields = tc.as_dict()

        if auth.has_permission('create', db.atms_tcases):
            db.atms_tcases.insert(node=newNodeId, **fields)

    childrenNodes = db(db.atms_nodes.parent == target)(qnr).select()
    if not childrenNodes:
        return
    for ch in childrenNodes:
        copy_nodes(ch.id, newNodeId)


@auth.requires_login()
@service.json
def adbCmd(udid, cmd):
    active = True
    group_name = db(db.atms_devices.udid == udid).select(
        db.atms_devices.host,
        cache=(cache.ram, 60)).first()[db.atms_devices.host]
    baseq = db.scheduler_task.status.contains(
        ['QUEUED', 'ASSIGNED', 'RUNNING'], all=False)
    console_task = db(
        (baseq) &
        (db.scheduler_task.function_name == 'adbConsole') &
        (db.scheduler_task.group_name == group_name) &
        (db.scheduler_task.args.contains(udid))).select(
        db.scheduler_task.id).first()
    if cmd != "KILL":
        if not console_task:
            active = False
            dev = db.atms_devices(udid=udid)
            console_task = queue_task_dep(
                "adbConsole",
                pargs=[udid],
                pvars=dict(dev_id=str(dev.id)),
                group_name=group_name,
                timeout=1800,
                sync_output=5,
                immediate=True)
            db.commit()
            websocket_send(settings.ws_server, '',
                           settings.security_key, 'scheduler_task')
        else:
            active = True
    if not console_task:
        active = False

    if cmd != "INIT":
        websocket_send(settings.ws_server, cmd,
                       settings.security_key, 'adbConsole_%sS' % udid)
    return (console_task, active)


@auth.requires_login()
@request.restful()
def probe():
    def GET(table, id):
        items = db(db[table]._id == id).select("id")
        if len(items) > 0:
            return dict(content=True)
        else:
            return dict(content=False)
    return locals()


@auth.requires_login()
@request.restful()
def api():
    def GET(*args, **vars):
        row_index = 0
        if 'id' in args:
            row_index = args[2]
        # if row_index == 'template':
        #     row = db(db[args[0]].id > 0).select(limitby=(0,1)).first()
        #     for field in row:
        #         row[field] = None
        #     return dict(content=[row])
        ignore_restrictions = ['scheduler_run']
        if args[0] not in ignore_restrictions:
            if not auth.has_permission('read', db[args[0]], row_index):
                raise HTTP(401, 'Unauthorized')
        patterns = [
            '/atms_nodes[atms_nodes]',
            '/atms_nodes/id/{atms_nodes.id}',
            '/atms_tcases[atms_tcases]',
            '/atms_tcases/id/{atms_tcases.id}',
            '/atms_tcases/id/{atms_tcases.id}/:field',
            '/atms_cases[atms_cases]',
            '/atms_cases/id/{atms_cases.id}',
            '/atms_cases/test/{atms_cases.test}',
            '/atms_cases/id/{atms_cases.id}/:field',
            '/atms_tests[atms_tests]',
            '/atms_tests/id/{atms_tests.id}',
            '/atms_tests/project/{atms_tests.project}',
            '/atms_ttests[atms_ttests]',
            '/atms_ttests/id/{atms_ttests.id}',
            '/atms_attachments[atms_attachments]',
            '/atms_attachments/id/{atms_attachments.id}',
            '/atms_devices[atms_devices]',
            '/atms_devices/id/{atms_devices.id}',
            '/atms_servers[atms_servers]',
            '/atms_servers/id/{atms_servers.id}',
            '/wiki_page/id/{wiki_page.id}',
            '/scheduler_run/task_id/{scheduler_run.task_id}']
        parser = db.parse_as_rest(patterns, args, vars)
        perm = get_perm(args[0], [row_index]) if row_index > 0 else None
        if parser.status == 200:
            return dict(content=parser.response, perm=perm)
        else:
            raise HTTP(parser.status, parser.error)

    def POST(table_name, **vars):
        data = simplejson.loads(vars['content'])[0]
        if not auth.has_permission('create', db[table_name]):
            raise HTTP(401, 'Unauthorized')
        for k in data:
            data[k] = None if data[k] == "None" else data[k]
            if 'user' in data:
                data['user'] = auth.user_id if \
                    data['user'] == 'me' else data['user']
        for k, v in data.items():
            if v == 'null':
                del data[k]
        if table_name == 'atms_nodes':
            if 'node' not in data:
                data['node'] = None
            atms_nodes.insert_node(
                data['node'],
                name=data['name'],
                description=data['description'])
        else:
            # exp points
            if table_name == 'atms_nodes':
                addExp(1, 'mng_test')
            elif table_name == 'atms_tcases':
                if 'procedure_text_html' in data:
                    del data['procedure_text_html']
                if 'expected_result_html' in data:
                    del data['expected_result_html']
                addExp(10, 'mng_test')
            elif table_name == 'atms_tests':
                if 'description_html' in data:
                    del data['description_html']
                if 'summary_html' in data:
                    del data['summary_html']
                addExp(1, 'mng_test')
            elif table_name == 'atms_cases':
                if 'procedure_text_html' in data:
                    del data['procedure_text_html']
                if 'expected_result_html' in data:
                    del data['expected_result_html']
                if 'result_notes_html' in data:
                    del data['result_notes_html']
                addExp(1, 'test')
            ttest = 0
            if table_name == 'atms_tests' and 'ttest' in data:
                ttest = data['ttest']
                del data['ttest']
            cItem = db[table_name].validate_and_insert(**data)
            if cItem and ttest:
                copyCases(cItem, ttest)

        websocket_send(settings.ws_server, '',
                       settings.security_key, table_name)

    def PUT(table_name, **vars):
        data = simplejson.loads(vars['content'])[0]
        update = auth.has_permission('update', db[table_name], vars['id'])
        assign = auth.has_permission('assign', db[table_name], vars['id'])
        do = auth.has_permission('do', db[table_name], vars['id'])
        if table_name == 'atms_cases':
            do = db.atms_cases(id=vars['id']).user == auth.user.id
        mod = False
        if table_name != 'scheduler_task' and \
                'created_by' in db[table_name].fields:
            mod = db(
                (db[table_name].id == vars['id']) &
                ((db[table_name].created_by == auth.user_id) |
                 (db[table_name].created_by == auth.user_id))).select(
                db[table_name].created_by,
                db[table_name].modified_by).first()
        permM = True if mod else False
        perm = update or assign or do or permM
        if not perm:
            raise HTTP(401, 'Unauthorized')
        for k, v in data.items():
            data[k] = None if v == "None" else v
            if v == 'null':
                del data[k]
        if table_name == 'atms_nodes':
            if 'node' in data:
                del data['node']
        if table_name == 'atms_tcases':
            if 'procedure_text_html' in data:
                del data['procedure_text_html']
            if 'expected_result_html' in data:
                del data['expected_result_html']
            addExp(2, 'mng_test')
        if table_name == 'atms_tests':
            if 'description_html' in data:
                del data['description_html']
            if 'summary_html' in data:
                del data['summary_html']
            if 'ttest' in data:
                del data['ttest']
            addExp(2, 'mng_test')
        if table_name == 'atms_cases':
            if 'user' in data:
                assign |= auth.user.id == data['user']
                if not assign:
                    del data['user']
            if 'procedure_text_html' in data:
                del data['procedure_text_html']
            if 'expected_result_html' in data:
                del data['expected_result_html']
            if 'result_notes_html' in data:
                del data['result_notes_html']
            flag = db.atms_cases[vars['id']].doneBefore
            if not flag and 'status' in data:
                if data['status'] != 'new' and \
                    data['status'] != 'pending' and \
                    data['status'] != 'aborted' and \
                        data['status'] != 'impossible':

                    if 'expected_time' in data:
                        import math
                        r = int(data['result_time'])
                        e = int(data['expected_time'])
                        bonus = e - r if r < e else 0
                        if data['status'] == 'failed':
                            bonus += e / 3
                        w = int(data['weight'])
                        addExp(int(math.pow(w * (e + bonus), 1 / 2.0)), 'test')
                        data['doneBefore'] = 1
                else:
                    addExp(1, 'test')

        updated = db(db[table_name]._id == vars["id"]).validate_and_update(
            **data)
        if len(updated.errors.keys()) > 0:
            raise HTTP(500, 'Data not valid!')
        if table_name == 'atms_cases':
            testID = db(db[table_name]._id == vars["id"]).select(
                db.atms_cases.test).first()['test']
            if testID:
                count = db(db.atms_cases.test == testID).count()
                count_made = db(
                    (db.atms_cases.test == testID) &
                    (db.atms_cases.status.contains(
                        ['aborted',
                         'impossible',
                         'passed',
                         'failed']))).count()
                factor = float(count_made) / count
                if factor >= 0 and factor < 1:
                    db(db.atms_tests._id == testID).validate_and_update(
                        status='pending')
                elif factor == 1:
                    db(db.atms_tests._id == testID).validate_and_update(
                        status='finished')
                websocket_send(settings.ws_server, '',
                               settings.security_key, 'atms_tests')
        websocket_send(settings.ws_server, '',
                       settings.security_key, table_name)

    def DELETE(table_name, **vars):
        if not auth.has_permission('delete', db[table_name], vars['id']):
            raise HTTP(401, 'Unauthorized')
        if table_name == 'atms_nodes':
            if vars['type'] == 'folder':
                nodes = atms_nodes.descendants_from_node(
                    vars['id'], include_self=True).select('id')
                nodes2del = [n._extra.id for n in nodes]
                if vars['mode'] == 'tcases':
                    atms_nodes.delete_node(vars['id'])
                    db(db.atms_tcases.node.belongs(nodes2del)).delete()
                if vars['mode'] == 'ttests':
                    tcases = db(db.atms_tcases.node.belongs(nodes2del)).select(
                        db.atms_tcases.id)
                    db((db.atms_ttests_rel.tcase.belongs(tcases)) &
                       (db.atms_ttests_rel.ttest == vars['ttest'])).delete()
            if vars['type'] == 'tcase':
                if vars['mode'] == 'tcases':
                    db(db['atms_tcases']._id == vars['id']).delete()
                if vars['mode'] == 'ttests':
                    db((db.atms_ttests_rel.ttest == vars['ttest']) &
                       (db.atms_ttests_rel.tcase == vars['id'])).delete()
        elif table_name == 'atms_tests':
            db(db.atms_cases.test == vars['id']).delete()
            db(db[table_name]._id == vars['id']).delete()
        else:
            db(db[table_name]._id == vars['id']).delete()
        websocket_send(settings.ws_server, '',
                       settings.security_key, table_name)
    return locals()


@auth.requires_login()
@request.restful()
def auto_devs_api():
    def PUT(table_name, **vars):
        if not auth.has_permission('update', db[table_name], vars['id']):
            raise HTTP(401, 'Unauthorized')
        devs = db(db[table_name].id == vars['id']).select(
            db[table_name].auto_devs).first()['auto_devs']
        devs = devs if devs is not None else []
        devs = [
            r.id for r in
            db(db.atms_devices.id.belongs(devs)).select(db.atms_devices.id)]
        if isinstance(vars['devs'], list):
            devs2 = [long(i) for i in vars['devs']]
        else:
            devs2 = [long(vars['devs'])]
        if vars['add'] == 'true':
            devs = list(set(devs) | set(devs2))
        else:
            devs = list(set(devs) - set(devs2))
        db(db[table_name].id == vars['id']).validate_and_update(
            auto_devs=devs)

    return locals()


@auth.requires_login()
@request.restful()
def task_api():
    response.view = 'generic.json'

    def PUT(task_id, action):
        if action == 'stop':
            task_ref = db.scheduler_task[task_id]
            if task_ref.status == 'RUNNING':
                scheduler.stop_task(task_ref.uuid)
        if action == 'resume':
            websocket_send(current.settings.ws_server, "RESUME",
                           current.settings.security_key, 'taskId%s' % task_id)

    def POST(task, **vars):
        SYNC = 5
        TIMEOUT = 500
        group_name = settings.main_group
        if 'group_name' in vars:
            group_name = (
                settings.main_group
                if settings.host_master == vars['group_name']
                else vars['group_name'])
            del vars['group_name']
        if 'dev_id' in vars:
            case_id = long(vars['case_id'])
            mobile_types = zip(*db.atms_devices.type.requires.options())[0][2:]
            ted_types = zip(*db.atms_devices.type.requires.options())[0][:2]
            testcase = db.atms_cases[case_id]
            TIMEOUT = (testcase.auto_timeout or 2880) * 60
            if vars['dev_id'] == '_all':
                devs = testcase.auto_devs
            else:
                devs = [db.atms_devices[long(vars['dev_id'])]]

            if len(devs) == 0:
                raise HTTP(405, T('You have to use at least one device!'))

            # states = list(set([d.state for d in devs]))
            # if states[0] != 'idle' or len(states) > 1:
            #     raise HTTP(405, T('Not all devices are in IDLE state!'))

            connected_states = list(set([d.connected for d in devs]))
            if connected_states[0] is not True or len(connected_states) > 1:
                raise HTTP(405, T('Not all devices are in CONNECTED state!'))

            mobile_devs = [d for d in devs if d.type in mobile_types]
            ted_devs = [d for d in devs if d.type in ted_types]
            stb_devs = [d for d in devs if d.type in ['dut']]
            task_name = ', '.join([f for f in testcase.auto_funcs])
            if len(set(mobile_devs)) > 0 and len(set(stb_devs)) == 0:
                for d in mobile_devs:
                    # TODO - coś się wywala przy walidacji, ale nie wiem
                    # jeszcze gdzie :/
                    queue_task_dep(
                        task,
                        devices=[d.id],
                        pvars={
                            'case_id': case_id,
                            'duts': [d.id],
                            'teds': [t.id for t in ted_devs],
                            'user_id': auth.user.id,
                            'lang': request.env.http_accept_language
                            },
                        task_name=task_name,
                        group_name=d.host,
                        timeout=TIMEOUT,
                        sync_output=SYNC)
                return
            group_name = [d.host for d in devs if d.host != 'shared'][0]
            queue_task_dep(
                task,
                devices=[d.id for d in devs],
                pvars={
                    'case_id': case_id,
                    'duts': [d.id for d in mobile_devs] + [
                        d.id for d in stb_devs],
                    'teds': [t.id for t in ted_devs],
                    'user_id': auth.user.id,
                    'lang': request.env.http_accept_language
                    },
                task_name=task_name,
                group_name=group_name,
                timeout=TIMEOUT,
                sync_output=SYNC)
            return
        if task == 'dumpTcases':
            vars['user'] = auth.user_id
        if task == 'parseUECap' and vars['id'] == 'null':
            db(db.atms_uecap.id > 0).update(f_processed=False)
            TIMEOUT = 3600 * 48
        if task == '_empty':
            task = 'listAndroidDevices'
            group_name = ''
        if task in ['parseUECap', 'parseWiFiCap']:
            TIMEOUT = 3600 * 3
        queue_task_dep(
            task,
            pvars=vars,
            group_name=group_name,
            timeout=TIMEOUT,
            sync_output=SYNC)

    def DELETE(action):
        if action == 'clear_finished_ok':
            db(db.scheduler_task.status.contains(
                ['COMPLETED']))(
                ~db.scheduler_task.task_name.startswith('hidden')).delete()
        if action == 'clear_finished_all':
            db(~db.scheduler_task.status.contains(
                ['ASSIGNED', 'QUEUED', 'RUNNING']))(
                ~db.scheduler_task.task_name.startswith('hidden')).delete()

    websocket_send(settings.ws_server, '',
                   settings.security_key, 'scheduler_task')
    return locals()


@auth.requires_login()
@service.json
def test_methods(case_id, query=None, extra=None):
    if extra:
        if not isinstance(extra, list):
            extra = [extra]
        else:
            extra = extra
    else:
        extra = []

    if len(extra) > 0:
        return [dict(id=d, label=d) for d in extra]

    try:
        script_id = db.atms_cases[case_id].auto_script
    except AttributeError:
        script_id = None
    if not script_id or query == 'null':
        return []

    @cache.action(time_expire=5, cache_model=cache.ram)
    def get_methods():
        import ast
        import re
        filename = db.atms_attachments[script_id].filename
        openf = open
        file_path = os.path.join(request.folder, 'uploads', filename)
        if fs:
            openf = fs.open
            file_path = filename

        body = openf(file_path, 'r').read()
        body = re.sub(r'^#.*', '', body, flags=re.MULTILINE)
        methods = []
        for node in ast.walk(ast.parse(body)):
            try:
                if 'TestCase' in node.name:
                    for child in ast.iter_child_nodes(node):
                        if 'FunctionDef' == child.__class__.__name__:
                            methods.append(child.name)
            except:
                pass
        forbidden = ['setUp', 'tearDown',
                     'setUpClass', 'tearDownClass',
                     'setUpModule', 'tearDownModule']
        return [dict(id=m, label=m) for m in methods if not
                (m in forbidden or m.startswith('_'))]

    return get_methods()


@auth.requires_login()
@request.restful()
def file_api():
    from misc import validate_script

    def GET():
        filelist = os.listdir(
            os.path.join(request.folder, 'sample_scripts'))
        return dict(content=filelist)

    def POST(table_name, mode, filename, template=None, **kwargs):
        for k, v in kwargs.items():
            kwargs[k] = None if v == "None" else v
            if v == 'null':
                del kwargs[k]
        if mode in ['script', 'create-script']:
            if mode == 'create-script':
                filename = '%s_%s.py' % (
                    template.replace('.py', ''), 'tmplt')
            filename = filename.replace('.py', '') + '.py'
            tpath = os.path.join(request.folder, 'sample_scripts',
                                 template + '.py')
            if not os.path.exists(tpath):
                raise HTTP(503, 'No such template!')
            kwargs['type'] = 'text/x-python/py'
            kwargs['size'] = os.path.getsize(tpath)
            if 'refcase' in kwargs:
                kwargs['tcase'] = None
                kwargs['test'] = None
            if 'tcase' in kwargs:
                kwargs['test'] = None

            att_id = db[table_name].insert(
                filename=db[table_name].filename.store(
                    open(tpath, 'r'), filename),
                title=filename,
                **kwargs)
            if 'refcase' in kwargs and mode == 'create-script':
                db(db.atms_cases.id == kwargs['refcase']).update(
                    auto_script=att_id,
                    auto_funcs=[])

                websocket_send(settings.ws_server, '',
                               settings.security_key, 'atms_attachments')
            addExp(1, 'code')

    def PUT(table_name, rId, **vars):
        import os
        import tempfile
        from datetime import datetime
        row = db[table_name][rId]

        openf = open
        file_path = os.path.join(request.folder, 'uploads', row.filename)
        if fs:
            openf = fs.open
            file_path = row.filename

        body = request.body.read()
        tmp = tempfile.mkstemp(suffix=".py")
        with open(tmp[1], 'w') as f:
            f.write(body)
        result = validate_script(tmp[1])
        os.remove(tmp[1])

        if result[0]:
            with openf(file_path, "w") as f:
                # TODO - fsopen & open acts differently!!!
                try:
                    f.write(body.replace('\t', '    ').decode('utf-8'))
                except:
                    f.write(body.replace('\t', '    '))

            row.modified_on = datetime.now()
            row.update_record()
            addExp(1, 'code')
        else:
            response.view = 'generic.json'
            return dict(content=result[1])

    return locals()


@auth.requires_login()
def get_data():
    table = request.args[0]
    response.view = 'generic.' + request.extension
    query1 = auth.accessible_query('read', db[table])

    V = request.vars
    for k, v in V.items():
        V[k] = None if v == "None" else v
        if v == 'null':
            del V[k]

    query2 = None
    for k, v in V.items():
        if not query2:
            query2 = (db[table][k] == v)
        elif table == 'atms_attachments':
            query2 |= (db[table][k] == v)
        else:
            query2 &= (db[table][k] == v)

    query = ((db[table].created_by == db.atms_auth_user.id) & query1 & query2)
    result = db(query).select(
        db.atms_attachments.id,
        db.atms_attachments.refcase,
        db.atms_attachments.tcase,
        db.atms_attachments.test,
        db.atms_attachments.type,
        db.atms_attachments.title,
        db.atms_attachments.filename,
        db.atms_attachments.size,
        db.atms_auth_user.first_name,
        db.atms_auth_user.last_name,
        db.atms_attachments.created_on)
    factored_result = []
    for i in result.as_list():
        factored_result.append(flatten(i))
    return dict(content=factored_result)


@auth.requires_login()
def get_tests_events():
    response.view = 'generic.' + request.extension

    from datetime import datetime
    from datetime import timedelta
    query = auth.accessible_query('read', db.atms_tests)
    start = datetime.strptime(request.vars['start'], '%Y-%m-%d')
    end = datetime.strptime(request.vars['end'], '%Y-%m-%d')
    rows = db(
        (query) &
        ((db.atms_tests.start >= start) | (db.atms_tests.stop >= start)) &
        (db.atms_tests.start <= end)).select()
    arr = []

    for r in rows:
        query = db.atms_cases.test == r.id

        cases_all = db(query).count()
        cases_done = db(query)(~db.atms_cases.status.contains(
            ['new', 'pending'])).count()

        sum1 = db.atms_tcases.expected_time.sum()
        sum2 = db.atms_tcases.result_time.sum()
        cases_allT = db(query).select(sum1).first()[sum1]
        cases_doneT = db(query)(~db.atms_cases.status.contains(
            ['new', 'pending'])).select(sum2).first()[sum2]

        if cases_doneT is not None:
            cases_doneT = timedelta(
                minutes=cases_doneT)
            cases_allT = timedelta(
                minutes=cases_allT)
        else:
            cases_doneT = '...'
            cases_allT = '...'

        if cases_all:
            percents = float(cases_done * 100) / cases_all
        else:
            percents = 0

        active_bar = ''
        color = '#008A00'
        if 'pending' in r.status:
            active_bar = 'progress-striped active'
        if 'waiting' in r.status:
            color = '#008299'
        if 'aborted' in r.status:
            color = '#888888'
        if 'finished' in r.status:
            color = '#094AB2'
        # if late:
        #     color = '#DC572E'

        mod = db(
            (db.atms_tests.id == r.id) &
            ((db.atms_tests.created_by == auth.user_id) |
             (db.atms_tests.created_by == auth.user_id))).select(
            db.atms_tests.created_by,
            db.atms_tests.modified_by).first()
        perm = True if mod else False
        perm = perm or auth.has_permission(
            'update', db.atms_tests, r.id, auth.user_id)

        event = Storage()
        event.id = r.id
        event.start = r.start
        event.end = r.stop
        event.title = r.name
        event.color = color
        event.cases = TAG[''](
            DIV(DIV(_class='bar',
                    _style="width:{0}%;height:5px;".format(percents)),
                _class='progress %s' % active_bar,
                _style='height:5px;margin-bottom:5px;'),
            DIV("{0}/{1} - {2:.0f}%".format(
                cases_done, cases_all, percents)),
            # DIV("{0}/{1}".format(cases_doneT, cases_allT),
            #     _class='btn btn-danger')
            )
        event.startEditable = perm
        event.editable = perm
        arr.append(event)
    return dict(content=arr)


@auth.requires_login()
def calculate_test_duration():
    idt = request.vars['id']

    from datetime import timedelta
    # import BusinessTime
    query = auth.accessible_query('update', db.atms_tests)
    test = db(query)(db.atms_tests.id == idt).select().first()
    cases = db(db.atms_cases.test == idt).select(
        db.atms_cases.expected_time,
        db.atms_cases.result_time,
        db.atms_cases.status)
    eventDuration = test.start
    for c in cases:
        status = c.status
        if status == 'new' or status == 'pending':
            eventDuration += timedelta(minutes=c.expected_time)
        else:
            eventDuration += timedelta(minutes=c.result_time)
    test.stop = eventDuration
    # print '****' + test.name + '****'
    # print test.start
    # print test.stop
    # print test.stop - test.start
    # businessTime = BusinessTime.BusinessTime(
    #     holidays=BusinessTime.USFederalHolidays())
    # print businessTime.businesstimedelta(test.start, test.stop)
    db(db.atms_tests.id == idt).validate_and_update(stop=test.stop)
    return dict()


@auth.requires_login()
def copy_cases():
    content = request.vars.content
    if not content:
        return
    if not isinstance(content, list):
        content = [content]
    authq = auth.accessible_query('read', db.atms_cases)
    toSelect = set([
        field for field in db.atms_cases if 'html' not in str(field)])
    toSelect = list(
        toSelect -
        set([db.atms_cases._id,
             db.atms_cases.doneBefore,
             db.atms_cases.result_notes,
             db.atms_cases.result_time,
             db.atms_cases.status,
             db.atms_cases.auto_report,
             db.atms_cases.auto_execution_time,
             db.atms_cases.created_by,
             db.atms_cases.created_on,
             db.atms_cases.modified_by,
             db.atms_cases.modified_on,
             db.atms_cases.is_active])
        )

    rows = db(authq)(db.atms_cases.id.belongs(content)).select(*toSelect)
    for r in rows:
        r.status = 'new'
        data = {k: v for k, v in r.as_dict().items() if v is not None}
        result = db.atms_cases.validate_and_insert(**data)
        if result.id > 0:
            from misc import copy_and_assign_files
            # copy and update attachments' refs
            copy_and_assign_files(result.id)


@auth.requires_login()
def html_render():
    v = request.post_vars.content
    return dict(content=auth._wiki.markmin_base(v, True, True))


@auth.requires_login()
@request.restful()
def uapi():
    response.view = 'generic.' + request.extension

    def GET(table_name, *args):
        if table_name == 'user':
            return dict(content=db(db.atms_auth_user.id == args[0]).select(
                db.atms_auth_user.id,
                db.atms_auth_user.first_name,
                db.atms_auth_user.last_name))
        if table_name == 'users':
            activeq = db.atms_auth_user.is_active == True
            if auth.has_permission('delete', db.atms_auth_user):
                activeq |= db.atms_auth_user.is_active == False
            d = db(db.atms_auth_user.id > 0)(activeq).select(
                db.atms_auth_user.id,
                db.atms_auth_user.first_name,
                db.atms_auth_user.last_name,
                db.atms_auth_user.identicon,
                db.atms_auth_user.color,
                db.atms_auth_user.is_active,
                orderby=~db.atms_auth_user.is_active |
                db.atms_auth_user.last_name)
            for r in d:
                groups = db(
                    (~db.atms_auth_group.role.startswith('user_')) &
                    (db.atms_auth_membership.user_id == r.id) &
                    (db.atms_auth_group.id ==
                     db.atms_auth_membership.group_id)).select(
                         db.atms_auth_group.role)
                fejms = db(
                    (db.atms_auth_user.id == db.atms_fejms.user_id) &
                    (db.atms_fejms.expire_date >= datetime.datetime.now()) &
                    (db.atms_auth_user.id == r.id)).select(
                    db.atms_fejms.code, distinct=True, cacheable=True,
                    cache=(cache.ram, 5))
                r.fejms = [x['code'] for x in fejms]
                r.special_state = None
                # handle fejms
                if 'milord' in r.fejms:
                    r.first_name = "Sir " + r.first_name
                    r.identicon = "atms/milord.jpg"
                if 'ladymilord' in r.fejms:
                    r.first_name = "Milady " + r.first_name
                    r.identicon = "atms/ladymilord.png"
                if 'birthday' in r.fejms:
                    r.first_name = "Najlepsiejszego " + r.first_name
                    r.last_name = r.last_name + " z okazji urodzinek"
                    r.identicon = "atms/birthday.jpg"
                    r.special_state = "BD"
                if r.is_active is False:
                    r.identicon = 'atms/fired.png'

                c_count = db.atms_cases.id.count()
                exp_time = db.atms_cases.expected_time.sum()
                res_time = db.atms_cases.result_time.sum()

                query = (r.id == db.atms_cases.user) & (
                    db.atms_cases.test == db.atms_tests.id)
                query_done = query & (~db.atms_cases.status.contains(
                    ['new'])) & (db.atms_tests.status == 'pending')
                query_doneAndPending = query & (~db.atms_cases.status.contains(
                    ['new', 'pending'])) & (db.atms_tests.status == 'pending')
                query_total = query & (db.atms_tests.status == 'pending')

                itemsTotal = db(query_total).select(
                    c_count,
                    cacheable=True,
                    cache=(cache.ram, 5)).first()[c_count]
                itemsNb = db(query_doneAndPending).select(
                    c_count,
                    cacheable=True,
                    cache=(cache.ram, 5)).first()[c_count]
                timeOccupanceTotal = db(query_total).select(
                    exp_time,
                    cacheable=True,
                    cache=(cache.ram, 5)).first()[exp_time]
                timeOccupance = db(query_done).select(
                    res_time,
                    cacheable=True,
                    cache=(cache.ram, 5)).first()[res_time]
                lastUpdate = '-'
                try:
                    lastUpdate = db(db.atms_cases.user == r.id).select(
                        db.atms_cases.modified_on,
                        orderby=~db.atms_cases.modified_on,
                        limitby=(0, 1)
                        ).first()[db.atms_cases.modified_on]
                except:
                    pass
                r["label"] = r.get("first_name", "Pan") + " " + \
                    r.get("last_name", "Ktoś")
                r["itemsNb"] = itemsNb
                r["itemsTotal"] = itemsTotal
                r["timeOccupance"] = timeOccupance or 0
                r["timeOccupanceTotal"] = timeOccupanceTotal or 0
                r["lastUpdate"] = lastUpdate
                r["tooltip"] = "tooltip tu będzie"
                r["state"] = r.special_state
                r["groups"] = [x.role for x in groups]
                r["is_active"] = r.is_active
            return dict(content=d)
        if table_name == 'permissions':
            d = db(db.atms_auth_permission.group_id == db.atms_auth_group.id)(
                db.atms_auth_membership.group_id == db.atms_auth_group.id)(
                db.atms_auth_membership.user_id == auth.user.id).select(
                db.atms_auth_permission.name,
                db.atms_auth_permission.table_name)
            return dict(content=d)

    def PUT(table_name, user_id, role, to_remove):
        if not auth.has_permission('delete', db.atms_auth_user):
            raise HTTP(401, 'Unauthorized')

        to_remove = True if to_remove == 'true' else False
        group_id = db.atms_auth_group(role=role).id
        if to_remove:
            db((db.atms_auth_membership.group_id == group_id) &
               (db.atms_auth_membership.user_id == user_id)).delete()
        else:
            db.atms_auth_membership.insert(
                group_id=group_id,
                user_id=user_id)
        return dict(content=None)

    def DELETE(table_name, id, is_active):
        deactivate = False if is_active == 'true' else True
        if not auth.has_permission('delete', db.atms_auth_user):
            raise HTTP(401, 'Unauthorized')

        user = db.atms_auth_user[id]
        user.is_active = deactivate
        user.registration_key = '' if deactivate else 'blocked'
        user.update_record()
        return dict(content=None)
    return locals()


@auth.requires_login()
def notifyUser():
    args = request.args
    args.append(request.env.http_accept_language)
    scheduler.queue_task('notifyUser',
                         task_name="hidden_notifyUser",
                         pargs=args,
                         group_name=settings.host)


@auth.requires_login()
@request.restful()
def deps_api():
    response.view = 'generic.' + request.extension

    def GET(task_id):
        rows = db(db.scheduler_task_deps.task_parent == task_id).select(
            db.scheduler_task_deps.task_child)
        data = []
        for r in rows:
            data.append(r.task_child)
        return dict(content=data)

    def PUT(**vars):
        parent_id = vars['id']
        db(db.scheduler_task_deps.task_parent == parent_id).delete()
        children = vars['children']
        children = children if isinstance(children, list) else [children]
        for p in children:
            if p == 'NaN':
                continue
            db.scheduler_task_deps.insert(
                task_parent=parent_id, task_child=int(p))

    return locals()


@auth.requires_login()
def raiseJobs():
    task = request.vars['task']

    if task == 'addTcases':
        scheduler.queue_task(
            'addTcases',
            timeout=800,
            sync_output=5)


@auth.requires_login()
def copyCases(toTest, fromTtest):
    perm = auth.has_permission('create', db.atms_tests)
    if not perm:
        return
    ids = [x.tcase for x in db(db.atms_ttests_rel.ttest == fromTtest).select(
        db.atms_ttests_rel.tcase)]

    tcases = db(db.atms_tcases.id.belongs(set(ids))).select()

    for tc in tcases:
        path = atms_nodes.ancestors_from_node(tc.node, True).select(
            db.atms_nodes.name,
            orderby=db.atms_nodes.level)
        path = str.join('/', [x.name for x in path])
        db.atms_cases.validate_and_insert(
            test=toTest,
            path=path,
            parent=tc.id,
            user=None,
            name=tc.name,
            procedure_text=tc.procedure_text,
            expected_result=tc.expected_result,
            expected_time=tc.expected_time,
            weight=tc.weight)


# @auth.requires_login()
@request.restful()
def tapi():
    response.view = 'generic.' + request.extension

    def GET(table_name, action, **vars):
        query = (db[table_name].id > 0)
        extra = Storage()

        # special handling
        if table_name == 'scheduler_task':
            query &= ~db.scheduler_task.task_name.startswith('hidden')

        # advanced filtering
        if 'csfilter' in vars:
            import base64
            import gluon.contrib.simplejson as simplejson
            b64 = base64.b64decode(vars['csfilter'])
            for k, v in simplejson.loads(b64).items():
                # special query cases
                if table_name == 'atms_devices':
                    if k in ['tcase_id', 'case_id']:
                        extra[k] = v
                        continue
                if table_name == 'atms_cases':
                    if k in ['test', 'user']:
                        v = None if v == 'None' else v
                        v = auth.user.id if v == 'me' else v
                        query &= (db[table_name][k] == v)
                        continue
                if isinstance(v, dict):
                    if 'min' in v:
                        query &= (db[table_name][k] >= v['min'])
                    if 'max' in v:
                        query &= (db[table_name][k] <= v['max'])
                elif isinstance(v, list):
                    if 'ref' in k or k in ['created_by', 'updated_by']:
                        query &= (db[table_name][k].belongs(
                            [int(x) for x in v]))
                    else:
                        query &= (db[table_name][k].contains(
                            [x for x in v]))
                elif isinstance(v, bool):
                    query &= (db[table_name][k] == v)
                else:
                    query &= (db[table_name][k].like(
                              '%%%s%%' % v, case_sensitive=False))

        if action == 'count':
            return dict(
                content=db(query).count(),
                descriptor=getTableDescriptor(table_name))

        select_params = Storage()

        if table_name == 'atms_cases' and 'orderby' not in vars:
            select_params.orderby = '%s DESC' % db.atms_cases.created_on
        if table_name == 'scheduler_task' and 'orderby' not in vars:
            select_params.orderby = '%s DESC' % db.scheduler_task.id
        if table_name == 'atms_devices' and 'orderby' not in vars:
            select_params.orderby = \
                db.atms_devices.host | db.atms_devices.subtype

        if 'orderby' in vars:
            select_params.orderby = '%s %s' % (
                str(db[table_name][vars['orderby']]),
                vars['asc'])
        if 'to' in vars and 'from' in vars:
            lfrom = int(vars['from'])
            lto = int(vars['to']) + 1
            if lto - lfrom > 5000:
                raise HTTP(503, 'Requested volume of data too big!')
            select_params.limitby = (lfrom, lto)

        data2refactor = False
        refactorRules = Storage()
        toSelect = []
        lefts = {}

        for f in db[table_name]:
            rule = Storage()
            # handle references
            if 'list:reference' in f.type:
                continue
            if 'reference' in f.type:
                data2refactor = True
                second_table = f.type.split(" ")[1]
                alias = db[second_table].with_alias(f.name)
                lefts[f.name] = db[second_table].with_alias(f.name).on(
                    db[table_name][f.name] == alias.id)

                rule.ref = f.name
                # special handling
                if second_table == 'atms_auth_user':
                    toSelect += [alias.first_name,
                                 alias.last_name]
                    rule.format = "%(first_name)s %(last_name)s"
                elif second_table == 'atms_tests':
                    toSelect += [alias.name]
                    rule.format = "%(name)s"
            if f.name in ['auto_bin', 'auto_script', 'auto_report']:
                rule.func = lambda r: bool(r[table_name].auto_bin)
            if f.name == 'auto_devs':
                rule.func = (lambda r: len(r[table_name].auto_devs) if
                             r[table_name].auto_devs else 0)
            # hide text fields as redundant for table widgets
            if f.type != 'text' and table_name != 'scheduler_task':
                toSelect.append(f)

            if rule.func is not None or rule.format is not None:
                refactorRules[f.name] = rule

        select_params.left = lefts.values()
        data = db(query).select(
            *toSelect,
            **select_params)

        # extra parameters
        data = get_extra(data, table_name, extra)

        if not data2refactor:
            return dict(content=data.as_list())

        refactoredData = []
        for r in data:
            direct = r[table_name]
            for key in refactorRules:
                rule = refactorRules[key]
                if 'func' in rule:
                    direct[key] = rule.func(r)
                else:
                    direct[key] = rule.format % r[rule.ref]
                    if 'None' in direct[key]:
                        direct[key] = ''
            refactoredData.append(direct)
        return dict(content=refactoredData)

    return locals()


@auth.requires_login()
@service.json
def store(t_name, f_name, query=None, extra=None):
    if extra:
        if not isinstance(extra, list):
            extra = [extra]
        else:
            extra = extra
    else:
        extra = []

    # cast args -> ID, belongs func requires it
    if db[t_name][f_name].type in ['id', 'integer']:
        extra = [int(x) for x in extra]

    if db[t_name][f_name].requires.__class__.__name__ == 'IS_IN_SET':
        options = db[t_name][f_name].requires.options()
        if len(options) != 0:
            data = []
            for o in db[t_name][f_name].requires.options():
                if not o[0]:
                    continue
                data.append(dict(
                    value=True if o[0] in extra else False,
                    id=o[0],
                    label=o[1]))
            return data

    tablemap = dict(
        test={'tn': 'atms_tests',
              'id_path': 'id', 'format': '%(name)s',
              'extraToSelect': [db.atms_tests.name]},
        auto_report={'tn': 'wiki_page',
                     'id_path': 'id', 'format': '%(title)s',
                     'extraToSelect': [db.wiki_page.title]},
        auto_devs={'tn': 'atms_devices',
                   'id_path': 'id', 'format': '%(model)s',
                   'extraToSelect': [db.atms_devices.model]},
        user={'tn': 'atms_auth_user',
              'id_path': 'id', 'format': '%(first_name)s %(last_name)s',
              'extraToSelect': [db.atms_auth_user.first_name,
                                db.atms_auth_user.last_name]},
        ref_cbs={'tn': 'cbs_device',
                 'id_path': 'id',
                 'format': '%(cbs_company_name)s %(cbs_device_model_name)s \
                            | %(cbs_device_imei)s',
                 'joins': [db.cbs_device.model_id == db.cbs_device_model.id,
                           db.cbs_device.company_id == db.cbs_company.id,
                           db.cbs_device.visible==True],
                 'extraToSelect': [db.cbs_device.imei,
                                   db.cbs_device_model.name,
                                   db.cbs_company.name]})

    user_fields = ['user', 'created_by', 'updated_by', 'modified_by']
    if f_name in user_fields:
        f_name = 'user'
    if f_name not in tablemap:
        raise HTTP(503, "This store is not supported")

    table_name = tablemap[f_name]['tn']
    format = tablemap[f_name]['format']
    id_path = tablemap[f_name]['id_path']

    q = (db[table_name].id > 0)
    if 'is_active' in db[table_name].fields:
        q &= db[table_name].is_active == True
    if 'joins' in tablemap[f_name]:
        for j in tablemap[f_name]['joins']:
            q &= j
    if query != 'null':
        qq = None
        for f in tablemap[f_name]['extraToSelect']:
            x = str(f).split('.')
            qqq = (db[x[0]][x[1]].like(
                '%%%s%%' % query, case_sensitive=False))
            if not qq:
                qq = qqq
            else:
                qq |= qqq
        q &= qq

    rows = db(q).select(
        db[table_name][id_path],
        *tablemap[f_name]['extraToSelect'],
        orderby=tablemap[f_name]['extraToSelect'][0],
        limitby=(0, 2000)).as_list()

    if len(extra) > 0:
        if extra[0] != '_':
            rows += db(q)(db[table_name][id_path].belongs(extra)).select(
                db[table_name][id_path],
                *tablemap[f_name]['extraToSelect']).as_list()

    data = []
    for r in rows:
        data.append(flatten(r))
    rows = [dict(y) for y in set(tuple(x.items()) for x in data)]
    if 'joins' in tablemap[f_name]:
        id_path = "%s_%s" % (
            tablemap[f_name]['tn'], tablemap[f_name]['id_path'])
    data = []
    for r in rows:
        preselect = True if str(r[id_path]) in extra else False
        label = format % r
        data.append(dict(
            value=preselect,
            id=r[id_path],
            label=label))
    return data
