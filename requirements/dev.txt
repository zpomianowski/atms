mysqlclient # libmysqlclient-dev  python-mysqldb
numpy
redis
tornado
simplejson
cffi
paramiko
fs==0.5.4 # build-essential libssl-dev libffi-dev python-dev
pillow
lxml
pykml
watchdog
Appium-Python-Client
websocket-client
openpyxl
pyvisa-py
PyUSB
PySerial
netifaces
bokeh
ipython
#requests==2.3.0 # for ixchariot
requests>=2.18.4
gTTS
influxdb

# For docs & dev
pylint
pylint-web2py
sphinx
sphinxcontrib-napoleon
sphinx_rtd_theme
grip