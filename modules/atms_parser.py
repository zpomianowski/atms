# -*- conding: utf-8 -*-
from gluon.storage import Storage
import os
import csv
import codecs
import cStringIO


class UTF8Recoder:
    def __init__(self, f, encoding):
        self.reader = codecs.getreader(encoding)(f)

    def __iter__(self):
        return self

    def next(self):
        return self.reader.next().encode("utf-8")


class UnicodeReader:
    def __init__(self, f, dialect=csv.excel, encoding="utf-8-sig", **kwds):
        f = UTF8Recoder(f, encoding)
        self.reader = csv.reader(f, dialect=dialect, **kwds)

    def next(self):
        '''next() -> unicode
        This function reads and returns the next line as a Unicode string.
        '''
        row = self.reader.next()
        return [unicode(s, "utf-8") for s in row]

    def __iter__(self):
        return self


class UnicodeWriter:
    def __init__(self, f, dialect=csv.excel, encoding="utf-8-sig", **kwds):
        self.queue = cStringIO.StringIO()
        self.writer = csv.writer(self.queue, dialect=dialect, **kwds)
        self.stream = f
        self.encoder = codecs.getincrementalencoder(encoding)()

    def writerow(self, row):
        '''writerow(unicode) -> None
        This function takes a Unicode string and encodes it to the output.
        '''
        self.writer.writerow([s.encode("utf-8") for s in row])
        data = self.queue.getvalue()
        data = data.decode("utf-8")
        data = self.encoder.encode(data)
        self.stream.write(data)
        self.queue.truncate(0)

    def writerows(self, rows):
        for row in rows:
            self.writerow(row)


class toTcases:
    def __init__(self, nodes, db, path):
        self.db = db
        self.n = nodes
        self.path = path
        self.status = 'Parser instantieted'

        self.cols = Storage()
        self.cols.name = None
        self.cols.procedure = None
        self.cols.expected_result = None
        self.cols.expected_time = None
        self.cols.impact_factor = None
        self.cols.dir = None

    def validate_columns(self, fieldnames):
        import difflib
        avgprec = 0.
        for c in self.cols:
            prec = 0.
            for f in fieldnames:
                prec_ = difflib.SequenceMatcher(None, f.lower(), c).ratio()
                if prec_ > prec:
                    prec = prec_
                    self.cols[c] = f
            avgprec = avgprec + prec
        avgprec /= len(self.cols)
        if avgprec < 0.8:
            raise Exception('Columns not matched!')
        else:
            return True

    def parse(self):
        import csv
        db = self.db
        with open(self.path, 'rb') as f:
            reader = csv.DictReader(f)

            self.validate_columns(reader.fieldnames)

            counter = 0
            for r in reader:
                folder = self.create_nodes(r[self.cols.dir])

                db.atms_tcases.update_or_insert(
                    node=folder.id,
                    name=r[self.cols.name],
                    procedure_text=r[self.cols.procedure],
                    expected_result=r[self.cols.expected_result],
                    expected_time=self.parse_time(
                        r[self.cols.expected_time]),
                    weight=r[self.cols.impact_factor])
                counter += 1

            return counter

    def create_nodes(self, dirpath):
        folders = dirpath.split("/")
        db = self.db

        currentf = db(db.atms_nodes.name == folders[0]).select().first()
        if not currentf:
            currentf = self.n.insert_node(None, name=folders[0])

        for f in folders:
            parent = self.n.get_node(currentf)
            query = (db.atms_nodes.lft > parent.lft) & \
                (db.atms_nodes.rgt < parent.rgt)
            query &= (db.atms_nodes.name == f)
            query &= (db.atms_nodes.tree_id == parent.tree_id)
            targetf = db(query).select().first()
            if not targetf:
                targetf = self.n.insert_node(currentf, name=f)
            currentf = targetf

        return currentf

    def parse_time(self, atms_time):
        import re
        groups = re.findall("\d*?[a-z|A-Z]", atms_time)

        counter = 0
        for g in groups:
            if g[-1] == 'M':
                counter += int(g[:-1]) * 14600
            if g[-1] == 'w':
                counter += int(g[:-1]) * 2400
            if g[-1] == 'd':
                counter += int(g[:-1]) * 480
            if g[-1] == 'h':
                counter += int(g[:-1]) * 60
            if g[-1] == 'm':
                counter += int(g[:-1])
        return counter


def dump_tcases(db, user):
    import csv as CSV
    import subprocess
    from datetime import datetime

    FILE = '/tmp/%s_cases_templates_dump.csv' % str(datetime.now())
    with open(FILE, 'wb') as csvfile:
        csv = CSV.writer(csvfile, quoting=CSV.QUOTE_ALL)
        csv.writerow(['Name', 'Procedure', 'Expected result',
                      'Expected time', 'Impact factor', 'Dir'])
        step = 1
        qcount = db.atms_tcases.id.count()
        count = db(db.atms_tcases.id > 0).select(qcount).first()[qcount]
        pages = (count / step) + 1
        for i in range(pages):
            rows = db(db.atms_tcases.id > 0).select(
                limitby=(i * step, i * step + step))
            for r in rows:
                csv.writerow([r.name, r.procedure_text, r.expected_result,
                              r.expected_time, r.weight, r.path])
    with open(FILE, 'rb') as stream:
        db.atms_attachments.insert(
            created_by=user,
            task='dumpTcases',
            type='text/csv/csv',
            size=os.fstat(stream.fileno()).st_size,
            title=os.path.basename(FILE),
            filename=db.atms_attachments.filename.store(stream, FILE))
        db.commit()
    subprocess.call('rm %s' % FILE, shell=True)
