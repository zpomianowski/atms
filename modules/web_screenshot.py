import sys
from PySide.QtCore import Qt, QUrl, QSize, QRect
from PySide.QtWebKit import QWebPage
from PySide.QtGui import QApplication, QImage, QPainter
from PySide.QtNetwork import QNetworkProxy


class Screenshot(QWebPage):
    def __init__(self, link, filename):
        self.app = QApplication(sys.argv, QApplication.GuiServer)
        self.app.setQuitOnLastWindowClosed(True)
        QWebPage.__init__(self)

        self.__loaded = False
        self.__link = link
        self.__filename = filename

        self._W = 1024
        self._H = 1600
        self._w = 400
        self._h = 800

        self.loadFinished.connect(self._render)

    def capture(self):
        self.mainFrame().load(QUrl(self.__link))
        self.app.exec_()

    def _render(self):
        frame = self.mainFrame()
        self.setViewportSize(QSize(self._W, self._H))
        image = QImage(self.viewportSize(), QImage.Format_ARGB32)
        painter = QPainter(image)
        frame.render(painter)
        painter.end()
        image = image.copy(QRect(0, 0, self._W - 16, self._H))
        image = image.scaled(
            self._w, self._h,
            Qt.KeepAspectRatio,
            Qt.SmoothTransformation)

        image.save(self.__filename,
                   'JPG',
                   quality=60)

        self.__loaded = True
        self.app.quit()

if __name__ == "__main__":
    s = None
    if len(sys.argv) == 1:
        s = Screenshot('http://www.wp.com', '/tmp/test.jpg')
    else:
        s = Screenshot(sys.argv[1], sys.argv[2])
    s.capture()
