import os
import sys
PATH = os.path.dirname(
    os.path.abspath(os.path.join(__file__, '../../../..')))
os.chdir(PATH)
sys.path = [PATH] + [p for p in sys.path if not p == PATH]


if __name__ == '__main__':
    from gluon.shell import env
    from tornado.ioloop import IOLoop
    from bokeh.server.server import Server
    from bokeh.command.util import build_single_handler_applications
    APP = 'atms'
    globals().update(env(a='atms', import_models=True))

    path = os.path.join(PATH, 'applications', APP, 'modules', 'bokeh', 'apps')
    apps = [os.path.join(path, d) for d in os.listdir(os.path.join(path))]
    print build_single_handler_applications(apps, {})

    io_loop = IOLoop.current()
    server = Server(
        build_single_handler_applications(apps, {}),
        io_loop=io_loop)
    server.start()
    io_loop.start()
