# -*- coding: utf-8 -*-
from lxml import etree
from gluon import current
from collections import defaultdict
import re

MIMO_MAP = dict(two=2, four=4, eight=8)


def like(param, value):
    return "[starts-with(@%s, '%s')]" % (param, value)


def xml2caps_data(value):
    data = {}
    root = etree.fromstring(value)

    # release
    rel = root.xpath(
        r".//field[@name='lte-rrc.accessStratumRelease']")
    if rel:
        data['release'] = int(
            re.search(r'rel(\d*)', rel[0].get('showname')).group(1))

    # ue categories
    categories = root.xpath(
        r'.//field[re:match(@showname, "^ue-Category(:|-v)")]',
        namespaces={"re": "http://exslt.org/regular-expressions"})
    if categories:
        data['lte_cats'] = []
        for c in categories:
            data['lte_cats'].append(int(c.get('show')))

    # ue categories DL/UL
    tags = ['DL', 'UL']

    for t in tags:
        categories = root.xpath(
            r'.//field[re:match(@showname, "^ue-Category%s")]' % t,
            namespaces={"re": "http://exslt.org/regular-expressions"})
        if categories:
            t = t.lower()
            data['lte_cats_' + t] = []
            for c in categories:
                data['lte_cats_' + t].append(int(c.get('show')))

    # pdn type
    pdn_type = root.xpath(
        r".//field[@name='nas_eps.esm_pdn_type']")
    if pdn_type:
        data['pdn_type'] = re.search(
            '(IPv.*)\s', pdn_type[0].get('showname')).group(1)

    # rohc
    profiles_not_found = 'Not supported'
    profiles = root.xpath(
        r".//field%s/field%s" % (
            like('showname', 'supportedROHC-Profiles'),
            like('name', 'lte-rrc.profile')))
    data['rohc_profiles'] = [profiles_not_found]
    if profiles:
        data['rohc_profiles'] = []
        r = re.compile(r'profile0x(\d*)')
        for p in profiles:
            if int(p.get('show')):
                data['rohc_profiles'].append(
                    int(r.search(p.get('showname')).group(1)))
        if len(data['rohc_profiles']) == 0:
            data['rohc_profiles'] = [profiles_not_found]

    # EUTRA bands
    bands = root.xpath(
        r".//field[@name='lte-rrc.supportedBandListEUTRA']" +
        "//field[@name='lte-rrc.bandEUTRA']")
    if bands:
        data['eutran_bands'] = []
        r = re.compile(r'bandEUTRA:\s(\d*)')
        for b in bands:
            data['eutran_bands'].append(
                int(r.search(b.get('showname')).group(1)))
        data['eutran_bands'] = sorted(data['eutran_bands'])

    # R12 - additional mcss' for DL & UL for each band
    # band -> info
    data['extra_mcs'] = defaultdict(lambda: {
        "UL-64QAM": False,
        "DL-256QAM": False,
        "mimoLayers": None
        })
    extra_mcs = root.xpath(
        r'.//field[re:match(@name, "lte-rrc.SupportedBandEUTRA[-_]v")]' +
        r'/field[re:match(@name, "lte-rrc.(dl|ul)")]',
        namespaces={"re": "http://exslt.org/regular-expressions"})
    if extra_mcs:
        r = re.compile(r"[du]l-(.*)-r")
        for e in extra_mcs:
            if int(e.get('show')) != 0:
                continue
            index = int(e.getparent().getparent().get('show').split(' ')[1])
            item = '-'.join(e.get('showname').upper().split('-')[:-1])
            bindex = data['eutran_bands'][index]
            data['extra_mcs'][bindex][item] = True

    # R10 - MIMO - extra_mcs needed!
    mimo = root.xpath(
        r'.//field%s//field%s//field%s' % (
            like('name', 'lte-rrc.rf_Parameters_v'),
            like('name', 'lte-rrc.supportedBandCombination_r'),
            like('name', 'lte-rrc.BandCombinationParameters_r')))

    if mimo:
        for m in mimo:
            if len(m) != 1:
                continue
            clss = m.xpath(
                './/field%s' % like('name', 'lte-rrc.ca_BandwidthClass'))
            clss = list(set([int(c.get('show')) for c in clss]))

            if len(clss) == 1 and clss[0] == 0:
                bands = m.xpath(
                    './/field%s' % like('name', 'lte-rrc.bandEUTRA_r'))
                bands = [int(r.get('show')) for r in bands]
                mimoLayers = [MIMO_MAP.get(
                    re.search(r'\s(\w*)Layer', r.get('showname')).group(1), -1)
                    for r in m.xpath(
                        './/field%s' % like(
                            'name', 'lte-rrc.supportedMIMO_CapabilityDL_r'))]
                data['extra_mcs'][bands[0]]['mimoLayers'] = mimoLayers[0]

    # Refactor extra_mcs
    for k, v in data['extra_mcs'].items():
        v['band'] = k
    data['extra_mcs'] = sorted(data['extra_mcs'].values(),
                               key=lambda v: v['band'])

    # UTRA bands FDD
    bands = root.xpath(
        r".//field[@name='lte-rrc.supportedBandListUTRA_FDD']" +
        "//field[@name='lte-rrc.SupportedBandUTRA_FDD']")
    if bands:
        data['utran_bands_fdd'] = []
        r = re.compile(r'\((\d*)\)')
        for b in bands:
            data['utran_bands_fdd'].append(
                int(r.search(b.get('showname')).group(1)) + 1)
        data['utran_bands_fdd'] = sorted(data['utran_bands_fdd'])

    # UTRA bands TDD
    bands = root.xpath(
        ".//field%s//field%s" % (
            like('name', 'lte-rrc.supportedBandListUTRA_TDD'),
            like('name', 'lte-rrc.SupportedBandUTRA_TDD')))
    if bands:
        data['utran_bands_tdd'] = []
        r = re.compile(r'\((\d*)\)')
        for b in bands:
            data['utran_bands_tdd'].append(
                int(r.search(b.get('showname')).group(1)) + 1)
        data['utran_bands_tdd'] = sorted(data['utran_bands_tdd'])

    # GERAN bands
    bands = root.xpath(
        r".//field[@name='lte-rrc.supportedBandListGERAN']" +
        "//field[@name='lte-rrc.SupportedBandGERAN']")
    if bands:
        data['geran_bands'] = []
        r = re.compile(r'SupportedBandGERAN:\s(.*)\s\(')
        for b in bands:
            data['geran_bands'].append(
                r.search(b.get('showname')).group(1))

    # HSDPA (HS-DSCH)
    cats = root.xpath(
        r".//field%s" % like('showname', 'hsdsch-physical-layer-category-ext'))
    if cats:
        data['hsdpa_cats'] = []
        for c in cats:
            data['hsdpa_cats'].append(int(c.get('show')))
        data['hsdpa_cats'] = sorted(data['hsdpa_cats'])

    # HSUPA (HS-DSCH)
    cats = root.xpath(
        r".//field%s" % like('name', 'rrc.edch_PhysicalLayerCategory'))
    if cats:
        data['hsupa_cats'] = []
        for c in cats:
            data['hsupa_cats'].append(int(c.get('show')))
        data['hsupa_cats'] = sorted(data['hsupa_cats'])

    # Voice usage
    voice = root.xpath(
        r".//field%s" % like('name', 'gsm_a.gm.gmm.ue_usage_setting'))
    if voice:
        data['voice_usage'] = re.search(
            r":(.*)", voice[0].get('showname')).group(1).strip()

    # Voice domain
    voice = root.xpath(
        r".//field%s" % like(
            'name', 'gsm_a.gm.gmm.voice_domain_pref_for_eutran'))
    if voice:
        data['voice_domain'] = re.search(
            r":(.*)\(", voice[0].get('showname')).group(1).strip()
    return data


def xml2ca_data(value):
    def setif(el, d, k, k2='show'):
        if len(el) > 0:
            value = el[0].get(k2)
            if 'cls' in k:
                value = chr(int(value) + 97).upper()
            d[k] = value

    def get_band_cls(el):
        item = dict(bandEUTRA=el.get('show'))
        ulcls = el.xpath(
            r"..//field%s" % like('showname', 'ca-BandwidthClassUL-r'))
        dlcls = el.xpath(
            r"..//field%s" % like('showname', 'ca-BandwidthClassDL-r'))
        setif(ulcls, item, 'ul_cls', 'show')
        setif(dlcls, item, 'dl_cls', 'show')

        return item

    root = etree.fromstring(value)
    data = []

    # band combination
    items = root.xpath(
        r".//field%s/field/field%s" % (
            like('showname', 'supportedBandCombination-r'),
            like('showname', 'BandCombinationParameters-r')))
    for i in items:
        combination = {}
        combination['items'] = []
        combination['ca'] = []
        combination['uplink'] = []
        combination['group'] = None

        itembands = i.xpath(r".//field%s" % like('showname', 'bandEUTRA'))
        mimoLayers = [MIMO_MAP.get(re.search(
            r'\s(\w*)Layer', r.get('showname')).group(1), -1)
            for r in i.xpath(
                './/field%s//field%s' % (
                    like('name', 'lte-rrc.BandParameters_r'),
                    like('name', 'lte-rrc.supportedMIMO_CapabilityDL_r')))]
        for idx, b in enumerate(itembands):
            bandinfo = get_band_cls(b)
            if 'dl_cls' in bandinfo:
                combination['ca'].append(
                    bandinfo['bandEUTRA'] + bandinfo['dl_cls'])
            if 'ul_cls' in bandinfo:
                combination['uplink'].append(
                    bandinfo['bandEUTRA'] + bandinfo['ul_cls'])
            if len(mimoLayers) > 0:
                bandinfo['mimoLayersDL'] = mimoLayers[idx]
            combination['items'].append(bandinfo)
        combination['ca'] = sorted(
            combination['ca'], key=lambda x: int(x[:-1]))
        combination['uplink'] = ['-'.join(sorted(
            combination['uplink'], key=lambda x: int(x[:-1])))]
        if len(set([int(x[:-1]) for x in combination['ca']])) > 1:
            combination['group'] = 'interband'
        data.append(combination)

    # band sets
    items = root.xpath(
        r".//field%s" % like('showname', 'BandCombinationParametersEx'))

    # no info about sets - assume all supported
    if len(items) == 0:
        def set_default_key(item, k, v):
            item[k] = v
            return item

        data = map(lambda x: set_default_key(x, 'bcs', [0]), data)

    for i in range(len(items)):
        bcs = items[i].xpath(
            r".//field%s" % like(
                'showname', 'supportedBandwidthCombinationSet-r'))
        if len(bcs) == 0:
            data[i]['bcs'] = [0]
        else:
            bcss = []
            binenc = bin(int(bcs[0].get('value'), 16))[2:]
            for b in range(len(binenc)):
                if binenc[b] == "1":
                    bcss.append(b)
            data[i]['bcs'] = bcss

    # combine a-b b-a as a one CA combination
    edata = {}
    for d in data:
        if len(d['items']) == 1:
            # specified more precise key for intrabandc elements
            # so we can distinguish those with the same ca attribute
            # {'items': [{'ul_cls': 'A', 'dl_cls': 'C', 'bandEUTRA': '1'}],
            #  'ca': ['1C'], 'group': None, 'uplink': ['1A'], 'bcs': [0]}
            # {'items': [{'ul_cls': 'C', 'dl_cls': 'C', 'bandEUTRA': '1'}],
            #  'ca': ['1C'], 'group': None, 'uplink': ['1C'], 'bcs': [0]}
            # will respectively get keys: CA_1C1A and CA_1C1C insted of CA_!C
            # thanks to that they will both appear in the final view
            d['ca'][-1] = d['ca'][-1] + d['uplink'][0]
            for x in d['items']:
                if 'ul_cls' not in x:
                    x['ul_cls'] = x['dl_cls']
            if set([x['ul_cls'] for x in d['items']] +
                   [x['dl_cls'] for x in d['items']]) == set(['A']):
                continue

        d['ca'] = 'CA_' + '-'.join(d['ca'])
        if d['ca'].count('-') == 0:
            d['group'] = 'intrabandc'
            d['uplink'] = []
            __cls = d['ca'][-1]
            postfix = 'C' if __cls == 'A' else chr(ord(d['ca'][-1]) + 1)
            d['ca'] = d['ca'][:-1] + postfix
        if d['group'] is None:
            d['group'] = 'intraband'
        if d['ca'] in edata.keys():
            edata[d['ca']]['uplink'] += d['uplink']
            edata[d['ca']]['uplink'] = list(set(edata[d['ca']]['uplink']))
            continue

        edata[d['ca']] = d

    data = sorted(
        edata.values(), key=lambda x: int(x['items'][0]['bandEUTRA']))

    return sorted(data, key=lambda x: x['group'])


def generate_xlsx(id, lang='pl', created_by=None):
    import os
    import uuid
    import tempfile
    import openpyxl as xlsx
    from copy import copy
    from openpyxl.styles import Font
    from openpyxl.styles import Alignment, PatternFill
    from openpyxl.styles.borders import Border, Side
    from datetime import datetime

    db = current.db
    T = current.T
    data = db(db.atms_uecap.id == id).select(
        db.atms_uecap.f_model,
        db.atms_uecap.f_company,
        db.atms_uecap.f_imei,
        db.atms_uecap.f_sw_version,
        db.atms_uecap.f_ca_json,
        db.atms_uecap.f_caps_json).first()

    caps = [
        'releaes',
        'voice_usage',
        'voice_domain',
        'rohc_profiles',
        'lte_cats',
        'lte_cats_dl',
        'lte_cats_ul',
        'eutran_bands',
        'utran_bands_fdd',
        'utran_bands_tdd',
        'geran_bands',
        'hsdpa_cats',
        'hsupa_cats',
        'pdn_type',
        'mimoDL',
        'mimoUL'
        ]
    langd = {
        'release': T('3GPP release', language=lang),
        'rohc_profiles': T('ROHC profiles', language=lang),
        'voice_usage': T('Voice usage', language=lang),
        'voice_domain': T('Voice domain', language=lang),
        'lte_cats': T('LTE categories', language=lang),
        'lte_cats_dl': T('LTE category DL', language=lang),
        'lte_cats_ul': T('LTE category UL', language=lang),
        'eutran_bands': T('EUTRAN bands', language=lang),
        'utran_bands_fdd': T('UTRAN bands FDD', language=lang),
        'utran_bands_tdd': T('UTRAN bands TDD', language=lang),
        'geran_bands': T('GERAN bands', language=lang),
        'hsdpa_cats': T('HSDPA categories', language=lang),
        'hsupa_cats': T('HSUPA categories', language=lang),
        'interband': T('INTERBAND', language=lang),
        'intraband': T('INTRABAND Non contiguous', language=lang),
        'intrabandc': T('INTRABAND Contiguous', language=lang),
        'pdn_type': T('PDN type', language=lang),
        'mimoDL': T('MIMO %s layers', language=lang) % 'DL',
        'mimoUL': T('MIMO %s layers', language=lang) % 'UL'
        }

    wb = xlsx.Workbook()
    ws = wb.active
    ws.title = str(T('Summary', language=lang))

    thin_border = Border(
        left=Side(style='thin'),
        right=Side(style='thin'),
        top=Side(style='thin'),
        bottom=Side(style='thin'))
    font = Font(
        name='Calibri',
        size=11,
        bold=False)
    fontB = copy(font)
    fontB.bold = True

    GFILL = PatternFill(start_color='92D888',
                        end_color='92D888',
                        fill_type='solid')
    RFILL = PatternFill(start_color='e41515',
                        end_color='e41515',
                        fill_type='solid')

    TH_COUNT = 4
    CONTROL_COL = None

    def cell(value, row, col, font=font, rowSpan=1, colSpan=1, center=False):
        c = ws.cell(value=value, row=row, column=col)
        c.font = font
        c.border = thin_border

        if rowSpan != 1 or colSpan != 1:
            ws.merge_cells(start_row=row, start_column=col,
                           end_row=row + rowSpan - 1,
                           end_column=col + colSpan - 1)

        if center:
            c.alignment = Alignment(
                horizontal='center', vertical='center')

        if row < TH_COUNT:
            return
        max_col = len(tuple(ws.rows)[0]) + 1
        if ws.cell(row=row - 1, column=3).fill != GFILL and col == CONTROL_COL:
            for r in range(row, row + rowSpan):
                for i in range(1, max_col):
                    ws.cell(row=r, column=i).fill = GFILL

    def adjust_sizes(ws):
        dims = {}
        for row in ws.rows:
            for cell in row:
                if cell.value:
                    dims[cell.column] = max(
                        (dims.get(cell.column, 0), len(cell.value))) + 1
        for col, value in dims.items():
            ws.column_dimensions[col].width = value

    # basic data -----------------------------------
    CROW = 1
    if created_by:
        author = db.atms_auth_user[created_by]
        author_name = '%s %s' % (author.first_name, author.last_name)
        value = ('%s: %s [%s]' % (
            str(T('Report created by', language=lang)),
            author_name,
            author.email)).decode('utf-8')
        ws.cell(value=value, row=CROW, column=1)
        ws.merge_cells(start_row=CROW, start_column=1,
                       end_row=CROW, end_column=2)
        CROW += 2
    cell(str(T('Tested terminal', language=lang)), CROW, 1, font=fontB)
    cell("%s %s" % (data.f_company, data.f_model), CROW, 2)
    CROW += 1
    cell('IMEI', CROW, 1, font=fontB), cell(data.f_imei, CROW, 2)
    CROW += 1
    cell("SW version", CROW, 1, font=fontB)
    cell(data.f_sw_version, CROW, 2)
    CROW += 2

    if data.f_caps_json:
        for k in caps:
            if k not in data.f_caps_json:
                continue

            v = data.f_caps_json[k]
            cell(str(langd[k]), CROW, 1, font=fontB)
            cell(', '.join([str(x) for x in v]) if isinstance(v, list) else
                 str(v), CROW, 2)
            CROW += 1

    # adjust sizes
    adjust_sizes(ws)

    # Additional Band data -----------------------------------
    extra_mcs = data.f_caps_json.get('extra_mcs', [])
    if len(extra_mcs) > 0:
        langmcs = [
            dict(id='band', tr=T('Band', language=lang)),
            dict(id='DL-256QAM', tr=T('DL-256QAM', language=lang)),
            dict(id='UL-64QAM', tr=T('UL-64QAM', language=lang)),
            dict(id='mimoLayers', tr=T('MIMO layers', language=lang))
            ]

        title = str(T('Additional band info', language=lang)).decode('utf-8')
        ws = wb.create_sheet(title=title)
        ws.column_dimensions['A'].width = 8
        ws.column_dimensions['B'].width = 15
        ws.column_dimensions['C'].width = 15
        ws.column_dimensions['D'].width = 17

        CROW = 1
        CCOL = 1
        cell(title, CROW, CCOL, colSpan=4, center=True, font=fontB)
        for l in langmcs:
            cell(str(l['tr']), CROW + 1, CCOL, center=True, font=fontB)
            CCOL += 1

        CROW += 2
        for item in extra_mcs:
            CCOL = 1
            for l in langmcs:
                value = item[l['id']]
                if isinstance(value, bool):
                    cell(u'\u2714' if value else u'\u274C',
                         CROW, CCOL, center=True, font=fontB)
                    ws.cell(row=CROW, column=CCOL).fill = (
                        GFILL if value else RFILL)
                else:
                    cell(value, CROW, CCOL, center=True)
                CCOL += 1
            CROW += 1

    CONTROL_COL = 3
    # ca data -----------------------------------
    group = None
    if data.f_ca_json:
        for d in data.f_ca_json:
            rowSpan = len(d['items'])
            inter = d['group'] != 'intrabandc' and d['group'] != 'intraband'
            sh = 0 if inter else 1

            if group != d['group']:
                CROW = 1
                ws = wb.create_sheet(title=str(langd[d['group']]))
                ws.column_dimensions['A'].width = 8
                ws.column_dimensions['B'].width = 20
                ws.column_dimensions['C'].width = 20
                ws.column_dimensions['E' if inter else 'D'].width = 20

                cell(str(langd[d['group']]), CROW, 1,
                     colSpan=5 - sh, font=fontB, center=True)
                CROW += 1
                cell(str(T('Configuration', language=lang)),
                     CROW, 1, colSpan=3 - sh, font=fontB, center=True)
                cell(str(T('BCSs', language=lang)),
                     CROW, 4 - sh, rowSpan=2, font=fontB, center=True)
                cell(str(T('MIMO DL layers', language=lang)),
                     CROW, 5 - sh, rowSpan=2, font=fontB, center=True)
                CROW += 1
                cell(str(T('Bands', language=lang)),
                     CROW, 1, font=fontB, center=True)
                cell(str(T('Classes', language=lang)),
                     CROW, 2, font=fontB, center=True)
                if inter:
                    cell(str(T('Uplink', language=lang)),
                         CROW, 3, font=fontB, center=True)
                CROW += 1
            group = d['group']

            uplink_td_flag = False
            bcs_td_flag = False
            for i in d['items']:
                desc = []
                if 'dl_cls' in i:
                    desc.append('DL_' + i['dl_cls'])
                if 'ul_cls' in i and not inter:
                    desc.append('UL_' + i['ul_cls'])

                cell(i['bandEUTRA'], CROW, 1, center=True)
                cell(', '.join(desc), CROW, 2, center=True)
                if inter and not uplink_td_flag:
                    cell(', '.join(d['uplink']), CROW, 3,
                         rowSpan=rowSpan, center=True)
                    uplink_td_flag = True
                if not bcs_td_flag:
                    try:
                        cell(', '.join([str(x) for x in d['bcs']]),
                             CROW, 4 - sh, rowSpan=rowSpan, center=True)
                    except KeyError:
                        cell('0', CROW, 4 - sh, rowSpan=rowSpan, center=True)
                    bcs_td_flag = True
                cell(i.get('mimoLayersDL', 'n/a'), CROW, 5 - sh, center=True)

                # backround color in intrabandc
                # if d['group'] == 'intrabandc':
                #     ws.cell(row=CROW, column=5).fill = whiteFill

                CROW += 1

            ws.cell(row=CROW - 1, column=3).border = thin_border
            ws.cell(row=1, column=1).border = thin_border
            ws.cell(row=2, column=1).border = thin_border
            ws.cell(row=2, column=3).border = thin_border
            if d['group'] == 'interband':
                ws.cell(row=CROW - 1, column=4).border = thin_border
                ws.cell(row=1, column=4).border = thin_border
                ws.cell(row=2, column=2).border = thin_border
                ws.cell(row=2, column=4).border = thin_border
                ws.cell(row=3, column=4).border = thin_border
            if d['group'] == 'intraband' or d['group'] == 'intrabandc':
                ws.cell(row=1, column=2).border = thin_border
                ws.cell(row=1, column=3).border = thin_border
                ws.cell(row=3, column=3).border = thin_border

    absfilename = os.path.join(
        tempfile.gettempdir(),
        'atms_attachments.filename.%s.xlsx' % uuid.uuid4())

    # metadane
    if created_by:
        from openpyxl.worksheet.properties import PageSetupProperties
        wb.security.lockWindows = True
        wb.security.lockStructure = True
        wb.security.lockRevision = True
        author = db.atms_auth_user[created_by]
        author_name = '%s %s' % (author.first_name, author.last_name)
        saut_name = 'CP/LAB Łubinowa M5/'
        signature = '%s [%s]' % (
            author_name,
            datetime.now().strftime('%d/%m/%Y %H:%M'))
        wb.properties.category = 'raport'
        wb.properties.creator = author_name.decode('utf-8')
        wb.properties.keywords = 'ATMS, UECAPs'
        wb.properties.title = 'Podsumowanie UECAPs i CA'
        wb.properties.subject = ('%s %s %s %s' % (
            data.f_model, data.f_company,
            data.f_imei, data.f_sw_version)).decode('utf-8')
        wb.properties.language = lang
        wb.properties.description = ('%s\n%s\n%s\n%s' % (
            saut_name[:-1], author_name, author.email,
            datetime.now().strftime('%d/%m/%Y %H:%M'))).decode('utf-8')
        for sheet in wb.worksheets:
            sheet.oddHeader.alignment = Alignment(wrapText=True)
            sheet.oddHeader.left.text = (saut_name + signature).decode('utf-8')
            sheet.oddHeader.left.size = 12
            sheet.oddHeader.left.font = "Calibri,Bold"
            sheet.oddHeader.left.color = "ed8e00"
            sheet.sheet_properties.pageSetUpPr = PageSetupProperties(
                fitToPage=True, autoPageBreaks=False)

    if current.settings.config != 'prod':
        absfilename = '/tmp/test.xlsx'
        wb.save(absfilename)
        return
    wb.save(absfilename)

    title = ("%s_%s_CAPS_CA_%s_%s.xlsx" % (
        datetime.now().strftime('%Y%m%d'),
        datetime.now().strftime('%H:%m'),
        data.f_company,
        data.f_model)).replace(r" ", "_").replace(r"(", "").replace(r")", "")

    user = db.atms_auth_user(last_name='ATMS').id

    db.atms_attachments.insert(
        refuecap=id,
        created_by=user,
        modified_by=user,
        task='generate_xlsx',
        type='application/xlsx/xlsx',
        size=os.stat(absfilename).st_size,
        title=title,
        filename=open(absfilename, 'r'))

    db.commit()
    os.unlink(absfilename)

if __name__ == '__main__':
    # import traceback
    # generate_xlsx(87)
    db = current.db
    task_parseUECap('null')
