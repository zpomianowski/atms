# -*- coding: utf-8 -*-
from gluon import current
from websocket_messaging import websocket_send
from time import sleep
import random


def dice(number_of_dice):
    for i in range(0, number_of_dice):
        yield random.randint(1, 6)


def task_performGameAction(attacker, victims, actions):
    msg = []
    db = current.db
    system_id = db.atms_auth_user(last_name='ATMS').id
    attacker = db.atms_auth_user[int(attacker)]
    victims = db(db.atms_auth_user.id.belongs(victims)).select()
    for victim in victims:
        sleep(1)
        if 'steal' in actions:
            attacker_p = 0
            victim_p = 0
            for i in dice(2):
                attacker_p += i
            for i in dice(3):
                victim_p += i
            if attacker_p > victim_p:
                won_cash = victim.coins * \
                    random.gauss(0.5, 0.1) * float(attacker_p) / 12
                won_cash = min(won_cash, attacker.coins)
                if won_cash == 0:
                    continue
                attacker.coins += won_cash
                victim.coins -= won_cash
                attacker.update_record()
                victim.update_record()
                db.commit()
                if system_id is None:
                    continue
                msg.append("Okradłeś %s na %.2f$" %
                           (victim.first_name, won_cash))
                notify("Zostałeś okradziony! Odebrano Ci %.2f$. " % won_cash,
                       system_id, [victim.id, system_id])
            else:
                msg.append("Nie udało Ci się okraść %s. " % victim.first_name)
                notify("Ktoś próbował Cię okraść! ",
                       system_id, [victim.id, system_id])
    if system_id and len(msg) > 0:
        notify("[[NEWLINE]]".join(msg), system_id, [attacker.id, system_id])


def notify(msg, user, members):
    db = current.db
    db.atms_comments.insert(
        channel='users',
        msg=msg,
        members=members,
        created_by=user)
    db.commit()
