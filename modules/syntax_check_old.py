import ast

forbidden_functions = set([
    # #math library
    # 'acos', 'acosh', 'asin', 'asinh', 'atan', 'atan2', 'atanh',
    # 'ceil', 'copysign', 'cos', 'cosh', 'degrees', 'e', 'erf',
    # 'erfc', 'exp', 'expm1', 'fabs', 'factorial', 'floor', 'fmod',
    # 'frexp', 'fsum', 'gamma', 'hypot', 'isinf', 'isnan', 'ldexp',
    # 'lgamma', 'log', 'log10', 'log1p', 'modf', 'pi', 'pow', 'radians',
    # 'sin', 'sinh', 'sqrt', 'tan', 'tanh', 'trunc',
    # #builtins
    # 'abs', 'max', 'min', 'range', 'xrange'
])

forbidden_node_types = set([
    'imp', 'current'
    # #Meta
    # 'Module', 'Assign', 'Expr',
    # #Control
    # 'For', 'If', 'Else',
    # #Data
    # 'Store', 'Load', 'AugAssign', 'Subscript',
    # #Datatypes
    # 'Num', 'Tuple', 'List',
    # #Operations
    # 'BinOp', 'Add', 'Sub', 'Mult', 'Div', 'Mod', 'Compare'
])

unsafe_names = set([
    'TestCase'
])


class Visit(ast.NodeVisitor):
    def visit_ClassDef(self, node):
        print [n.id for n in node.bases]


class SyntaxChecker(ast.NodeVisitor):
    def __init__(self, file_path):
        src = open(file_path, 'r').read()
        self.check(src)

    def check(self, syntax):
        tree = ast.parse(syntax)
        self.visit(tree)

    class Visit(ast.NodeVisitor):
        def visit_ClassDef(self, node):
            print [n.id for n in node.bases]

    def visit_Call(self, node):
        atts = vars(node)
        if 'args' in atts:
            for arg in atts['args']:
                ast.NodeVisitor.generic_visit(self, arg)
        try:
            name = vars(node.func)['attr']
            # print name
            if name in forbidden_functions:
                raise SyntaxError(
                    "%s is not an allowed function!" % node.func.id)
            else:
                ast.NodeVisitor.generic_visit(self, node)
        except:
            pass

    # def visit_Name(self, node):
    #     try:
    #         eval(node.id)
    #     except NameError:
    #         ast.NodeVisitor.generic_visit(self, node)
    #     else:
    #         if node.id in unsafe_names or node.id in forbidden_functions:
    #             raise SyntaxError("%s is a reserved name!" % node.id)
    #         else:
    #             ast.NodeVisitor.generic_visit(self, node)

    # def generic_visit(self, node):
    #     v = None
    #     try:
    #         v = vars(node)
    #     except:
    #         pass
    #     if v:
    #         print v
    #         if 'name' in v:
    #             if v['name'] in forbidden_node_types:
    #                 raise SyntaxError("%s is not allowed!" % v['name'])
    #     if type(node).__name__ in forbidden_node_types:
    #         raise SyntaxError("%s is not allowed!" % type(node).__name__)
    #     else:
    #         ast.NodeVisitor.generic_visit(self, node)


def check(filename):
    try:
        SyntaxChecker(filename)
    except Exception as e:
        line = '?'
        desc = ''
        try:
            desc = "%s\n\n" % str(e)
            desc += e.text
            line = e.lineno
        finally:
            return (line, desc)


if __name__ == '__main__':
    text = open('/tmp/debug_caseId6_duts1_3/test_script.py').read()
    tree = ast.parse(text)
    Visit().visit(tree)
    # try:
    #     x = SyntaxChecker('/tmp/debug_caseId6_duts1_3/test_script.py')
    # except Exception as e:
    #     print e
