from gluon import Field, IS_NOT_EMPTY, IS_IN_SET, IS_NOT_IN_DB
from gluon import IS_INT_IN_RANGE, IS_DATETIME, IS_IN_DB
from gluon.utils import web2py_uuid
from gluon.scheduler import Scheduler as sch
from gluon.scheduler import *


class Scheduler(sch):
    def __init__(self, *args, **kwargs):
        self.__ticker_group = None
        if 'ticker_group' in kwargs:
            self.__ticker_group = kwargs['ticker_group']
            del kwargs['ticker_group']
        super(Scheduler, self).__init__(*args, **kwargs)

    def define_tables(self, db, migrate):
        """Define Scheduler tables structure."""
        from pydal.base import DEFAULT
        logger.debug('defining tables (migrate=%s)', migrate)
        now = self.now
        db.define_table(
            'scheduler_task',
            Field('application_name', requires=IS_NOT_EMPTY(),
                  default=None, writable=False),
            Field('task_name', default=None),
            Field('group_name', default='main'),
            Field('status', requires=IS_IN_SET(TASK_STATUS),
                  default=QUEUED, writable=False),
            Field('function_name',
                  requires=IS_IN_SET(sorted(self.tasks.keys()))
                  if self.tasks else DEFAULT),
            Field('uuid', length=255,
                  requires=IS_NOT_IN_DB(db, 'scheduler_task.uuid'),
                  unique=True, default=web2py_uuid),
            Field('args', 'text', default='[]', requires=TYPE(list)),
            Field('vars', 'text', default='{}', requires=TYPE(dict)),
            Field('enabled', 'boolean', default=True),
            Field('start_time', 'datetime', default=now,
                  requires=IS_DATETIME()),
            Field('next_run_time', 'datetime', default=now),
            Field('stop_time', 'datetime'),
            Field('repeats', 'integer', default=1, comment="0=unlimited",
                  requires=IS_INT_IN_RANGE(0, None)),
            Field('retry_failed', 'integer', default=0, comment="-1=unlimited",
                  requires=IS_INT_IN_RANGE(-1, None)),
            Field('period', 'integer', default=60, comment='seconds',
                  requires=IS_INT_IN_RANGE(0, None)),
            Field('prevent_drift', 'boolean', default=False,
                  comment='Exact start_times between runs'),
            # Field('cronline', default=None,
            #       comment='Discard "period", use this cron expr instead',
            #       requires=IS_EMPTY_OR(IS_CRONLINE())),
            Field('timeout', 'integer', default=60, comment='seconds',
                  requires=IS_INT_IN_RANGE(1, None)),
            Field('sync_output', 'integer', default=0,
                  comment="update output every n sec: 0=never",
                  requires=IS_INT_IN_RANGE(0, None)),
            Field('times_run', 'integer', default=0, writable=False),
            Field('times_failed', 'integer', default=0, writable=False),
            Field('last_run_time', 'datetime', writable=False, readable=False),
            Field('assigned_worker_name', default='', writable=False),
            # atms mod
            Field('devices', 'list:reference atms_devices'),
            on_define=self.set_requirements,
            migrate=self.__get_migrate('scheduler_task', migrate),
            format='(%(id)s) %(task_name)s')

        db.define_table(
            'scheduler_run',
            Field('task_id', 'reference scheduler_task'),
            Field('status', requires=IS_IN_SET(RUN_STATUS)),
            Field('start_time', 'datetime'),
            Field('stop_time', 'datetime'),
            Field('run_output', 'text'),
            Field('run_result', 'text'),
            Field('traceback', 'text'),
            Field('worker_name', default=self.worker_name),
            migrate=self.__get_migrate('scheduler_run', migrate)
            )

        db.define_table(
            'scheduler_worker',
            Field('worker_name', length=255, unique=True),
            Field('first_heartbeat', 'datetime'),
            Field('last_heartbeat', 'datetime'),
            Field('status', requires=IS_IN_SET(WORKER_STATUS)),
            Field('is_ticker', 'boolean', default=False, writable=False),
            Field('group_names', 'list:string', default=self.group_names),
            Field('worker_stats', 'json'),
            migrate=self.__get_migrate('scheduler_worker', migrate)
            )

        db.define_table(
            'scheduler_task_deps',
            Field('job_name', default='job_0'),
            Field('task_parent', 'integer',
                  requires=IS_IN_DB(db, 'scheduler_task.id', '%(task_name)s')
                  ),
            Field('task_child', 'reference scheduler_task'),
            Field('can_visit', 'boolean', default=False),
            migrate=self.__get_migrate('scheduler_task_deps', migrate)
            )

        if migrate is not False:
            db.commit()

    def being_a_ticker(self):
        db = self.db_thread
        sw = db.scheduler_worker
        my_name = self.worker_name
        all_active = db(
            (sw.worker_name != my_name) & (sw.status == ACTIVE)
        ).select(sw.is_ticker, sw.worker_name)
        ticker = all_active.find(lambda row: row.is_ticker is True).first()
        not_busy = self.w_stats.status == ACTIVE
        if not ticker:
            if not_busy:
                query =  sw.worker_name == my_name
                if self.__ticker_group:
                    query &= sw.group_names.contains(self.__ticker_group)
                    logger.info("TICKER GROUP: %s" % self.__ticker_group)
                db(query).update(is_ticker=True)
                db(sw.worker_name != my_name).update(is_ticker=False)
                db(~sw.group_names.contains('main')).update(is_ticker=False)
                logger.info("TICKER: I'm a ticker")
            else:
                # I'm busy
                if len(all_active) >= 1:
                    # so I'll "downgrade" myself to a "poor worker"
                    db(sw.worker_name == my_name).update(is_ticker=False)
                else:
                    not_busy = True
            db.commit()
            return not_busy
        else:
            logger.info(
                "%s is a ticker, I'm a poor worker" % ticker.worker_name)
            return False

    def send_heartbeat(self, counter):
        """Testing this method behaviour!
        """
        if not self.db_thread:
            logger.debug('thread building own DAL object')
            self.db_thread = DAL(
                self.db._uri, folder=self.db._adapter.folder)
            self.define_tables(self.db_thread, migrate=False)
        try:
            db = self.db_thread
            sw, st = db.scheduler_worker, db.scheduler_task
            now = self.now()
            # record heartbeat
            mybackedstatus = db(sw.worker_name == self.worker_name).select().first()
            if not mybackedstatus:
                sw.insert(status=ACTIVE, worker_name=self.worker_name,
                          first_heartbeat=now, last_heartbeat=now,
                          group_names=self.group_names,
                          worker_stats=self.w_stats)
                self.w_stats.status = ACTIVE
                self.w_stats.sleep = self.heartbeat
                mybackedstatus = ACTIVE
                # test
                db.commit()
            else:
                mybackedstatus = mybackedstatus.status
                if mybackedstatus == DISABLED:
                    # keep sleeping
                    self.w_stats.status = DISABLED
                    logger.debug('........recording heartbeat (%s)',
                                 self.w_stats.status)
                    db(sw.worker_name == self.worker_name).update(
                        last_heartbeat=now,
                        worker_stats=self.w_stats)
                    # test
                    db.commit()
                elif mybackedstatus == TERMINATE:
                    self.w_stats.status = TERMINATE
                    logger.debug("Waiting to terminate the current task")
                    self.give_up()
                elif mybackedstatus == KILL:
                    self.w_stats.status = KILL
                    self.die()
                    # test
                    db.commit()
                    return
                else:
                    if mybackedstatus == STOP_TASK:
                        logger.info('Asked to kill the current task')
                        self.terminate_process()
                    logger.debug('........recording heartbeat (%s)',
                                 self.w_stats.status)
                    db(sw.worker_name == self.worker_name).update(
                        last_heartbeat=now, status=ACTIVE,
                        worker_stats=self.w_stats)
                    self.w_stats.sleep = self.heartbeat  # re-activating the process
                    if self.w_stats.status != RUNNING:
                        self.w_stats.status = ACTIVE
                    # test
                    db.commit()

            self.do_assign_tasks = False
            if counter % 5 == 0 or mybackedstatus == PICK:
                try:
                    # delete dead workers
                    # redefine dead worker
                    expiration = now - datetime.timedelta(
                        seconds=self.heartbeat * 6)
                    departure = now - datetime.timedelta(
                        seconds=self.heartbeat * 6 * 15)
                    logger.debug(
                        '    freeing workers that have not sent heartbeat')
                    dead_workers = db(
                        ((sw.last_heartbeat < expiration) & (sw.status == ACTIVE)) |
                        ((sw.last_heartbeat < departure) & (sw.status != ACTIVE))
                    )
                    dead_workers_name = dead_workers._select(sw.worker_name)
                    db(
                        (st.assigned_worker_name.belongs(dead_workers_name)) &
                        (st.status == RUNNING)
                    ).update(assigned_worker_name='', status=QUEUED)
                    dead_workers.delete()
                    try:
                        self.is_a_ticker = self.being_a_ticker()
                    except:
                        logger.error('Error coordinating TICKER')
                        raise
                    if self.w_stats.status == ACTIVE:
                        self.do_assign_tasks = True
                except Exception as err:
                    logger.error('Error cleaning up %s' % str(err))
            db.commit()
        except:
            logger.error('Error retrieving status')
            db.rollback()
        self.adj_hibernation()
        self.sleep()
