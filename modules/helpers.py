# -*- coding: utf-8 -*-
from gluon import *
from gluon.sqlhtml import FormWidget


def gen_menu(div, menu_list, elid=None):
    ul = UL(_id=elid)
    div.append(ul)
    for item in menu_list:
        idx = item[0]
        if idx == 'login':
            k = current.T('Login')
        elif idx == 'register':
            k = current.T('Register')
        elif idx == 'request_reset_password':
            k = current.T('Lost password?')
        elif idx == 'retrieve_username':
            k = current.T('Forgot username?')
        elif idx == 'logout':
            k = current.T('Logout')
        elif idx == 'profile':
            k = current.T('Profile')
        elif idx == 'change_password':
            k = current.T('Password')
        elif idx == 'teams':
            k = current.T('Teams')
        elif idx == 'extra':
            menu_item = LI(item[1])
        else:
            k = idx

        if len(item) > 2:
            href = '#' if item[2] is None else item[2]
            menu_item = LI(A(k, _href=href))
            if len(item) > 3:
                if len(item[3]) > 0:
                    menu_item = LI(A(k, _href=href, _class='ic ic-left'))
                    div2 = DIV(_class='mp-level')
                    div2.append(A(current.T('Back'), _class='mp-back'))
                    menu_item.append(gen_menu(div2, item[3]))
        ul.append(menu_item)
    return div


def MENU(menu):
    div = DIV(_class='mp-level')
    return gen_menu(div, menu)


class UploadWidget2(FormWidget):
    _class = 'upload'

    DEFAULT_WIDTH = '150px'
    ID_DELETE_SUFFIX = '__delete'
    GENERIC_DESCRIPTION = 'file'
    DELETE_FILE = 'delete'

    @classmethod
    def widget(cls, field, value, download_url=None, **attributes):
        """Generate a INPUT file tag.

        Optionally provides an A link to the file, including a checkbox so
        the file can be deleted.
        All is wrapped in a DIV.

        see also: :meth:`FormWidget.widget`

        :param download_url: Optional URL to link to the file (default = None).
        """
        default = dict(_type='file',)
        attr = cls._attributes(field, default, **attributes)

        inp = INPUT(**attr)
        pb = None
        pb = DIV(DIV(
            _class="bar", _style="width:0%;"),
            _class="progress progress-striped active")

        if download_url and value:
            if callable(download_url):
                url = download_url(value)
            else:
                url = download_url + '/' + value
            (br, image) = ('', '')
            if UploadWidget2.is_image(value):
                br = BR()
                image = IMG(_src=url, _width=cls.DEFAULT_WIDTH)

            requires = attr["requires"]
            if requires == [] or isinstance(requires, IS_EMPTY_OR):
                inp = DIV(inp, pb, DIV('[',
                          A(T(
                              UploadWidget2.GENERIC_DESCRIPTION), _href=url),
                          '|',
                          INPUT(_type='checkbox',
                                _name=field.name + cls.ID_DELETE_SUFFIX,
                                _id=field.name + cls.ID_DELETE_SUFFIX),
                          LABEL(T(cls.DELETE_FILE),
                                _for=field.name + cls.ID_DELETE_SUFFIX,
                                _style='display: inline'),
                          ']'), br, image)
            else:
                inp = DIV(inp, '[',
                          A(cls.GENERIC_DESCRIPTION, _href=url),
                          ']', br, image)
        return inp

    @classmethod
    def represent(cls, field, value, download_url=None):
        """How to represent the file.

        - with download url and if it is an image: <A href=...><IMG ...></A>
        - otherwise with download url: <A href=...>file</A>
        - otherwise: file

        :param field: the field
        :param value: the field value
        :param download_url: url for the file download (default = None)
        """
        inp = cls.GENERIC_DESCRIPTION

        if download_url and value:
            if callable(download_url):
                url = download_url(value)
            else:
                url = download_url + '/' + value
            if cls.is_image(value):
                inp = IMG(_src=url, _width=cls.DEFAULT_WIDTH)
            inp = A(inp, _href=url)

        return inp

    @staticmethod
    def is_image(value):
        """Try to check if the filename provided references to an image.

        Checking is based on filename extension. Currently recognized:
           gif, png, jp(e)g, bmp

        :param value: filename
        """
        extension = value.split('.')[-1].lower()
        if extension in ['gif', 'png', 'jpg', 'jpeg', 'bmp']:
            return True
        return False
