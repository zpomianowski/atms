# -*- coding: utf-8 -*-
import traceback
from tester import errors
try:
    from applications.atms.modules.ixiawebapi.ixia.webapi import webApi
    from applications.atms.modules.ixiawebapi import ixchariotApi
except:
    desc = traceback.format_exc()
    raise errors.Error(
        'IXIA webapi not installed! '
        'Please read README.md and install proper version. '
        'Try again: %s' % desc)


class Client(object):
    def __init__(self):
        from gluon.contrib.appconfig import AppConfig
        conf = AppConfig(reload=True)
        self.__server = conf.take('ixia.server')
        self.__api_version = conf.take('ixia.api_version')
        self.__username = conf.take('ixia.username')
        self.__password = conf.take('ixia.password')
        self.__api_key = conf.take('ixia.api_key')

        self._api = webApi.connect(
            self.__server, self.__api_version, None,
            self.__username, self.__password)
        self._session = self._api.createSession("ixchariot")

        self._session.startSession()

    def release(self):
        self._session.stopSession()
        self._session.httpDelete()
