## IXIA API
Put here extracted content of the IXIA's *Python API Library* with version __~1.3.0.70__.
API is not put here intentionally, because it is a prioprietary library.

You will find library in the IxChariot's web GUI:
* connect to M5 Lab network (10.17.199.0/24)
* login to __IxChariot__ > *ixia.lab.redefine.pl*
* Click settings and go to > Help > *Python API Library*
