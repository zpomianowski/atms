#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""Konfiguracja dla Celery."""

import redis
from gluon import current
from gluon.shell import env
from celery import Celery


REDIS_CLIENT = redis.Redis(host='10.5.133.46')


def make_celery(app):
    u"""Dodanie do env środowiska web2py."""
    celery = Celery(
        'celery_atms',
        broker='amqp://10.5.133.46',
        backend='amqp://10.5.133.46')
    TaskBase = celery.Task

    class ContextTask(TaskBase):
        abstract = True

        def __call__(self, *args, **kwargs):
            _env = env(a=app, import_models=True)
            globals().update(_env)
            return TaskBase.__call__(self, *args, **kwargs)

    celery.Task = ContextTask
    return celery


def only_one(function=None, key="", timeout=None):
    """Enforce only one celery task at a time."""
    def _dec(run_func):
        """Decorator."""
        def _caller(*args, **kwargs):
            """Caller."""
            ret_value = None
            have_lock = False
            lock = REDIS_CLIENT.lock(key, timeout=timeout)
            try:
                have_lock = lock.acquire(blocking=False)
                if have_lock:
                    ret_value = run_func(*args, **kwargs)
            finally:
                if have_lock:
                    lock.release()

            return ret_value

        return _caller

    return _dec(function) if function is not None else _dec


celery = make_celery('atms')
