 # -*- conding: utf-8 -*-
from lxml import etree
import csv
from gluon import current
from gluon.storage import Storage
import re
import os

CAT_24GHZ = '24GHZ'
CAT_5GHZ = '5GHZ'

CLASSES = ['AC5300', 'AC3200', 'AC3100', 'AC2600', 'AC2350',
           'AC1900', 'AC1750', 'AC1600', 'AC1300', 'AC1200', 'AC750',
           'AC580', 'N900', 'N750', 'N750', 'N600', 'N450', 'N300',
           'N150', 'G54']
L1 = [1000, 600, 1000, 800, 600, 600, 450, 300, 450, 300, 300, 150, 450,
      450, 300, 300, 450, 300, 150, 54]
L2 = [2165, 1300, 2165, 1733, 1733, 1300, 1300, 1300, 867, 867, 433,
      433, 450, 300, 450, 300, 450, 300, 150, 54]


def like(param, value):
    return "[starts-with(@%s, '%s')]" % (param, value)


def xml2caps_data(gdata, value, tag):
    if not value:
        return

    data = Storage()
    root = etree.fromstring(value)

    # with open('/tmp/test.xml', 'wb') as f:
    #     f.write(value)

    # MCSs for N standard
    tags = ['rx', 'tx', 'basic']
    data.ht = Storage()

    def get_ht_mcs(_tag):
        tag = _tag
        rule = r"[(@name='wlan.ht.mcsset') and (contains(@showname, '%s'))]"
        if _tag != 'basic':
            rule = rule % ': MCS'
        else:
            tag = 'rx'
            rule = rule % 'Basic'
        ht = root.xpath(r".//field%s" % rule)
        if ht:
            l = []
            for idx, d in enumerate(ht[0].xpath(
                    r"./*[@name='wlan.ht.mcsset.%sbitmask']/*" % tag)):
                # we are looking for up to 32 sets
                # >32 has to be implemented differntly
                if idx == 4:
                    break
                l.append(int(d.get('value'), 16) << idx * 8)
            _tag = 'mcs_%s' % _tag
            data.ht[_tag] = []
            if len(l) == 0:
                return
            for idx, d in enumerate(
                [int(m) for m in
                 list(bin(reduce(lambda x, y: x + y, l)))[:1:-1]]):
                if d:
                    data.ht[_tag].append('MCS%d' % idx)

    for t in tags:
        get_ht_mcs(t)

    # HT TX not defined - fallback
    txdefined = int(root.xpath(
        r".//field[@name='wlan.ht.mcsset.txsetdefined']")[0].get(
        'value')) != 0
    txrxequal = int(root.xpath(
        r".//field[@name='wlan.ht.mcsset.txrxmcsnotequal']")[0].get(
        'value')) == 0
    data.ht.txdefined = txdefined
    data.ht.txrxequal = txrxequal
    if not txdefined and txrxequal:
        data.ht.mcs_tx = data.ht.mcs_rx

    # MCSs for AC - VHT (Very High Throughput)
    # map: key -> x; x = MCSs + 1, beacause loop starts at 0
    vht_sup_map = Storage({
        0: 8,
        1: 9,
        2: 10
    })
    tags = ['rx', 'tx', 'basic']
    data.vht = Storage()

    def get_vht_mcs(tag):
        rule = r"[@name='wlan.vht.mcsset.%smcsmap']" % tag
        if tag == 'basic':
            rule = r"[@name='wlan.vht.op.basicmcsmap']"
        vht = root.xpath(r".//*%s" % rule)
        if vht:
            tag = 'mcs_%s' % tag
            data.vht[tag] = {}
            for idx, d in enumerate(vht[0].xpath('./*')):
                data.vht[tag]['ss%d' % (idx + 1)] = \
                    ['MCS%d' % i for i in
                     range(vht_sup_map.get(int(d.get('value')), 0))]
    for t in tags:
        get_vht_mcs(t)

    # supported rates
    tags = ['supported_rates', 'extended_supported_rates']
    data.supported_rates = {}

    def get_rates(tag):
        rates = root.xpath(
            r".//field[@name='wlan.%s']" % tag)
        if rates:
            # data.supported_rates[tag] = []
            for r in rates:
                rate = int(re.search(
                    r"Rates:\s+(\d+)", r.get('showname')).group(1))
                mandatory = 'B' in r.get('showname')
                data.supported_rates[str(rate).zfill(3)] = dict(
                    rate=rate, mandatory=mandatory)

    for t in tags:
        get_rates(t)

    # HT support channel width
    data.ht.support = '20 MHz'
    ht_support = root.xpath(
        r".//field[@name='wlan.ht.capabilities.width']")
    if ht_support:
        if int(ht_support[0].get('value')) == 1:
            data.ht.support = '20/40 MHz'

    # Short GI
    short_gi = {}
    tags = [20, 40, 80, 160]

    def get_gi(tag):
        gi = (root.xpath(
            r".//field[@name='wlan.ht.capabilities.short%d']" % tag) +
            root.xpath(
                r".//field[@name='wlan.vht.capabilities.short%d']" % tag))
        if gi:
            for g in gi:
                desc = re.search(
                    r"for\s(\d+.*?):",
                    g.get('showname')).group(1).replace('and', '&')
                short_gi[desc] = (int(g.get('value')) > 0)
    data.short_gi = short_gi

    for t in tags:
        get_gi(t)

    # Country settings
    country = Storage()
    country.code = None
    ccode = root.xpath(
        r".//field[@name='wlan.country_info.code']")
    if ccode:
        country.code = ccode[0].get('show')

    c_extra = root.xpath(
        r".//field[@name='wlan.country_info.fnm']")
    if c_extra:
        for d in c_extra[0].xpath('./*'):
            key = d.get('name').split('.')[-1]
            if key == 'fcn':
                country.first_channel_nb = int(d.get('show'))
            if key == 'nc':
                country.channel_nbs = int(d.get('show'))
            if key == 'mtpl':
                country.max_tx_pwr = int(d.get('show'))
    data.country = dict(country)

    # HT Max A-MSDU length
    amsdu = root.xpath(r".//field[@name='wlan.ht.capabilities.amsdu']")
    if amsdu:
        data.ht.amsdu = int(re.search(
            'length:\s+(\d+)', amsdu[0].get('showname')).group(1))

    # HT Capabilities: Maximum Rx A-MPDU Length
    ampdu = root.xpath(r".//field[@name='wlan.ht.ampduparam.maxlength']")
    if ampdu:
        data.ht.ampdu = int(re.search(
            '(\d+)\[Bytes', ampdu[0].get('showname')).group(1))

    # HT Capabilities: MPDU Density
    mpdudensity = root.xpath(
        r".//field[@name='wlan.ht.ampduparam.mpdudensity']")
    if mpdudensity:
        data.ht.mpdudensity = int(mpdudensity[0].get('value')) - 1

    # VHT Capabilities: Maximum MPDU Length
    maxmpdulength = root.xpath(
        r".//field[@name='wlan.vht.capabilities.maxmpdulength']")
    if maxmpdulength:
        data.vht.maxmpdulength = int(re.search(
            r":(.*)\(",
            maxmpdulength[0].get(
                'showname')).group(1).replace(',', '').replace(' ', ''))

    # VHT Capabilities: Supported Channel Width Set
    supportedchanwidthset = root.xpath(
        r".//field[@name='wlan.vht.capabilities.supportedchanwidthset']")
    if supportedchanwidthset:
        value = int(supportedchanwidthset[0].get('value'))
        if value == 0:
            data.vht.support = '80MHz'
        if value == 1:
            data.vht.support = '160MHz'
        if value == 2:
            data.vht.support = '160/80+80MHz'

    # VHT Capabilities: Max A-MPDU Length
    maxampdu = root.xpath(
        r".//field[@name='wlan.vht.capabilities.maxampdu']")
    if maxampdu:
        data.ht.maxampdu = int(re.search(
            r":(.*)\(",
            maxampdu[0].get(
                'showname')).group(1).replace(',', '').replace(' ', ''))

    gdata[tag] = data
    return gdata


def get_wifi_cat(gdata, band=CAT_24GHZ, cat='ht', csv_file='wifi_mcsinfo.csv'):
    csv_file = os.path.join(
        os.getcwd(), 'applications/atms/modules',
        csv_file)
    data = gdata.get(band, {}).get(cat, {})
    if len(data) == 0 or cat not in gdata[band]:
        return
    MCS_ID = '%s_mcs_index' % cat
    mcs_data = Storage()
    with open(csv_file, 'rb') as f:
        _mcs_data = csv.reader(f)
        keys = _mcs_data.next()
        for row in _mcs_data:
            item = Storage(dict(zip(keys, row)))
            if item[MCS_ID] == '':
                continue
            if cat == 'ht':
                mcs_data['MCS' + item[MCS_ID]] = item
            else:
                key = 'MCS' + item[MCS_ID]
                if key not in mcs_data:
                    mcs_data[key] = Storage()
                mcs_data[key]['ss%s' % item.spatial_streams] = item

    maxsupport = '20MHz'
    for d in [40, 80, 160]:
        if str(d) in data.support:
            maxsupport = '%sMHz' % d
    gi = 'gi'
    for k, v in gdata[band].short_gi.items():
        if v and maxsupport in k:
            gi = 'sgi'
    support = gi + maxsupport

    maxrate = 0
    if cat == 'ht':
        maxrate = float(mcs_data[data.mcs_tx[-1]][support])
    else:
        vht_spatial_streams = 0
        max_mcs_index = 99
        max_mcs = 'MCS0'
        for key, value in data.mcs_tx.items():
            if len(value) > 0:
                vht_spatial_streams += 1
                max_mcs_index = min(max_mcs_index, len(value) - 1)
                max_mcs = value[max_mcs_index]
        ss = 'ss%d' % vht_spatial_streams
        maxrate = float(mcs_data[max_mcs][ss][support])
    data.maxrate = maxrate


def get_class(gdata):
    gdata['class'] = '-'
    try:
        maxrate24 = gdata[CAT_24GHZ].ht.maxrate
    except KeyError:
        return

    try:
        maxrate5 = (gdata[CAT_5GHZ].vht.maxrate if len(gdata[CAT_5GHZ].vht) > 0
                    else gdata[CAT_5GHZ].ht.maxrate)
    except KeyError:
        maxrate5 = None

    if maxrate24 and not maxrate5:
        gdata['class'] = min(
            zip(CLASSES[16:], L1[16:]),
            key=lambda x: abs(x[1] - maxrate24))[0]
        return
    gdata['class'] = min(
        zip(CLASSES[:-4], zip(L1[:-4], L2[:-4])),
        key=lambda x: abs(x[1][0] - maxrate24) + abs(x[1][1] - maxrate5))[0]


if __name__ == '__main__':
    task_parseWiFiCap(None)
