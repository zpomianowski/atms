#!/usr/bin/env python
# -*- coding: utf-8 -*-
from gluon import current
from tester.base import AbstractTestProc


class Report(object):
    DEV_FORMAT = '%(manufacturer)s %(model)s'

    def __init__(self, lang, case_id=None):
        self.lang = lang
        if not case_id:
            return
        db = current.db
        self.testcase = db.atms_cases[case_id]
        self.devs = [
            Report.DEV_FORMAT % d for d in
            db(db.atms_devices.id.belongs(self.testcase.auto_devs)).select()]

    @staticmethod
    def notReady(title, lang):
        body = "## %(title)s\n%(sorry)s" % dict(
            title=title,
            sorry=current.T('The test is currently being performed...',
                            language=lang))
        return body

    def getTags(self, tags=[], duts=None):
        db = current.db
        tags.append(AbstractTestProc.TAG)
        if duts:
            duts = [
                Report.DEV_FORMAT % d for d in
                db(db.atms_devices.id.belongs(duts)).select()]
        else:
            duts = self.devs
        return tags + duts

    def getPartial(self, result, duts):
        db = current.db
        duts = [
            Report.DEV_FORMAT % d for d in
            db(db.atms_devices.id.belongs(duts)).select()]
        body = """dupcia
        """
        return '\n'.join(filter(str.strip, body.splitlines()))

    def getMaster(self):
        context = dict(
            result='',
            title='',
            now='',
            tab='',
            att_list='')
        return current.response.render('wiki_report.markmin', context)
