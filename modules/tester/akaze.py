#!/usr/bin/env python
# -*- coding: utf-8 -*-
import cv2
import numpy as np

COUNTOUR_TH = .1
THRESHOLDS = [.1, .5]
MARGIN = 200
MIN_MATCH_COUNT = 4
RANSAC_REPROJ_TH = 5.0


def kaze_match(train_path, tmplt_path, ths=None, debug=False):
    THS = THRESHOLDS
    if isinstance(ths, list):
        THS = ths
    elif str(ths).isdigit():
        THS = [ths]

    # load the image and convert it to grayscale
    train = cv2.imread(train_path)
    tmplt = cv2.imread(tmplt_path)
    (h, w, _) = tmplt.shape
    tmplt = cv2.copyMakeBorder(
        tmplt, MARGIN, MARGIN, MARGIN, MARGIN,
        cv2.BORDER_CONSTANT, value=[0, 0, 0])
    gray1 = cv2.cvtColor(train, cv2.COLOR_BGR2GRAY)
    gray2 = cv2.cvtColor(tmplt, cv2.COLOR_BGR2GRAY)

    (train_height, train_width, _) = train.shape

    # initialize the AKAZE descriptor, then detect keypoints and extract
    # local invariant descriptors from the image
    detector = cv2.AKAZE_create()
    # exctract (keypoints, descriptors)
    (kps1, descs1) = detector.detectAndCompute(gray1, None)
    (kps2, descs2) = detector.detectAndCompute(gray2, None)

    # Apply ratio test
    good = []
    # Match the features
    matches = cv2.BFMatcher(cv2.NORM_HAMMING).knnMatch(descs1, descs2, k=2)
    for index, T in enumerate(THS):
        for m, n in matches:
            if m.distance < T * n.distance:
                good.append([m])
        if len(good) >= MIN_MATCH_COUNT:
            break

    def findCenterViaHomography(train=train):
        if len(good) < MIN_MATCH_COUNT:
            return None

        train_pts = np.float32(
            [kps1[m[0].queryIdx].pt for m in good]).reshape(-1, 1, 2)
        dst_pts = np.float32(
            [kps2[m[0].trainIdx].pt for m in good]).reshape(-1, 1, 2)

        M, mask = cv2.findHomography(dst_pts, train_pts, cv2.LMEDS,
                                     RANSAC_REPROJ_TH)

        if not M.any() and not mask.any():
            return None

        (h, w, _) = tmplt.shape
        tmplt_center = np.float32([
            [w / 2, h / 2]
            ]).reshape(-1, 1, 2)

        center = cv2.perspectiveTransform(tmplt_center, M)[0][0]

        if debug:
            pts = np.float32([
                [MARGIN, MARGIN], [MARGIN, h - MARGIN - 1],
                [w - MARGIN - 1, h - MARGIN - 1], [w - MARGIN - 1, MARGIN]
                ]).reshape(-1, 1, 2)
            dst = cv2.perspectiveTransform(pts, M)
            train = cv2.polylines(
                train, [np.int32(dst)], True, (0, 255, 255), 3, cv2.LINE_AA)
        return center

    # init return value - center of found match
    center = None

    # most advanced AKAZE detection - full homography extraction
    center = findCenterViaHomography()

    # if AKAZE fails, calculate apporx tap region from key points
    if len(good) > 1 and center is None:
        train_pts = np.float32([kps1[m[0].queryIdx].pt for m in good])
        center = train_pts.mean(axis=0)

    # last approach for very simple templates - shape descriptors
    elif center is None:
        ret, gray1 = cv2.threshold(gray1, 110, 255, 0)
        ret, gray2 = cv2.threshold(gray2, 110, 255, 0)
        im1, countours1, h1 = cv2.findContours(
            gray1, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        im2, countours2, h2 = cv2.findContours(
            gray2, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

        countour_matches = []
        for i1, c1 in enumerate(countours1):
            for i2, c2 in enumerate(countours2):
                value = cv2.matchShapes(c1, c2, 1, 0.0)
                if value < COUNTOUR_TH:
                    countour_matches.append([i1, i2, value])

        center = np.asarray(
            countours1[sorted(
                countour_matches,
                key=lambda item: item[2])[0][0]], np.int0).mean(axis=0)[0]
        if debug:
            for m in countour_matches:
                # print np.asarray(countours1[m[0]], np.int0).mean(axis=0)
                cv2.drawContours(train, countours1, m[0], (0, 0, 255), 3)
                cv2.drawContours(tmplt, countours2, m[1], (0, 0, 255), 3)

    if not debug and center:
        return (center[0], center[1])

    # print "keypoints: {}, descriptors: {}".format(len(kps1), descs1.shape)
    # print "keypoints: {}, descriptors: {}".format(len(kps2), descs2.shape)
    # cv2.drawMatchesKnn expects list of lists as matches.

    if center is not None:
        train = cv2.circle(
            train, (int(center[0]), int(center[1])),
            10, (255, 0, 255), 15, cv2.LINE_AA)
    im3 = cv2.drawMatchesKnn(train, kps1, tmplt, kps2, good, None, flags=2)

    cv2.imshow("Zbysiowe zabawy z AKAZE", im3)
    cv2.imwrite('/tmp/akaze_test.png', im3)
    cv2.waitKey(0)

if __name__ == '__main__':
    print kaze_match('screen.png', 'button.png', debug=True)
