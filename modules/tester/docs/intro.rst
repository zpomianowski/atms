Architektura ATMSa
====================================

**ATMS** ma architekturę nieco rozproszoną. Elementy ATMSa są spiąte w całość
dzięki sieci *LAB*.

Użytkownik ma pośredni dostęp do wszystkich elementów poprzez
webowy interfejs graficzny. Dostęp ten gwarantuje tylko sieć *VLAN533* oraz *LAB*.
Poprzez interfejs graficzny użytkownik ma sposobność definiowania zadań/testów.
Węzłem zarządzającym i zlecającym zadania jest serwer *MASTER*. Część zadań wykonuje
*MASTER*, są typy zadań delegowanych na serwery *SLAVE*. Najczęściej są to
różnego rodzaju testy, które wymagają konkretnej konfiguracji fizycznego
środowiska testującego *DUT*.

.. image:: atms_net.png

Schemat konfiguracji (dla testów *DUT STB* ) typowego węzła typu *SLAVE* wygląda następująco:

.. image:: stb_aten.png

Koncepcja kolejki zadań (przycisk *Kolejka* ) pozwala planować testy, długotrwałe procesy oraz
zarządzać (rezerwować) dostępem do konkretnych urządzeń w momencie, gdy jest
więcej niż jeden żądań użycia danego zasobu/urządzenia.

==============================================================

Organizacja zasobów w sieci LAB
###############################

.. Important::
   Każde nowe urządzenie podłączone do sieci należy wpisać do odpowiedniego
   `arkusza <https://docs.google.com/spreadsheets/d/1olkty8c1fBsLdjCWmzi0DFURU7LbhSAX8sUyDoabAHM/edit?usp=sharing>`_.
   Dotyczy to także (a nawet w szczególności) urządzeń, które nie działaja w ramach ATMS.
   
Tutaj ogromna prośba do wszystkich użytkowników sieci *LAB*, by dbać o aktualność
i rzetelne wypełnienie tegoż arkusza.

.. Tip::
   Na ten moment praktycznie wszystkie **nowe urządzenia konfigurujemy ze statycznym IP**.

Należy wybrać odpowiednią kategorię i IP dla naszego urządzenia -
mamy pewne problemy z działaniem serwera DHCP.

.. Tip::
   Dodatkowo jest możliwość przypisania aliasu dla urządzenia. Praca z aliasami jest dużo wygodniejsza
   i bardziej intuicyjna - nie musimy pamiętać adresów IP. Do przydzielania aliasów służy
   `ns-admin <http://nsadmin.redefine.pl/>`_.

.. Warning::
   Dostęp mają nieliczne osoby w *LAB M5* oraz `Jarosław Micota <mailto:jmicota@plus.pl>`_.
   Dostęp do *ns-admin.redefine.pl* wymaga konfiguracji dodatkowego VPNa (OpenVPN).

Alias zawsze ma postać: **unikalny_alias**.lab.redefine.pl (mamy przydzieloną subdomenę *lab* w publicznej domenie *redefine.pl*)
Przykład: *http://komora-espec.lab.redefine.pl*
