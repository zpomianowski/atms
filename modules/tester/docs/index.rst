.. ATMS tester - testy automatyzowane documentation master file, created by
   sphinx-quickstart on Thu Apr 27 09:48:39 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

ATMS tester - testy automatyzowane
==============================================================
Na początek ważna notka. Podczas automatycznych testów należy mieć świadomość podstawowego rozgarniczenia typu urządzeń.

:dut:`DUT` Urządzenia typu `device under test`. Oznaczane kolorem zielonym w opisie interfejsów.
W założeniu są tu urządzenia, które są przedmiotem testu. Są przedmiotem obserwacji i diagnostyki. \
Przy wymianie urządzeń należy zawsze upewnić się czy ustawienia interfejsów, podłączenia kabli itp. \
są poprawne.

==============================================================

:ted:`TED` Urządzenia typu `test environment device`. Oznaczane kolorem niebieskim w opisie interfejsów.
Są to urządzenia laboratoryjne (w większości), które umożliwiają realizację scenariuszy testowych. \
ATMS powinien te urządzenia listować automatycznie. Grzebanie w ich ustawieniach nie powinno mieć racji \
bytu, a przynajmniej w interfejsie webowym ATMS. Inną sprawą są ustawienia na fizycznym urządzeniu. \
Warto się upewnić czy są w trybie zdalnego sterowania oraz czy są odpowiednio podłączone \
np. czy są w sieci LAB i mają poprawnie przydzielone adresy IP.

.. toctree::
   :maxdepth: 10
   :caption: Zawartość:

   intro
   modules



Indeksy i tabele
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
