#!/usr/bin/env python
# -*- coding: utf-8 -*-
from tester.base import TestCase as TC
from tester.base import AbstractTestProc
from datetime import datetime
from ..errors import Error
import types
from time import sleep


class TestCase(TC):

    """Class to run tests against the tested STB."""

    def __init__(self, testFunc, parent):
        super(TestCase, self).__init__(testFunc, parent)
        self.__p = parent
        self._pl = None
        self._ipcamsrc = \
            'rtsp://admin:test1234@10.17.199.47:554/Streaming/Channels/101'

    def ipcamsrc(self, src=None):
        import gi
        gi.require_version('Gst', '1.0')
        from gi.repository import GObject
        from gi.repository import Gst as gst

        GObject.threads_init()
        gst.init(None)

        self._ipcamsrc = src if src else self._ipcamsrc

        PIPELINE = """
        rtspsrc location=%(src)s !
            decodebin ! videoconvert !
            videorate ! video/x-raw,framerate=1/1 !
            clockoverlay halignment=right valignment=bottom deltay=30
                     shaded-background=true font-desc="Sans, 12"
                     time-format="%%Y.%%m.%%D %%H:%%M:%%S" !
            jpegenc ! multifilesink location=%(tmp)s/.current.jpg
        """
        pipeline = PIPELINE % dict(
            src=self._ipcamsrc,
            tmp=self.__p.tmp)
        if self._pl:
            del self._pl
        self._pl = gst.parse_launch(pipeline)
        self._pl.set_state(gst.State.PLAYING)
        sleep(10)

    def snapshot(self, name=None):
        from shutil import copyfile
        import os
        if not self._pl:
            self.ipcamsrc()
        if name:
            src_file = os.path.join(self.__p.tmp, '.current.jpg')
            dst_file = os.path.join(self.__p.tmp, name + '.jpg')
            try:
                copyfile(src_file, dst_file)
                if (datetime.now() -
                    datetime.fromtimestamp(
                        os.path.getctime(src_file))).seconds > 5:
                    self.ipcamsrc()
            except:
                pass


class TestProc(AbstractTestProc):
    def __init__(self, case_id, script_id, funcs=[],
                 duts=[], teds=[], bins=[], user_id=None, lang='en'):
        super(self.__class__, self).__init__(
            case_id, script_id, funcs, duts, teds, bins, user_id, lang)

    def execute(self):
        return self.runScript()

    def clean(self):
        pass


class Bundle(object):
    """:dut:`Interfejs sterowania grupą urządzeń DUT.`

    Note:
        Klasa stanowi kontener dla kilku urządzeń. Przydatna w sytuacji,
        kiedy potrzeba jest testować kilka urządzeń w ramach jednego
        środowiska testowego.

    Warning:
        W odróżnieniu do zwykłej instancji DUT, należy użyć metody **apply**.

    Można iterować po wszystkich urządzeniach DUT jak po zwykłej liście:

    >>> bundle = Bundle(self)
    Załóżmy, że bundle ma listę referencji do trzech obiektów DUT-STB
    >>> for dut in bundle:
    >>> ....print dut.get_info()
    (u'info o DUT a', u'info o DUT b', u'info o DUT c')
    >>> bundle.is_active
    (True, True, True)

    Args:
        context: Ten sam kontekst co dla **LabFactory**.
    """
    def __init__(self, context, subtype=None, model=None):
        from gluon import current
        from socket import gethostname
        from tester.lab import LabFactory
        db = current.db

        teds = None
        try:
            teds = vars(context)['_TestCase__p'].teds
        except:
            pass

        query = ((db.atms_devices.type == 'dut') &
                 (db.atms_devices.host == gethostname()) &
                 (db.atms_devices.connected == True) &
                 (db.atms_devices.state == 'busy'))
        if teds is not None:
            query &= (db.atms_devices.id.belongs(teds))

        factory = LabFactory(context)

        if model:
            query &= (db.atms_devices.model == model)

        if subtype:
            subtype = factory.get_subtype_query(subtype)
            if subtype:
                query &= (db.atms_devices.subtype == subtype)

        duts = db(query).select(db.atms_devices.id)

        self.__dict__['duts'] = []
        for d in duts:
            self.duts.append(factory(id=d.id))
        if len(self.duts) == 0:
            raise Error('No DUT involved in the test! Bundle is empty.')

    def apply(self, name, *args, **kwargs):
        """Wykonuje określoną metodą na grupie urządzeń DUT.

        Args:
            name: nazwa metody, która ma być wykonana
            *args: kolejne argumenty dla metody o nazwie `name`
            **kwargs: kolejne argumenty metody o nazwie `name`
                      postaci klucz-wartość
        Returns:
            tuple: niemutowalna lista wyników wykonania metody
                   dla kolejnych urządzeń DUT.
        """
        obj = self.duts[0]
        if not hasattr(obj, name):
            raise Error('DUT has no "%s" attribute!' % name)
        if not type(getattr(obj, name)) == types.MethodType:
            raise Error('Use standard __getattr__ to get tuple!')
        else:
            return tuple(
                map(lambda x: getattr(x, name)(*args, **kwargs), self.duts))

    def __iter__(self):
        for d in self.duts:
            yield d

    def __call__(self):
        return self.duts

    def __getattr__(self, name, *args, **kwargs):
        obj = self.duts[0]
        if not hasattr(obj, name):
            raise Error('DUT has no "%s" attribute!' % name)
        if type(getattr(obj, name)) == types.MethodType:
            raise Error('Use "apply" method instead!')
        else:
            return tuple(map(lambda x: getattr(x, name), self.duts))

    def __setattr__(self, name, value):
        obj = self.duts[0]
        if not hasattr(obj, name):
            raise Error('DUT has no "%s" attribute!' % name)
        if type(getattr(obj, name)) == types.MethodType:
            raise Error('Use "apply" method instead!')
        else:
            map(lambda x: setattr(x, name, value), self.duts)
