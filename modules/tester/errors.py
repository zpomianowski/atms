#!/usr/bin/env python
# -*- coding: utf-8 -*-
import subprocess
from datetime import datetime
from os import getcwd
from applications.atms.modules.websocket_messaging import websocket_send

WP_CMD = 'bash applications/atms/extras/set_wallpaper.sh file:///%s'
ICON_PATH = getcwd() + "/applications/atms/static/images/notify/"
START_ICON = ICON_PATH + 'cogs.png'
START_ICON_BIG = ICON_PATH + 'cogs_big.png'
ERROR_ICON = ICON_PATH + 'cancel.png'
ERROR_ICON_BIG = ICON_PATH + 'cancel_big.png'
INFO_ICON = ICON_PATH + 'information.png'
STOP_ICON = ICON_PATH + 'checked.png'
STOP_ICON_BIG = ICON_PATH + 'checked_big.png'
SETTINGS = None


class FAILURE():
    GEN = 'GENERAL ERRORS'
    NET = 'NETWORK COMMUNICATION ERRORS'
    ATP = 'ATP ERRORS'
    TESTAPP = 'TESTAPP ERRORS'
    AV = 'AV ERRORS'


def call(msg=None, icon=INFO_ICON):
    if msg:
        subprocess.call(['notify-send', '-i', icon, 'ATMS', msg])
    if SETTINGS:
        msg = "[%s] %s" % (datetime.now().strftime('%H:%M:%S'), msg)
        websocket_send(SETTINGS.ws_server, msg,
                       SETTINGS.security_key, 'notifications')


def NOTIFY_START(msg='-'):
    # subprocess.call(WP_CMD % START_ICON_BIG, shell=True)
    call(msg, START_ICON)


def NOTIFY_STOP(msg='-'):
    # subprocess.call(WP_CMD % STOP_ICON_BIG, shell=True)
    call(msg, STOP_ICON)


def NOTIFY_ERROR(msg=None):
    # subprocess.call(WP_CMD % ERROR_ICON_BIG, shell=True)
    call(msg, ERROR_ICON)


def NOTIFY_WARN(msg=None):
    call(msg, INFO_ICON)


def NOTIFY_INFO(msg):
    call(msg, INFO_ICON)


class Error(Exception):

    """Abstract basic exception class for tester module."""

    def __init__(self, description):
        super(Error, self).__init__(description)
        # TODO
        # tutaj pójdzie obsługa propagacji błędu po websocketach
        # oraz archiwizacja błędu do DB wraz z relacją dla którego
        # przypadku testowego problem się pojawił
        NOTIFY_ERROR(description)


class PropertyNotSupported(Error):
    def __init__(self, cls, property_name):
        super(PropertyNotSupported, self).__init__(
            '%s %s' % (cls, property_name))


class SettingsError(Error):
    def __init__(self, attr):
        super(SettingsError, self).__init__(
            '"%s" is not set or invalid!' % attr)


class ConnectionError(Error):
    def __init__(self, dev):
        super(ConnectionError, self).__init__(
            'Cannot connect to model: %s id:%d udid:%s!' %
            (dev.model, dev.id, dev.udid))
