#!/usr/bin/env python
# -*- coding: utf-8 -*-
from tester.base import TestCase as TC
import os
import time
import tempfile
import warnings
from subprocess import call, Popen, PIPE
from PIL import Image
from tester.base import AbstractTestProc
from tester.base import ProcConsumer
from misc import PickPort
from appium import webdriver


class TestCase(TC):
    IMPLICITY_WAIT = 30
    THUMB = (1980, 1080)
    QSIZE = 50

    TIMEOUT = 60
    DEV_TIMEOUT = 90

    "Class to run tests against the tested app"
    def __init__(self, testFunc, parent):
        super(TestCase, self).__init__(testFunc, parent)
        self.apk = parent.apk
        self.__p = parent

    def get_driver(self, caps):
        if not isinstance(caps, dict):
            raise Exception(
                'Desired capabilities has to be valid dict object!')
        if 'newCommandTimeout' not in caps:
            caps['newCommandTimeout'] = TestCase.TIMEOUT
        if 'deviceReadyTimeout' not in caps:
            caps['deviceReadyTimeout'] = TestCase.DEV_TIMEOUT
        if 'deviceName' not in caps:
            caps['deviceName'] = 'default'
        if 'platformVersion' not in caps:
            p = Popen(
                "adb -s %s shell getprop ro.build.version.release" %
                self.__p.udid,
                stdout=PIPE, stderr=PIPE,
                shell=True)
            caps['platformVersion'] = p.communicate()[0]
        if 'fullReset' not in caps:
            caps['fullReset'] = True
        if 'unicodeKeyboard' not in caps:
            caps['unicodeKeyboard'] = True
        if self.apk is not None:
            caps['app'] = self.apk
        driver = webdriver.Remote(self.__p.appium_url, caps)
        # driver.implicitly_wait(TestCase.IMPLICITY_WAIT)
        return driver

    def snapshot(self, name=None):
        path = tempfile.mkstemp(suffix='.png')[1]
        time.sleep(2)

        call("adb -s %s shell screencap -p | sed 's/\r$//' > %s" %
             (self.__p.udid, path), shell=True)
        if not os.path.isfile(path):
            return

        im = Image.open(path)
        im.thumbnail(TestCase.THUMB)
        jpath = os.path.join(self.__p.tmp, 'appiumsnap_%s.jpg' % name)
        im.save(jpath, "JPEG", quality=75)
        # self.__p.appendFile(jpath, name)
        os.unlink(path)


class TestProc(AbstractTestProc):
    def __init__(self, case_id, script_id, funcs=[],
                 duts=[], teds=[], bins=[], user_id=None, lang='en'):
        super(self.__class__, self).__init__(
            case_id, script_id, funcs, duts, teds, bins, user_id, lang)
        self.apk = None
        try:
            self.apk = self.fetchFile(bins[0].filename)
        except:
            warnings.warn('No binary *.apk file with the app!')

        self.__appium = None
        self.appium_url = None

    def __startAppium(self):
        db = self.db
        udid = db(db.atms_devices.id == self.duts[0]).select(
            db.atms_devices.udid).first().udid
        self.udid = udid
        p_port = PickPort()
        bp_port = PickPort()
        cmd = "appium -bp %s -p %s -U %s" % (
            str(bp_port), str(p_port), udid) + \
            " --session-override"
        self.__appium = ProcConsumer(
            cmd, False, waitForOutput=True, tempdir=self.tmp)
        self.appium_url = 'http://localhost:%d/wd/hub' % p_port

    def __stopAppium(self):
        if self.__appium:
            self.__appium.kill()

    def execute(self):
        self.__startAppium()
        rv = self.runScript()
        self.appendFile(self.__appium.getLogPath())
        return rv

    def clean(self):
        self.__stopAppium()
