#!/usr/bin/env python
# -*- coding: utf-8 -*-
import re
import os
import subprocess
import time
import socket
import shlex
from gluon import current
from applications.atms.modules.websocket_messaging import websocket_send


def task_listAndroidDevices():
    logger = current.logger
    logger.debug("Starting")
    db = current.db
    proc = subprocess.Popen(
        # ["su", current.settings.sys_user, "-c", "adb  devices -l"],
        shlex.split("adb devices -l"),
        stdout=subprocess.PIPE,
        bufsize=1)
    out, err = proc.communicate()
    logger.debug(out)

    r = re.compile(
        r'(\w+)\s+device\s',
        flags=re.I | re.M)
    matches = r.finditer(out)
    counter = 0
    for m in matches:
        counter = + 1
        props = {
            "ro.build.version.release": "android_version",
            "ro.build.version.sdk": "sdk",
            "ro.product.model": "model",
            "ro.product.manufacturer": "manufacturer"
        }
        result = {}
        for k in props.keys():
            cmd = shlex.split(
                "adb -s %s shell getprop %s" % (str(m.group(1)), k))
            proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, bufsize=1)
            result[props[k]] = proc.communicate()[0].strip()

        result['host'] = socket.gethostname()
        result['type'] = 'android'
        result['subtype'] = 'mobile'
        result['udid'] = m.group(1)
        result['state'] = 'unknown'
        result['connected'] = False
        db.atms_devices.update_or_insert(
            db.atms_devices.udid == m.group(1),
            **result)
        db.commit()
        logger.debug(
            'Device \"%s\" found [%s].' % (result["model"], m.group(1)))
    websocket_send(current.settings.ws_server, '',
                   current.settings.security_key, 'atms_devices')

    verifyStates()
    if counter == 0:
        return 'No android devices found. \
        Propably marked as offline. Try to restart adb server.'
    return "OK"


def task_apktool(*args, **vars):
    import shutil
    db = current.db
    apk_id = int(vars['apk_file_id'])
    filename = db(
        db.atms_attachments._id == apk_id).select(
        db.atms_attachments.filename).first().filename
    filename = os.path.join(os.getcwd(), 'applications/atms/uploads',
                            filename)

    apktool = os.path.join(os.getcwd(), 'applications/atms/static/extras',
                           'apktool.jar')

    proc = subprocess.Popen(
        shlex.split("java -jar %s d %s -fo /tmp/apk_reverse_%d" %
                    (apktool, filename, apk_id)),
        stdout=subprocess.PIPE,
        bufsize=1)
    out, err = proc.communicate()
    print out
    print err
    with open("/tmp/apk_reverse_%d/AndroidManifest.xml" % apk_id,
              "r") as manifest:
        websocket_send(current.settings.ws_server, manifest.read(),
                       current.settings.security_key,
                       'apk_reverse_%d' % apk_id)
    shutil.rmtree("/tmp/apk_reverse_%d" % apk_id)
    return 'OK'


def task_adbConsole(*args, **vars):
    udid = args[0]
    from threading import Timer
    import websocket
    from tester.base import ProcConsumer

    db = current.db

    websocket_send(current.settings.ws_server, "INIT",
                   current.settings.security_key, 'adbConsole_%sC' % udid)

    db.atms_devices(udid=udid).update_record(state="adb console")
    db.commit()
    websocket_send(current.settings.ws_server, '',
                   current.settings.security_key, 'atms_devices')

    def on_message(ws, msg):
        timer.cancel()
        if msg == "KILL":
            if ws.atms_proc:
                ws.atms_proc.killProc()
            ws.close()
            return
        if msg == "INIT":
            return
        cmd = "adb -s %s %s" % (udid, msg)
        if ws.atms_proc:
            ws.atms_proc.killProc()
        websocket_send(current.settings.ws_server, ">> %s\n" % cmd,
                       current.settings.security_key, 'adbConsole_%sC' % udid)
        ws.atms_proc = ProcConsumer(
            shlex.split(cmd), shell=False,
            bChannel='adbConsole_%sC' % udid)
        timer.start()

    def on_error(ws, error):
        print error

    def on_close(ws):
        websocket_send(current.settings.ws_server, "KILL",
                       current.settings.security_key, 'adbConsole_%sC' % udid)
        print "# closed #"

    def on_open(ws):
        print "# opened #"

    # websocket.enableTrace(True)
    ws = websocket.WebSocketApp(
        current.settings.ws_client + "/realtime/adbConsole_%sS" % udid,
        on_message=on_message,
        on_error=on_error,
        on_close=on_close)
    ws.on_open = on_open
    ws.atms_proc = None

    timer = Timer(30, ws.close)
    timer.start()

    ws.run_forever(sslopt={"check_hostname": False})

    db.atms_devices(udid=udid).update_record(state="idle")
    db.commit()
    websocket_send(current.settings.ws_server, '',
                   current.settings.security_key, 'atms_devices')


def task_reboot(dev_id):
    db = current.db
    dev = db(db.atms_devices.id == dev_id).select().first()
    dev.update_record(state='rebooting')
    db.commit()
    websocket_send(current.settings.ws_server, '',
                   current.settings.security_key, 'atms_devices')
    proc = subprocess.Popen(
        shlex.split("adb -s %s reboot" % str(dev.udid)),
        stdout=subprocess.PIPE,
        bufsize=1)
    out, err = proc.communicate()
    time.sleep(60)
    dev.update_record(state='rebooting')
    db.commit()
    websocket_send(current.settings.ws_server, '',
                   current.settings.security_key, 'atms_devices')
    proc = subprocess.Popen(
        shlex.split("adb -s %s wait-for-device" % str(dev.udid)),
        stdout=subprocess.PIPE,
        bufsize=1)
    out, err = proc.communicate()
    dev.update_record(state='idle')
    db.commit()
    websocket_send(current.settings.ws_server, '',
                   current.settings.security_key, 'atms_devices')


def verifyStates(devs=None):
    db = current.db

    query = ((db.atms_devices.type == 'android') &
             (db.atms_devices.host == socket.gethostname()))
    if isinstance(devs, list):
        query &= db.atms_devices.id.contains(devs)

    rows = db(query).select()
    for r in rows:
        proc = subprocess.Popen(
            shlex.split("adb -s %s get-state" % r.udid),
            stdout=subprocess.PIPE,
            bufsize=1)
        out, err = proc.communicate()
        if 'device' in out:
            query = db.scheduler_task.status.contains(
                ['RUNNING'], all=False)
            query &= db.scheduler_task.vars.contains(
                "dev_id\": \"%s\"" % str(r.id))
            state = 'busy' if db(query).count() > 0 else 'idle'
            r.update_record(
                state=state,
                connected=True)
        else:
            r.update_record(
                state='offline',
                connected=False)
        db.commit()
    websocket_send(current.settings.ws_server, '',
                   current.settings.security_key, 'atms_devices')
