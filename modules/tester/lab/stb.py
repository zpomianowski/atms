#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Moduł z klasami dla obsługi urządzeń DUT-STB.

Klasy te służą do opakowania funkcji urządzenia testowanego.
W tym wypadku STB.
"""
from abc import ABCMeta
import telnetlib
from urlparse import urlsplit
from time import sleep
import subprocess
import warnings
import socket
import weakref
import re
from gluon.storage import Storage
from pyvisa.constants import AccessModes
from ..errors import Error, ConnectionError, FAILURE
from .base import BaseDevice
from . import rm
from tester.lab.gst.analyser import AVProblemDetector
from tester.base import threadsafe


class StbErrorCollector(object):
    def __init__(self, *args, **kwargs):
        self._map = Storage()
        self._clbck = None
        if 'callback' in kwargs:
            self._clbck = kwargs['callback']
        for arg in args:
            self._map[arg] = 0

    def set_callback(self, func):
        self._clbck = func

    def errors_count(self, name=None):
        if name is None:
            return sum(self._map.values())
        return self._map[name] or 0

    def add_error(self, name):
        if self._map[name] is None:
            self._map[name] = 1
        else:
            self._map[name] += 1
        if self._clbck:
            self._clbck(name, self._map[name])

    def reset(self, name):
        if name is None:
            self._map = Storage()
        elif name in self._map:
            self._map[name] = 0
        else:
            raise ValueError()

    def __str__(self):
        if self.errors_count() == 0:
            return 'OK'
        return ', '.join(['%s: %d' % (k, v) for k, v in self._map.items()])


# interfejs udający pyvisa.resources.Resource
# warto pomyśleć czy da się w miarę łatwo dołożyć do visy interfejs telnetlib
# trzeba dopisać kod dla telnet -> MessageBasedResource
# trzeba dopisać klasę telnet do pyvisa-py
class Telnet():
    def __init__(self, host, port=23,
                 username=None, passwd=None, timeout=5):
        self.max_failures = 3
        self.read_termination = '\r'
        self.write_termination = '\n\r'
        self.timeout = timeout
        self.__host = host
        self.__port = port
        self.__username = username
        self.__passwd = passwd
        self.tn = None
        self.connect()

    def _(self, func, *args, **kwargs):
        status = False
        tries = 0
        return_value = None
        while tries < self.max_failures:
            try:
                return_value = func(*args, **kwargs)
                status = True
                if return_value == '':
                    sleep(self.timeout)
                else:
                    break
            except:
                self.tn.close()
                self.connect()
                sleep(self.timeout)
            tries += 1
        if not status:
            raise Error('Telnet communcation for %s ' % self.__host +
                        'not possible for some reason!')
        return return_value

    def connect(self):
        self.tn = telnetlib.Telnet(self.__host, self.__port)
        if self.__username:
            cmd = (self.__username +
                   self.write_termination).encode('ascii')
            self.tn.read_until("login: ")
            self.tn.write(cmd)
        if self.__passwd:
            cmd = (self.__passwd + self.write_termination).encode('ascii')
            self.tn.read_until("Password: ")
            self.tn.write(cmd)
        self.tn.read_some()

    def query(self, cmd):
        self.write(cmd)
        return self.read()

    @threadsafe
    def write(self, cmd):
        cmd = (cmd + self.write_termination).encode('ascii')
        self._(self.tn.write, cmd)

    @threadsafe
    def read(self, timeout=0.1):
        return self._(self.tn.read_until, '%$%$', timeout)


# for fast debug: echo "<cmd>" | nc -u <ip> <port>
class UDP():
    def __init__(self, parent, host, port=23, timeout=5, max_failures=3):
        self.max_failures = max_failures
        self.read_termination = '\r'
        self.write_termination = '\n\r'
        self.timeout = timeout
        self.__parent = weakref.proxy(parent)
        self.__host = host
        self.__port = int(port)
        self._isok = True

    @property
    def isok(self):
        return self._isok

    @threadsafe
    def query(self, cmd, timeout=None, max_failures=None):
        timeout = timeout or self.timeout
        tries = 0
        while tries < (max_failures or self.max_failures):
            try:
                s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
                s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
                s.settimeout(timeout)
                s.sendto((cmd + self.write_termination).encode('ascii'),
                         (self.__host, self.__port))
                (msgo, addr) = s.recvfrom(4096)
                self._isok = True
                return msgo
            except Exception as err:
                self._isok = False
                self.__parent.warn(
                    'UDP communication not possible: %s [%d/%d]' %
                    (str(err), tries + 1, (max_failures or self.max_failures)))
            tries += 1
        self.__parent.error('UDP communication failed! Connection lost?')
        self.__parent._ec.add_error(FAILURE.NET)
        raise ConnectionError(self.__parent.dev)

    def write(self, cmd):
        raise NotImplementedError()

    def read(self, timeout=0.1):
        raise NotImplementedError()


class GenericSTB(BaseDevice):
    """:dut:`Główny interfejs sterowania STB DUT.`

    Note:
        W miarę możliwości korzystaj
        tylko z niego. Pozwala zapewnić, że skrypt są uniwersalne.

    Warning:
        Nie twórz obiektów urządzeń bezpośrednio.
        Użyj do tego celu fabryki obiektów **tester.lab.LabFactory**!

    Args:
        dev (gluon.storage.Storage): Obiekt danych urządzenia.
            Dane są strukturą pobieraną z bazy danych.
    """
    __metaclass__ = ABCMeta

    def __init__(self, dev):
        super(GenericSTB, self).__init__(dev)
        self.load_defaults({
            'videosrc': '/dev/video0',
            'audiosrc': None,
            'verbose': True
            })
        self.save_settings()

        self.__pingip = None
        self.__is_online = True
        self.driver = None
        self.drivers = Storage({
            'telnet': None,
            'visa': None,
            'udp': None
            })

        self.__log_counter = 0

        self.__av = None
        self._nav = None
        self._is_active = True
        self._paused = False

        self._init_ec()

        self.__interfaceFactory(self.settings.connstr)

    def _init_ec(self):
        # error collector
        self._ec = StbErrorCollector()

    def _set_logger(self, *args, **kwargs):
        super(GenericSTB, self)._set_logger(*args, **kwargs)
        self.__av = AVProblemDetector(self,
                                      videosrc=self.settings.videosrc,
                                      audiosrc=self.settings.audiosrc)
        self.__av.is_active = True

    def get_analyser(self):
        return self.__av

    def make_nav(self, rmt):
        from tester.lab.remote import GenericRemoteCtrl
        from tester.lab.gst.navigator import Navigator
        if not isinstance(rmt, GenericRemoteCtrl):
            raise TypeError(
                '"rmt" has to be "tester.lab.gst.navigator.Navigator" type')
        self._nav = Navigator(self, rmt)

    def destroy_nav(self):
        self._nav = None

    def errors_count(self, name=None):
        if self._ec is None:
            return 0
        if name is None:
            return self._ec.errors_count()
        return self._ec.errors_count(name)

    @property
    def is_online(self):
        return self.__is_online

    @property
    def nav(self):
        return self._nav

    @property
    def paused(self):
        """:obj:`bool`: `True` ignoruje zaistniałe błędy. \
        `False` aktywuje ponownie detektory błędów.

        Przydatne w przypadku testów `hot start`/`cold start`, kiedy STB
        potrzebuje czasu na zbootowanie się.
        """
        return self._paused

    @paused.setter
    def paused(self, value):
        self._paused = value
        if not isinstance(value, bool):
            raise TypeError('"paused" has to be bool type')
        if self.__av:
            self.__av.ignore_errors = value
        self._q.join()

    @property
    def is_active(self):
        """:obj:`bool`: włącza/wyłącza automatyczne
        logowanie i detektory błędów."""
        return self._is_active

    @is_active.setter
    def is_active(self, value):
        if not isinstance(value, bool):
            raise TypeError('"is_active" has to be bool type')
        self._is_active = value
        if self.__av:
            self.__av.is_active = value
        if value:
            self.start_log()
        else:
            self.stop_log()

    def __interfaceFactory(self, conn_strs):
        conn_strs = conn_strs if isinstance(conn_strs, list) else [conn_strs]

        for conn in conn_strs:
            parsed = urlsplit(conn)
            if conn.count('::') > 1:
                try:
                    self.drivers.visa = rm.open_resource(
                        conn,
                        access_mode=AccessModes.exclusive_lock,
                        open_timeout=5000)
                except BaseException:
                    raise ConnectionError(self.dev)
                self.driver = self.drivers.visa

            if parsed.scheme == 'telnet':
                try:
                    self.drivers.telnet = Telnet(
                        parsed.hostname, parsed.port,
                        parsed.username, parsed.password, 5)
                except BaseException:
                    raise ConnectionError(self.dev)
                self.driver = self.drivers.telnet

            if parsed.scheme == 'udp':
                self.drivers.udp = UDP(self, parsed.hostname, parsed.port)
                self.driver = self.drivers.udp
            if parsed.hostname:
                self.__pingip = parsed.hostname

    def set_driver(self, type='udp'):
        """Zmienia `driver`, czyli sposób komunikacji z STB
        (o ile jest zdefiniowanych więcej niz jeden).

        Args:
            type (str, ['telnet', 'udp']): identyfikator sposoby komunikacji
        """
        if type in self.drivers.keys():
            self.driver = self.drivers[type]
            msg = 'Communication driver changed by user.'
            self.warn(msg)
            warnings.warn(msg)
        else:
            raise Error('Requested interface is not supported!')
        if not self.driver:
            raise Error('Driver is not setup!')

    def log(self):
        """Wiadomość 'INFO' generowana podczas automatycznego logowania.

        Zwraca m.in.: `Ping` oraz kondycję STB na bazie \
        wyników detekcji błędów video.

        Returns:
            str: Wiadomość typu `INFO`.
        """
        msg = []
        if not self.is_active or self.paused:
            return 'STB monitor not active or ignores errors'

        msg += self._log_net()
        msg.append(self._log_av())

        if self.settings.verbose:
            return ';'.join([str(self._ec)] + msg)
        return str(self._ec)

    def _log_net(self):
        if self.__pingip:
            try:
                out = subprocess.Popen(
                    'fping -c 3 -q %s' % self.__pingip,
                    stdin=subprocess.PIPE,
                    stdout=subprocess.PIPE,
                    stderr=subprocess.PIPE,
                    shell=True).communicate()[1].split(':')[1].strip()
                if '/100%' in out:
                    self._ec.add_error(FAILURE.NET)
                    self.__is_online = False
                else:
                    self.__is_online = True
                return ['PING: %s[ms]' % out]
            except:
                self.__is_online = False
                self._ec.add_error(FAILURE.NET)
                return ['No ping data!!! fping not installed?']

    def _log_av(self):
        if self.__av:
            s = self.__av.get_current_summary()
            if s['state'] != AVProblemDetector.DEFAULT_STATE:
                self._ec.add_error(FAILURE.AV)
            return '%(state)s, FRAMES: %(framecount)d' % s

    def get_info(self):
        """Informacje ogólne o STB.

        Returns:
            str: Opis urządzenia.
        """
        info = 'GenericSTB - no particular info available'
        self.info(info)
        return info

    def telemetry_stats(self):
        return {
            'errors_nb': self.errors_count(),
            'errors_gen_nb': self.errors_count(FAILURE.GEN),
            'errors_atp_nb': self.errors_count(FAILURE.ATP),
            'errors_av_nb': self.errors_count(FAILURE.AV),
            'errors_testapp_nb': self.errors_count(FAILURE.TESTAPP)
        }

    def release(self):
        """Zwalnia zasoby urządzenia.
        Wyłącza detektory błędów oraz zamyka komunikację z STB.
        """
        if self.__av:
            while self.__av.is_recording:
                self.info('Waiting until error recording is finished...')
                sleep(AVProblemDetector.REC_AFTER)
            self.__av.quit()
        try:
            self.driver.close()
        except:
            pass


class TestAppSTB(GenericSTB):
    def __init__(self, dev):
        super(TestAppSTB, self).__init__(dev)

        # additional parameters
        self.__re_temp = re.compile(r"\d+\.\d+")
        self.__temp = None
        self.__volts = None

    def log(self):
        """Wiadomość 'INFO' generowana podczas automatycznego logowania.

        Zwraca m.in.: wybiórcze dane z TESTAPP, `Ping` \
        oraz kondycję STB na bazie wyników detekcji błędów video.

        Returns:
            str: Wiadomość typu `INFO`.
        """
        msg = []
        if not self.is_active or self.paused:
            return 'STB monitor not active or ignores errors'

        msg = self._log_net()
        msg.append(self._log_testapp())
        msg.append(self._log_av())

        if self.settings.verbose:
            return ';'.join([str(self._ec)] + msg)
        return str(self._ec)

    def channel_up(self):
        self.drivers.udp.query('channel up')

    def channel_down(self):
        self.drivers.udp.query('channel down')

    def toggle_mute(self):
        self.drivers.udp.query('mute')

    def _log_testapp(self):
        if not self.is_online:
            return 'Cannot query TESTAPP - no network connection'
        try:
            out = []
            temp_info = self.driver.query(
                'read temperature', timeout=1).strip()
            out.append(temp_info)
            try:
                self.__temp = float(self.__re_temp.findall(temp_info)[0])
                self.__volts = float(self.__re_temp.findall(temp_info)[1])
            except:
                self.__temp = None
                self.__volts = None
            fcnt = self.driver.query(
                'framecounter', timeout=1).strip().split(' ')
            if fcnt[0] == 'ok':
                out.append(
                    'FRAMES: ok/damaged/rejected ' + '/'.join(fcnt[2:]))
            else:
                out.append('framecounter NOK')
            # TESTAPP not stable for now
            # try:
            #     satpwr = self.driver.query(
            #         'read signalLevel', timeout=1).strip()
            #     if 'ok' in satpwr:
            #         out.append('RF: ' + satpwr.split(' ')[-1])
            # except:
            #     pass
            return ', '.join(out)
        except:
            self._ec.add_error(FAILURE.TESTAPP)
            self.__temp = None
            self.__volts = None
            return 'No data from TESTAPP'

    def error(self, msg, extra_dict=None):
        if self.drivers.udp is not None and self.is_online:
            if self.drivers.udp.isok:
                self.debug('Trying to get data from dmesg...')
                dmesg_dump = self.drivers.udp.query(
                    'shellcmd dmesg', timeout=1, max_failures=1).strip()
                msg = '%s;%s' % (msg, dmesg_dump.replace('\r\n', '\n'))
        super(TestAppSTB, self).error(msg, extra_dict)

    def get_info(self):
        """Informacje ogólne o STB TESTAPP.

        Returns:
            str: Opis urządzenia.
        """
        info = 'No info available'
        if self.drivers.udp is not None:
            info = self.drivers.udp.query(
                'read softwareversion', timeout=1).strip()
            try:
                info = 'SW: ' + info.split(' ')[-1]
            except:
                pass
            info += ', MAC: ' + ', '.join([x for x in self.drivers.udp.query(
                "shellcmd ifconfig eth0 |"
                "grep -o -E '([[:xdigit:]]{1,2}:){5}[[:xdigit:]]{1,2}'",
                timeout=1).strip().split() if len(x) == 17])
            try:
                info += ', IP: ' + self.drivers.udp.query(
                    "shellcmd ip addr show eth0 | grep 'inet'").strip(
                    ).split()[1]
            except:
                pass
            try:
                info += ', ' + self.drivers.udp.query(
                    "shellcmd cat /proc/version").strip()
            except:
                pass

            cpu_info = ''
            try:
                cpu_info = self.drivers.udp.query(
                    'read pubid', timeout=1).strip()
                cpu_info = cpu_info.split(' ')[-1]
            finally:
                if cpu_info:
                    info += ', CPU_ID: %s' % cpu_info
        self.info('This is STB with TESTAPP: ' + info)
        return info

    def telemetry_stats(self):
        if not self.is_active or self.paused:
            self.__temp = None
            self.__volts = 0.0
        return {
            'temp': self.__temp,
            'volts': self.__volts,
            'errors_nb': self.errors_count(),
            'errors_gen_nb': self.errors_count(FAILURE.GEN),
            'errors_atp_nb': self.errors_count(FAILURE.ATP),
            'errors_av_nb': self.errors_count(FAILURE.AV),
            'errors_testapp_nb': self.errors_count(FAILURE.TESTAPP)
        }


class AdbSTB(GenericSTB):
    def log(self):
        raise NotImplementedError()
