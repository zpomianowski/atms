# -*- coding: utf-8 -*-
"""
Moduł z klasami dla obsługi urządzeń DUT-router.

Klasy te służą do opakowania funkcji urządzenia testowanego.
W tym wypadku routera.
"""
from abc import ABCMeta, abstractmethod
from .base import BaseDevice
from subprocess import Popen, PIPE
from threading import Thread
from time import sleep
from tempfile import mkstemp
from ..errors import ConnectionError
import netifaces as ni
import weakref
import shlex
import re
import os

TEST_HOSTS = ['5.172.249.2', '5.172.249.10',  # FTP servers
              '212.2.96.53', '212.2.96.54']   # DNSs in CORE
FTP_DL_NOT = 'FTP DL not active'
FTP_UL_NOT = 'FTP UL not active'
PING_NOT = 'PING not active'


class Worker(Thread):
    def __init__(self, parent, cmd, interval=5, loop=True, filename=None):
        Thread.__init__(self)
        self.setDaemon(True)
        self.interval = interval
        self.__parent = weakref.proxy(parent)
        self.__flag = True
        self.__cmd = cmd
        self.__proc = None
        self.__loop = loop
        self.__filename = filename

    def run(self):
        self.__proc = Popen(
            shlex.split(self.__cmd), stdout=PIPE, stderr=PIPE)
        while self.__flag:
            if self.__proc.poll() is not None and self.__loop:
                self.__proc = Popen(
                    shlex.split(self.__cmd), stdout=PIPE, stderr=PIPE)
            sleep(self.interval)

        if self.__proc is not None:
            self.__proc.kill()

    def stop(self):
        self.__flag = False
        if self.__proc is not None:
            self.__proc.terminate()
        if self.__filename:
            os.unlink(self.__filename)


class SpeedMeter(Thread):
    def __init__(self, parent, interval=15):
        Thread.__init__(self)
        self.setDaemon(True)
        self.__parent = weakref.proxy(parent)
        self.__flag = True
        self.__interval = interval

    def run(self):
        counter = 0
        rx_megabits_new = 0
        tx_megabits_new = 0
        while self.__flag:
            try:
                # check if routing is not changed
                # ex. when router was restarted
                gw = self.__parent.router_gw
                out = Popen('route -n',
                            stdout=PIPE, stderr=PIPE,
                            shell=True).communicate()[0]
                routing_exists = False
                for l in out.split('\n'):
                    info = [i for i in l.split(' ') if i != '']
                    if len(info) == 0:
                        continue
                    if info[1] == gw and info[0] == '0.0.0.0':
                        # router reconnected
                        # we need to configure routing again
                        self.__parent._del_routes()
                        self.__parent._add_routes()
                        self.__parent.dl_speed = 0
                        self.__parent.ul_speed = 0
                        counter = 0
                    if info[1] == gw:
                        routing_exists = True

                # no routing - assumption there should not be any traffic
                if not routing_exists:
                    self.__parent.dl_speed = 0
                    self.__parent.ul_speed = 0
                    counter = 0
                    continue

                # measure downlink/uplink speed
                output = Popen(['ifconfig', self.__parent.router_eth],
                               stdout=PIPE).communicate()[0]
                rx_bytes = re.findall('RX bytes:([0-9]*) ', output)[0]
                tx_bytes = re.findall('TX bytes:([0-9]*) ', output)[0]
                rx_megabits = (((float(rx_bytes) * 8) / 1024) / 1024)
                tx_megabits = (((float(tx_bytes) * 8) / 1024) / 1024)
                current_rx_usage = rx_megabits - rx_megabits_new
                current_tx_usage = tx_megabits - tx_megabits_new
                rx_megabits_new = rx_megabits
                tx_megabits_new = tx_megabits
                if counter > 0:
                    self.__parent.dl_speed = current_rx_usage / self.__interval
                    self.__parent.ul_speed = current_tx_usage / self.__interval
            except weakref.ReferenceError:
                break
            sleep(self.__interval)
            counter += 1

    def stop(self):
        self.__flag = False


class GenericRouter(BaseDevice):
    """:dut:`Główny interfejs sterowania routerem DUT.`

    Note:
        W miarę możliwości korzystaj
        tylko z niego. Pozwala zapewnić, że skrypt są uniwersalne.

    Warning:
        Nie twórz obiektów urządzeń bezpośrednio.
        Użyj do tego celu fabryki obiektów **tester.lab.LabFactory**!

    Args:
        dev (gluon.storage.Storage): Obiekt danych urządzenia.
            Dane są strukturą pobieraną z bazy danych.
    """
    __metaclass__ = ABCMeta

    def __init__(self, dev):
        """
        Non-root user has to change routes. You need to override this.
        lftp & fallocate MUST be installed.

        Type: sudo visudo
        Add lines:
            <user> ALL=(ALL) NOPASSWD: /sbin/route
        """
        super(GenericRouter, self).__init__(dev)

        # Find out what IP has router
        self.router_eth = None
        self.router_gw = None
        self.router_mask = None
        self.router_ip = self.settings.connstr
        for iface in ni.interfaces():
            try:
                addr = ni.ifaddresses(iface)[ni.AF_INET][0]['addr']
                if '.'.join(self.router_ip.split('.')[:-1]) in addr:
                    self.router_mask = ni.ifaddresses(
                        iface)[ni.AF_INET][0]['netmask']
                    self.router_eth = iface
                    self.router_gw = '.'.join(
                        self.router_ip.split('.')[:-1] + ['1'])
                    break
            except:
                self.warn('Interface "%s" is not in use.' % iface)

        if self.router_gw is None:
            self.error(
                'Router has to be connected and active during initialization!')
            raise ConnectionError(dev)

        # Clear routes for this router & add new
        self.test_hosts = TEST_HOSTS

        self.dl_speed = 0  # Mbit/s
        self.ul_speed = 0  # Mbit/s
        self.ip2ping = ['212.2.96.53', self.router_gw]

        self.__pFtpDL = None
        self.__pFtpUL = None

        self.__speedm = SpeedMeter(self)
        self.__speedm.start()

    @property
    def test_hosts(self):
        """:obj:`list(str)`: lista serwerów, które biorą udział w testach."""
        return self._test_hosts

    @test_hosts.setter
    def test_hosts(self, value):
        self._test_hosts = value if isinstance(value, list) else [value]
        self._del_routes()
        self._add_routes()

    def log(self):
        """Wiadomość 'INFO' generowana podczas automatycznego logowania.

        Zwraca m.in.: `DL`, `UL`, `FTPDL`, `FTPUL`, `Ping`.

        Returns:
            str: Wiadomość typu `INFO`.
        """
        msg = ['DL: %.2f [Mb/s]\tUL: %.2f [Mb/s]' % (
            self.dl_speed, self.ul_speed)]
        msg.append('FTPDL: %s' % bool(self.__pFtpDL))
        msg.append('FTPUL: %s' % bool(self.__pFtpUL))

        self.ip2ping = (self.ip2ping if isinstance(self.ip2ping, list)
                        else [self.ip2ping])
        for ip in self.ip2ping:
            info = 'PING for %s: ' % ip
            try:
                msg.append(info + ', '.join(
                    Popen(shlex.split('ping -c 3 %s' % ip),
                          stdin=PIPE, stdout=PIPE, stderr=PIPE
                          ).communicate()[0].split('\n')[-3:-1]))
            except:
                msg.append(info + PING_NOT)
        return ';'.join(msg)

    def _del_routes(self):
        out = Popen('route -n',
                    stdout=PIPE, stderr=PIPE, shell=True).communicate()[0]
        for l in out.split('\n'):
            info = [i for i in l.split(' ') if i != '']
            if len(info) == 0:
                continue
            if self.router_gw in info[1]:
                Popen('sudo route del -net %s gw %s netmask %s dev %s' % (
                    info[0], info[1], info[2], info[7]),
                    stdout=PIPE, stderr=PIPE, shell=True).communicate()

    def _add_routes(self):
        for host in self._test_hosts:
            Popen('sudo route add -host %s gw %s dev %s' % (
                host, self.router_gw, self.router_eth),
                stdout=PIPE, stderr=PIPE, shell=True).communicate()

    def get_info(self):
        """Informacje ogólne o urządzeniu.

        Returns:
            str: Opis urządzenia.
        """
        return 'Universal generic class to test every router'

    def run_ftpdl(self, host='5.172.249.2',
                  user='lte', password='lte',
                  remote_path='download/1g', connections=5):
        """Odpala test FTP DL.

        Args:
            host (str): obsługujący test serwer FTP.
            user (str): użytkownik serwera FTP.
            password (str): hasło użytkownika serwera FTP.
            remote_path (str): ścieżka do pliku testowego.
            connections (int): ilość jednoczesnych sesji FTP.
        """
        cmd = 'set xfer:clobber yes; pget -n %d -c %s -o /dev/null' % (
            connections, remote_path)
        cmd = 'lftp -u %(user)s,%(password)s -e \'%(cmd)s\' %(host)s' % dict(
            user=user, password=password, host=host, cmd=cmd)
        self.__pFtpDL = Worker(self, cmd)
        self.__pFtpDL.start()

    def stop_ftpdl(self):
        """Zatrzymuje test FTP DL."""
        if self.__pFtpDL is not None:
            self.__pFtpDL.stop()
        self.__pFtpDL = None

    def run_ftpul(self, host='5.172.249.2',
                  user='lte', password='lte',
                  local_path=None, remote_path='upload/', connections=5):
        """Odpala test FTP UL.

        Args:
            host (str): obsługujący test serwer FTP.
            user (str): użytkownik serwera FTP.
            password (str): hasło użytkownika serwera FTP.
            local_path (str): ścieżka do pliku wysyłanego. Gdy `None`
                generowany jest automatycznie tymczasowy plik dla testu.
            connections (int): ilość jednoczesnych sesji FTP.
        """
        local_path = (mkstemp(suffix='_upload_1GB')[1]
                      if not local_path else local_path)
        Popen(shlex.split(
            'fallocate -l 1G %s' % local_path),
            stdout=PIPE, stderr=PIPE).communicate()
        cmd = 'put %s -O %s' % (local_path, remote_path)
        cmd = 'lftp -u %(user)s,%(password)s -e \'%(cmd)s\' %(host)s' % dict(
            user=user, password=password, host=host, cmd=cmd)
        self.__pFtpUL = Worker(self, cmd, filename=local_path)
        self.__pFtpUL.start()

    def stop_ftpul(self):
        """Zatrzymuje test FTP UL."""
        if self.__pFtpUL is not None:
            self.__pFtpUL.stop()
        self.__pFtpUL = None

    def release(self):
        """Zwalnia zasoby urządzenia. Zatrzymuje diagnostykę."""
        self.stop_ftpdl()
        self.stop_ftpul()
        self.__speedm.stop()
        self._del_routes()
