#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Moduł z klasami dla obsługi sterowanych źródel zasilania.
"""
from abc import ABCMeta, abstractmethod, abstractproperty
from ..errors import PropertyNotSupported, ConnectionError
from .base import BaseDevice
from . import rm
from pyvisa.constants import AccessModes
from tester.base import threadsafe
from misc import Memoize
from time import time, sleep


class GenericACPower(BaseDevice):
    """:ted:`Główny interfejs sterowania źródłem zasilania.`

    Note:
        W miarę możliwości korzystaj
        tylko z niego. Pozwala zapewnić, że skrypt są uniwersalne.

    Warning:
        Nie twórz obiektów urządzeń bezpośrednio.
        Użyj do tego celu fabryki obiektów **tester.lab.LabFactory**!

    Args:
        dev (gluon.storage.Storage): Obiekt danych urządzenia.
            Dane są strukturą pobieraną z bazy danych.
    """
    __metaclass__ = ABCMeta

    def __init__(self, dev):
        super(GenericACPower, self).__init__(dev)

    @abstractproperty
    def curr(self):
        """:obj:`float`: odczytuje/ustawia natężenie prądu źródła"""
        pass

    @abstractproperty
    def volts(self):
        """:obj:`float`: odczytuje/ustawia napięcie źródła"""
        pass

    @abstractproperty
    def freq(self):
        """:obj:`float`: odczytuje/ustawia częstotliwość prądu źródła"""
        pass

    @abstractmethod
    def on(self):
        """Włącza zasilanie."""
        pass

    @abstractmethod
    def off(self):
        """Wyłącza zasilanie.

        Warning:
            Weź pod uwagę wplyw na urządzenia podłączone do źródła.
            Zabezpiecz się przed skutkami.
        """
        pass

    @Memoize(timeout=5)
    def log(self):
        """Wiadomość 'INFO' generowana podczas automatycznego logowania.

        Zwraca m.in.: `V [V]`, `I [A]`, `F [Hz]`.

        Returns:
            str: Wiadomość typu `INFO`.
        """
        try:
            return 'V=%.2f[V];I=%.2f[A];F=%.2f[Hz]' % (
                self.volts, self.curr, self.freq)
        except:
            return 'V=?;I=?;F=?'

    def set_volts_limits(self, min, max):
        """Ustawia limity dla napięcia zasilania `V`.

        Args:
            min (float): minimalna wartość `V`
            max (float): maksymalna wartość `V`
        """
        raise NotImplementedError()

    def set_curr_limits(self, min, max):
        """Ustawia limity dla natężenia prądu zasilania `I`.

        Args:
            min (float): minimalna wartość `I`
            max (float): maksymalna wartość `I`
        """
        raise NotImplementedError()

    def set_freq_limits(self, min, max):
        """Ustawia limity dla częstotliwości prądu zasilania `F`.

        Args:
            min (float): minimalna wartość `F`
            max (float): maksymalna wartość `F`
        """
        raise NotImplementedError()

    def telemetry_stats(self):
        return {
            'curr': self.curr,
            'volts': self.volts,
            'freq': self.freq
        }



class phyIT7321(GenericACPower):
    """Interfejs sterowania źródłem `ITECH IT7321`.

        .. image:: it7321.jpg
            :scale: 50 %

    Note:
        Używaj głównego interfejsu `GenericACPower`. Skrypty
        testowe wówczas są uniwersalne.
        Tej funkcjonalność używaj, gdy potrzebujesz unikalnych cech
        urządzenia `ITECH IT7321`.

        * upewnij się, że sterowanie zdalne jest aktywne

    Warning:
        Nie twórz obiektów urządzeń bezpośrednio.
        Użyj do tego celu fabryki obiektów **tester.lab.LabFactory**!

    Args:
        dev (gluon.storage.Storage): Obiekt danych urządzenia.
            Dane są strukturą pobieraną z bazy danych.
    """

    CMD_GUARD = 2  # it7321 do not like too fast queries one after another

    def __init__(self, dev):
        super(phyIT7321, self).__init__(dev)
        self._connect()

        self.__timesig = time()

        self.__write('SYST:REM')
        self.__write('SYST:BEEP 1')
        self.__write('SYST:CLE')

    def _connect(self):
        if self.driver is not None:
            try:
                self.driver.close()
            except:
                pass
        try:
            self.driver = rm.open_resource(
                self.settings.connstr,
                access_mode=AccessModes.exclusive_lock,
                open_timeout=1000)
        except BaseException:
            raise ConnectionError(self.dev)
        self.driver.read_termination = '\n'
        self.driver.timeout = 1000

    def is_ok(self):
        result = self.driver.query('SYST:ERR?')
        try:
            code, msg = result.split(',')
            self.driver.write('SYST:CLE')
            if int(code) == 0:
                return True
            else:
                self.warn('[%s] %s' % (code, msg))
                return False
        except:
            self.error('Fatal communication error.')
            return False

    @threadsafe
    @BaseDevice.reconnection_handler
    def __write(self, cmd, cmd_guard=False):
        if abs(time() - self.__timesig) < phyIT7321.CMD_GUARD and cmd_guard:
            sleep(phyIT7321.CMD_GUARD)
        self.driver.write(cmd)
        if not self.is_ok():
            self.warn('Command "%s" sent again' % cmd)
            self.driver.write(cmd)
        if not self.is_ok():
            raise ConnectionError(self.dev)
        self.__timesig = time()

    @threadsafe
    @BaseDevice.reconnection_handler
    def __query(self, cmd):
        return self.driver.query(cmd)

    def get_info(self):
        return self.__query('*IDN?')

    @property
    def curr(self):
        self._curr = self.__query('FETC:CURR?')
        return float(self._curr)

    @curr.setter
    def curr(self, value):
        raise PropertyNotSupported(self.__name__, __name__)

    @property
    def volts(self):
        self._volts = self.__query('FETC:VOLT?')
        return float(self._volts)

    @volts.setter
    def volts(self, value):
        self.__write('VOLT:IMM %f' % value)

    @property
    def freq(self):
        self._freq = self.__query('FETC:FREQ?')
        return float(self._freq)

    @freq.setter
    def freq(self, value):
        self.__write('FREQ:IMM %f' % value)

    def set_volts_limits(self, min, max):
        self.__write('CONF:VOLT:MIN %.2f' % min)
        self.__write('CONF:VOLT:MAX %.2f' % max)

    def on(self):
        max_counter = 3
        counter = 0
        self.__write('OUTP 1', cmd_guard=True)
        while self.volts == 0 and counter < max_counter:
            self.__write('OUTP 1', cmd_guard=True)
            counter += 1
        if counter == max_counter:
            raise ConnectionError(self.dev)

    def off(self):
        max_counter = 3
        counter = 0
        self.__write('OUTP 0', cmd_guard=True)
        while self.volts > 0 and counter < max_counter:
            self.__write('OUTP 0', cmd_guard=True)
            counter += 1
        if counter == max_counter:
            raise ConnectionError(self.dev)

    def release(self):
        self.__write('SYST:LOC')
        self.driver.close()
