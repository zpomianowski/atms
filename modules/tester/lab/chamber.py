#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Moduł z klasami dla obsługi komór klimatycznych.
"""
from abc import ABCMeta, abstractmethod, abstractproperty
from ..errors import ConnectionError
from .base import BaseDevice
from . import rm
from pyvisa.constants import AccessModes
from time import sleep
from tester.base import threadsafe
from misc import Memoize


class GenericClimateChamber(BaseDevice):
    """:ted:`Główny interfejs sterowania komorą.`

    Note:
        W miarę możliwości korzystaj
        tylko z niego. Pozwala zapewnić, że skrypt są uniwersalne.

    Warning:
        Nie twórz obiektów urządzeń bezpośrednio.
        Użyj do tego celu fabryki obiektów **tester.lab.LabFactory**!

    Args:
        dev (gluon.storage.Storage): Obiekt danych urządzenia.
            Dane są strukturą pobieraną z bazy danych.
    """
    __metaclass__ = ABCMeta

    def __init__(self, dev):
        super(GenericClimateChamber, self).__init__(dev)

    def _cmp(self, v1, v2, margin=1):
        # przykład: traktuj temp 25 i 24.7 jako osiągnięty cel 25
        return abs(v1 - v2) < margin

    def _validateHumiCtrl(self, humic):
        # kontrola wilgotności nie jest możliwa dla
        # wszystkich wartości (temp, humi)
        # t > ah^2 - bh + c
        temp = self._temp
        humi = self._humi
        if temp < 10 or humi < 10 or humi > 98 or not humic:
            return False
        return temp > 0.006 * humi**2 - 0.78 * humi + 37.22

    @abstractmethod
    def wait(self, temp=True, humi=True):
        """Czeka na zadane parametry.

        Args:
            temp (bool): Czekaj na temperaturę. Domyślnie `TAK`.
            humi (bool): Czekaj na wilgotność. Domyślnie `TAK`.
        """
        pass

    @abstractmethod
    def on(self):
        """Włącza komorę."""
        pass

    @abstractmethod
    def off(self):
        """Wyłącza komorę."""
        pass

    @abstractproperty
    def temp(self):
        """:obj:`float`: zwraca aktualną lub ustawia pożądaną temperaturę"""
        pass

    @abstractproperty
    def humi(self):
        """:obj:`float`: zwraca aktualną lub ustawia pożądaną wilgotność

        Warning:
            Wcześniej musi być włączone ``control_humi``!
        """
        pass

    @abstractproperty
    def control_humi(self):
        """:obj:`bool`: kontrola wilgotności włączona/wyłączona (domyślnie)"""
        pass

    def telemetry_stats(self):
        return {
            'temp': self.temp,
            'humi': self.humi if self.control_humi else None,
            'control_humi': self.control_humi
        }



class phyP3ARC(GenericClimateChamber):
    """Interfejs sterowania komorą `Espec`.

        .. image:: espec.jpg
            :scale: 35 %

    Note:
        Używaj głównego interfejsu `GenericClimateChamber`. Skrypty
        testowe wówczas są uniwersalne.
        Tej funkcjonalność używaj, gdy potrzebujesz unikalnych cech
        urządzenia `Espec`.

        * upewnij się, że sterowanie zdalne jest aktywne

    Warning:
        Nie twórz obiektów urządzeń bezpośrednio.
        Użyj do tego celu fabryki obiektów **tester.lab.LabFactory**!

    Args:
        dev (gluon.storage.Storage): Obiekt danych urządzenia.
            Dane są strukturą pobieraną z bazy danych.
    """
    def __init__(self, dev):
        super(phyP3ARC, self).__init__(dev)
        self.__on = False
        self._temp = 28.0
        self._humi = 0.0
        self.control_humi = False

    def _connect(self):
        if self.driver is not None:
            try:
                self.driver.close()
            except:
                pass
        try:
            self.driver = rm.open_resource(
                self.settings.connstr,
                access_mode=AccessModes.exclusive_lock,
                open_timeout=1000)
        except BaseException:
            raise ConnectionError(self.dev)
        self.driver.timeout = 1000
        self.driver.read_termination = '\r\n'
        self.driver.write_termination = '\r\n'

    @Memoize(timeout=15)
    def log(self):
        try:
            value = 'T=%.2f[°C]' % self.temp
            if self.control_humi:
                value += ';H=%.2f[%%]' % self.humi
            return value
        except:
            return 'T=?;H=?'

    @threadsafe
    @BaseDevice.reconnection_handler
    def __query(self, cmd):
        sleep(.1)
        return self.driver.query(cmd)

    def get_info(self):
        return self.__query('ROM?')

    def on(self):
        self.__on = True
        self.__query('MODE, CONSTANT')

    def off(self):
        self.__on = False
        self.__query('MODE, STANDBY')

    def wait(self, temp=True, humi=True):
        humi = self._validateHumiCtrl(humi)
        interval = 10
        if not self.__on:
            self.warn('No waiting: the chamber is OFF')
            return
        while temp and not self._cmp(self._temp, self.temp):
            sleep(interval)
        while humi and not self._cmp(self._humi, self.humi, margin=2):
            sleep(interval)

    @property
    def temp(self):
        try:
            return float(self.__query('TEMP?').split(',')[0])
        except ValueError:
            # OK:TEMP ???
            self.warn('Could not parse temp value!')
            return -999

    @temp.setter
    def temp(self, value):
        self.__query('TEMP, S%.1f' % value)
        self._temp = value

    @property
    def humi(self):
        try:
            return float(self.__query('HUMI?').split(',')[0])
        except ValueError:
            self.warn('Could not parse humi value!')
            return -1

    @humi.setter
    def humi(self, value):
        if value and self.control_humi:
            self.__query('HUMI, S%.0f' % value)
            self._humi = value

    @property
    def control_humi(self):
        return self._control_humi

    # TODO - nie sprawdzone! Wymaga testu!
    @control_humi.setter
    def control_humi(self, value):
        if not isinstance(value, bool):
            raise TypeError('"control_humi" has to be bool type')
        if not value:
            self.__query('HUMI,SOFF')
        self._control_humi = value

    def release(self):
        self.control_humi = False
        self.temp = 25
        self.off()
        self.driver.close()


class phyWKL(GenericClimateChamber):
    """Interfejs sterowania komorą `WKL`.

        .. image:: wkl64.jpg
            :scale: 50 %
        .. image:: wkl100.jpg
            :scale: 50 %

    Note:
        Używaj głównego interfejsu `GenericClimateChamber`. Skrypty
        testowe wówczas są uniwersalne.
        Tej funkcjonalność używaj, gdy potrzebujesz unikalnych cech
        urządzenia `WKL`.

        * upewnij się, że protokół ASCII2 jest aktywny
        * sprawdź czy jest włączony tryb sterowania zdalnego \
            (EXTERN dla WKL100)

    Warning:
        Nie twórz obiektów urządzeń bezpośrednio.
        Użyj do tego celu fabryki obiektów **tester.lab.LabFactory**!

    Args:
        dev (gluon.storage.Storage): Obiekt danych urządzenia.
            Dane są strukturą pobieraną z bazy danych.
    """
    WKL_SERIAL = 'FTYUS43E'

    def __init__(self, dev):
        super(phyWKL, self).__init__(dev)

        self.__on = False
        self._temp = 25.0
        self._humi = 10.0
        self._control_humi = False
        self.control_humi = False

    def _connect(self):
        if self.driver is not None:
            try:
                self.driver.close()
            except:
                pass
        try:
            self.driver = rm.open_resource(
                self.settings.connstr,
                access_mode=AccessModes.exclusive_lock,
                open_timeout=1000)
        except BaseException:
            raise ConnectionError(self.dev)
        self.driver.timeout = 1000
        self.driver.read_termination = '\r'
        self.driver.write_termination = '\r'
        if self.dev['udid'] == phyWKL.WKL_SERIAL:
            self.driver.write_termination = '\r\n'
            self.driver.baud_rate = 19200

    # TODO - needed, but onl for WKL Serial!!!
    @threadsafe
    def __cmd(self):
        CMD = '$01E %06.1f %06.1f 0%d0000%d000000000' % (
            self._temp, self._humi, self.__on, self._control_humi)
        if self.dev['udid'] == phyWKL.WKL_SERIAL:
            CMD = ('$01E %06.1f %06.1f 0000.0 0000.0 0000.0 0000.0 ' +
                   '0000.0 0%d%d10101010101010101010101010101') % (
                self._temp, self._humi,
                self.__on, self._control_humi)
        return self.__query(CMD)

    @threadsafe
    @BaseDevice.reconnection_handler
    def __query(self, cmd):
        sleep(.1)
        return self.driver.query(cmd)

    @Memoize(timeout=15)
    def log(self):
        try:
            data = self.__query('$01I').split(' ')
            value = 'T=%.2f[°C]' % float(data[1])
            if self.control_humi:
                value += ';H=%.2f[%%]' % float(data[3])
            return value
        except:
            return 'T=?;H=?'

    def get_info(self):
        # $01? - informacje dotyczace komory
        return self.__query('$01?')

    def on(self):
        self.__on = True
        self.__cmd()

    def off(self):
        self.__on = False
        self.__cmd()

    def wait(self, temp=True, humi=True):
        humi = self._validateHumiCtrl(humi)
        interval = 10
        if not self.__on:
            self.warn('No waiting: the chamber is OFF')
            return
        while temp and not self._cmp(self._temp, self.temp):
            sleep(interval)
        while humi and not self._cmp(self._humi, self.humi, margin=2):
            sleep(interval)

    @property
    def temp(self):
        try:
            return float(self.__query('$01I').split(' ')[1])
        except IndexError:
            return -1.0

    @temp.setter
    def temp(self, value):
        self._temp = value
        self.__cmd()

    @property
    def humi(self):
        try:
            return float(self.__query('$01I').split(' ')[3])
        except IndexError:
            return -1.0

    @humi.setter
    def humi(self, value):
        self._humi = value
        self.__cmd()

    @property
    def control_humi(self):
        return self._control_humi

    @control_humi.setter
    def control_humi(self, value):
        if not isinstance(value, bool):
            raise TypeError('"control_humi" has to be bool type')
        self._control_humi = value
        if self.dev['udid'] == phyWKL.WKL_SERIAL:
            self._humi = 10
        else:
            self._humi = 10 if value else 0
        self.__cmd()

    @threadsafe
    def release(self):
        self.control_humi = False
        self.temp = 25
        self.off()
        self.driver.close()
