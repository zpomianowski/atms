# -*- coding: utf-8 -*-
"""
Moduł z klasami dla obsługi włączników zasilania
(sterowalne listwy zasilające).
"""
import paramiko
from abc import ABCMeta, abstractmethod
from .base import BaseDevice
from misc import Memoize


class GenericMFi(BaseDevice):
    """:ted:`Główny interfejs sterowania listwą.`

    Note:
        W miarę możliwości korzystaj
        tylko z niego. Pozwala zapewnić, że skrypt są uniwersalne.

    Warning:
        Nie twórz obiektów urządzeń bezpośrednio.
        Użyj do tego celu fabryki obiektów **tester.lab.LabFactory**!

    Args:
        dev (gluon.storage.Storage): Obiekt danych urządzenia.
            Dane są strukturą pobieraną z bazy danych.
    """
    __metaclass__ = ABCMeta

    def __init__(self, dev):
        super(GenericMFi, self).__init__(dev)

    @abstractmethod
    def _send(self):
        pass

    @abstractmethod
    def on(self, socket):
        """Włącza port/porty.

        Args:
            *args (int): Numer portu/portów do włączenia.
        """
        pass

    @abstractmethod
    def off(self, socket):
        """Wyłącza port/porty.

        Args:
            *args (int): Numer portu/portów do wyłączenia.
        """
        pass

    @abstractmethod
    def stop(self):
        """Zamyka komunikację z urządzeniem."""
        pass


class phyMFi(GenericMFi):
    """Interfejs sterowania listwą `Ubiquiti mPower-Pro mFi`.

        .. image:: mfi.jpg
            :scale: 35 %

    Note:
        Używaj głównego interfejsu `GenericMFi`. Skrypty
        testowe wówczas są uniwersalne.
        Tej funkcjonalność używaj, gdy potrzebujesz unikalnych cech
        urządzenia `Ubiquiti mPower-Pro mFi`.

        * upewnij się, że sterowanie zdalne jest aktywne

    Warning:
        Nie twórz obiektów urządzeń bezpośrednio.
        Użyj do tego celu fabryki obiektów **tester.lab.LabFactory**!

    Args:
        dev (gluon.storage.Storage): Obiekt danych urządzenia.
            Dane są strukturą pobieraną z bazy danych.
    """
    def __init__(self, dev):
        super(phyMFi, self).__init__(dev)
        self.ssh = None

        self.ssh = paramiko.SSHClient()
        self.ssh.load_system_host_keys()
        self.ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        self.ssh.connect(self.settings.connstr.split('::')[1],
                         username='ubnt', password='ubnt')
        self.__socket_nb = 6
        self.__values = [None] * self.__socket_nb

    def _send(self, cmd):
        return self.ssh.exec_command(cmd)[1]

    def on(self, *args):
        if len(args) == 0:
            for socket in range(1, self.__socket_nb + 1):
                self.ssh.exec_command("echo 1 > /dev/output%s" % socket)
            return

        for socket in args:
            if 0 < socket < 7:
                self.ssh.exec_command("echo 1 > /dev/output%s" % socket)

    def off(self, *args):
        if len(args) == 0:
            for socket in range(1, self.__socket_nb + 1):
                self.ssh.exec_command("echo 0 > /dev/output%s" % socket)
            return

        for socket in args:
            if 0 < socket < 7:
                self.ssh.exec_command("echo 0 > /dev/output%s" % socket)

    def stop(self):
        self.ssh.close()

    def get_info(self):
        pass

    @Memoize(timeout=5)
    def log(self):
        msg = []
        for i in range(1, 7):
            s = 'socket%s: ' % str(i).zfill(2)
            stdin, stdout, stderr = self.ssh.exec_command(
                "cat /proc/power/output%s" % i)
            sactive = bool(True if int(stdout.read()) == 1 else False)
            if sactive:
                self.__values[i - 1] = {
                    'curr': float(self.ssh.exec_command(
                        "cat /proc/power/i_rms%s" % i)[1].read()),
                    'volts': float(self.ssh.exec_command(
                        "cat /proc/power/v_rms%s" % i)[1].read()),
                    'pwr': float(self.ssh.exec_command(
                        "cat /proc/power/active_pwr%s" % i)[1].read())
                }
                s += 'ON '
                s += ' %.2fW' % self.__values[i - 1]['pwr']
                s += ' %.2fA' % self.__values[i - 1]['curr']
                s += ' %.2fV' % self.__values[i - 1]['volts']
            else:
                s += 'OFF'
                self.__values[i - 1] = False
            msg.append(s)
        return ';'.join(msg)

    def release(self):
        self.ssh.close()

    def telemetry_stats(self):
        self.log()
        return [
            dict(curr=v['curr'], volts=v['volts'], pwr=v['pwr']) if v else None
            for v in self.__values]
