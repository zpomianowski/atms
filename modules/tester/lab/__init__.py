#!/usr/bin/env python
# -*- coding: utf-8 -*-
import re
from os import path, mkdir
from importlib import import_module
from pdb import set_trace
from gluon import current
import pyvisa

rm = pyvisa.ResourceManager('@py')


MODEL_MAPPING = {
    'it7321': ('.acpwr', 'phyIT7321'),
    'm3500a': ('.meas', 'phyM3500A'),
    'chroma636x': ('.meas', 'phyChroma636x'),
    'espec': ('.chamber', 'phyP3ARC'),
    'wkl': ('.chamber', 'phyWKL'),
    'irduino': ('.remote', 'phyIrduino'),
    'mfi': ('.mfi', 'phyMFi'),
    'stb': ('.stb', 'GenericSTB'),
    'stb:testapp': ('.stb', 'TestAppSTB'),
    'stb:adb': ('.stb', 'AdbSTB'),
    'router': ('.router', 'GenericRouter'),
    'att': ('.attenuator', 'phyRR'),
    'czaki': ('.thermo', 'phyCzakiWRT9'),
    'keysight': ('.thermo', 'phyKeysight34972A')
    }


def get_subtype(model):
    item = MODEL_MAPPING.get(model, None)
    if item:
        return item[0][1:]
    return ''


class LabFactory(object):
    """Narzędzie do tworzenia instancji urządzeń LAB (TED i DUT).

    Żądanie urządzenia odbywa się poprzez zapytanie, które zidentyfikuje
    nasz zasób. Domyślnie żądanie dotyczy jedynie urządzeń przypisanych
    do hosta, na którym odpalamy test/shell. Wyjątkiem są zasoby `shared`.
    One są dostępne dla każdego węzła testującego.
    Zapytanie może być ogólne (`subtype`), dotyczące konkretnego modelu
    urządzenia ('model'), konkretnego egzemplarza (`udid`, `id`).
    Zaleca się stosować ogólne interfejsy `Generic*` i ogólne zapytania o
    `subtype`. Pozostałe powinny być używane, gdy chcemy wykorzystać
    specyficzne funkcje z interfejsów konkretnych urządzeń (które nie są
    już ogólne dla pozostałych urządzeń tego podtypu).

    >>> some_chamber = factory(subtype='chamber')
    Fabryka zwróci obiekt jakiejś komory klimatycznej: GenericChamber
    >>> wkl_chamber = factory(model='wkl')
    Fabryka zwróci obiekt komory klimatycznej modelu WKL: phyWKL
    >>> wkl_chamber_rs232 = factory(id='31')
    >>> wkl_chamber_rs232 = factory(id='31', model='wkl')
    Fabryka zwróci obiekt konkretnej komory klimatycznej modelu WKL: phyWKL

    Note:
        Nie twórz obiektów urządzeń bezpośrednio.
        Użyj do tego celu fabryki obiektów **tester.lab.LabFactory**!
        Tak! To ta klasa ;)

    Args:
        test: Kontekst, w którym fabryka ma działać. W normalnym trybie
            tym kontekstem jest klasa testu. W przypadku, gdy nie ma konteksu
            (bezpośrednia inicjalizacja np. w `ATMS shell`) część logiki jest
            zmieniona. Logi trafiają do folderu `/tmp/atms_debug_tmp`,
            polityka dostępu do zasobów urządzeń jest mniej restrykcyjna.
    """
    def __init__(self, test=None):
        self.__consolemode = True
        self.__testMethodName = 'test'
        if test is not None:
            self.__consolemode = False
            parent = vars(test)['_TestCase__p']
            self.__tmp = vars(parent)['tmp']
            self.__testMethodName = test._testMethodName
            self.__report_ids = vars(parent)['report_ids']
            self.__duts = parent.duts
            self.__case_id = parent.case_id
            self.teds_reserved = parent.teds
        else:
            self.__tmp = path.join('/tmp', 'atms_debug_tmp')
            try:
                mkdir(self.__tmp)
            except OSError:
                current.logger.warn('"%s" already exist!' % self.__tmp)
            self.__report_ids = None
            self.__duts = []
            self.__case_id = None
            self.teds_reserved = []

    def get_subtype_query(self, subtype):
        items = [v for k, v in MODEL_MAPPING.items() if subtype in v[0]]
        if len(items) == 0:
            return None
        return items[0][0][1:]

    def __call__(self, subtype=None, id=None, udid=None, model=None):
        db = current.db
        query = (
            (db.atms_devices.host == current.settings.host) |
            (db.atms_devices.host == 'shared'))
        query &= ((db.atms_devices.type == 'lab') |
                  (db.atms_devices.type == 'dut'))
        if not self.__consolemode:
            query &= (db.atms_devices.state == 'busy')

        if id:
            query &= (db.atms_devices.id == id)

        if udid:
            query &= (db.atms_devices.udid == udid)

        if model:
            query &= (db.atms_devices.model == model)

        if subtype:
            subtype = self.get_subtype_query(subtype)
            if subtype:
                query &= (db.atms_devices.subtype == subtype)

        dev = db(query).select().first()

        if not dev:
            raise Exception('No such registered device in the LAB!')
        elif not dev.connected:
            raise Exception('The device not connected!')
        elif dev.id not in self.teds_reserved:
            if not self.__consolemode:
                raise Exception(
                    'The device not marked as involved in the test!')

        try:
            m = MODEL_MAPPING[dev.model]
        except Exception as err:
            from tester.errors import Error
            raise Error('No implementation for %s device' % dev.model)

        obj = getattr(import_module(m[0], __name__), m[1])(dev)
        obj._set_logger(
            case_id=self.__case_id,
            duts=self.__duts,
            teds=self.teds_reserved,
            reports_ids=self.__report_ids,
            folder=self.__tmp,
            tmname=self.__testMethodName,
            filename='%s_id%s' % (
                dev.model,
                str(dev.id).zfill(7)))
        return obj


def task_scanlab():
    import socket
    import usb
    from misc import say
    from subprocess import Popen, PIPE
    from websocket_messaging import websocket_send
    settings = current.settings
    db = current.db

    db((db.atms_devices.host == socket.gethostname()) &
       (~db.atms_devices.type.contains(['android', 'ios', 'windows']))).update(
        connected=False, state='offline')
    db.commit()

    say('Stanowisko: %s. Listowanie urządzeń' % socket.gethostname())

    websocket_send(settings.ws_server, '',
                   settings.security_key, 'atms_devices')

    known_devices = {
        '164e:dad': 'm3500a',
        'it7321': 'it7321',
        'espec': 'espec',
        'FTDI:FTYUS43E': 'wkl',
        'FTDI:A400guTo': 'czaki',
        'Arduino__www.arduino.cc_': 'irduino'
        }

    def update_entry(r, dev, key, **kwargs):
        attrs = {
            'type': 'lab',
            'host': socket.gethostname(),
            'udid': None,
            'model': None,
            'manufacturer': None
            }

        if key not in known_devices.keys():
            print 'Device "%s" is not known!' % key
            return
        if known_devices[key] in ['m3500a', 'it7321']:
            if known_devices[key] == 'it7321':
                dev.read_termination = '\n'
            info_data = [i.strip() for i in dev.query('*IDN?').split(',')]
            attrs['manufacturer'] = info_data[0]
            attrs['model'] = info_data[1].lower()
            attrs['udid'] = info_data[2]
            dev.write('SYST:LOC')
            dev.close()
        # TODO bad solution so far :/
        if known_devices[key] in ['espec']:
            attrs['manufacturer'] = 'Espec Corp.'
            attrs['model'] = 'espec'
            attrs['udid'] = kwargs['serialnb']
            dev.close()
        if known_devices[key] in ['wkl']:
            attrs['manufacturer'] = 'Weiss Umwelttechnik GmbH'
            attrs['model'] = 'wkl'
            attrs['udid'] = kwargs['serialnb']
            dev.close()
        if known_devices[key] in ['irduino']:
            attrs['manufacturer'] = 'Arduino SA'
            attrs['model'] = known_devices[key]
            attrs['udid'] = kwargs['serialnb']
            dev.close()
        if known_devices[key] in ['czaki']:
            attrs['manufacturer'] = 'CZAKI Thermo-Product'
            attrs['model'] = 'czaki'
            attrs['udid'] = '?'
            dev.close()
        attrs['subtype'] = get_subtype(attrs['model'])

        attrs.update({'connected': True, 'state': 'idle'})
        db.atms_devices.update_or_insert(
            (db.atms_devices.udid == attrs['udid']), **attrs)
        dev = db.atms_devices(udid=attrs['udid'])
        if not isinstance(dev.settings, list):
            dev.settings = [{}]
        dev.settings[0]['connstr'] = r
        dev.update_record()
        db.commit()

    # non network devices
    resources = rm.list_resources()
    for r in resources:
        device = rm.open_resource(r, timeout=1)
        # TODO for serial based instruments
        if isinstance(device, pyvisa.resources.SerialInstrument):
            tty = rm.resource_info(r).interface_board_number.split('/')[-1]
            data = Popen('udevadm info -n %s' % tty,
                         stdout=PIPE, shell=True).communicate()[0]
            m = re.search(r'ID_VENDOR=(.*)', data)
            if m:
                serialnb = re.search(r'ID_SERIAL_SHORT=(.*)', data).group(1)
                key = (m.group(1) if m.group(1) in known_devices.keys() else
                       '%s:%s' % (m.group(1), serialnb))
                update_entry(r, device, key, serialnb=serialnb)
        # VID&PID based
        elif isinstance(device, pyvisa.resources.USBInstrument):
            dev = usb.core.find(
                idVendor=int(r.split('::')[1]),
                idProduct=int(r.split('::')[2]))
            if dev:
                key = '%x:%x' % (dev.idVendor, dev.idProduct)
                update_entry(r, device, key)

    # network devices - discovery
    known_net_devices = {
        '38:BF:2F:21:00:FE': dict(model='espec', port=57732, hint='SOCKET'),
        '70:B3:D5:A9:2C:62': dict(model='wkl', port=2049, hint='SOCKET'),
        '04:18:D6:26:5C:D7': dict(model='mfi', port=22, hint='SOCKET'),
        '24:A4:3C:DC:27:40': dict(model='mfi', port=22, hint='SOCKET'),
        '24:A4:3C:DC:26:53': dict(model='mfi', port=22, hint='SOCKET'),
        '00:17:F8:01:09:E5': dict(model='it7321', port=30000, hint='SOCKET'),
        '00:17:F8:01:07:76': dict(model='it7321', port=30000, hint='SOCKET'),
        '00:17:F8:01:09:E3': dict(model='it7321', port=30000, hint='SOCKET'),
        'FC:36:53:51:D5:FD': dict(model='att', port=50000, hint='SOCKET'),
        'FC:35:53:2A:74:BD': dict(model='att', port=50000, hint='SOCKET'),
        '00:40:9D:8C:40:BA': dict(
            model='chroma636x', port=2101, hint='SOCKET'),
        '80:09:02:01:63:F0': dict(model='keysight', port='inst0', hint='INSTR')
        }
    nmap_output = Popen('nmap --privileged -sP 10.17.199.51-130',
                        stdout=PIPE, shell=True).communicate()[0]
    for item in [m.groupdict() for m in re.finditer(
            r'(?P<ip>(?:[0-9]{1,3}\.){3}[0-9]{1,3})\n.*?\n.*?' +
            r'(?P<udid>([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2}))' +
            r'\s\((?P<manufacturer>.*)\)',
            nmap_output)]:
        item['type'] = 'lab'
        item['state'] = 'offline'
        info = known_net_devices.get(item['udid'], None)
        if info and not db.atms_devices(udid=item['udid']):
            item['model'] = info['model']
            item['settings'] = [
                {'connstr': 'TCPIP::%s::%s::%s' % (
                    item['ip'], info['port'], info['hint'])}]
            del item['ip']
            db.atms_devices.insert(**item)
    db.commit()

    net_devs = db(
        ((db.atms_devices.host == socket.gethostname()) |
         (db.atms_devices.host == 'shared')) &
        (db.atms_devices.settings.like(r"%TCPIP::%"))).select()

    for dev in net_devs:
        ping = Popen('nmap --privileged -sP %s' %
                     dev.settings[0]['connstr'].split('::')[1],
                     stdout=PIPE, shell=True).communicate()[0]
        if ping.find('Host is up') > 0:
            dev.update_record(
                connected=True,
                state='idle',
                subtype=get_subtype(dev.model))
            db.commit()

    # inspect stbs
    stb_devs = db(
        (db.atms_devices.model.contains('stb')) &
        (db.atms_devices.type == 'dut') &
        (db.atms_devices.host == socket.gethostname())).select()
    for stb in stb_devs:
        connstr = stb.settings[0].get('connstr', None)
        if not connstr:
            continue
        connstr = connstr if isinstance(connstr, list) else [connstr]
        for string in connstr:
            m = re.search(r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}', string)
            if m:
                model = stb.model
                ip = m.group()
                ping = Popen('nmap --privileged -sP %s' % ip,
                             stdout=PIPE, shell=True).communicate()[0]
                try:
                    # Very simple and stupid verification if STB with TESTAPP
                    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
                    s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
                    s.settimeout(5)
                    s.sendto(('read status\r').encode('ascii'),
                             (ip, 1234))
                    if 'wifistatus' in s.recvfrom(4096)[0]:
                        model = 'stb:testapp'
                        stb.update_record(model=model)
                        db.commit()
                except:
                    pass
                if ping.find('Host is up') > 0:
                    stb.update_record(
                        connected=True,
                        state='idle',
                        subtype=get_subtype(model))
                    db.commit()
                    continue

    say('Stanowisko: %s. Listowanie skończone!' % socket.gethostname())

    websocket_send(settings.ws_server, '',
                   settings.security_key, 'atms_devices')


if __name__ == '__main__':
    task_scanlab()
