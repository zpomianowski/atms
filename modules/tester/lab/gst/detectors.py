#!/usr/bin/env python
# -*- coding: utf-8 -*-
from collections import deque
import numpy as np

NAME_FREEZE = 'FREEZE'


class BaseDetector(object):
    def __init__(self):
        self.__result = ('BASE', False, -1)

    def diagnose(self, arr):
        # Have to always return a tuple (ERROR_CODE, HAS_OCCURED, MSG)
        # Example for Freeze Error (NAME_FREEZE, True, 0.0234)
        # ERROR_CODE - to distiguish the type of Error
        # HAS_OCCURED - True problem occured, False nothing detected
        # MSG - details for logs
        raise NotImplementedError

    def reset(self):
        raise NotImplementedError


class VideoFreezeDetector(BaseDetector):
    TH = 1e-4
    START_TH = 120
    EVENT_COUNTS_TH = 5

    def __init__(self):
        BaseDetector.__init__(self)
        self.__history = deque(maxlen=VideoFreezeDetector.EVENT_COUNTS_TH)
        self.__nth = 0
        self.__result = (NAME_FREEZE, False, -1)

    def diagnose(self, arr):
        self.__nth += 1

        value = self.entropy(arr)
        self.__history.appendleft(value)

        data = list(self.__history)
        if len(data) < VideoFreezeDetector.EVENT_COUNTS_TH or \
                self.__nth < VideoFreezeDetector.START_TH:
            return (NAME_FREEZE, False, -1)

        result = np.std(data)
        self.__result = (NAME_FREEZE, result < VideoFreezeDetector.TH,
                         '%.4f' % result)
        return self.__result

    def reset(self):
        self.__nth = 0

    def entropy(self, A, axis=None):
        if axis is None:
            A = A.flatten()
            counts = np.bincount(A)
            counts = counts[counts > 0]
            if len(counts) == 1:
                return -1
            probs = counts / float(A.size)
            return -np.sum(probs * np.log2(probs))
        elif axis == 0:
            entropies = map(lambda col: entropy(col), A.T)
            return np.array(entropies)
        elif axis == 1:
            entropies = map(lambda row: entropy(row), A)
            return np.array(entropies).reshape((-1, 1))
        else:
            raise ValueError("unsupported axis: {}".format(axis))
