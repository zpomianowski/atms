#!/usr/bin/env python
# -*- coding: utf-8 -*-
import weakref
from time import time


class Navigator(object):
    TIMEOUT = 30
    MAX_TRIES = 30

    def __init__(self, stb, rmt):
        """Navigator służy do interakcji z STB na podobnej
        zasadzie jak stb-tester. Navigator jest tworzony automatycznie
        w momencie przypisania pilota do DUT STB.

        Args:
            stb (tester.lab.stb.GenericSTB): przypisany STB
            rmt (tester.lab.GenericRemoteCtrl): symulator pilota
        """
        self.__stb = weakref.proxy(stb)
        self.__rmt = weakref.proxy(rmt)

        self._cursor_pos = None
        self._query_pos = None

    def __query(self):
        sample = self.__stb.get_analyser().pull_sample()

    def find(self, img):
        return True

    def click(self, img):
        timeout = Navigator.TIMEOUT
        max_tries = Navigator.MAX_TRIES

        tries = 0
        start_time = time()
        while (time() < start_time + timeout) or (tries < max_tries):
            tries += 0
