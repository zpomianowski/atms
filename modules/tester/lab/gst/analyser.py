#!/usr/bin/env python
# -*- coding: utf-8 -*-
import gi
import re
gi.require_version('Gst', '1.0')
from gi.repository import GObject
from gi.repository import Gst as gst
from collections import deque
import weakref
import os
import threading
import numpy as np
from misc import hw2alsa
from detectors import VideoFreezeDetector, BaseDetector
from tester.errors import Error, FAILURE


DEBUG = False
if DEBUG:
    os.environ["GST_DEBUG"] = '*:2'
    # os.environ["GST_DEBUG"] = '*:2,*scope*:4'

gst.init(None)
GObject.threads_init()


RTMP_SERVER = 'rtmp://atms.lab.redefine.pl/hls/%s-%d'
# DEFAULT_W = 480
# DEFAULT_H = 270
DEFAULT_W = 640
DEFAULT_H = 360
FRAMERATE = 25
DEFAULT_PIPELINE = """
videoconvert ! videoscale ! videorate !
    video/x-raw,framerate=%(framerate)d/1,
    format=YUY2,width=%(default_w)s,height=%(default_h)s !
tee name=t1
    t1. ! queue !
        appsink name=atms emit-signals=true drop=true max-buffers=10
    t1. ! queue !
        videomixer name=mix
            sink_1::alpha=0.5 sink_2::alpha=0.5 sink_2::ypos=400 !
        textoverlay name=txt text="%(desc)s"
            shaded-background=true font-desc="Sans, 16"
            halignment=left valignment=top deltay=-20 !
        clockoverlay halignment=right valignment=bottom deltay=-16
            shaded-background=true font-desc="Sans, 14"
            time-format="%%Y.%%m.%%D %%H:%%M:%%S" !
        timeoverlay halignment=right valignment=bottom deltay=20
            shaded-background=true font-desc="Sans, 14" !
        tee name=t2
    t2. ! queue
        min-threshold-time=%(rec_before)d
        max-size-time=%(rec_before)d
        max-size-bytes=0
        max-size-buffers=0 name=rec_src
    t2. ! queue ! videoconvert !
        x264enc tune=zerolatency speed-preset=1 key-int-max=75 !
            video/x-h264,profile=baseline ! queue !
            flvmux streamable=True !
            rtmpsink sync=false location=\"%(rtmp_server)s\"
pulsesrc device="%(audiosrc)s" volume=1 ! tee name=taudio
    taudio. ! queue ! audioconvert ! spectrascope !
        video/x-raw,width=%(default_w)s,height=100 ! videoscale ! mix.
"""
if DEBUG:
    DEFAULT_PIPELINE = "%s\n%s\n%s" % (
        # """filesrc location=/home/zpomianowski/sample.mp4 ! decodebin !
        # videoconvert ! videoscale ! video/x-raw,format=YUY2,width=%(default_w)s,height=%(default_h)s !
        # """,
        """rtmpsrc location=rtmp://atms.lab.redefine.pl/vod/owca !
           decodebin !""",
        DEFAULT_PIPELINE,
        "t2. ! queue ! videoconvert ! xvimagesink sync=false "
        "taudio. ! queue ! audioconvert ! pulsesink volume=1")
else:
    DEFAULT_PIPELINE = "%s\n%s" % (
        """v4l2src device=%(videosrc)s ! videoscale !
        video/x-raw,format=YUY2,framerate=%(framerate)d/1,
        width=%(default_w)s,height=%(default_h)s !
        videoconvert !""",
        DEFAULT_PIPELINE)


class RecBin(gst.Pipeline):
    def __init__(self, path):
        gst.Pipeline.__init__(self)

        factory = gst.ElementFactory()

        self.vconv = factory.make('videoconvert')
        self.add(self.vconv)

        self.x264enc = factory.make('x264enc')
        self.x264enc.set_property('tune', 'zerolatency')
        self.add(self.x264enc)

        self.mp4mux = factory.make('mp4mux')
        self.mp4mux.set_property('faststart', True)
        self.mp4mux.set_property('faststart-file', True)
        self.add(self.mp4mux)

        self.filesink = factory.make('filesink')
        self.filesink.set_property('location', path)
        self.filesink.set_property('sync', False)
        self.add(self.filesink)

        self.vconv.link(self.x264enc)
        self.x264enc.link(self.mp4mux)
        self.mp4mux.link(self.filesink)

        pad = self.vconv.get_static_pad("sink")
        ghostpad = gst.GhostPad.new("sink", pad)
        ghostpad.set_active(True)
        self.add_pad(ghostpad)

        self.__stopped = False

        sink_pad = self.filesink.get_static_pad('sink')
        probe_id = None

        def eos_clbck(pad, info):
            if info.get_event().type == gst.EventType.EOS:
                self.x264enc.set_state(gst.State.NULL)
                self.mp4mux.set_state(gst.State.NULL)
                self.filesink.set_state(gst.State.NULL)
                self.filesink.set_property('location', None)
                self.filesink.set_property('location', '/tmp/.test.mp4')
                self.__stopped = True
            return gst.PadProbeReturn.OK

        probe_id = sink_pad.add_probe(
            gst.PadProbeType.EVENT_DOWNSTREAM, eos_clbck)

    def is_stopped(self):
        return self.__stopped

    def play(self):
        self.set_state(gst.State.PLAYING)
        self.sync_children_states()
        self.__stopped = False

    def stop(self):
        self.get_static_pad('sink').send_event(gst.Event.new_eos())
        self.mp4mux.send_event(gst.Event.new_eos())


class AVProblemDetector(threading.Thread):
    NTH_ANALYSED = FRAMERATE
    REC_BEFORE = 9
    REC_AFTER = 15
    SAME_ERRS_INROW = 4
    DEFAULT_STATE = 'HEALTHY'
    UNKNOWN_STATE = 'UNKNOWN'

    def __init__(self, parent, videosrc=None, audiosrc=None,
                 pipe_desc=DEFAULT_PIPELINE):
        threading.Thread.__init__(self)
        self.setDaemon(True)
        self.__parent = weakref.proxy(parent)

        portinc = 0
        try:
            portinc = int(re.sub('[a-zA-Z/]', '', videosrc))
        except:
            self.__parent.warn('Warning! Default port used!')

        audiosrc = 'hw:0,0'
        try:
            from subprocess import check_output
            audiosrc = check_output(['mwcap-info', '-a', videosrc]).strip()
            self.__parent.debug(
                '%s chosen as the corresponding audio device' % audiosrc)
        except:
            if DEBUG:
                self.__parent.warn('No audio source found! Default used!')
            else:
                raise Error('Could not find audio source for STB!')
        audiosrc = hw2alsa(audiosrc)

        factor = 1e9
        self.desc = "[ID:%s] %s" % (parent.dev.id, parent.dev.udid.upper())
        settings = dict(
            rec_before=int(AVProblemDetector.REC_BEFORE * factor),
            rec_after=int((AVProblemDetector.REC_BEFORE +
                           AVProblemDetector.REC_AFTER) * factor),
            videosrc=videosrc,
            audiosrc=audiosrc,
            framerate=FRAMERATE,
            log_dir=parent.get_log_dir(),
            desc=self.desc,
            rtmp_server=RTMP_SERVER % (parent.dev.host, portinc),
            default_w=DEFAULT_W,
            default_h=DEFAULT_H)
        with open('/tmp/test.sh', 'w') as f:
            f.write('gst-launch-1.0 ' +
                    (DEFAULT_PIPELINE % settings).replace('\n', '\\\n'))
        self.pl = gst.parse_launch(DEFAULT_PIPELINE % settings)
        consumer = self.pl.get_by_name('atms')
        consumer.connect('new-sample', self.new_vbuff)

        bus = self.pl.get_bus()
        bus.connect('message::eos', self.on_msg)
        bus.connect('message::error', self.on_error)
        bus.add_signal_watch()

        self._is_recording = False
        self.is_active = False
        self._ignore_errors = False

        self.__nth = 0
        self._errors = deque(maxlen=10)
        self.__errors_count = 0

        self.gloop = GObject.MainLoop()
        self.start()

        self.__timer = None
        self.rec_bin = None

        factory = gst.ElementFactory()
        self.fakesink = factory.make('fakesink')
        self.fakesink.set_property('sync', False)
        self.pl.set_state(gst.State.READY)

        # bypass to disable block effect
        # because queue not filled enough with buffers :(
        # there is no problem for files, only for v4l2src - fact to debug
        def start_fakesink():
            self.pl.add(self.fakesink)
            self.pl.get_by_name('rec_src').link(self.fakesink)
            self.fakesink.set_state(gst.State.PLAYING)
            timer.cancel()

        timer = threading.Timer(AVProblemDetector.REC_BEFORE, start_fakesink)
        timer.start()

        self._detectors = [
            VideoFreezeDetector()
            ]

        self.__result = AVProblemDetector.UNKNOWN_STATE

        assert self._errors.maxlen > AVProblemDetector.SAME_ERRS_INROW
        self.pl.set_state(gst.State.READY)

    @property
    def label(self):
        return self.pl.get_by_name('txt').get_property('text')

    @label.setter
    def label(self, msg):
        msg = self.desc if msg == '' else '%s - %s' % (self.desc, msg)
        if self.__errors_count > 0:
            msg += ' (ERR EVENTS: %d)' % self.__errors_count
        self.pl.get_by_name('txt').set_property('text', msg)

    @property
    def ignore_errors(self):
        return self._ignore_errors

    @ignore_errors.setter
    def ignore_errors(self, value):
        if not isinstance(value, bool):
            raise TypeError('"ignore_errors" has to be bool type')
        self._ignore_errors = value

    @property
    def detectors(self):
        return self._detectors

    @detectors.setter
    def detectors(self, value):
        if not isinstance(value, (list, tuple)):
            _detectors = [value]
        else:
            _detectors = value
        for d in _detectors:
            if not isinstance(d, BaseDetector):
                raise TypeError(
                    'All detectors have to be instance of BaseDetector!')
        self._detectors = _detectors

    @property
    def is_recording(self):
        return self._is_recording

    @is_recording.setter
    def is_recording(self, value):
        if not isinstance(value, bool):
            raise TypeError('"is_active" has to be bool type')
        if value == self._is_recording:
            return

        if value:
            if self.rec_bin is not None:
                if self.rec_bin.is_stopped():
                    self.rec_bin.set_state(gst.State.NULL)
                    self.rec_bin.sync_children_states()
                else:
                    self.__parent.warn(
                        'Too fast! '
                        'Recording not stopped and new error occured!')
                    return
            if not self.rec_bin:
                self.rec_bin = RecBin(self.get_file_path())
                self.pl.add(self.rec_bin)

            self.rec_bin.filesink.set_property(
                'location', self.get_file_path()),  self.get_file_path()

            self.rec_bin.play()

            self.pl.get_by_name('rec_src').unlink(self.fakesink)
            self.pl.get_by_name('rec_src').link(self.rec_bin)

            def stop_rec():
                self.__timer.cancel()
                self.is_recording = False

            self.__timer = threading.Timer(
                AVProblemDetector.REC_BEFORE +
                AVProblemDetector.REC_AFTER, stop_rec)

            self.__timer.start()
        else:
            self.label = ''
            rec_src = self.pl.get_by_name('rec_src')

            src_pad = rec_src.get_static_pad('src')
            sink_pad = self.rec_bin.get_static_pad('sink')

            probe_id = None

            def clbck(pad, info):
                src_pad.unlink(self.rec_bin.get_static_pad('sink'))
                src_pad.link(self.fakesink.get_static_pad('sink'))
                self.rec_bin.stop()
                pad.remove_probe(probe_id)
                return gst.PadProbeReturn.OK

            probe_id = src_pad.add_probe(
                gst.PadProbeType.BLOCK_DOWNSTREAM, clbck)
        self._is_recording = value

    @property
    def is_active(self):
        return self._is_active

    @is_active.setter
    def is_active(self, value):
        if not isinstance(value, bool):
            raise TypeError('"is_active" has to be bool type')
        if value:
            from time import sleep
            self.pl.set_state(gst.State.PLAYING)
        else:
            self.pl.set_state(gst.State.PAUSED)
        self._is_active = value

    def run(self):
        try:
            self.gloop.run()
        except:
            pass

    def quit(self):
        self.pl.set_state(gst.State.NULL)
        try:
            self.gloop.quit()
        except:
            pass

    def on_error(self, bus, msg):
        print 'GST ERROR: %s' % gst.Message.parse_error(msg)[0].message

    def on_msg(self, bus, msg):
        t = msg.type
        if t == gst.MessageType.EOS:
            self.pl.set_state(gst.State.NULL)
            self.pl.sync_children_states()
            self.pl.set_state(gst.State.PLAYING)
        self.__parent.warn(t)

    def get_file_path(self):
            return os.path.join(
                self.__parent.get_log_dir(),
                '%s.error_dev%d_frame%d.mp4' % (
                    self.__parent.test_method_name,
                    self.__parent.dev.id, self.__nth))

    def new_file(self, splitmux, id):
        pass

    def pull_sample(self):
        return self.pl.get_by_name('atms').pull_sample()

    def new_vbuff(self, appsink):
        # for safety - if parent is dead kill tje GLib loop
        if self.__parent is None:
            self.quit()

        self.__nth += 1
        if self.__nth % AVProblemDetector.NTH_ANALYSED != 0:
            return gst.FlowReturn.OK

        sample = appsink.emit('pull-sample')
        buf = sample.get_buffer()
        caps = sample.get_caps()

        arr = np.ndarray(
            (caps.get_structure(0).get_value('height'),
             caps.get_structure(0).get_value('width'),
             2),
            buffer=buf.extract_dup(0, buf.get_size()),
            dtype=np.uint8)

        self.diagnose(arr)

        return gst.FlowReturn.OK

    def diagnose(self, arr):
        # Do not diagnose while recording
        if self.is_recording or self.ignore_errors:
            return

        results = []
        for d in self._detectors:
            r = d.diagnose(arr)
            if r[1]:
                self.__errors_count += 1
                results.append(r)
                self._errors.appendleft(r[0])
                d.reset()

        if len(results) == 0:
            # self.pl.get_by_name('txt').set_property('text', self.desc)
            self.__result = self.DEFAULT_STATE
            return

        for state in set(list(self._errors)):
            if state == AVProblemDetector.DEFAULT_STATE:
                continue

            err_count = self._errors.count(state)
            if err_count == 0:
                continue

            if err_count == AVProblemDetector.SAME_ERRS_INROW:
                msg = 'Will not record. Several occurances in a row.'
                self.show_error(state, msg)
            elif err_count > AVProblemDetector.SAME_ERRS_INROW:
                pass
            else:
                last = [err for err in list(results) if err[0] == state][-1]
                self.show_error(state, last[2])
                self.__parent._ec.add_error(FAILURE.AV)
                self.is_recording = True

        self.__result = ', '.join(['%s - %s' % (r[0], r[2]) for r in results])

    def show_error(self, type, msg=None):
        from datetime import datetime
        _msg = 'ERROR %s, %s' % (type, datetime.now().strftime(
            '%Y.%m.%d %H:%M:%S'))
        self.label = _msg
        _msg = _msg if msg is None else '%s - %s' % (_msg, msg)
        self.__parent.error(_msg)

    def get_current_summary(self):
        return dict(
            state=self.__result,
            framecount=self.__nth)

    def __del__(self):
        self.pl.set_state(gst.State.NULL)
