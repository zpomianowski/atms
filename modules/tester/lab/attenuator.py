# -*- coding: utf-8 -*-
"""
Moduł z klasami dla obsługi sterowalnych tłumików.
"""
from abc import ABCMeta, abstractproperty
from ..errors import ConnectionError
from .base import BaseDevice
from . import rm
from pyvisa.constants import AccessModes
from tester.base import threadsafe


class GenericAttenuator(BaseDevice):
    """:ted:`Główny interfejs sterowania tłumikiem.`

    Note:
        W miarę możliwości korzystaj
        tylko z niego. Pozwala zapewnić, że skrypt są uniwersalne.

    Warning:
        Nie twórz obiektów urządzeń bezpośrednio.
        Użyj do tego celu fabryki obiektów **tester.lab.LabFactory**!

    Args:
        dev (gluon.storage.Storage): Obiekt danych urządzenia.
            Dane są strukturą pobieraną z bazy danych.
    """
    __metaclass__ = ABCMeta

    def __init__(self, dev):
        super(GenericAttenuator, self).__init__(dev)

    @abstractproperty
    def att(self):
        """:obj:`int`: ustawia/odczytuje wartości tłumienia na
        wszystkich kanałach."""
        pass


# TODO - not tested at all!
class phyRR(GenericAttenuator):
    """Interfejs sterowania komorą `Radio Rack 4-22(0/1)`.

        .. image:: radioRack.jpg
            :scale: 50 %

    Note:
        Używaj głównego interfejsu `GenericAttenuator`. Skrypty
        testowe wówczas są uniwersalne.
        Tej funkcjonalność używaj, gdy potrzebujesz unikalnych cech
        urządzenia `Radio Rack 4-22(0/1)`.

        * upewnij się, że sterowanie zdalne jest aktywne

    Warning:
        Nie twórz obiektów urządzeń bezpośrednio.
        Użyj do tego celu fabryki obiektów **tester.lab.LabFactory**!

    Args:
        dev (gluon.storage.Storage): Obiekt danych urządzenia.
            Dane są strukturą pobieraną z bazy danych.
    """
    MIN = 0
    MAX = 110

    def __init__(self, dev):
        super(phyRR, self).__init__(dev)
        try:
            self.driver = rm.open_resource(
                self.settings.connstr,
                access_mode=AccessModes.exclusive_lock,
                open_timeout=1000)
        except BaseException:
            raise ConnectionError(self.dev)
        self.driver.read_termination = '\r\n'
        self.driver.write_termination = '\r\n'
        self.__nb_of_channels = 4

    def get_info(self):
        """Informacje ogólne o urządzeniu.

        Returns:
            str: Opis urządzenia.
        """
        return 'Radio Rack 4 4-221'

    @threadsafe
    def __query(self, cmd):
        return self.driver.query(cmd)

    def log(self):
        """Wiadomość `INFO` generowana podczas automatycznego logowania.

        Zwraca wartości tlumienia na kolejnych kanałach.

        Returns:
            str: Wiadomość typu `INFO`.
        """
        try:
            return ';'.join(['ATT%s=%d[dB]' % (str(i + 1).zfill(2), j)
                             for i, j in enumerate(self.att)])
        except:
            return ';'.join(['ATT%s=?' % str(i + 1).zfill(2)
                             for i in range(self.__nb_of_channels)])

    def set_att(self, channel, value):
        """Ustawia tlumienie na konkretnym kanale.

        Args:
            channel (int, [1,2,3,4]): numer kanału
            value (int, <0;110>): wartość tłumienia w dB
        """
        if not isinstance(channel, int):
            raise TypeError('channel has to be integer type')
        if not isinstance(value, int):
            raise TypeError('attenuation has to be integer type')
        if not 1 <= channel <= self.__nb_of_channels:
            raise ValueError(
                'channel has to be between: 1-%d' % self.__nb_of_channels)
        if not phyRR.MIN <= value <= phyRR.MAX:
            raise ValueError('attenuation has to be between: %d-%d dB' % (
                phyRR.MIN, phyRR.MAX))
        self.__query('ATT %d %d' % (channel, value))

    @property
    def att(self):
        atts = self.__query('ATT').split(';')
        return [int(a.split(' ')[-1]) for a in atts]

    @att.setter
    def att(self, value):
        if not isinstance(value, int):
            raise TypeError('"att" has to be integer type')
        if not phyRR.MIN <= value <= phyRR.MAX:
            raise ValueError('"att" has to be between: %d-%d dB' % (
                phyRR.MIN, phyRR.MAX))
        x = []
        for i in range(self.__nb_of_channels):
            x.append('ATT %d %d' % (i, value))
        cmd = ';'.join(x)
        self.__query(cmd)

    def release(self):
        self.driver.tn.close()
