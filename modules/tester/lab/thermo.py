#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Moduł z klasami dla obsługi mierników termicznych (termopary).
"""
from abc import ABCMeta, abstractmethod
from ..errors import ConnectionError
from .base import BaseDevice
from . import rm
from pyvisa.constants import AccessModes
from time import sleep
from tester.base import threadsafe
from misc import Memoize


class GenericThermo(BaseDevice):
    __metaclass__ = ABCMeta

    def __init__(self, dev):
        """:ted:`Główny interfejs sterowania termoparą.`

        Note:
            W miarę możliwości korzystaj
            tylko z niego. Pozwala zapewnić, że skrypt są uniwersalne.

        Warning:
            Nie twórz obiektów urządzeń bezpośrednio.
            Użyj do tego celu fabryki obiektów **tester.lab.LabFactory**!

        Args:
            dev (gluon.storage.Storage): Obiekt danych urządzenia.
                Dane są strukturą pobieraną z bazy danych.
        """
        super(GenericThermo, self).__init__(dev)

    @abstractmethod
    def get_temp(self, channel_nb):
        """Zwraca temperaturę w °C na konkretnym kanale.

        Args:
            channel_nb (int, <1;10>): numer kanału

        Returns:
            float: temperatura w °C.
        """
        pass


class phyCzakiWRT9(GenericThermo):
    """Interfejs sterowania termoparą `Czaki WRT-9`.

        .. image:: czaki.jpg
            :scale: 35 %

    Note:
        Używaj głównego interfejsu `GenericThermo`. Skrypty
        testowe wówczas są uniwersalne.
        Tej funkcjonalność używaj, gdy potrzebujesz unikalnych cech
        urządzenia `Czaki WRT-9`.

        * upewnij się, że sterowanie zdalne jest aktywne

    Warning:
        Nie twórz obiektów urządzeń bezpośrednio.
        Użyj do tego celu fabryki obiektów **tester.lab.LabFactory**!

    Args:
        dev (gluon.storage.Storage): Obiekt danych urządzenia.
            Dane są strukturą pobieraną z bazy danych.
    """
    def __init__(self, dev):
        super(phyCzakiWRT9, self).__init__(dev)
        try:
            self.driver = rm.open_resource(
                self.settings.connstr,
                access_mode=AccessModes.no_lock,
                read_termination='\r\n',
                write_termination='\r',
                baud_rate=2400,
                #flow_control=VI_ASRL_FLOW_XON_XOFF,
                open_timeout=1000)
        except BaseException:
            raise ConnectionError(dev)

        self.__channels = 10
        self.channels = self.__channels
        self.__delay = 12

        self.driver.query('01t?')
        self.driver.read_raw()

    @property
    def channels(self):
        return self.__channels

    @channels.setter
    def channels(self, nb):
        if not isinstance(nb, int):
            raise TypeError('"nb" has to be int type')
        self.__channels = nb
        self.__values = [0] * nb

    @property
    def delay(self):
        return self.__delay

    @delay.setter
    def delay(self, seconds):
        if not isinstance(seconds, int):
            raise TypeError('"seconds" has to be int type')
        if seconds < 4:
            self.warn('Query interval time is lower than 4 seconds. '
                      'It is not recommended!')
        self.__delay = seconds

    @threadsafe
    @Memoize(timeout=15)
    def log(self):
        try:
            self.driver.read_raw()
            values = []
            for ch in range(1, self.__channels + 1):
                values.append(self.get_temp(ch))
            self.__values = values
            return ';'.join(
                ['T%02d=%.2f[°C]' % (i + 1, item)
                 for (i, item) in enumerate(values)])
        except:
            return 'T=?'

    @threadsafe
    def get_temp(self, channel_nb):
        self.driver.read_raw()
        self.driver.write('02K%s' % str(channel_nb).zfill(2))
        sleep(self.__delay)
        self.driver.read_raw()
        value = self.driver.query('01t?', delay=1)
        return float(value)

    def get_info(self):
        return 'Czaki WRT9'

    def release(self):
        self.driver.close()

    def telemetry_stats(self):
        self.log()
        return [dict(temp=v) for v in self.__values]


class phyKeysight34972A(GenericThermo):
    """Interfejs sterowania termoparami `Keysight 34972A`.

        .. image:: keysight_thermo.jpg
            :scale: 35 %

    Note:
        Używaj głównego interfejsu `GenericThermo`. Skrypty
        testowe wówczas są uniwersalne.
        Tej funkcjonalność używaj, gdy potrzebujesz unikalnych cech
        urządzenia `Keysight 34972A`.

        * upewnij się, że sterowanie zdalne jest aktywne

    Warning:
        Nie twórz obiektów urządzeń bezpośrednio.
        Użyj do tego celu fabryki obiektów **tester.lab.LabFactory**!

    Args:
        dev (gluon.storage.Storage): Obiekt danych urządzenia.
            Dane są strukturą pobieraną z bazy danych.
    """
    def __init__(self, dev):
        super(phyKeysight34972A, self).__init__(dev)
        try:
            self.driver = rm.open_resource(
                self.settings.connstr,
                access_mode=AccessModes.exclusive_lock,
                open_timeout=1000)
        except BaseException:
            raise ConnectionError(dev)

        self.__channels = 12
        self.channels = self.__channels

    @property
    def channels(self):
        return self.__channels

    @channels.setter
    @threadsafe
    def channels(self, nb):
        if not isinstance(nb, int):
            raise TypeError('"nb" has to be int type')
        self.__channels = nb
        self.__values = [0] * nb
        self.driver.query('CONF:TEMP TC,J,(@101:%d)' % (100 + self.__channels))
        self.driver.query('UNIT:TEMP C,(@101:%d)' % (100 + self.__channels))
        self.driver.query(
            'ROUT:CHAN:DEL:AUTO ON,(@101:%d)' % (100 + self.__channels))

    @threadsafe
    @Memoize(timeout=50)
    def log(self):
        try:
            self.driver.query('ROUT:SCAN (@101:%d)' % (100 + self.__channels))
            values = [float(x) for x in self.driver.query('READ?').split(',')]
            self.__values = values
            return ';'.join(
                ['T%02d=%.2f[°C]' % (i + 1, item)
                 for (i, item) in enumerate(values)])
        except:
            return 'T=?'

    @threadsafe
    def get_temp(self, channel_nb):
        self.driver.query('ROUT:SCAN (@%d)' % (100 + channel_nb))
        return float(self.driver.query('READ?'))

    def get_info(self):
        return self.driver.query('*IDN?')

    def release(self):
        self.driver.close()

    def telemetry_stats(self):
        self.log()
        return [dict(temp=v) for v in self.__values]
