#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Moduł z klasami dla obsługi mierników (multimetrów na ten moment).
"""
from abc import ABCMeta, abstractmethod, abstractproperty
from ..errors import PropertyNotSupported, ConnectionError
from .base import BaseDevice
from . import rm
from pyvisa.constants import AccessModes
from tester.base import threadsafe


class GenericMultimeter(BaseDevice):
    """:ted:`Główny interfejs sterowania multimetrem.`

    Note:
        W miarę możliwości korzystaj
        tylko z niego. Pozwala zapewnić, że skrypt są uniwersalne.

    Warning:
        Nie twórz obiektów urządzeń bezpośrednio.
        Użyj do tego celu fabryki obiektów **tester.lab.LabFactory**!

    Args:
        dev (gluon.storage.Storage): Obiekt danych urządzenia.
            Dane są strukturą pobieraną z bazy danych.
    """
    __metaclass__ = ABCMeta

    def __init__(self, dev):
        super(GenericMultimeter, self).__init__(dev)

    @abstractproperty
    def volts_dc(self):
        """:obj:`float`: zwraca pomiar napięcia (stałego)"""
        pass

    @abstractproperty
    def volts_ac(self):
        """:obj:`float`: zwraca pomiar napięcia (zmiennego)"""
        pass

    @abstractproperty
    def curr_dc(self):
        """:obj:`float`: zwraca pomiar natężenia prądu (stałego)"""
        pass

    @abstractproperty
    def curr_ac(self):
        """:obj:`float`: zwraca pomiar natężenia prądu (zmiennego)"""
        pass

    @abstractproperty
    def res(self):
        """:obj:`float`: zwraca pomiar rezystancji"""
        pass

    @abstractproperty
    def fres(self):
        """:red:`nieużywane na ten moment`"""
        pass

    @abstractproperty
    def freq(self):
        """:obj:`float`: zwraca częstotliwośc prądu zmiennego"""
        pass

    @abstractproperty
    def period(self):
        """:red:`nieużywane na ten moment`"""
        pass

    @abstractproperty
    def temp(self):
        """:obj:`float`: zwraca pomiar temperatury"""
        pass

    @abstractmethod
    def configure(self, name, range, resolution):
        """:red:`nieużywane na ten moment`"""
        pass


class phyM3500A(GenericMultimeter):
    """Interfejs sterowania miernikiem `M3500`.

        .. image:: picotest.jpg
            :scale: 35 %

    Note:
        Używaj głównego interfejsu `GenericMultimeter`. Skrypty
        testowe wówczas są uniwersalne.
        Tej funkcjonalność używaj, gdy potrzebujesz unikalnych cech
        urządzenia `M3500`.

        * upewnij się, że sterowanie zdalne jest aktywne

    Warning:
        Nie twórz obiektów urządzeń bezpośrednio.
        Użyj do tego celu fabryki obiektów **tester.lab.LabFactory**!

    Args:
        dev (gluon.storage.Storage): Obiekt danych urządzenia.
            Dane są strukturą pobieraną z bazy danych.
    """
    def __init__(self, dev):
        super(phyM3500A, self).__init__(dev)
        try:
            self.driver = rm.open_resource(
                self.settings.connstr,
                access_mode=AccessModes.exclusive_lock,
                open_timeout=1000)
        except BaseException:
            raise ConnectionError(dev)
        self.__write('SYST:REM')
        self.__write('SYST:BEEP')

    @threadsafe
    def __write(self, cmd):
        self.driver.write(cmd)
        self.driver.write('SYST:BEEP')

    @threadsafe
    def __query(self, cmd):
        self.driver.write('SYST:BEEP')
        return self.driver.query(cmd)

    def get_info(self):
        return self.__query('*IDN?')

    @property
    def volts_dc(self):
        return float(self.__query('MEAS:VOLT:DC?'))

    @property
    def volts_ac(self):
        return float(self.__query('MEAS:VOLT:AC?'))

    @property
    def curr_dc(self):
        return float(self.__query('MEAS:CURR:DC?'))

    @property
    def curr_ac(self):
        return float(self.__query('MEAS:CURR:AC?'))

    @property
    def res(self):
        return float(self.__query('MEAS:RES?'))

    @property
    def fres(self):
        return float(self.__query('MEAS:FRES?'))

    @property
    def freq(self):
        return float(self.__query('MEAS:FREQ?'))

    @property
    def period(self):
        return float(self.__query('MEAS:PER?'))

    @property
    def temp(self):
        return float(self.__query('MEAS:TEMP?'))

    def configure(self, name, range, resolution):
        if not (isinstance(range, (int, long, float)) and
                isinstance(resolution, (int, long, float))):
            raise TypeError()
        name = name.upper()
        name = 'VOLT:DC' if name == 'VOLT' else name
        name = 'CURR:DC' if name == 'CURR' else name
        if name not in ['VOLT:DC', 'CURR:DC', 'VOLT:AC', 'CURR:AC'
                        'RES', 'FRES', 'REQ', 'PER']:
            raise PropertyNotSupported(name)
        self.__query('%s %f,%f')

    def release(self):
        self.__write('SYST:BEEP')
        self.__write('SYST:LOC')
        self.driver.close()


class phyChroma636x(GenericMultimeter):
    """Interfejs sterowania miernikiem `Chroma636x`.

        .. image:: chroma636x.jpg
            :scale: 35 %

    Note:
        Używaj głównego interfejsu `GenericMultimeter`. Skrypty
        testowe wówczas są uniwersalne.
        Tej funkcjonalność używaj, gdy potrzebujesz unikalnych cech
        urządzenia `Chroma636x`.

        * upewnij się, że sterowanie zdalne jest aktywne

    Warning:
        Nie twórz obiektów urządzeń bezpośrednio.
        Użyj do tego celu fabryki obiektów **tester.lab.LabFactory**!

    Args:
        dev (gluon.storage.Storage): Obiekt danych urządzenia.
            Dane są strukturą pobieraną z bazy danych.
    """
    def __init__(self, dev):
        super(phyChroma636x, self).__init__(dev)
        try:
            self.driver = rm.open_resource(
                self.settings.connstr,
                access_mode=AccessModes.exclusive_lock,
                open_timeout=5000)
        except BaseException:
            raise ConnectionError(dev)
        self.driver.read_termination = '\n'
        self.driver.write_termination = '\n'
        self.driver.timeout = 10000
        self.__write('SYST:REM')
        self.__ch = 1

    @property
    def ch(self):
        return int(self.__query('CHAN?'))

    @ch.setter
    def ch(self, value):
        if not isinstance(value, int):
            raise TypeError('"channel" has to be int type')
        self.__write('CHAN %d' % value)

    @threadsafe
    def __write(self, cmd):
        self.driver.write(cmd)

    @threadsafe
    def __query(self, cmd):
        return self.driver.query(cmd)

    def get_info(self):
        info = self.__query('*IDN?')
        info += '\nAvailable channels: %s-%s' % (
            self.__query('CHAN? MIN'), self.__query('CHAN? MAX'))
        return info

    @property
    def pwr_dc(self):
        return float(self.__query('MEAS:POW?'))

    @property
    def volts_dc(self):
        return float(self.__query('MEAS:VOLT?'))

    @property
    def volts_ac(self):
        raise NotImplementedError()

    @property
    def curr_dc(self):
        return float(self.__query('MEAS:CURR?'))

    @property
    def curr_ac(self):
        raise NotImplementedError()

    @property
    def res(self):
        raise NotImplementedError()

    @property
    def fres(self):
        raise NotImplementedError()

    @property
    def freq(self):
        raise NotImplementedError()

    @property
    def period(self):
        raise NotImplementedError()

    @property
    def temp(self):
        raise NotImplementedError()

    def configure(self, name, range, resolution):
        raise NotImplementedError()

    def log(self):
        try:
            return ('V=%(v).3f[V];I=%(i).3f[A];P=%(p).3f[W];'
                    '%(v)f;%(i)f;%(p)f') % dict(
                v=self.volts_dc, i=self.curr_dc, p=self.pwr_dc)
        except IndexError:
            return 'V=?;I=?;P=?;-1;-1;-1'

    def release(self):
        self.__write('SYST:LOC')
        self.driver.close()
