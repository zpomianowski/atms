#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
Moduł z klasami do zdalnego sterowania STB.
"""
from abc import ABCMeta, abstractmethod
from ..errors import ConnectionError, Error
from .base import BaseDevice
from . import rm
from pyvisa.constants import AccessModes
from time import sleep
from tester.base import threadsafe
from gluon.storage import Storage


GENERIC = 'cmds_generic'
D8 = 'cmds_d8'
D30 = 'cmds_d30'
BLIZOO = 'cmds_blizoo'
VECTRA = 'cmds_vectra'
_GENERIC = {
    'STANDBY': '1e',
    'MUTE': '20',
    'KEY_0': '09',
    'KEY_1': '00',
    'KEY_2': '01',
    'KEY_3': '02',
    'KEY_4': '03',
    'KEY_5': '04',
    'KEY_6': '05',
    'KEY_7': '06',
    'KEY_8': '07',
    'KEY_9': '08',
    'INFO': '21',
    'MENU': '6e',
    'VOL_UP': '13',
    'VOL_DOWN': '12',
    'OPTIONS': '35',
    'EXIT': '30',
    'CH_UP': '60',
    'CH_DOWN': '61',
    'OK': '14',
    'NAV_UP': '59',
    'NAV_LEFT': '0b',
    'NAV_RIGHT': '0a',
    'NAV_DOWN': '5a',
    'BACK': '5d',
    'X': '5d',
    'PVR': '6d',
    'RED': '55',
    'GREEN': '56',
    'YELLOW': '57',
    'BLUE': '58',
    'SETTINGS': '54',
    'EPG': '0f',
    'REWIND': '66',
    'PAUSE_PLAY': '6b',
    'REC': '6a',
    'FORWARD': '67',
    'STOP': '69',
    'TXT': '33',
    'CP': '6e'
}
_D8 = {
    # dodać kody nadpisujące konf GENERIC lub rozszerzające możłiwości
    # o nowe przyciski
}
_D30 = {
    # dodać kody nadpisujące konf GENERIC lub rozszerzające możłiwości
    # o nowe przyciski
}
_BLIZOO = {
    'MUTE': '0d',
    'STANDBY': '0c',
    'PAUSE_PLAY': '32',
    'REWIND': '31',
    'FORWARD': '34',
    'STOP': '35',
    'REC': '2c',
    'NAV_UP': '10',
    'NAV_LEFT': '15',
    'OK': '24',
    'NAV_RIGHT': '16',
    'BACK': '1f',
    'NAV_DOWN': '11',
    'EXIT': '21',
    'VOL_UP': '14',
    'EPG': '0b',
    'CH_UP': '0e',
    'VOL_DOWN': '17',
    'INFO': '1b',
    'CH_DOWN': '0a',
    'VOD': '13',
    'OPT': '1e',
    'RED': '38',
    'GREEN': '0f',
    'YELLOW': '18',
    'BLUE': '22',
    'KEY_0': '00',
    'KEY_1': '01',
    'KEY_2': '02',
    'KEY_3': '03',
    'KEY_4': '04',
    'KEY_5': '05',
    'KEY_6': '06',
    'KEY_7': '07',
    'KEY_8': '08',
    'KEY_9': '09',
    'SETUP': '1a',
    'TXT': '2f'

}
_VECTRA = {
    'STANDBY': '47',
    'DISPLAY': '51'
}


class GenericRemoteCtrl(BaseDevice):
    """:ted:`Główny interfejs sterowania STB.`

    Note:
        W miarę możliwości korzystaj
        tylko z niego. Pozwala zapewnić, że skrypt są uniwersalne.

    Warning:
        Nie twórz obiektów urządzeń bezpośrednio.
        Użyj do tego celu fabryki obiektów **tester.lab.LabFactory**!

    Args:
        dev (gluon.storage.Storage): Obiekt danych urządzenia.
            Dane są strukturą pobieraną z bazy danych.
    """
    __metaclass__ = ABCMeta

    def __init__(self, dev):
        super(GenericRemoteCtrl, self).__init__(dev)

    @abstractmethod
    def send(self, pin_nb, code, longpress=False):
        """Wysyła komendę do STB. Symuluje klasyczny pilot TV.

        Args:
            pin_nb (int): numer diody/pilota
            code (str): kod klawisza do wysłania
        """
        pass


class phyIrduino(GenericRemoteCtrl):
    """Interfejs sterowania komorą `Irduino`.

        .. image:: irduino.jpg
            :scale: 35 %

    Note:
        Używaj głównego interfejsu `GenericRemoteCtrl`. Skrypty
        testowe wówczas są uniwersalne.
        Tej funkcjonalność używaj, gdy potrzebujesz unikalnych cech
        urządzenia `Irduino`.

        `Irduino` to projekt na bazie popularnego `Arduino`. \
        Twórcą jest `Dariusz Wieczorek <dwieczorek@cyfrowypolsat.pl>`_. \
        Wszelkie pytania na temat szczegółów działania należy \
        kierować do niego.

    Warning:
        Nie twórz obiektów urządzeń bezpośrednio.
        Użyj do tego celu fabryki obiektów **tester.lab.LabFactory**!

    Args:
        dev (gluon.storage.Storage): Obiekt danych urządzenia.
            Dane są strukturą pobieraną z bazy danych.
    """
    GUARD_INTERVAL = .3

    def __init__(self, dev):
        super(phyIrduino, self).__init__(dev)
        try:
            self.driver = rm.open_resource(
                self.settings.connstr,
                access_mode=AccessModes.exclusive_lock,
                open_timeout=5000)
        except BaseException:
            raise ConnectionError(dev)
        self.driver.timeout = 7000
        self.driver.baud_rate = 19200
        self.codes = Storage(_GENERIC)
        # First byte structure
        # |toggle|echo|sys1|sys0|mask0|mask1|mask2|mask3|
        # final should be: self.__fbyte = '01000000'
        self.__fbyte = '01001111'
        self.__standard = '00'

    def set_type(self, type):
        if type.lower() == BLIZOO:
            self.__standard = '01'
            self.codes.update(_BLIZOO)
        elif type.lower() == D8:
            self.__standard = '00'
            self.codes.update(_D8)
        elif type.lower() == D30:
            self.__standard = '00'
            self.codes.update(_D30)
        elif type.lower() == VECTRA:
            self.__standard = '11'
            self.codes.update(_VECTRA)
        else:
            self.__standard = '00'
            self.codes.update(_GENERIC)

    def __get_fbyte_hex(self, pin_nb, toggle=False):
        pin_nb = pin_nb if pin_nb < 5 else 4
        new = list(self.__fbyte)
        new[0] = int(toggle)
        new[2:4] = list(self.__standard)
        new[3 + pin_nb] = 1
        return str(hex(int(''.join([str(d) for d in new]), 2)))[2:]

    @threadsafe
    def send(self, pin_nb, code, longpress=False):
        tries = 0
        v = None
        while not v and tries < 3:
            try:
                v = self.driver.query(self.__get_fbyte_hex(pin_nb) + code)[:-1]
            except:
                pass
            tries += 1
            sleep(phyIrduino.GUARD_INTERVAL)
        if not v or 'Incorrect' in v:
            Error('Irduino did not accept the command after several tries.')
        else:
            stds = ['CP', 'BLIZOO', 'MULTIMEDIA', 'VECTRA']
            self.debug('Code: %s Key: %s Std: %s' %
                       (code, {v: k for k, v in self.codes.items()}[code],
                        stds[int(self.__standard, 2)]))

    @threadsafe
    def get_info(self):
        v = self.driver.query('?')
        sleep(phyIrduino.GUARD_INTERVAL)
        return v

    def release(self):
        self.driver.close()
