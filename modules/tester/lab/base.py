#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Baza dla każdego urządzenia fizycznego.

Każde urządzenie w laboratorium ma wsparcie poniższych funkcjonalności.
"""
import json
import weakref
import codecs
import Queue
import contextlib
import threading
from time import sleep
from time import time
from urlparse import urlparse
from datetime import datetime
from functools import wraps
from abc import ABCMeta, abstractmethod
from gluon.storage import Storage
from gluon import current
from tester.base import threadsafe, MAIN_THREAD_ID
from ..errors import (SettingsError, ConnectionError,
                      NOTIFY_INFO, NOTIFY_WARN, NOTIFY_ERROR)
from influxdb import InfluxDBClient


class INFLUXJSONEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            return o.isoformat()

        return json.JSONEncoder.default(self, o)


class DBLogger(threading.Thread):
    def __init__(self, baseDeviceObj,
                 log_path, case_id, duts, teds, report_ids, interval=60):
        threading.Thread.__init__(self)
        self.setDaemon(True)
        self.interval = interval
        self.__parent = weakref.proxy(baseDeviceObj)
        self.__flag = True
        self.__log_path = (log_path + '.csv').replace(':', '_')

    def run(self):
        # self.__parent.debug('Auto logging start!')
        try:
            while self.__flag:
                self.dump()
                sleep(self.interval)
            self.dump(info=False)
        except weakref.ReferenceError:
            pass

    # TODO - save to db to be done
    def dump(self, info=True):
        if info and self.__flag:
            self.__dump2file(self.__parent._info())
        q = self.__parent._q
        while not q.empty():
            self.__dump2file(q.get(False))
            q.task_done()

    def __dump2file(self, item):
        superdata = ''
        # try:
        #     superdata = '[logger_qsize: %d]' % self.__parent._q.qsize()
        # except:
        #     pass
        with codecs.open(self.__log_path, 'a', encoding='utf-8') as f:
            item.timestamp = item.timestamp.strftime('%x %A %X')
            if item.timestamp_end:
                item.timestamp += (
                    ' - %s' % item.timestamp_end.strftime('%X'))
            item.msg = unicode(str(item.msg), 'utf-8')
            item.level = item.level.upper()
            item.superdata = superdata
            f.write(
                '%(level)5s;%(timestamp)s;%(msg)s;%(superdata)s\n' % item)

    def stop(self):
        self.__flag = False
        # if self.is_alive():
        #     try:
        #         self.__parent.debug('Auto logging stop!')
        #     except weakref.ReferenceError:
        #         pass


class BaseDevice(object):
    """Klasa bazowa dla każdego urządzenia fizycznego w laboratorium.

    Jest to klasa abstrakcyjna. Nie można tworzyć jej instancji.
    Służy jako interfejs.

    Warning:
        Nie twórz obiektów urządzeń bezpośrednio.
        Użyj do tego celu fabryki obiektów **tester.lab.LabFactory**!

    Args:
        dev (gluon.storage.Storage): Obiekt danych urządzenia.
            Dane są strukturą pobieraną z bazy danych.

    Attributes:
        LOG_INTERVAL (int): Interwał logowania kondycji urządzenia.
    """
    WARN = 'warn'
    INFO = 'info'
    ERROR = 'error'
    DEBUG = 'debug'
    LOG_INTERVAL = 60
    ERR_INTERVAL = 10
    PRINT_LINES_LIMIT = 300

    MAX_RECONNECTIONS = 3

    WARN_MSG_RECONNECT = 'Problem with connection. Trying to reconnect!'

    @classmethod
    def reconnection_handler(cls, func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            try:
                return func(*args, **kwargs)
            except:
                objref = args[0]

                tries = (BaseDevice.MAX_RECONNECTIONS
                         if threading._get_ident() == MAIN_THREAD_ID else 0)

                for i in range(tries):
                    try:
                        sleep(BaseDevice.ERR_INTERVAL * (i + 1))
                        objref._connect()
                        return func(*args, **kwargs)
                    except:
                        objref.warn('Could not send the command [%d/%d]' %
                                    (i + 1, BaseDevice.MAX_RECONNECTIONS))
                objref.error('Connection with the device lost!')
                raise ConnectionError(args[0].dev)

        return wrapper

    def __init__(self, dev):
        self.dev = dev
        self.load_defaults()
        self.__log_interval = BaseDevice.LOG_INTERVAL
        self.dblog = None
        self.dblog_options = None
        self._q = Queue.Queue(maxsize=100)
        self.__log_root_path = None
        self.logger = None

        self.driver = None

        self._testMethodName = '_'

        self.__print_cnt = 0
        self.__binded_devs = []

        try:
            self.logger = current.logger
        except:
            pass

        self._setup_influx()
        self._connect()

        self.__channelsmap = {}

    # @abstractmethod - aktywować kiedy wszystkie klasy będą zmigrowane
    def _connect(self):
        """Metoda odpowiedzialna za łączenie i ponowne łączenie z urządzeniem.

        W ramach implementacji, zainicjuj połączenie. Uwzględnij przypadek,
        kiedy połączenie już istnieje, ale należy je zwolnić i zainicjować
        jeszcze raz (np. w razie problemów sieciowych).

        Warning:
            Bardzo ważna metoda odpowiadająca za inicjalizację interfejsu \
            sterującego. Zadbaj o rzetelną implementację.
        """
        pass

    # it is not critical feature
    def _setup_influx(self):
        client = None
        dbname = self._get_influxdb_name()
        try:
            params = urlparse(current.settings.influxdb)
            client = InfluxDBClient(
                params.hostname, params.port,
                params.username, params.password,
                dbname,
                timeout=5)
            client.create_database(dbname)
            client.alter_retention_policy(
                'autogen', dbname, '180d', None, True)
        except Exception as err:
            self.warn('Influx client not connected')
        self.influx_client = client

    def _get_influxdb_name(self):
        return current.settings.influxdb_name

    def _log_influx(self):
        interface = ''.join(
            [str(b.__name__) for b in self.__class__.__bases__]).lower()
        devcls = self.__class__.__name__.lower()
        client = self.influx_client
        if not client:
            return
        try:
            payload = self.telemetry_stats()
        except NotImplementedError:
            return
        if not isinstance(payload, (list, tuple)):
            payload = [payload]
        for i, item in enumerate(payload):
            if item is None:
                continue
            channel_name = self.__channelsmap.get(i, None)
            channel_name = channel_name if channel_name else ''
            load = [dict(measurement='log',
                         time=datetime.utcnow().isoformat() + 'Z',
                         tags=dict(
                             channel=str(i).zfill(2),
                             channel_name=channel_name,
                             host=self.dev.host,
                             interface=interface,
                             model=devcls,
                             id=self.dev.id),
                         fields=item)]
            try:
                client.write_points(load)
            except Exception as err:
                self.logger.warn('influx: %s' % str(err))

    def reset_channel_name(self, chnb):
        self.set_channel_name(chnb, None)

    def set_channel_name(self, chnb, name=None):
        if name is None:
            del self.__channelsmap[chnb]
            return
        self.__channelsmap[chnb] = name

    def chname(self, chnb, name=None):
        self.set_channel_name(chnb, name)

    def __log_event(self, msg, level):
        import socket
        client = self.influx_client
        if not client:
            return
        payload = [dict(
            measurement='events',
            time=datetime.utcnow().isoformat() + 'Z',
            tags=dict(
                host=socket.gethostname(),
                level=level),
            fields=dict(msg=msg))]
        try:
            client.write_points(payload)
        except Exception as err:
            self.logger.warn('influx: %s' % str(err))

    def telemetry_stats(self):
        """Telemetria. Zwraca parametry mierzalne.

        Returns:
            dict: Słownik pomiarów.

        Note:
            Przeciąż dla nowej klasy urządzenia.
        """
        raise NotImplementedError

    def bind(self, *devs):
        self.__binded_devs = [weakref.proxy(d) for d in devs]

    def unbind(self):
        self.__binded_devs = []

    @property
    def test_method_name(self):
        return self._testMethodName

    @test_method_name.setter
    def test_method_name(self, name):
        self._testMethodName = name

    def get_log_dir(self):
        """Zwraca aktualną ścieżkę pliku logów.

        Returns:
            str: Ścieżka do pliku z logiem o ile logger jest skonfigurowany.
        """
        return self.__log_root_path

    def _set_logger(
            self, case_id, duts, teds, reports_ids,
            folder='/tmp', tmname='_', filename='_.csv'):
        from os import path
        self.test_method_name = tmname
        self.__log_root_path = folder
        self.dblog_options = [path.join(
            folder, '%s.%s' % (tmname, filename)),
            case_id, duts, teds, reports_ids]
        self.dblog = DBLogger(
            self, *self.dblog_options, interval=self.__log_interval)

    @threadsafe
    def __print(self, msg):
        MAX_L = 32
        if self.__print_cnt > BaseDevice.PRINT_LINES_LIMIT:
            self.__print_cnt = 0
            print '!clear!\n...'
        s = '[%s:%d] %s\t%s' % (
            self.dev.model.upper(),
            self.dev.id,
            datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
            msg)
        print s
        self.__print_cnt += 1
        return (s[:MAX_L] + '..') if len(s) > MAX_L else s

    def _info(self):
        timestamp = datetime.now()
        timestamp_end = None
        msg = [self.log()]
        self._log_influx()        
        for d in self.__binded_devs:
            try:
                d._log_influx()        
                msg.append('[%s:%d] %s' % (
                    d.dev.model.upper(),
                    d.dev.id, d.log().replace(';', ' ')))
            except:
                pass
        timestamp_end = datetime.now()
        return Storage(
            timestamp=timestamp,
            timestamp_end=timestamp_end,
            level=BaseDevice.INFO, msg=';'.join(msg), extra_dict={})

    def info(self, msg, extra_dict=None):
        """Publikuje log typu `INFO`.

        Args:
            msg (str): Wiadomość.
            extra_dict (dict): Serializowalna struktura \
                dodatkowych danych. Domyślnie `None`.
        """
        self.__log(BaseDevice.INFO, msg=msg, extra_dict=extra_dict)
        # NOTIFY_INFO("%s id: %d - %s" % (self.dev.model, self.dev.id, msg))
        self.__print('[%s] %s' % (BaseDevice.INFO.upper(), msg))

    def debug(self, msg, extra_dict=None):
        """Publikuje log typu `DEBUG`.

        Args:
            msg (str): Wiadomość typu `DEBUG`.
            extra_dict (dict): Serializowalna struktura \
                dodatkowych danych. Domyślnie `None`.
        """
        self.__log(BaseDevice.DEBUG, msg, extra_dict=extra_dict)
        # NOTIFY_INFO("%s id: %d - %s" % (self.dev.model, self.dev.id, msg))
        self.__print('[%s] %s' % (BaseDevice.DEBUG.upper(), msg))

    def warn(self, msg, extra_dict=None):
        """Publikuje log typu `WARNING`.

        Args:
            msg (str): Wiadomość typu `WARNING`.
            extra_dict (dict): Serializowalna struktura \
                dodatkowych danych. Domyślnie `None`.
        """
        self.__log(BaseDevice.WARN, msg, extra_dict=extra_dict)
        # NOTIFY_WARN("%s id: %d - %s" % (self.dev.model, self.dev.id, msg))
        s = self.__print('[%s] %s' % (BaseDevice.WARN.upper(), msg))
        self.__log_event(s, BaseDevice.WARN.upper())

    def error(self, msg, extra_dict=None):
        """Publikuje log typu `ERROR`.

        Args:
            msg (str): Wiadomość typu `ERROR`.
            extra_dict (dict): Serializowalna struktura \
                dodatkowych danych. Domyślnie `None`.
        """
        self.__log(BaseDevice.ERROR, msg, extra_dict=extra_dict)
        NOTIFY_ERROR("%s id: %d - %s" % (self.dev.model, self.dev.id, msg))
        s = self.__print('[%s] %s' % (BaseDevice.ERROR.upper(), msg))
        self.__log_event(s, BaseDevice.ERROR.upper())

    def check(self, msg=None, extra_dict=None):
        """Wysyła żądanie wykonania wpisu w logach.

        Metoda wywołuje metodę `log` na aktualnym obiekcie i wszystkich
        powiązanych przez `bind`. Logi poszczególnych obiektów są konkatenowane.

        Args:
            msg (str): opcjonalna wiadomość dodawana na koniec wpisu
        """
        _msg = self._info().msg
        if msg:
            _msg += ';' + msg
        self.__log(BaseDevice.INFO, _msg, extra_dict=extra_dict)
        self.__print('[%s] %s' % (BaseDevice.INFO.upper(), _msg))
        return _msg

    def start_log(self):
        """Start automatycznego logowania co `LOG_INTERVAL` sekund."""
        if self.dblog is not None and self.dblog.is_alive():
            self.dblog.stop()
        self.dblog = DBLogger(self, *self.dblog_options,
                              interval=self.__log_interval)
        self.dblog.start()

    def stop_log(self):
        """Stop automatycznego logowania co `LOG_INTERVAL` sekund."""
        if self.dblog is not None:
            self.dblog.stop()

    def __log(self, level, msg, extra_dict={}):
        try:
            if self.logger:
                msg = '%s;%s' % \
                    (msg, str(extra_dict))
                if level == BaseDevice.DEBUG:
                    self.logger.debug(msg)
                if level == BaseDevice.ERROR:
                    self.logger.error(msg)
                if level == BaseDevice.INFO:
                    self.logger.info(msg)
                if level == BaseDevice.WARN:
                    self.logger.warn(msg)
            self._q.put_nowait(
                Storage(timestamp=datetime.now(),
                        level=level, msg=msg, extra_dict=extra_dict))
        except Queue.Full:
            if self.logger:
                self.logger.error('Msg queue is full!')
        finally:
            self._log_influx()
            if self.dblog and not self.dblog.isAlive():
                self.dblog.dump(info=False)

    @abstractmethod
    def get_info(self):
        """Informacje ogólne o urządzeniu.

        Returns:
            str: Opis urządzenia.
        """
        pass

    def log(self):
        """Wiadomość `INFO` generowana podczas automatycznego logowania.

        Returns:
            str: Wiadomość typu `INFO`.

        Note:
            Metoda abstracyjna. Przeciąż dla nowej klasy urządzenia.
        """
        return 'To be implemented'

    def load_defaults(self, defaults={'connstr': None}):
        """Ładuje ustawienia i konfigurację dla urządzenia.

        Args:
            defaults (dict): Dodatkowe dane \
                potrzebne dla poprawnej obslugi urządzenia.
        """
        defaults.update(self.dev.settings[0])
        self.settings = Storage(defaults)
        if not self.settings.connstr:
            raise SettingsError('connstr')

    def save_settings(self):
        """Zapisuje ustawienia i konfigurację urządzenia do bazy danych."""
        self.dev.update_record(settings=[self.settings])
        current.db.commit()

    @abstractmethod
    def release(self):
        """Zwalnia zasoby urządzenia.

        Warning:
            Bardzo ważna metoda odpowiadająca za zwalnianie zasobów \
            urządzenia. Zadbaj o rzetelną implementację.
        """
        pass

    def close(self):
        self.release()

    # very dirty, piece of shit solution
    def __del__(self):
        try:
            self.release()
        except:
            pass
