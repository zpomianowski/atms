#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Moduły bazowy testera. Podstawowe składniki odpowiedzialne za workflow testów.

Note:
    Regularny użytkownik tutaj nic użytecznego nie znajdzie ;)
    No chyba, że bardzo interesuje go dogłębna analiza jak to się wszystko
    kręci - samo przestudiowanie tego modułu jednak to za mało.
    Musisz się nastawić na długie dupogodziny.
"""

# Maybe you need to change isolation level in db to avoid deadlocks.
#
# Connect to db: sudo -u <user> mysql -u <db_user> -p<db_user_passw> <database>
#
# Query the isolation level:
# SELECT @@GLOBAL.tx_isolation, @@tx_isolation;
#
# Set new isolation level (REPEATABLE READ, READ COMMITTED, etc.):
# SET GLOBAL tx_isolation='REPEATABLE READ';
# SET tx_isolation='REPEATABLE READ';
import os
import imp
import shutil
import tempfile
import unittest
import traceback
import subprocess
import codecs
import threading
import redis
import time
import signal
import Queue
from resource import getrusage, RUSAGE_SELF
from functools import wraps
from datetime import datetime
from datetime import timedelta
from zipfile import ZipFile, ZIP_DEFLATED
from urlparse import urlparse

from gluon import current
from gluon.html import URL, A
from gluon.tools import Mail
from applications.atms.modules.websocket_messaging import websocket_send
from applications.atms.modules.misc import validate_script
from tester.errors import (NOTIFY_START, NOTIFY_STOP, NOTIFY_ERROR,
                           NOTIFY_INFO, NOTIFY_WARN, Error)
from tester import errors
from influxdb import InfluxDBClient


REDIS_CLIENT = None
LOCK = threading.RLock()
MAIN_THREAD_ID = threading._get_ident()


class TestCase(unittest.TestCase):
    """Wspólna klasa bazowa `TestCase'a` dla testów STB i testów mobilnych.

    Args:
        testFunc (str): nazwa funkcji testującej
        parent (obj): obiekt procesu obsługującego test
    """
    def __init__(self, testFunc, parent):
        super(TestCase, self).__init__(testFunc)
        self.__parent = parent
        self._setup_influx()

    def run(self, result=None):
        testm_name = '*' * 3 + ' %s' + ' %s ' % self._testMethodName + '*' * 3
        # TODO - save the start datetime
        self.info_stamp(testm_name % 'START')
        super(TestCase, self).run(result)
        self.info_stamp(testm_name % 'STOP') 
        # TODO - save the stop datetime
        # calculate time, cache it somewhere for future time estimation

    def _setup_influx(self):
        client = None
        dbname = current.settings.influxdb_name
        try:
            params = urlparse(current.settings.influxdb)
            client = InfluxDBClient(
                params.hostname, params.port,
                params.username, params.password,
                dbname,
                timeout=5)
            client.create_database(dbname)
            client.alter_retention_policy(
                'autogen', dbname, '180d', None, True)
        except Exception as err:
            pass
        self.influx_client = client

    def __log(self, msg, level):
        import socket
        client = self.influx_client
        if not client:
            return
        payload = [dict(
            measurement='events',
            time=datetime.utcnow().isoformat() + 'Z',
            tags=dict(
                host=socket.gethostname(),
                level=level),
            fields=dict(msg=msg))]
        try:
            client.write_points(payload)
        except Exception as err:
            NOTIFY_WARN('influx: %s' % str(err))

    def log_stamp(self, msg):
        """Dodaje adnotację `LOG` na oś czasu w bazie `influxdb`.

        Args:
            msg (str): wiadomość
        """
        self.__log(msg, 'LOG')

    def debug_stamp(self, msg):
        """Dodaje adnotację `LOG` na oś czasu w bazie `influxdb`.

        Args:
            msg (str): wiadomość
        """
        self.__log(msg, 'DEBUG')

    def warn_stamp(self, msg):
        """Dodaje adnotację `WARN` na oś czasu w bazie `influxdb`.

        Args:
            msg (str): wiadomość
        """
        self.__log(msg, 'WARN')

    def error_stamp(self, msg):
        """Dodaje adnotację `ERROR` na oś czasu w bazie `influxdb`.

        Args:
            msg (str): wiadomość
        """
        self.__log(msg, 'ERROR')

    def info_stamp(self, msg):
        """Dodaje adnotację `INFO` na oś czasu w bazie `influxdb`.

        Args:
            msg (str): wiadomość
        """
        self.__log(msg, 'INFO')

    def sleep(self, seconds=15, minutes=0, hours=0, days=0):
        """Zawiesza wykonywanie aktualnego wątku na określoną ilość czasu.

        Bardziej rozbudowany wariant standardowej funkcji :obj:`time.sleep`.

        Args:
            seconds (int): ilość sekund uśpienia
            minutes (int): ilość minut uśpienia
            hours (int): ilość godzin uśpienia
            days (int): ilość dni uśpienia
        """
        time.sleep(86400 * days + 3600 * hours + 60 * minutes + seconds)

    def progress(self, precentage):
        """Ustawia procent wykonania zadania.

        Args:
            precentage (int): stopień zaawansowania zadania wyrażony w %
        """
        print('!clear!----%.2f%%' % float(min(precentage, 100)))

    def now(self):
        """Zwraca sygnaturę czasu obecnej chwili.

        Returns:
            :obj:`datetime.datetime`
        """
        return datetime.now()

    def get_stop_time(self, days=0, hours=0, minutes=0, seconds=0):
        """Generuje sygnaturę czasu w przyszłości.

        Przydatne podczas budowania pętli, kiedy coś ma się wykonywać w cyklach
        przez określoną ilość czasu.

        Args:
            days (int): ilość dni dodanych do obecnej chwili
            hours (int): ilość godzin dodanych do obecnej chwili
            minutes (int): ilość minut dodanych do obecnej chwili
            seconds (int): ilość sekund dodanych do obecnej chwili

        Returns:
            :obj:`datetime.datetime`: kiedy
        """
        return datetime.now() + timedelta(
            seconds=seconds + minutes * 60 +
            hours * 3600 + days * 86400)

    def get_left_hours(self, stop_datetime):
        """Szacuje ile godzin zostało do danej chwili.

        Przydatne podczas logowowania.

        Args:
            stop_datetime (datetime.datetime): planowany STOP

        Returns:
            int: ilość godzin
        """
        return (stop_datetime - datetime.now()).total_seconds() // 3600

    def repeat_func_until(self, func, *args, **kwargs):
        """Powtarza jakąś funkcję/metodę przez jakiś czas lub określoną ilość
        razy co określony czas. Metoda ta blokuje aktualny wątek.

        Upraszcza konstrukcję cykli.

        Args:
            func: funkcja, która ma być powtarzana w cyklu
            *args: kolejne argumenty `func`
            **kwargs: kolejne argumenty `func` postaci klucz-wartość
            _cycles (): liczba cykli (ma wyższy priorytet niż poniższe
                argumenty)
            _days, _hours, _minutes, _seconds: they determine time until
                the func will be repeated
            _interval: wait time between func calls, BE CAREFULL - interval
                should be > 0, otherwise it will be CPU consuming
        """
        td = dict(days=0, hours=0, minutes=0, seconds=0,
                  interval=600, cycles=0)
        for k in td:
            ak = '_' + k
            if ak in kwargs:
                td[k] = kwargs[ak]
                del kwargs[ak]

        interval = td['interval']
        cycles = td['cycles']
        stop_time = self.get_stop_time(
            seconds=td['seconds'] + td['minutes'] * 60 +
            td['hours'] * 3600 + td['days'] * 86400)
        if cycles > 0:
            for c in range(cycles):
                func(*args, **kwargs)
                time.sleep(interval)
            return
        else:
            while datetime.now() < stop_time:
                func(*args, **kwargs)
                time.sleep(interval)

    def mail(self, msg, emails=None, email_title=None):
        """Wysyła dowolnego maila do dowolnej grupy osób.

        Args:
            msg (str): wiadomość
            emails (list): email adresata lub lista adresatów
            email_title (str): tytuł maila
        """
        self.__parent.mail(msg, emails, email_title)

    def mail_current_logs(self, emails=[]):
        """Wysyła podsumowanie z logami cząstkowymi testu.

        Args:
            emails (list): email adresata lub lista adresatów
        """
        try:
            import socket
            p = self.__parent
            T = current.T
            T.force(p.lang)

            uri = p.packFiles(include_extra=False)

            msg = T('Partial logs for test method: %s') % \
                self._testMethodName
            email_title = T(
                '[SAUT ATMS] %s - Partial report %s C#%d', lazy=False) % (
                socket.gethostname(), p.case_name, p.case_id)
            p.mail(msg, emails, email_title, attachments=[uri])

            os.unlink(uri)
        except:
            pass

    def wait_for_manual_action(self):
        """Wstrzymuje wykonywanie scenariusza testowego.

        Wznowienie możliwe po maunualnej rekacji
        w menu kontekstowym zakolejkowanego zadania.
        """
        from threading import Timer
        from misc import say
        from time import sleep
        import websocket

        p = self.__parent
        T = current.T
        T.force(p.lang)

        s1 = T('>>> MANUAL ACTION NEEDED <<<', lazy=False)
        s2 = T('>>> Click "Resume" under task context menu <<<', lazy=False)
        s3 = T('Task stopped! Manual action needed', lazy=False)
        s4 = T('Remember - the task needs your attention', lazy=False)
        s5 = T('Task execution resumed', lazy=False)
        s6 = T('>>> TASK RESUMED <<<', lazy=False)


        def on_message(ws, msg):
            if msg == "RESUME":
                ws.close()

        def on_error(ws, error):
            print error

        def on_open(ws):            
            print s1 + '\n' + s2
            say(s3)
            sleep(3)
            self.mail(s1)

        def on_close(ws):
            print s6
            say(s5)
            self.mail(s6)

        # websocket.enableTrace(True)
        wsuri = current.settings.ws_client + (
            "/realtime/" + self.__parent.task_id)
        ws = websocket.WebSocketApp(wsuri,
                                    on_message=on_message,
                                    on_error=on_error,
                                    on_close=on_close,
                                    on_open = on_open)
        
        ws.run_forever(sslopt={"check_hostname": False})
        

class ProcConsumer():
    QSIZE = 10000

    """Class manages a process, children and its logs."""

    def __init__(self, params, tag=None, shell=True, waitForOutput=False,
                 tempdir=None, dfuncs=[], bChannel=None):
        self.bChannel = bChannel
        self.settings = current.settings
        Qsize = 1 if bChannel else ProcConsumer.QSIZE
        self.proc = subprocess.Popen(
            params,
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            bufsize=1,
            shell=shell,
            env=os.environ)
        self.q = Queue.Queue(maxsize=Qsize)
        t1 = threading.Thread(target=self.enqueue_output,
                              args=(self.proc.stdout, self.q, 'LOG'))
        t1.daemon = True
        t1.start()

        t2 = threading.Thread(target=self.enqueue_output,
                              args=(self.proc.stderr, self.q, 'ERR'))
        t2.daemon = True
        t2.start()

        tag = tag if tag else params.split()[0]
        self.tmpfile = os.path.join(
            tempdir, '%s_pid%s.log' % (tag, self.proc.pid))

        self.waitForOutput = waitForOutput
        maxiters = 30
        iters = 0
        while self.waitForOutput or iters > maxiters:
            time.sleep(1)
            iters += 1

        for d in dfuncs:
            t = threading.Thread(target=d)
            t.daemon = True
            t.start()

    def getProc(self):
        return self.proc

    def kill(self):
        """Specific for Linux OS."""
        def kill_child_processes(parent_pid, sig=signal.SIGTERM):
            ps_command = subprocess.Popen(
                "ps -o pid --ppid %d --noheaders" % parent_pid,
                shell=True, stdout=subprocess.PIPE)
            ps_output = ps_command.stdout.read()
            ps_command.wait()
            for pid_str in ps_output.split("\n")[:-1]:
                    os.kill(int(pid_str), sig)
        kill_child_processes(self.proc.pid)
        # self.proc.kill()
        os.kill(self.proc.pid, signal.SIGTERM)
        return self.proc.poll()

    def enqueue_output(self, out, queue, mtype):
        for line in iter(out.readline, b''):
            try:
                queue.put_nowait((mtype, datetime.utcnow(), line))
            except Queue.Full:
                self.__emptyQ()
            self.waitForOutput = False
        out.close()

    def getLogPath(self):
        return self.__emptyQ()

    def __emptyQ(self):
        if self.bChannel:
            while True:
                try:
                    line = self.q.get_nowait()
                    websocket_send(self.settings.ws_server, line[2],
                                   self.settings.security_key, self.bChannel)
                except Queue.Empty:
                    return None
        # with codecs.open(self.tmpfile, "a", "utf-8") as f:
        with open(self.tmpfile, "a") as f:
            while True:
                try:
                    line = self.q.get_nowait()
                    f.write("%s [%s] %s\n" % (line[0], line[1], line[2]))
                except Queue.Empty:
                    return self.tmpfile


def global_sync(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        global REDIS_CLIENT
        if not REDIS_CLIENT:
            REDIS_CLIENT = redis.Redis(host=current.settings.redis_url)
        with REDIS_CLIENT.lock('global_atms_lock'):
            return func(*args, **kwargs)
    return wrapper


def threadsafe(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        with LOCK:
            return func(*args, **kwargs)
    return wrapper


class Timer(threading.Thread):
    SLEEP = 1500

    def __init__(self, target):
        self._target = target
        threading.Thread.__init__(self)
        self.setDaemon(True)
        self.stop = False

    def run(self):
        while not self.stop:
            time.sleep(Timer.SLEEP)
            self._target()


class AbstractTestProc(object):

    """Convention to run all kind of test cases in ATMS.

    Derive this class to support specific scenarios and test types.

    Note:
        Do not call it directly in the code. Use inheritance.
        There may be a need to apadapt 'task_api' in 'manager' control
        to provide proper task handling.

    Attributes:
        db (DAL): Reference to the DAL object.
        tmp (path): path to the temporary location test files are generated.
        files (Dictionary[path]): dict of all generated files during the test.
        script_id (int): reference to the file with test scenario stored in db.
        case_id (int): reference to the test case.
        funcs (List[str]): test functions defined in test script.
        report_id (int): reference to the generated report
            (results of the test).
    """

    TIMEOUT = 10
    VERBOSITY = 2
    PARTIAL_SLUG = 'atmscase%d-report-%s'
    MASTER_SLUG = 'atmscase%d'
    TAG = 'autotest'
    TASK_ID_DEBUG = 'debug'

    def __init__(self, case_id, script_id, funcs=[],
                 duts=[], teds=[], bins=[], user_id=None, lang='en'):
        """Init the test environment.

        Args:
            case_id (int): test case reference.
            script (row): the script entry (not abspath) stored in db.
            funcs (List[str]): test functions defined in test script.
            duts (List[int]): list of DUTs references stored in
                db.db_attachments
            teds (List[int]): list of managable Test Equipment Devices
                (power supplies, measurement devices, climate chambers etc.)
            bins (List[int]: list of binary files, example: apk to be tested)
        """
        self.__lock = threading.RLock()
        self.db = current.db

        # prepare temporary directory for result files
        try:
            task_id = 'taskId%s' % (current.W2P_TASK.id)
        except:
            task_id = AbstractTestProc.TASK_ID_DEBUG
        self.task_id = task_id
        tmpp = "%s_caseId%s_duts%s" % (task_id, str(case_id), '_'.join(
            [str(dut_id) for dut_id in duts]))
        self.tmp = os.path.join(tempfile.gettempdir(), tmpp)

        self.script_id = script_id
        self.case_id = case_id
        self.case_name = self.db.atms_cases[case_id].name
        self.funcs = funcs
        self.duts = duts
        self.teds = teds
        self.bins = bins
        self.user = self.db.atms_auth_user[user_id]
        self.lang = lang

        self.return_value = False
        self.status = 'unknown'

        self.settings = current.settings
        errors.SETTINGS = self.settings

        # report part
        self.__files = []
        self.report_ids = []

    @threadsafe
    def appendFile(self, local_path, name=None):
        import mimetypes
        db = self.db
        title = name if name else os.path.basename(local_path)
        if not os.path.isabs(local_path):
            local_path = os.path.join(self.tmp, local_path)
        mimetype = mimetypes.guess_type(local_path)

        with open(local_path, 'rb') as f:
            aux = db.wiki_media.filename.store(f, os.path.basename(local_path))
        self.__files.append({
            'id': db.wiki_media.insert(
                filename=aux, title=title),
            'mimetype': mimetype})

    def fetchFile(self, remote_path, name=None):
        name = name if name else os.path.basename(remote_path)
        if not os.path.exists(self.tmp):
            os.makedirs(self.tmp, 0755)
        local_path = os.path.join(self.tmp, name)
        with open(local_path, 'w') as f:
            f.write(current.fs.open(remote_path, 'rb').read())
        return local_path

    def get_signature(self):
        return """TASK: %(task_id)s, \"%(name)s\", RSS: %(rss)d MB
        """ % dict(
            task_id=self.task_id,
            name=self.case_name,
            rss=getrusage(RUSAGE_SELF).ru_maxrss / 1024)

    def _execute(self):
        START = datetime.now()
        sg = self.get_signature()
        result = '<nothing>'
        db = current.db
        if not self.isReady():
            msg = sg + 'Task postponed! Devices not ready.'
            NOTIFY_WARN(msg)
            self.__setReportStatus('postponed')
            return msg
        try:
            NOTIFY_START(sg + 'Start...')
            self.lockDevices()
            self.__initReport()
            self.__setReportStatus('pending')

            def interval_func():
                seconds = (datetime.now() - START).total_seconds()
                hrs = seconds / 3600
                seconds = seconds - (hrs * 3600)
                mins = seconds / 60
                secs = seconds - (mins * 60)
                NOTIFY_INFO(self.get_signature() +
                            'Elapsed: %(hrs)02d:%(mins)02d:%(secs)02d' % dict(
                    hrs=hrs, mins=mins, secs=secs))

            t = Timer(interval_func)
            t.start()

            result = self.execute()

            t.stop = True

            with codecs.open(
                    os.path.join(self.tmp, 'test_result.log'), "r", "utf-8") as f:
                result = f.read()

        except Exception:
            result = traceback.format_exc()
            print(result)
            NOTIFY_ERROR(result)
        finally:
            NOTIFY_STOP(sg + 'Stop!')
            # for long running tasks - refresh db connection when not used
            self.db._adapter.close()
            self.db._adapter.reconnect()

            self.unlockDevices()
            self._clean()
            return result

    def execute(self):
        """You have to implement the whole test workflow here.

        Here you have to run "runScript" method. It is the minimum!
        """
        raise NotImplementedError()

    def runScript(self):
        unittest.installHandler()

        # copy to local machine
        r_filep = self.db.atms_attachments[self.script_id].filename
        l_filep = self.fetchFile(r_filep, 'test_script.py')
        testCaseMod = imp.load_source(l_filep, l_filep)

        # validate the script
        self.__scriptValidation(l_filep)

        # execute the script
        with codecs.open(
                os.path.join(self.tmp, 'test_result.log'), "w", "utf-8") as f:
            runner = unittest.TextTestRunner(
                f, verbosity=AbstractTestProc.VERBOSITY)
            suite = unittest.TestSuite()
            for func_name in self.funcs:
                suite.addTest(testCaseMod.TestCase(func_name.strip(), self))
            self.return_value = runner.run(suite)
            errors = self.return_value.errors
            if len(errors) > 0:
                current.logger.error(errors)
        return self.return_value

    @threadsafe
    def isReady(self):
        from simplejson import loads
        db = self.db
        rows = db(db.atms_devices.id.belongs(
            self.duts + self.teds)).select(
                db.atms_devices.state,
                db.atms_devices.connected)

        general_state = reduce(lambda x, y: (x and y),
                               [d == 'idle' for d in
                                [r['state'] for r in rows]])
        ready_result = general_state is True or general_state == 'idle'
        ready_result &= reduce(lambda x, y: x and y,
                               [r['connected'] for r in rows])
        if ready_result:
            return True

        # TODO - check if long running tasks are reporting failed/timeout state
        # try:
        current_task = db(
            db.scheduler_task.id == current.W2P_TASK.id).select(
            ).first()
        current.queue_task_dep(
            'runTest',
            devices=self.duts + self.teds,
            group_name=current_task.group_name,
            pvars=loads(current_task.vars),
            timeout=current_task.timeout,
            start_time=current_task.start_time + timedelta(minutes=15),
            sync_output=current_task.sync_output)
        # finally:
        #     return False

    def lockDevices(self):
        self.__changeDevicesStatus('busy')

    def unlockDevices(self):
        self.__changeDevicesStatus('idle')

    @threadsafe
    def __changeDevicesStatus(self, state):
        db = self.db
        db(db.atms_devices.id.belongs(
            self.duts + self.teds)).update(state=state)
        db.commit()
        websocket_send(self.settings.ws_server, '',
                       self.settings.security_key, 'atms_devices')

    def __scriptValidation(self, script_path):
        validation_result = validate_script(script_path)
        if not validation_result[0]:
            self.__setReportStatus('aborted')
            for el in ["%s - %s" % (
                x['line'], x['desc'])
                    for x in validation_result[1]['data']]:
                print(el)
            raise Error(
                "Script has errors. \
                Please go to the editor, correct it and try again:\n%s" %
                '\n'.join(["%s - %s" % (
                    x['line'], x['desc'])
                    for x in validation_result[1]['data']]))

    def _clean(self):
        self.__generateReport()
        self.packFiles()
        self.clean()
        # Don't remove files generated during the test! Not now!
        return
        shutil.rmtree(self.tmp)

    def clean(self):
        raise NotImplementedError()

    @threadsafe
    def __setReportStatus(self, status):
        db = self.db
        if status not in [o[0] for o in
                          db.atms_reports.status.requires.options()]:
            current.logger.warn('Status "%s" is not valid state' % status)
            return
        db((db.atms_reports.dev_id.belongs(self.duts)) &
           (db.atms_reports.case_id == self.case_id)).update(
            status=status)
        db.commit()
        websocket_send(self.settings.ws_server, '',
                       self.settings.security_key, 'atms_devices')

    @global_sync
    def __initReport(self):
        db = self.db
        lang = self.lang

        for d in self.duts:
            db.atms_reports.update_or_insert(
                dev_id=d,
                case_id=self.case_id)
            db.commit()
            self.report_ids.append(
                db.atms_reports(dev_id=d, case_id=self.case_id).id)

        this_case = db.atms_cases[self.case_id]
        this_case.status = 'pending'
        this_case.update_record()
        db.commit()

        websocket_send(self.settings.ws_server, '',
                       self.settings.security_key, 'atms_cases')

    @global_sync
    def __generateReport(self):
        if len(self.duts) == 0:
            return
        from tester.reporter import Report
        db = self.db
        result = self.return_value
        status = 'failed'
        if isinstance(result, unittest.TestResult):
            errnb = len(result.errors) + len(result.failures)
            status = 'failed' if errnb > 0 else 'passed'

        # partial reports
        db.atms_reports.update_or_insert(
            ((db.atms_reports.dev_id.belongs(self.duts)) &
             (db.atms_reports.case_id == self.case_id)),
            status=status,
            # wiki_page=self.wiki_id_partial,
            files=self.__files)
        db.commit()

        this_case = db.atms_cases[self.case_id]
        this_case.status = status
        this_case.update_record()
        db.commit()

        websocket_send(self.settings.ws_server, '',
                       self.settings.security_key, 'atms_cases')

        self.status = status

    @threadsafe
    def packFiles(self, include_extra=True):
        models = ["%s-%s" % (
            self.db.atms_devices[d].model,
            self.db.atms_devices[d].id) for d in self.duts]
        models = '_'.join(models)
        zipfilename = "%s_task-%s_%s" % (
            datetime.now().strftime('%Y.%m.%d_%H.%M'),
            self.task_id, models) + '.zip'

        zipfilename = os.path.join(self.tmp, zipfilename)
        with ZipFile(zipfilename, 'w',
                     allowZip64=True) as myzip:
            special_exts = ['.jpg', '.mp4', '.png']
            for fname in os.listdir(self.tmp):
                fpath = os.path.join(self.tmp, fname)
                fname, fext = os.path.splitext(fpath)
                fname = os.path.basename(fpath)
                if fext == '.zip':
                    continue
                if fext in special_exts and include_extra:
                    myzip.write(fpath, os.path.join('extra', fname))
                else:
                    myzip.write(fpath, fname, compress_type=ZIP_DEFLATED)

        if not include_extra or 'prod' not in current.settings.config:
            return zipfilename

        db = self.db
        zip_id = db.atms_attachments.insert(
            refcase=self.case_id,
            created_by=self.user,
            modified_by=self.user,
            type='application/zip/zip',
            size=os.stat(zipfilename).st_size,
            title=os.path.basename(zipfilename),
            filename=open(zipfilename, 'rb'))
        db.commit()

        websocket_send(self.settings.ws_server, '',
                       self.settings.security_key, 'atms_attachments')

        # send notification email to Marian ;)
        zip_url = URL(
            'manager', 'download', args=['atms_attachments', zip_id],
            scheme=True, host=urlparse(current.settings.ws_server).hostname)

        T = current.T
        T.force(self.lang)
        context = dict(
            title=T('Test Case: "%s" Summary') % self.case_name,
            msg_href=zip_url,
            msg_note=T(
                'Email generated automatically.\nPlease do not respond.'),
            msg_body=A(
                T('ZIP archive with result files: logs etc.'), _href=zip_url))

        statuses = {
            'passed': T('Passed'),
            'failed': T('Failed')
            }
        email_title = '[SAUT ATMS] %s C#%d %s' % (
            self.case_name, self.case_id,
            statuses.get(self.status.lower(), T('Uknown')))

        self.mail(context, None, email_title)

        return zip_url

    def mail(self, context, emails=None, email_title=None, attachments=None):
        T = current.T
        T.force(self.lang)

        case_url = URL(
            'manager', 'index', args=['case', 'update', self.case_id],
            scheme=True, host=urlparse(current.settings.ws_server).hostname)

        _context = dict(
            title=T('Test Case notification: "%s" ') % self.case_name,
            title_href=case_url,
            msg_title='[%d] %s (%s)' % (
                self.case_id, self.case_name, ', '.join(self.funcs)),
            msg_href=None,
            msg_note='',
            msg_body=None,
            user='%s %s' % (self.user.first_name, self.user.last_name),
            username=self.user.first_name,
            useremail=self.user.email)

        if isinstance(context, dict):
            _context.update(context)
        else:
            _context['msg_body'] = context

        message = current.response.render('mail_message.html', _context)
        mail = Mail()
        mail.settings.server = current.settings.email_server
        mail.settings.sender = current.settings.email_sender
        mail.settings.login = current.settings.email_login

        if attachments:
            attachments = [mail.Attachment(uri) for uri in attachments]

        cc = emails
        if emails is None:
            db = self.db
            group = db.atms_auth_group(role='atms_manager')
            users = db(
                (db.atms_auth_membership.group_id == group.id) &
                (db.atms_auth_membership.user_id == db.atms_auth_user.id) &
                (db.atms_auth_user.is_active == True)).select(
                db.atms_auth_user.email,
                distinct=True)

            cc = [d['email'] for d in users]
            if 'prod' not in current.settings.config:
                cc = []
                return
        mail.send(
            self.user.email,
            email_title or '[SAUT ATMS] %s C#%d' % (
                self.case_name, self.case_id),
            message,
            cc=cc, attachments=attachments)


# duts - komorki, stb (device under test)
# teds - test environment devices
def task_runTest(case_id, duts=[], teds=[], user_id=None, lang='en'):
    from misc import say

    try:
        lang = lang.split(',')[0]
    except:
        pass

    tr = current.T
    db = current.db
    case = db.atms_cases[case_id]

    # let assume mobile subtypes only = mobile individual test
    subtypes = list(set([r.subtype for r in db(
        db.atms_devices.id.belongs(duts)).select(db.atms_devices.subtype)]))

    if len(subtypes) == 1 and subtypes[0] == 'mobile':
        from tester.mobile import TestProc
    else:
        from tester.stb import TestProc

    # start time
    start_time = time.time()

    # args for testcaseproc
    script_id = db.atms_attachments[case.auto_script].id
    bins = [db.atms_attachments[case.auto_bin]]
    funcs = case.auto_funcs

    # prevalidation
    err_msg = None
    if len(funcs) == 0:
        err_msg = "No test function provided! \
            Please, specify at least one valid test \
            function from the script."
    if current.settings.host in [current.settings.host_master]:
        err_msg = ("Script execution forbidden for \"%s\". \
            You're a naughty creature!") % current.settings.host
    if err_msg is not None:
        raise Error(err_msg)

    devs = db(db.atms_devices.id.belongs(duts + teds)).select(
        db.atms_devices.id,
        db.atms_devices.host,
        db.atms_devices.state,
        db.atms_devices.connected)

    testhosts = list(set([d.host for d in devs]))
    if len(testhosts) > 1 or current.settings.host != testhosts[0]:
        raise Error(
            "Chosen devices are distributed on different test slaves.")

    if script_id and funcs and devs:
        case_id_spk = ',' + '! '.join(str(case_id))
        say(tr('Test %s has begun', language=lang) % case_id_spk, lang=lang)
        T = TestProc(case_id, script_id, funcs, duts, teds, bins,
                     user_id, lang)
        result = T._execute()
        case.update_record(auto_execution_time=long(
            time.time() - start_time) / 60)
        # max(case.result_time, long((time.time() - start_time) / 60)))
        db.commit()
        if 'FAILED' in result:
            say(
                tr('Test %s stopped due to errors',
                   language=lang) % case_id_spk, lang=lang)
        else:
            say(
                tr('Test %s finished successfully',
                   language=lang) % case_id_spk, lang=lang)
        return dict(content=result)
    else:
        raise Error("Wrong parameters for auto test")


if __name__ == '__main__':
    import argparse
    import types
    from functools import partial
    from tester.lab import LabFactory, task_scanlab

    available_funcs = dict()
    for l in dict(locals()):
        v = locals()[l]
        if isinstance(v, types.FunctionType) and l.startswith('task'):
            available_funcs[l] = v

    parser = argparse.ArgumentParser(
        description='Run specific tasks/functions in ATMS environment.'
                    'Example: bash worker_run.sh -f task_runTest 859')
    parser.add_argument('-f', '--func', type=str,
                        default='task_scanlab',
                        choices=available_funcs.keys(),
                        help='Task/function to run')
    parser.add_argument('args', nargs='*', metavar='arg', help='Arguments')
    args = parser.parse_args()

    if args.func == 'task_runTest':
        try:
            TASK_ID = int(args.args[0])
        except IndexError:
            raise Exception('You forgot to provide TASK_ID!')

        db(db.atms_devices.state == 'busy').update(state='idle')
        db.commit()

        task_desc = db.atms_cases[TASK_ID]
        mobile_types = zip(*db.atms_devices.type.requires.options())[0][2:]
        ted_types = zip(*db.atms_devices.type.requires.options())[0][:2]
        mobile_devs = [d for d in task_desc.auto_devs if d.type in mobile_types]
        ted_devs = [d for d in task_desc.auto_devs if d.type in ted_types]
        stb_devs = [d for d in task_desc.auto_devs if d.type in ['dut']]
        print(task_runTest(
            TASK_ID, mobile_devs, ted_devs, user_id=9, lang='pl'))
    else:
        func = partial(available_funcs[args.func], *args.args)
        print(func())
