# -*- coding: utf-8 -*-
from gluon import current
import time


def say(msg, lang='pl', vol=100):
    import os
    from tempfile import mktemp
    from base64 import b64encode
    from subprocess import call
    from simplejson import dumps
    from websocket_messaging import websocket_send
    from tester.base import global_sync
    settings = current.settings
    try:
        filename = '%s.mp3' % mktemp()
        @global_sync
        def _s():
            from gtts import gTTS
            gTTS(text=unicode(str(msg), "utf-8"), lang=lang).save(filename)
            with open(filename, 'rb') as f:
                payload = dict(
                    audio='data:audio/wav;base64,' + b64encode(f.read()))
                websocket_send(settings.ws_server, dumps(payload),
                            settings.security_key, 'broadcast')
            call('mpg321 %s --gain %d -o alsa' % (filename, vol), shell=True)
        _s()
    except:
        pass
    finally:
        if os.path.exists(filename):
            os.unlink(filename)


def copy_and_assign_files(item_id):
    import os
    db = current.db
    new_item = db.atms_cases[item_id]

    keys = ['auto_script', 'auto_bin']

    def resolve(key):
        attachment = db.atms_attachments[new_item[key]]
        if attachment is None:
            return

        tpath = os.path.join(
            current.request.folder, 'uploads', attachment.filename)
        attachment.refcase = item_id
        # attachment.title = \
        #     current.T('copy') + '_id%d_' % item_id + attachment.title

        kwargs = attachment.as_dict()
        del kwargs['id']
        del kwargs['filename']

        stream = None
        if current.fs:
            stream = current.fs.open(attachment.filename, 'rb')
        else:
            stream = open(tpath, 'rb')

        new_attachment_id = db.atms_attachments.insert(
            filename=db.atms_attachments.filename.store(
                stream,
                attachment.title),
            **kwargs)
        new_item[key] = new_attachment_id

    for k in keys:
        resolve(k)
    new_item.update_record()
    db.commit()


def pack(arg):
    def seq_iter(obj):
        return obj if isinstance(obj, dict) else xrange(len(obj))
    for i in seq_iter(arg):
        if arg[i] == 'null':
            arg[i] = None
            continue
        arg[i] = None if arg[i] == "None" else arg[i]
        arg[i] = arg[i] if isinstance(arg[i], list) else [arg[i]]
    return arg


def PickPort():
    import socket
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind(('localhost', 0))
    addr, port = s.getsockname()
    s.close()
    return port


def validate_script(filename):
    import subprocess
    import os.path
    import re
    import shlex
    from syntax_check import safe_eval

    if not os.path.isfile(filename):
        raise Exception('Script file does not exist or '
                        'it was not trasfered to the slave testing machine.')

    ignore_list = ['E501', 'E302', 'E303', 'E201', 'E221', 'E222', 'E271',
                   'E225', 'E265', 'E265', 'E266', 'E231', 'E261', 'E711',
                   'E712']
    proc = subprocess.Popen(
        shlex.split("pep8 --format pylint %s" % filename),
        stdout=subprocess.PIPE,
        bufsize=1)
    out = proc.communicate()[0]
    r = re.compile(r'^.+\.py:(.+):\s\[(.+)\]\s(.+)', re.MULTILINE)

    warnings = 0
    errors = 0
    data = []
    critical_marker = ' . . . . . . . . .'
    for match in r.finditer(out):
        g = match.groups()
        marker = ''
        if g[1][0] == 'W':
            warnings += 1
        elif g[1][0] == 'E' and g[1] not in ignore_list:
            marker = critical_marker
            errors += 1
        elif g[1][0] == 'E' and g[1] in ignore_list:
            warnings += 1
        data.append(
            dict(line='line:%s' % g[0] + marker,
                 incident=g[1] + marker, desc=g[2]))

    ast_check = safe_eval(open(filename, 'r').read())
    if ast_check:
        for err in ast_check:
            errors += 1
            data.append(dict(
                line="%s%s" % (err[0], critical_marker),
                incident="Parsing" + critical_marker,
                desc=err[1]))

    validation_data = dict(
        caption="%s\n%s\n%s" % (
            current.T('PEP8 and AST verification result'),
            "\t%s: %s" % (current.T('Ignored PEP8 errors'),
                          ", ".join(ignore_list)),
            current.T("Please make sure your code is PEP8 compliant.")),
        errors_count=errors,
        warnings_count=warnings,
        data=data)
    return (False if errors > 0 else True, validation_data)


def is_holiday(day):
    from datetime import datetime
    from datetime import timedelta
    if day.weekday() in [5, 6]:
        return True  # Weekend
    if day.month == 1 and day.day == 1:
        return True  # Nowy Rok
    if day.month == 5 and day.day == 1:
        return True  # 1 maja
    if day.month == 5 and day.day == 3:
        return True  # 3 maja
    if day.month == 8 and day.day == 15:
        return True  # Wniebowzięcie Najświętszej Marii Panny
    if day.month == 11 and day.day == 1:
        return True  # Dzień Wszystkich Świętych
    if day.month == 11 and day.day == 11:
        return True  # Dzień Niepodległości
    if day.month == 12 and day.day == 25:
        return True  # Boże Narodzenie
    if day.month == 12 and day.day == 26:
        return True  # Boże Narodzenie
    A = 24
    B = 5  # https://pl.wikipedia.org/wiki/Wielkanoc
    a = day.year % 19
    b = day.year % 4
    c = day.year % 7
    d = (a * 19 + A) % 30
    e = (2 * b + 4 * c + 6 * d + B) % 7
    if d == 29 and e == 6:
        d -= 7
    if d == 28 and e == 6 and a > 10:
        d -= 7
    easter_day = datetime(day.year, 3, 22) + timedelta(days=d + e)
    if easter_day == day - timedelta(days=1):
        return True  # Wielkanoc
    if easter_day == day - timedelta(days=60):
        return True  # Boże Ciało
    return False


def hw2alsa(hwname):
    import re
    from subprocess import check_output
    hwname = hwname.replace('hw:', '').split(',')
    r1 = re.compile(r'alsa.card\s\=\s"(%s)"' % hwname[0], re.IGNORECASE)
    r2 = re.compile(r'alsa.device\s\=\s"(%s)"' % hwname[1], re.IGNORECASE)
    output = check_output(
        "pactl list | awk '/alsa_input/,/alsa.card_name/'", shell=True)
    for o in output.split('alsa_input.')[1:]:
        dname = 'alsa_input.' + o.split()[0]
        m1 = re.search(r1, o)
        m2 = re.search(r2, o)
        if m1 and m2:
            return dname
    raise Exception('Audio device not found!!!') 


class Memoize(object):
    _caches = {}
    _timeouts = {}

    def __init__(self, timeout=2):
        self.timeout = timeout

    def collect(self):
        for func in self._caches:
            cache = {}
            for key in self._caches[func]:
                if ((time.time() - self._caches[func][key][1]) <
                        self._timeouts[func]):
                    cache[key] = self._caches[func][key]
            self._caches[func] = cache

    def __call__(self, f):
        self.cache = self._caches[f] = {}
        self._timeouts[f] = self.timeout

        def func(*args, **kwargs):
            kw = kwargs.items()
            kw.sort()
            key = (args, tuple(kw))
            try:
                v = self.cache[key]
                if (time.time() - v[1]) > self.timeout:
                    raise KeyError
            except KeyError:
                v = self.cache[key] = f(*args, **kwargs), time.time()
            return v[0]
        func.func_name = f.func_name

        return func


class TPClient(object):
    def __init__(self, settings=None):
        if not settings:
            settings = current.settings
        kwargs = {
            'host': settings.tp_server,
            'port': settings.tp_port,
            'user': settings.tp_user,
            'passw': settings.tp_passw,
            'put_dir': settings.tp_init_dir
            }
        self.client = None
        if settings.tp == 'ftp':
            self.client = FTPClient(**kwargs)
        else:
            self.client = SFTPClient(**kwargs)

    def get_file(self, pfrom, pto):
        self.client.get_file(pfrom, pto)

    def put_file(self, pfrom, pto):
        self.client.put_file(pfrom, pto)

    def close(self):
        self.client.close()


class SFTPClient(object):
    def __init__(self, host='127.0.0.1', port=22, user='test', passw='test',
                 put_dir='/tmp',
                 get_dir='/home/cp/Dropbox/web2py/applications/atms/uploads'):
        import paramiko
        self.get_dir = get_dir
        self.put_dir = put_dir
        self.t = paramiko.Transport((host, port))
        self.t.connect(username=user, password=passw)
        self.sftp = paramiko.SFTPClient.from_transport(self.t)

    def get_file(self, pfrom, pto):
        self.sftp.chdir(self.get_dir)
        self.sftp.get(pfrom, pto)

    def put_file(self, pfrom, pto):
        from subprocess import call
        call("mv %s /tmp/%s" % (pfrom, pto), shell=True)
        return
        self.sftp.chdir(self.put_dir)
        self.sftp.put(pfrom, pto)

    def close(self):
        self.sftp.close()
        self.t.close()


class FTPClient(object):
    def __init__(
            self, host='192.168.80.215', port=21, user='saut-test',
            passw='cxf7NmLbt3',
            put_dir='/tmp',
            get_dir='/home/_ftp/saut-test/web2py/applications/atms/uploads'):
        from ftplib import FTP
        self.get_dir = get_dir
        self.put_dir = put_dir
        self.ftp = FTP(host, user, passw, timeout=150)

    def get_file(self, pfrom, pto):
        self.ftp.cwd(self.get_dir)
        self.ftp.retrbinary('RETR %s' % pfrom, open(pto, 'wb').write)

    def put_file(self, pfrom, pto):
        self.ftp.cwd(self.put_dir)
        self.ftp.storbinary('STOR %s' % pto, open(pfrom, 'r'))

    def close(self):
        self.ftp.close()
