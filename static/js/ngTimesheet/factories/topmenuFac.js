// main.js factory
/***** MENU *****/
app.factory("top_menu", function (USER_ROLES) {
  return {
        a_calendar: {
            title: 'Kalendarz',
            icon: 'fa fa-calendar',
            url: '#/calendar',
            active: true,
            privileges: [USER_ROLES.all],
            id: "aa"
        },

        b_statistics: {
            title: 'Statystyki',
            icon: 'fa fa-table',
            url: '#/statystyki',
            active: false,
            privileges: [USER_ROLES.all],
            id: "bb"
        }
    };
});
