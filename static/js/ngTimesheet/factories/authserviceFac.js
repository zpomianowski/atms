// main.js factory
/***** ACCESS CONTROL *****/
app.factory('AuthService', function($http, Session, $rootScope, USER_ROLES) {
    var authService = {};

    authService.login = function() {
      return $http.get("getUserInfo.json")
        .then(function(res) {
          Session.create(res.data);
          $rootScope.$broadcast('userLoggedIn', res.data)
          return res.data;
        });
    };

    authService.logout = function() {
      Session.destroy();
    }

    authService.isAuthenticated = function() {
      if (!Session.user) {
        return false;
      }
      return !!Session.user['data'].id;
    };

    authService.isAuthorized = function(authorizedRoles) {
      if (!angular.isArray(authorizedRoles)) {
        authorizedRoles = [authorizedRoles];
      }

      if (authService.isAuthenticated() && authorizedRoles.indexOf(USER_ROLES.all) !== -1) {
        return true
      } else {
        if (authService.isAuthenticated()) {
          for (i = 0; i < authorizedRoles.length; i++) {
            if (angular.equals(authorizedRoles[i], Session.user['data'].privileges)) {
              return true;
            }
          }
          return false;
        } else {
          return false;
        }
      }
    };
    return authService;
  })
