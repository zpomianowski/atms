// main.js factory
/***** FLASH *****/
app.factory('flash', function ($timeout) {
  var obj = {};

    obj.visible = false;
    obj.message = "test";
    obj.type = " ";
    obj.show = function (msg, typ) {
        obj.message = msg;
        switch (typ) {
        case 'success':
            obj.type = "myFlash flash-success";
            break;
        case 'error':
            obj.type = "myFlash flash-error";
            break;
        case 'info':
            obj.type = "myFlash flash-info";
            break;
        case 'warning':
            obj.type = "myFlash flash-warning";
            break;
        }
        obj.visible = true;
        $timeout(function () {
            obj.visible = false;
        }, 4000);
    }
  return obj;
})
