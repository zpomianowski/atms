app.controller('ModalSettingsCtrl', function ($scope, dialogs, $uibModalInstance, $filter,flash, uiCalendarConfig, Events, $http, Projects, Tasks, Relationship) {
	$scope.selectedTab = "projekty"
    $scope.projects = Projects.getProjects();
 	$scope.tasks = Tasks.getTasks()
 	$scope.relationship = Relationship.getRelationship();

 	$scope.newProject ={}
	$scope.models = {
        selected: null,
        lists: {
        	"Nieprzypisane zadania": [],
        	"Przypisane zadania": []}
    };

    $scope.reload_relation = function(){
    	$scope.models.lists["Przypisane zadania"] = []
    	$scope.models.lists["Nieprzypisane zadania"] = []
    	for(var i=0; i< $scope.tasks.length; i++){
    		if(!$scope.newProject.hasOwnProperty('id')){
    			$scope.models.lists["Nieprzypisane zadania"].push($scope.tasks[i])
    			continue;
    		}
    		rel = $filter('filter')($scope.relationship, {"project_id": $scope.newProject.id, "task_type_id":$scope.tasks[i].id, "visible":true},true)
    		if(rel.length >0){
    			$scope.models.lists["Przypisane zadania"].push($scope.tasks[i])
    		}else{
    			$scope.models.lists["Nieprzypisane zadania"].push($scope.tasks[i])
    		}

    	}
    }

    $scope.$watch("selected_project", function(newVal, oldVal){
    	if(newVal ==null){
    		return
    	}
		$scope.newProject = angular.copy(newVal)
		$scope.reload_relation()
   	})


    $scope.reload_relation()


    $scope.setRelations = function(project_id, task_type_id){
    	newdat = {
			project_id: project_id,
			task_type_id: task_type_id
		}

    	$http.post('setRelations.json', {'data':newdat}).
			success(function(data, status, headers, config){
				rel = $filter('filter')($scope.relationship, {'project_id': project_id,
																											'task_type_id':task_type_id},true)

				if(rel.length>0){
					rel[0].visible = true;
				}else if(data['data'] != "None"){
					newrel = {
						project_id: project_id,
						task_type_id: task_type_id,
						visible: true,
						id: parseInt(data['data'])
					}
					$scope.relationship.push(newrel)
					//Relationship.relationships.push(newrel)

				}
			}).
			error(function(data, status, headers, config) {
				flash.show("Wystąpił błąd podczas resetu relacji.", "error")
			});
    }


    $scope.save = function(){
     		if($scope.selected_project ==null){
     			return;
     		}
      var dlg = null;
      dlg = dialogs.confirm('Potwierdzenie','Zapisać zmiany');
      dlg.result.then(function(btn){
      	$http.post("editRecord.json", {'table': 'atms_timesheet_project',
																		 'data':$scope.newProject,
																		 'id':$scope.selected_project.id}).
        		success(function(data, status, headers, config) {
        			$scope.selected_project.name = $scope.newProject.name
        			$scope.selected_project.color = $scope.newProject.color
        			$scope.selected_project.background = $scope.newProject.background
        			$scope.selected_project.description = $scope.newProject.description
        			$scope.selected_project.visible = $scope.newProject.visible
        			$scope.selected_project.state = $scope.newProject.state

        			events = $filter('filter')(Events.events, {"project_id": $scope.newProject.id},true)
        		 	for(var  i =0; i<events.length;i++){
  			        events[i].textColor = $scope.newProject.color
  			        events[i].title = $scope.newProject.name || ""
  			        events[i].backgroundColor = $scope.newProject.background
  			        events[i].subtitle= $scope.newProject.name || ""
  			    }
        			uiCalendarConfig.calendars["myCalendar"].fullCalendar( 'rerenderEvents' )

          		$http.post("resetRelations.json", {'target': "project",
																							   'id':$scope.newProject.id}).
  					success(function(data, status, headers, config){
							var rels = $filter('filter')($scope.relationship, {"project_id": $scope.newProject.id},true)
  						for(var i =0; i<rels.length ;i++){
  							rels[i].visible = false
  						}
  						for(var i =0; i< $scope.models.lists["Przypisane zadania"].length ;i++){
  							var task = $scope.models.lists["Przypisane zadania"][i]
  							$scope.setRelations($scope.newProject.id, task.id)
  						}
  					}).
  					error(function(data, status, headers, config) {
  				        flash.show("Wystąpił błąd podczas resetu relacji.", "error")
  				    });
          		flash.show("Zmodyfikowano projekt", "success")

        		}).
        		error(function(data, status, headers, config) {
          		flash.show("Wystąpił błąd podczas edycji.", "error")
        		});
      },function(btn){});
    }


    $scope.add = function(){

      if($scope.newProject.status==null){
        $scope.newProject.status = "stable"
      }
      if(!$scope.newProject.name || $scope.newProject.name ==""){
        flash.show("Podaj nazwę projektu.", "info")
        return
      }

      var dlg = null;
      dlg = dialogs.confirm('Potwierdzenie','Dodać nowy projekt?');
      dlg.result.then(function(btn){
        	$http.post("addRecord.json", {'table': 'atms_timesheet_project', 'data':$scope.newProject}).
        		success(function(data, status, headers, config) {
        			$scope.newProject.id= parseInt(data['data'])
        			$scope.projects.push($scope.newProject)

        			for(var i =0; i< $scope.models.lists["Przypisane zadania"].length ;i++){
    					 var task = $scope.models.lists["Przypisane zadania"][i]
    					 $scope.setRelations($scope.newProject.id, task.id)
    				  }
              $scope.newProject = {}
    				flash.show("Dodano projekt", "success")
        		}).
        		error(function(data, status, headers, config) {
            		flash.show("Wystąpił błąd podczas dodawania nowego projektu.", "error")
          		});
    	 },function(btn){});
    }

/////////////////////////////////////////////////////////////////////////////////////////////////////////

    $scope.newTask ={}
	  $scope.modelsTask = {
        selected: null,
        lists: {
        	"Nieprzypisane projekty": [],
        	"Przypisane projekty": []}
    };

    $scope.$watch("selected_task", function(newVal, oldVal){
    	if(newVal ==null){
    		return
    	}
		$scope.newTask = angular.copy(newVal)
		$scope.reload_relationTask()
   	})

    $scope.reload_relationTask = function(){
    	$scope.modelsTask.lists["Przypisane projekty"] = []
    	$scope.modelsTask.lists["Nieprzypisane projekty"] = []

    	for(var i=0; i< $scope.projects.length; i++){
    		if(!$scope.newTask.hasOwnProperty('id')){
    			$scope.modelsTask.lists["Nieprzypisane projekty"].push($scope.projects[i])
    			continue;
    		}
    		rel = $filter('filter')($scope.relationship, {"project_id": $scope.projects[i].id, "task_type_id": $scope.newTask.id, "visible":true},true)
    		if(rel.length >0){
    			$scope.modelsTask.lists["Przypisane projekty"].push($scope.projects[i])
    		}else{
    			$scope.modelsTask.lists["Nieprzypisane projekty"].push($scope.projects[i])
    		}
    	}
    }
    $scope.reload_relationTask()



    $scope.savetask = function(){
    	if($scope.selected_task ==null){
 			return;
 		 }
     var dlg = null;
      dlg = dialogs.confirm('Potwierdzenie','Zapisać zmiany');
      dlg.result.then(function(btn){

      	$http.post('editRecord.json', {'table': 'atms_timesheet_task_type',
																		 'data':$scope.newTask,
																		 'id':$scope.selected_task.id}).
        		success(function(data, status, headers, config) {
        			$scope.selected_task.name = $scope.newTask.name
        			$scope.selected_task.description = $scope.newTask.description
        			$scope.selected_task.visible = $scope.newTask.visible

        			uiCalendarConfig.calendars["myCalendar"].fullCalendar( 'rerenderEvents' )

          		$http.post("resetRelations.json", {'target': "task",
																								 'id':$scope.newTask.id}).
  					success(function(data, status, headers, config){
							var rels = $filter('filter')($scope.relationship, {"task_type_id": $scope.newTask.id},true)
  						for(var i =0; i<rels.length ;i++){
  							rels[i].visible = false
  						}
  						for(var i =0; i< $scope.modelsTask.lists["Przypisane projekty"].length ;i++){
  							var project = $scope.modelsTask.lists["Przypisane projekty"][i]
  							$scope.setRelations(project.id, $scope.newTask.id)
  						}
  					}).
  					error(function(data, status, headers, config) {
  				        flash.show("Wystąpił błąd podczas resetu relacji.", "error")
  				    });
          		flash.show("Zmodyfikowano projekt", "success")

        		}).
        		error(function(data, status, headers, config) {
          		flash.show("Wystąpił błąd podczas edycji zadania.", "error")
        		});
      },function(btn){});
    }


    $scope.addtask = function(){
      if(!$scope.newTask.name || $scope.newTask.name ==""){
        flash.show("Podaj nazwę zadania.", "info")
        return
      }
      var dlg = null;
      dlg = dialogs.confirm('Potwierdzenie','Dodać nowy projekt?');
      dlg.result.then(function(btn){
        	$http.post("addRecord.json", {'table': 'atms_timesheet_task_type', 'data':$scope.newTask}).
        		success(function(data, status, headers, config) {
        			$scope.newTask.id= parseInt(data['data'])
        			$scope.tasks.push($scope.newTask)


        			for(var i =0; i< $scope.modelsTask.lists["Przypisane projekty"].length ;i++){
    					 var proj = $scope.modelsTask.lists["Przypisane projekty"][i]
    					 $scope.setRelations(proj.id,$scope.newTask.id)
    				  }
              $scope.tasks = {}
    				flash.show("Dodano zadanie", "success")
        		}).
        		error(function(data, status, headers, config) {
            		flash.show("Wystąpił błąd podczas dodawania zadania.", "error")
          		});
      },function(btn){});
    }

    // Model to JSON for demo purpose
    $scope.$watch('models', function(model) {
        $scope.modelAsJson = angular.toJson(model, true);
    }, true);



	$scope.cancel = function () {
    	$uibModalInstance.dismiss("");
  	};

})
