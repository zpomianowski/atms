app.controller("summationCtrl", function($scope,$rootScope, $filter, ngTableParams, Projects, AuthService, USER_ROLES,Session, Tasks, Relationship, Events, Users, dateFilter){


	$scope.projects = Projects.getProjects();
  	$scope.tasks = Tasks.getTasks()
  	$scope.users = Users.getUsers()
  	$scope.relationship = Relationship.getRelationship();
  	$scope.events = Events.getEvents();
  	$scope.loaded_events = Events.getEvents();

 	$scope.userRoles = USER_ROLES;
  	$scope.isAuthorized = AuthService.isAuthorized;
  	$scope.session = Session

  	$scope.begin_date = null
  	$scope.end_date = null


/****** LOAD DATA *****/
  /***** PROJECTS *****/
    if(!$scope.projects){
      Projects.loadProjectsData()
    }
    $scope.$on('projectsDataReady', function (event, data) {
      $scope.projects = data;
      Projects.projects = data;

      $scope.changeLoading()
    });

  /***** TASKA *****/
    if(!$scope.tasks){
      Tasks.loadTasksData()
    }
    $scope.$on('tasksDataReady', function (event, data) {
      $scope.tasks = data;
      Tasks.tasks = data;


      $scope.changeLoading()
      $scope.loading = true;
      var today = new Date()
      Events.loadEventsData(0, (today.getMonth()+1)+"/"+today.getFullYear());
    });

  /***** RELATIONSHIP *****/
    if(!$scope.relationship){
      Relationship.loadRelationshipData();
    }
    $scope.$on('relationshipDataReady', function (event, data) {
      $scope.relationship = data;
      Relationship.relationships = data;
      $scope.changeLoading()
    });

  /***** EVENTS ****
    if(!$scope.events){
      $scope.loading = true;
      var today = new Date()
      Events.loadEventsData(0, (today.getMonth()+1)+"/"+today.getFullYear());
    }*/

    $scope.$on('eventsDataReady', function (event, data){
      if($scope.tasks && $scope.projects){
        for(var  i =0; i<data.length;i++){
          var project = $scope.getItemById($scope.projects, data[i].project_id)
          var task = $scope.getItemById($scope.tasks, data[i].task_type_id)
          data[i].textColor = project.color //"#A1D3FF"
          data[i].title = project.name || ""
          data[i].backgroundColor = project.background //"#0000FF"
          data[i].borderColor = "A3ABAF" //project.background //"#FF0000"
          data[i].subtitle= task.name || ""
          data[i].duration = data[i].duration+" h"
          data[i].project = project
          data[i].task = task

          if(!data[i].description){
            data[i].description = ""
          }
        }
      }

      if(!$scope.events){
        $scope.events = data;
        Events.events = data;
      }else{
        $scope.events = $scope.events.concat(data);
        Events.events = Events.events.concat(data);
      }


      $scope.loaded_events =  $scope.fine_events_filter($scope.events,$scope.begin_date, $scope.end_date);

      if($scope.tableParamsRes != null){
          $scope.tableParamsRes.reload();
      }

      $scope.changeLoading()
    });

  /***** Users *****/
    if(!$scope.users){
      Users.loadUsersData()
    }
    $scope.$on('usersDataReady', function (event, data) {
      $scope.users = data;
      Users.users = data;

      $scope.changeLoading()
    });
//


	$scope.getItemById = function(src, id){
    	if(src){
	      	for(var i =0; i < src.length; i++){
	        	if(src[i].id == id){
	          	return src[i]
	        	}
	      	}
    	}
    	return null
  }


  $scope.start_date = "2015/02/11"
  $scope.options = {
    format: 'dd-mm-yyyy',
    editable: true,
    onClose: function(e) {
      // do something when the picker closes
    },
  }


  $scope.sumtime = 0;

  $scope.filter_tasks = [];
  $scope.filter_projects = [];
  $scope.filter_users = [];


  $scope.resetFilter = function(){
    $scope.filter_projects = [];
    $scope.filter_tasks = [];
    $scope.filter_users = [];
  }


  $scope.getEvents= function(){
    if($scope.begin_date == null){
      return;
    }
    var temp_begin_date = angular.copy($scope.begin_date)
    if(temp_begin_date.getMonth()+1 <10){
      var str_begin = temp_begin_date.getFullYear()+"-0"+(temp_begin_date.getMonth()+1)
    }else{
      var str_begin = temp_begin_date.getFullYear()+"-"+(temp_begin_date.getMonth()+1)
    }

    if($filter('filter')($scope.events, {"start":str_begin},false).length == 0){  // GET events
      var elems = $filter('orderBy')($scope.events, "start", true)
      if((typeof elems == 'undefined') || (elems.length == 0) ){
        var year = new Date().getFullYear();
        var month = new Date().getMonth()+1;
        var day = 1;
      }else{
        var year = elems[elems.length-1].start.split("-")[0]
        var month = elems[elems.length-1].start.split("-")[1]
        var day =elems[elems.length-1].start.split("-")[2].split(" ")[0]
      }

      var nd = new Date(year,month-1,0,0,0,0,0)

      $scope.begin_date.setHours(1)
      for (var d = temp_begin_date; d < nd; d.setMonth(d.getMonth() + 1)) {
        Events.loadEventsData(0, (d.getMonth()+1)+"/"+d.getFullYear());
      }
    }else{
      $scope.loaded_events =  $scope.fine_events_filter($scope.events,$scope.begin_date, $scope.end_date);
      if($scope.tableParamsRes != null){
        $scope.tableParamsRes.reload();
      }else{
        $scope.initTable()
      }
    }
  }


  $scope.fine_events_filter = function(items, begin, end){
    var retn_data = [];
    if((begin instanceof Date) && (end instanceof Date)){
      angular.forEach(items, function(item){
        var year = item.start.split("-")[0]
        var month = item.start.split("-")[1]
        var day = item.start.split("-")[2].split(" ")[0]
        var nd = new Date(year,month-1,day,23,0,0,0)
        if( nd >= begin) {
          var year_e = item.end.split("-")[0]
          var month_e = item.end.split("-")[1]
          var day_e = item.end.split("-")[2].split(" ")[0]
          var nd_e = new Date(year_e,month_e-1,day_e,0,0,0,0)
          if( nd_e <= end){
            retn_data.push(item)
          }
        }
      });
    }else if(begin instanceof Date){
      angular.forEach(items, function(item){
        var year = item.start.split("-")[0]
        var month = item.start.split("-")[1]
        var day = item.start.split("-")[2].split(" ")[0]
        var nd = new Date(year,month-1,day,23,0,0,0)
        if( nd >= begin) {
          retn_data.push(item)
        }
      });
    }else if(end  instanceof Date){
      angular.forEach(items, function(item){
        var year_e = item.end.split("-")[0]
        var month_e = item.end.split("-")[1]
        var day_e = item.end.split("-")[2].split(" ")[0]
        var nd_e = new Date(year_e,month_e-1,day_e,0,0,0,0)
        if( nd_e <= end){
          retn_data.push(item)
        }
      });
    }

    if($scope.filter_projects.length>0){
      var items = []
      for(var i=0; i < $scope.filter_projects.length; i++){
        var temp = $filter('filter')(retn_data, {"project_id": $scope.filter_projects[i].id, "visible":true}, true)
        items = items.concat(temp)
      }
      retn_data = items
    }

    if($scope.filter_tasks.length>0){
      var items = []
      for(var i=0; i < $scope.filter_tasks.length; i++){
        var temp = $filter('filter')(retn_data, {"task_type_id": $scope.filter_tasks[i].id, "visible":true}, true)
        items = items.concat(temp)
      }
      retn_data = items
    }

    if($scope.filter_users.length>0){
      var items = []
      for(var i=0; i < $scope.filter_users.length; i++){
        var temp = $filter('filter')(retn_data, {"user_id": $scope.filter_users[i].id, "visible":true}, true)
        items = items.concat(temp)
      }
      retn_data = items
    }

    $scope.sumtime =0
    for(var i=0; i < retn_data.length; i++){
       $scope.sumtime =  $scope.sumtime+ parseFloat(retn_data[i].duration.split(" ")[0])
    }

    return retn_data;
  }

  $scope.customfilter = {}
  $scope.getData = function(){
      retn_data= []
      if($scope.customfilter.hasOwnProperty("description") && ($scope.customfilter.description != "")){
        angular.forEach($scope.loaded_events, function(item, itemkey) {
          var keepGoing = true;
          var i = 0;
          angular.forEach(item, function(value, key) {
            if(keepGoing && (value.toString().toLowerCase().indexOf($scope.customfilter.description.toLowerCase() ) >=0)){
              retn_data.push(item);
               keepGoing = false;
            }
           },null);
        },null);
      }else{
        retn_data = $filter('filter')($scope.loaded_events, $scope.customfilter)
      }
      $scope.sumtime =0
      for(var i=0; i < retn_data.length; i++){
         $scope.sumtime =  $scope.sumtime+ parseFloat(retn_data[i].duration.split(" ")[0])
      }
      return retn_data
  }



	$scope.initTable = function(){
	    $scope.tableParamsRes = new ngTableParams({
	        page: 1,            // show first page
	        count: 50          // count per page
	    }, {
	        total:  $scope.getData().length,//function () { return getData().length; }, // length of data
	        getData: function($defer, params) {
	          var filteredData = $scope.getData();

	          var orderedData = params.sorting() ?
                                  $filter('orderBy')(filteredData, params.orderBy()) :
                                  filteredData;
              params.total(orderedData.length);
	            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
	        }//,
	        //$scope: { $data: {} }
	    });
	    $scope.tableParamsRes.settings().$scope = $scope;
	}

	$scope.changeLoading = function(){
	    if($scope.projects && $scope.tasks && $scope.relationship && $scope.events &&  $scope.users){
	      $scope.loading = false;
        if($scope.tableParamsRes ==null){
          $scope.initTable()
        }

	    }else{
	      $scope.loading = true;
	    }
	}

	$scope.changeLoading()


  $scope.$watch("customfilter", function (newValue, oldvalue) {
    if($scope.tableParamsRes){
       $scope.tableParamsRes.reload();
    }
  }, true);


  $scope.csvfilename = 'summation';
  $scope.csvseparator = ";";
  $scope.csvHeader = ['id', 'Zadanie', 'Projekt', 'Użytkownik', 'Start', 'Czas trwania', 'Opis' ];

  $scope.getCSV = function(){
    var items = []
    if($scope.customfilter.hasOwnProperty("description") && ($scope.customfilter.description != "")){
      angular.forEach($scope.loaded_events, function(item, itemkey) {
        var keepGoing = true;
        var i = 0;
        angular.forEach(item, function(value, key) {
          if(keepGoing && (value.toString().toLowerCase().indexOf($scope.customfilter.description.toLowerCase() ) >=0)){
            items.push(item);
            keepGoing = false;
          }
        },null);
      },null);
    }else{
      items = $filter('filter')($scope.loaded_events, $scope.customfilter)
    }
    //var items = $filter('filter')($scope.loaded_events, $scope.customfilter)
    var csv = []

    angular.forEach(items, function(item, key) {
      var user = $filter('filter')($scope.users, {id:item.user_id}, true)[0]
			$scope.dupa = user;
			try {
				var username = user.first_name+" "+user.last_name;
			}
			catch (err) {
			}
      csv.push({id:item.id, task:item.task.name, project:item.project.name, user:username, start:item.start, duration:item.duration, description: item.description })
    }, false);
    return csv;
  }

})
