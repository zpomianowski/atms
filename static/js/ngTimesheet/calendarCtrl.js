app.controller("calendarCtrl", function($scope, $rootScope, $compile, $uibModal, $http, $filter, flash, AuthService, USER_ROLES, uiCalendarConfig, Session, Projects, Tasks, Relationship, Events, Users, $localStorage, $sessionStorage) {
  $scope.flash = flash;
  $scope.loading = true;
  $scope.calendar_title;
  $scope.userRoles = USER_ROLES;
  $scope.isAuthorized = AuthService.isAuthorized;

  $scope.side_menu_selected_tab = "event";
  $scope.display_decription = false;
  $scope.remove_after_drop = true;
  $scope.projects = Projects.getProjects();
  $scope.tasks = Tasks.getTasks()
  $scope.users = Users.getUsers()
  $scope.relationship = Relationship.getRelationship();
  $rootScope.events = Events.getEvents();
  $scope.events_month = []
  $scope.eventSources = [];
  $scope.session = Session['user'];
  $scope.filter_users = [];
  // zapisywanie wybranych za pomoca filtra uzytkownikow do pamieci lokalnej
  $scope.$storage = $localStorage.$default()
    // jesli nie ma listy, to jest ona tworzona
    // jedyny jej element to zalogowany uzytkownik
  if (!$scope.$storage.filter_users) {
    $scope.$storage.filter_users = [$scope.session['data']];
  }
  if (!$scope.filter_users) {
    $scope.filter_users = [$scope.session['data']];
  }


  var storedEvents = []

  /****** LOAD DATA *****/
  /***** PROJECTS *****/
  if (!$scope.projects) {
    Projects.loadProjectsData()
  }
  $scope.$on('projectsDataReady', function(event, data) {
    $scope.projects = data;
    Projects.projects = data;

    $scope.changeLoading()
  });

  /***** TASKA *****/
  if (!$scope.tasks) {
    Tasks.loadTasksData()
  }
  $scope.$on('tasksDataReady', function(event, data) {
    $scope.tasks = data;
    Tasks.tasks = data;

    $scope.loading = true;
    var today = new Date()
    Events.loadEventsData(0, (today.getMonth() + 1) + "/" + today.getFullYear());

    $scope.changeLoading()
  });

  /***** RELATIONSHIP *****/
  if (!$scope.relationship) {
    Relationship.loadRelationshipData();
  }
  $scope.$on('relationshipDataReady', function(event, data) {
    $scope.relationship = data;
    Relationship.relationships = data;
    $scope.changeLoading()
  });

  /***** EVENTS *****/
  if (!$rootScope.events) {
    //$scope.loading = true;
    //var today = new Date()
    //Events.loadEventsData(0, (today.getMonth()+1)+"/"+today.getFullYear());
  } else {
    $scope.loading = true;
    var prev_year = new Date().getFullYear() //parseInt(uiCalendarConfig.calendars["myCalendar"].fullCalendar('getDate').format().split("-")[0])
    var prev_month = new Date().getMonth() + 1 //parseInt(uiCalendarConfig.calendars["myCalendar"].fullCalendar('getDate').format().split("-")[1])

    if (parseInt(prev_month) < 10) {
      prev_month = "0" + prev_month.toString()
    }
    var items = $filter('filter')($rootScope.events, {
      "start": prev_year.toString() + "-" + prev_month.toString()
    })
    $scope.events_month = items

    if ($scope.eventSources.length == 0) {
      $scope.eventSources.push([])
    }
    $scope.eventSources[0] = $scope.events_month
    storedEvents = $scope.eventSources[0]
  }
  $scope.$on('eventsDataReady', function(event, data) {
    if ($scope.tasks && $scope.projects) {
      for (var i = 0; i < data.length; i++) {
        var project = $scope.getItemById($scope.projects, data[i].project_id)
        var task = $scope.getItemById($scope.tasks, data[i].task_type_id)
        data[i].textColor = project.color //"#A1D3FF"
        data[i].title = project.name|| ""
        data[i].backgroundColor = project.background //"#0000FF"
        data[i].borderColor = "A3ABAF" //project.background //"#FF0000"
        data[i].subtitle = task.name || ""
        data[i].duration = data[i].duration + " h"
        data[i].project = project
        data[i].task = task
        if (!data[i].description) {
          data[i].description = ""
        }
      }
    } else {
      for (var i = 0; i < data.length; i++) {
        data[i].textColor = "#FFFFFF" //"#A1D3FF"
        data[i].title = ""
        data[i].backgroundColor = "#000000" //"#0000FF"
        data[i].borderColor = "A3ABAF" //project.background //"#FF0000"
        data[i].subtitle = ""
        data[i].duration = "0 h"
        data[i].project = null
        data[i].task = null
        if (!data[i].description) {
          data[i].description = ""
        }
      }
    }

    if (!$rootScope.events) {
      $rootScope.events = data;
      Events.events = data;
    } else {
      $rootScope.events = $rootScope.events.concat(data);
      Events.events = Events.events.concat(data);
    }

    var prev_year = parseInt(uiCalendarConfig.calendars["myCalendar"].fullCalendar('getDate').format().split("-")[0])
    var prev_month = parseInt(uiCalendarConfig.calendars["myCalendar"].fullCalendar('getDate').format().split("-")[1])
    if (parseInt(prev_month) < 10) {
      prev_month = "0" + prev_month.toString()
    }

    var items = $filter('filter')($rootScope.events, {
      "start": prev_year.toString() + "-" + prev_month.toString()
    })
    $scope.events_month = items

    if ($scope.eventSources.length == 0) {
      $scope.eventSources.push([])
    }

    $scope.eventSources[0] = $scope.events_month
    storedEvents = $scope.eventSources[0]
      /*
      if(Session.hasOwnProperty("user")){
        $scope.filter_users = $filter('filter')($scope.users, {"id":Session.user.id}, true)
      }*/

    $scope.runFilter()
    $scope.changeLoading()
  });

  /***** Users *****/
  if (!$scope.users) {
    Users.loadUsersData()
  }
  $scope.$on('usersDataReady', function(event, data) {
    $scope.users = data;
    Users.users = data;

    $scope.changeLoading()
  });
  //

  $scope.getItemById = function(src, id) {
    if (src) {
      for (var i = 0; i < src.length; i++) {
        if (src[i].id == id) {
          return src[i]
        }
      }
    }
    return null
  }

  $scope.changeLoading = function() {
    if ($scope.projects && $scope.tasks && $scope.relationship && $rootScope.events && $scope.users) {
      $scope.loading = false;
      if (uiCalendarConfig.calendars["myCalendar"]) {
        $scope.calendar_title = uiCalendarConfig.calendars["myCalendar"].fullCalendar('getView').title
      }

    } else {
      $scope.loading = true;
    }
  }

  $scope.changeLoading()

  $scope.$watch("selected_project", function(newVal, oldVal) {
    if (newVal && (typeof newVal == "object")) {
      $scope.selected_task = null
      var temp = $filter('filter')($scope.relationship, {
        "project_id": newVal.id,
        "visible": true
      }, true)
      var filtered_tasks_id = []
      for (var i = 0; i < temp.length; i++) {
        filtered_tasks_id.push(temp[i].task_type_id)
      }
      var items = []
      for (var i = 0; i < $scope.tasks.length; i++) {
        if (filtered_tasks_id.indexOf($scope.tasks[i].id) != -1) {
          items.push($scope.tasks[i])
        }
      }
      $scope.task_for_selected_project = items;
    }
  });

  $scope.task_for_filtered_project = []
  $scope.filter_tasks = []
  $scope.filter_projects = []



  $scope.$watch("filter_projects", function(newVal, oldVal) {
    if (newVal && (typeof newVal == "object")) {
      $scope.filter_tasks = []
      var filtered_relationship = []
      var filtered_task = []
      for (var i = 0; i < $scope.filter_projects.length; i++) {
        var temp = $filter('filter')($scope.relationship, {
          "project_id": $scope.filter_projects[i].id,
          "visible": true
        }, true)
        filtered_relationship = filtered_relationship.concat(temp);
      }

      for (var i = 0; i < filtered_relationship.length; i++) {
        var temp = $filter('filter')($scope.tasks, {
          "id": filtered_relationship[i].task_type_id,
          "visible": true
        }, true)
        filtered_task = filtered_task.concat(temp);
      }

      $scope.task_for_filtered_project = filtered_task;
    }
  });

  $scope.runFilter = function() {
    $scope.events_month = storedEvents
    $scope.eventSources[0] = storedEvents
    var items = []
    $scope.$storage.filter_users = $scope.filter_users
    if ($scope.filter_projects.length > 0) {
      for (var i = 0; i < $scope.filter_projects.length; i++) {
        var temp = $filter('filter')($scope.eventSources[0], {
          "project_id": $scope.filter_projects[i].id,
          "visible": true
        }, true)
        items = items.concat(temp)
      }
    }

    if (items.length == 0) {
      items = storedEvents
    }

    if ($scope.filter_tasks.length > 0) {
      itemstask = []
      for (var i = 0; i < $scope.filter_tasks.length; i++) {
        var temp = $filter('filter')(items, {
          "task_type_id": $scope.filter_tasks[i].id,
          "visible": true
        }, true)
        itemstask = itemstask.concat(temp)
      }
      items = itemstask
    }
    if (items.length == 0) {
      items = storedEvents
    }
    if ($scope.filter_users.length > 0) {

      var userstemp = []
      for (var i = 0; i < $scope.filter_users.length; i++) {
        var temp = $filter('filter')(items, {
          "user_id": parseInt($scope.filter_users[i].id),
          "visible": true
        }, true)
        userstemp = userstemp.concat(temp)
      }
      items = userstemp
    }
    $scope.events_month = items
    $scope.eventSources[0] = items
  }


  $scope.resetFilter = function() {
    $scope.filter_projects = []
    $scope.task_for_filtered_project = []
    $scope.events_month = storedEvents
    $scope.eventSources[0] = storedEvents
    $scope.filter_users = [$scope.session['data']]
    $scope.$storage.filter_users = [$scope.session['data']]
  }


  /****** CALENDAR *****/

  $scope.dayClick = function(date, allDay, jsEvent, view) {};

  //with this you can handle the events that generated by droping any event to different position in the calendar
  $scope.alertOnDrop = function(event, dayDelta, minuteDelta, allDay, revertFunc, jsEvent, ui, view) {
    var item = $filter('filter')($rootScope.events, {
      "id": event.id
    }, true)[0]

    en = angular.copy(item.end)
    end_year = parseInt(en.substring(0, 4));
    end_month = parseInt(en.substring(5, 7)) + dayDelta._data.months;;
    end_day = parseInt(en.substring(8, 10)) + dayDelta._data.days;
    end_hour = parseInt(en.substring(11, 13)) + dayDelta._data.hours;
    end_min = parseInt(en.substring(14, 16)) + dayDelta._data.minutes;
    new_end_date = new Date(end_year, end_month - 1, end_day, end_hour, end_min, 0, 0);

    st = angular.copy(item.start)
    start_year = parseInt(st.substring(0, 4));
    start_month = parseInt(st.substring(5, 7)) + dayDelta._data.months;;
    start_day = parseInt(st.substring(8, 10)) + dayDelta._data.days;
    start_hour = parseInt(st.substring(11, 13)) + dayDelta._data.hours;
    start_min = parseInt(st.substring(14, 16)) + dayDelta._data.minutes;
    new_start_date = new Date(start_year, start_month - 1, start_day, start_hour, start_min, 0, 0);

    var new_end = $filter('date')(new_end_date, 'yyyy-MM-dd HH:mm:ss')
    var new_start = $filter('date')(new_start_date, 'yyyy-MM-dd HH:mm:ss')

    item.end = new_end
    item.start = new_start
    var newEvent = {
      stop: new_end,
      start: new_start
    }

    $http.post("editRecord.json", {
      'table': 'atms_timesheet_task',
      'data': newEvent,
      "id": event.id
    }).
    success(function(data, status, headers, config) {
      flash.show("Operacja powiodła się.", "success");
    }).
    error(function(data, status, headers, config) {
      flash.show("Wystąpił błąd podczas edycji.", "error");
    });
  };

  //with this you can handle the events that generated by resizing any event to different position in the calendar
  $scope.alertOnResize = function(event, dayDelta, minuteDelta, revertFunc, jsEvent, ui, view) {
    var item = $filter('filter')($scope.events_month, {
      "id": event.id
    }, true)[0]
    var hour_delta = dayDelta._data.hours
    var minutes_delta = dayDelta._data.minutes
    var new_hour = parseInt(item.end.split(" ")[1].split(":")[0]) + hour_delta
    var new_minute = parseInt(item.end.split(" ")[1].split(":")[1]) + minutes_delta

    if (new_minute == 0) {
      new_minute = "00"
    }
    if (new_minute == 60) {
      new_minute = "00"
      new_hour = new_hour + 1
    }
    if (new_minute == -30) {
      new_minute = "30"
      new_hour = new_hour - 1
    } else {
      new_minute = new_minute.toString()
    }

    if (new_hour < 10) {
      new_hour = "0" + new_hour.toString()
    } else {
      new_hour = new_hour.toString()
    }

    var old_year = event.end._d.getFullYear()
    var old_month = event.end._d.getMonth()
    var old_day = event.end._d.getDate()

    var new_end = $filter('date')(new Date(old_year, old_month, old_day, new_hour, new_minute, 0), 'yyyy-MM-dd HH:mm:ss')
    item.end = new_end

    var new_duration = 0
    var temp1 = 0
    var temp2 = 0

    if (new_minute == "30") {
      temp2 = parseFloat(new_hour + ".5")
    } else {
      temp2 = parseFloat(new_hour + ".0")
    }

    var hour_start = parseInt(item.start.split(" ")[1].split(":")[0])
    var minute_start = parseInt(item.start.split(" ")[1].split(":")[1])

    if (minute_start == 30) {
      temp1 = parseFloat(hour_start + ".5")
    } else {
      temp1 = parseFloat(hour_start + ".0")
    }
    var new_dur = temp2 - temp1

    event.duration = new_dur.toString() + " h"
    item.duration = new_dur.toString() + " h"

    var newEvent = {
      stop: new_end,
      duration: new_dur
    }

    $http.post("editRecord.json", {
      'table': 'atms_timesheet_task',
      'data': newEvent,
      'id': event.id
    }).
    success(function(data, status, headers, config) {
      flash.show("Zmodyfikowano event.", "success")
    }).
    error(function(data, status, headers, config) {
      flash.show("Wystąpił błąd podczas edycji.", "error")
    });
  };

  //with this you can handle the click on the events
  $scope.eventClick = function(event) {};

  //with this you can handle the events that generated by each page render process
  $scope.renderView = function(view) {};

  $scope.eventRender = function(event, element, icon) {
    element.find('.fc-time').replaceWith('<b>'+ event.duration + '</b>');
    element.bind('contextmenu', function(e) {
      var usr = $filter('filter')($scope.users, {
        "id": event.user_id
      }, true)
      if (usr.length > 0) {
        usr = usr[0]
      }
      $scope.open(event, usr)
      e.preventDefault();
    })
  }


  $scope.changeView = function(view, calendar) {
    uiCalendarConfig.calendars[calendar].fullCalendar('changeView', view);
    $scope.calendar_title = uiCalendarConfig.calendars["myCalendar"].fullCalendar('getView').title
  };


  /* config object */
  $scope.uiConfig = {
    calendar: {
      // height: 1000,
      lang: 'pl',
      editable: true,
      droppable: true,
      lazyFetching: true,
      eventLimit: 3,
      header: null,

      slotDuration: '00:30:00',
      minTime: "08:00:00",
      maxTime: "23:00:00",

      hiddenDays: [0], // nie wyświetlaj niedzieli

      allDay: false,

      dayClick: $scope.dayClick,
      eventDrop: $scope.alertOnDrop,
      eventResize: $scope.alertOnResize,
      eventClick: $scope.eventClick,
      viewRender: $scope.renderView,
      eventRender: $scope.eventRender,
      drop: function(date, allDay) { // this function is called when something is dropped
        if ($scope.selected_event) {
          var temp_event = angular.copy($scope.selected_event)
          var index = $scope.no_assigned_events.indexOf($scope.selected_event)
          $scope.no_assigned_events.splice(index, 1);
          var year = date._d.getFullYear().toString()
          var month = (date._d.getMonth() + 1).toString()
          var day = date._d.getDate().toString();

          var start = $scope.selected_event.start
          var duration = $scope.selected_event.duration.toString()

          if (duration.indexOf(".") >= 0) {
            var end_min = parseInt(start.split(":")[1]) + 30 //parseInt(duration.split(",")[1])
            var end_hour = parseInt(start.split(":")[0]) + parseInt(duration.split(".")[0])
            var d = parseInt(duration.split(":")[0]) + "." + parseInt(duration.split(".")[1])
          } else {
            var end_min = parseInt(start.split(":")[1])
            var end_hour = parseInt(start.split(":")[0]) + parseInt(duration)
            var d = parseInt(duration).toString()
          }

          if (end_min == 0) {
            end_min = "00"
          }
          if (end_min == 60) {
            end_min = "00"
            end_hour = end_hour + 1
          }

          if (end_hour < 10) {
            end_hour = "0" + end_hour.toString()
          }

          if (parseInt(day) < 10) {
            day = "0" + day.toString()
          }

          if (parseInt(month) < 10) {
            month = "0" + month.toString()
          }

          $scope.selected_event.start = year + "-" + month + "-" + day + " " + $scope.selected_event.start + ":00"
          $scope.selected_event.end = year + "-" + month + "-" + day + " " + end_hour.toString() + ":" + end_min.toString() + ":00"

          $(this).remove();

          var newEventData = {
            description: $scope.selected_event.description,
            project_id: $scope.selected_event.project_id,
            task_type_id: $scope.selected_event.task_type_id,
            user_id: $scope.selected_event.user_id,
            start: $scope.selected_event.start,
            stop: $scope.selected_event.end,
            duration: d,
            visible: true
          }

          $scope.loading = true;
          $http.post("addRecord.json", {
            'table': 'atms_timesheet_task',
            'data': newEventData
          }).
          success(function(data, status, headers, config) {
            $scope.selected_event.id = parseInt(data['data'])
            $scope.selected_event.visible = true

            $rootScope.events.push($scope.selected_event)
            var items = $filter('filter')($rootScope.events, {
              "start": year + "-" + month
            })

            $scope.events_month = items
            $scope.eventSources[0] = items

            $scope.selected_event = {}
            $scope.loading = false;

            storedEvents = items
            $scope.runFilter()
            flash.show("Operacja powiodła się.", "success");
          }).
          error(function(data, status, headers, config) {
            console.log(data);
            console.log(status);
            flash.show("Wystąpił błąd.", "error")
          });

          if (!$scope.remove_after_drop) {
            $scope.no_assigned_events.push(temp_event)
          }

        }
      },

    }
  };


  /**
   * Ruch pomiędzy miesiącami
   */
  $scope.move = function(direction) {
      var view = uiCalendarConfig.calendars["myCalendar"].fullCalendar('getView').name //
      var prev_year = parseInt(uiCalendarConfig.calendars["myCalendar"].fullCalendar('getDate').format().split("-")[0])
      var prev_month = parseInt(uiCalendarConfig.calendars["myCalendar"].fullCalendar('getDate').format().split("-")[1])
      var prev_day = parseInt(uiCalendarConfig.calendars["myCalendar"].fullCalendar('getDate').format().split("-")[2])

      if (view != "agendaWeek" || (((prev_day < 8) && (direction == "prev")) || ((prev_day >= 24) && (direction == "next")))) {
        if (direction == "prev") {
          if (prev_month == 1) {
            prev_month = 12;
            prev_year = prev_year - 1;
          } else {
            prev_month = prev_month - 1;
          }
        }

        if (direction == "next") {
          if (prev_month == 12) {
            prev_month = 1;
            prev_year = prev_year + 1;
          } else {
            prev_month = prev_month + 1;
          }
        }
      }

      if (direction == "today") {
        prev_month = new Date().getMonth() + 1;
        prev_year = new Date().getFullYear();
      }
      if (parseInt(prev_month) < 10) {
        prev_month = "0" + prev_month.toString()
      }

      var items = $filter('filter')($rootScope.events, {
        "start": prev_year.toString() + "-" + prev_month.toString(),
        "visible": true
      })

      if (items.length == 0) {
        $scope.loading = true;
        Events.loadEventsData(0, prev_month + "/" + prev_year); //(Session.user.id, prev_month+"/"+prev_year)
      } else {
        $scope.events_month = items
        $scope.eventSources[0] = items //$scope.events_month
        storedEvents = items

        $scope.runFilter()
      }
      uiCalendarConfig.calendars["myCalendar"].fullCalendar(direction);
      $scope.calendar_title = uiCalendarConfig.calendars["myCalendar"].fullCalendar('getView').title
    }

  $scope.select_side_tab = function(tabs) {
    $scope.side_menu_selected_tab = tabs;
  }

  $scope.no_assigned_events = []

  $scope.selected_event = {}
  $scope.selectNewEvent = function(ev) {
    $scope.selected_event = ev;
  }

  $scope.addEvent = function() {
    if (!$scope.selected_project || !$scope.selected_task) {
      flash.show("Wybierz projekt i zadanie.", "info")
      return
    }
    if ($scope.duration_hour == "00" && $scope.duration_min == "00") {
      flash.show("Określ czas pracy.", "info")
      return
    }

    if (!$scope.description || ($scope.description == "")) {
      flash.show("Uzupełnij pole opis.", "info")
      return
    }

    var duration = parseInt($scope.duration_hour)
    if (parseInt($scope.duration_min) == 30) {
      duration = duration.toString() + ".5 h"
    } else {
      duration = duration.toString() + " h"
    }

    var newEvent = {
      project_id: $scope.selected_project.id,
      project: $scope.selected_project,
      task: $scope.selected_task,
      task_type_id: $scope.selected_task.id,
      user_id: Session.user.data.id,
      title: $scope.selected_project.name,
      textColor: $scope.selected_project.color,
      backgroundColor: $scope.selected_project.background,
      borderColor: "white",
      subtitle: $scope.selected_task.name,
      duration: duration,
      description: $scope.description,
      start: $scope.begin_hour + ":" + $scope.begin_min,
      allDay: false
    }
    $scope.no_assigned_events.push(newEvent)

  }

  $scope.open = function(event, user) {
    var modalInstance = $uibModal.open({
      templateUrl: 'myModalContent.html',
      controller: 'ModalInstanceCtrl',
      resolve: {
        item: function() {
          return event;
        },
        user: function() {
          return user;
        }
      }
    });

    modalInstance.result.then(function(retev) {}, function(retev) {});
  };


  $scope.openSettingsPanel = function() {
    var modalInstance = $uibModal.open({
      templateUrl: 'modalSettingsContent.html',
      controller: 'ModalSettingsCtrl',
      size: 'lg',
      resolve: {
      }
    });

    modalInstance.result.then(function(retev) {}, function(retev) {});

  }
})
