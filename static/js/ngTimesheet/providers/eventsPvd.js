// main.js provider
/***** Events *****/
app.provider('Events', function() {
  this.events = null;
  this.$get = function($http, $rootScope) {
    return {
      events: this.events,
      getEvents: function() {
        return this.events;
      },
      loadEventsData: function(user_id, date) { // 0 -pobiera wszystkie eventy
        if (date != "") {
          var month = parseInt(date.split("/")[0])
          var year = parseInt(date.split("/")[1])
        } else {
          var month = ""
          var year = ""
        }
        $http.post("getEvents.json", {
            "user_id": user_id,
            "month": month,
            "year": year
          }) // GET -> {params:{"user_id": user_id}}
          .success(function(res) {
            this.events = res.data;
            $rootScope.$broadcast('eventsDataReady', res.data)
          })
      }
    }
  };
});
