// main.js provider
/***** TASKS TYPE - PROJECTS *****/
app.provider('Relationship', function() {
  this.relationships = null;
  this.$get = function($http, $rootScope) {
    return {
      relationships: this.relationships,
      getRelationship: function() {
        return this.relationships;
      },
      loadRelationshipData: function() {
        $http.get("getProjectTaskType.json")
          .success(function(res) {
            this.relationships = res.data;
            $rootScope.$broadcast('relationshipDataReady', res.data)
          })
      }
    }
  };
});
