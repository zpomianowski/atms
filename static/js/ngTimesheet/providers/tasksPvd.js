// main.js provider
/***** TASKS TYPE *****/
app.provider('Tasks', function() {
  this.tasks = null;
  this.$get = function($http, $rootScope) {
    return {
      tasks: this.tasks,
      getTasks: function() {
        return this.tasks;
      },
      loadTasksData: function() {
        $http.get("getTaskType.json")
          .success(function(res) {
            this.tasks = res.data;
            $rootScope.$broadcast('tasksDataReady', res.data)
          })
      }
    }
  };
});
