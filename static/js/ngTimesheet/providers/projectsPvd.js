// main.js provider
/***** PROJECTS *****/
app.provider('Projects', function() {
  this.projects = null;
  this.$get = function($http, $rootScope) {
    return {
      projects: this.projects,
      getProjects: function() {
        return this.projects;
      },
      loadProjectsData: function() {
        $http.get("getProjects.json")
          .success(function(res) {
            this.projects = res.data;
            $rootScope.$broadcast('projectsDataReady', res.data)
          })
      }
    }
  };
});
