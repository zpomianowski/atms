// main.js provider
/***** USERS *****/
app.provider('Users', function() {
  this.users = null;
  this.$get = function($http, $rootScope) {
    return {
      users: this.users,
      getUsers: function() {
        return this.users;
      },
      loadUsersData: function() {
        $http.get("getUsers.json")
          .success(function(res) {
            this.users = res.data;
            $rootScope.$broadcast('usersDataReady', res.data)
          })
      }
    }
  };
})
