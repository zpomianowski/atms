var app = angular.module('Timesheet', [ "ngSanitize", "ngCsv", "ui.calendar", "ui.router", 'dialogs.main', 'ngTable', 'ui.bootstrap', 'ngAnimate', 'minicolors', 'dndLists', 'angular-datepicker', 'ngStorage']);

/***** ACCESS CONTROL *****/
app.constant('USER_ROLES', {
  all: {
    name: 'all',
    id: 0
  },
  admin: {
    name: 'TimesheetAdmin',
    id: 1
  },
  manager: {
    name: 'Manager',
    id: 2
  },
})

app.service('Session', function() {
  this.create = function(person) {
    this.user = person;
  };
  this.destroy = function() {
    this.user = null;
  };
  return this;
})

app.run(function($http, $timeout, $rootScope, $location, AuthService) {
  //if($location.absUrl().indexOf('timesheet/index')>0 && $location.absUrl().indexOf('user')==-1){ // run only when we are in default/index
  //	$location.path("/")

  AuthService.login()

  $rootScope.$on('$stateChangeStart', function(event, next) {
    if (AuthService.isAuthenticated()) {
      var authorizedRoles = next.data.authorizedRoles;

      if (!AuthService.isAuthorized(authorizedRoles)) {
        event.preventDefault();
      }
    }
  });
  //}
});


app.config(function($stateProvider, $urlRouterProvider, USER_ROLES) {
  $urlRouterProvider.otherwise('/calendar');
  $stateProvider
    .state('calendar', {
      url: '/calendar',
      templateUrl: 'calendar.html',
      data: {
        authorizedRoles: [USER_ROLES.all]
      }
    })
    .state('statistics', {
      url: '/statystyki',
      templateUrl: 'statistics.html',
      data: {
        authorizedRoles: [USER_ROLES.all]
      }
    })
    .state('summation', {
      url: '/podsumowanie',
      templateUrl: 'summation.html',
      data: {
        authorizedRoles: [USER_ROLES.all]
      }
    })
})

/** MAIN CONTROLLER**/
app.controller("MainCtrl", function($scope, $rootScope, $location, top_menu, USER_ROLES, AuthService) {
  $scope.selected_tab = angular.copy($location.path())
  if ($scope.selected_tab == "/") {
    $scope.selected_tab = "/calendar"
  }


  $scope.menu = top_menu

  $scope.userRoles = USER_ROLES;
  $scope.isAuthorized = AuthService.isAuthorized;


  $scope.selectedTab = top_menu.a_calendar;
  $scope.selectTab = function(tab) {
    this.selectedTab = tab;
  };


})
