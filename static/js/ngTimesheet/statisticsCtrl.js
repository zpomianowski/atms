app.controller("statisticsCtrl", function ($scope, $rootScope, $filter, Projects, AuthService, USER_ROLES, Session, Tasks, Relationship, Events, Users, dateFilter) {


    $scope.projects        = Projects.getProjects();
    $scope.tasks           = Tasks.getTasks();
    $scope.users           = Users.getUsers();
    $scope.relationship    = Relationship.getRelationship();
    $scope.events          = Events.getEvents();
    $scope.filtered_events = Events.getEvents();

    $scope.userRoles       = USER_ROLES;
    $scope.isAuthorized    = AuthService.isAuthorized;
    $scope.session         = Session['user'];
/*
  if($scope.isAuthorized($scope.userRoles.admin)){
    if ($scope.users != null) {
      $scope.filtered users = $scope.users.slice(0, 1);
  }
  }else{
    if (Session.hasOwnProperty("user")) {
      $scope.filtered_users = $filter('filter')($scope.users, {"id":Session.user.id}, true)
    }
*/

// jak przelaczamy zakladki (kalendarz/statystki/podsumowanie)
// troche inne klucze niz w $scope.users
    $scope.filtered_users = [$scope.session['data']];

    $scope.begin_date = null;
    $scope.end_date   = null;

/****** LOAD DATA *****/
  /***** PROJECTS *****/
    if (!$scope.projects) {
        Projects.loadProjectsData();
    }
    $scope.$on('projectsDataReady', function (event, data) {
        $scope.projects = data;
        Projects.projects = data;

        $scope.changeLoading();
    });

  /***** TASKA *****/
    if (!$scope.tasks) {
        Tasks.loadTasksData();
    }
    $scope.$on('tasksDataReady', function (event, data) {
        $scope.tasks = data;
        Tasks.tasks = data;


        $scope.changeLoading();
    });

  /***** RELATIONSHIP *****/
    if (!$scope.relationship) {
        Relationship.loadRelationshipData();
    }
    $scope.$on('relationshipDataReady', function (event, data) {
        $scope.relationship = data;
        Relationship.relationships = data;
        $scope.changeLoading();
    });

  /***** EVENTS *****/
    if (!$scope.events) {        /// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!    DATA PICKER
        $scope.loading = true;
        var today = new Date();
        Events.loadEventsData(0, (today.getMonth() + 1) + "/" + today.getFullYear());
    }

    $scope.$on('eventsDataReady', function (event, data) {
      if ($scope.tasks && $scope.projects) {
        for (var  i = 0; i < data.length; i++) {
          var project = $scope.getItemById($scope.projects, data[i].project_id)
          var task = $scope.getItemById($scope.tasks, data[i].task_type_id)
          data[i].textColor = project.color //"#A1D3FF"
          data[i].title = project.name || ""
          data[i].backgroundColor = project.background //"#0000FF"
          data[i].borderColor = "A3ABAF" //project.background //"#FF0000"
          data[i].subtitle= task.name || ""
          data[i].duration = data[i].duration+" h"
          data[i].project = project
          data[i].task = task
          if(!data[i].description){
            data[i].description =""
          }
        }
      }
      if(!$scope.events){
        $scope.events = data;
        Events.events = data;
      }else{
        $scope.events = $scope.events.concat(data);
        Events.events = Events.events.concat(data);
      }
      $scope.changeLoading()
    });

  /***** USERS *****/
    if(!$scope.users){
      Users.loadUsersData()
    }
    $scope.$on('usersDataReady', function (event, data) {
      $scope.users = data;
      $scope.filtered_users = [$scope.session['data']]

      $scope.changeLoading()
    });
//


 $scope.getItemById = function(src, id){
    if(src){
      for(var i = 0; i < src.length; i++){
        if(src[i].id == id){
          return src[i]
        }
      }
    }
    return null
  }

  $scope.changeLoading = function(){
    if($scope.projects && $scope.tasks && $scope.relationship && $scope.events &&  $scope.users){
      $scope.loading = false;
    }else{
      $scope.loading = true;
    }
  }


	$scope.start_date = "2015/02/11"

	// angular-datepicker (wincyj info patrz github)
	$scope.options = {
	  format: 'dd-mm-yyyy', // ISO formatted date
	  editable: true,
	  onClose: function(e) {
	    // do something when the picker closes
	  },
	}


	$scope.fine_events_filter = function(items, begin, end){
		var retn = [];
        if((begin instanceof Date) && (end instanceof Date)){
        	angular.forEach(items, function(item){
        		var year = item.start.split("-")[0]
        		var month = item.start.split("-")[1]
        		var day = item.start.split("-")[2].split(" ")[0]

        		var nd = new Date(year,month-1,day,23,0,0,0)

        		if( nd >= begin) {

        			var year_e = item.end.split("-")[0]
        			var month_e = item.end.split("-")[1]
        			var day_e = item.end.split("-")[2].split(" ")[0]
        			var nd_e = new Date(year_e,month_e-1,day_e,0,0,0,0)
        			if( nd_e <= end){
        				//console.log(item)
        				retn.push(item)
        			}
        		}
        	});
        }
        else if(begin instanceof Date){
        	angular.forEach(items, function(item){
	        	var year = item.start.split("-")[0]
	        	var month = item.start.split("-")[1]
	        	var day = item.start.split("-")[2].split(" ")[0]
	        	var nd = new Date(year,month-1,day,23,0,0,0)

	        	if( nd >= begin) {
	        		retn.push(item)
	        	}
	        });
        }
        else if(end  instanceof Date){
        	angular.forEach(items, function(item){
	        	var year_e = item.end.split("-")[0]
	        	var month_e = item.end.split("-")[1]
	        	var day_e = item.end.split("-")[2].split(" ")[0]
	        	var nd_e = new Date(year_e,month_e-1,day_e,0,0,0,0)
	        	if( nd_e <= end){
	        		retn.push(item)
	        	}
	        });
        }
        return retn;
	}


	$scope.runFilter = function(){
		if($scope.begin_date == null){
			return
		}
		var temp_begin_date = angular.copy($scope.begin_date)
		if(temp_begin_date.getMonth()+1 <10){
			var str_begin = temp_begin_date.getFullYear()+"-0"+(temp_begin_date.getMonth()+1)
		}else{
			var str_begin = temp_begin_date.getFullYear()+"-"+(temp_begin_date.getMonth()+1)
		}

		if($filter('filter')($scope.events, {"start":str_begin},false).length == 0){  // GET events
			var elems = $filter('orderBy')($scope.events, "start", true)
		     if((typeof elems == 'undefined') || (elems.length == 0) ){
		        var year = new Date().getFullYear();
		        var month = new Date().getMonth()+1;
		        var day = 1;
		     }else{
		        var year = elems[elems.length-1].start.split("-")[0]
		        var month = elems[elems.length-1].start.split("-")[1]
		        var day =elems[elems.length-1].start.split("-")[2].split(" ")[0]
		     }
        	var nd = new Date(year,month-1,0,0,0,0,0)

        	$scope.begin_date.setHours(1)
			for (var d = temp_begin_date; d < nd; d.setMonth(d.getMonth() + 1)) {
				Events.loadEventsData(0, (d.getMonth()+1)+"/"+d.getFullYear());
			}
		}else{
			$scope.filtered_events =  $scope.fine_events_filter($scope.events,$scope.begin_date, $scope.end_date );
			$scope.createTable()
		}
	}


	$scope.$watch("events", function(newVal, oldVal){
        $scope.filtered_events =  $scope.fine_events_filter($scope.events,$scope.begin_date, $scope.end_date );
        if($scope.filtered_events.length >0){
        	$scope.createTable()
        }
	})


	$scope.table = {}

	$scope.createTable = function(){
		$scope.table = {}

		angular.forEach($scope.filtered_events, function(value, key) {
			if(value != null){

	 			if(value.project.name in $scope.table){
	 				if(value.task.name in $scope.table[value.project.name] ){
	 					if(value.user_id in $scope.table[value.project.name][value.task.name]){
	 						$scope.table[value.project.name][value.task.name][value.user_id] =  $scope.table[value.project.name][value.task.name][value.user_id] + parseFloat(value.duration)
	 					}else{
	 						$scope.table[value.project.name][value.task.name][value.user_id] =  parseFloat(value.duration)
	 					}
	 				}else{
	 					$scope.table[value.project.name][value.task.name] = {}
	 					$scope.table[value.project.name][value.task.name][value.user_id] =  parseFloat(value.duration)

	 				}
	 			}else{
					$scope.table[value.project.name] = {}
					$scope.table[value.project.name][value.task.name] = {}
					$scope.table[value.project.name][value.task.name][value.user_id] =  parseFloat(value.duration)
	 			}
			}

		}, false);
	}


	$scope.sumAllProject = 0;
	$scope.sumByProject = function(user){
		var time = 0;
		$scope.sumAllProject = 0;
		angular.forEach($scope.table, function(project, projkey){
			angular.forEach(project, function(task, keytask){
				angular.forEach(task, function(vuser, keyuser){
					//$scope.sumAllProject += vuser
					for(var i =0; i < $scope.filtered_users.length; i++){
							if( $scope.filtered_users[i].id == keyuser ){
								$scope.sumAllProject += vuser
							}
						}

					if(keyuser==user){
						time += vuser
					}
				});
			});
		});
		return time
	}


	// handle event
 	$scope.headersleft = "12px"

	$scope.myFunction = function(elem){
		scroll = elem.scrollLeft;
		if(scroll ==0){
			$scope.headersleft = "12px"
		}else{
			$scope.headersleft = (12-scroll)+"px";
		}
		$scope.$apply()

		//header = angular.element(document.querySelector('#usersheader'));
	}

})

app.directive('row', function(){
	// Runs during compile
	return {
		scope: { // {} = isolate, true = child, false/undefined = no change
			users: "=users",
			project: "=project",
			tasks: "=tasks"
		},
		controller: function($scope, $element, $attrs, $transclude){
			//$scope.zoom = false
			$scope.allProjectTime = 0
			$scope.getValue = function(task, user){
				if(user.id in task ){
					return task[user.id]
				}else{
					return 0
				}
			}

			$scope.getTimeProjectUser = function(user){
				var time = 0;
				$scope.allProjectTime = 0
				angular.forEach($scope.tasks, function(value, key){
					angular.forEach(value, function(vuser, keyuser){
						for(var i =0; i < $scope.users.length; i++){
							if( $scope.users[i].id == keyuser ){
								$scope.allProjectTime  += vuser
							}
						}

						if(keyuser==user){
							time += vuser
						}
					});

				});
				return time
			}

			$scope.sumTaskTime = function(task){
				var time =0;
				angular.forEach(task, function(value, key){
					time += value
				})
				return time
			}


		},
		restrict: 'A', // E = Element, A = Attribute, C = Class, M = Comment
		template: '<tr class="project_row"> <td>{{project}}  <i class="fa fa-eye pull-right" ng-click="zoom = !zoom" ></i> </td> '+
					    '<td class="{{zoom? \'active\': \'\'}}" ng-repeat="(userkey, user) in users"> {{getTimeProjectUser(user.id)}}</td>'+
					    '<td style="border-right: 1px solid #4c4f53;" class="{{zoom? \'active\': \'sum\'}} "> {{allProjectTime }}</td>'+
				  '</tr>'+
				  '<tr ng-show="zoom" ng-repeat="(taskkey, task) in tasks"> <td>{{taskkey}}</td>'+
				     '<td ng-repeat="(userkey, user) in users"> {{getValue(task, user)}}</td> '+
				     '<td style="border-right: 1px solid #4c4f53; border-left: 1px dashed #4c4f53;" class="sum">{{sumTaskTime(task)}}</td>'+
				  '</tr>',

		replace: false,
		link: function(scope, iElm, iAttrs, controller) {
		}
	};
});
