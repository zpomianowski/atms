// main.js directive
app.directive('flashmessage', function(flash) {
  return {
    restrict: 'E',
    template: '<div class="{{flash.type}}"  ng-show="flash.visible"  ng-click="flash.visible=!flash.visible"> {{flash.message}} <span id="closeflash" class="pull-right"> × </span></div>',
  }
});
