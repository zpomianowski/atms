// calendarCtrl directive
app.directive('dragg', function() {
  return {
    scope: {
      item: "&"
    },
    restrict: 'A',
    link: function(scope, elem, attr, controller) {
      var item = scope.item()

      elem.draggable({
        zIndex: 999,
        revert: 'invalid',
        revertDuration: 0
      });


    }
  };
});
