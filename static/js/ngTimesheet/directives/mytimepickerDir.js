// calendarCtrl directive
app.directive('mytimepicker', function() {
  return {
    scope: {
      selected_minutes: "=minute",
      selected_hour: "=hour",
      first_hour: "=firstH",
      first_minute: "=firstM"

    },
    controller: function($scope, $element, $attrs, $transclude) {
      var hours = ["00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23"]
      var minutes = ["00", "30"]
      $scope.selected_hour = hours[parseInt($scope.first_hour)];
      $scope.selected_minutes = minutes[parseInt($scope.first_minute)];


      $scope.changehour = function(step) {
        var index = hours.indexOf($scope.selected_hour)
        index = index + step;
        $scope.selected_hour = hours[(index + 24) % 24]
      }

      $scope.changeminute = function(step) {
        var index = minutes.indexOf($scope.selected_minutes)
        index = index + step;
        $scope.selected_minutes = minutes[(index + 2) % 2]

      }
    },
    restrict: 'A', // E = Element, A = Attribute, C = Class, M = Comment
    template: '<table><tbody>\
              <tr class="text-center"> \
               <td class="change_value_arrow" ng-click="changehour(1)" style="width:20px;text-align:center;cursor:pointer"> \
                  <span> <i class="fa fa-chevron-up"></i></span> \
                </td> \
                <td style="width:4px"></td> \
                <td class="change_value_arrow" ng-click="changeminute(1)" style="width:20px; text-align:center;cursor:pointer"> \
                  <span> <i  class="fa fa-chevron-up"></i></span> \
                </td> \
              </tr> \
              <tr> \
                <td style="width:20px;text-align:center" class="form-group"> \
                  <b>{{selected_hour}}</b> \
                </td> \
                <td style="width:4px; text-align:center">:</td> \
                <td style="width:20px;text-align:center" class="form-group"> \
                  <b>{{selected_minutes}}</b> \
                </td> \
              </tr> \
              <tr class="text-center"> \
                <td class="change_value_arrow" ng-click="changehour(-1)" style="width:20px; text-align:center;cursor:pointer"> \
                  <span> <i class="fa fa-chevron-down"></i></span> \
                </td> \
                <td style="width:4px"> </td> \
                <td class="change_value_arrow" ng-click="changeminute(-1)" style="width:20px; text-align:center;cursor:pointer"> \
                  <span> <i class="fa fa-chevron-down"></i></span> \
                </td> \
              </tr></tbody></table>',
    link: function(scope, elem, attrs, controller) {
      elem.find(".change_value_arrow").on('mouseenter', function() {
        elem.find(this).css('color', '#f39200');
      })
      elem.find(".change_value_arrow").on('mouseleave', function() {
        elem.find(this).css('color', '#333');
      })
    }
  };
});
