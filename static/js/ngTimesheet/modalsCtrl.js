app.controller('ModalInstanceCtrl', function ($scope, $rootScope, $uibModalInstance, flash, $filter, $http, uiCalendarConfig, item, user, Events, Projects, Tasks, Relationship) {
  $scope.item = item;
  $scope.flash = flash;
  $scope.newItem = angular.copy(item)
  $scope.user = user;

  $scope.projects = Projects.getProjects();
  $scope.tasks = Tasks.getTasks()
  $scope.relationships = Relationship.getRelationship();

  $scope.selected_project = angular.copy(item.project);
  $scope.selected_task = angular.copy(item.task);
  $scope.description = angular.copy(item.description)


  try{
    $scope.first_begin_hour = $scope.item.start.hour()
  }catch(err){
    $scope.first_begin_hour = new Date($scope.item.start.replace(" ", "T")).getHours()
  }


  if($scope.item.start.minutes() == 30 ){
    $scope.first_begin_minute = 1
  }else{
    $scope.first_begin_minute = 0
  }


  var dur = $scope.item.duration.replace("h", "")

  if(dur.split(".").length >1){
    $scope.first_duration_hour = dur.split(".")[0]
    $scope.first_duration_minute = 1
  }else{
    $scope.first_duration_hour = dur
    $scope.first_duration_minute = 0
  }


  $scope.$watch("selected_project", function(newVal, oldVal){

    if(newVal && (typeof newVal == "object")){
      if( $scope.selected_task.id != item.task.id){
        $scope.selected_task = null
      }

      var temp = $filter('filter')($scope.relationships, {"project_id": newVal.id, "visible":true}, true)
      if(temp==null){return}
      var filtered_tasks_id =[]
      for(var i = 0; i< temp.length; i++){ filtered_tasks_id.push(temp[i].task_type_id) }
      var items = []
      for(var i = 0; i<  $scope.tasks.length; i++){
        if (filtered_tasks_id.indexOf($scope.tasks[i].id) != -1) {
          items.push($scope.tasks[i])
        }
      }
      $scope.task_for_selected_project = items;
    }
  });




  $scope.ok = function () {

    //old_start_date = new Date(item.start.replace(" ", "T"))
    var new_start_date = new Date($scope.item.start._d)
    new_start_date.setHours(parseInt($scope.begin_hour));
    new_start_date.setMinutes(parseInt($scope.begin_min));



    var new_end_date = new Date($scope.item.end._d)
    if( (parseInt($scope.begin_min) + parseInt($scope.duration_min))==60 ) {
      new_end_date.setHours(parseInt($scope.begin_hour)+parseInt($scope.duration_hour)+1 );
      new_end_date.setMinutes(0);
    }else{
      new_end_date.setHours(parseInt($scope.begin_hour)+parseInt($scope.duration_hour));
      new_end_date.setMinutes(parseInt($scope.begin_min)+parseInt($scope.duration_min));
    }

    var new_start = $filter('date')(new_start_date, 'yyyy-MM-dd HH:mm:ss')
    var new_end = $filter('date')(new_end_date, 'yyyy-MM-dd HH:mm:ss')

    var duration = parseInt($scope.duration_hour)
    if(parseInt($scope.duration_min)==30){
      duration = duration.toString()+".5"
    }else{
      duration = duration.toString()
    }

    var newEvent ={
      stop: new_end,
      start: new_start,
      description:  $scope.description,
      project_id: $scope.selected_project.id,
      task_type_id: $scope.selected_task.id,
      duration: duration
    }




    $http.post("editRecord.json", {'table': 'atms_timesheet_task',
                                 'data':newEvent,
                                 'id':$scope.item.id})
      .success(function(data, status, headers, config) {

           ////////////////////////////////////////////////////////

           $scope.item.start.hour(new_start_date.getHours()); // = new_start_date;
           $scope.item.start.minutes(new_start_date.getMinutes());

           $scope.item.end.hour(new_end_date.getHours()); // = new_start_date;
           $scope.item.end.minutes(new_end_date.getMinutes()) ;

           $scope.item._start.hour(new_start_date.getHours()); // = new_start_date;
           $scope.item._start.minutes(new_start_date.getMinutes());

           $scope.item._end.hour(new_end_date.getHours()); // = new_start_date;
           $scope.item._end.minutes(new_end_date.getMinutes()) ;


          ////////
          $scope.item.description = $scope.description;
          $scope.item.duration = duration+" h"
          $scope.item.project = $scope.selected_project
          $scope.item.project_id = $scope.selected_project.id
          $scope.item.task = $scope.selected_task;
          $scope.item.task_id = $scope.selected_task.id
          $scope.item.backgroundColor = $scope.selected_project.background;

          $scope.item.textColor = $scope.selected_project.color;

          $scope.item.subtitle = $scope.selected_task.name;
          $scope.item.title = $scope.selected_project.name

          uiCalendarConfig.calendars["myCalendar"].fullCalendar( 'refetchEvents' )

          //uiCalendarConfig.calendars["myCalendar"].fullCalendar( 'renderEvent', $scope.item.id )

          var temp = $filter('filter')(Events.events, {"id":  $scope.item.id}, true)
          if(temp.length >=1){
            temp[0].description = $scope.description;
            temp[0].duration = duration+" h"
            temp[0].start = new_start
            temp[0].end = new_end

            temp[0].project = $scope.selected_project
            temp[0].project_id = $scope.selected_project.id
            temp[0].task = $scope.selected_task;
            temp[0].task_id = $scope.selected_task.id
            temp[0].backgroundColor = $scope.selected_project.background;

            temp[0].textColor = $scope.selected_project.color;
            temp[0].subtitle = $scope.selected_task.name;
            temp[0].title = $scope.selected_project.name
          }

          $uibModalInstance.dismiss('cancel');
          flash.show("Edycja powiodła się", "success");

        })
        .error(function(data, status, headers, config) {
          $uibModalInstance.dismiss('cancel');
          console.log(data);
          console.log(status);
          flash.show("Nope ;)", "error");
        });
  };


  $scope.remove= function(){
    bootbox.confirm({message: "Usunąć?",
                     buttons: {
                       confirm: {
                         label: "tak",
                         className: "btn-danger"
                       },
                       cancel: {
                         label: "nie",
                         className: "btn-primary"
                       }
                     },
                     title: "<i class=\"fa fa-check-square-o fa-lg\"></i> Potwierdzenie",
                     callback: function (result) {
       if (result) {
       $http.post("delRecord.json", {'table': 'atms_timesheet_task',
                                   'id':$scope.item.id  })
        .success(function(data, status, headers, config) {
          $scope.item.visible = false;

          // z eventow wypadaloby zrobic service i wywalic rootScope
          // dotyczy calendatCtrl i tego Ctrl
          for(var i = 0; i < $rootScope.events.length; i++){
            if($scope.item.id === $rootScope.events[i].id){
              $rootScope.events.splice(i, 1);
            }
          }
          elem = $filter('filter')(Events.events, {"id": $scope.item.id}, true)[0]
          if(elem){
            $filter('filter')(Events.events, {"id": $scope.item.id}, true)[0].visible = false
            var index = Events.events.indexOf(elem);
            Events.events.splice(index, 1);
          }
          uiCalendarConfig.calendars["myCalendar"].fullCalendar('removeEvents', $scope.item._id  )
          $uibModalInstance.dismiss('cancel');
          flash.show("Usunięto element", "success");
        })
        .error(function(data, status, headers, config) {
          console.log(data);
          console.log(status);
          flash.show("Nope ;)", "error")
          $uibModalInstance.dismiss('cancel');
        });
  }  }});
  }


  $scope.cancel = function () {
    $uibModalInstance.dismiss($scope.item);
  };


});
