newsApp.controller('newsCtrl', function($scope, $location, $uibModal, $http, $timeout) {
    'use strict';
    $scope.load = function() {
      $http.get('/atms/default/getNews.py')
        .success(function(data) {
          $scope.news = data.news_items;
        })
        .error(function(err) {

        });
    };

    $scope.load();

    // wyszukiwanie w bazie po słowach kluczowych z inputa
    // tylko jestli jest >= znakow
    $scope.search = function(keywords) {
      $scope.brak = null;
      try {
        if (keywords.length === 0) {
          $scope.load();
        } else if (keywords.length >= 3) {
          keywords = keywords.split(' ');
          $http.post('search.json', {'keywords': keywords})
            .success(function(data) {
              $scope.news = data.news_items;
              if (!data.news_items[0]) {
                $scope.brak = 'brak wyników spełniających kryterium wyszukiwania';
              }
            });
        }
      } catch (err) {}
    };

    // czy artykul wiki jest stary - ma wiecej niz 120 dni
    $scope.isOld = function(date) {
      var modDay = new Date(date).getTime();
      var today = new Date().getTime();
      if ((today - modDay) / (1000 * 3600 * 24) > 120) {
        return true;
      }

      return false;
    };

    // filtr wyswietlania newsow
    $scope.filter = function(news_item, type) {
      if (type == 'wiki') {
        return news_item.slug;
      } else if (type == 'news') {
        return !news_item.slug;
      } else {
        return true;
      }
    };

    // odswiezanie siatki masonry (ogarnac bez timeoutu)
    $scope.refresh = function(time) {
      setTimeout(function() {
        $('.grid').masonry();
      }, time);
    };

    // create the timer variable
    var timer;

    // mouseenter event
    $scope.showIt = function(elem) {
      timer = $timeout(function() {
        $scope.idx = elem.currentTarget.getAttribute('id');
        $scope.hover = true;
        $scope.refresh(10);
      }, 1000);
    };

    // mouseleave event
    $scope.hideIt = function() {
      $timeout.cancel(timer);
      $scope.idx = -1;
      $scope.hover = false;
      $scope.refresh(10);
    };

    // przelaczanie widocznosci przycisku edycji
    $scope.btnVisibility = -1;
    $scope.viewBtn = function(idx) {
      if ($scope.btnVisibility == idx) {
        $scope.btnVisibility = -1;
      } else {
        $scope.btnVisibility = idx;
      }

      $scope.refresh(10);
    };

    $scope.isVisible = function(idx) {
      return ($scope.btnVisibility == idx) ? 'btn-group' : 'btn-group hide';
    };

    // jesli jest art_wiki ? (stary ? kciukwdol : kciukwgore) : chmurka
    // czywiki ? (czystary ? stary : nowy) : news
    $scope.isWiki = function(slug, modified_on) {
      return slug ? ($scope.isOld(modified_on) ? 'fa fa-thumbs-o-down' : 'fa fa-thumbs-o-up') : 'fa fa-cloud';
    };

    // wyswietlanie okna edycji newsa
    $scope.updWin = function(newsId, newsItem) {
      var modalInstance = $uibModal.open({
        templateUrl: 'newsModalContent.html',
        controller: 'newsModalCtrl',
        resolve: {
          item: function() {
            return newsItem;
          }
        }
      });

      modalInstance.result.then(function(retev) {},

                                function(retev) {
                                  $scope.refresh(2);
                                });
    };
  });
