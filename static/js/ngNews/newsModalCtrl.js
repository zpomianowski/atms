// kontroler okna do edycji newsów
newsApp.controller('newsModalCtrl', function($scope, $http, $uibModalInstance, item) {
    //$scope.news_item = angular.copy(item);
    $scope.news_item = item;
    // dodanie nowego, pustego linka do listy
    $scope.addNewLink = function() {
      $scope.news_item.link.push('');
    };

    // usunięcie ostatniego z linków (w widoku zablokowane usunięcie ostatniego)
    $scope.removeLink = function() {
      var lastItem = $scope.news_item.link.length-1;
      $scope.news_item.link.splice(lastItem, 1);
    };

    // zamknięcie okno edycji
    $scope.cancel = function () {
        while ( $scope.news_item.link.slice(-1)[0] == '') {
          console.log($scope.news_item.link.slice(-1)[0]);
          $scope.removeLink();
        }
        $uibModalInstance.dismiss();
    };

    // edycja newsa
    $scope.update = function () {
      item = $scope.news_item;
      $http.post('editRecord.json', {'table': 'atms_news',
                                   'data': {'description': $scope.news_item.description,
                                            'title': $scope.news_item.title,
                                            'link': $scope.news_item.link},
                                   'id': $scope.news_item.id}).
        success( function () {
          $uibModalInstance.dismiss();
          $('#msg').html('Edycja newsa powiodło się').fadeIn('slow').addClass("alert alert-success");
          $('#msg').delay(3000).fadeOut('slow');

        }).
        error ( function () {
          $uibModalInstance.dismiss();
          $('#msg').html('Edycja newsa nie powiodło się').fadeIn('slow').addClass("alert alert-danger");
          $('#msg').delay(3000).fadeOut('slow');

        });
    };

    $scope.add = function () {
      item = $scope.news_item;
      $http.post('addRecord.json',
        { 'table': 'atms_news',
          'data': { 'description': $scope.news_item.description,
                    'title': $scope.news_item.title,
                    'link': $scope.news_item.link }
          }).
      success( function () {
        $uibModalInstance.dismiss();
        $('#msg').html('Dodanie newsa powiodło się').fadeIn('slow').addClass("alert alert-success");
        $('#msg').delay(3000).fadeOut('slow');
      }).
      error ( function () {
        $uibModalInstance.dismiss();
        $('#msg').html('Dodanie newsa nie powiodło się').fadeIn('slow').addClass("alert alert-danger");
        $('#msg').delay(3000).fadeOut('slow');
      });
  };

    // usunięcie newsa
    $scope.remove = function () {
        $http.post('delRecord.json', {'table': 'atms_news',
                                    'id': $scope.news_item.id}).
          success( function () {
            $uibModalInstance.dismiss();
            $('#msg').html('Usunięcie newsa powiodło się').fadeIn('slow').addClass("alert alert-success");
            $('#msg').delay(3000).fadeOut('slow');
          }).
          error ( function () {
            $uibModalInstance.dismiss();
            $('#msg').html('Usunięcie newsa nie powiodło się').fadeIn('slow').addClass("alert alert-danger");
            $('#msg').delay(3000).fadeOut('slow');
          });
    };
});
