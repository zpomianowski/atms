$(function(){
	
	var dropbox = $('#dropbox'),
		message = $('.message', dropbox);
	
	dropbox.filedrop({
		// The name of the $_FILES entry:
		paramname:'pic',
		
		maxfiles: 500,
    	maxfilesize: 100,
		url: REQUEST_UPLOAD,
		
		uploadFinished:function(i,file,response){
			$.data(file).addClass('done');
			//$('#response').append(response);
		},
		
    	error: function(err, file) {
			switch(err) {
				case 'BrowserNotSupported':
					showMessage('Your browser does not support HTML5 file uploads!');
					break;
				case 'TooManyFiles':
					alert('Too many files! Please select 5 at most! (configurable)');
					break;
				case 'FileTooLarge':
					alert(file.name+' is too large! Please upload files up to 2mb (configurable).');
					break;
				default:
					break;
			}
		},
		
		uploadStarted:function(i, file, len){
			createImage(file);
		},
		
		progressUpdated: function(i, file, progress) {
			$.data(file).find('.progress').width(progress);
		}
    	 
	});
	
	var template = '<div class="preview">'+
						'<span class="imageHolder">'+
							'<img />'+
							'<span class="uploaded"></span>'+
						'</span>'+
						'<p id="text"></p>'+
						'<div class="progressHolder">'+
							'<div class="progress"></div>'+
						'</div>'+
					'</div>'; 
	
	
	function createImage(file){

		var preview = $(template), 
			image = $('img', preview);
			text = $('#text', preview);
			
		var reader = new FileReader();
		
		image.width = 100;
		image.height = 100;
		
		reader.onload = function(e){
			if(!file.type.match(/^image\//)){
				image.addClass('customFile');
				var ext = file.name.split('.').pop();
				console.log(ext);
				if (ext == 'doc' ||
					ext == 'docx')
					image.addClass('msword');
				if (ext == 'xls' ||
					ext == 'xlsx')
					image.addClass('msxls');
				if (ext == 'pdf') image.addClass('pdf');
				if (ext == 'py' ||
					ext == 'java' ||
					ext == 'sh' ||
					ext == 'c' ||
					ext == 'cpp' ||
					ext == 'html' ||
					ext == 'css' ||
					ext == 'xml' ||
					ext == 'js')
					image.addClass('code');
				else image.addClass('customFile');
			} else {			
				// e.target.result holds the DataURL which
				// can be used as a source of the image:
				
				image.attr('src',e.target.result);
			}			
		};
		text.html(file.name);
		
		// Reading the file as a DataURL. When finished,
		// this will trigger the onload function above:
		reader.readAsDataURL(file);
		
		message.hide();
		preview.appendTo(dropbox);
		
		// Associating a preview container
		// with the file, using jQuery's $.data():		

		$.data(file,preview);
	}

	function showMessage(msg){
		message.html(msg);
	}

});