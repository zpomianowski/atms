// -------------------------------------------------------------------
// markItUp!
// -------------------------------------------------------------------
// Copyright (C) 2008 Jay Salvat
// http://markitup.jaysalvat.com/
// -------------------------------------------------------------------
// MarkMin tags example
// http://web2py.com/examples/static/markmin.html
//
// file created by Massimo Di Pierro
// -------------------------------------------------------------------
// Feel free to add more tags
// -------------------------------------------------------------------
mySettings = {
  previewParserPath:  '',
  onShiftEnter: { keepDefault:false, openWith:'[[NEWLINE]]\n' },
  onCtrlEnter: { keepDefault:true, replaceWith:'\n[[NEWLINE]][[NEWLINE]][[NEWLINE]]\n' },
  markupSet: [
    {
      name: 'Headings', dropMenu: [
        { name:'Nagłówek 1', key:'1', openWith:'# ',      placeHolder:'Tytuł',    closeWith: '\n' },
        { name:'Nagłówek 2', key:'2', openWith:'\n[[NEWLINE]][[NEWLINE]][[NEWLINE]]\n----\n\n## ',     placeHolder:'Rozdział 1', closeWith: ' [[anch_r1]][[^ #menu]]\n' },
        { name:'Nagłówek 3', key:'3', openWith:'\n[[NEWLINE]][[NEWLINE]][[NEWLINE]]\n----\n\n### ',    placeHolder:'Podrozdział 1',      closeWith: ' [[anch_pr1]][[^ #menu]]\n' },
        { name:'Nagłówek 4', key:'4', openWith:'\n[[NEWLINE]][[NEWLINE]][[NEWLINE]]\n----\n\n#### ',   placeHolder:'Sekcja 1', closeWith: ' [[anch]][[^ #menu]]\n' },
        { name:'Nagłówek 5', key:'5', openWith:'\n[[NEWLINE]][[NEWLINE]][[NEWLINE]]\n----\n\n##### ',  placeHolder:'Podsekcja 1', closeWith: ' [[anch]][[^ #menu]]\n' },
        { name:'Nagłówek 6', key:'6', openWith:'\n[[NEWLINE]][[NEWLINE]][[NEWLINE]]\n----\n\n###### ', placeHolder:'Podsekcja 2', closeWith: ' [[anch]][[^ #menu]]\n' },
        { name:'Menu',       key:'M', replaceWith: '[[NEWLINE]][[NEWLINE]][[NEWLINE]]\n----\n\n## Menu [[menu]]\n- [[Rozdział 1 #anch_r1]]\n-- [[Podrozdział 1 #anch_pr1]]\n' }
      ]
    },
    { separator:'---------------' },
    { name:'Nowa linia', replaceWith:'[[NEWLINE]]\n' },
    { name:'Separator', key: 'G', replaceWith:'\n[[NEWLINE]][[NEWLINE]][[NEWLINE]]\n----\n\n' },
    { separator:'---------------' },
    { name:'Pogrubienie', key:'B', openWith:'**', closeWith:'**' },
    { name:'Kursywa', key:'I', openWith:"''", closeWith:"''" },
    { name:'Przekreślenie', key:'S', openWith: '~~', closeWith:'~~' },
    { separator:'---------------' },
    { name:'Lista', openWith:'-', placeHolder: 'opcja', closeWith: '\n' },
    { name:'Numerowana Lista', openWith:'+', placeHolder: 'opcja', closeWith: '\n' },
    { separator:'---------------' },
    { name:'Obrazek pozycjonowany', key:'P', replaceWith:'[[[![Krótki opis]!] [![Url:!:http://]!] [![Pozycja:!:left]!] [![Rozmiar:!:500px]!]]]' },
    { name:'Galeria', key:'L', openWith:'``', closeWith:'``:img' },
    { name:'Link', key:'P', replaceWith:'[[[![Nazwa linku]!] [[![Krótki opis]!]] [![Url:!:http://]!]]]' },
    { separator:'---------------' },
    { name:'Tabela', key: 'T', openWith:'----\n', placeHolder: '**nagłówek1**|**nagłówek2**\ndane11|dane12\ndane21|dane22', closeWith:'\n----:table-wiki\n' },
    { separator:'---------------' },
    { name:'Cytat', key: 'Q', openWith:'\n-------\n', closeWith:'\n-------\n\n' },
    { name:'Kod źródłowy', key:'<', openWith:'\n``', placeHolder: 'import sys\nprint "test"', closeWith:'``:code_' },
  ]
};

// mIu nameSpace to avoid conflict.
miu = {
  markminTitle: function(markItUp, char) {
    var heading = '';
    var n = $.trim(markItUp.selection || markItUp.placeHolder).length;

    for (i = 0; i < n; i++) {
      heading += char;
    }

    return '\n' + heading;
  }
};
