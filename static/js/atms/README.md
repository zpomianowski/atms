Inline one-page-app for ATMS - A qooxdoo Application
================================================

For debug purposes use "source", for production "build"
Web2py needs some tweaks:
- in atms qooxdoo project create "ln -s <qooxdoo_path> qooxdoo"
- create source: "python generate.py source"
- create build: "python generate.py build"
- take care "web2py/applications/atms/views/manager/index.html" references to "build" in "production" mode and "source" is set as forbidden location in http server settings


