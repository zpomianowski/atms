/**
*@asset(atms/*)
*@asset(qx/icon/${qx.icontheme}/*)
*/

qx.Class.define('atms.WinADB',
{
  extend: qx.ui.container.Composite,

  construct: function(udid) {
    this.base(arguments);
    this.setLayout(new qx.ui.layout.VBox(5));

    this.__toolbar = new qx.ui.toolbar.ToolBar();
    this.__toolbar.setAppearance('toolbar-alert');
    var part1 = new qx.ui.toolbar.Part();
    var reconnectButton = new qx.ui.toolbar.Button(this.tr('Reconnect'), 'atms/bolt.png');
    reconnectButton.addListener('execute', function() {
      this.__R.sendCmd({ cmd: 'INIT' });
    }, this);

    part1.add(reconnectButton);
    var killButton = new qx.ui.toolbar.Button(this.tr('Close session'), 'atms/remove-sign.png');
    killButton.addListener('execute', function() {
      this.__R.sendCmd({ cmd: 'KILL' });
    }, this);

    part1.add(killButton);
    var clearButton = new qx.ui.toolbar.Button(this.tr('Clear output'), 'atms/eraser.png');
    clearButton.addListener('execute', function() {
      this.__output.setValue('');
    }, this);

    part1.add(clearButton);
    this.__toolbar.add(part1);

    var part2 = new qx.ui.toolbar.Part();
    var adbButton = new qx.ui.toolbar.Button(this.tr('ADB'), 'atms/question-sign.png');
    adbButton.addListener('execute', function() {
      qx.bom.Window.open('http://developer.android.com/tools/help/adb.html');
    }, this);

    part2.add(adbButton);
    var shellButton = new qx.ui.toolbar.Button(this.tr('Android Shell'), 'atms/question-sign.png');
    shellButton.addListener('execute', function() {
      qx.bom.Window.open('https://github.com/jackpal/Android-Terminal-Emulator/wiki/Android-Shell-Command-Reference');
    }, this);

    part2.add(shellButton);
    this.__toolbar.add(part2);

    var label1 = new qx.ui.basic.Label('adb -s ' + udid + ' <' + this.tr('Type in a command') + '>');
    this.__cmdLine = new qx.ui.form.TextField();

    var label2 = new qx.ui.basic.Label(this.tr('Output') + ':');
    this.__output = new qx.ui.form.TextArea;
    this.__output.setReadOnly(true);
    this.__output.setAppearance('textarea-code');

    this.add(this.__toolbar);
    this.add(label1);
    this.add(this.__cmdLine);
    this.add(label2);
    this.add(this.__output, { flex: 1 });

    // setup
    this.__setup(udid);

    // events handling
    this.__cmdLine.addListener('keypress', function(e) {
      if (e.getKeyIdentifier() != 'Enter') return;
      this.__R.sendCmd({}, {
        cmd: this.__cmdLine.getValue()
      });
    }, this);

    // ws bridge
    var that = this;
    this.__ws = new atms.WebSocket(null, 'realtime/adbConsole_' + udid + 'C', function(e) {
      if (e.data == 'KILL' || e.data == 'INIT') {
        that._applyConsole(e.data);
        return;
      }

      var oldv = that.__output.getValue() || '';
      that.__output.setValue(oldv + e.data);
      var el = that.__output.getContentElement();
      that.__output.getContentElement().scrollToY(el.getDomElement().scrollHeight);
    });

    this.__R.sendCmd({ cmd: 'INIT' });
  },

  properties: {
    consoleActive: {
      init: false,
      check: 'Boolean',
      apply: '_applyConsole'
    }
  },

  members: {
    __toolbar: null,
    __UDID: null,
    __cmdLine: null,
    __output: null,
    __ws: null,

    __cmdHist: null,

    __R: null,
    __consoleS: null,

    destroy: function() {
      if (this.__ws) this.__ws.close();
      this.__R.sendCmd({
        cmd: 'KILL'
      });
      arguments.callee.base.apply(this, arguments);
    },

    __setup: function(udid) {
      this.__R = new qx.io.rest.Resource({
        sendCmd: {
          method: 'GET',
          url: '/atms/manager/call/json/adbCmd/' + udid + '/{cmd}'
        }
      });
      this.__R.addListener('sendCmdSuccess', function() {

      }, this);

      this.__consoleS  = new qx.data.store.Rest(this.__R, 'sendCmd');
      this.__consoleS.bind('model[1]', this, 'consoleActive');
    },

    _applyConsole: function(newv, old) {
      if (newv == 'INIT') newv = true;
      if (newv == 'KILL') newv = false;
      if (newv != true && newv != false) return;
      if (newv == true) {
        this.__toolbar.addState('info');
        this.__toolbar.removeState('alert');
      } else {
        this.__toolbar.addState('alert');
        this.__toolbar.removeState('info');
      }
    }
  }
});
