/**
*@asset(atms/*)
*@asset(qx/icon/${qx.icontheme}/*)
*/

qx.Class.define('atms.Quiz',
{
  extend: qx.ui.container.Composite,

  construct: function() {
    this.base(arguments);
    this.setLayout(new qx.ui.layout.VBox(5));
    this.__qList = new qx.ui.form.List();
    this.add(this.__qList, { flex: 1 });
    this.__qList.setVisibility('hidden');
    var submitButton = new qx.ui.form.Button(this.tr('Submit'));
    this.add(submitButton);
    submitButton.setEnabled(false);
    submitButton.addListener('execute', function() {
      this.__verify();
      submitButton.setEnabled(false);
    }, this);

    this.__sButton = submitButton;

    this.__setupResources();
    this.__setupStores();

    this.__R.get();
  },

  members: {
    __R: null,
    __S: null,

    __qList: null,
    __sButton: null,

    __setupResources: function() {
      var base_url = '/atms/manager/../quiz_manager/';
      this.__R = new qx.io.rest.Resource({
        get: {
            method: 'GET',
            url: base_url + 'get_quiz.json' },
        verify: {
            method: 'POST',
            url: base_url + 'verify_quiz' }
      });
    },

    __setupStores: function() {
      this.__S = new qx.data.store.Rest(this.__R, 'get', {
        manipulateData: function(data) {
          return data.content;
        }
      });
      this.__S.addListener('changeModel', function(e) {
        var that = this;
        this.__generate();
        setTimeout(function() {
          that.__qList.setVisibility('visible');
          that.__qList.fadeIn(2000);
        }, 2000);
      }, this);
    },

    __generate: function() {
      var model = this.__S.getModel();
      if (model.length > 1)
          this.__sButton.setEnabled(true);
      model.forEach(function(item) {
        var qq = new atms.QuizQ('read');
        qq.load(item.getId());
        this.__qList.add(qq);
      }, this);
    },

    __verify: function() {
      var quiz_result = [];
      var widgets = this.__qList.getChildren();
      widgets.forEach(function(widget) {
        quiz_result.push(widget.getResult());
      });

      this.__R.verify(null,
          qx.util.Serializer.toJson(quiz_result));
    }
  }
});
