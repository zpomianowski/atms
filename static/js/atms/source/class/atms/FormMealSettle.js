/**
*@asset(atms/*)
*@asset(qx/icon/${qx.icontheme}/*)
*/

qx.Class.define('atms.FormMealSettle',
{
  extend: qx.ui.container.Composite,

  events: {
    init: 'qx.event.type.Data'
  },

  construct: function() {
    this.base(arguments);

    // layout
    var that = this;
    var layout = new qx.ui.layout.VBox(8);
    this.setLayout(layout);

    // main form
    var flayout = new qx.ui.layout.Grid(4, 4);
    flayout.setColumnWidth(0, 200);
    flayout.setColumnFlex(1, 1);
    var holder = new qx.ui.container.Composite(flayout);

    // var date = new qx.ui.form.DateField();
    var additional_cost = new qx.ui.form.Spinner().set({
      maximum: 999,
      minimum: -999,
      numberFormat: new qx.util.format.NumberFormat('pl').set({
        postfix: ' PLN'
      })
    });
    var notes = new qx.ui.form.TextArea();

    var form = new qx.ui.form.Form();
    form.add(additional_cost, 'f_cost2share');
    form.add(notes, 'f_notes');
    var ctrl = new qx.data.controller.Form(null, form);

    // holder.add(new qx.ui.basic.Label(this.tr('Day')), { row: 0, column: 0 });
    // holder.add(date, { row: 0, column: 1 });
    holder.add(new qx.ui.basic.Label(
      this.tr('Additional cost')), { row: 1, column: 0 });
    holder.add(additional_cost, { row: 1, column: 1 });
    holder.add(new qx.ui.basic.Label(this.tr('Notes')), { row: 2, column: 0 });
    holder.add(notes, { row: 2, column: 1 });

    // bottom panel
    var submitBar = new qx.ui.container.Composite(
      new qx.ui.layout.Flow(0, 4, 'right'));
    var butSubmit = new qx.ui.form.Button(this.tr('Settle'), 'atms/coin.png');
    butSubmit.addListener('execute', function() {
      var that = this;
      var caption = this.tr('Send the notification about the settlement?');
      var desc = this.tr(
        'This action will send an email with summary of the settlement.');
      var dialog = new atms.WinConfirmation(
          caption,
          desc,  function() {
            that.__R.put({ id: that.__S.getModel().getId() },
              { content: that.__serialize(that.__S.getModel()) }
            );
          },  null);

      this.getApplicationRoot().add(dialog);
      dialog.open();
    }, this);

    butSubmit.addState('critical');
    submitBar.add(butSubmit);

    var table = new atms.table.TableMeal();

    this.add(holder);
    this.add(new qx.ui.basic.Atom(
      this.tr('Be careful! As the warden you can edit ordes!'),
      'atms/warning.png'));
    this.add(table, { flex: 1 });
    this.add(submitBar);

    this.__setupResources();
    this.__S.bind('model', ctrl, 'model');

    this.__R.get();
  },

  members: {
    __R: null,
    __S: null,

    __setupResources: function() {
      var api = {
        get: {
          method: 'GET',
          url: '/atms/meal/api/atms_settlement/id/{id=null}.json'
        },
        put: {
          method: 'PUT',
          url: '/atms/meal/api/atms_settlement.json/?id={id}'
        }
      };
      this.__R = new qx.io.rest.Resource(api);

      this.__R.addListener('putSuccess', function(e) {
        this.getLayoutParent().getLayoutParent().close();
      }, this);

      this.__S = new qx.data.store.Rest(this.__R, 'get', {
        manipulateData: function(data) {
          return data.content[0];
        }
      });
    },

    __serialize: function(model) {
      var data = qx.util.Serializer.toNativeObject(
          model,
          null,
          new qx.util.format.DateFormat('yyyy-MM-dd kk:mm:ss'));
      return qx.lang.Json.stringify([data]);
    }
  }
});
