/**
*@asset(atms/*)
*@asset(qx/icon/${qx.icontheme}/*)
*/

qx.Class.define('atms.DevicesTaskMngr',
{
  extend: qx.ui.container.Composite,
  include: [atms.MWBlocker],

  construct: function(tcase_id, case_id) {
    this.settings = new Object();
    this.settings.tcase_id = tcase_id || null;
    this.settings.case_id = case_id || null;
    this.base(arguments);

    var that = this;

    this.setLayout(new qx.ui.layout.VBox(5));

    var vmngr = new qx.ui.form.validation.Manager();

    var box1 = new qx.ui.container.Composite();
    box1.setLayout(new qx.ui.layout.HBox(10));
    var label_script = new qx.ui.basic.Label(this.tr('Script'));
    label_script.setWidth(70);
    this.__script = new qx.ui.form.TextField(this.tr('Drop script attachment here'));
    this.__script.setMinWidth(150);
    this.__script.setReadOnly(true);
    this.__script.setDroppable(true);
    this.__script.setRequired(true);
    this.__script.addListener('drop', this._handleDrop, this);
    this.__script.setToolTipText(this.tr(
        'Only *.py supported - overridden tester.(mobile/stb).TestCase class'
        ));
    this.__script.addListener('keydown', function(e) {
      if (e.getKeyCode() == 46) {
        this.__caseFuncs.reset();
        this.__attS_sc.setModel(null);
        this.__script.setValue('');
      }

      e.preventDefault();
    }, this);

    // manage script execution
    var codeButton = new qx.ui.form.ToggleButton(
        this.tr('Code editor'), 'atms/code.png');
    codeButton.setMinWidth(120);
    codeButton.addListener('execute', function() {
      if (this.__script.getValue() == '') {
        codeButton.setValue(false);

        this.__R.addListenerOnce('getTemplatesSuccess', function(e) {
          var menu = new qx.ui.menu.Menu();
          menu.setOpener(codeButton);

          e.getData().content.forEach(function(d) {
            var scb = new qx.ui.menu.Button(d, 'atms/code_blue.png', null);
            scb.addListener('execute', function() {
              that.__R.createFile(
                { filename: scb.getLabel() },
                { tcase: that.settings.tcase_id,
                  refcase: that.settings.case_id });
            });

            menu.add(scb);
          });

          menu.open();
        }, this);

        this.__R.getTemplates();

        return;
      }

      if (codeButton.getValue()) {
        var item = this.__attS_sc.getModel().getContent().getItem(0);
        var widget = new atms.editor.FullEditor(item.getId(), false);
        var win = new atms.WinPanel(widget,
          this.tr('Editing') + ': ' + item.getTitle());
        qx.core.Init.getApplication().gDesk1.add(win);
        win.open();
        qx.data.SingleValueBinding
            .bind(codeButton, 'value', win, 'alive');
        qx.data.SingleValueBinding
          .bind(win, 'alive', codeButton, 'value');
      } else {
        qx.data.SingleValueBinding
          .removeAllBindingsForObject(codeButton);
      }
    }, this);

    this.__script.bind('value', codeButton, 'label', {
      converter: function(value) {
        if (value == '') return that.tr('New');
        return that.tr('Code editor');
      }
    });

    box1.add(label_script);
    box1.add(this.__script, { flex: 1 });
    box1.add(codeButton);
    vmngr.add(this.__script);

    var label_timeout = new qx.ui.basic.Label(this.tr('Timeout [min]')).set(
      { width: 200 });
    var nf = new atms.TimeFormat().set({ mode: true });
    var setTimeValue = function(value) {
      var v  = nf.parse(value);
      var fv = nf.format(v);
      this.getContentElement().setValue(fv);
      this.fireDataEvent('changeValue', v);
      return v;
    };

    var getTimeValue = function() {
      var value = this.getContentElement().getValue();
      value = nf.parse(value);
      if (value == 0) return null;
      return value;
    };

    this.__timeout = new qx.ui.form.TextField().set({
      minWidth: 50, width: 200, required: true });
    this.__timeout.setToolTip(new qx.ui.tooltip.ToolTip(this.tr(
      'm - minutes (default)<br>' +
      'M - months<br>' +
      'w - weeks<br>' +
      'd - days<br>' +
      'h - hours<br>' +
      's - seconds<br>' +
      '<b>Example: 1M 1w 3d 12h 34m<b>'
    )).set({ rich: true }));
    this.__timeout.setValue = setTimeValue;
    this.__timeout.getValue = getTimeValue;
    vmngr.add(this.__timeout);

    box1.add(label_timeout);
    box1.add(this.__timeout);

    var box2 = new qx.ui.container.Composite();
    box2.setLayout(new qx.ui.layout.HBox(10));
    var label_bin = new qx.ui.basic.Label(this.tr('Binary file'));
    label_bin.setWidth(70);
    this.__caseBin = new qx.ui.form.TextField(
      this.tr('Drop binary file attachment here'));
    this.__caseBin.setReadOnly(true);
    this.__caseBin.setDroppable(true);
    this.__caseBin.addListener('drop', this._handleDrop, this);
    this.__caseBin.setToolTipText(this.tr(
        'Only *.apk supported (for now)'
        ));

    var manifestButton = new qx.ui.form.Button(
      this.tr('Manifest'), 'atms/code.png').set({
        width: 120
      });
    manifestButton.addListener('execute', function() {
      var bin_id = this.__S.getModel().getContent().getItem(0).getAuto_bin();
      this.__R.postTask({ task: 'apktool' }, { apk_file_id: bin_id });
      var widget = new atms.editor.FullEditor(null, true);
      widget.registerWsSource('realtime/apk_reverse_' + bin_id);
      var win = new atms.WinPanel(widget,
          this.tr("The app's manifest file"));
      qx.core.Init.getApplication().gDesk1.add(win);
      win.open();
    }, this);

    this.__caseBin.bind('value', manifestButton, 'enabled', {
      converter: function(value) {
        if (value == '') return false;
        return true;
      }
    });

    box2.add(label_bin);
    box2.add(this.__caseBin, { flex: 1 });
    box2.add(manifestButton);

    var label_exetime = new qx.ui.basic.Label(
      this.tr('Last execution time [min]')).set({ width: 200 });
    this.__exeTime = new qx.ui.form.TextField().set({
      readOnly: true,
      minWidth: 50,
      width: 200
    });
    this.__exeTime.setValue = setTimeValue;
    this.__exeTime.getValue = getTimeValue;

    box2.add(label_exetime);
    box2.add(this.__exeTime);

    var box3 = new qx.ui.container.Composite();
    box3.setLayout(new qx.ui.layout.HBox(10));
    var label_funcs = new qx.ui.basic.Label(
      this.tr('Tests'));
    label_funcs.setWidth(70);

    this.__caseFuncs = new tokenfield.Token().set({ minChars: 0 });
    this.__caseFuncs.setSelectionMode('multi');
    this.__caseFuncs.setLabelPath('label');
    this.__script.setRequired(true);

    this.__caseFuncs.setTypeInText(
      this.tr('Start typing to find test method'));
    this.__caseFuncs.setSearchingText(
      this.tr('Searching in the script...'));
    this.__caseFuncs.setNoResultsText(
      this.tr('No test methods :('));
    this.__caseFuncs.setInvalidMessage(
      this.tr('At least one test method must be set'));

    this.__caseFuncs.setREST(
      '/atms/manager/call/json/test_methods/' +
      this.settings.case_id + '/{q}/{extra}');

    box3.add(label_funcs);
    box3.add(this.__caseFuncs, { flex: 1 });
    vmngr.add(this.__caseFuncs, function() {
      if (this.__caseFuncs.getValue().length > 0) return true;
      return false;
    }, this);

    this.__devs = new atms.table.TableDevice(
      this.settings.tcase_id,
      this.settings.case_id,
      false);

    var saveButton = new qx.ui.form.Button(
      this.tr('Save'), 'atms/update-white.png');
    saveButton.setMinWidth(100);
    saveButton.addListener('execute', function() {
      if (vmngr.validate()) this.__updateFuncs();
    }, this);

    var fastHostFilter = new qx.ui.toolbar.CheckBox(
      this.tr('Limit scope'), 'atms/filter.png');

    var toolbar = new qx.ui.toolbar.ToolBar();
    var part1 = new qx.ui.toolbar.Part();
    var part2 = new qx.ui.toolbar.Part();

    var fastFilter1 = new qx.ui.toolbar.RadioButton(
      null, 'atms/stb.png');
    var fastFilter2 = new qx.ui.toolbar.RadioButton(
      null, 'atms/meas.png');
    var fastFilter3 = new qx.ui.toolbar.RadioButton(
      null, 'atms/android.png');
    part1.add(fastHostFilter);
    part2.add(fastFilter1);
    part2.add(fastFilter2);
    part2.add(fastFilter3);
    toolbar.add(part1);
    toolbar.add(part2);

    var fastFilterHandler = function() {
      var hvalue = fastHostFilter.getValue();
      var dvalue = fastFilter1.getValue();
      var mvalue = fastFilter2.getValue();
      var avalue = fastFilter3.getValue();

      var model = that.__devs.getModel();
      var query = model.getQuery();
      if (hvalue) {
        var hosts = [];
        for (var i = 0; i < model.getRowCount(); i++) {
          var item = model.getRowData(i);
          if (item.hasOwnProperty('check'))
            if (item['check']) hosts.push(item['host']);
        }

        function onlyUnique(value, index, self) {
          return self.indexOf(value) === index;
        }

        if (hosts.length == 0) return;

        query['host'] = hosts.filter(onlyUnique)[0];

      } else {
        if (query.hasOwnProperty('host')) delete query['host'];
      }

      var types = [];
      if (dvalue) types.push('dut');
      if (mvalue) types.push('lab');
      if (avalue) types.push('ios', 'windows', 'android');
      if (types.length > 0) query['type'] = types;
      else if (query.hasOwnProperty('type')) delete query['type'];

      model.setQuery(query);
      model.reloadData();
    };

    var radioGroup = new qx.ui.form.RadioGroup(
      fastFilter1, fastFilter2, fastFilter3);
    radioGroup.setAllowEmptySelection(true);
    radioGroup.resetSelection();

    fastFilter1.addListener('changeValue', fastFilterHandler);
    fastFilter2.addListener('changeValue', fastFilterHandler);
    fastFilter3.addListener('changeValue', fastFilterHandler);
    fastHostFilter.addListener('changeValue', fastFilterHandler);

    var box5 = new qx.ui.container.Composite();
    box5.setLayout(new qx.ui.layout.HBox(10));
    var spacer = new qx.ui.core.Spacer();
    var runButton = new qx.ui.form.Button(
        this.tr('Save and run'), 'atms/new-white.png');
    runButton.addState('critical');
    runButton.addListener('execute', function() {
      if (vmngr.validate()) {
        this.__R.addListenerOnce('updateSuccess', function() {
          this.__devs.runTests();
        }, this);

        this.__updateFuncs();
      }
    }, this);

    box5.add(toolbar);
    box5.add(spacer, { flex: 1 });
    box5.add(saveButton);
    box5.add(runButton);

    this.add(box1);
    this.add(box2);
    this.add(box3);
    this.add(this.__devs, { flex: 1 });
    if (this.settings.case_id)
        this.add(box5);

    this.__setupResources();
    this.__setupStores();
    this.__setupBindings();

    // call for data
    this.__R.get({ id: case_id || tcase_id });

    try {
      this.__ws = new atms.WebSocket(null, 'realtime/atms_attachments',
      function(e) {
        that.__R.get({ id: case_id || tcase_id });
      });
    } catch (err) {}
  },

  members: {
    settings: null,
    __script: null,
    __caseBin: null,
    __caseFuncs: null,
    __devs: null,

    __R:  null,
    __S:  null,
    __attR_sc:  null,
    __attS_sc:  null,
    __attR_bin:  null,
    __attS_bin:  null,

    __ws: null,

    destroy: function() {
      if (this.__ws) this.__ws.close();
      this.__devs.destroy();
      arguments.callee.base.apply(this, arguments);
    },

    __setupResources: function() {
      var that = this;
      var route = null;
      if (this.settings.tcase_id)
          var route = 'api/atms_tcases';
      if (this.settings.case_id)
          var route = 'api/atms_cases';
      this.__R = new qx.io.rest.Resource({
        get: {
          method: 'GET',
          url: '/atms/manager/' + route + '/id/{id}.json'
        },
        update: {
          method: 'PUT',
          url: '/atms/manager/' + route + '{params}'
        },
        postTask: {
          method: 'POST',
          url: '/atms/manager/task_api/{task}'
        },
        getTemplates: {
          method: 'GET',
          url: '/atms/manager/file_api.json'
        },
        createFile: {
          method: 'POST',
          url: '/atms/manager/file_api.json/atms_attachments/' +
               'create-script/{filename}/{filename}'
        }
      });
      var attconf = { get: {
        method: 'GET',
        url: '/atms/manager/api/atms_attachments/id/{id}.json'
      } };

      this.__attR_sc = new qx.io.rest.Resource(attconf);
      this.__attR_bin = new qx.io.rest.Resource(attconf);

      this.__R.configureRequest(function() {
        try {
          that.getBlocker().block();
        } catch (err) {
          that.addListenerOnce('appear', function() {
            that.getBlocker().block();  });
        }
      });

      this.__R.addListener('success', function() {
        this.getBlocker().unblock();
      }, this);

      this.__R.addListener('error', function() {
        this.getBlocker().unblock();
      }, this);

      this.__R.addListener('updateSuccess', function() {
        jQuery('.flash').html(this.tr('Changes saved').toString());
        jQuery('.flash').show();
      }, this);

      this.__R.addListener('postTaskSuccess', function() {
        jQuery('.flash').html(this.tr('Task queued').toString());
        jQuery('.flash').show();
      }, this);
    },

    __setupStores: function() {
      this.__S = new qx.data.store.Rest(this.__R, 'get');
      this.__S.addListener('changeModel', function(e) {
        var script_id = this.__S.getModel().getContent().getItem(0).getAuto_script();
        var bin_id = this.__S.getModel().getContent().getItem(0).getAuto_bin();
        if (script_id) this.__attR_sc.get({ id: script_id });
        if (bin_id) this.__attR_bin.get({ id: bin_id });
      }, this);

      this.__attS_sc = new qx.data.store.Rest(this.__attR_sc, 'get');
      this.__attS_bin = new qx.data.store.Rest(this.__attR_bin, 'get');
    },

    __setupBindings: function() {
      var that = this;
      this.__attS_sc.bind('model.content[0].title', this.__script, 'value', {
        converter: function(data) {
          if (data) {
            if (data.split('.').pop() == 'py') return data;
            else {
              jQuery('.flash').html(that.tr('Script must by *.py file').toString());
              jQuery('.flash').show();
            }
          }

          return '';
        }
      });
      this.__attS_bin.bind('model.content[0].title', this.__caseBin, 'value', {
        converter: function(data) {
          if (data) {
            if (data.split('.').pop() == 'apk') return data;
            else {
              jQuery('.flash').html(that.tr('Binary must by *.apk file').toString());
              jQuery('.flash').show();
            }
          }

          return '';
        }
      });
      this.__S.bind('model.content[0].auto_funcs', this.__caseFuncs, 'value');
      this.__S.bind('model.content[0].auto_timeout', this.__timeout, 'value');
      this.__S.bind('model.content[0].auto_execution_time',
        this.__exeTime, 'value');
    },

    _handleDrop: function(e) {
      var sender = e.getData('sender');
      var target = e.getData('target');
      if (sender instanceof atms.Uploader) {
        var modelSkeleton = new Object();
        modelSkeleton.id = this.__S.getModel().getContent().getItem(0).getId();
        var att_id = e.getData('atts').getItem(0).getAtms_attachments_id();

        if (target === this.__script) {
          this.__attR_sc.get({ id:  att_id });
          modelSkeleton.auto_script = att_id;
          this.__caseFuncs.reset();
        }

        if (target === this.__caseBin) {
          this.__attR_bin.get({ id:  att_id });
          modelSkeleton.auto_bin = att_id;
        }

        var model = qx.data.marshal.Json.createModel(modelSkeleton);
        this.__R.update(
          { params: '?id=' + model.getId() },
          { content: this.__serialize(model) });
      }
    },

    __updateFuncs: function() {
      var modelSkeleton = new Object();
      modelSkeleton.id = this.settings.case_id || this.settings.tcase_id;
      modelSkeleton.auto_funcs = this.__caseFuncs.getValue();//.split(/[\s,]+/);
      modelSkeleton.auto_timeout = this.__timeout.getValue();
      var model = qx.data.marshal.Json.createModel(modelSkeleton);
      this.__R.update(
        { params: '?id=' + model.getId() },
        { content: this.__serialize(model) });
    },

    __serialize: function(model) {
      var data = qx.util.Serializer.toNativeObject(
        model,
        null,
        new qx.util.format.DateFormat('yyyy-MM-dd kk:mm:ss'));
      return qx.lang.Json.stringify([data]);
    }
  }
});
