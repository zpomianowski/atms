/**
*@asset(atms/*)
*@asset(qx/icon/${qx.icontheme}/*)
*/

qx.Class.define('atms.Gamification',
{
  extend: qx.ui.container.Composite,

  construct: function() {
    this.base(arguments);

    this.setLayout(new qx.ui.layout.VBox());

    // fejmbox & game
    var fb = new qx.ui.groupbox.GroupBox('FejmBox', 'atms/coin.png');
    fb.setLayout(new qx.ui.layout.VBox());
    var game = new qx.ui.groupbox.GroupBox('WannaPlay', 'atms/steam.png');
    game.setLayout(new qx.ui.layout.VBox());
    var gameTest = new qx.ui.groupbox.GroupBox(
        'BeWise', 'atms/steam.png');
    gameTest.setLayout(new qx.ui.layout.VBox());
    var makeQuiz = new qx.ui.form.Button(
        this.tr('Manage my Quiz questions'),
        'atms/cases.png');
    var doQuiz = new qx.ui.form.Button(
        this.tr('Play Quiz'),
        'atms/steam.png');
    gameTest.add(makeQuiz);
    gameTest.add(doQuiz);
    gameTest.add(new qx.ui.basic.Label(
        this.tr(
            'Earn extra EXP points and cash for creating new quiz questions.' +
            '<br>Get EXP and grab your extra bonus for playing!')
        ).set({ rich: true }));

    // fejmbox
    var ratesChartB = new qx.ui.form.Button(
        this.tr('Chart of rates'),
        'atms/statistics.png');
    ratesChartB.addListener('execute', function() {
      var scroll = new qx.ui.container.Scroll();
      var container = new qx.ui.container.Composite(new qx.ui.layout.VBox());
      var widget1 = new atms.Chart({
        layeredBars: {
          args: ['lines', 'linear'],
          url: '/atms/manager/../stats/call/json/rates',
          params: {
            dfrom: {
              type: 'datetime',
              value: -1
            },
            dto: {
              type: 'datetime',
              value: 0
            },
            layer: {
              type: 'select',
              multi: true,
              model: [
                  { id: 'quiz', value: true, label: 'quiz' },
                  { id: 'case', value: true, label: 'case' },
                  { id: 'tcase', value: true, label: 'tcase' },
                  { id: 'test', value: true, label: 'test' },
                  { id: 'ttest', value: true, label: 'ttest' },
                  { id: 'wiki', value: true, label: 'wiki' },
                  { id: 'timesheet', value: true, label: 'timesheet' },
                  { id: 'news', value: true, label: 'news' }
              ],
              path: 'label',
              filterp: 'Label',
              value: []
            }
          }
        }
      });
      container.add(widget1, { flex: 1 });
      scroll.add(container);
      scroll.classname = 'atms.Charts';
      var win = new atms.WinPanel(scroll);
      qx.core.Init.getApplication().gDesk1.add(win);
      win.open();
    });

    var user_cashlabel = new qx.ui.container.Composite(new qx.ui.layout.HBox(4));
    user_cashlabel.add(new qx.ui.basic.Label(this.tr('You have')));
    this.__cashLabel = new qx.ui.basic.Label('0');
    user_cashlabel.add(this.__cashLabel);
    user_cashlabel.add(new qx.ui.basic.Label('$'));
    user_cashlabel.add(new qx.ui.basic.Label(this.tr('ATMS earned')));
    this.__systemCashLabel = new qx.ui.basic.Label('0');
    user_cashlabel.add(this.__systemCashLabel);
    user_cashlabel.add(new qx.ui.basic.Label(this.tr('$ so far')));
    fb.add(user_cashlabel);

    this.__bonusLabel = new qx.ui.basic.Label(this.tr('No bonuses')).set({
      rich: true
    });

    fb.add(ratesChartB);
    fb.add(this.__bonusLabel);

    var store_widget = new qx.ui.container.Composite(new qx.ui.layout.HBox(4));
    this.__fejmsSB = new atms.DynVirtualSelectBox('Label', false).set({
      labelPath: 'label'
    });
    this.__fejmsSB.setMinWidth(300);
    this.__buyB = new qx.ui.form.Button(this.tr('Buy Fejm'), 'atms/coin.png');
    this.__buyB.addListener('execute', function() {
      this.__R.buyFejm({
        fejm: this.__fejmsSB.getDefaultValue()
      });
    }, this);

    store_widget.add(this.__fejmsSB);
    store_widget.add(this.__buyB);
    fb.add(store_widget);

    // game
    this.__victimSB = new atms.DynVirtualSelectBox('Label', false, true, 'victim').set({
      labelPath: 'label'
    });
    this.__actionSB = new atms.DynVirtualSelectBox('Label', false, true, 'action').set({
      labelPath: 'label'
    });
    this.__specialSB = new atms.DynVirtualSelectBox('Label', false, true, 'special').set({
      labelPath: 'label'
    });
    game.add(this.__victimSB);
    game.add(this.__actionSB);
    game.add(this.__specialSB);

    this.__gameBuyB = new qx.ui.form.Button(this.tr('Buy'), 'atms/coin.png');
    game.add(this.__gameBuyB, { flex: 1 });
    this.__gameBuyB.addListener('execute', function() {
      var victims = [];
      var actions = [];
      var specials = [];
      this.__victimSB.getModel().forEach(function(item) {
        if (item.getValue()) victims.push(item.getId());
      });

      this.__actionSB.getModel().forEach(function(item) {
        if (item.getValue()) actions.push(item.getId());
      });

      this.__specialSB.getModel().forEach(function(item) {
        if (item.getValue()) specials.push(item.getId());
      });

      this.__R.buyAction(null, {
        victims: victims,
        actions: actions,
        specials: specials
      });
    }, this);

    // general
    this.add(fb);
    this.add(game);
    this.add(gameTest);

    // configuration
    this.__setupResources();
    this.__setupStores();
    this.__setupBindings();

    this.__R.get();
    this.__R.getFejms();
    this.__R.getVictims();
    this.__R.getActions();
    this.__R.getSpecials();

    // quiz
    makeQuiz.addListener('execute', function() {
      var widget = new atms.QuizManager();
      var caption = this.tr('Make quiz questions');
      var win = new atms.WinPanel(widget, caption);
      qx.core.Init.getApplication().gDesk1.add(win);
      win.open();
    }, this);

    doQuiz.addListener('execute', function() {
      var widget = new atms.Quiz();
      var caption = this.tr('Destroy it!');
      var win = new atms.WinPanel(widget, caption);
      qx.core.Init.getApplication().gDesk1.add(win);
      win.open();
    }, this);

    //tooltip
    // this.__buyB.setToolTip(new qx.ui.tooltip.ToolTip(
    //     "Kupno Milorda likwiduje tytuły innych użytkowników<br> \
    //     Może być tylko jeden Milord!<br>"
    //     ).set({ rich: true }));
    this.__bonusLabel.setToolTip(new qx.ui.tooltip.ToolTip(
        'Kursy promocyjnych bonusów:<br> \
        basic - kurs dla akcji nie objętych promocją<br> \
        test - kurs dla akcji związanych z Testami<br> \
        ttest - kurs dla akcji związanych z Szablonami Testów<br> \
        tcase - kurs dla akcji związanych z Szablonami Przypadków Testowych<br> \
        case - kurs dla akcji związanych z Przypadkami Testowymi<br> \
        wiki - kurs dla akcji związanych z tworzeniem Artykułów Wiki<br> \
        code - kurs dla akcji związanych z tworzeniem Skryptów<br> \
        timesheet - kurs dla akcji związanych z logowaniem pracy'
        ).set({ rich: true }));
  },

  properties: {
    cash: {
      init: 0,
      check: 'Float'
    }
  },

  members: {
    __desktop: null,

    __R: null,
    __S: null,

    __buyB: null,
    __fejmsSB: null,
    __fejmsS: null,
    __cashLabel: null,
    __systemCashLabel: null,
    __bonusLabel: null,

    __victimsS: null,
    __victimSB: null,
    __actionsS: null,
    __actionSB: null,
    __specialsS: null,
    __specialSB: null,
    __gameBuyB: null,

    __setupResources: function() {
      this.__R = new qx.io.rest.Resource({
        get: {
          method: 'GET',
          url: '/atms/manager/../stats/call/json/user_levels/week'
        },
        buyFejm: {
          method: 'POST',
          url: '/atms/manager/../stats/fejmapi/{fejm}'
        },
        getFejms: {
          method: 'GET',
          url: '/atms/manager/../stats/fejmapi.json'
        },
        getActions: {
          method: 'GET',
          url: '/atms/manager/../game/actions.json'
        },
        getSpecials: {
          method: 'GET',
          url: '/atms/manager/../game/specials.json'
        },
        getVictims: {
          method: 'GET',
          url: '/atms/manager/uapi/users.json'
        },
        buyAction: {
          method: 'POST',
          url: '/atms/manager/../game/play'
        }
      });
      this.__R.addListener('buyFejmError', function(e) {
        jQuery('.flash').html(e.getData());
        jQuery('.flash').show();
      }, this);

      this.__R.addListener('buyFejmSuccess', function(e) {
        this.__R.get();
        jQuery('.flash').html(this.tr('Great choice!').toString());
        jQuery('.flash').show();
      }, this);

      this.__R.addListener('buyActionError', function(e) {
        jQuery('.flash').html(e.getData());
        jQuery('.flash').show();
      }, this);

      this.__R.addListener('buyActionSuccess', function(e) {
        this.__R.get();
        jQuery('.flash').html(this.tr('Great choice!').toString());
        jQuery('.flash').show();
      }, this);
    },

    __setupStores: function() {
      this.__S  = new qx.data.store.Rest(this.__R, 'get');
      this.__fejmsS = new qx.data.store.Rest(this.__R, 'getFejms');

      this.__actionsS = new qx.data.store.Rest(this.__R, 'getActions', {
        manipulateData: function(data) {
          data.content.forEach(function(item) {
            item.value = false;
          });

          return data;
        }
      });
      this.__victimsS = new qx.data.store.Rest(this.__R, 'getVictims', {
        manipulateData: function(data) {
          data.content.forEach(function(item) {
            item.value = false;
          });

          return data;
        }
      });
      this.__specialsS = new qx.data.store.Rest(this.__R, 'getSpecials', {
        manipulateData: function(data) {
          data.content.forEach(function(item) {
            item.value = false;
          });

          return data;
        }
      });
    },

    __setupBindings: function() {
      var that = this;
      var store  = this.__S;

      store.bind('model.user.coins', this.__cashLabel, 'value');
      store.bind('model.system.system_coins', this.__systemCashLabel, 'value');
      store.bind('model.user.bonuses', this.__bonusLabel, 'value');
      this.__fejmsS.bind('model.content', this.__fejmsSB, 'model');
      this.__actionsS.bind('model.content', this.__actionSB, 'model');
      this.__victimsS.bind('model.content', this.__victimSB, 'model');
      this.__specialsS.bind('model.content', this.__specialSB, 'model');
    }
  }
});
