/**
*@asset(atms/*)
*@asset(qx/icon/${qx.icontheme}/*)
*/

qx.Class.define('atms.DateField',
{
  extend: qx.ui.form.DateField,

  construct: function() {
    this.base(arguments);
    this.getChildControl('button').setIcon('atms/calendar.png');
    this.setDateFormat(new qx.util.format.DateFormat('yyyy-MM-dd kk:mm'));
  },

  members: {
    _createChildControlImpl: function(id, hash) {
      var control = null;
      switch (id) {
        case 'list':
          control = new atms.DateTimeChooser();
          control.setFocusable(false);
          control.setKeepFocus(true);
          control.addListener('execute', this._onChangeDate, this);
        break;
      }

      control = control || arguments.callee.base.apply(this, arguments);
      return control;
    },

    _onChangeDate: function(e) {
      var textField = this.getChildControl('textfield');

      var selectedDate = this.getChildControl('list').getValue();

      textField.setValue(this.getDateFormat().format(selectedDate));
    },

    _onBlur: function(e) {},

    setValue: function(value) {
      // set the date to the textfield
      var textField = this.getChildControl('textfield');
      textField.setValue(this.getDateFormat().format(value));

      // set the date in the datechooser
      var dateChooser = this.getChildControl('list');
      dateChooser.setValue(value);
    },

    getValue: function() {
      // get the value of the textfield
      var textfieldValue = this.getChildControl('textfield').getValue();

      // return the parsed date
      try {
        return this.getDateFormat().parse(textfieldValue);
      } catch (ex) {
        return null;
      }
    }
  }
});
