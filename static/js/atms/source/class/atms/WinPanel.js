/**
*@asset(atms/*)
*@asset(qx/icon/${qx.icontheme}/*)
*/

qx.Class.define('atms.WinPanel',
{
  extend: qx.ui.window.Window,

  events: {
    changeAlive: 'qx.event.type.Data'
  },

  properties: {
      alive: {
        init: true,
        apply: '__applyAlive',
        event: 'changeAlive'
      }
    },

  statics: {
    X: 10,
    Y: 10,
    INC: 32,
    anim: {
      duration: 300,
      keep: 100,
      keyFrames: {
        0:   { opacity: 0,    scale: 0 },
        50:  { opacity: 0.75, scale: 1.03 },
        100: { opacity: 1,    scale: 1 }
      },
      origin: '50% 50%',
      repeat: 1,
      timing: 'ease-out',
      alternate: false
    }
  },

  construct: function(widget, caption) {
    this.base(arguments);
    this.setLayout(new qx.ui.layout.Grow());
    this.add(widget);
    this.__widget = widget;

    var caption = caption || '';

    this.setShowClose(true);
    this.setShowMaximize(true);

    //this.setShowMinimize(false);
    this.setWidth(320);
    this.setHeight(240);

    this.__setup();
    if (caption) this.setCaption(caption);

    var bounds = this.getLayoutParent().getBounds();
    var hint   = this.getSizeHint();
    atms.WinPanel.X = atms.WinPanel.X < bounds.width - hint.width ? atms.WinPanel.X + atms.WinPanel.INC : 10;
    atms.WinPanel.Y = atms.WinPanel.Y < bounds.height - hint.height - 200 ? atms.WinPanel.Y + atms.WinPanel.INC : 10;

    this.addListener('close',  this.close);

    this.getChildControl('captionbar').setContextMenu(this.__getMenu());

    this.addListener('keypress', function(keyEvent) {
      var where = keyEvent.getKeyIdentifier();
      if (where && keyEvent.isCtrlPressed()) {
        switch (where) {
          case 'Right':
            this._layWin('layout4_3');
          break;

          case 'Left':
            this._layWin('layout4_1');
          break;

          case 'Up':
            this._layWin('layout4_2');
          break;

          case 'Down':
            this._layWin('layout6_2');
          break;
        }
      }
    });

    // this.getChildControl("captionbar").addListener("dblclick", function(e) {
    //     console.log(e.isCtrlPressed());
    //     if (e.isCtrlPressed()) {
    //         return false;
    //     }
    //     return true;
    // }, this);

    this.addListener('appear', function(e) {
      this.animateIn();
    }, this);

    this.show();
    var audiopath = 'resource/atms/open.mp3';
    var audio = new qx.bom.media.Audio(audiopath).set({ volume: 0.01 });
    audio.play();
  },

  members: {
    __hint: null,
    __widget: null,
    __anim: null,

    close: function() {
      if (!this.isVisible()) {
        return;
      }

      var audiopath = 'resource/atms/close.mp3';
      var audio = new qx.bom.media.Audio(audiopath).set({ volume: 0.01 });
      audio.play();

      var that = this;
      that.animateOut();
      setTimeout(function() {
        if (that.fireNonBubblingEvent(
            'beforeClose', qx.event.type.Event, [false, true])) {
          that.hide();
          that.destroyAll();
        }
      }, atms.WinPanel.anim['duration']);
    },

    _onMovePointerUp: function(e) {
      if (this.hasListener('roll')) {
        this.removeListener('roll', this._onMoveRoll, this);
      }

      arguments.callee.base.apply(this, arguments);
    },

    escapeAnim: function(anim) {
      if (qx.core.Environment.get('browser.name') == 'firefox') {
        anim['duration'] = 0;
        return true;
      }

      return false;
    },

    animateIn: function() {
      if (this.escapeAnim(atms.WinPanel.anim)) return;
      var el = this.getContentElement().getDomElement();
      qx.bom.element.Animation.animate(el, atms.WinPanel.anim);
    },

    animateOut: function() {
      if (this.escapeAnim(atms.WinPanel.anim)) return;
      var el = this.getContentElement().getDomElement();
      qx.bom.element.Animation.animateReverse(el, atms.WinPanel.anim);
    },

    destroyAll: function() {
      this.setAlive(false);
      this.__widget.destroy();
      this.destroy();
    },

    getWidget: function() {
      return this.__widget;
    },

    __getMenu: function() {
      var menu = new qx.ui.menu.Menu;

      var layWindow   = new qx.ui.menu.Button(this.tr('Layout the window'),
      'atms/layout.png', null);
      layWindow.addListener('click', this._layWinMngr, this);

      menu.add(layWindow);
      return menu;
    },

    _layWinMngr: function() {
      var win = new qx.ui.window.Window();

      win.addListener('keypress', function(e) {
        if (e.getKeyIdentifier() == 'Escape') {
          win.destroy();
        }
      });

      var bounds = this.getLayoutParent().getBounds();
      win.setWidth(parseInt(0.7 * bounds.width));
      win.setHeight(parseInt(0.7 * bounds.height));
      win.setShowStatusbar(false);
      win.setShowClose(false);
      win.setShowMinimize(false);
      win.setShowMaximize(false);
      win.getChildControl('captionbar').hide();
      win.center();
      win.setAppearance('layout-win');

      // win layout
      var layout = new qx.ui.layout.Grid(10, 10);
      layout.setRowFlex(0, 1);
      layout.setRowFlex(1, 1);
      layout.setColumnFlex(0, 1);
      layout.setColumnFlex(1, 1);
      layout.setColumnFlex(2, 1);
      win.setLayout(layout);

      // layout_6
      var layout6c = new qx.ui.container.Composite();
      var layout6l = new qx.ui.layout.Grid();
      layout6l.setRowFlex(0, 1);
      layout6l.setColumnFlex(0, 1);
      layout6l.setColumnFlex(1, 6);
      layout6c.setLayout(layout6l);
      var layout6_1 = new qx.ui.form.Button(null, 'atms/layout6_1.png');
      var layout6_2 = new qx.ui.form.Button(null, 'atms/layout6_2.png');
      layout6c.add(layout6_1, { row: 0, column: 0, rowSpan: 1, colSpan: 1 });
      layout6c.add(layout6_2, { row: 0, column: 1, rowSpan: 1, colSpan: 1 });
      layout6_1.addListener('execute', function() {
        this._layWin('layout6_1'); win.destroy();
      }, this);

      layout6_2.addListener('execute', function() {
        this._layWin('layout6_2'); win.destroy();
      }, this);

      win.add(layout6c, { row: 0, column: 0, rowSpan: 1, colSpan: 1 });

      // layout_4
      var layout4c = new qx.ui.container.Composite();
      var layout4l = new qx.ui.layout.Grid();
      layout4l.setRowFlex(0, 1);
      layout4l.setColumnFlex(0, 1);
      layout4l.setColumnFlex(1, 1);
      layout4l.setColumnFlex(2, 1);
      layout4c.setLayout(layout4l);
      var layout4_1 = new qx.ui.form.Button(null, 'atms/layout4_1.png');
      var layout4_2 = new qx.ui.form.Button(null, 'atms/layout4_2.png');
      var layout4_3 = new qx.ui.form.Button(null, 'atms/layout4_3.png');
      layout4c.add(layout4_1, { row: 0, column: 0, rowSpan: 1, colSpan: 1 });
      layout4c.add(layout4_2, { row: 0, column: 1, rowSpan: 1, colSpan: 1 });
      layout4c.add(layout4_3, { row: 0, column: 2, rowSpan: 1, colSpan: 1 });
      layout4_1.addListener('execute', function() {
        this._layWin('layout4_1'); win.destroy();
      }, this);

      layout4_2.addListener('execute', function() {
        this._layWin('layout4_2'); win.destroy();
      }, this);

      layout4_3.addListener('execute', function() {
        this._layWin('layout4_3'); win.destroy();
      }, this);

      win.add(layout4c, { row: 0, column: 1, rowSpan: 1, colSpan: 1 });

      // layout_5
      var layout5c = new qx.ui.container.Composite();
      var layout5l = new qx.ui.layout.Grid();
      layout5l.setRowFlex(0, 1);
      layout5l.setColumnFlex(0, 1);
      layout5l.setColumnFlex(1, 1);
      layout5c.setLayout(layout5l);
      var layout5_1 = new qx.ui.form.Button(null, 'atms/layout5_1.png');
      var layout5_2 = new qx.ui.form.Button(null, 'atms/layout5_2.png');
      layout5c.add(layout5_1, { row: 0, column: 0, rowSpan: 1, colSpan: 1 });
      layout5c.add(layout5_2, { row: 0, column: 1, rowSpan: 1, colSpan: 1 });
      layout5_1.addListener('execute', function() {
        this._layWin('layout5_1'); win.destroy();
      }, this);

      layout5_2.addListener('execute', function() {
        this._layWin('layout5_2'); win.destroy();
      }, this);

      win.add(layout5c, { row: 1, column: 0, rowSpan: 1, colSpan: 1 });

      // layout_3
      var layout3c = new qx.ui.container.Composite();
      var layout3l = new qx.ui.layout.Grid();
      layout3l.setRowFlex(0, 1);
      layout3l.setRowFlex(1, 1);
      layout3l.setColumnFlex(0, 1);
      layout3l.setColumnFlex(1, 1);
      layout3c.setLayout(layout3l);
      var layout3_1 = new qx.ui.form.Button(null, 'atms/layout3_1.png');
      var layout3_2 = new qx.ui.form.Button(null, 'atms/layout3_2.png');
      var layout3_3 = new qx.ui.form.Button(null, 'atms/layout3_3.png');
      var layout3_4 = new qx.ui.form.Button(null, 'atms/layout3_4.png');
      layout3c.add(layout3_1, { row: 0, column: 0, rowSpan: 1, colSpan: 1 });
      layout3c.add(layout3_2, { row: 0, column: 1, rowSpan: 1, colSpan: 1 });
      layout3c.add(layout3_3, { row: 1, column: 0, rowSpan: 1, colSpan: 1 });
      layout3c.add(layout3_4, { row: 1, column: 1, rowSpan: 1, colSpan: 1 });
      layout3_1.addListener('execute', function() {
        this._layWin('layout3_1'); win.destroy();
      }, this);

      layout3_2.addListener('execute', function() {
        this._layWin('layout3_2'); win.destroy();
      }, this);

      layout3_3.addListener('execute', function() {
        this._layWin('layout3_3'); win.destroy();
      }, this);

      layout3_4.addListener('execute', function() {
        this._layWin('layout3_4'); win.destroy();
      }, this);

      win.add(layout3c, { row: 1, column: 1, rowSpan: 1, colSpan: 1 });

      // layout_1
      var layout1c = new qx.ui.container.Composite();
      var layout1l = new qx.ui.layout.Grid();
      layout1l.setRowFlex(0, 1);
      layout1l.setRowFlex(1, 6);
      layout1l.setColumnFlex(0, 1);
      layout1l.setColumnFlex(1, 1);
      layout1c.setLayout(layout1l);
      var layout1_1 = new qx.ui.form.Button(null, 'atms/layout1_1.png');
      var layout1_2 = new qx.ui.form.Button(null, 'atms/layout1_2.png');
      var layout1_3 = new qx.ui.form.Button(null, 'atms/layout1_3.png');
      layout1c.add(layout1_1, { row: 0, column: 0, rowSpan: 1, colSpan: 2 });
      layout1c.add(layout1_2, { row: 1, column: 0, rowSpan: 1, colSpan: 1 });
      layout1c.add(layout1_3, { row: 1, column: 1, rowSpan: 1, colSpan: 1 });
      layout1_1.addListener('execute', function() {
        this._layWin('layout1_1'); win.destroy();
      }, this);

      layout1_2.addListener('execute', function() {
        this._layWin('layout1_2'); win.destroy();
      }, this);

      layout1_3.addListener('execute', function() {
        this._layWin('layout1_3'); win.destroy();
      }, this);

      win.add(layout1c, { row: 0, column: 2, rowSpan: 1, colSpan: 1 });

      // layout_2
      var layout2c = new qx.ui.container.Composite();
      var layout2l = new qx.ui.layout.Grid();
      layout2l.setRowFlex(0, 1);
      layout2l.setRowFlex(1, 1);
      layout2l.setColumnFlex(0, 1);
      layout2l.setColumnFlex(1, 6);
      layout2c.setLayout(layout2l);
      var layout2_1 = new qx.ui.form.Button(null, 'atms/layout2_1.png');
      var layout2_2 = new qx.ui.form.Button(null, 'atms/layout2_2.png');
      var layout2_3 = new qx.ui.form.Button(null, 'atms/layout2_3.png');
      layout2c.add(layout2_1, { row: 0, column: 0, rowSpan: 2, colSpan: 1 });
      layout2c.add(layout2_2, { row: 0, column: 1, rowSpan: 1, colSpan: 1 });
      layout2c.add(layout2_3, { row: 1, column: 1, rowSpan: 1, colSpan: 1 });
      layout2_1.addListener('execute', function() {
        this._layWin('layout2_1'); win.destroy();
      }, this);

      layout2_2.addListener('execute', function() {
        this._layWin('layout2_2'); win.destroy();
      }, this);

      layout2_3.addListener('execute', function() {
        this._layWin('layout2_3'); win.destroy();
      }, this);

      win.add(layout2c, { row: 1, column: 2, rowSpan: 1, colSpan: 1 });

      win.open();
    },

    relay: function() {
      this._layWin(this.__hint);
    },

    _layWin: function(where) {
      this.restore();
      this.__hint = where || this.__hint;
      if (!this.__hint) return;

      var newpos = Object();
      newpos.left = 0;
      newpos.top = 0;
      newpos.width = 0;
      newpos.height = 0;
      var bounds = this.getLayoutParent().getBounds();
      switch (this.__hint) {
        case 'layout6_1':
          newpos.left = 0;
          newpos.width = parseInt(0.3333 * bounds.width);
          newpos.height = bounds.height;
        break;

        case 'layout6_2':
          newpos.left = parseInt(0.3333 * bounds.width);
          newpos.width = parseInt(0.6666 * bounds.width);
          newpos.height = bounds.height;
        break;

        case 'layout4_1':
          newpos.left = 0;
          newpos.width = parseInt(0.3333 * bounds.width);
          newpos.height = bounds.height;
        break;

        case 'layout4_2':
          newpos.left = parseInt(0.3333 * bounds.width);
          newpos.width = parseInt(0.3333 * bounds.width);
          newpos.height = bounds.height;
        break;

        case 'layout4_3':
          newpos.left = parseInt(0.6666 * bounds.width);
          newpos.width = parseInt(0.3333 * bounds.width);
          newpos.height = bounds.height;
        break;

        case 'layout1_1':
          newpos.left = 0;
          newpos.width = bounds.width;
          newpos.height = parseInt(0.3333 * bounds.height);
        break;

        case 'layout1_2':
          newpos.left = 0;
          newpos.top = parseInt(0.3333 * bounds.height);
          newpos.width = parseInt(0.5 * bounds.width);
          newpos.height = parseInt(0.6666 * bounds.height);
        break;

        case 'layout1_3':
          newpos.left = parseInt(0.5 * bounds.width);
          newpos.top = parseInt(0.3333 * bounds.height);
          newpos.width = parseInt(0.5 * bounds.width);
          newpos.height = parseInt(0.6666 * bounds.height);
        break;

        case 'layout5_1':
          newpos.left = 0;
          newpos.top = 0;
          newpos.width = parseInt(0.5 * bounds.width);
          newpos.height = bounds.height;
        break;

        case 'layout5_2':
          newpos.left = parseInt(0.5 * bounds.width);
          newpos.top = 0;
          newpos.width = parseInt(0.5 * bounds.width);
          newpos.height = bounds.height;
        break;

        case 'layout3_1':
          newpos.left = 0;
          newpos.top = 0;
          newpos.width = parseInt(0.5 * bounds.width);
          newpos.height = parseInt(0.5 * bounds.height);
        break;

        case 'layout3_2':
          newpos.left = parseInt(0.5 * bounds.width);
          newpos.top = 0;
          newpos.width = parseInt(0.5 * bounds.width);
          newpos.height = parseInt(0.5 * bounds.height);
        break;

        case 'layout3_3':
          newpos.left = 0;
          newpos.top = parseInt(0.5 * bounds.height);
          newpos.width = parseInt(0.5 * bounds.width);
          newpos.height = parseInt(0.5 * bounds.height);
        break;

        case 'layout3_4':
          newpos.left = parseInt(0.5 * bounds.width);
          newpos.top = parseInt(0.5 * bounds.height);
          newpos.width = parseInt(0.5 * bounds.width);
          newpos.height = parseInt(0.5 * bounds.height);
        break;

        case 'layout2_1':
          newpos.left = 0;
          newpos.width = parseInt(0.3333 * bounds.width);
          newpos.height = bounds.height;
        break;

        case 'layout2_2':
          newpos.left = parseInt(0.3333 * bounds.width);
          newpos.width = parseInt(0.6666 * bounds.width);
          newpos.height = parseInt(0.5 * bounds.height);
        break;

        case 'layout2_3':
          newpos.left = parseInt(0.3333 * bounds.width);
          newpos.top = parseInt(0.5 * bounds.height);
          newpos.width = parseInt(0.6666 * bounds.width);
          newpos.height = parseInt(0.5 * bounds.height);
        break;
      }
      console.log(this.__hint);
      this.moveTo(newpos.left, newpos.top);
      this.setWidth(newpos.width);
      this.setHeight(newpos.height);
    },

    __setup: function() {
      var type = this.__widget.classname;
      this.setWidth(320);
      this.setHeight(540);
      switch (type) {
        case 'atms.Tree':
          this.setIcon('atms/folder-close.png');
          this.setCaption(this.tr('Cases'));
          this.getChildControl('captionbar').add(
            this.__createFilter(), { row: 0, column: 5 });
        break;

        case 'atms.UserList':
          this.setIcon('atms/group.png');
          this.setCaption(this.tr('User list'));
        break;

        case 'atms.TestsMngr':
          this.setIcon('atms/calendar.png');
          this.setCaption(this.tr('Tests'));
        break;

        case 'atms.table.TableCase':
          this.setIcon('atms/cases.png');
          this.setCaption(this.tr('My to do list'));
          this.setWidth(640);
          this.setHeight(480);
        break;

        case 'atms.table.TableUECap':
          this.setIcon('atms/devinfo.png');
          this.setCaption(this.tr('UE Capabilites repository'));
          this.__hint = 'layout5_1';
        break;

        case 'atms.table.TableWiFiCap':
          this.setIcon('atms/wifi.png');
          this.setCaption(this.tr('WiFi Capabilites repository'));
          this.__hint = 'layout5_1';
        break;

        case 'atms.FormUECap':
          this.setIcon('atms/devinfo.png');
          this.setWidth(800);
          this.setHeight(540);
        break;

        case 'atms.FormWiFiCap':
          this.setIcon('atms/wifi.png');
          this.setWidth(800);
          this.setHeight(400);
        break;

        case 'atms.FormMeal':
          this.setCaption(this.tr('My order for today'));
          this.setIcon('atms/spoon-knife.png');
          this.setWidth(600);
          this.setHeight(480);
        break;

        case 'atms.FormMealSettle':
          this.setCaption(this.tr('Settle up'));
          this.setIcon('atms/spoon-knife.png');
          this.setWidth(740);
          this.setHeight(540);
        break;

        case 'atms.table.TableCanteen':
          this.setCaption(this.tr('Our canteens'));
          this.setIcon('atms/spoon-knife.png');
          this.setWidth(240);
          this.setHeight(360);
        break;

        case 'atms.ViewHTML':
        case 'atms.ViewHTMLueCap':
          this.setIcon('atms/devinfo.png');
          this.setWidth(600);
          this.setHeight(540);
        break;

        case 'atms.Form':
          this.setWidth(960);
          this.setHeight(540);
        break;

        case 'atms.Uploader':
          this.setIcon('atms/dl.png');
        break;

        case 'atms.Calendar':
          this.setIcon('atms/clock.png');
          this.setCaption(this.tr('Manage tests in time'));
          this.setWidth(640);
          this.setHeight(480);
        break;

        case 'atms.Chart':
          this.setIcon('atms/statistics.png');
          this.setCaption(this.tr('Charts'));
          this.setWidth(960);
          this.setHeight(360);
        break;

        case 'atms.table.TableDevice':
          this.setIcon('atms/inputs.png');
          this.setCaption(this.tr('Devices connected to the farm'));
          this.__hint = 'layout6_1';
        break;

        case 'atms.DevicesTaskMngr':
          this.setIcon('atms/cogs_blue.png');
          this.__hint = 'layout6_1';
        break;

        case 'atms.table.TableServer':
          this.setIcon('atms/tree.png');
          this.setCaption(this.tr('Test nodes'));
          this.setWidth(800);
          this.setHeight(360);
        break;

        case 'atms.Tasks':
        case 'atms.table.TableTask':
          this.setIcon('atms/hour-glass.png');
          this.setCaption(this.tr('Scheduled tasks'));
          this.__hint = 'layout6_2';
        break;

        case 'atms.WinTask':
          this.setIcon('atms/hour-glass.png');
          this.setWidth(960);
          this.setHeight(540);
        break;

        case 'atms.editor.FullEditor':
        case 'atms.editor.Editor':
          this.setIcon('atms/code_blue.png');
          this.setCaption(this.tr('Edit device'));
          this.__hint = 'layout6_2';
        break;

        case 'atms.WinADB':
          this.setIcon('atms/adb.png');
          this.setWidth(680);
          this.setHeight(480);
        break;

        case 'atms.RankList':
          this.setWidth(640);
          this.setIcon('atms/trending-up.png');
        break;

        case 'atms.Comments':
          this.setIcon('atms/bubbles.png');
        break;

        case 'atms.CommentRooms':
          this.setIcon('atms/bubbles.png');
          this.setCaption(this.tr('Active conversations'));
          this.setWidth(360);
          this.setHeight(360);
        break;

        case 'atms.Quiz':
          this.setWidth(600);
        break;
      }
      if (!this.__hint)
          this.addListenerOnce('appear',
            function() { this.moveTo(atms.WinPanel.X, atms.WinPanel.Y); });

      else
          this.addListenerOnce('appear',
            function() { this._layWin(this.__hint); });
    },

    __createFilter: function() {
      var filterButton = new qx.ui.form.Button(null, 'atms/filter.png');
      filterButton.setFocusable(false);
      filterButton.setAppearance('window/maximize-button');
      filterButton.addListener('execute', function() {
        this.__widget.toggleFilter();
        this.setCaption(this.tr('Custom view'));
      }, this);

      return filterButton;
    },

    __applyAlive: function(value) {
          if (!value) this.fireEvent('close');
        }
  }
});
