/* **********************************************************************

Copyright:

License:

Authors:

************************************************************************ */

/**
 * @ignore(this.gPermissions)
 * @lint ignoreDeprecated(alert) *
 * @asset(atms/*)
 * @asset(qx/icon/${qx.icontheme}/*)
 */

qx.Class.define('atms.Application',
  {
    extend: qx.application.Inline,

    members:
      {
        gATMSVersion: 'qx-4.01.11',
        gPermissions: null,

        // gWsServer - must be defined in the index view
        gDesk1: null,
        __ws: null,
        __my_n: null,
        __my_nB: null,
        __news: null,
        gRouter: null,

        main: function () {
          var that = this;

          // Call super class
          this.base(arguments);

          // Enable logging in debug variant
          if (qx.core.Environment.get('qx.debug')) {
            this.gATMSVersion = this.gATMSVersion + '-test';
            qx.locale.Manager.getInstance().setLocale('pl');
            qx.log.appender.Console;
          }

          var req = new qx.io.request.Xhr(
            '/atms/manager/get_basic_permissions.json');
          req.addListener('success', function (e) {
            var req = e.getTarget();
            this.gPermissions = req.getResponse().content;

            var pane = null;
            var windowSelectBox = new qx.ui.form.SelectBox();
            windowSelectBox.setAppearance('selectbox-toolbar');
            windowSelectBox.getChildControl('atom').setVisibility('excluded');
            windowSelectBox.setWidth(36);
            windowSelectBox.addListener('click', function (e) {
              this.removeAll();
              var model = desktop.getWindows();
              model.forEach(function (item) {
                var litem = new qx.ui.form.ListItem(
                  item.getCaption(), item.getIcon());
                litem.addListener('click', function (e) {
                  item.show();
                  windowManager1.bringToFront(item);
                }, this);

                this.add(litem);
              }, this);

              var relayButton = new qx.ui.form.ListItem(
                this.tr('Relayout'),
                'atms/layout.png');
              relayButton.addListener('click', function () {
                desktop.getWindows().forEach(function (item) {
                  if (item.classname == 'atms.WinPanel') { item.relay(); }
                }, this);
              });

              this.add(relayButton);
            }, windowSelectBox);

            // Setup Toolbars
            var bar1 = new qx.ui.toolbar.ToolBar();
            var part1 = new qx.ui.toolbar.Part();
            var part2 = new qx.ui.toolbar.Part();
            var part3 = new qx.ui.toolbar.Part();
            var part4 = new qx.ui.toolbar.Part();
            bar1.add(part1);
            bar1.add(part2);
            bar1.add(part3);
            bar1.add(part4);
            bar1.add(windowSelectBox);
            bar1.add(new qx.ui.core.Spacer(), { flex: 1 });
            var openByNb = new qx.ui.form.TextField().set({
              height: 42,
              width: 200,
              placeholder: this.tr('case nb, i.e. C#1234'),
              decorator: 'no-border'
            });
            openByNb.setToolTip(new qx.ui.tooltip.ToolTip(
              'C#1234 - otwarcie case\'a od ID 1234<br>' +
              'TC#1234 - otwarcie szablonu case\'a o ID 1234<br>' +
              'T#1234 - otwarcie testu o ID 1234<br>'
            ).set({ rich: true }));
            openByNb.addListener('changeValue', function (e) {
              this.__parseStringRequest(e.getData());
            }, this);

            bar1.add(openByNb);

            if (this.gPermissions.nodes.read) {
              var allCasesCheckBox1 = new qx.ui.toolbar.CheckBox(
                this.tr('Case templates'), 'atms/cases.png');
              part1.add(allCasesCheckBox1);
              allCasesCheckBox1.addListener('changeValue', function (e) {
                this.__handleBar(e, 'tcases_templates', desktop, desktop);
              }, this);
            }

            if (this.gPermissions.ttests.read) {
              var allCasesCheckBox1 = new qx.ui.toolbar.CheckBox(
                this.tr('Test templates'), 'atms/folder-close.png');
              part1.add(allCasesCheckBox1);
              allCasesCheckBox1.addListener('changeValue', function (e) {
                this.__handleBar(e, 'ttests_templates', desktop, desktop);
              }, this);
            }

            if (this.gPermissions.tests.read) {
              var testsCheckBox1 = new qx.ui.toolbar.CheckBox(
                this.tr('Tests'), 'atms/calendar.png');
              part1.add(testsCheckBox1);
              testsCheckBox1.addListener('changeValue', function (e) {
                this.__handleBar(e, 'tests', desktop, desktop);
              }, this);
            }

            if (this.gPermissions.users.read) {
              var usersCheckBox1 = new qx.ui.toolbar.CheckBox(
                null, 'atms/group.png');
              part2.add(usersCheckBox1);
              usersCheckBox1.addListener('changeValue', function (e) {
                this.__handleBar(e, 'users', desktop, desktop);
              }, this);
            }

            if (this.gPermissions.cases.read) {
              var casesCheckBox1 = new qx.ui.toolbar.CheckBox(
                this.tr('TODOs'), 'atms/cases.png');
              part2.add(casesCheckBox1);
              casesCheckBox1.addListener('changeValue', function (e) {
                this.__handleBar(e, 'cases', desktop, desktop);
              }, this);
            }

            var devicesCheckBox = new qx.ui.toolbar.CheckBox(
              this.tr('Devices'), 'atms/inputs.png');
            part3.add(devicesCheckBox);
            devicesCheckBox.addListener('changeValue', function (e) {
              this.__handleBar(e, 'devices', desktop, desktop);
            }, this);

            var ueCapCheckBox = new qx.ui.toolbar.CheckBox(
              'UE Caps', 'atms/devinfo.png');
            part3.add(ueCapCheckBox);
            ueCapCheckBox.addListener('changeValue', function (e) {
              this.__handleBar(e, 'uecap', desktop, desktop);
            }, this);

            var wifiCapCheckBox = new qx.ui.toolbar.CheckBox(
              'WiFi Caps', 'atms/wifi.png');
            part3.add(wifiCapCheckBox);
            wifiCapCheckBox.addListener('changeValue', function (e) {
              this.__handleBar(e, 'wificap', desktop, desktop);
            }, this);

            var testnodesCheckBox = new qx.ui.toolbar.CheckBox(
              null, 'atms/tree.png');
            part4.add(testnodesCheckBox);
            testnodesCheckBox.addListener('changeValue', function (e) {
              this.__handleBar(e, 'servers', desktop, desktop);
            }, this);

            var tasksCheckBox = new qx.ui.toolbar.CheckBox(
              null, 'atms/hour-glass.png');
            part4.add(tasksCheckBox);
            tasksCheckBox.addListener('changeValue', function (e) {
              this.__handleBar(e, 'tasks', desktop, desktop);
            }, this);

            var telemCheckBox = new qx.ui.toolbar.Button(
              null, 'atms/meas.png').set({
                toolTip: new qx.ui.tooltip.ToolTip(
                  'login/pass : atms/atms').set({showTimeout: 2000})
              });
            part4.add(telemCheckBox);
            telemCheckBox.addListener('execute', function () {
              qx.bom.Window.open('http://atms.lab.redefine.pl/telemetry');
            }, this);

            this.__my_nB = new qx.ui.toolbar.CheckBox(
              null, 'atms/notifications-on.png').set({ enabled: false });
            part4.add(this.__my_nB);
            this.__my_nB.addListener('changeValue', function (e) {
              this.__handleBar(e, 'chat', desktop, desktop);
            }, this);

            var meal = new qx.ui.toolbar.CheckBox(null, 'atms/spoon-knife.png');
            part4.add(meal);
            meal.addListener('changeValue', function (e) {
              this.__handleBar(e, 'meal', desktop, desktop);
            }, this);
            var current_date = new Date();
            var anim = null;
            if (current_date.getHours() < 12) {
              meal.addListener('appear', function(e) {
                var el = meal.getContentElement().getDomElement();
                var color = qx.theme.manager.Color
                  .getInstance()
                  .resolve('background-selected')
                var desc = {
                  repeat: 'infinite',
                  duration: 3000,
                  timing: 'ease-in',
                  keyFrames : {
                    0   : {"background-color" : "inherit"},
                    50  : {"background-color" : color},
                    100 : {"background-color" : "inherit"}
                }};
                anim = qx.bom.element.Animation.animate(el, desc);
              }, this);
              meal.addListenerOnce('execute', function(e) {
                anim.stop();
              });
            }

            //Prepare desktops
            var windowManager1 = new qx.ui.window.Manager();
            var desktop = new qx.ui.window.Desktop(windowManager1);
            this.gDesk1 = desktop;

            var container_a = new qx.ui.container.Composite(
              new qx.ui.layout.Grow());
            container_a.add(desktop);

            // toolbars and main containers
            var container_A = new qx.ui.container.Composite(
              new qx.ui.layout.VBox());
            container_A.add(bar1);
            container_A.add(container_a, { flex: 1 });
            container_A.add(new qx.ui.basic.Label(this.gATMSVersion).set(
              { appearance: 'attlistitem/size', marginLeft: 4 }));
            this.__news = new atms.FloatingNews();
            container_A.add(this.__news);

            this.getRoot().set({
              blockerColor: '#000000',
              blockerOpacity: 0.7
            });

            // Hint: the second and the third parameter control if the dimensions
            // of the element should be respected or not.
            var htmlElement = document.getElementById('isle');
            var inlineIsle = new qx.ui.root.Inline(htmlElement, true, true);
            inlineIsle.setLayout(new qx.ui.layout.Grow());
            inlineIsle.add(container_A);
            if (!this.gPermissions.nodes.read & !this.gPermissions.tests.read &
              !this.gPermissions.users.read) {
              var caption = this.tr('No basic Permissions');
              var desc = this.tr('Your account settings doesn\'t allow ' +
                'you to read/perform/modify anything in this service.');
              var dialog = new atms.WinConfirmation(
                caption,
                desc, function () {
                  window.location.href = that.gServer + '../default/index';
                }, null);

              desktop.add(dialog);
              dialog.open();
            }

            // for test only
            if (qx.core.Environment.get('qx.debug')) {
              setTimeout(function () {
                var caption = 'Test';

                // var widget = new atms.UserList();
                // var widget = new atms.table.TableUECap();
                // var widget = new atms.table.TableDevice(null, null, true);
                // var widget = new atms.Form('atms_devices',
                //   { id: 15 }, 'update');
                // var widget = new atms.FormUECap(355, 'update'); //347
                // var widget = new atms.ViewHTMLueCap(355);
                // var widget = new atms.FormMealSettle();
                // var widget = new atms.Form('atms_cases', { id: 260 }, 'update');
                // var widget = new atms.WinTask(20836);
                //
                // var win = new atms.WinPanel(widget, caption);
                // desktop.add(win);
                // win.open();
              }, 5);
            }
          }, this);

          req.send();

          this.gRouter = this.__getRouter();
          this.gRouter.executeGet(window.location.href.split('index')[1]);

          // Broadcast
          var handleBroadcast = function (data, context) {
            try {
              var audio = new Audio(JSON.parse(data).audio);
              audio.play();
            } catch (er) { }

            if (!context.gPermissions) { return; }
            context.__my_n = data[context.gPermissions.my_id];

            var sum = 0;
            for (var i in context.__my_n) {
              sum += context.__my_n[i].count;
            }

            if (sum > 0) {
              context.__my_nB.addState('notifications');
              var audiopath = 'resource/atms/open.mp3';
              var audio = new qx.bom.media.Audio(audiopath);
              audio.play();
            } else { context.__my_nB.removeState('notifications'); }
            context.__my_nB.setEnabled(true);
          };

          var handleNotifications = function (data, context) {
            var tplt = '<span style="color:#3d72c9;font-weight: ' +
              'bold;font-size:16px;">\u2d59</span>    ';
            data = tplt + data.replace('\n', '');
            if (context.__news) { context.__news.addLabel(data); }
          };

          try {
            this.__ws = new atms.WebSocket(
              null, 'realtime/broadcast', function (e) {
                handleBroadcast(e.data, that);
              });

            this.__ws = new atms.WebSocket(
              null, 'realtime/notifications', function (e) {
                handleNotifications(e.data, that);
              });
          } catch (err) { }

          var response;
          var req = new qx.io.request.Xhr(
            '/atms/manager/../chat/notify.json', 'GET');
          req.setTimeout(2400000);
          req.addListener('success', function (e) {
            handleBroadcast(req.getResponse().data, this);
          }, this);

          setTimeout(function () {
            req.send();
          }, 1000);

          // meal - force user to fullfill the meal order
          var current_date = new Date();
          if (current_date.getHours() > 11) { return; }
          var meal_cookie = parseInt(qx.bom.Cookie.get('meal'));
          if (isNaN(meal_cookie)) {
            qx.bom.Cookie.set(
              'meal', current_date.getUTCDay());
          }
          if (meal_cookie != current_date.getUTCDay()) {
            this.gRouter.executeGet('/meal/update/0');
            qx.bom.Cookie.set('meal', current_date.getUTCDay());
          }
        },

        __handleBar: function (e, type, desktop_host, desktop_other) {
          var value = e.getData();
          var widget = null;
          if (value) {
            var ctrl = e.getCurrentTarget();
            if (type == 'tcases_templates') { widget = new atms.Tree(desktop_other); }
            if (type == 'ttests_templates') { widget = new atms.Tree(desktop_other, 'ttests'); }
            if (type == 'users') { widget = new atms.UserList(desktop_other); }
            if (type == 'tests') { widget = new atms.TestsMngr(); }
            if (type == 'servers') { widget = new atms.table.TableServer(); }
            if (type == 'tasks') { widget = new atms.table.TableTask(); }
            if (type == 'devices') { widget = new atms.table.TableDevice(null, null, true); }
            if (type == 'cases') { widget = new atms.table.TableCase(null, 'me'); }
            if (type == 'uecap') { widget = new atms.table.TableUECap(); }
            if (type == 'wificap') { widget = new atms.table.TableWiFiCap(); }
            if (type == 'chat') { widget = new atms.CommentRooms(this.__my_n); }
            if (type == 'meal') { widget = new atms.FormMeal(); }
            if (widget === null) { return; }
            var win = new atms.WinPanel(widget);
            qx.data.SingleValueBinding.bind(ctrl, 'value', win, 'alive');
            qx.data.SingleValueBinding.bind(win, 'alive', ctrl, 'value');
            desktop_host.add(win);
            win.open();
          }
        },

        __getRouter: function () {
          var r = new qx.application.Routing();
          r.onGet('/{data_type}/{mode}/{id}', function (data) {
            var widget = null;
            var data_type = data.params.data_type;
            var mode = data.params.mode;
            var id = data.params.id.match(/\d+/)[0];
            switch (data.params.data_type) {
              case 'atms_cases':
                widget = new atms.Form(
                  data_type, { id: id }, mode,
                  null, null);
                break;

              case 'uecap':
                widget = new atms.FormUECap(id, mode);
                break;

              case 'meal':
                widget = new atms.FormMeal();
                break;

              case 'lang':
                qx.locale.Manager.getInstance().setLocale(mode);
                break;
            }

            if (!widget) { return; }
            var that = this;
            setTimeout(function () {
              var win = new atms.WinPanel(widget);
              that.gDesk1.add(win);
              win.open();
            }, 1000);
          }, this);

          return r;
        },

        __parseStringRequest: function (phrase) {
          var pattern = /(\w{0,2})\#(\d+)/i;
          var m = pattern.exec(phrase);
          if (!m) { return; }
          var type = m[1].toUpperCase();
          var nb = parseInt(m[2]);
          var widget = null;
          var table = null;
          switch (type) {
            case '':
            case 'C':
              table = 'atms_cases';
              break;

            case 'TC':
              table = 'atms_tcases';
              break;

            case 'T':
              table = 'atms_tests';
              break;
          }
          var req = new qx.io.request.Xhr(
            '/atms/manager/probe.json/' + table + '/' + nb, 'GET');
          req.addListener('success', function (e) {
            var action = 'update';
            if (req.getResponse().content == false) {
              action = 'create';
              nb = null;
            }

            widget = new atms.Form(table, { id: nb }, action);
            if (!widget) { return; }
            var win = new atms.WinPanel(widget);
            this.gDesk1.add(win);
            win.open();
          }, this);

          req.send();
        },

        gGIP_nodes: function (items) {
          var response;
          var nodes = [];
          var tcases = [];
          items.forEach(function (item) {
            if (item.getIcon() == 'folder') { nodes.push(item.getId()); }
            if (item.getIcon() == 'tcase') { tcases.push(item.getId()); }
          });

          var p = Object();
          var pN = this.gGIP('atms_nodes', nodes);
          var pT = this.gGIP('atms_tcases', tcases);
          if (pN && pT) {
            for (var key in pN) {
              p[key] = Boolean(pN[key] * pT[key]);
            }

            response = p;
          } else {
            if (pN) { response = pN; }
            if (pT) { response = pT; }
          }

          return response;
        },

        gGIP: function (table, items) {
          var response;
          var req = new qx.io.request.Xhr(
            '/atms/manager/get_rows_permissions.json', 'GET');
          req.setAsync(false);
          req.setTimeout(2400000);
          req.addListener('success', function (e) {
            response = req.getResponse();
          }, this);

          req.setRequestData({ table: table, ids: items });
          req.send();
          return response;
        }
      }
  });
