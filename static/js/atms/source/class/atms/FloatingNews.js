/**
*@asset(atms/*)
*@asset(qx/icon/${qx.icontheme}/*)
*/

qx.Class.define('atms.FloatingNews',
{
  extend: qx.ui.container.Composite,

  construct: function() {
    this.base(arguments);
    this.setMaxHeight(28);

    this.setLayout(new qx.ui.layout.HBox(4));
    this.__setupResources();

    this.addListener('appear', function() {
      this.__createAnimation();
      this.fadeIn(500);
    }, this);

    this.__R.get();
  },

  members: {
    __R: null,
    __timer: null,

    __setupResources: function() {
      var base_url = '/atms/manager/../stats/';
      this.__R = new qx.io.rest.Resource({
        get: {
            method: 'GET',
            url: base_url + 'call/json/get_bonuses' }
      });
      this.__R.addListener('getSuccess', function(e) {
        this.setMaxWidth(window.innerWidth);
        this.addLabel(e.getData().content);
        var parentw = this
            .getLayoutParent()
            .getLayoutParent().getSizeHint().width;
        while (this.__getChildrenWidth() < parentw) {
          this.addLabel(e.getData().content);
        }
      }, this);
    },

    __getChildrenWidth: function() {
      var llength = 0;
      var overlap = 0;
      this.getChildren().forEach(function(label) {
        llength += label.getSizeHint().width;
        overlap = label.getSizeHint().width;
      });

      return llength - overlap;
    },

    addLabel: function(content, length) {
      var parentw = this
            .getLayoutParent()
            .getLayoutParent().getSizeHint().width;
      if (this.__getChildrenWidth() > 3 * parentw) return;
      this.add(new qx.ui.basic.Label(content).set({
        rich: true
      }));
    },

    __createAnimation: function() {
      var shift = -1;
      this.__timer = new qx.event.Timer(50);
      this.__timer.addListener('interval', function(e) {
        this.getChildren().forEach(function(label) {
          var el = label.getContentElement().getDomElement();
          if (!el) return;
          var lwidth = label.getSizeHint().width;
          var newleft = qx.bom.element.Location.get(el).left + shift;
          qx.bom.element.Style.set(el, 'left', newleft + 'px');

          if (newleft < -lwidth) {
            this.remove(label);
            this.__R.get();
          }
        }, this);
      }, this);

      this.__timer.start();
    }
  }
});
