/**
*@asset(atms/*)
*@asset(qx/icon/${qx.icontheme}/*)
*/

qx.Class.define('atms.Uploader',
{
  extend: qx.ui.container.Composite,
  include: [atms.MWBlocker],

  properties: {
    remain:
    {
      init: 0,
      check: 'Integer',
      event: 'remainChanged'
    }
  },

  construct: function(table, params, protection_level) {
    this.__protectionLvl = protection_level || [];
    var params = qx.data.marshal.Json.createModel(params);
    var params = qx.util.Serializer.toUriParameter(params);
    this.__params = params;
    this.__table = table;
    this.__uri = '/atms/manager/uploadFile/' + table + '?' + params;
    this.__uritask = '/atms/manager/raiseJobs/?' + params;
    this.base(arguments);
    this.setLayout(new qx.ui.layout.Grow());
    this.__list = new qx.ui.list.List();
    this.__list.setSelectable(true);
    this.__list.setItemHeight(60);
    this.add(this.__list);

    this.setMinWidth(300);

    this.setAppearance('upload-widget');
    this.addState('helpImage');

    // check support for file upload
    if (!(window.File && window.FileReader && window.FileList && window.Blob)) {
      alert('The File APIs are not fully supported in this browser.');
    }

    var that = this;
    this.addListener('appear', function() {
      var element = this.getContentElement().getDomElement();
      element.ondrop = function(e) {
        that.removeState('upload');
        if (that.getRemain() == 0) {
          that.__handleListFiles(that, e);
        } else {
          e.stopPropagation();
          e.preventDefault();
          var caption = that.tr('Uploading process is ongoing');
          var desc = that.tr('Wait until it is done.');
          var dialog = new atms.WinConfirmation(caption, desc, function() {}, null);

          that.getApplicationRoot().add(dialog);
          dialog.open();
        }
      };

      element.ondragover = that.__handleDragOver;
      element.ondragenter = function(e) { that.addState('upload'); };

      element.ondragleave = function(e) { that.removeState('upload'); };

      element.onmouseenter = function(e) {
        that.removeState('helpImage');
      };
    });

    this.addListener('dblclick', function(e) {
      this.__list.getSelection().forEach(function(item) {
        this.__openDownloadLink(item.getAtms_attachments_id());
      }, this);
    }, this);

    // empty Array for items to be uploaded
    this.__itemsToUpload = new qx.data.Array();
    this.addListener('remainChanged', function() {
      if (this.getRemain() == 0) {
        this.__itemsToUpload.removeAll();
        this.__attR.get();
      }
    }, this);

    // configuration
    this.__setupResources();
    this.__setupStores();
    this.__setupBindings();
    this.__list.setSelectionMode('multi');

    this.setContextMenu(this.__getMenu());
    this.__list.getSelection().addListener('change', function(e) {
      var items = this.__list.getSelection();
      if (items.length == 1) {
        var mode = items.getItem(0).getAtms_attachments_type();
        this.__resetMenu(mode);
      } else this.__resetMenu(null);
    }, this);

    this.__dragFeedback = new atms.DragFeedback(this.tr('Move attachments to...'));
    qx.core.Init.getApplication().getRoot().add(this.__dragFeedback);
    this.setDraggable(true);
    this.addListener('droprequest', this._handleDropRequest, this);
    this.addListener('drag', this.__onDrag, this);
    this.addListener('dragend', this.__onDragEnd, this);
    this.addListener('dragstart', this._handleDragStart, this);

    // reload timer
    try {
      var that = this;
      this.__ws = new atms.WebSocket(null, 'realtime/atms_attachments', function(e) {
        that.__attR.get();
      });
    } catch (err) {}
  },

  members: {
    __ws: null,
    __table: null,
    __params: null,
    __uri: null,
    __attR: null,
    __attS: null,
    __list: null,

    __protectionLvl: null,

    __menu: [],

    __itemsToUpload: null,

    __dragFeedback: null,

    destroy: function() {
      if (this.__ws) this.__ws.close();
      arguments.callee.base.apply(this, arguments);
    },

    __handleDragOver: function(e) {
      e.dataTransfer.dropEffect = 'copy';
      e.stopPropagation();
      e.preventDefault();
    },

    __handleFiles: function(files) {
      for (var i = 0, f; f = files[i]; i++) {
        if (f.name.split('.').length == 1) continue;
        var remain = this.getRemain();
        this.setRemain(remain + 1);
        var item = {
          atms_attachments_id: -1,
          atms_attachments_filename: f,
          atms_attachments_title: f.name,
          atms_attachments_size: f.size,
          atms_attachments_type: 'upload/x/x',
          atms_attachments_created_on: '-',
          atms_auth_user_name: '-',
          atms_attachments_refcase: false,
          atms_attachments_tcase: false,
          atms_attachments_test: false,
          progress: 0,
          stored: false
        };
        this.__itemsToUpload.push(item);
      }

      this.__attR.get();
      this.__attR.addListenerOnce('success', function() {
        this.__list.getModel().toArray().forEach(function(item) {
          if (item.getAtms_attachments_id() == -1) {
            var that = this;

            var fd = new FormData();
            var file = item.getAtms_attachments_filename();
            fd.append('myFile', file);

            var xhr = new XMLHttpRequest();
            xhr.open('POST', this.__uri, true);
            xhr.upload.addEventListener('progress', function(e) {
              var p = parseInt((e.loaded / e.total) * 100);
              item.setProgress(p);
            }, false);

            xhr.addEventListener('load', function(e) {
              item.setProgress(100);
              var remain = that.getRemain();
              that.setRemain(remain - 1);
            }, false);

            xhr.send(fd);
          }
        }, this);

        var xhr = new XMLHttpRequest();
        xhr.open('GET', this.__uritask, true);
        xhr.send();
      }, this);
    },

    __handleListFiles: function(context, e) {
      context.__handleFiles(e.dataTransfer.files);
      e.stopPropagation();
      e.preventDefault();
    },

    __setupResources: function() {
      var data_route = 'get_data.json/' + this.__table + '?';
      var file_route = 'file_api/atms_attachments/';
      this.__attR = new qx.io.rest.Resource({
        get: {
          method: 'GET',
          url: '/atms/manager/' + data_route + this.__params
        },
        getTemplates: {
          method: 'GET',
          url: '/atms/manager/file_api.json'
        },
        createFile: {
          method: 'POST',
          url: '/atms/manager/' + file_route +
               '{mode}/{filename}/{template}?' + this.__params
        },
        put: {
          method: 'PUT',
          url: '/atms/manager/api/{table}/?id={id}'
        }
      });

      this.__attR.configureRequest(function(req) {
        try {
          this.getBlocker().block();
        } catch (err) {}
      }, this);

      this.__attR.addListener('success', function() {
        this.getBlocker().unblock();
      }, this);

      this.__attR.addListener('error', function() {
        this.getBlocker().unblock();
      }, this);

      this.__attR.addListener('createFileSuccess', function() {
        this.__attR.get();
      }, this);
    },

    __setupStores: function() {
      var that = this;
      this.__attS = new qx.data.store.Rest(this.__attR, 'get', {
        manipulateData: function(data) {
          data.content.forEach(function(item) {
            item.progress = 100;
            item.stored = true;
            item.atms_auth_user_name =
                item.atms_auth_user_first_name +
                ' ' +
                item.atms_auth_user_last_name;
          });

          data.content = data.content.concat(that.__itemsToUpload.toArray());
          return data;
        }
      });
    },

    __setupBindings: function() {
      var that = this;
      this.__list.setDelegate({
        sorter: function(item_a, item_b) {
          var GA = item_a.getAtms_attachments_type().split('/')[2];
          var GB = item_b.getAtms_attachments_type().split('/')[2];
          var a = item_a.getAtms_attachments_title().toLowerCase();
          var b = item_b.getAtms_attachments_title().toLowerCase();
          var A = item_a.getProgress();
          var B = item_b.getProgress();
          return GA > GB ? 1 : GA < GB ? -1 : A > B ? 1 : A < B ? -1 : a > b ? 1 : a < b ? -1 : 0;
        },

        createItem: function() {
          return new atms.AttachmentView();
        },

        bindItem: function(controller, item, id) {
          controller.bindProperty('atms_attachments_id', 'attID', null, item, id);
          controller.bindProperty('atms_attachments_title', 'label', null, item, id);
          controller.bindProperty('atms_attachments_size', 'size', null, item, id);
          controller.bindProperty('atms_attachments_type', 'icon', {
            converter: function(value, model) {
              var path = '/atms/manager/../static/images/';
              var mime = value.split('/');
              var general = mime[0];
              var ext = mime[2];
              if (general == 'upload') return path + 'upload_x.gif';
              else return path + general + '_' + ext + '.png';
            }
          }, item, id);
          controller.bindProperty('progress', 'progress', null, item, id);
          controller.bindProperty('stored', 'stored', null, item, id);
          controller.bindProperty('atms_auth_user_name', 'created_by', null, item, id);
          controller.bindProperty('atms_attachments_created_on', 'created_on', null, item, id);
          controller.bindProperty('progress', 'progress', null, item, id);
          var convertProtection = function(toCmp) {
            return {
              converter: function(value, model) {
                if (value && (that.__protectionLvl.indexOf(toCmp) + 1))
                    return true;
                else return null;
              }
            };
          };

          controller.bindProperty('atms_attachments_refcase', 'protCase',
              convertProtection('case'), item, id);
          controller.bindProperty('atms_attachments_tcase', 'protTCase',
              convertProtection('tcase'), item, id);
          controller.bindProperty('atms_attachments_test', 'protTest',
              convertProtection('test'), item, id);

        },

        configureItem: function(item) {
          item.getChildControl('icon').setWidth(56);
          item.getChildControl('icon').setHeight(56);
          item.getChildControl('icon').setAllowGrowX(true);
          item.getChildControl('icon').setAllowShrinkX(true);
          item.getChildControl('icon').setScale(true);
        }
      });

      this.__attS.bind('model.content', this.__list, 'model', {
        converter: function(value) {
          return value || new qx.data.Array();
        }
      });
      this.__attR.get();
    },

    __openDownloadLink: function(id) {
      window.open('/atms/manager/download/' + this.__table + '/' + id, '_self', false);
    },

    __getMenu: function() {
      var that = this;
      var menu = new qx.ui.menu.Menu;

      var openScriptButton   = new qx.ui.menu.Button(this.tr('Edit script'),
          'atms/code_blue.png', null);
      var newScriptButton   = new qx.ui.menu.Button(
          this.tr('Create a new script from template'),
          'atms/magic.png', null);
      var delButton   = new qx.ui.menu.Button(this.tr('Delete'),
          'atms/trash.png', null);
      var refreshButton   = new qx.ui.menu.Button(this.tr('Refresh'),
          'atms/refresh.png', null);
      var modifyButton = new qx.ui.menu.Button(this.tr('Edit properties'),
          'atms/edit.png', null);

      refreshButton.addListener('execute', this.__refreshItems, this);
      delButton.addListener('execute', this.__deleteItems, this);
      openScriptButton.addListener('execute', this.__openEditor, this);
      modifyButton.addListener('execute', this.__modifyFile, this);
      newScriptButton.addListener('pointerover', function() {
        if (newScriptButton.getMenu() !== null) return;

        this.__attR.addListenerOnce('getTemplatesSuccess', function(e) {
          var scmenu = new qx.ui.menu.Menu();
          e.getData().content.forEach(function(d) {
            var scb = new qx.ui.menu.Button(d, 'atms/code_blue.png', null);
            scb.addListener('execute', function() {
              that.__createScript(d);
            });

            scmenu.add(scb);
          });

          newScriptButton.setMenu(scmenu);
        });

        this.__attR.getTemplates();
      }, this);

      this.__menu['newScriptButton'] = newScriptButton;
      this.__menu['openScriptButton'] = openScriptButton;
      this.__menu['delButton'] = delButton;
      this.__menu['refreshButton'] = refreshButton;
      this.__menu['modifyButton'] = modifyButton;

      menu.add(openScriptButton);
      menu.add(new qx.ui.menu.Separator());
      menu.add(newScriptButton);
      menu.add(new qx.ui.menu.Separator());
      menu.add(modifyButton);
      menu.add(new qx.ui.menu.Separator());
      menu.add(delButton);
      menu.add(new qx.ui.menu.Separator());
      menu.add(refreshButton);

      return menu;
    },

    __resetMenu: function(mode) {
      var scriptMode = mode == 'text/x-python/py' ? true : false;
      this.__menu['openScriptButton'].setEnabled(scriptMode);
    },

    __modifyFile: function(e) {
      var that = this;
      var file = this.__list.getSelection().getItem(0);

      var form = new qx.ui.form.Form();
      var controller = new qx.data.controller.Form(null, form);
      var name = new qx.ui.form.TextField().set({
        minWidth: 300
      });
      name.setRequired(true);
      form.addGroupHeader(this.tr('Basic properties'));
      form.add(name, this.tr('Filename'), null, 'title');
      var savebutton = new qx.ui.form.Button(this.tr('Save'));
      form.addButton(savebutton);
      var widget = new qx.ui.form.renderer.Single(form);

      var win = new atms.WinPanel(widget, this.tr('Editing') + ': ' + file.getAtms_attachments_title()).set({
        allowMinimize: false,
        allowMaximize: false,
        width: 440,
        height: 100
      });
      qx.core.Init.getApplication().gDesk1.add(win);
      win.open();

      name.setValue(file.getAtms_attachments_title());

      savebutton.addListener('execute', function() {
        if (form.validate()) {
          that.__attR.put(
          {
            id: file.getAtms_attachments_id(),
            table: that.__table
          },
          { content: qx.util.Serializer.toJson(
              [controller.createModel()]) });
          win.close();
        }
      });
    },

    __openEditor: function(e) {
      var that = this;
      var item = that.__list.getSelection().getItem(0);
      var widget = new atms.editor.FullEditor(
        item.getAtms_attachments_id(), false);
      var win = new atms.WinPanel(widget,
          this.tr('Editing') + ': ' + item.getAtms_attachments_title());
      qx.core.Init.getApplication().gDesk1.add(win);
      win.open();
    },

    __createScript: function(template) {
      var that = this;
      var caption = this.tr('Enter the script name');
      var dialog = new atms.WinConfirmation(
          caption,
          null,
          function(value) {
            that.__attR.createFile({
              mode: 'script',
              template: template,
              filename: value
            });
          }, null, true);

      this.getApplicationRoot().add(dialog);
      dialog.open();
    },

    __refreshItems: function() {
      this.__attR.get();
    },

    __deleteItems: function(e) {
      var that = this;
      var s = that.__list.getSelection();
      var caption = s.length > 1 ? this.tr('Delete these files?') : this.tr('Delete this file?');
      var desc = this.tr('This action cannot be undone!');
      var dialog = new atms.WinConfirmation(
        caption,
        desc, function() {
          s.forEach(function(item) {
            var caseProt = (that.__protectionLvl.indexOf('case') + 1) && item.getAtms_attachments_refcase();
            var tcaseProt = (that.__protectionLvl.indexOf('tcase') + 1) && item.getAtms_attachments_tcase();
            var testProt = (that.__protectionLvl.indexOf('test') + 1) && item.getAtms_attachments_test();
            if (caseProt || tcaseProt || testProt) {
              jQuery('.flash').html(
                that.tr(
                    'Attachment "%1" can\'t be removed - go to template edit form',
                    item.getAtms_attachments_title()).toString());
              jQuery('.flash').show();
            } else {
              var req = new qx.io.request.Xhr('/atms/manager/api/' + that.__table + '?id=' + item.getAtms_attachments_id(), 'DELETE');
              req.setTimeout(2400000);
              req.addListener('success', function(e) {
                that.__refreshItems();
                jQuery('.flash').html(
                    that.tr(
                        'Attachment "%1" removed from the server',
                        item.getAtms_attachments_title()).toString());
                jQuery('.flash').show();
              });

              req.send();
            }
          });
        }, null);

      this.getApplicationRoot().add(dialog);
      dialog.open();
    },

    __onDrag: function(e) {
      this.__dragFeedback.setDomPosition(e.getDocumentLeft() + 15, e.getDocumentTop() + 15);
    },

    __onDragEnd: function(e) {
      this.__dragFeedback.setDomPosition(-1000, -1000);
      this.__dragFeedback.purgeChildren();
    },

    _handleDragStart: function(e) {
      e.addType('sender');
      e.addType('target');
      e.addType('atts');
      e.addAction('move');

      var that = this;
      var selection = this.__list.getSelection();
      if (selection.length === 0) {
        if (e.getDragTarget().classname !== 'atms.AttachmentView') return;
        this.__list.getModel().forEach(function(item) {
          if (e.getDragTarget().getAttID() == item.getAtms_attachments_id())
            selection.push(item);
        }, this);
      }

      selection.forEach(function(item) {
        var itemWidget = new qx.ui.basic.Label(item.getAtms_attachments_title());
        that.__dragFeedback.addDragChild(itemWidget);
      });
    },

    _handleDropRequest: function(e) {
      e.addData('sender', this);
      e.addData('target', e.getRelatedTarget());
      e.addData('atts', this.__list.getSelection());
    }
  }
});
