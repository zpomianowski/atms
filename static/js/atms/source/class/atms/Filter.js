/**
*@asset(atms/*)
*@asset(qx/icon/${qx.icontheme}/*)
*/

qx.Class.define('atms.Filter',
{
  extend: qx.ui.core.Widget,

  construct: function(params, callback, mini) {
    this.__params = params;
    this.__callback = callback;
    this.__mini = mini || false;
    this.base(arguments);
    var layout = new qx.ui.layout.VBox(5);
    this.__layout2 = new qx.ui.layout.HBox(5);
    this._setLayout(layout);

    this.__timer = new qx.event.Timer(500);
    this._createChildControl('filter-button');
    this._createChildControl('filter-box');

    for (var i in this.__params) {
      this._createChildControl(i);
    }

    this.setAppearance('atms-filterBox');
    this.__filterMap = [];
  },

  members: {
    __params: null,
    __timer: null,

    __filterMap: null,
    __queryItems: null,
    __callback: null,
    __mini: null,
    __comp: null,

    _createChildControlImpl: function(id) {
      var control;
      switch (id) {
        case 'filter-button':
          var comp = new qx.ui.container.Composite();
          this.__comp = comp;
          comp.setLayout(this.__layout2);
          control = new qx.ui.form.Button(this.tr('Run filter'), 'atms/fire.png');
          control.addListener('execute', function() {
            this.__buildQuery();
          }, this);

          var showFilterQ = new qx.ui.form.Button(null, 'atms/eye.png');
          showFilterQ.addListener('execute', function() {
            var box = this.getChildControl('filter-box');
            var vis = box.getVisibility();
            if (vis == 'visible') {
              box.setVisibility('excluded');
              showFilterQ.setIcon('atms/eye.png');
            } else {
              box.setVisibility('visible');
              showFilterQ.setIcon('atms/eye-blocked.png');
            }
          }, this);

          var delButton = new qx.ui.form.Button(null, 'atms/trash-black.png');
          delButton.addListener('execute', function() {
            this.__delQueryItems();
          }, this);

          comp.add(control, { flex: 1 });
          if (!this.__mini) comp.add(showFilterQ);
          comp.add(delButton);
          this._add(comp);
        break;

        case 'filter-box':
          control = new qx.ui.core.Widget();
          control._setLayout(new qx.ui.layout.Flow());
          if (this.__mini) {
            control.setMinWidth(150);
            control.setMaxWidth(150);
            this.__comp._add(control);
          } else {
            control.setVisibility('excluded');
            control.setMinHeight(50);
            this._add(control);
          }

        break;

        case 'filter-status':
          control = this.__filterFactory(id);
          this._add(control);
        break;

        case 'filter-user':
          control = this.__filterFactory(id);
          this._add(control);
        break;

        case 'filter-test':
          control = this.__filterFactory(id);
          this._add(control);
        break;

        case 'filter-qualificator':
          control = this.__filterFactory(id);
          this._add(control);
        break;

        case 'filter-name':
          control = this.__filterFactory(id);
          this._add(control);
        break;

        case 'filter-modified_on':
          control = this.__filterFactory(id);
          this._add(control);
        break;
      }
      return control || this.base(arguments, id);
    },

    __filterFactory: function(id) {
      var p = this.__params[id];
      var boxLabel = '';
      switch (p.label) {
        case 'status':
          boxLabel = this.tr('Status filter');
        break;

        case 'user':
          boxLabel = this.tr('User filter');
        break;

        case 'test':
          boxLabel = this.tr('Test filter');
        break;

        case 'path':
          boxLabel = this.tr('Qualificator');
        break;

        case 'name':
          boxLabel = this.tr('Name');
        break;

        case 'modified_on':
          boxLabel = this.tr('Modified on');
        break;
      }
      if (this.__mini) {
        var control = new qx.ui.container.Composite();
      } else {
        var control = new qx.ui.groupbox.CheckGroupBox(boxLabel);
        control.addListener('changeValue', function(e) {
          var visbility = e.getData() ? 'visible' : 'excluded';
          this.getChildrenContainer().setVisibility(visbility);
        });

        control.setValue(false);
      }

      control.setLayout(new qx.ui.layout.VBox(1));

      if (p.label == 'path' || p.label == 'name') {
        var textField = new qx.ui.form.TextField();
        var addButton = new qx.ui.form.Button(null, 'atms/plus.png');
        addButton.addListener('execute', function() {
          var box = this.getChildControl('filter-box');
          var key = p.label + '-' + textField.getValue();
          if (this.__filterMap.indexOf(key) > -1) return;
          var atom = new qx.ui.form.Button('~' + textField.getValue(), 'atms/cross.png');
          atom.setAppearance('filter-item');
          atom.setUserData('type', p.label);
          atom.setUserData('id', textField.getValue());
          atom.setAppearance('filter-tag');
          atom.addState(p.label);
          var that = this;
          atom.addListener('click', function() {
            box._remove(this);
            delete this;
            var index = that.__filterMap.indexOf(key);
            that.__filterMap.splice(index, 1);
          });

          box._add(atom);
          this.__filterMap.push(key);
        }, this);

        var comp = new qx.ui.container.Composite();
        comp.setLayout(new qx.ui.layout.HBox(5));
        comp.add(textField, { flex: 1 });
        comp.add(addButton);
        control.add(comp);
      } else if (p.label == 'modified_on') {
        var lfrom = new qx.ui.basic.Label(this.tr('From'));
        var lto = new qx.ui.basic.Label(this.tr('To'));
        var from = new qx.ui.form.DateField();
        var to = new qx.ui.form.DateField();
        lfrom.setMinWidth(50);
        lto.setMinWidth(50);

        var addButton = new qx.ui.form.Button(null, 'atms/plus.png');
        addButton.addListener('execute', function() {
          var box = this.getChildControl('filter-box');
          var dfrom = from.getChildControl('textfield').getValue() || '..';
          var dto = to.getChildControl('textfield').getValue() || '..';
          var key = p.label + '-' + from.getValue() + '|' + to.getValue();
          if (this.__filterMap.indexOf(key) > -1) return;
          var atom = new qx.ui.form.Button(dfrom + ' - ' + dto, 'atms/cross.png');
          atom.setAppearance('filter-item');
          atom.setUserData('type', p.label);
          atom.setUserData('id', {
            from: from.getValue(),
            to: to.getValue()
          });
          atom.setAppearance('filter-tag');
          atom.addState(p.label);
          var that = this;
          atom.addListener('click', function() {
            box._remove(this);
            delete this;
            var index = that.__filterMap.indexOf(key);
            that.__filterMap.splice(index, 1);
          });

          box._add(atom);
          this.__filterMap.push(key);
        }, this);

        var comp1 = new qx.ui.container.Composite();
        comp1.setLayout(new qx.ui.layout.HBox(5));
        var comp2 = new qx.ui.container.Composite();
        comp2.setLayout(new qx.ui.layout.HBox(5));

        comp1.add(lfrom, { flex: 1 });
        comp1.add(from, { flex: 3 });

        comp2.add(lto, { flex: 1 });
        comp2.add(to, { flex: 3 });

        control.add(comp1);
        control.add(comp2);
        control.add(addButton);

      } else {
        var textField = new qx.ui.form.TextField();
        textField.addListener('keyinput', function() {
          this.__timer.restart();
        }, this);

        var list = new qx.ui.list.List();
        var that = this;
        list.setDelegate({
          filter: function(item) {
            var a = eval('item.get' + p.filterby + '().toLowerCase()');
            var b = textField.getValue() || '';
            if (qx.lang.String.contains(a, b)) return true;
            else return false;
          },

          bindItem: function(controller, item, id) {
            controller.bindProperty(p.filterby.toLowerCase(), 'label', null, item, id);
          }
        });
        list.setSelectionMode('multi');
        list.setHeight(100);
        p.store.bind('model.content', list, 'model', {
          converter: function(value) {
            return value || new qx.data.Array();
          }
        });

        var addButton = new qx.ui.form.Button(null, 'atms/plus.png');
        addButton.addListener('execute', function() {
          this.__addQueryItems(
              p.label.toLowerCase(),
              list.getSelection().toArray(),
              p.filterby);
        }, this);

        list.addListener('dblclick', function() {
          this.__addQueryItems(
              p.label.toLowerCase(),
              list.getSelection().toArray(),
              p.filterby);
        }, this);

        list.addListener('keydown', function(e) {
          if (e.getKeyIdentifier() == 'Enter') {
            this.__addQueryItems(
                p.label.toLowerCase(),
                list.getSelection().toArray(),
                p.filterby);
          }
        }, this);

        var comp = new qx.ui.container.Composite();
        comp.setLayout(new qx.ui.layout.HBox(5));
        comp.add(textField, { flex: 1 });
        comp.add(addButton);

        control.add(comp);
        control.add(list, { flex: 1 });

        this.__timer.addListener('interval', function() {
          list.refresh();
        }, this);
      }

      return control;
    },

    __addQueryItems: function(type, items, labelPath) {
      var box = this.getChildControl('filter-box');
      for (var i in items) {
        var item = items[i];
        var key = type + '-' + item.getId();
        if (this.__filterMap.indexOf(key) > -1) continue;
        var atom = new qx.ui.form.Button(eval('item.geat' + labelPath + '()'), 'atms/cross.png');
        atom.setAppearance('filter-item');
        atom.setUserData('type', type);
        atom.setUserData('id', item.getId());
        atom.setAppearance('filter-tag');
        atom.addState(type);
        var that = this;
        atom.addListener('click', function() {
          box._remove(this);
          delete this;
          var index = that.__filterMap.indexOf(key);
          that.__filterMap.splice(index, 1);
        });

        box._add(atom);
        this.__filterMap.push(key);
      }
    },

    __delQueryItems: function() {
      var box = this.getChildControl('filter-box');

      var instance_list = [];
      var items = box._getChildren();
      for (var i in items) {
        instance_list.push(items[i]);
        var type = items[i].getUserData('type');
        var idx = items[i].getUserData('id');
        var key = type + '-' + idx;
        var index = this.__filterMap.indexOf(key);
        this.__filterMap.splice(index, 1);
      }

      for (var i in instance_list) {
        box._remove(instance_list[i]);
        delete instance_list[i];
      }
    },

    __buildQuery: function() {
      var qy = { _all: 1 };
      for (var i in this.__filterMap) {
        var splited = this.__filterMap[i].split('-');
        var key = splited[0];
        var value = splited[1];
        if (!(key in qy)) {
          qy[key] = [];
        }

        qy[key].push(value);
      }

      var model = qx.data.marshal.Json.createModel(qy);
      this.__queryItems = qy;
      if (this.__callback)
        this.__callback({
          query: qx.util.Serializer.toUriParameter(model)
        });
      else alert('FILTER has no callback');
    }
  }
});
