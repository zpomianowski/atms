/**
*@asset(atms/*)
*@asset(qx/icon/${qx.icontheme}/*)
*/

qx.Class.define('atms.Tree',
{
  include: atms.MParent,

  extend: qx.ui.container.Composite,

  events: {
    atms_delete: 'qx.event.type.Data'
  },

  construct: function(desktop, mode) {
    this.__mode = mode || 'tcases';
    this.__url_n   = '/atms/manager/call/json/get_nodes';
    this.__url_api = '/atms/manager/api';
    this.__url_move = '/atms/manager/move_nodes';
    this.__url_copy2Ttest = '/atms/manager/copy2Ttest';
    this.__url_getPath = '/atms/manager/get_node_path.json';
    this.__query = '';

    this.__clipboard = { hint: null, content: null };

    this.base(arguments);
    this.__desktop = desktop;

    var layout = new qx.ui.layout.VBox();
    this.setLayout(layout);
    this.setPadding(0);
    this.setDecorator(null);

    var that = this;

    var delegateP = {
      getModelMixins: function(properties) {
        return atms.MParent;
      },

      configureRequest: function(req) {
        req.setCache('no-store');
        req.setRequestData(that.__query);
      }
    };

    // STORE
    var store = new qx.data.store.Json(null, delegateP);
    this.__store = store;

    // TREE - BASIC CUSTOMIZATION
    var tree = new qx.ui.tree.VirtualTree(null, 'label', 'children');
    this.__tree = tree;

    store.addListener('loaded', function() {
      tree.refresh();
    });

    store.bind('model', tree, 'model');
    tree.setHideRoot(true);
    tree.setShowTopLevelOpenCloseIcons(true);

    tree.setIconPath('icon');
    tree.setIconOptions({
      converter: function(value, model) {
        if (value == 'tcase') {
          return model.getAutomated() ? 'atms/cog.png' : 'atms/tsc.png';
        }

        if (value == 'loading') {
          return 'atms/loader.gif';
        }

        if (value == 'warn') {
          return 'atms/exclamation.png';
        }
      }
    });

    tree.setSelectionMode('multi');

    // TREE - DRAG&DROP
    if (qx.core.Init.getApplication().gPermissions.nodes.update & qx.core.Init.getApplication().gPermissions.tcases.update) {
      tree.setDraggable(true);
      tree.setDroppable(true);
      tree.addListener('dragstart', this._handleDragStart, this);
      tree.addListener('droprequest', this._handleDropRequest, this);
      tree.addListener('drop', this._handleDrop, this);
      tree.addListener('drag', this.__onDrag, this);
      tree.addListener('dragend', this.__onDragEnd, this);
      tree.addListener('dragover', function(e) {
        // Stop when the dragging comes from outside
        if (e.getRelatedTarget()) {
          if (!(e.getRelatedTarget() instanceof qx.ui.tree.VirtualTree))
              e.preventDefault();
        } else e.preventDefault();
      });
    }

    this.__dragFeedback = new atms.DragFeedback(this.tr('Assign users to test/case'));
    qx.core.Init.getApplication().getRoot().add(this.__dragFeedback);

    // TREE - CONTROL
    var that = this;

    var delegate = {
      bindItem: function(controller, item, index) {
        controller.bindDefaultProperties(item, index);
        controller.bindProperty('', 'open',
                {
                  converter: function(value, model, source, target) {
                    var isOpen = target.isOpen();
                    if (value.getIcon() != 'folder') return isOpen;
                    if (isOpen && !value.getLoaded()) {
                      //qx.event.Timer.once(function()
                      //{
                      var req = new qx.io.request.Xhr(that.__url_n + '/' + value.getId());
                      req.setTimeout(2400000);
                      req.setRequestData(that.__query);
                      req.addListener('success', function(e) {
                        value.getChildren().removeAll();
                        var req = e.getTarget();
                        req.setAsync(false);
                        var nodes = req.getResponse();
                        var marshal = new qx.data.marshal.Json(delegateP);
                        marshal.toClass(nodes, true);
                        model = marshal.toModel(nodes);
                        model.getChildren().forEach(function(item) {
                          item.setParentNode(value);
                          value.getChildren().push(item);
                        });

                        if (model.getChildren().length > 0) value.setLoaded(true);
                        else {
                          var item = { label: that.tr('This folder is empty'), icon: 'warn', loaded: false, id: -1 };
                          var model = qx.data.marshal.Json.createModel(item);
                          value.getChildren().push(model);
                          value.setLoaded(true);
                        }
                      }, value);

                      req.send();

                      //}, that, 2000);
                    }

                    if (!isOpen && value.getLoaded()) {
                      value.getChildren().removeAll();
                      var item = { label: 'Loading', icon: 'loading', loaded: false, id: -1 };
                      var model = qx.data.marshal.Json.createModel(item);
                      value.getChildren().push(model);
                      value.setLoaded(false);
                    }

                    return isOpen;
                  }
                }, item, index);
      },

      sorter: function(item_a, item_b) {
        var a = item_a.getLabel().toLowerCase();
        var b = item_b.getLabel().toLowerCase();
        var A = item_a.getIcon();
        var B = item_b.getIcon();
        return A > B ? 1 : A < B ? -1 : a > b ? 1 : a < b ? -1 : 0;
      },

      configureItem: function(widget) {
        //widget.setDraggable(true);
        widget.setDroppable(true);
      }
    };
    tree.setDelegate(delegate);

    // TREE - CONTEXT MENU
    //this.__createCommands();
    tree.setContextMenu(this.__getMenu());
    tree.addListener('beforeContextmenuOpen', function(e) {
      var perm = qx.core.Init.getApplication().gGIP_nodes(this.__tree.getSelection());
      this.__resetMenu();

      if (this.__mode != 'ttests') {
        this.__menu['uploadParse'].setEnabled(
            qx.core.Init.getApplication().gPermissions.tcases.create &&
            qx.core.Init.getApplication().gPermissions.nodes.create);
        this.__menu['newFolderButton'].setEnabled(qx.core.Init.getApplication().gPermissions.nodes.create);
        this.__menu['newCaseButton'].setEnabled(qx.core.Init.getApplication().gPermissions.tcases.create);
        if (perm) {
          this.__menu['cpButton'].setEnabled(perm.create);
          this.__menu['cutButton'].setEnabled(perm.update);
          if (this.__clipboard['content'] != null)
              this.__menu['pasteButton'].setEnabled(perm.create);
          this.__menu['delButton'].setEnabled(perm.del);
        }

        this.__menu['pasteButton'].setEnabled(false);
        this.__menu['editButton'].setEnabled(false);
      }

      this.__menu['delButton'].setEnabled(true);
      if (perm) {
        this.__menu['editButton'].setEnabled(perm.update || perm.read);
        this.__menu['pathButton'].setEnabled(perm.read);
      }
    }, this);

    tree.addListener('dblclick', qx.lang.Function.bind(this.__editItem, this));

    this.__dragFeedback = new atms.DragFeedback(this.tr('Create cases from templates for user/test'));
    qx.core.Init.getApplication().getRoot().add(this.__dragFeedback);

    // filter toolbar
    var filterPanel = new atms.Filter({
      'filter-name':
          { label: 'name', filterby: 'Name' }
    }, function(data) {
      that.__query = data.query;
      that.__store.reload();
    }, true);

    var scroll = new qx.ui.container.Composite(new qx.ui.layout.Grow());
    scroll.add(filterPanel);
    scroll.setVisibility('excluded');
    this.__filterPanel = scroll;

    this.add(scroll);

    // ttests manager
    if (this.__mode == 'ttests') {
      var ttests_c = new qx.ui.container.Composite(new qx.ui.layout.HBox());

      this.ttest = new atms.DynVirtualSelectBox('Name', true);
      this.ttest.getSelection().addListener('change', function(e) {
        var ttest_id = this.ttest.getSelection().getItem(0).getId();
        this.__store.setUrl(this.__url_n + '?relatedTest=' + ttest_id);
      }, this);

      var delete_ttestB = new qx.ui.form.Button(null, 'atms/trash-black.png');
      delete_ttestB.addListener('click', function() {
        this.__delTtest(this.ttest.getSelection().toArray()[0].getId());
      }, this);

      var edit_ttestB = new qx.ui.form.Button(null, 'atms/edit-black.png');
      edit_ttestB.addListener('click', function() {
        this.__editTtest(this.ttest.getSelection().toArray()[0].getId());
      }, this);

      var add_ttestB = new qx.ui.form.Button(null, 'atms/plus.png');
      add_ttestB.addListener('click', function() {
        this.__newTtest(null);
      }, this);

      add_ttestB.setEnabled(qx.core.Init.getApplication().gPermissions.ttests.create);
      edit_ttestB.setEnabled(qx.core.Init.getApplication().gPermissions.ttests.update);
      delete_ttestB.setEnabled(qx.core.Init.getApplication().gPermissions.ttests.del);

      ttests_c.add(this.ttest, { flex: 1 });
      ttests_c.add(edit_ttestB);
      ttests_c.add(add_ttestB);
      ttests_c.add(delete_ttestB);
      this.add(ttests_c);

      this.__setupResources();
      this.__setupStores();
      this.__setupBindings();
      this.__ttestsR.get();
    } else {
      this.__store.setUrl(this.__url_n);
    }

    this.add(tree, { flex: 1 });

    this.__R = new qx.io.rest.Resource({
      dumpTcases: {
        method: 'POST',
        url: '/atms/manager/task_api/dumpTcases'
      }
    });
    this.__R.addListener('dumpTcasesSuccess', function() {
      jQuery('.flash').html(this.tr('New task "dump tcases to *.csv" added').toString());
      jQuery('.flash').show();
    }, this);
  },

  members: {
    __mode: null,

    __store: null,
    __tree: null,
    __desktop: null,

    __cmds: [],
    __menu: [],

    __url_n:   null,
    __url_api: null,
    __url_move: null,

    __clipboard: null,

    __dragFeedback: null,
    __query: null,

    ttest: null,
    __ttestsR: null,
    __ttestsS: null,

    __R: null,

    __setupResources: function() {
      this.__ttestsR = new qx.io.rest.Resource({
        get:   { method: 'GET',    url: '/atms/manager/api/atms_ttests.json' }
      });
    },

    __setupStores: function() {
      var that = this;
      this.__ttestsS = new qx.data.store.Rest(this.__ttestsR, 'get');
    },

    __setupBindings: function() {
      this.__ttestsS.bind('model.content', this.ttest, 'model');
      this.ttest.setLabelPath('name');
    },

    toggleFilter: function() {
      var control = this.__filterPanel;
      var vis = control.getVisibility();
      if (vis == 'visible')
          control.setVisibility('excluded');
      else control.setVisibility('visible');
    },

    __resetMenu: function() {
      this.__menu['newFolderButton'].setEnabled(false);
      this.__menu['newCaseButton'].setEnabled(false);
      this.__menu['delButton'].setEnabled(false);
      this.__menu['cutButton'].setEnabled(false);
      this.__menu['cpButton'].setEnabled(false);
      this.__menu['pasteButton'].setEnabled(false);
      this.__menu['editButton'].setEnabled(false);
      this.__menu['pathButton'].setEnabled(false);
      this.__menu['uploadParse'].setEnabled(false);
    },

    __createCommands: function() {
      this.__cmds['newFolderCmd'] = new qx.ui.command.Command('Shift+F');
      this.__cmds['newFolderCmd'].addListener('execute', this.__newFolder, this);

      this.__cmds['newCaseCmd'] = new qx.ui.command.Command('Shift+N');
      this.__cmds['newCaseCmd'].addListener('execute', this.__newCase, this);

      this.__cmds['deleteCmd'] = new qx.ui.command.Command('Delete');
      this.__cmds['deleteCmd'].addListener('execute', this.__deleteItem, this);

      this.__cmds['cutCmd'] = new qx.ui.command.Command('Ctrl+X');
      this.__cmds['cutCmd'].addListener('execute', this.__cutItem, this);

      this.__cmds['copyCmd'] = new qx.ui.command.Command('Ctrl+C');
      this.__cmds['copyCmd'].addListener('execute', this.__copyItem, this);

      this.__cmds['pasteCmd'] = new qx.ui.command.Command('Ctrl+V');
      this.__cmds['pasteCmd'].addListener('execute', this.__pasteItem, this);

      this.__cmds['editCmd'] = new qx.ui.command.Command('Alt+E');
      this.__cmds['editCmd'].addListener('execute', this.__editItem, this);
    },

    __getMenu: function() {
      var menu = new qx.ui.menu.Menu;

      var newFolderButton   = new qx.ui.menu.Button(this.tr('New folder'),
          'atms/folder-close.png', this.__cmds['newFolderCmd']);
      var newCaseButton   = new qx.ui.menu.Button(this.tr('New case'),
          'atms/tsc.png', this.__cmds['newCaseCmd']);
      var cpButton    = new qx.ui.menu.Button(this.tr('Copy'),
          'atms/copy.png', this.__cmds['copyCmd']);
      var cutButton   = new qx.ui.menu.Button(this.tr('Cut'),
          'atms/cut.png', this.__cmds['cutCmd']);
      var pasteButton = new qx.ui.menu.Button(this.tr('Paste'),
          'atms/paste.png', this.__cmds['pasteCmd']);
      var delLabel = this.tr('Delete');
      if (this.__mode == 'ttests') delLabel = this.tr('Remove from template');
      var delButton   = new qx.ui.menu.Button(delLabel,
          'atms/trash.png', this.__cmds['deleteCmd']);
      var editButton   = new qx.ui.menu.Button(this.tr('Edit/View'),
          'atms/edit.png', this.__cmds['editCmd']);
      var pathButton   = new qx.ui.menu.Button(this.tr('Get qualificator'),
          null, this.__cmds['getPath']);
      var uploadParse   = new qx.ui.menu.Button(this.tr('Upload & parse') + ' [*.csv]',
          'atms/cloud-upload.png', null);
      var exportCases = new qx.ui.menu.Button(this.tr('Export all cases') + ' [*.csv]',
          'atms/cog.png', null);
      var viewExported = new qx.ui.menu.Button(this.tr('View exported files'),
          'atms/cloud-download.png', null);

      newFolderButton.addListener('click', this.__newFolder, this);
      newCaseButton.addListener('click', this.__newCase, this);
      cpButton.addListener('click', this.__copyItem, this);
      cutButton.addListener('click', this.__cutItem, this);
      pasteButton.addListener('click', this.__pasteItem, this);
      delButton.addListener('click', this.__deleteItem, this);
      editButton.addListener('click', this.__editItem, this);
      pathButton.addListener('click', this.__pathItem, this);
      uploadParse.addListener('click', this.__uploadParse, this);
      exportCases.addListener('click', this.__dumpTcases, this);
      viewExported.addListener('click', this.__viewExported, this);

      this.__menu['newFolderButton'] = newFolderButton;
      this.__menu['newCaseButton']   = newCaseButton;
      this.__menu['cpButton']        = cpButton;
      this.__menu['cutButton']       = cutButton;
      this.__menu['pasteButton']     = pasteButton;
      this.__menu['delButton']       = delButton;
      this.__menu['editButton']      = editButton;
      this.__menu['pathButton']      = pathButton;
      this.__menu['uploadParse']     = uploadParse;
      this.__menu['exportCases']     = exportCases;

      menu.add(newFolderButton);
      menu.add(newCaseButton);
      menu.add(new qx.ui.menu.Separator());
      menu.add(editButton);
      menu.add(new qx.ui.menu.Separator());
      menu.add(cpButton);
      menu.add(cutButton);
      menu.add(pasteButton);
      menu.add(new qx.ui.menu.Separator());
      menu.add(pathButton);
      menu.add(new qx.ui.menu.Separator());
      menu.add(delButton);
      menu.add(new qx.ui.menu.Separator());
      menu.add(uploadParse);
      menu.add(new qx.ui.menu.Separator());
      menu.add(exportCases);
      menu.add(viewExported);

      return menu;
    },

    __newFolder: function(e) {
      var that = this;
      var item = this.__tree.getSelection().getItem(0);
      var type = item == null ? null : item.getIcon();

      var holder = null;
      var parent = null;

      if (type == 'folder') holder = item.getId();
      if (item != null)
                if (item.getParentNode() != null) {
                  if (type == 'tcase') holder = item.getParentNode().getId();
                  if (type != null)    parent = item.getParentNode();
                }

      // var newFolder = new atms.WinFolder(holder, function() {
      //     if (parent) parent.setLoaded(false);
      //     else that.__store.reload();
      //     that.__tree.refresh();
      // });
      // this.__desktop.add(newFolder);
      // newFolder.open();
      var widget = new atms.Form('atms_nodes', { node: holder }, 'create',
                function() {
                  if (parent) parent.setLoaded(false);
                  else that.__store.reload();
                  that.__tree.refresh();
                });

      var win = new atms.WinPanel(widget);
      win.setWidth(360);
      win.setHeight(360);
      qx.core.Init.getApplication().gDesk1.add(win);
      win.open();
    },

    __newCase: function(e) {
      var that = this;
      var item = this.__tree.getSelection().getItem(0);
      var type = item == null ? null : item.getIcon();

      var holder = null;
      var parent = null;

      if (type == 'folder') holder = item.getId();
      if (item != null)
                if (item.getParentNode() != null) {
                  if (type == 'tcase') holder = item.getParentNode().getId();
                  if (type != null)    parent = item.getParentNode();
                }

      // var newTCase = new atms.WinTCase(holder, function() {
      //     if (parent) parent.setLoaded(false);
      //     else that.__store.reload();
      //     that.__tree.refresh();
      // });
      var widget = new atms.Form('atms_tcases', { node: holder }, 'create',
                function() {
                  if (parent) parent.setLoaded(false);
                  else that.__store.reload();
                  that.__tree.refresh();
                });

      var win = new atms.WinPanel(widget);
      qx.core.Init.getApplication().gDesk1.add(win);
      win.open();
    },

    __newTtest: function() {
      var that = this;

      // var newTTest = new atms.WinTTest(function() {
      //     that.__ttestsR.get();
      // }, null);
      // this.__desktop.add(newTTest);
      // newTTest.open();
      var widget = new atms.Form('atms_ttests', {}, 'create',
          function() { that.__ttestsR.get(); });

      var win = new atms.WinPanel(widget);
      win.setWidth(14); win.setHeight(240);
      qx.core.Init.getApplication().gDesk1.add(win);
      win.open();
    },

    __editTtest: function(id) {
      var that = this;

      // var data = new qx.data.store.Json(that.__url_api + "/atms_ttests/id/" + id + ".json");
      // data.addListener("loaded", function() {
      //     var newTTest = new atms.WinTTest(function() {
      //         that.__ttestsR.get();
      //     }, data.getModel().getContent().getItem(0));
      //     that.__desktop.add(newTTest);
      //     newTTest.open();
      // });
      var widget = new atms.Form('atms_ttests', { id: id }, 'create',
          function() { that.__ttestsR.get(); });

      var win = new atms.WinPanel(widget);
      win.setWidth(14); win.setHeight(240);
      qx.core.Init.getApplication().gDesk1.add(win);
      win.open();
    },

    __delTtest: function(id) {
      var that = this;
      var s = that.__tree.getSelection();
      var caption = s.length > 1 ? this.tr('Delete these items?') : this.tr('Delete this item?');
      var desc = this.tr('This action cannot be undone!');
      var dialog = new atms.WinConfirmation(
          caption,
                desc, function() {
                  var req = new qx.io.request.Xhr(that.__url_api + '/atms_ttests', 'DELETE');
                  req.setTimeout(2400000);
                  req.addListener('success', function(e) {
                    that.__ttestsR.get();
                    jQuery('.flash').html(that.tr('Items deleted').toString());
                    jQuery('.flash').show();
                  }, this);

                  req.setRequestData({ id: id });
                  req.send();
                }, null);

      this.getApplicationRoot().add(dialog);
      dialog.open();
    },

    __deleteItem: function(e) {
      var that = this;
      var s = that.__tree.getSelection();
      var caption = s.length > 1 ? this.tr('Delete these items?') : this.tr('Delete this item?');
      var desc = this.tr('This action cannot be undone!');
      var dialog = new atms.WinConfirmation(
          caption,
                desc, function() {
                  s.forEach(function(item) {
                    var req = null;
                    req = new qx.io.request.Xhr(that.__url_api + '/atms_nodes', 'DELETE');
                    var ttest = null;
                    if (that.ttest)
                        ttest = that.ttest.getSelection().getItem(0).getId();
                    req.setRequestData({ id: item.getId(),
                                        type: item.getIcon(),
                                        mode: that.__mode,
                                        ttest: ttest });
                    req.setTimeout(2400000);
                    req.addListener('success', function(e) {
                      var parent = item.getParentNode();
                      if (parent) parent.setLoaded(false);
                      else that.__store.reload();
                      that.__tree.refresh();
                      jQuery('.flash').html(that.tr('Items deleted').toString());
                      jQuery('.flash').show();
                    }, this);

                    req.send();
                  });
                }, null);

      this.getApplicationRoot().add(dialog);
      dialog.open();
    },

    __cutItem: function(e) {
      this.__clipboard['content'] = this.__tree.getSelection().copy();
      this.__clipboard['hint'] = 'cut';
    },

    __copyItem: function(e) {
      this.__clipboard['content'] = this.__tree.getSelection().copy();
      this.__clipboard['hint'] = 'copy';
    },

    __pathItem: function(e) {
      var node = this.__tree.getSelection().getItem(0).getId();
      var req = new qx.io.request.Xhr(this.__url_getPath + '/' + node, 'DELETE');
      req.setTimeout(2000);
      req.addListener('success', function(e) {
        var path = e.getTarget().getResponse().content;
        window.prompt(this.tr('Copy to clipboard: Ctrl+C, Enter').toString(), path);
      }, this);

      req.send();
    },

    __uploadParse: function(e) {
      var widget = new atms.Uploader('atms_attachments', {
        task: 'addTcases'
      }, false);
      var win  = new atms.WinPanel(widget);
      qx.core.Init.getApplication().gDesk1.add(win);
      win.open();
      win.setCaption(this.tr('Add files to parse'));
    },

    __dumpTcases: function() {
      this.__R.dumpTcases();
    },

    __viewExported: function() {
      var widget = new atms.Uploader('atms_attachments', {
        task: 'dumpTcases'
      }, false);
      var win  = new atms.WinPanel(widget);
      qx.core.Init.getApplication().gDesk1.add(win);
      win.open();
      win.setCaption(this.tr('Wait for generated files...'));
    },

    __pasteItem: function(e) {
      var target = this.__tree.getSelection().getItem(0);
      var invalidNodes = new qx.data.Array();
      if (target.getIcon() == 'folder') {
        moveTo = target.getId();
        invalidNodes.push(target);
      }

      if (target.getIcon() == 'tcase') {
        moveTo = target.getParentNode().getId();
        invalidNodes.push(target.getParentNode());
      }

      var items = { nodes: [], tcases: [], copy: false, moveTo: moveTo };
      this.__clipboard['content'].forEach(function(item) {
        if (item.getParentNode() === null) invalidNodes.push(-1);
        else if (typeof item.getParentNode().getId == 'function' &&
            !invalidNodes.contains(item.getParentNode().getId())) {
          invalidNodes.push(item.getParentNode());
        }

        if (item.getIcon() == 'folder') items['nodes'].push(item.getId());
        if (item.getIcon() == 'tcase') items['tcases'].push(item.getId());
      });

      this.__moveNodes(items, invalidNodes, this.__clipboard['hint']);
    },

    __editItem: function(e) {
      var that = this;
      var item = this.__tree.getSelection().getItem(0);
      var type = item.getIcon();

      if (type == 'tcase') {
        var id  = item.getId();
        var pid = item.getParentNode() != null ? item.getParentNode().getId() : null;
        var data = new qx.data.store.Json(that.__url_api + '/atms_tcases/id/' + id + '.json');
        data.addListener('loaded', function() {
          // var newTCase = new atms.WinTCase(pid, function() {
          //     if (item.getParentNode())
          //         item.getParentNode().setLoaded(false);
          //     else item.setLoaded(false);
          //     that.__tree.refresh();
          // }, data.getModel().getContent().getItem(0));
          // that.__desktop.add(newTCase);
          // newTCase.open();
          var widget = new atms.Form('atms_tcases', { node: pid, id: id }, 'update',
                        function() {
                          if (item.getParentNode())
                              item.getParentNode().setLoaded(false);
                          else item.setLoaded(false);
                          that.__tree.refresh();
                        });

          var win = new atms.WinPanel(widget);
          qx.core.Init.getApplication().gDesk1.add(win);
        });
      }

      if (type == 'folder') {
        var id  = item.getId();
        var pid = item.getParentNode() != null ? item.getParentNode().getId() : null;
        var data = new qx.data.store.Json(that.__url_api + '/atms_nodes/id/' + id + '.json');
        data.addListener('loaded', function() {
          // var newFolder = new atms.WinFolder(pid, function() {
          //     if (item.getParentNode())
          //         item.getParentNode().setLoaded(false);
          //     else item.setLoaded(false);
          //     that.__tree.refresh();
          // }, data.getModel().getContent().getItem(0));
          // that.__desktop.add(newFolder);
          // newFolder.open();
          var widget = new atms.Form('atms_nodes', { id: id, node: pid }, 'update',
                        function() {
                          if (item.getParentNode())
                              item.getParentNode().setLoaded(false);
                          else item.setLoaded(false);
                          that.__tree.refresh();
                        });

          var win = new atms.WinPanel(widget);
          win.setWidth(360);
          win.setHeight(360);
          qx.core.Init.getApplication().gDesk1.add(win);
          win.open();
        });
      }
    },

    __copy2Ttest: function(ttest_id, items) {
      var that = this;
      var args = '/' + ttest_id;
      var req = new qx.io.request.Xhr(this.__url_copy2Ttest + args, 'POST');
      req.setTimeout(2400000);
      req.setRequestHeader('Accept', 'application/json');
      req.setRequestHeader('content-type', 'application/json');
      req.addListener('success', function(e) {
        that.__clipboard['hint']    = null;
        that.__clipboard['content'] = null;
        jQuery('.flash').html(that.tr('Cases added to the test template').toString());
        jQuery('.flash').show();
      }, this);

      req.setRequestData(qx.util.Serializer.toJson(items));
      req.setAsync(false);
      req.send();
    },

    __moveNodes: function(items, items2refresh, hint) {
      var that = this;
      var s = that.__tree.getSelection();
      var caption = s.length > 1 ? this.tr('Move these items?') : this.tr('Move this item?');
      var desc = this.tr('This action will relocate the items among the folders.');
      var dialog = new atms.WinConfirmation(
          caption,
                desc, function() {
                  var args = '/None';
                  if (hint) args = '/' + hint;
                  var req = new qx.io.request.Xhr(that.__url_move + args, 'POST');
                  req.setTimeout(2400000);
                  req.setRequestHeader('Accept', 'application/json');
                  req.setRequestHeader('content-type', 'application/json');
                  req.addListener('success', function(e) {
                    that.__clipboard['hint']    = null;
                    that.__clipboard['content'] = null;
                    jQuery('.flash').html(that.tr('Nodes moved').toString());
                    jQuery('.flash').show();
                  }, this);

                  req.setRequestData(qx.util.Serializer.toJson(items));
                  req.setAsync(false);
                  req.send();
                }, null);

      this.getApplicationRoot().add(dialog);
      dialog.open();
    },

    __refreshNodes: function(items2refresh) {
      var that = this;
      items2refresh.forEach(function(item) {
        var force = item === -1 ? true : item.getId() == -1 ? true : false;
        if (force) {
          that.__store.reload();
          that.__tree.refresh();
          return;
        }

        item.setLoaded(false);
        that.__tree.refresh();
        that.__tree.openNodeAndParents(item);
      });
    },

    _handleDragStart: function(e) {
      e.addType('sender');
      e.addType('target');
      e.addType('tree-items');
      e.addType('tree-items-uri');
      e.addAction('move');

      var that = this;
      this.__tree.getSelection().forEach(function(item) {
        var itemWidget = new qx.ui.basic.Atom(
            item.getLabel(),
            item.getIcon() == 'tcase' ? 'atms/tsc.png' : 'atms/folder-close.png');
        itemWidget.setBackgroundColor('background-selected');
        itemWidget.setTextColor('white');
        itemWidget.setPadding([0, 4, 0, 24]);
        that.__dragFeedback.addDragChild(itemWidget);
      });
    },

    __onDrag: function(e) {
      this.__dragFeedback.setDomPosition(e.getDocumentLeft() + 15, e.getDocumentTop() + 15);
    },

    __onDragEnd: function(e) {
      this.__dragFeedback.setDomPosition(-1000, -1000);
      this.__dragFeedback.purgeChildren();
    },

    _handleDropRequest: function(e) {
      e.addData('sender', this);
      e.addData('target', e.getRelatedTarget());
      e.addData('tree-items', this.__tree.getSelection());

      var o = new Object();
      o.nodes = new qx.data.Array();
      o.cases = new qx.data.Array();
      this.__tree.getSelection().forEach(function(item) {
        if (item.getIcon() == 'folder') o.nodes.push(item.getId());
        if (item.getIcon() == 'tcase')  o.cases.push(item.getId());
      });

      var model = qx.data.marshal.Json.createModel(o);
      var uri = qx.util.Serializer.toUriParameter(model);
      e.addData('tree-items-uri', uri);
    },

    _handleDrop: function(e) {
      var sender = e.getData('sender');
      if (sender instanceof atms.Tree) {
        var target = e.getData('target').getModel();
        var invalidNodes = new qx.data.Array();
        if (target.getIcon() == 'folder') {
          moveTo = target.getId();
          invalidNodes.push(target);
        }

        if (target.getIcon() == 'tcase') {
          try {
            moveTo = target.getParentNode().getId();
            invalidNodes.push(target.getParentNode());
          } catch (exception) {}
        }

        var items = { nodes: [], tcases: [], copy: false, moveTo: moveTo };
        e.getData('tree-items').forEach(function(item) {
          if (item.getParentNode() === null) invalidNodes.push(-1);
          else if (typeof item.getParentNode().getId == 'function' &&
              !invalidNodes.contains(item.getParentNode().getId())) {
            invalidNodes.push(item.getParentNode());
          }

          if (item.getIcon() == 'folder') items['nodes'].push(item.getId());
          if (item.getIcon() == 'tcase') items['tcases'].push(item.getId());
        }, this);

        if (this.__mode == 'tcases') {
          this.__moveNodes(items, invalidNodes);
          this.__refreshNodes(invalidNodes);
        }

        if (this.__mode == 'ttests') {
          this.__copy2Ttest(
              this.ttest.getSelection().getItem(0).getId(),
              items);
          this.__store.reload();
        }
      }
    }
  }
});
