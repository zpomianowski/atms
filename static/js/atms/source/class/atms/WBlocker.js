/**
*@asset(atms/*)
*@asset(qx/icon/${qx.icontheme}/*)
*/

qx.Class.define('atms.WBlocker',
{
  extend: qx.ui.core.Blocker,

  construct: function(widget) {
    arguments.callee.base.apply(this, arguments, widget);
    this.getBlockerElement().setStyle('cursor', 'wait');
  },

  members: {
    block: function() {
      arguments.callee.base.apply(this, arguments);
    },

    unblock: function() {
      arguments.callee.base.apply(this, arguments);
    }
  }
});
