/**
*@asset(atms/*)
*@asset(qx/icon/${qx.icontheme}/*)
*/

qx.Class.define('atms.QuizManager',
{
  extend: qx.ui.container.Composite,

  construct: function() {
    this.base(arguments);
    this.setLayout(new qx.ui.layout.HBox(5));

    this.__model = new atms.table.RemoteTableQuiz();
    var custom = {
      tableColumnModel: function(obj) {
        return new qx.ui.table.columnmodel.Resize(obj);
      }
    };
    this.__table = new qx.ui.table.Table(this.__model, custom);
    this.__table.getSelectionModel()
        .setSelectionMode(
            qx.ui.table.selection.Model.MULTIPLE_INTERVAL_SELECTION);
    this.__table.setMinWidth(200);

    var that = this;
    var form = new atms.QuizQ('update', function() {
            that.__model.reloadData(); });

    var scroll = new qx.ui.container.Scroll();
    scroll.setMinWidth(500);
    scroll.add(form);

    this.add(this.__table, { flex: 1 });
    this.add(scroll, { flex: 5 });

    this.__table.setContextMenu(this.__getMenu());
    this.__table.addListener('click', function(e) {
      this.__table.getSelectionModel().iterateSelection(
                function(index) {
                  form.load(this.__model.getRowData(index).id);
                }, this);
    }, this);

    this.__setupResources();
  },

  members: {
    __model: null,
    __table: null,

    __setupResources: function() {
      var base_url = '/atms/manager/../quiz_manager/';
      this.__R = new qx.io.rest.Resource({
        del: {
            method: 'DELETE',
            url: base_url + 'api/{ids}' }
      });
      this.__R.addListener('delSuccess', function() {
        this.__model.reloadData();
      }, this);
    },

    __getMenu: function() {
      var menu = new qx.ui.menu.Menu;
      var delButton   = new qx.ui.menu.Button(this.tr('Delete'),
          'atms/trash.png', null);
      delButton.addListener('click', this.__delete, this);
      menu.add(delButton);
      return menu;
    },

    __delete: function() {
      var that = this;
      var selectedRowData = [];
      this.__table.getSelectionModel().iterateSelection(function(index) {
        selectedRowData.push(this.__model.getRowData(index).id);
      }, this);

      var caption = selectedRowData.length > 1 ? this.tr('Delete these questions?') : this.tr('Delete this item?');
      var desc = this.tr('This action cannot be undone!');
      var dialog = new atms.WinConfirmation(
          caption,
          desc,
                function() {
                  that.__R.del({
                    ids: selectedRowData
                  });
                }
            );
      this.getApplicationRoot().add(dialog);
      dialog.open();
    }
  }
});
