var qxversion = qx.core.Environment.get('qx.version');
if ({ '4.1': true }[qxversion]) {
  qx.bom.Event.$$stopPropagation_old = qx.bom.Event.stopPropagation;
  qx.bom.Event.stopPropagation = function(e) {
    if (typeof e.target.className !== 'object') {
      qx.bom.Event.$$stopPropagation_old(e);
    }
  };
}

qx.Class.define('atms.HLSPlayer',
{
  extend: qx.ui.core.Widget,

  construct: function(url) {
    this.base(arguments);
    this.__url = url;

    // this.__addCss('<css_file>');
    var codeArr = [
        '/atms/static/js/bower_components/hls.js/dist/hls.min.js'
    ];
    this.__loadScriptArr(codeArr,
                         qx.lang.Function.bind(this.__loadElement, this));
    // this.__setup();
  },

  statics: {
    INSTANCE_COUNTER: 0,
    LOADED: {},
    LOADING: {}
  },

  events: {
    scriptLoaded: 'qx.event.type.Event'
  },

  members: {
    __url: null,

    __loadScriptArr: function(codeArr, handler) {
      var that = this;
      var script = codeArr.shift();
      if (script) {
        if (atms.HLSPlayer.LOADING[script]) {
          atms.HLSPlayer.LOADING[script].addListenerOnce(
              'scriptLoaded', function() {
            this.__loadScriptArr(codeArr, handler);
          }, this);
        } else if (atms.HLSPlayer.LOADED[script]) {
          this.__loadScriptArr(codeArr, handler);
        } else {
          atms.HLSPlayer.LOADING[script] = this;
          var sl = new qx.bom.request.Script();
          var src = qx.util.ResourceManager.getInstance().toUri(script);
          sl.onload = function() {
            that.debug('Dynamically loaded ' + src + ': ' + status);
            that.__loadScriptArr(codeArr, handler);
            atms.HLSPlayer.LOADED[script] = true;
            atms.HLSPlayer.LOADING[script] = null;
            that.fireDataEvent('scriptLoaded', script);

          };

          sl.open('GET', src);
          sl.send();
        }
      } else {
        handler();
      }
    },

    __addCss: function(url) {
      if (!atms.HLSPlayer.LOADED[url]) {
        atms.HLSPlayer.LOADED[url] = true;
        var head = document.getElementsByTagName('head')[0];
        var el = document.createElement('link');
        el.type = 'text/css';
        el.rel = 'stylesheet';
        el.href = qx.util.ResourceManager.getInstance().toUri(url);
        setTimeout(function() {
          head.appendChild(el);
        }, 0);
      };
    },

    __loadElement: function() {
      var el = this.getContentElement().getDomElement();
      if (el == null) {
        this.addListenerOnce(
          'appear',
          qx.lang.Function.bind(this.__loadElement, this), this);
      } else {
        var video = document.createElement('video');
        video.innerHTML = el.innerHTML;
        el.parentNode.replaceChild(video, el);
        if (Hls.isSupported()) {
          var hls = new Hls();
          console.log(hls);
          hls.loadSource(this.__url);
          hls.attachMedia(el);
          hls.on(Hls.Events.MANIFEST_PARSED, function() {
            console.log('dupa');
            video.play();
          });
        }
      }
    },

    destroy: function() {
      if (this.__ws) this.__ws.close();
      arguments.callee.base.apply(this, arguments);
    }
  }
});
