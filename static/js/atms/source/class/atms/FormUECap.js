/**
*@asset(atms/*)
*@asset(qx/icon/${qx.icontheme}/*)
*/

qx.Class.define('atms.FormUECap',
{
  extend: qx.ui.container.Composite,

  construct: function(id, mod_type, callback_success, callback_fail) {
    this.base(arguments);

    var href_els = window.location.href.split('/');
    var newurl = (href_els.slice(0, 6).join('/').replace('#', '') +
      '/uecap/' + mod_type + '/' + id);
    window.history.replaceState({}, null, newurl);

    this.__callback_s = callback_success;
    this.__callback_f = callback_fail;

    this.setLayout(new qx.ui.layout.VBox());
    this.__holder = new qx.ui.container.Composite().set({
      layout: new qx.ui.layout.HBox()
    });

    var layout = new qx.ui.layout.Grid(4, 4);
    layout.setColumnWidth(0, 150);
    layout.setColumnFlex(1, 1);
    layout.setColumnWidth(2, 150);
    layout.setColumnFlex(3, 1);

    var tb = new qx.ui.toolbar.ToolBar();
    var part = new qx.ui.toolbar.Part();
    tb.add(part);

    var butForm = new qx.ui.toolbar.CheckBox(
      this.tr('Main form'), 'atms/details.png');
    var butAtt = new qx.ui.toolbar.CheckBox(
        this.tr('Attachments'), 'atms/dl.png').set({
          value: false
        });
    this.__butAtt = butAtt;

    part.add(butForm);
    part.add(butAtt);

    this.__form = new qx.ui.container.Composite();
    this.__form.setLayout(layout);

    this.add(tb);
    this.add(this.__holder, { flex: 1 });
    this.__holder.add(this.__form, { flex: 3 });

    this.addListener('appear', function(e) {
      var p = this.getLayoutParent().getLayoutParent();
      this.__parent = p;
      if (mod_type == 'create') {
        p.setCaption(this.tr('Creating new UE Capabilites entry'));
      }

      if (mod_type == 'update') {
        p.setCaption('[' + id + '] ' +
          this.tr('Updating UE Capabilites entry'));
      }

      this.__R.get({ id: id });
    }, this);

    this.__vmngr = new qx.ui.form.validation.Manager();
    this.__setupResources();

    // handle submit bar
    var toolbar = new qx.ui.container.Composite(
      new qx.ui.layout.Flow(0, 4, 'right'));

    var butSubmit = new qx.ui.form.Button();
    butSubmit.setLabel(
      mod_type == 'create' ? this.tr('Create') : this.tr('Save'));
    butSubmit.setIcon(
      mod_type == 'create' ? 'atms/new-white.png' : 'atms/update-white.png');
    butSubmit.addState(mod_type == 'create' ? 'critical' : 'info');

    butSubmit.addListener('execute', function() {
      if (!this.__vmngr.validate()) return;

      this.__S.getModel().setRef_cbs(
        (this.__widgets['ref_cbs'].getValues().getItem(0).getId()));
      //this.__S.getModel().setOutput_format(
      //  (this.__widgets['output_format'].getValue().getItem(0).getId()));

      this.__R.post(
        { id: id },
        { content: this.__serialize(this.__S.getModel()) });
    }, this);

    toolbar.add(butSubmit);

    this.__form.add(toolbar, { row: 9, column: 0, colSpan: 4 });

    // handle events
    butForm.bind('value', this.__form, 'visibility', {
      converter: function(value) {
        return value ? 'visible' : 'excluded';
      }
    });
    butForm.setValue(true);
  },

  members: {
    __parent: null,
    __holder: null,
    __butAtt: null,
    __form: null,
    __R: null,
    __S: null,
    __widgets: null,
    __vmngr: null,
    __callback_s: null,
    __callback_f: null,

    __setupResources: function() {
      var api = {
        get: {
          method: 'GET',
          url: '/atms/lipstein/api/atms_uecap/id/{id}.json'
        },
        post: {
          method: 'POST',
          url: '/atms/lipstein/api/atms_uecap/{id}'
        },
        getDeviceModel: {
          method: 'GET',
          url: '/atms/lipstein/api/cbs_device/id/{id}/model/name.json'
        },
        getDeviceIMEI: {
          method: 'GET',
          url: '/atms/lipstein/api/cbs_device/id/{id}/imei.json'
        },
        getDeviceProducent: {
          method: 'GET',
          url: '/atms/lipstein/api/cbs_device/id/{id}/company/name.json'
        }
      };
      this.__R = new qx.io.rest.Resource(api);

      this.__R.addListenerOnce('getSuccess', function(e) {
        var fields = e.getData().content[0];
        this.__widgets = {};
        for (var k in fields) {
          this.__widgets[k] = this.__widgetFactory(k);
        }

        if (fields.hasOwnProperty('id')) {
          var upload = new atms.Uploader('atms_attachments', {
              refuecap: fields.id
            }, []);
          this.__holder.add(upload, { flex: 1 });
          this.__butAtt.bind('value', upload, 'visibility', {
            converter: function(value) {
              return value ? 'visible' : 'excluded';
            }
          });
        }
      }, this);

      this.__R.addListener('postSuccess', function() {
        if (this.__callback_s) this.__callback_s();
        console.log('bubu');
      }, this);

      this.__R.addListener('error', function(e) {
        if (this.__callback_f) this.__callback_f();
        this.__parent.setShowStatusbar(true);
        this.__parent.setStatus(
          this.tr('Operation failed!'));
      }, this);

      this.__S = new qx.data.store.Rest(this.__R, 'get', {
        manipulateData: function(data) {
          return data.content[0];
        }
      });

      // binding
      this.__S.addListenerOnce('changeModel', function(e) {
        for (var k in this.__widgets) {
          if (!this.__widgets[k]) continue;
          var cls = this.__widgets[k].classname;
          if (cls == 'atms.VirtualSelectBox') {
            qx.data.SingleValueBinding.bind(
              this.__S, 'model.' + k, this.__widgets[k], 'value');
          } else {
            qx.data.SingleValueBinding.bind(
              this.__S, 'model.' + k, this.__widgets[k], 'value', {
                converter: function(value) {
                  return value ? String(value) : '';
                }
              });
            qx.data.SingleValueBinding.bind(
              this.__widgets[k], 'value', this.__S, 'model.' + k);
          }
        }
      }, this);

      // bindings for helpers - model, producent
      var modelS = new qx.data.store.Rest(this.__R, 'getDeviceModel');
      modelS.addListenerOnce('changeModel', function() {
        qx.data.SingleValueBinding.bind(
          modelS, 'model.content[0].name',
          this.__widgets['f_model'], 'value');
      }, this);

      var producentS = new qx.data.store.Rest(this.__R, 'getDeviceProducent');
      producentS.addListenerOnce('changeModel', function() {
        qx.data.SingleValueBinding.bind(
          producentS, 'model.content[0].name',
          this.__widgets['f_company'], 'value');
      }, this);

      var imeiS = new qx.data.store.Rest(this.__R, 'getDeviceIMEI');
      producentS.addListenerOnce('changeModel', function() {
        qx.data.SingleValueBinding.bind(
          imeiS, 'model.content[0].imei',
          this.__widgets['f_imei'], 'value');
      }, this);
    },

    __widgetFactory: function(id) {
      var that = this;
      var rowFlex = 0;
      var widget = null;
      var validator = null;
      switch (id) {
        case 'f_category_lte':
        case 'id':
        case 'f_processed':
        case 'f_ca_json':
        case 'f_caps_json':
        case 'f_ca_search_index':
        case 'f_pdn_type':
        case 'f_mimoDL':
        case 'f_mimoUL':
        case 'f_bands':
        case 'f_uecap_size':
        case 'f_dl_mimo4l':
        case 'f_dl_qam256':
        case 'f_ul_qam64':
          return null;
        break;

        case 'ref_cbs':
          widget = new atms.VirtualSelectBox(
            '/atms/manager/call/json/store/atms_uecap/ref_cbs',
            { null_option: true });
          // widget.setREST(
          //   '/atms/manager/call/json/store/atms_uecap/ref_cbs');
          widget.addListener('closed', function() {
            var index = widget.getValues().getItem(0).getId();
            this.__R.getDeviceModel({ id: index });
            this.__R.getDeviceProducent({ id: index });
            this.__R.getDeviceIMEI({ id: index });
          }, this);

        break;

        //case 'output_format':
        //  var store = [
        //    { id: 'pdml', label: this.tr('pdml + generate views') },
        //    { id: 'text', label: 'text' },
        //    { id: 'psml', label: 'psml' },
        //    { id: 'ps', label: 'ps' },
        //    { id: 'fields', label: 'fields' },
        //  ];
        //  store = qx.data.marshal.Json.createModel(store);
        //  widget = new cosmetosafe.MultiSelectBox();
        //  widget.setStaticModel(store);
        //  widget.setPreselection('pdml');
        //break;

        case 'f_uecap_hex':
        case 'f_attach_hex':
          rowFlex = 1;
          widget = new qx.ui.form.TextArea();
        break;

        case 'f_imei':
          widget = new qx.ui.form.TextField();
          validator = function(value, item) {
            if (!value) return true;
            value = value.split('/');
            var valid = true;
            for (var i = 0; i < value.length; i++) {
              valid = valid && !isNaN(value[i]) && value[i].length == 15;
            }

            if (!valid) {
              item.setInvalidMessage(
                that.tr(
                  'IMEIs have to be numbers of 15 signs<br>' +
                  'long and optionally separated by /'));
            }

            return valid;
          };

        break;

        default:
          widget = new qx.ui.form.TextField();
      }
      var label = id;

      var pos = {
        ref_cbs: 1,
        f_model: 3,
        f_company: 2,
        f_chipset: 4,
        f_category_lte: 5,
        f_sw_version: 6,
        f_uecap_hex: 7,
        f_attach_hex: 8,
        f_imei: 0
      };

      var tr = {
        f_imei: this.tr('IMEI'),
        ref_cbs: this.tr('Ref CBS'),
        f_model: this.tr('Model'),
        f_company: this.tr('Company'),
        f_chipset: 'Chipset',
        f_category_lte: this.tr('UE category'),
        f_sw_version: this.tr('SW version'),
        f_uecap_hex: this.tr('UE Capabilites hex file'),
        f_attach_hex: this.tr('Attach Request hex file')
      };
      this.__form.add(
        new qx.ui.basic.Label().set({ value: tr[id] }),
        { row: pos[id], column: 0 });
      this.__form.add(widget, { row: pos[id], column: 1, colSpan: 3 });
      this.__form.getLayout().setRowFlex(pos[id], rowFlex);

      if (validator) {
        this.__vmngr.add(widget, validator);
      }

      return widget;
    },

    __serialize: function(model) {
      return qx.util.Serializer.toJson(
          model,
          null,
          new qx.util.format.DateFormat('yyyy-MM-dd kk:mm:ss'));
    }
  }
});
