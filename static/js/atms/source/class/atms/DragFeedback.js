/**
*@asset(atms/*)
*@asset(qx/icon/${qx.icontheme}/*)
*/

qx.Class.define('atms.DragFeedback',
{
  extend: qx.ui.container.Composite,

  construct: function(caption) {
    this.base(arguments);

    var layout = new qx.ui.layout.VBox(0);
    this.setLayout(layout);

    var msg = new qx.ui.basic.Label(caption);
    msg.setBackgroundColor('white');
    msg.setPadding([0, 4, 0, 24]);
    this.add(msg);

    this.setOpacity(0.9);
    this.setZIndex(500);

    this.__children = new qx.data.Array();

    this.setLayoutProperties({ left: -1000, top: -1000 });
  },

  members: {
    __children: null,

    addDragChild: function(item) {
      this.add(item);
      this.__children.push(item);
    },

    purgeChildren: function() {
      this.__children.forEach(function(item) {
        this.remove(item);
      }, this);

      this.__children.removeAll();
    }
  }
});
