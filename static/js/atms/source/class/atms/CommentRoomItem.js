/**
*@asset(atms/*)
*@asset(qx/icon/${qx.icontheme}/*)
*/

qx.Class.define('atms.CommentRoomItem',
{
  extend: qx.ui.core.Widget,
  include: [qx.ui.form.MModelProperty],

  properties: {
    appearance: {
      refine: true,
      init: 'chat'
    },
    channel: {
      init: 'channel',
      check: 'String',
      apply: '_applyChannel'
    },
    caption: {
      init: '-',
      nullable: true,
      apply: '_applyCaption'
    },
    count: {
      init: '-',
      check: 'String',
      apply: '_applyCount'
    },
    windowCaption: {
      init: '-',
      nullable: true,
      apply: '_applyWindowCaption'
    }
  },

  construct: function() {
    this.base(arguments);

    var layout = new qx.ui.layout.Grid(3, 3);
    layout.setColumnFlex(1, 2);
    this._setLayout(layout);
    this._createChildControl('icon');
    this._createChildControl('channel');
    this._createChildControl('count');
    this._createChildControl('caption');
  },

  members: {
    _createChildControlImpl: function(id) {
      var control;
      var that = this;

      switch (id)
      {
        case 'icon':
          control = new qx.ui.basic.Image();
          control.setWidth(52);
          control.setHeight(52);
          control.setScale(true);
          this._add(control, { row: 0, column: 0, rowSpan: 4 });
        break;

        case 'channel':
          control = new qx.ui.basic.Label();
          this._add(control, { row: 0, column: 1 });
        break;

        case 'count':
          control = new qx.ui.basic.Label();
          this._add(control, { row: 0, column: 2 });
        break;

        case 'caption':
          control = new qx.ui.basic.Label();
          this._add(control, { row: 1, column: 1 });
        break;
      }
      return control || this.base(arguments, id);
    },

    _applyChannel: function(value, old) {
      var color = '#000';
      var channel = this.getChildControl('channel');
      var icon = this.getChildControl('icon');
      var re = /(.*?)_?(\d+)?$/g;
      var m = re.exec(value);
      var label = this.tr('Channel') + ': ';
      if (m) {
        if (m[1]) {
          var icon_path = 'atms/bubbles.png';
          switch (m[1]) {
            case 'atms_tester':
              label += this.tr('Testers');
              icon.setSource('atms/award_best_day.png');
            break;

            case 'atms_manager':
              label += this.tr('Managers');
              icon.setSource('atms/award_best_manager.png');
            break;

            case 'wiki_editor':
              label += this.tr('Wiki editors');
              icon.setSource('atms/award_wiki_master.png');
            break;

            case 'atms_cases':
              label += this.tr('Cases');
              icon.setSource('atms/casechat.png');
            break;

            case 'users':
              label += this.tr('Chat');
              icon.setSource('atms/chat.png');
            break;

            default:
              label += m[1];
            break;
          }
        }

        if (m[2]) label += ' [' + m[2] + ']';
      }

      this.addState(m[1]);
      channel.setValue(label);
    },

    _applyCaption: function(value, old) {
      if (value) {
        var caption = this.getChildControl('caption');
        caption.setValue(value);
      }
    },

    _applyCount: function(value, old) {
      var count = this.getChildControl('count');
      count.setValue(String(value) + ' ' + this.tr('messages'));
    }
  }
});
