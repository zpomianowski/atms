/**
*@asset(atms/*)
*@asset(qx/icon/${qx.icontheme}/*)
*/

qx.Class.define('atms.FormMeal',
  {
    extend: qx.ui.container.Composite,

    construct: function (id) {
      this.base(arguments);

      var layout = new qx.ui.layout.Grid(4, 4);
      layout.setColumnWidth(0, 200);
      layout.setColumnFlex(1, 1);
      layout.setRowFlex(4, 1);
      layout.setColumnWidth(2, 250);
      layout.setColumnFlex(3, 1);

      this.setLayout(layout);

      this.__setup();

      var toolbar = new qx.ui.container.Composite(
        new qx.ui.layout.Flow(0, 4, 'right'));
      var butSubmit = new qx.ui.form.Button().set({
        label: this.tr('Save')
      });
      var butManagePlaces = new qx.ui.form.Button().set({
        label: this.tr('Manage Places')
      });
      var butSettle = new qx.ui.form.Button().set({
        label: this.tr('Settle'),
        enabled: false,
        toolTip: new qx.ui.tooltip.ToolTip(this.tr(
          'You are the chosen one! ATMS picks responsible user automatically.'))
      });
      butSubmit.addState('info');
      butManagePlaces.addState('critical');
      butSettle.addState('critical');
      this.__butSettle = butSettle;
      this.__butManagePlaces = butManagePlaces;

      toolbar.add(butSettle);
      toolbar.add(butManagePlaces);
      toolbar.add(butSubmit);
      this.add(toolbar, { row: 10, column: 0, colSpan: 4 });

      butSubmit.addListener('execute', function () {
        this.__R.put({ id: this.__S.getModel().getId() },
          { content: this.__serialize(this.__S.getModel()) }
        );
      }, this);

      butManagePlaces.addListener('execute', function () {
        var widget = new atms.table.TableCanteen();
        var win = new atms.WinPanel(widget);
        qx.core.Init.getApplication().gDesk1.add(win);
        win.open();
      }, this);

      butSettle.addListener('execute', function () {
        var widget = new atms.FormMealSettle();
        var win = new atms.WinPanel(widget);
        qx.core.Init.getApplication().gDesk1.add(win);
        win.open();
      }, this);

      if (id) this.__R.get({ id: id });
      else this.__R.get();
      this.__R.checkmyduty();
    },

    events: {
      init: 'qx.event.type.Data'
    },

    members: {
      __R: null,
      __S: null,
      __widgets: null,
      __ctrl: null,
      __butManagePlaces: null,
      __butSettle: null,

      __setup: function () {
        this.__widgets = {};
        var api = {
          get: {
            method: 'GET',
            url: '/atms/meal/api/atms_order/id/{id=null}.json'
          },
          put: {
            method: 'PUT',
            url: '/atms/meal/api/atms_order.json/?id={id}'
          },
          checkmyduty: {
            method: 'GET',
            url: '/atms/meal/duty.json'
          }
        };
        this.__R = new qx.io.rest.Resource(api);

        this.__S = new qx.data.store.Rest(this.__R, 'get', {
          manipulateData: function (data) {
            return data.content[0];
          }
        });

        // get responsible person
        this.__R.addListener('checkmydutySuccess', function (e) {
          this.__butSettle.setEnabled(e.getData().content);
        }, this);

        this.__R.addListener('putSuccess', function (e) {
          this.getLayoutParent().getLayoutParent().close();
        }, this);

        // init form
        this.__R.addListenerOnce('getSuccess', function (e) {
          var data = e.getData().content[0];
          for (var k in data) {
            var w = this.__widgetFactory(k);
            this.__widgets[k] = w;
          }

          this.fireDataEvent('init', Object.keys(data));
        }, this);

        this.addListener('init', function (e) {
          this.__ctrl = new qx.data.controller.Object(this.__S.getModel());
          e.getData().forEach(function (k) {
            var w = this.__widgets[k];
            if (w) {
              if (this.__widgets[k].classname == 'atms.VirtualSelectBox')
                this.__widgets[k].addListener('closed', function () {
                  this.__ctrl.getModel().set(
                    k, this.__widgets[k].getValues().getItem(0).getId());
                }, this);

              if (k == 'f_not_interested' &&
                  this.__widgets[k].classname == 'qx.ui.form.CheckBox') {
                this.__widgets[k].addListener('changeValue', function (e) {
                  for (var kk in this.__widgets) {
                    var w = this.__widgets[kk];
                    if (!w || kk == 'f_not_interested') continue;
                    this.__widgets[kk].setEnabled(!e.getData());
                  }
                }, this);
              }

              this.__ctrl.addTarget(this.__widgets[k], 'value', k, true);
            }
          }, this);
        }, this);
      },

      __widgetFactory: function (id) {
        var widget = null;
        var order = [
          'f_1', 'f_2', 'f_3', 'f_final',
          'f_name', 'f_cost', 'f_cash', 'f_not_interested'];
        var row = order.indexOf(id);
        var tr = {
          f_1: this.tr('First choice'),
          f_2: this.tr('Second choice'),
          f_3: this.tr('Third choice'),
          f_final: this.tr('Chosen option'),
          f_name: this.tr('Meal'),
          f_cost: this.tr('Cost'),
          f_cash: this.tr('Payed'),
          f_not_interested: this.tr('Not interested / got own meal')
        };
        var label = new qx.ui.basic.Label().set({ value: tr[id] || 'TODO' });

        switch (id) {
          case 'f_1':
          case 'f_2':
          case 'f_3':
          case 'f_final':
            widget = new atms.VirtualSelectBox(
              '/atms/meal/call/json/store/atms_order/f_1',
              { null_option: true, group: false });
            if (id == 'f_final') {
              widget.setEnabled(false);

              // disable the form when final option is not set
              widget.addListener('changeValue', function (e) {
                this.setEnabled(e.getData() != null);
                this.__butManagePlaces.setEnabled(true);
              }, this);
            }

            break;

          case 'f_name':
            widget = new qx.ui.form.TextArea();
            break;

          case 'f_cash':
          case 'f_cost':
            var nf = new qx.util.format.NumberFormat('pl');
            nf.setPostfix(' PLN');
            widget = new qx.ui.form.Spinner().set({
              maximum: 999,
              minimum: 0,
              numberFormat: nf
            });
            var that = this;
            break;
          case 'f_not_interested':
            widget = new qx.ui.form.CheckBox();
            break;
        }
        if (row > -1) {
          this.add(label, { row: row, column: 0 });
          this.add(widget, { row: row, column: 1, colSpan: 3 });
        }

        this.__wid += 2;
        return widget;
      },

      __serialize: function (model) {
        var data = qx.util.Serializer.toNativeObject(
          model,
          null,
          new qx.util.format.DateFormat('yyyy-MM-dd kk:mm:ss'));
        return qx.lang.Json.stringify([data]);
      }
    }
  });
