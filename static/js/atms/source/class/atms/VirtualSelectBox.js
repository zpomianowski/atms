/**
*@asset(atms/*)
*@asset(qx/icon/${qx.icontheme}/*)
*/

qx.Class.define('atms.VirtualSelectBox',
{
  implement: [qx.ui.table.ICellEditorFactory, qx.ui.form.IExecutable],
  include: qx.ui.core.MExecutable,
  extend: qx.ui.form.VirtualSelectBox,

  construct: function(model, settings) {
    var that = this;
    this.__applyDefaults(settings);
    this.base(arguments);

    this.setupModel(model);

    //setup filter
    var filter = new qx.ui.form.TextField().set({
      focusable: true,
      placeholder: that.tr('Search...'),
      appearance: 'textfield-no-border'
    });
    filter.addListener('changeValue', function() {
      if (!this.__R) {
        this.refresh();
        return;
      }

      this.__R.get({
        q: filter.getValue(),
        extra: '?' + this.getMultiCachetoUri()
      });
    }, this);

    //setup dropdown
    var dd = this.getChildControl('dropdown');
    dd.setMinWidth(600);
    dd.setMinHeight(32);
    dd.add(filter);
    dd.close = function() {};

    dd.setAutoHide(true);

    //setup delegate to configure list items
    var delegate = {
      configureItem: function(item) {
        item.addListener('tap', function() {
          setTimeout(function() {
            that.fireEvent('closed');
            dd.hide();
          }, 0);
        });
      },

      sorter: function(a, b) {
        var a = a.getLabel();
        var b = b.getLabel();
        return a > b ? 0 : a < b ? -1 : 1;
      },

      group: function(model) {
        return model.getLabel().charAt(0).toUpperCase();
      },

      filter: function(item) {
        if (item.hasOwnProperty('$$user_value'))
            if (item.getValue()) return true;
        var b = filter.getValue();
        if (!b) return true;
        var a = item.getLabel().toLowerCase();
        if (qx.lang.String.contains(a, b)) return true;
        else return false;
      }
    };
    if (!this.__settings.group) delegate.group = null;

    if (this.__settings.multi) {
      this.setMultiCache({});
      delegate.createItem = function() {
        var box = new qx.ui.form.CheckBox();
        box.addListener('click', function() {
          that.refresh();
        });

        return box;
      };

      delegate.bindItem = function(controller, item, index) {
        controller.bindProperty('', 'model', null, item, index);
        controller.bindProperty(that.getLabelPath(), 'label', that.getLabelOptions(), item, index);
        controller.bindProperty('value', 'value', {
          converter: function(value, model) {
            if (value)
                that.getMultiCache()[model.getId()] = value;
            else delete that.getMultiCache()[model.getId()];
            return value;
          }
        }, item, index);
        controller.bindPropertyReverse('value', 'value', null, item, index);
      };
    }

    this.setDelegate(delegate);

    this.setLabelPath('label');

    this.__R.get({
      q: null,
      extra: ''
    });
  },

  events: {
    closed: 'qx.event.type.Event'
  },

  properties: {
    value: {
      nullable: true,
      apply: '_applyValue',
      event: 'changeValue'
    },
    multiCache: {
      nullable: true,
      apply: '_applyMultiCache',
      event: 'changeMultiCache'
    }
  },

  members: {
    __R: null,
    __settings: null,

    _applyValue: function(value, old) {
      if (!value) return;
      var preselected = [];
      if (!(value instanceof Array)) {
        preselected.push(parseInt(value) || value);
      }

      if (this.getModel().length == 0) {
        this.__R.get({
          q: null,
          extra: '?extra=' + preselected.join('&extra=')
        });
        return;
      }

      this.getModel().forEach(function(d) {
        var fIndex = preselected.indexOf(d.getId());
        if (fIndex > -1 && this.__settings.multi) {
          d.setValue(true);
          this.getMultiCache()[d.getId()] = true;
        }

        if (fIndex > -1 && !this.__settings.multi) {
          this.getSelection().push(d);
        }
      }, this);
    },

    getValues: function() {
      if (this.__settings.multi) {
        var data = [];
        for (var key in this.getMultiCache()) {
          data.push(key);
        }

        return data;
      } else
        return this.getSelection();
    },

    getMultiCachetoUri: function() {
      var data = [];
      for (var key in this.getMultiCache()) {
        data.push('extra=' + key);
      }

      return data.join('&');
    },

    setupModel: function(model) {
      this.addListener('changeModel', function() {
        this._applyValue(this.getValue());
      }, this);

      if (typeof (model) == 'string') {
        var that = this;
        var api = {
          get: {
            method: 'GET',
            url: model + '/{q}/{extra}'
          }
        };
        var R = new qx.io.rest.Resource(api);
        var S = new qx.data.store.Rest(R, 'get', {
          manipulateData: function(data) {
            if (that.__settings['null_option'])
              data.unshift({ id: 'None', label: that.tr('<None>'), value: false });
            return data;
          }
        });
        S.addListener('changeModel', function(e) {
          this.setModel(S.getModel());
          this.refresh();
        }, this);

        this.__R = R;

      } else this.setModel(model);
    },

    setStaticModel: function(model) {
      this.getDelegate().group = null;
      this.setModel(model);
    },

    toggle: function() {
      var dropDown = this.getChildControl('dropdown');

      if (dropDown.isVisible()) {
        this.fireEvent('closed');
        dropDown.hide();
      } else {
        this.open();
      }
    },

    __applyDefaults: function(settings) {
      this.__settings = settings || {};

      if (!this.__settings.hasOwnProperty('multi')) {
        this.__settings['multi'] = false;
      }

      if (!this.__settings.hasOwnProperty('null_option')) {
        this.__settings['null_option'] = false;
      }

      if (!this.__settings.hasOwnProperty('group')) {
        this.__settings['group'] = true;
      }
    },

    //what to display on default TextField
    _addBindings: function() {
      this.base(arguments);
      var that = this;
      var atom = this.getChildControl('atom');

      var custom = null;
      if (this.__settings.multi) {
        var custom = {
          converter: function(data) {
            var counter = 0;
            var currentValue = null;
            that.getModel().forEach(function(d) {
              if (d.getValue()) {
                counter += 1;
                currentValue = d.getLabel();
              }
            });

            if (!counter) return that.tr('No selection');
            var value = counter == 1 ? currentValue : that.tr('selected') + ' ' + counter;
            return value;
          }
        };
      }

      var opts = this.getLabelOptions() || custom;
      var labelSourcePath = this._getBindPath('selection', this.getLabelPath());
      this.bind(labelSourcePath, atom, 'label', opts);
    },

    _applyMultiCache: function(value, old) {},

    createCellEditor: function(cellInfo) {
      this.setPreselection(cellInfo.value);
      return this;
    },

    //no support for mutli
    getCellEditorValue: function(cellEditor) {
      return cellEditor.getSelection().getItem(0).getId();
    }
  }
});
