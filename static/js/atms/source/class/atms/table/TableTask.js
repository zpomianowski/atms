/**
*@asset(atms/*)
*@asset(qx/icon/${qx.icontheme}/*)
*/

qx.Class.define('atms.table.TableTask',
{
  extend: cosmetosafe.TableGeneric,

  construct: function() {
    this.base(arguments,
      'scheduler_task',
      '/atms/manager/tapi/scheduler_task/count',
      '/atms/manager/tapi/scheduler_task/data',
      {
        columns: {
          id: this.tr('Id'), //0
          application_name: this.tr('Application'), //1
          task_name: this.tr('Task'), //2
          group_name: this.tr('Group'), //3
          status: this.tr('Status'), //4
          function_name: this.tr('Function'), //5
          uuid: this.tr('UUID'), //6
          args: this.tr('Args'), //7
          vars: this.tr('Vars'), //8
          enabled: this.tr('Enabled'), //9
          start_time: this.tr('Start time'), //10
          next_run_time: this.tr('Next run time'), //11
          stop_time: this.tr('Stop time'), //12
          repeats: this.tr('Repeats'), //13
          retry_failed: this.tr('Retry failed'), //14
          period: this.tr('Period [s]'), //15
          prevent_drift: this.tr('Prevent drift'), //16
          timeout: this.tr('Timeout [s]'), //17
          sync_output: this.tr('Sync output [s]'), //18
          times_run: this.tr('Times run'), //19
          times_failed: this.tr('Times failed'), //20
          last_run_time: this.tr('Last run time'), //21
          assigned_worker_name: this.tr('Assigned worker') //22
        },
        to_hide: [1, 5, 6, 7, 8, 9, 16, 18],
        renderer_map: {
          0: 'id',
          4: 'status',
          2: 'function',
          9: 'bool',
          16: 'bool',
          18: 'bold',
          19: 'green',
          20: 'red'
        },
        editor_map: {
          1: 'text',
          2: 'text',
          3: 'text',
          4: 'status',
          5: 'function',
          9: 'bool',
          13: 'text',
          14: 'text',
          15: 'text',
          16: 'bool',
          17: 'text',
          18: 'text',
          19: 'text',
          20: 'text'
        }
      }
    );
    this.__setupResources();
    this.setModalEditorFactory(
      new atms.ModalCellEditorFactory(),
      [7, 8, 10, 11, 12]
    );
    this.getTable().setForceLineHeight(false);
  },

  members: {
    __R: null,
    __statuses: null,
    __statusesS: null,
    __functions: null,
    __functionsS: null,

    __menuList: null,

    createMenu: function() {
      var mm = {};
      var menu = new qx.ui.menu.Menu();

      mm.butKillTask = new qx.ui.menu.Button(this.tr('Kill task'),
        'atms/stop.png', null, null);
      mm.butResumeTask = new qx.ui.menu.Button(this.tr('Resume task'),
        'atms/play.png', null, null);
      mm.butDel = new qx.ui.menu.Button(this.tr('Delete task'),
        'atms/trash.png', null, null);
      mm.butNew = new qx.ui.menu.Button(this.tr('Add new empty task'),
        'atms/edit.png', null, null);
      mm.butEdit = new qx.ui.menu.Button(this.tr('Details & dependencies'),
        'atms/edit.png', null, null);

      mm.butClearOk = new qx.ui.menu.Button(this.tr('Clear completed'),
        'atms/trash.png');
      mm.butClearAll = new qx.ui.menu.Button(this.tr('Clear consumed'),
        'atms/trash.png');

      mm.butKillTask.addListener('execute', function() {
        this.__killTask();
      }, this);

      mm.butResumeTask.addListener('execute', function() {
        this.__resumeTask();
      }, this);

      mm.butDel.addListener('execute', function() {
        this.__deleteTask();
      }, this);

      mm.butNew.addListener('execute', function() {
        this.__R.post({ task: '_empty' });
      }, this);

      mm.butEdit.addListener('execute', function() {
        this.__viewTask();
      }, this);

      mm.butClearOk.addListener('execute', function() {
        this.__clearTasks('clear_finished_ok');
      }, this);

      mm.butClearAll.addListener('execute', function() {
        this.__clearTasks('clear_finished_all');
      }, this);

      menu.add(mm.butNew);
      menu.add(mm.butEdit);
      menu.add(new qx.ui.menu.Separator());
      menu.add(mm.butDel);
      menu.add(new qx.ui.menu.Separator());
      menu.add(mm.butClearOk);
      menu.add(mm.butClearAll);
      menu.add(new qx.ui.menu.Separator());
      menu.add(mm.butKillTask);
      menu.add(new qx.ui.menu.Separator());
      menu.add(mm.butResumeTask);
      menu.add(new qx.ui.menu.Separator());

      this.addListener('beforeContextmenuOpen', function() {
        // default disabled
        for (var key in mm) {
          mm[key].setEnabled(false);
        }

        var query = {};
        query.ids = this.getIds();
        query.rname = 'scheduler_task';
        var notempty = Boolean(query.ids.length);

        query = qx.data.marshal.Json.createModel(query);
        var json = qx.util.Serializer.toJson(query);

        var req = new qx.io.request.Xhr(
            '/atms/default/call/json/getperm/' +
            qx.util.Base64.encode(json));
        req.addListener('success', function(e) {
          var permmap = e.getTarget().getResponse();

          if (permmap.read || permmap.update) {
            mm.butEdit.setEnabled(true && notempty);
          }

          if (permmap.create) {
            mm.butNew.setEnabled(true);
            mm.butResumeTask.setEnabled(true);
          }

          if (permmap.del) {
            mm.butDel.setEnabled(true && notempty);
            mm.butKillTask.setEnabled(true && notempty);
            mm.butClearOk.setEnabled(true);
            mm.butClearAll.setEnabled(true);
          }
        });

        req.send();
      }, this);

      return menu;
    },

    cellRenderFactory: function(name) {
      var renderer = null;
      switch (name) {
        case 'id':
          renderer = new qx.ui.table.cellrenderer.Number();
          var nf = new qx.util.format.NumberFormat();
          nf.setGroupingUsed(false);
          renderer.setNumberFormat(nf);
        break;

        case 'bool':
          renderer = new qx.ui.table.cellrenderer.Boolean();
        break;

        case 'bold':
          renderer = new qx.ui.table.cellrenderer.Conditional('right', '', '', '');
          renderer.addNumericCondition('!=',  0, null, null, null, 'bold');
        break;

        case 'red':
          renderer = new qx.ui.table.cellrenderer.Conditional('right', '', '', '');
          renderer.addNumericCondition('>',  0, null, '#FF0000', null, 'bold');
        break;

        case 'green':
          renderer = new qx.ui.table.cellrenderer.Conditional('right', '', '', '');
          renderer.addNumericCondition('>',  0, null, '#00A300', null, 'bold');
        break;

        case 'status':
          this.addStaticStore(name, [
              { id: 'QUEUED', label: this.tr('Queued') },
              { id: 'ASSIGNED', label: this.tr('Assigned') },
              { id: 'RUNNING', label: this.tr('Running') },
              { id: 'COMPLETED', label: this.tr('Completed') },
              { id: 'FAILED', label: this.tr('Failed') },
              { id: 'TIMEOUT', label: this.tr('Timeout') },
              { id: 'STOPPED', label: this.tr('Stopped') },
              { id: 'EXPIRED', label: this.tr('Expired') }
          ]);
          renderer = new atms.RendererCustom();
          renderer.setReplaceMap(
              this.getReplaceMap(name));
          renderer.addReversedReplaceMap();
        break;

        case 'function':
          this.addStaticStore(name, [
              { id: 'reportServerStatus', label: this.tr('Get slave status') },
              { id: 'restartAdb', label: this.tr('Restart Adb server') },
              { id: 'restartATMS', label: this.tr('Restart ATMS') },
              { id: 'webthumb', label: this.tr('Making thumbnail') },
              { id: 'addTcases', label: this.tr('Add templates of cases') },
              { id: 'listAndroidDevices', label: this.tr('Enumerate devices') },
              { id: 'runTest', label: this.tr('Run test') },
              { id: 'reboot', label: this.tr('Reboot device') },
              { id: 'makeWikiReport', label: this.tr('Make wiki report') },
              { id: 'makePartialWikiReport', label: this.tr('Make partial wiki report') },
              { id: 'makePDFReport', label: this.tr('Make PDF report') },
              { id: 'rebootHost', label: this.tr('Reboot testnode server') },
              { id: 'adbConsole', label: this.tr('Adb session') },
              { id: 'calculateAwards', label: this.tr('Recalculate rankings') },
              { id: 'dumpTcases', label: this.tr('Dump Tcases') },
              { id: 'notifyUser', label: this.tr('Notify user') }
          ]);
          renderer = new atms.RendererCustom();
          renderer.setReplaceMap(
              this.getReplaceMap(name));
          renderer.addReversedReplaceMap();
        break;
      }
      return renderer || new qx.ui.table.cellrenderer.Default();
    },

    cellEditorFactory: function(name) {
      var editor;
      switch (name) {
        case 'bool':
          editor = new qx.ui.table.celleditor.CheckBox;
        break;

        case 'status':
        case 'function':
          editor = new cosmetosafe.MultiSelectBox();
          editor.setStaticModel(
              this.getStaticStore(name));
        break;

        case 'text':
          editor = new qx.ui.table.celleditor.TextField();
        break;
      }
      return editor;
    },

    _dataEdited: function(row, col, value, oldValue, item, e) {
      var params = {};
      params['id'] = item.id;
      params[this.getModel().getColumnId(col)] = value;
      var model = qx.data.marshal.Json.createModel(params);
      this.__R.update(
        { id: item.id },
        { content: this.__serialize(model) });
    },

    __viewTask: function() {
      var item = this.getFirstFromSelection();
      var widget = new atms.WinTask(item.id);
      var win  = new atms.WinPanel(widget);
      qx.core.Init.getApplication().gDesk1.add(win);
      win.open();
      win.setCaption('[' + item.id + '] ' + this.tr('Task details'));
    },

    __killTask: function() {
      var that = this;
      var ids = this.getIds();

      var caption = ids.length > 1 ?
        this.tr('Stop these tasks?') : this.tr('Stop this task?');
      var desc = this.tr('This action will kill pending processess' +
        ' and set status to FAILED.');
      var dialog = new atms.WinConfirmation(
          caption,
          desc,
          function() {
            ids.forEach(function(item) {
              that.__R.kill({
                task_id: item,
                action: 'stop'
              });
            });
          });

      this.getApplicationRoot().add(dialog);
      dialog.open();
    },

    __resumeTask: function() {
      var that = this;
      var ids = this.getIds();

      ids.forEach(function(item) {
        that.__R.resume({
          task_id: item,
          action: 'resume'
        });
      });
    },

    __deleteTask: function() {
      var that = this;
      var ids = this.getIds();

      var caption = ids.length > 1 ?
        this.tr('Delete these tasks?') : this.tr('Delete this task?');
      var desc = this.tr('Make sure you know what you are doing. ' +
          'This action cannot be undone and can brake ' +
          'dependencies among tasks. In some cases it can fix locks.');
      var dialog = new atms.WinConfirmation(
          caption,
          desc,
          function() {
            ids.forEach(function(item) {
              that.__R.deleteTask({
                params: '?id=' + item
              });
            });
          });

      this.getApplicationRoot().add(dialog);
      dialog.open();
    },

    __clearTasks: function(action) {
      var that = this;
      var caption = this.tr('Clear task history?');
      var desc = this.tr('This action only clears the history of executed tasks.');
      var dialog = new atms.WinConfirmation(
          caption,
          desc,
          function() {
            that.__R.clear({ action: action });
          });

      this.getApplicationRoot().add(dialog);
      dialog.open();
    },

    __serialize: function(model) {
      var data = qx.util.Serializer.toNativeObject(
          model,
          null,
          new qx.util.format.DateFormat('yyyy-MM-dd kk:mm:ss'));
      return qx.lang.Json.stringify([data]);
    },

    __setupResources: function() {
      var stdroute = '/atms/manager/api/scheduler_task';
      this.__R = new qx.io.rest.Resource({
        update: {
            method: 'PUT',
            url: stdroute + '.json/?id={id}' },
        deleteTask: {
            method: 'DELETE',
            url: stdroute + '{params}' },
        kill: {
            method: 'PUT',
            url: '/atms/manager/task_api/{task_id}/{action}' },
        resume: {
            method: 'PUT',
            url: '/atms/manager/task_api/{task_id}/{action}' },
        clear: {
            method: 'DELETE',
            url: '/atms/manager/task_api/{action}' },
        post: {
            method: 'POST',
            url: '/atms/manager/task_api/{task}' }
      });
      this.__R.addListener('success', function() {
        this.setStatus('info', this.tr('Operation completed.'));
      }, this);

      this.__R.addListener('error', function() {
        this.reload();
        this.setStatus('error', this.tr('Operation failed.'));
      }, this);
    }
  }
});
