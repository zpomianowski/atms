qx.Class.define('atms.table.RemoteTableGeneric',
{
  extend: qx.ui.table.model.Remote,

  include: [qx.locale.MTranslation],

  construct: function(get_count_uri, get_data_uri, colnames_dict) {
    this.__get_count = get_count_uri;
    this.__get_data = get_data_uri;
    this.base(arguments);

    var col_keys = new Array();
    var col_values = new Array();

    for (var key in colnames_dict) {
      col_keys.push(key);
      col_values.push(colnames_dict[key]);
    }

    this.setColumns(col_values, col_keys);
    this.__colKeys = col_keys;

    this.setColumnSortable(0, true);

    this.setBlockSize(100);
    this.__setupResources();
  },

  properties: {
    descriptor: {
      init: new Object(),
      apply: '_applyDescriptor',
      event: 'changeDescriptor'
    }
  },

  members:
    {
      __get_count: null,
      __get_data: null,
      __colKeys: null,

      __setupResources: function() {
        var that = this;
        this.__rows = new qx.io.rest.Resource({
          getCount:   {
            method: 'GET',
            url: that.__get_count + '.json{query}'
          },
          getData:   {
            method: 'GET',
            url: that.__get_data + '.json{query}'
          }
        });
        this.__rows.addListener('getCountSuccess', function(e) {
          this.setDescriptor(e.getData().descriptor);
          this._onRowCountCompleted(e.getData().content);
        }, this);

        this.__rows.addListener('getDataSuccess', function(e) {
          this._onLoadRowDataCompleted(e.getData().content);
        }, this);
      },

      _loadRowCount: function() {
        var filter = '?';
        this.__rows.getCount({
          query: filter
        });
      },

      _onRowCountCompleted: function(result) {
        if (result != null) {
          this._onRowCountLoaded(result);
        }
      },

      _loadRowData: function(firstRow, lastRow) {
        var sortIndex = this.getSortColumnIndex() == -1 ?
            null : this.__colKeys[this.getSortColumnIndex()];
        var sortOrder =  this.isSortAscending() ? '' : '~';

        var filter = {
          from: firstRow,
          to: lastRow
        };
        if (sortIndex)
            filter['orderby'] = sortOrder + sortIndex;

        filter = qx.data.marshal.Json.createModel(filter);
        this.__rows.getData({
          query: '?' + qx.util.Serializer.toUriParameter(filter)
        });
      },

      _onLoadRowDataCompleted: function(result) {
        if (result != null) {
          // Apply it to the model - the method "_onRowDataLoaded" has to be called
          this._onRowDataLoaded(result);
        }
      },

      _applyDescriptor: function(descriptor) {
        // console.log(Object.keys(descriptor));
      }
    }
});
