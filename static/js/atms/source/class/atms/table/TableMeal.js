/**
*@asset(atms/*)
*@asset(qx/icon/${qx.icontheme}/*)
*/

qx.Class.define('atms.table.TableMeal',
{
  extend: cosmetosafe.TableGeneric,

  construct: function() {
    this.__setupResources();
    this.base(arguments,
      'atms_order',
      '/atms/meal/tapi/atms_order/count',
      '/atms/meal/tapi/atms_order/data',
      {
        columns: {
          id: this.tr('Id'), //0,
          created_by: this.tr('Created by'), //1
          f_final: this.tr('Chosen option'), //2
          f_name: this.tr('Order'), //3
          f_cost: this.tr('Cost'), //4
          f_cash: this.tr('Payed') //5
          // debt: this.tr('Debt') //6
        },
        to_hide: [0],
        col_size_hints: {
          1: 180,
          2: 120,
          3: 180,
          4: 80,
          5: 80
          // 6: 80
        },
        renderer_map: {
          4: 'currency',
          5: 'currency'
          // 6: 'debt'
        },
        editor_map: {
          4: 'currency',
          5: 'currency'
        }
      }
    );
    this.setModalEditorFactory(
      new atms.ModalCellEditorFactory(),
      [3]
    );
  },

  members: {
    __R: null,

    createMenu: function() {
      var mm = {};
      var menu = new qx.ui.menu.Menu();

      mm.butNew = new qx.ui.menu.Button(this.tr('Add order for user'),
        'atms/spoon-knife.png',  null,  null);
      mm.butDel = new qx.ui.menu.Button(this.tr('Delete order'),
        'atms/trash.png',  null,  null);

      mm.butDel.addListener('execute', function() {
        var selectedRowData = [];
        this.getIds().forEach(function(index) {
          selectedRowData.push(index);
        }, this);

        var that = this;
        var caption = selectedRowData.length > 1 ?
          this.tr('Delete these orders?') : this.tr('Delete this order?');
        var desc = this.tr(
          'This action will purge orders.\nAre you sure?\n\n' +
          'Please inform your colleague.');
        var dialog = new atms.WinConfirmation(
            caption,
            desc,  function() {
              selectedRowData.forEach(function(item) {
                that.__R.delUser({ id: item });
              });
            },  null);

        this.getApplicationRoot().add(dialog);
        dialog.open();
      }, this);

      menu.add(mm.butNew);
      menu.add(new qx.ui.menu.Separator());
      menu.add(mm.butDel);
      menu.add(new qx.ui.menu.Separator());

      this.__R.addListener('getUsersSuccess', function(e) {
        var user_menu = new qx.ui.menu.Menu();
        for (var i in e.getData().content) {
          var button = new qx.ui.menu.Button(
            e.getData().content[i].label, 'atms/user.png');

          // if 'let' would be supported by qooxdoo ;/
          // button.addListener('execute', function() {
          //   this.__R.updateUser({ id: e.getData().content[i].id });
          // }, this);
          var that = this;
          (function() {
            var item = i;
            button.addListener('execute', function() {
              that.__R.updateUser({ id: e.getData().content[item].id });
            });
          })();

          user_menu.add(button);
        }

        mm.butNew.setMenu(user_menu);
      }, this);

      this.addListener('beforeContextmenuOpen', function() {
        this.__R.getUsers();
      }, this);

      return menu;
    },

    cellRenderFactory: function(name) {
      var renderer = null;
      switch (name) {
        case 'id':
          renderer = new qx.ui.table.cellrenderer.Number();
          var nf = new qx.util.format.NumberFormat();
          nf.setGroupingUsed(false);
          renderer.setNumberFormat(nf);
        break;

        case 'debt':
          renderer = new qx.ui.table.cellrenderer.Conditional(
            'right', '', '', '');
          renderer.addNumericCondition('>', 0, null, '#FF0000', null, 'bold');
          renderer.addNumericCondition('<', 0, null, '#3d72c9', null, 'bold');
        break;

        case 'currency':
          renderer = new qx.ui.table.cellrenderer.Number();
          var nf = new qx.util.format.NumberFormat();
          nf.setPostfix(' PLN');
          renderer.setNumberFormat(nf);
        break;
      }
      return renderer || new qx.ui.table.cellrenderer.Default();
    },

    cellEditorFactory: function(name) {
      var editor;
      switch (name) {
        case 'currency':
          editor = new qx.ui.table.celleditor.TextField();
        break;
      }
      return editor;
    },

    _dataEdited: function(row, col, value, oldValue, item, e) {
      var params = {};
      params['id'] = item.id;
      params[this.getModel().getColumnId(col)] = value;
      var model = qx.data.marshal.Json.createModel(params);
      this.__R.update(
        { id: item.id },
        { content: this.__serialize(model) });
    },

    __serialize: function(model) {
      var data = qx.util.Serializer.toNativeObject(
          model,
          null,
          new qx.util.format.DateFormat('yyyy-MM-dd kk:mm:ss'));
      return qx.lang.Json.stringify([data]);
    },

    __setupResources: function() {
      this.__R = new qx.io.rest.Resource({
        getUsers: {
          method: 'GET',
          url: '/atms/meal/uapi.json' },
        updateUser: {
          method: 'PUT',
          url: '/atms/meal/uapi.json/?id={id}' },
        delUser: {
          method: 'DELETE',
          url: '/atms/meal/uapi.json/?id={id}' },
        update: {
            method: 'PUT',
            url: '/atms/meal/api/atms_order.json/?id={id}' }
      });

      this.__R.addListener('updateUserSuccess', function() {
        this.reload();
        this.setStatus('info', this.tr('Order added.'));
      }, this);

      this.__R.addListener('delUserSuccess', function() {
        this.reload();
        this.setStatus('info', this.tr('Order deleted.'));
      }, this);

      this.__R.addListener('success', function() {
        this.setStatus('info', this.tr('Operation completed.'));
      }, this);

      this.__R.addListener('error', function() {
        this.reload();
        this.setStatus('error', this.tr('Operation failed.'));
      }, this);
    }
  }
});
