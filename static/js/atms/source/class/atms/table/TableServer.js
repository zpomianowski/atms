/**
*@asset(atms/*)
*@asset(qx/icon/${qx.icontheme}/*)
*/

qx.Class.define('atms.table.TableServer',
{
  extend: cosmetosafe.TableGeneric,

  construct: function() {
    this.base(arguments,
      'atms_servers',
      '/atms/manager/tapi/atms_servers/count',
      '/atms/manager/tapi/atms_servers/data',
      {
        columns: {
          id: this.tr('Id'), //0,
          hostname: this.tr('Hostname'), //1
          description: this.tr('Description'), //2
          addr: this.tr('Addresses'), //3
          adb_version: this.tr('ADB version'), //4
          node_version: this.tr('node.js version'), //5
          appium_version: this.tr('Appium version'), //6
          atms_version: this.tr('ATMS version'), //7
          status: this.tr('Status'), //8
          is_master: this.tr('Is master') //9
        },
        to_hide: [0, 2, 4, 5, 6, 9],
        renderer_map: {
          0: 'id',
          8: 'status',
          9: 'checkbox'
        }
      },
      {
        tableColumnModel: function(obj) {
          return new qx.ui.table.columnmodel.Resize(obj);
        }
      }
    );
    this.__setupResources();
  },

  members: {
    __R: null,

    createMenu: function() {
      var mm = {};
      var menu = new qx.ui.menu.Menu();

      mm.butRestart = new qx.ui.menu.Button(
        this.tr('Reboot slave'), 'atms/hammer.png');
      mm.butRestartADB = new qx.ui.menu.Button(
        this.tr('Restart ADB server'), 'atms/hammer.png');
      mm.butStatus = new qx.ui.menu.Button(
        this.tr('Get slave status'), 'atms/cog.png');
      mm.butListDevices = new qx.ui.menu.Button(
        this.tr('List Android devices'), 'atms/cog.png');
      mm.butListLABDevices = new qx.ui.menu.Button(
        this.tr('List LAB devices'), 'atms/cog.png');

      mm.butRestart.addListener('execute', function() {
        this.__applyActionOnServer('rebootHost');
      }, this);

      mm.butRestartADB.addListener('execute', function() {
        this.__applyActionOnServer('restartAdb');
      }, this);

      mm.butStatus.addListener('execute', function() {
        this.__applyActionOnServer('reportServerStatus');
      }, this);

      mm.butListDevices.addListener('execute', function() {
        this.__applyActionOnServer('listAndroidDevices');
      }, this);

      mm.butListLABDevices.addListener('execute', function() {
        this.__applyActionOnServer('scanLab');
      }, this);

      menu.add(mm.butRestart);
      menu.add(mm.butRestartADB);
      menu.add(new qx.ui.menu.Separator());
      menu.add(mm.butStatus);
      menu.add(mm.butListDevices);
      menu.add(mm.butListLABDevices);
      menu.add(new qx.ui.menu.Separator());

      this.addListener('beforeContextmenuOpen', function() {
        // default disabled
        for (var key in mm) {
          mm[key].setEnabled(false);
        }

        var query = {};
        query.ids = this.getIds();
        query.rname = 'atms_servers';
        var notempty = Boolean(query.ids.length);

        query = qx.data.marshal.Json.createModel(query);
        var json = qx.util.Serializer.toJson(query);

        var req = new qx.io.request.Xhr(
            '/atms/default/call/json/getperm/' +
            qx.util.Base64.encode(json));
        req.addListener('success', function(e) {
          var permmap = e.getTarget().getResponse();

          if ((permmap.create || permmap.update || permmap.del) && notempty) {
            mm.butRestart.setEnabled(true);
            mm.butRestartADB.setEnabled(true);
            mm.butStatus.setEnabled(true);
            mm.butListDevices.setEnabled(true);
            mm.butListLABDevices.setEnabled(true);
          }
        });

        req.send();
      }, this);

      return menu;
    },

    __applyActionOnServer: function(task_name) {
      this.getTable().getSelectionModel().iterateSelection(function(index) {
        this.__R.addTask({
          task: task_name,
          params: 'group_name=' + this.getModel().getRowData(index).hostname
        });
      }, this);
    },

    cellRenderFactory: function(name) {
      var renderer = null;
      switch (name) {
        case 'id':
          renderer = new qx.ui.table.cellrenderer.Number();
          var nf = new qx.util.format.NumberFormat();
          nf.setGroupingUsed(false);
          renderer.setNumberFormat(nf);
        break;

        case 'checkbox':
          renderer = new qx.ui.table.cellrenderer.Boolean;
        break;

        case 'status':
          this.addStaticStore(name, [
            { id: 'up', label: this.tr('Up') },
            { id: 'down', label: this.tr('Down') },
            { id: 'unknown', label: this.tr('Unknown') },
            { id: 'rebooting', label: this.tr('Rebooting') }
          ]);
          renderer = new atms.RendererCustom();
          renderer.setReplaceMap(
              this.getReplaceMap(name));
          renderer.addReversedReplaceMap();
        break;
      }
      return renderer || new qx.ui.table.cellrenderer.Default();
    },

    __setupResources: function() {
      this.__R = new qx.io.rest.Resource({
        addTask: {
          method: 'POST',
          url: '/atms/manager/task_api/{task}?{params}'
        }
      });
      this.__R.addListener('success', function() {
        this.setStatus('info', this.tr('Operation completed.'));
      }, this);

      this.__R.addListener('error', function() {
        this.setStatus('error', this.tr('Operation failed.'));
      }, this);
    }
  }
});
