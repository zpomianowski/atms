/**
*@asset(atms/*)
*@asset(qx/icon/${qx.icontheme}/*)
*/

qx.Class.define('atms.table.TableCanteen',
{
  extend: cosmetosafe.TableGeneric,

  construct: function() {
    this.base(arguments,
      'atms_canteen',
      '/atms/meal/tapi/atms_canteen/count',
      '/atms/meal/tapi/atms_canteen/data',
      {
        columns: {
          id: this.tr('Id'), //0
          f_name: this.tr('Name'), //1
          f_contact: this.tr('Contact') //2
        },
        to_hide: [0]
      }
    );

    this.getTable().addListener('dblclick',  function(e) {
      this.__newOrEditCanteen();
    },  this);

    this.__setupResources();
  },

  members: {
    __R: null,

    createMenu: function() {
      var mm = {};
      var menu = new qx.ui.menu.Menu();

      mm.butDel = new qx.ui.menu.Button(this.tr('Delete canteens'),
        'atms/trash.png',  null,  null);

      mm.butDel.addListener('execute', function() {
        this.__deleteCanteen();
      }, this);

      menu.add(mm.butDel);
      menu.add(new qx.ui.menu.Separator());

      return menu;
    },

    __newOrEditCanteen: function() {
      var that = this;
      var widget = new atms.Form(
        'atms_canteen',  { id: this.getFirstFromSelection().id },  'update',
        function() {
          that.reload();
        },  null, 'meal');

      var win = new atms.WinPanel(widget);
      qx.core.Init.getApplication().gDesk1.add(win);
      win.setWidth(360); win.setHeight(360);
      win.open();
    },

    __deleteCanteen: function() {
      var that = this;
      var selectedRowData = [];
      this.getIds().forEach(function(index) {
        selectedRowData.push(index);
      }, this);

      var caption = selectedRowData.length > 1 ?
        this.tr('Delete these canteens?') : this.tr('Delete this canteen?');
      var desc = this.tr(
        'This action cannot be undone!\n' +
        'This can delete meal orders for many users!');
      var dialog = new atms.WinConfirmation(
          caption,
          desc,
          function() {
            selectedRowData.forEach(function(item) {
              that.__R.deleteCanteen({ id: item });
            });
          }
      );
      this.getApplicationRoot().add(dialog);
      dialog.open();
    },

    __setupResources: function() {
      this.__R = new qx.io.rest.Resource({
        deleteCanteen: {
            method: 'DELETE',
            url: '/atms/meal/api/atms_canteen?id={id}' }
      });
      this.__R.addListener('success', function() {
        this.reload();
        this.setStatus('info', this.tr('Operation completed.'));
      }, this);

      this.__R.addListener('error', function() {
        this.reload();
        this.setStatus('error', this.tr('Operation failed.'));
      }, this);
    },

    cellRenderFactory: function(name) {
      var renderer = null;
      switch (name) {
        case 'id':
          renderer = new qx.ui.table.cellrenderer.Number();
          var nf = new qx.util.format.NumberFormat();
          nf.setGroupingUsed(false);
          renderer.setNumberFormat(nf);
        break;
      }
      return renderer || new qx.ui.table.cellrenderer.Default();
    }
  }
});
