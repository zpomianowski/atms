/**
*@asset(atms/*)
*@asset(qx/icon/${qx.icontheme}/*)
*/

qx.Class.define('atms.table.TableDevice',
{
  extend: cosmetosafe.TableGeneric,

  construct: function(tcase_id, case_id, readonly) {
    qx.Class.include(qx.ui.table.Table, qx.ui.table.MTableContextMenu);
    this.base(arguments,
      'atms_devices',
      '/atms/manager/tapi/atms_devices/count',
      '/atms/manager/tapi/atms_devices/data',
      {
        columns: {
          check: this.tr('To test'), //0
          type: this.tr('Type'), //1
          host: this.tr('Host'), //2
          udid: this.tr('UDID/SN'), //3
          manufacturer: this.tr('Manufacturer'), //4
          subtype: this.tr('Subtype'), //5
          model: this.tr('Model'), //6
          android_version: this.tr('System version'), //7
          sdk: this.tr('Sdk'), //8
          state: this.tr('State'), //9
          connected: this.tr('Connected'), //10
          queue: this.tr('Queue'), //11
          runs: this.tr('Runs (this case)'), //12
          last_run: this.tr('Last run'), //13
          report_status: this.tr('Case status'), //14
          id: this.tr('Id') //15
        },
        to_hide: [0, 3, 7, 8, 10, 11, 12, 13, 14, 15],
        renderer_map: {
          0: 'checkbox',
          1: 'platform',
          9: 'map',
          10: 'power',
          14: 'map'
        }
      },
      {
        tableColumnModel: function(obj) {
          return new qx.ui.table.columnmodel.Resize(obj);
        }
      }
    );
    var tm = this.getTable().getTableModel();
    tm.setColumnSortable(0, false);
    tm.setColumnSortable(10, false);
    tm.setColumnSortable(11, false);
    tm.setColumnSortable(12, false);
    tm.setColumnSortable(13, false);

    // context menu
    for (var i = 0; i < this.getTable()
      .getTableColumnModel()
      .getOverallColumnCount(); i++) {
      this.getTable().setContextMenuHandler(
        i, this._contextMenuHandler, this);
    }

    this.__setupResources();
    this.setReadOnly(readonly || false);

    var ids = {};
    if (tcase_id) ids.tcase_id = tcase_id;
    if (case_id) ids.case_id = case_id;
    this.setHiddenQuery(ids);

    this.getTable().addListener('dblclick',  function(e) {
      this.__editDevice();
    },  this);
  },

  properties: {
    readOnly: {
      init: true,
      check: 'Boolean',
      apply: '_applyReadOnly',
      event: 'changeReadOnly'
    }
  },

  members: {
    __R: null,

    createMenu: function() {
      var mm = {};
      var menu = new qx.ui.menu.Menu();

      mm.butTelemetry = new qx.ui.menu.Button(
        this.tr('Telemetry'), 'atms/meter.png');
      mm.butAPI = new qx.ui.menu.Button(
        this.tr('Show API reference'), 'atms/question.png');
      mm.butRestart = new qx.ui.menu.Button(
        this.tr('Restart the device'), 'atms/hammer.png');
      mm.butRun = new qx.ui.menu.Button(
        this.tr('Run case again for this device'), 'atms/cog.png');
      mm.butReport = new qx.ui.menu.Button(
        this.tr('Show report for this device'),
            'atms/details.png');
      mm.butCleanReport = new qx.ui.menu.Button(
        this.tr('Clean device case status'), 'atms/remove-sign.png');
      mm.butConsoleADB = new qx.ui.menu.Button(
        this.tr('Open ADB console - EXPERIMENTAL'), 'atms/adb.png');
      mm.butEdit = new qx.ui.menu.Button(
        this.tr('Edit this device'), 'atms/cog.png');

      mm.butTelemetry.addListener('execute', function() {
        var dev = this.getFirstFromSelection();
       
        var params = qx.util.Serializer.toUriParameter(
          qx.data.marshal.Json.createModel({
            varhost: dev.host,
            kiosk: true
          })
        );

        window.open(
          '/telemetry/dashboard/db/stanowiska-stb?' +
          params.replace('var', 'var-')
        );
      }, this);

      mm.butAPI.addListener('execute', function() {
        var subtype = this.getFirstFromSelection().subtype;
        qx.bom.Window.open(
          '/atms/static/docs/tester/' +
          'tester.lab.html#module-tester.lab.' + subtype);
      }, this);

      mm.butEdit.addListener('execute', function() {
        this.__editDevice();
      }, this);

      mm.butRestart.addListener('execute', function() {
        this.__delegateTask('reboot');
      }, this);

      mm.butRun.addListener('execute', function() {
        this.__delegateTask('runTest');
      }, this);

      mm.butReport.addListener('execute', function() {
        var dev_id = this.getFirstFromSelection();
        var pad = '000000';
        var n = String(this.getHiddenQuery().case_id);
        var result = (pad + n).slice(-pad.length);
        window.open('/atms/manager/../default/wiki/atmscase' +
            result +
            '-report-' + dev_id);
      }, this);

      mm.butCleanReport.addListener('execute', function() {
        console.log('TODO');
      }, this);

      mm.butConsoleADB.addListener('execute', function() {
        var item = this.getFirstFromSelection();
        if (item.type != 'android') return;
        var widget = new atms.WinADB(item.udid);
        var win  = new atms.WinPanel(widget);
        qx.core.Init.getApplication().gDesk1.add(win);
        win.open();
        win.setCaption('[' + item.id + '] ' + item.model);
      }, this);

      // menu.add(mm.butReport);

      // TODO
      // menu.add(mm.butCleanReport);
      menu.add(mm.butTelemetry)
      menu.add(new qx.ui.menu.Separator());
      menu.add(mm.butAPI);
      menu.add(new qx.ui.menu.Separator());
      menu.add(mm.butEdit);
      menu.add(new qx.ui.menu.Separator());
      menu.add(mm.butRestart);
      menu.add(mm.butRun);
      menu.add(new qx.ui.menu.Separator());
      menu.add(mm.butConsoleADB);

      this.addListener('beforeContextmenuOpen', function() {
        // default disabled
        for (var key in mm) {
          mm[key].setEnabled(false);
        }

        var query = {};
        query.ids = this.getIds();
        query.rname = 'atms_devices';
        var notempty = Boolean(query.ids.length);

        query = qx.data.marshal.Json.createModel(query);
        var json = qx.util.Serializer.toJson(query);

        var req = new qx.io.request.Xhr(
            '/atms/default/call/json/getperm/' +
            qx.util.Base64.encode(json));
        req.addListener('success', function(e) {
          var permmap = e.getTarget().getResponse();
          var fhost = this.getFirstFromSelection().host;
          var ftype = this.getFirstFromSelection().type;
          var fsubtype = this.getFirstFromSelection().subtype;
          var fstate = this.getFirstFromSelection().state;
          var is_test_dev = (ftype == 'android' || ftype == 'dut');
          var show_stats = (fstate == 'busy' && fhost != 'shared');

          if ((permmap.create || permmap.update) && notempty) {
            mm.butRestart.setEnabled(is_test_dev);
            mm.butRun.setEnabled(is_test_dev);
            mm.butEdit.setEnabled(true);
          }

          if (permmap.read && notempty && this.getHiddenQuery().case_id) {
            mm.butReport.setEnabled(is_test_dev);
          }

          if (permmap.read) {
            mm.butAPI.setEnabled(true);
            mm.butTelemetry.setEnabled(show_stats);
            mm.butConsoleADB.setEnabled(is_test_dev);
          }

          if (permmap.del && notempty) {
            mm.butCleanReport.setEnabled(is_test_dev);
          }
        }, this);

        req.send();
      }, this);

      return menu;
    },

    _contextMenuHandler: function(col, row, table, dataModel, contextMenu) {
      var that = this;
      var store = null;
      switch (col) {
        case 0:
          store = [
              { id: true,  label: that.tr('Mark') },
              { id: false, label: that.tr('Unmark') }
          ];
        break;
      }
      if (store && !this.getReadOnly()) {
        store = qx.data.marshal.Json.createModel(store);
        var colID = that.getModel().getColumnId(col);
        var icon = 'atms/' + colID + '.png';
        contextMenu.add(
          new qx.ui.menu.Button(
            that.tr('Change selected to'),
            icon,
            null, (function() {
              var menu = new qx.ui.menu.Menu();
              var menuEntry;
              store.forEach(function(item) {
                var label = item.getLabel();
                menuEntry = new qx.ui.menu.Button(label);
                menuEntry.setUserData('value', item.getId());
                menuEntry.addListener('execute', (function(menuEntry) {
                  return function() {
                    var value = menuEntry.getUserData('value');
                    that._dataEdited(col, -1, value, null, that);
                  };
                })(menuEntry));

                menu.add(menuEntry);
              });

              return menu;
            })()));
      }

      contextMenu.setOffsetTop(-32);

      return true;
    },

    cellRenderFactory: function(name) {
      var renderer = null;
      switch (name) {
        case 'id':
          renderer = new qx.ui.table.cellrenderer.Number();
          var nf = new qx.util.format.NumberFormat();
          nf.setGroupingUsed(false);
          renderer.setNumberFormat(nf);
        break;

        case 'checkbox':
          renderer = new qx.ui.table.cellrenderer.Boolean;
        break;

        case 'platform':
          renderer = new atms.RendererCustomImg(24, 24);
        break;

        case 'power':
          var connected = new qx.ui.table.cellrenderer.Boolean();
          connected.setIconTrue('atms/power-cord.png');
          connected.setIconFalse('atms/power-cord-none.png');
          return connected;
        break;

        case 'map':
          this.addStaticStore(name, [
            { id: 'adb console', label: this.tr('ADB console') },
            { id: 'downloading', label: this.tr('Downloading') },
            { id: 'init', label: this.tr('Init') },
            { id: 'pending', label: this.tr('Pending') },
            { id: 'idle', label: this.tr('Idle') },
            { id: 'busy', label: this.tr('Busy') },
            { id: 'unknown', label: this.tr('Unknown') },
            { id: 'rebooting', label: this.tr('Rebooting') },
            { id: 'failed', label: this.tr('Failed') },
            { id: 'passed', label: this.tr('Passed') },
            { id: 'offline', label: this.tr('Offline') }
          ]);
          renderer = new atms.RendererCustom();
          renderer.setReplaceMap(
              this.getReplaceMap(name));
          renderer.addReversedReplaceMap();
        break;
      }
      return renderer || new qx.ui.table.cellrenderer.Default();
    },

    _dataEdited: function(row, col, value, oldValue, item, e) {
      var id = null;
      var table_name = null;
      if (this.getHiddenQuery().case_id) {
        id = this.getHiddenQuery().case_id;
        table_name = 'atms_cases';
      }

      if (this.getHiddenQuery().tcase_id) {
        id = this.getHiddenQuery().tcase_id;
        table_name = 'atms_tcases';
      }

      var devIds = this.getIds();
      var model = qx.data.marshal.Json.createModel({
        id: id,
        devs: devIds,
        add: value
      });

      this.__R.updateDevs({
        params: '?' + qx.util.Serializer.toUriParameter(model),
        table_name: table_name
      });
    },

    runTests: function() {
      this.__R.postTask({ task: 'runTest' },
          { dev_id: '_all',
            case_id: this.getHiddenQuery().case_id });
    },

    __delegateTask: function(task_name) {
      this.getIds().forEach(function(d) {
        var pvars = { dev_id: d };
        if (task_name == 'runTest')
          pvars.case_id = this.getHiddenQuery().case_id;
        this.__R.postTask(
          { task: task_name },
          pvars);
      }, this);
    },

    __editDevice: function() {
      var dev = this.getFirstFromSelection();
      var that = this;
      var caption = this.tr('For advanced users only!');
      var desc = this.tr(
          'ATMS should manage devices automatically.\n' +
          'Continue only if you are sure what you are doing!');
      var dialog = new atms.WinConfirmation(
          caption,
          desc,  function() {
            var widget = new atms.Form('atms_devices', {
              id: dev.id
            }, 'update');
            console.log(caption);
            var win = new atms.WinPanel(widget);
            qx.core.Init.getApplication().gDesk1.add(win);
            win.open();
          }, null);

      this.getApplicationRoot().add(dialog);
      dialog.open();
    },

    __setupResources: function() {
      this.__R = new qx.io.rest.Resource({
        // used for update
        updateDevs: {
            method: 'PUT',
            url: '/atms/manager/auto_devs_api/{table_name}{params}' },
        postTask: {
            method: 'POST',
            url: '/atms/manager/task_api/{task}' },
        getPreviewURL: {
          method: 'GET',
          url: '/atms/stb/show.json/{id}'
        }
      });
      this.__R.addListener('success', function() {
        this.setStatus('info', this.tr('Operation completed.'));
      }, this);

      this.__R.addListener('postTaskSuccess', function() {
        var dialog = new atms.WinConfirmation(
            this.tr('Confirmation'),
            this.tr('Job added successfully'));
        this.getApplicationRoot().add(dialog);
        dialog.open();
      }, this);

      this.__R.addListener('updateDevsSuccess', function() {
        this.reload();
      }, this);

      this.__R.addListener('error', function(e) {
        var dialog = new atms.WinConfirmation(
            this.tr('Operation not possible!'),
            e.getData());
        this.getApplicationRoot().add(dialog);
        dialog.open();
      }, this);
    },

    _applyReadOnly: function(value, oldValue) {
      var tcm = this.getTable().getTableColumnModel();
      tcm.setColumnVisible(0, !value);
      tcm.setColumnVisible(13, !value);
    },

    _applyXcases: function(value, oldValue) {
      this.setHiddenQuery(value);
    }
  }
});
