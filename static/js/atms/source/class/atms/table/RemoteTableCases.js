qx.Class.define('atms.table.RemoteTableCases',
{
  extend: qx.ui.table.model.Remote,

  include: [qx.locale.MTranslation],

  properties:
    {
      test: {
        init:  null,
        nullable: true,
        event: 'changeTest'
      },

      user: {
        init:  null,
        nullable: true,
        event: 'changeUser'
      },

      param:
        {
          init:  null,
          check: 'String'
        },

      customQuery:
        {
          init: null,
          check: 'String'
        }
    },

  construct: function() {
    this.base(arguments);
    var colnames = {
      status: this.tr('Status'),
      user: this.tr('Assigned to'),
      name: this.tr('Case Name'),
      test: this.tr('Test'),
      expected_time: this.tr('Expected time'),
      result_time: this.tr('Result time'),
      weight: this.tr('Impact factor')
    };
    var col_keys = new Array();
    var col_values = new Array();

    for (var key in colnames) {
      col_keys.push(key);
      col_values.push(colnames[key]);
    }

    this.base(arguments);
    this.setColumns(col_values, col_keys);
    this.__colKeys = col_keys;

    this.setColumnSortable(0, true);

    this.setBlockSize(100);
    this.__setupResources();
  },

  members:
    {
      __colKeys: null,

      __setupResources: function() {
        this.__rows = new qx.io.rest.Resource({
          get: {
            method: 'GET',
            url: '/atms/manager/' +              'get_cases_count.json{param}' },
          getCases: {
            method: 'GET',
            url: '/atms/manager/' +              'get_cases.json/{param}' }
        });
        this.__rows.addListener('getSuccess', function(e) {
          this._onRowCountCompleted(e.getData().content);
        }, this);

        this.__rows.addListener('getCasesSuccess', function(e) {
          this._onLoadRowDataCompleted(e.getData().content);
        }, this);
      },

      __buildQuery: function() {
        var customQ = this.getCustomQuery();
        if (customQ) return customQ;
        var parameters = '';
        var test = this.getTest();
        var user = this.getUser();
        if (test) parameters += '&test=' + test + '&_all=1';
        if (user) parameters += '&user=' + user + '&_all=0';
        return parameters;
      },

      _loadRowCount: function() {
        this.__rows.get({
          param: '?' + this.__buildQuery()
        });
      },

      _onRowCountCompleted: function(result) {
        if (result != null) {
          this._onRowCountLoaded(result);
        }
      },

      _loadRowData: function(firstRow, lastRow) {
        var parameters = '?from=' + firstRow + '&to=' + lastRow;

        var sortIndex = this.getSortColumnIndex() == -1 ?
            'atms_cases.node|atms_cases.name' : 'atms_cases.' + this.__colKeys[this.getSortColumnIndex()];
        var sortOrder =  this.isSortAscending() ? 'asc' : 'desc';
        parameters += '&sortOrder=' + sortOrder + '&sortIndex=' + sortIndex;

        this.__rows.getCases({
          param: parameters + '&' + this.__buildQuery()
        });
      },

      _onLoadRowDataCompleted: function(result) {
        if (result != null) {
          this._onRowDataLoaded(result);
        }
      }
    }
});
