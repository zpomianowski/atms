/**
*@asset(atms/*)
*@asset(qx/icon/${qx.icontheme}/*)
*/

qx.Class.define('atms.table.TableUECap',
{
  extend: cosmetosafe.TableGeneric,

  construct: function(test_id, user_id) {
    qx.Class.include(qx.ui.table.Table, qx.ui.table.MTableContextMenu);
    this.base(arguments,
      'atms_uecap',
      '/atms/lipstein/tapi/atms_uecap/count',
      '/atms/lipstein/tapi/atms_uecap/data',
      {
        columns: {
          f_processed: this.tr('Done'), //0
          id: this.tr('Id'), //1,
          ref_cbs: this.tr('ref CBS'), //2
          f_sw_version: this.tr('Software version'), //3
          f_model: this.tr('Model'), //4
          f_company: this.tr('Company'), //5
          f_chipset: 'Chipset', //6
          f_imei: 'IMEI', //7
          created_by: this.tr('Created by'), //8
          created_on: this.tr('Created on'), //9
          modified_by: this.tr('Modified by'), //10
          modified_on: this.tr('Modified on'), //11
          f_category_lte: this.tr('UE category'), //12
          f_bands: this.tr('LTE bands'), //13
          f_ca_search_index: this.tr('CA combinations'), //14,
          f_pdn_type: this.tr('PDN type'), //15
          f_dl_mimo4l: 'MIMO DL 4 layers', //16
          f_dl_qam256: '256QAM DL', //17
          f_ul_qam64: '64QAM UL', //18
          f_uecap_size: this.tr('UECap size') //19
        },
        to_hide: [1, 6, 7, 8, 9, 10, 11, 14],
        col_size_hints: {
          0: 70,
          2: 70,
          3: 180,
          4: 180,
          5: 140,
          7: 180,
          8: 180,
          9: 180,
          10: 180,
          11: 180,
          16: 130,
          19: 100
        },
        renderer_map: {
          0: 'checkbox',
          1: 'id',
          19: 'bytes'
        }
      }
    );

    this.__setupResources();

    this.getTable().addListener('dblclick',  function(e) {
      this.__newOrEditUECap('update');
    },  this);
  },

  members: {
    __R: null,

    createMenu: function() {
      var mm = {};

      mm.butNew = new qx.ui.menu.Button(
        this.tr('New entry for UE Capabilites'),
        'atms/devinfo.png',  null,  null);
      mm.butViewDetails = new qx.ui.menu.Button(
        this.tr('Show details'),
        'atms/devinfo.png',  null,  null);
      mm.butViewAttachR = new qx.ui.menu.Button(
        this.tr('Attach Request as raw txt'),
        'atms/code_blue.png',  null,  null);
      mm.butViewRawUECap = new qx.ui.menu.Button(
        this.tr('UE Capabilites as raw txt'),
        'atms/code_blue.png',  null,  null);

      mm.butExport = new qx.ui.menu.Button(
        this.tr('Export view [*.xlsx]'),
        'atms/cloud-download.png', null, null);

      mm.butRebuildDB = new qx.ui.menu.Button(
        this.tr('Rebuild items in DB'),
        'atms/warning.png', null, null);

      mm.butViewPdmlAttachR = new qx.ui.menu.Button(
        this.tr('Attach Request as pdml'),
        'atms/code_blue.png',  null,  null);
      mm.butViewPdmlUECap = new qx.ui.menu.Button(
        this.tr('UE Capabilites as pdml'),
        'atms/code_blue.png',  null,  null);

      mm.butEdit = new qx.ui.menu.Button(
        this.tr('Edit entry of UE Capabilites'),
        'atms/edit.png',  null,  null);
      mm.butDel = new qx.ui.menu.Button(this.tr('Delete entries'),
        'atms/trash.png',  null,  null);

      mm.butViewDetails.addListener('execute', function() {
        this.__openSummary();
      }, this);

      mm.butViewAttachR.addListener('execute', function() {
        this.__openRawTxt('f_attach_hex_txtdump');
      }, this);

      mm.butViewRawUECap.addListener('execute', function() {
        this.__openRawTxt('f_uecap_hex_txtdump');
      }, this);

      mm.butViewPdmlAttachR.addListener('execute', function() {
        this.__openRawTxt('f_attach_hex_pdmldump');
      }, this);

      mm.butViewPdmlUECap.addListener('execute', function() {
        this.__openRawTxt('f_uecap_hex_pdmldump');
      }, this);

      mm.butExport.addListener('execute', function() {
        this.__export();
      }, this);

      mm.butRebuildDB.addListener('execute', function() {
        this.__rebuildDB();
      }, this);

      mm.butNew.addListener('execute', function() {
        this.__newOrEditUECap('create');
      }, this);

      mm.butEdit.addListener('execute', function() {
        this.__newOrEditUECap('update');
      }, this);

      mm.butDel.addListener('execute', function() {
        this.__delItems();
      }, this);

      var menu = new qx.ui.menu.Menu();
      menu.add(mm.butViewDetails);
      menu.add(new qx.ui.menu.Separator());

      var view_menu = new qx.ui.menu.Menu();
      view_menu.add(mm.butViewRawUECap);
      view_menu.add(mm.butViewAttachR);
      view_menu.add(new qx.ui.menu.Separator());
      view_menu.add(mm.butViewPdmlUECap);
      view_menu.add(mm.butViewPdmlAttachR);
      menu.add(new qx.ui.menu.Button(
        this.tr('Preview'),
        'atms/code_blue.png', null, view_menu));
      menu.add(new qx.ui.menu.Separator());
      menu.add(new qx.ui.menu.Button(
        this.tr('Compare'),
        'atms/statistics.png', null, this.__getChartMenu()));
      menu.add(new qx.ui.menu.Separator());
      menu.add(mm.butExport);
      menu.add(new qx.ui.menu.Separator());
      menu.add(mm.butRebuildDB);
      menu.add(new qx.ui.menu.Separator());
      menu.add(mm.butNew);
      menu.add(mm.butEdit);
      menu.add(new qx.ui.menu.Separator());
      menu.add(mm.butDel);
      menu.add(new qx.ui.menu.Separator());

      this.addListener('beforeContextmenuOpen', function() {
        // default disabled
        for (var key in mm) {
          mm[key].setEnabled(false);
        }

        var query = {};
        query.ids = this.getIds();
        query.rname = 'atms_uecap';
        var notempty = Boolean(query.ids.length);

        query = qx.data.marshal.Json.createModel(query);
        var json = qx.util.Serializer.toJson(query);

        var req = new qx.io.request.Xhr(
            '/atms/default/call/json/getperm/' +
            qx.util.Base64.encode(json));
        req.addListener('success', function(e) {
          var permmap = e.getTarget().getResponse();

          if (permmap.read) mm.butExport.setEnabled(true);
          if (notempty) {
            mm.butViewDetails.setEnabled(true);
            mm.butViewAttachR.setEnabled(true);
            mm.butViewRawUECap.setEnabled(true);
            mm.butViewPdmlAttachR.setEnabled(true);
            mm.butViewPdmlUECap.setEnabled(true);
          }

          if (permmap.create) {
            mm.butNew.setEnabled(true);
          }

          if (permmap.update && notempty) {
            mm.butEdit.setEnabled(true);
          }

          if (permmap.del && notempty) {
            mm.butDel.setEnabled(true);
            mm.butRebuildDB.setEnabled(true);
          }

        }, this);

        req.send();
      }, this);

      return menu;
    },

    cellRenderFactory: function(name) {
      var renderer = null;
      switch (name) {
        case 'id':
          renderer = new qx.ui.table.cellrenderer.Number();
          var nf = new qx.util.format.NumberFormat();
          nf.setGroupingUsed(false);
          renderer.setNumberFormat(nf);
        break;

        case 'bytes':
          renderer = new qx.ui.table.cellrenderer.Number();
          var nf = new qx.util.format.NumberFormat();
          nf.setPostfix(this.tr(' bytes'));
          nf.setGroupingUsed(false);
          renderer.setNumberFormat(nf);
          renderer.addBetweenCondition(
            'between', 1000, 1999, null, '#ff7200');
          renderer.addNumericCondition(
            '>=', 2000, null, 'red');
        break;

        case 'checkbox':
          renderer = new qx.ui.table.cellrenderer.Boolean();
          renderer._identifyImage = function(cellInfo) {
            var imageHints =
            {
              imageWidth: 16,
              imageHeight: 16
            };

            switch (cellInfo.value) {
              case true:
                imageHints.url = this.getIconTrue();
              break;

              case false:
                imageHints.url = this.getIconFalse();
              break;

              default:
                imageHints.url = this.getIconFalse();
              break;
            }

            return imageHints;
          };

          renderer.setIconTrue('atms/thumbs-o-up.png');
          renderer.setIconFalse('atms/thumbs-o-down.png');
        break;
      }
      return renderer || new qx.ui.table.cellrenderer.Default();
    },

    __openSummary: function() {
      var item = this.getFirstFromSelection();
      var widget = new atms.ViewHTMLueCap(item.id);
      var win = new atms.WinPanel(widget,
          this.tr('Details of ') + item.f_company + ' ' + item.f_model);
      qx.core.Init.getApplication().gDesk1.add(win);
      win.open();
    },

    __openRawTxt: function(field) {
      var item = this.getFirstFromSelection();
      var widget = new atms.editor.FullEditor(
        item.id, true, '/atms/lipstein/txt_api/' + field);
      var win = new atms.WinPanel(widget,
          this.tr('View') + ': ' + item.f_company + ' ' + item.f_model);
      qx.core.Init.getApplication().gDesk1.add(win);
      win.open();
    },

    __export: function() {
      var m = this.getModel();
      var visibleIds = this.getTable()
        .getTableColumnModel()
        .getVisibleColumns();

      var filter = {};
      filter['exportCols'] = [];
      for (var i = 0; i < m.getColumnCount(); ++i) {
        if (visibleIds.indexOf(i) > 0)
          filter['exportCols'].push(m.getColumnId(i).toString());
      }

      if (m.getQuery() != '')
          filter['csfilter'] = m.getSerializedQuery();
      filter = qx.data.marshal.Json.createModel(filter);

      window.open(
        '/atms/lipstein/tapi/atms_uecap/data.xls?' +
        qx.util.Serializer.toUriParameter(filter),
        '_self', false);
    },

    __rebuildDB: function() {
      var that = this;

      var caption = this.tr('Rebuild these items?');
      var desc = this.tr(
        'This action cannot be reverted! ' +
        'New indexes will be created, old will be discarded. ' +
        'Also new reports will be generated. Old will be remained.');
      var dialog = new atms.WinConfirmation(
          caption,
          desc,
          function() {
            that.__R.rebuild(null, { id: null });
          });

      this.getApplicationRoot().add(dialog);
      dialog.open();
    },

    __newOrEditUECap: function(mode) {
      var that = this;
      var widget = null;

      if (mode == 'update')
        this.getSelectedRows().forEach(function(item) {
          widget = new atms.FormUECap(item.id,  mode,
            function() {
              // win.close();
              jQuery('.flash').html(this.tr('Changes saved').toString());
              jQuery('.flash').show();
              that.reload();
            },  null);
        });

      if (mode == 'create')
        widget = new atms.FormUECap(null,  mode,
          function() {
            // win.close();
            jQuery('.flash').html(this.tr('Changes saved').toString());
            jQuery('.flash').show();
            that.reload();
          },  null);

      var win = new atms.WinPanel(widget);
      qx.core.Init.getApplication().gDesk1.add(win);
      win.open();
    },

    __delItems: function() {
      var that = this;
      var selectedRowData = [];
      this.getIds().forEach(function(index) {
        selectedRowData.push(index);
      }, this);

      var caption = selectedRowData.length > 1 ?
        this.tr('Delete these items?') : this.tr('Delete this item?');
      var desc = this.tr('This action cannot be undone!');
      var dialog = new atms.WinConfirmation(
          caption,
          desc,
          function() {
            selectedRowData.forEach(function(item) {
              that.__R.del({ id: item });
            });
          });

      this.getApplicationRoot().add(dialog);
      dialog.open();
    },

    __setupResources: function() {
      var api = {
        del: {
          method: 'DELETE',
          url: '/atms/lipstein/api/atms_uecap/{id}'
        },
        rebuild: {
          method: 'POST',
          url: '/atms/manager/task_api/parseUECap'
        }
      };
      this.__R = new qx.io.rest.Resource(api);

      this.__R.addListener('success', function() {
        this.reload();
        this.setStatus('info', this.tr('Operation completed.'));
      }, this);

      this.__R.addListener('error', function() {
        this.reload();
        this.setStatus('error', this.tr('Operation failed.'));
      }, this);
    },

    __getChartMenu: function() {
      var chart_menu = new qx.ui.menu.Menu();

      var data = new qx.data.Array([
        [this.tr('Max vendors class'), 'max_class']
      ]);

      data.forEach(function(d) {
        var but = new qx.ui.menu.Button(d[0],
          'atms/statistics.png', null, null);
        chart_menu.add(but);
        but.addListener('execute', function() {
          this.__buildChart(d);
        }, this);
      }, this);

      return chart_menu;
    },

    __buildChart: function(item) {
      var title = this.tr('UECaps') + ' - ' + item[0];
      var widget = null;

      switch (item[1]) {
        case 'max_class':
          var widget = new atms.Chart({
            layeredBars: {
              args: ['group', 'linear'],
              url: '/atms/lipstein/call/json/chart_max_class',
              params: {}
            }
          });
        break;
      }

      var win = new atms.WinPanel(widget, title);
      qx.core.Init.getApplication().gDesk1.add(win);
      win.open();
    }
  }
});
