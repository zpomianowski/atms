qx.Class.define('atms.table.RemoteTableQuiz',
{
  extend: qx.ui.table.model.Remote,

  include: [qx.locale.MTranslation],

  construct: function() {
    this.base(arguments);
    var colnames = {
      id: this.tr('ID'),
      prev: this.tr('Preview')
    };
    var col_keys = new Array();
    var col_values = new Array();

    for (var key in colnames) {
      col_keys.push(key);
      col_values.push(colnames[key]);
    }

    this.base(arguments);
    this.setColumns(col_values, col_keys);
    this.__colKeys = col_keys;

    this.setColumnSortable(0, true);

    this.setBlockSize(100);
    this.__setupResources();
  },

  members:
    {
      __colKeys: null,

      __setupResources: function() {
        this.__rows = new qx.io.rest.Resource({
          get:   {
            method: 'GET',
            url: '/atms/manager/' +              '../quiz_manager/get_quizq_count.json/{param}'
          },
          getQuiz:   {
            method: 'GET',
            url: '/atms/manager/' +              '../quiz_manager/get_quizq.json/{param}'
          }
        });
        this.__rows.addListener('getSuccess', function(e) {
          this._onRowCountCompleted(e.getData().content);
        }, this);

        this.__rows.addListener('getQuizSuccess', function(e) {
          this._onLoadRowDataCompleted(e.getData().content);
        }, this);
      },

      _loadRowCount: function() {
        var parameters = '?';
        this.__rows.get({
          param: parameters
        });
      },

      _onRowCountCompleted: function(result) {
        if (result != null) {
          this._onRowCountLoaded(result);
        }
      },

      _loadRowData: function(firstRow, lastRow) {
        var parameters = '?from=' + firstRow + '&to=' + lastRow;

        var sortIndex = this.getSortColumnIndex() == -1 ?
            'null' : this.__colKeys[this.getSortColumnIndex()];
        var sortOrder =  this.isSortAscending() ? 'asc' : 'desc';
        parameters += '&sortOrder=' + sortOrder + '&sortIndex=' + sortIndex;

        this.__rows.getQuiz({
          param: parameters
        });
      },

      _onLoadRowDataCompleted: function(result) {
        if (result != null) {
          // Apply it to the model - the method "_onRowDataLoaded" has to be called
          this._onRowDataLoaded(result);
        }
      }
    }
});
