/**
*@asset(atms/*)
*@asset(qx/icon/${qx.icontheme}/*)
*/

qx.Class.define('atms.table.TableWiFiCap',
{
  extend: cosmetosafe.TableGeneric,

  construct: function(test_id, user_id) {
    qx.Class.include(qx.ui.table.Table, qx.ui.table.MTableContextMenu);
    this.base(arguments,
      'atms_wificap',
      '/atms/lubelak/tapi/atms_wificap/count',
      '/atms/lubelak/tapi/atms_wificap/data',
      {
        columns: {
          f_processed: this.tr('Done'), //0
          id: this.tr('Id'), //1,
          ref_cbs: this.tr('ref CBS'), //2
          f_sw_version: this.tr('Software version'), //3
          f_model: this.tr('Model'), //4
          f_company: this.tr('Company'), //5
          f_chipset: 'Chipset', //6
          f_sn: this.tr('Serial no'), //7
          f_cat: this.tr('Category'), //8
          f_maxrate24: this.tr('Max 2.4GHz'), //9
          f_maxrate5: this.tr('Max 5GHz'), //10
          created_by: this.tr('Created by'), //11
          created_on: this.tr('Created on'), //12
          modified_by: this.tr('Modified by'), //13
          modified_on: this.tr('Modified on') //14
        },
        to_hide: [1, 3, 6, 7, 11, 12, 13, 14],
        col_size_hints: {
          0: 70,
          2: 70
        },
        renderer_map: {
          0: 'checkbox',
          1: 'id',
          9: 'bitrate',
          10: 'bitrate'
        }
      }
    );

    this.__setupResources();

    this.getTable().addListener('dblclick',  function(e) {
      this.__newOrEditWiFiCap('update');
    },  this);
  },

  members: {
    __R: null,

    createMenu: function() {
      var mm = {};

      mm.butNew = new qx.ui.menu.Button(
        this.tr('New entry for WiFi Capabilites'),
        'atms/devinfo.png',  null,  null);
      mm.butExport = new qx.ui.menu.Button(
        this.tr('Export view [*.xlsx]'),
        'atms/cloud-download.png', null, null);
      mm.butEdit = new qx.ui.menu.Button(
        this.tr('Edit entry of WiFi Capabilites'),
        'atms/edit.png',  null,  null);
      mm.butDel = new qx.ui.menu.Button(this.tr('Delete entries'),
        'atms/trash.png',  null,  null);
      mm.butViewRawCaps = new qx.ui.menu.Button(
        this.tr('View Capabilities as raw json'),
        'atms/code_blue.png',  null,  null);
      mm.butRebuildDB = new qx.ui.menu.Button(
        this.tr('Rebuild items in DB'),
        'atms/warning.png', null, null);

      mm.butRebuildDB.addListener('execute', function() {
        this.__rebuildDB();
      }, this);

      mm.butExport.addListener('execute', function() {
        this.__export();
      }, this);

      mm.butNew.addListener('execute', function() {
        this.__newOrEditWiFiCap('create');
      }, this);

      mm.butEdit.addListener('execute', function() {
        this.__newOrEditWiFiCap('update');
      }, this);

      mm.butDel.addListener('execute', function() {
        this.__delItems();
      }, this);

      mm.butViewRawCaps.addListener('execute', function() {
        this.__openRawTxt('f_caps_json');
      }, this);

      var view_menu = new qx.ui.menu.Menu();
      view_menu.add(mm.butViewRawCaps);

      var menu = new qx.ui.menu.Menu();
      menu.add(mm.butExport);
      menu.add(new qx.ui.menu.Separator());
      menu.add(new qx.ui.menu.Button(
        this.tr('Preview'),
        'atms/code_blue.png', null, view_menu));
      menu.add(new qx.ui.menu.Separator());
      menu.add(new qx.ui.menu.Button(
        this.tr('Compare'),
        'atms/statistics.png', null, this.__getChartMenu()));
      menu.add(new qx.ui.menu.Separator());
      menu.add(mm.butNew);
      menu.add(mm.butEdit);
      menu.add(new qx.ui.menu.Separator());
      menu.add(mm.butRebuildDB);
      menu.add(new qx.ui.menu.Separator());
      menu.add(mm.butDel);
      menu.add(new qx.ui.menu.Separator());

      this.addListener('beforeContextmenuOpen', function() {
        // default disabled
        for (var key in mm) {
          mm[key].setEnabled(false);
        }

        var query = {};
        query.ids = this.getIds();
        query.rname = 'atms_wificap';
        var notempty = Boolean(query.ids.length);

        query = qx.data.marshal.Json.createModel(query);
        var json = qx.util.Serializer.toJson(query);

        var req = new qx.io.request.Xhr(
            '/atms/default/call/json/getperm/' +
            qx.util.Base64.encode(json));
        req.addListener('success', function(e) {
          var permmap = e.getTarget().getResponse();

          if (permmap.read) {
            mm.butExport.setEnabled(true);
            mm.butViewRawCaps.setEnabled(true);
          }

          if (notempty) {}

          if (permmap.create) {
            mm.butNew.setEnabled(true);
            mm.butRebuildDB.setEnabled(true);
          }

          if (permmap.update && notempty) {
            mm.butEdit.setEnabled(true);
          }

          if (permmap.del && notempty) {
            mm.butDel.setEnabled(true);
          }

        }, this);

        req.send();
      }, this);

      return menu;
    },

    __openRawTxt: function(field) {
      var item = this.getFirstFromSelection();
      var widget = new atms.editor.FullEditor(
        item.id, true, '/atms/lubelak/txt_api/' + field);
      var win = new atms.WinPanel(widget,
          this.tr('View') + ': ' + item.f_company + ' ' + item.f_model);
      qx.core.Init.getApplication().gDesk1.add(win);
      win.open();
    },

    __rebuildDB: function() {
      var that = this;
      var counter = 0;
      this.getIds().forEach(function(index) { counter++; });

      var caption = counter > 1 ?
        this.tr('Rebuild these items?') : this.tr('Rebuild this item?');
      var desc = this.tr(
        'This action cannot be reverted! ' +
        'New indexes will be created, old will be discarded. ' +
        'Also new reports will be generated. Old will be remained.');
      var dialog = new atms.WinConfirmation(
          caption,
          desc,
          function() {
            that.getSelectedRows().forEach(function(item) {
              that.__R.rebuild(null, { id: null });
            });
          });

      this.getApplicationRoot().add(dialog);
      dialog.open();
    },

    cellRenderFactory: function(name) {
      var renderer = null;
      switch (name) {
        case 'id':
          renderer = new qx.ui.table.cellrenderer.Number();
          var nf = new qx.util.format.NumberFormat();
          nf.setGroupingUsed(false);
          renderer.setNumberFormat(nf);
        break;

        case 'bitrate':
          renderer = new qx.ui.table.cellrenderer.Number;
          var nf = new qx.util.format.NumberFormat();
          nf.setPostfix(' Mb/s');
          renderer.setNumberFormat(nf);
        break;

        case 'checkbox':
          renderer = new qx.ui.table.cellrenderer.Boolean();
          renderer._identifyImage = function(cellInfo) {
            var imageHints =
            {
              imageWidth: 16,
              imageHeight: 16
            };

            switch (cellInfo.value) {
              case true:
                imageHints.url = this.getIconTrue();
              break;

              case false:
                imageHints.url = this.getIconFalse();
              break;

              default:
                imageHints.url = this.getIconFalse();
              break;
            }

            return imageHints;
          };

          renderer.setIconTrue('atms/thumbs-o-up.png');
          renderer.setIconFalse('atms/thumbs-o-down.png');
        break;
      }
      return renderer || new qx.ui.table.cellrenderer.Default();
    },

    __export: function() {
      var m = this.getModel();
      var visibleIds = this.getTable()
        .getTableColumnModel()
        .getVisibleColumns();

      var filter = {};
      filter['exportCols'] = [];
      for (var i = 0; i < m.getColumnCount(); ++i) {
        if (visibleIds.indexOf(i) > 0)
          filter['exportCols'].push(m.getColumnId(i).toString());
      }

      if (m.getQuery() != '')
          filter['csfilter'] = m.getSerializedQuery();
      filter = qx.data.marshal.Json.createModel(filter);

      window.open(
        '/atms/lubelak/tapi/atms_wificap/data.xls?' +
        qx.util.Serializer.toUriParameter(filter),
        '_self', false);
    },

    __newOrEditWiFiCap: function(mode) {
      var that = this;
      var widget = null;

      if (mode == 'update')
        this.getSelectedRows().forEach(function(item) {
          widget = new atms.FormWiFiCap(item.id,  mode,
            function() {
              // win.close();
              jQuery('.flash').html(this.tr('Changes saved').toString());
              jQuery('.flash').show();
              that.reload();
            },  null);
        });

      if (mode == 'create')
        widget = new atms.FormWiFiCap(null,  mode,
          function() {
            // win.close();
            jQuery('.flash').html(this.tr('Changes saved').toString());
            jQuery('.flash').show();
            that.reload();
          },  null);

      var win = new atms.WinPanel(widget);
      qx.core.Init.getApplication().gDesk1.add(win);
      win.open();
    },

    __delItems: function() {
      var that = this;
      var selectedRowData = [];
      this.getIds().forEach(function(index) {
        selectedRowData.push(index);
      }, this);

      var caption = selectedRowData.length > 1 ?
        this.tr('Delete these items?') : this.tr('Delete this item?');
      var desc = this.tr('This action cannot be undone!');
      var dialog = new atms.WinConfirmation(
          caption,
          desc,
          function() {
            selectedRowData.forEach(function(item) {
              that.__R.del({ id: item });
            });
          });

      this.getApplicationRoot().add(dialog);
      dialog.open();
    },

    __setupResources: function() {
      var api = {
        del: {
          method: 'DELETE',
          url: '/atms/lubelak/api/atms_wificap/{id}'
        },
        rebuild: {
          method: 'POST',
          url: '/atms/manager/task_api/parseWiFiCap'
        }
      };
      this.__R = new qx.io.rest.Resource(api);

      this.__R.addListener('success', function() {
        this.reload();
        this.setStatus('info', this.tr('Operation completed.'));
      }, this);

      this.__R.addListener('error', function() {
        this.reload();
        this.setStatus('error', this.tr('Operation failed.'));
      }, this);
    },

    __getChartMenu: function() {
      var chart_menu = new qx.ui.menu.Menu();

      var data = new qx.data.Array([
        [this.tr('Classes'), 'classes'],
        [this.tr('Rates'), 'rates'],
      ]);

      data.forEach(function(d) {
        var but = new qx.ui.menu.Button(d[0],
          'atms/statistics.png', null, null);
        chart_menu.add(but);
        but.addListener('execute', function() {
          this.__buildChart(d);
        }, this);
      }, this);

      return chart_menu;
    },

    __buildChart: function(item) {
      var title = this.tr('WifiCaps') + ' - ' + item[0];
      var widget = null;

      switch (item[1]) {
        case 'classes':
          var widget = new atms.Chart({
            layeredBars: {
              args: ['group', 'linear'],
              url: '/atms/lubelak/call/json/chart_classes',
              params: {}
            }
          });
        break;

        case 'rates':
          var widget = new atms.Chart({
            layeredBars: {
              args: ['group', 'linear'],
              url: '/atms/lubelak/call/json/chart_rates',
              params: {}
            }
          });
        break;
      }

      var win = new atms.WinPanel(widget, title);
      qx.core.Init.getApplication().gDesk1.add(win);
      win.open();
    }
  }
});
