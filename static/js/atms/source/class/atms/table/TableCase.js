/**
*@asset(atms/*)
*@asset(qx/icon/${qx.icontheme}/*)
*/

qx.Class.define('atms.table.TableCase',
{
  extend: cosmetosafe.TableGeneric,

  construct: function(test_id, user_id) {
    qx.Class.include(qx.ui.table.Table, qx.ui.table.MTableContextMenu);
    this.base(arguments,
      'atms_cases',
      '/atms/manager/tapi/atms_cases/count',
      '/atms/manager/tapi/atms_cases/data',
      {
        columns: {
          id: this.tr('Id'), //0
          status: this.tr('Status'), //1
          user: this.tr('Responsible user'), //2
          name: this.tr('Case name'), //3
          auto_funcs: this.tr('Auto functions'), //4
          test: this.tr('Test name'), //5
          result_time: this.tr('Result time'), //6
          expected_time: this.tr('Expected time'), //7
          weight: this.tr('Impact factor'), //8
          auto_type: this.tr('Auto template'), //9
          is_automated: this.tr('Is automated'), //10
          auto_script: this.tr('Auto script assigned'), //11
          auto_bin: this.tr('Auto binary (BUT)'), //12
          auto_report: this.tr('Auto report'), //13
          auto_devs: this.tr('Engaged devices'), //14
          created_by: this.tr('Created by'), //15
          created_on: this.tr('Created on') //16
        },
        to_hide: [0, 2, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16],
        col_size_hints: {
          1: 100,
          2: 180,
          3: 220,
          15: 180,
          16: 150
        },
        renderer_map: {
          0: 'id',
          1: 'status',
          5: 'time',
          6: 'time',
          10: 'bool',
          11: 'bool',
          12: 'bool',
          13: 'bool'
        }
      }
    );

    // context menu
    for (var i = 0; i < this.getTable()
      .getTableColumnModel()
      .getOverallColumnCount(); i++) {
      this.getTable().setContextMenuHandler(
        i, this._contextMenuHandler, this);
    }

    this.__setupResources();

    this.__clipboard = { hint: null,  content: null };

    var ids = {};
    if (test_id) ids.test = test_id;
    if (user_id) ids.user = user_id;
    this.setHiddenQuery(ids);

    if (user_id == 'None') this.getTable()
      .getTableColumnModel()
      .setColumnVisible(2, true);

    this.getTable().addListener('dblclick',  function(e) {
      this.__newOrEditCase('do');
    },  this);

    this.getTable().setDraggable(true);
    this.getTable().setDroppable(true);
    this.getTable().setFocusCellOnPointerMove(true);
    this.getTable().addListener('dragstart',  this._handleDragStart,  this);
    this.getTable().addListener('droprequest',  this._handleDropRequest,  this);
    this.getTable().addListener('drop',  this._handleDrop,  this);
    this.getTable().addListener('drag',  this.__onDrag,  this);
    this.getTable().addListener('dragend',  this.__onDragEnd,  this);

    this.__dragFeedback = new atms.DragFeedback(this.tr('Assign cases'));
    qx.core.Init.getApplication().getRoot().add(this.__dragFeedback);
  },

  members: {
    __dragFeedback: null,
    __clipboard: null,

    createMenu: function() {
      var mm = {};
      var menu = new qx.ui.menu.Menu();

      mm.butNew = new qx.ui.menu.Button(this.tr('New case'),
        'atms/sc.png',  null,  null);
      mm.butEdit = new qx.ui.menu.Button(this.tr('Edit cases'),
        'atms/edit.png',  null,  null);
      mm.butCopy = new qx.ui.menu.Button(this.tr('Copy cases'),
        'atms/copy.png',  null,  null);
      mm.butPaste = new qx.ui.menu.Button(this.tr('Paste cases'),
        'atms/paste.png',  null,  null);
      mm.butDel = new qx.ui.menu.Button(this.tr('Delete cases'),
        'atms/trash.png',  null,  null);

      mm.butNew.addListener('execute', function() {
        this.__newOrEditCase('create');
      }, this);

      mm.butEdit.addListener('execute', function() {
        this.__newOrEditCase('update');
      }, this);

      mm.butCopy.addListener('execute', function() {
        this.__copyItems();
      }, this);

      mm.butPaste.addListener('execute', function() {
        this.__pasteItems();
      }, this);

      mm.butDel.addListener('execute', function() {
        this.__deleteCase();
      }, this);

      menu.add(mm.butNew);
      menu.add(mm.butEdit);
      menu.add(new qx.ui.menu.Separator());
      menu.add(mm.butCopy);
      menu.add(mm.butPaste);
      menu.add(new qx.ui.menu.Separator());
      menu.add(mm.butDel);
      menu.add(new qx.ui.menu.Separator());

      this.addListener('beforeContextmenuOpen', function() {
        // default disabled
        for (var key in mm) {
          mm[key].setEnabled(false);
        }

        var query = {};
        query.ids = this.getIds();
        query.rname = 'atms_cases';
        var notempty = Boolean(query.ids.length);

        query = qx.data.marshal.Json.createModel(query);
        var json = qx.util.Serializer.toJson(query);

        var req = new qx.io.request.Xhr(
            '/atms/default/call/json/getperm/' +
            qx.util.Base64.encode(json));
        req.addListener('success', function(e) {
          var permmap = e.getTarget().getResponse();

          if (permmap.create) {
            mm.butNew.setEnabled(true);
            mm.butCopy.setEnabled(true && notempty);
            mm.butPaste.setEnabled(true && this.__clipboard['content'] != null);
          }

          if (permmap.update && notempty) {
            mm.butEdit.setEnabled(true);
          }

          if (permmap.del && notempty) {
            mm.butDel.setEnabled(true);
          }

        }, this);

        req.send();
      }, this);

      return menu;
    },

    _contextMenuHandler: function(col, row, table, dataModel, contextMenu) {
      var that = this;
      var store_rest_uri = false;
      var store = null;
      switch (col) {
        case 1:
          store =  this.getStaticStore('status');
        break;

        case 2:
          store = true;
          store_rest_uri = '/atms/manager/call/json/store/atms_cases/user';
        break;

        case 5:
          store = true;
          store_rest_uri = '/atms/manager/call/json/store/atms_cases/test';
        break;
      }
      if (store) {
        store = qx.data.marshal.Json.createModel(store);
        var colID = that.getModel().getColumnId(col);
        var icon = 'atms/' + colID + '.png';
        contextMenu.add(
          new qx.ui.menu.Button(
            that.tr('Change selected to'),
            icon,
            null, (function() {
              var menu = new qx.ui.menu.Menu();
              var menuEntry;

              if (!store_rest_uri) {
                store.forEach(function(item) {
                  var label = item.getLabel();
                  menuEntry = new qx.ui.menu.Button(label);
                  menuEntry.setUserData('value', item.getId());
                  menuEntry.addListener('execute', (function(menuEntry) {
                    return function() {
                      var value = menuEntry.getUserData('value');
                      that.getTable()
                        .getSelectionModel()
                        .iterateSelection(function(index) {
                          var item = that.getModel().getRowData(index);
                          that._dataEdited(index, col, value, null, item);
                        });
                    };
                  })(menuEntry));

                  menu.add(menuEntry);
                });
              } else {
                var editor = new cosmetosafe.MenuMultiSelectBox();
                editor.setREST(store_rest_uri);
                editor.addListener('execute', function(e) {
                  var value = editor.getValue().getItem(0).getId();
                  that.getTable()
                    .getSelectionModel()
                    .iterateSelection(function(index) {
                      var item = that.getModel().getRowData(index);
                      that._dataEdited(index, col, value, null, item);
                    });
                });

                menu.add(editor);
              }

              return menu;
            })()));
      }

      contextMenu.setOffsetTop(-32);

      return true;
    },

    cellRenderFactory: function(name) {
      var renderer = null;
      switch (name) {
        case 'id':
          renderer = new qx.ui.table.cellrenderer.Number();
          var nf = new qx.util.format.NumberFormat();
          nf.setGroupingUsed(false);
          renderer.setNumberFormat(nf);
        break;

        case 'bool':
          renderer = new qx.ui.table.cellrenderer.Boolean();
        break;

        case 'status':
          this.addStaticStore(name, [
            { id: 'new', label: this.tr('New') },
            { id: 'pending', label: this.tr('Pending') },
            { id: 'aborted', label: this.tr('Aborted') },
            { id: 'impossible', label: this.tr('Impossible') },
            { id: 'failed', label: this.tr('Failed') },
            { id: 'passed', label: this.tr('Passed') }
          ]);
          renderer = new atms.RendererCustom();
          renderer.setReplaceMap(
              this.getReplaceMap(name));
          renderer.addReversedReplaceMap();
        break;

        case 'time':
          renderer = new qx.ui.table.cellrenderer.Number();
          var format = new atms.TimeFormat();
          renderer.setNumberFormat(format);
        break;
      }
      return renderer || new qx.ui.table.cellrenderer.Default();
    },

    _dataEdited: function(row, col, value, oldValue, item, e) {
      var action = null;
      var params = {};
      var content = {};
      switch (col) {
        case 2:
          params['cases'] = item.id;
          params[this.getModel().getColumnId(col)] = value;
          action = 'assign';
        break;

        default:
          params['id'] = item.id;
          content[this.getModel().getColumnId(col)] = value;
          action = 'update';
      }
      var model = qx.data.marshal.Json.createModel(params);
      var uriParams = qx.util.Serializer.toUriParameter(model);
      this.__R[action]({
        params: '?' + uriParams
      }, { content: qx.util.Serializer.toJson([content]) });
    },

    __setupResources: function() {
      var stdroute = '/atms/manager/api/atms_cases';
      this.__R = new qx.io.rest.Resource({
        assign: {
          method: 'GET',
          url: '/atms/manager/assign_cases{params}'
        },
        update: {
            method: 'PUT',
            url: stdroute + '{params}' },
        deleteCase: {
            method: 'DELETE',
            url: stdroute + '{params}' },
        pasteCases: {
          method: 'POST',
          url: '/atms/manager/copy_cases' }
      });
      this.__R.addListener('success', function() {
        this.reload();
        this.setStatus('info', this.tr('Operation completed.'));
      }, this);

      this.__R.addListener('error', function() {
        this.reload();
        this.setStatus('error', this.tr('Operation failed.'));
      }, this);
    },

    __newOrEditCase: function(mode) {
      var that = this;
      this.getSelectedRows().forEach(function(item) {
        var data = qx.lang.Object.clone(item);
        var widget = new atms.Form('atms_cases',  { id: item.id },  mode,
          function() {
            that.reload();
          },  null);

        var win = new atms.WinPanel(widget);
        qx.core.Init.getApplication().gDesk1.add(win);
        win.open();
      });

      if (this.getTable().getSelectionModel().isSelectionEmpty()) {
        var widget = new atms.Form('atms_cases',  {},  mode,
          function() {
            that.reload();
          },  null);

        var win = new atms.WinPanel(widget);
        qx.core.Init.getApplication().gDesk1.add(win);
        win.open();
      }
    },

    __copyItems: function(e) {
      this.__clipboard['hint'] = 'copy';
      this.__clipboard['content'] = [];
      this.getIds().forEach(function(index) {
        this.__clipboard['content'].push(index);
      },  this);
    },

    __pasteItems: function(e) {
      var that = this;
      var caption = this.__clipboard['content'].length > 1 ?
        this.tr('Copy these items?') : this.tr('Copy this item?');
      var desc = this.tr('This action will create new cases based on currently selected. They are set as not done.');
      var dialog = new atms.WinConfirmation(
          caption,
          desc,  function() {
            that.__R.pasteCases(null, { content: that.__clipboard['content'] });
          },  null);

      this.getApplicationRoot().add(dialog);
      dialog.open();
    },

    __deleteCase: function() {
      var that = this;
      var selectedRowData = [];
      this.getIds().forEach(function(index) {
        selectedRowData.push(index);
      }, this);

      var caption = selectedRowData.length > 1 ?
        this.tr('Delete these items?') : this.tr('Delete this item?');
      var desc = this.tr('This action cannot be undone!');
      var dialog = new atms.WinConfirmation(
          caption,
          desc,
          function() {
            selectedRowData.forEach(function(item) {
              that.__R.deleteCase({
                params: '?id=' + item
              });
            });
          }
      );
      this.getApplicationRoot().add(dialog);
      dialog.open();
    },

    _handleDragStart: function(e) {
      e.addType('sender');
      e.addType('target');
      e.addType('cases');
      e.addAction('move');

      var that = this;
      this.getTable().getSelectionModel().iterateSelection(function(index) {
        var item = that.getTable().getTableModel().getRowData(index);
        var itemWidget = new qx.ui.basic.Atom(
            item.name,
            'atms/sc.png');
        itemWidget.setBackgroundColor('background-selected');
        itemWidget.setTextColor('white');
        itemWidget.setPadding([0,  4,  0,  24]);
        that.__dragFeedback.addDragChild(itemWidget);
      });
    },

    __onDrag: function(e) {
      this.__dragFeedback.setDomPosition(
        e.getDocumentLeft() + 15,  e.getDocumentTop() + 15);
    },

    __onDragEnd: function(e) {
      this.__dragFeedback.setDomPosition(-1000,  -1000);
      this.__dragFeedback.purgeChildren();
    },

    _handleDropRequest: function(e) {
      e.addData('sender',  this);
      e.addData('target',  e.getRelatedTarget());
      e.addData('cases',  this.getTable().getSelectionModel());
    },

    _handleDrop: function(e) {
      var that = this;
      var sender = e.getData('sender');
      var index = this.getTable().getFocusedRow();
      var icase  = this.getModel().getRowData(index);
      if (sender instanceof atms.UserList) {
        var user = e.getData('users').getItem(0).getId();
        that._modifyCell(1,  index,  user,  icase.user,  that);
      }

      if (sender instanceof atms.Tree) {
        var test = this.getModel().getTest();
        var user = this.getModel().getUser();
        var param = e.getData('tree-items-uri');
        var caption = this.tr('Add new cases for this group of cases');
        if (test) param += '&test=' + test;
        if (user) param += '&user=' + user;
        var desc = this.tr('This action will create a set of cases based on templates and will assign them to this particular test.');
        var dialog = new atms.WinConfirmation(
          caption,
          desc,  function() {
            var req = new qx.io.request.Xhr('/atms/manager/create_from_nodes?' + param);
            req.setTimeout(2400000);
            req.addListener('success',  function(e) {
              that.getModel().reloadData();
              jQuery('.flash').html(that.tr('Cases created for test').toString() + ' ' + test.name);
              jQuery('.flash').show();
            },  this);

            req.send();
          },  null);

        this.getApplicationRoot().add(dialog);
        dialog.open();
      }

      if (sender instanceof atms.table.TableCase) {
        if (sender === this) return;
        var test = this.getModel().getTest();
        var user = this.getModel().getUser();
        param = '';
        if (test) param += '&test=' + test;
        if (user) param += '&user=' + user;
        e.getData('cases').iterateSelection(function(index) {
          param += '&cases=' + sender.getModel().getRowData(index).id;
        });

        var req = new qx.io.request.Xhr('/atms/manager/assign_cases?' + param);
        req.setTimeout(2400000);
        req.addListener('success',  function(e) {
          that.getModel().reloadData();
          sender.getModel().reloadData();
          jQuery('.flash').html(that.tr('Cases moved').toString());
          jQuery('.flash').show();
        });

        req.send();
      }
    }
  }
});
