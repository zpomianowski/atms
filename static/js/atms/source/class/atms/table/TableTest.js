/**
*@asset(atms/*)
*@asset(qx/icon/${qx.icontheme}/*)
*/

qx.Class.define('atms.table.TableTest',
{
  extend: cosmetosafe.TableGeneric,

  construct: function() {
    qx.Class.include(qx.ui.table.Table, qx.ui.table.MTableContextMenu);
    this.base(arguments,
      'atms_tests',
      '/atms/manager/tapi/atms_tests/count',
      '/atms/manager/tapi/atms_tests/data',
      {
        columns: {
          id: this.tr('Id'), //0
          name: this.tr('Name'), //1
          status: this.tr('Status'), //2
          start: this.tr('Start'), //3
          duration: this.tr('Duration'), //4
          stop: this.tr('Stop'), //5
          modified_on: this.tr('Modified on'), //6
          modified_by: this.tr('Modified by'), //7
          created_on: this.tr('Created on'), //8
          created_by: this.tr('Created by') //9
        },
        to_hide: [0, 3, 4, 5, 6, 7, 8, 9],
        renderer_map: {
          0: 'id',
          2: 'status'
        }
      },
      {
        tableColumnModel: function(obj) {
          return new qx.ui.table.columnmodel.Resize(obj);
        }
      }
    );
    this.__setupResources();

    this.getTable().setDraggable(true);
    this.getTable().setDroppable(true);
    this.getTable().setFocusCellOnPointerMove(true);
    this.getTable().addListener('dragstart', this._handleDragStart, this);
    this.getTable().addListener('droprequest', this._handleDropRequest, this);
    this.getTable().addListener('drop', this._handleDrop, this);
    this.getTable().addListener('drag', this.__onDrag, this);
    this.getTable().addListener('dragend', this.__onDragEnd, this);
    this.__dragFeedback = new atms.DragFeedback(this.tr('Assign test to...'));
    qx.core.Init.getApplication().getRoot().add(this.__dragFeedback);
  },

  members: {
    __R: null,
    __dragFeedback: null,

    createMenu: function() {
      var mm = {};
      var menu = new qx.ui.menu.Menu();

      mm.refreshButton = new qx.ui.menu.Button(
        this.tr('Refresh'), 'atms/refresh.png');
      mm.newCaseButton = new qx.ui.menu.Button(
        this.tr('New case'), 'atms/sc.png');
      mm.newTestButton = new qx.ui.menu.Button(
        this.tr('New test'), 'atms/calendar.png');
      mm.editTestButton = new qx.ui.menu.Button(
        this.tr('Edit/View') + ' test', 'atms/edit.png');
      mm.openCaseManagerButton = new qx.ui.menu.Button(
        this.tr('Open case manager'), 'atms/cases.png');
      mm.openLooseCaseManagerButton = new qx.ui.menu.Button(
        this.tr('Open unassigned cases'), 'atms/cases.png');
      mm.delButton = new qx.ui.menu.Button(
        this.tr('Delete'), 'atms/trash.png');

      menu.add(mm.newTestButton);
      menu.add(mm.editTestButton);
      menu.add(new qx.ui.menu.Separator());
      menu.add(mm.openCaseManagerButton);
      menu.add(mm.openLooseCaseManagerButton);
      menu.add(new qx.ui.menu.Separator());
      menu.add(mm.newCaseButton);
      menu.add(new qx.ui.menu.Separator());
      menu.add(mm.delButton);
      menu.add(new qx.ui.menu.Separator());

      mm.newTestButton.addListener('execute', function() {
        this.__newTest('create');
      }, this);

      mm.editTestButton.addListener('execute', function() {
        this.__newTest('update');
      }, this);

      mm.openCaseManagerButton.addListener('execute', function() {
        this.__openCaseManager();
      }, this);

      mm.openLooseCaseManagerButton.addListener('execute', function() {
        this.__openCaseManager('loose');
      }, this);

      mm.newCaseButton.addListener('execute', function() {
        this.__newCase();
      }, this);

      mm.delButton.addListener('execute', function() {
        this.__deleteTests();
      }, this);

      this.addListener('beforeContextmenuOpen', function() {
        // default disabled
        for (var key in mm) {
          mm[key].setEnabled(false);
        }

        var query = {};
        query.ids = this.getIds();
        query.rname = 'atms_tests';
        var notempty = Boolean(query.ids.length);

        query = qx.data.marshal.Json.createModel(query);
        var json = qx.util.Serializer.toJson(query);

        var req = new qx.io.request.Xhr(
            '/atms/default/call/json/getperm/' +
            qx.util.Base64.encode(json));
        req.addListener('success', function(e) {
          var permmap = e.getTarget().getResponse();

          if (permmap.create) {
            mm.newTestButton.setEnabled(true);
          }

          if (permmap.update && notempty) {
            mm.newCaseButton.setEnabled(true);
            mm.editTestButton.setEnabled(true);
            mm.openLooseCaseManagerButton.setEnabled(true);
            mm.openCaseManagerButton.setEnabled(true);
          }

          if (permmap.del && notempty) {
            mm.delButton.setEnabled(true);
          }
        }, this);

        req.send();
      }, this);

      return menu;
    },

    cellRenderFactory: function(name) {
      var renderer = null;
      switch (name) {
        case 'id':
          renderer = new qx.ui.table.cellrenderer.Number();
          var nf = new qx.util.format.NumberFormat();
          nf.setGroupingUsed(false);
          renderer.setNumberFormat(nf);
        break;

        case 'status':
          this.addStaticStore(name, [
            { id: 'pending', label: this.tr('Pending') },
            { id: 'waiting', label: this.tr('Waiting') },
            { id: 'aborted', label: this.tr('Aborted') },
            { id: 'finished', label: this.tr('Finished') }
          ]);
          renderer = new atms.RendererCustom();
          renderer.setReplaceMap(
              this.getReplaceMap(name));
          renderer.addReversedReplaceMap();
        break;
      }
      return renderer || new qx.ui.table.cellrenderer.Default();
    },

    __setupResources: function() {
      var that = this;
      this.__R = new qx.io.rest.Resource({
        deleteTest: {
          method: 'DELETE',
          url: '/atms/manager/api/atms_tests?id={id}'
        },
        assignTest2User: {
          method: 'GET',
          url: '/atms/manager/assign_test2user?test={test}&user={user}'
        },
        createCasesFromNodes: {
          method: 'GET',
          url: '/atms/manager/create_from_nodes?test={test}'
        }
      });
      this.__R.addListener('success', function() {
        this.setStatus('info', this.tr('Operation completed.'));
      }, this);

      this.__R.addListener('error', function() {
        this.setStatus('error', this.tr('Operation failed.'));
        jQuery('.flash').html(that.tr('Operation failed.').toString());
        jQuery('.flash').show();
      }, this);

      this.__R.addListener('assignTest2UserSuccess', function() {
        jQuery('.flash').html(that.tr('Cases assigned to user').toString());
        jQuery('.flash').show();
      });

      this.__R.addListener('createCasesFromNodesSuccess', function() {
        jQuery('.flash').html(that.tr('Cases created for test').toString());
        jQuery('.flash').show();
      });
    },

    __deleteTests: function() {
      var that = this;
      var ids = this.getIds();

      var caption = ids.length > 1 ?
        this.tr('Delete these items?') : this.tr('Delete this item?');
      var desc = this.tr('This action cannot be undone!');
      var dialog = new atms.WinConfirmation(
          caption,
          desc,
          function() {
            ids.forEach(function(index) {
              that.__R.deleteTest({ id: index });
            }, that);
          });

      qx.core.Init.getApplication().gDesk1.add(dialog);
      dialog.open();
    },

    __newCase: function() {
      var that = this;
      var id = this.getFirstFromSelection();

      var widget = new atms.Form('atms_cases', {
        test: id
      }, 'create', null, null);
      var win = new atms.WinPanel(widget);
      qx.core.Init.getApplication().gDesk1.add(win);
      win.open();
    },

    __newTest: function(mode) {
      var that = this;
      var payload = {};
      if (mode == 'create') payload.project = this.getHiddenQuery().project;
      else if (mode == 'update' && this.getFirstFromSelection()) {
        payload.id = this.getFirstFromSelection().id;
        payload.project = this.getHiddenQuery().project;
      } else return;

      if (payload.project === undefined) payload.project = null;

      var widget = new atms.Form('atms_tests', payload, mode,
          function() { that.reload(); });

      var win = new atms.WinPanel(widget);
      qx.core.Init.getApplication().gDesk1.add(win);
      win.open();
    },

    __openCaseManager: function(mode) {
      var ids = [null];
      if (mode != 'loose') {
        ids = this.getSelectedRows();
      }

      ids.forEach(function(item) {
        var caption = this.tr('Unassigned cases');
        var index = 'None';
        if (item) {
          caption = this.tr('Cases for test') + ': ' + item.name;
          index = item.id;
        }

        var cases = new atms.table.TableCase(index);
        var win  = new atms.WinPanel(cases, caption);
        qx.core.Init.getApplication().gDesk1.add(win);
        win.open();
      }, this);
    },

    _handleDragStart: function(e) {
      e.addType('sender');
      e.addType('target');
      e.addType('tests');
      e.addAction('move');

      var that = this;
      this.getSelectedRows().forEach(function(item) {
        var itemWidget = new qx.ui.basic.Atom(
            item.name,
            'atms/calendar.png');
        itemWidget.setBackgroundColor('background-selected');
        itemWidget.setTextColor('white');
        itemWidget.setPadding([0, 4, 0, 24]);
        that.__dragFeedback.addDragChild(itemWidget);
      });
    },

    __onDrag: function(e) {
      this.__dragFeedback.setDomPosition(e.getDocumentLeft() + 15, e.getDocumentTop() + 15);
    },

    __onDragEnd: function(e) {
      this.__dragFeedback.setDomPosition(-1000, -1000);
      this.__dragFeedback.purgeChildren();
    },

    _handleDropRequest: function(e) {
      e.addData('sender', this);
      e.addData('target', e.getRelatedTarget());
      e.addData('tests', this.getTable().getSelectionModel());
    },

    _handleDrop: function(e) {
      var that = this;
      var sender = e.getData('sender');
      var index = this.getTable().getFocusedRow();
      var test = this.getModel().getRowData(index);

      if (sender instanceof atms.UserList) {
        var user = e.getData('users').getItem(0);
        console.log(user);
        var caption = this.tr('Assign') + ' ' + user.getLabel() + ' ' +
            this.tr('to chosen test') + ' ' + test.name;
        var desc = this.tr(
          'This action will assign all cases from ' +
          'this test to this particular user.');
        var dialog = new atms.WinConfirmation(
          caption,
          desc,
          function() {
            that.__R.assignTest2User({ test: test.id, user: user.getId() });
          }, null);

        this.getApplicationRoot().add(dialog);
        dialog.open();
      }

      if (sender instanceof atms.Tree) {
        if (!test) {
          jQuery('.flash').html(that.tr('No test to handle').toString());
          jQuery('.flash').show();
          return;
        }

        var param = e.getData('tree-items-uri');
        var caption = this.tr('Create new cases for test') + ' ' + test.name;
        var desc = this.tr(
          'This action will create a set of cases based on ' +
          'templates and will assign them to this particular test.');
        var dialog = new atms.WinConfirmation(
            caption,
            desc,
            function() {
              that.__R.createCasesFromNodes({ test: test.id });
            }, null);

        this.getApplicationRoot().add(dialog);
        dialog.open();
      }
    }
  }
});
