/**
*@asset(atms/*)
*@asset(qx/icon/${qx.icontheme}/*)
*/

qx.Class.define('atms.CommentPost',
{
  extend: qx.ui.core.Widget,
  include: [qx.ui.form.MModelProperty],

  properties: {
    appearance:
    {
      refine: true,
      init: 'post'
    },

    id:
    {
      init: null,
      check: 'Integer'
    },

    deletable:
    {
      init: false,
      check: 'Boolean',
      apply: '_applyDeletable'
    },

    user:
    {
      init: '',
      check: 'String',
      apply: '_applyUser'
    },

    txt:
    {
      init: '',
      check: 'String',
      apply: '_applyTxt'
    },

    icon:
    {
      apply: '_applyIcon'
    },

    timestamp:
    {
      init: null,
      apply: '_applyTimestamp'
    },

    read:
    {
      init: true,
      apply: '_applyRead'
    }

  },

  construct: function(parent) {
    this.base(arguments);
    this.parent = parent;

    var layout = new qx.ui.layout.Grid(3, 3);
    layout.setColumnFlex(1, 1);
    layout.setRowFlex(1, 1);
    this._setLayout(layout);
    this._createChildControl('icon');
    this._createChildControl('label');
    this._createChildControl('txt');
    this.setAllowGrowY(true);
  },

  members: {
    parent: null,

    _createChildControlImpl: function(id) {
      var control;
      var that = this;

      switch (id) {
        case 'icon':
          control = new qx.ui.basic.Image();
          control.setScale(true);
          control.setWidth(32);
          control.setHeight(32);
          this._add(control, { row: 1, column: 0 });
        break;

        case 'label':
          control = new qx.ui.basic.Label();
          control.setTextAlign('left');
          this._add(control, { row: 0, column: 1 });
        break;

        case 'txt':
          control = new qx.ui.basic.Label().set({
            rich: true,
            allowGrowX: true
          });
          control.addListener('dblclick', function() {
            var v = this.getValue();
            this.setValue('');
            this.setValue(v);
          }, control);

          this._add(control, { row: 1, column: 1, colSpan: 2 });
        break;

        case 'delButton':
          var that = this;
          control = new qx.ui.form.Button(this.tr('Delete'));
          control.addListener('execute', function() {
            this.parent.deletePost(this.getId(), function() {
              that.destroy();
            });
          }, this);

          this._add(control, { row: 0, column: 2 });
        break;

        case 'timestamp':
          control = new qx.ui.basic.Label();
          control.setAppearance('attlistitem/size');
          this._add(control, { row: 2, column: 2 });
        break;
      }
      return control || this.base(arguments, id);
    },

    _applyIcon: function(value, old) {
      var icon = this.getChildControl('icon');
      icon.setSource(value);
    },

    _applyUser: function(value, old) {
      var label = this.getChildControl('label');
      label.setValue(this.getUser());

      //label.setValue(this.getId() + " - " + this.getUser());
    },

    _applyTxt: function(value, old) {
      var txt = this.getChildControl('txt');
      txt.setValue(value);
      if (value.search('<img') > 0)
                setTimeout(function() {
                  txt.setValue('');
                  txt.setValue(value);
                }, 1500);
    },

    _applyDeletable: function(value, old) {
      if (value)
          this._createChildControl('delButton');
    },

    _applyTimestamp: function(value, old) {
      if (value) {
        var ctrl = this._createChildControl('timestamp');
        ctrl.setValue(value);
      }
    },

    _applyRead: function(value, old) {
      if (!value) {
        var ctrl = this.getChildControl('txt');
        ctrl.addState('not_read');
      }
    }
  }
});
