/**
*@asset(atms/*)
*@asset(qx/icon/${qx.icontheme}/*)
*/

qx.Class.define('atms.UserView',
{
  extend: qx.ui.core.Widget,
  include: [qx.ui.form.MModelProperty],

  properties: {
    appearance:
    {
      refine: true,
      init: 'userlistitem'
    },

    userID:
    {
      check: 'Integer',
      nullable: true
    },

    icon:
    {
      check: 'String',
      apply: '_applyIcon',
      nullable: true
    },

    color: {
      init: 'white',
      nullable: true,
      check: 'String',
      apply: '_applyColor'
    },

    label: {
      check: 'String',
      apply: '_applyLabel',
      nullable: true
    },

    itemsNb: {
      init: 0,
      check: 'Integer',
      apply: '_applyItemsNb'
    },

    itemsTotal: {
      init: 0,
      check: 'Integer',
      apply: '_applyItemsTotal',
      event: 'itemsTotalChanged'
    },

    timeOccupance: {
      init: 0,
      check: 'Integer',
      apply: '_applyTimeOccupance'
    },

    timeOccupanceTotal: {
      init: 0,
      check: 'Integer',
      apply: '_applyTimeOccupanceTotal',
      event: 'itemsTimeOccupanceTotalChanged'
    },

    lastUpdate: {
      init: '-',
      check: 'String',
      apply: '_applyLastUpdate'
    },

    specialState: {
      init: null,
      apply: '_applySpecialState',
      nullable: true
    }
  },

  construct: function(numberFormat, maxi) {
    this.base(arguments);
    this.__nf = numberFormat;

    var layout = new qx.ui.layout.Grid(4, 4);
    layout.setColumnFlex(2, 1);
    layout.setRowFlex(0, 1);
    this._setLayout(layout);

    this._createChildControl('label');
    this._createChildControl('itemsNb');
    this._createChildControl('itemsTotal');
    this._createChildControl('casesProgress');
    if (maxi) {
      this._createChildControl('icon');
      this._createChildControl('timeOccupance');
      this._createChildControl('timeOccupanceTotal');
      this._createChildControl('timeProgress');
      this._createChildControl('lastUpdate');
    }

    this.__maxi = maxi;
  },

  members: {
    __nf: null,
    __maxi: null,

    _createChildControlImpl: function(id) {
      var control;
      var that = this;

      var  setTimeValue = function(value) {
        var v  = that.__nf.parse(value);
        var fv = that.__nf.format(v);
        this.getContentElement().setValue(fv);
        return v;
      };

      switch (id) {
      case 'icon':
        control = new qx.ui.basic.Image(this.getIcon());
        control.setAnonymous(true);
        this._add(control, { row: 0, column: 0, rowSpan: 4 });
      break;

      case 'label':
        control = new qx.ui.basic.Label(this.getLabel());
        control.set({ allowGrowX: true });
        control.setAnonymous(true);
        this._add(control, { row: 0, column: 1, colSpan: 3 });
      break;

      case 'itemsNb':
        control = new qx.ui.basic.Label(this.getItemsNb().toString());
        control.setAnonymous(true);
        control.setTextAlign('right');
        this._add(control, { row: 1, column: 1, colSpan: 1 });
      break;

      case 'casesProgress':
        control = new qx.ui.indicator.ProgressBar(0, 100);
        control.setAnonymous(true);
        this._add(control, { row: 1, column: 2, colSpan: 1 });
        this.addListener('itemsTotalChanged', function() {
          var total = parseFloat(this.getItemsTotal());
          var items = parseFloat(this.getItemsNb());
          var precentage = (items / total) * 100;
          precentage = isNaN(precentage) ? 100 : precentage;
          control.setValue(parseInt(precentage));
        }, this);

      break;

      case 'itemsTotal':
        control = new qx.ui.basic.Label(this.getItemsTotal().toString());
        control.setAnonymous(true);
        this._add(control, { row: 1, column: 3, colSpan: 1 });
      break;

      case 'timeOccupance':
        control = new qx.ui.basic.Label(this.getTimeOccupance().toString());
        control.setValue = setTimeValue;
        control.setAnonymous(true);
        control.setTextAlign('right');
        this._add(control, { row: 2, column: 1, colSpan: 1 });
      break;

      case 'timeProgress':
        control = new qx.ui.indicator.ProgressBar(0, 100);
        this._add(control, { row: 2, column: 2, colSpan: 1 });
        control.getChildControl('progress').removeState('overflowed');
        this.addListener('itemsTimeOccupanceTotalChanged', function(e) {
          var control = this.getChildControl('timeProgress');
          var total = parseFloat(this.getTimeOccupanceTotal());
          var time  = parseFloat(this.getTimeOccupance());
          var precentage = (time / total) * 100;
          precentage = isNaN(precentage) ? 100 : precentage;
          control.getChildControl('progress').removeState('overflowed');
          control.setValue(parseInt(precentage));
          if (precentage > 100) {
            control.getChildControl('progress').addState('overflowed');
          }
        }, this);

      break;

      case 'timeOccupanceTotal':
        control = new qx.ui.basic.Label(this.getTimeOccupanceTotal().toString());
        control.setValue = setTimeValue;
        control.setAnonymous(true);
        this._add(control, { row: 2, column: 3, colSpan: 1 });
      break;

      case 'lastUpdate':
        control = new qx.ui.basic.Label(this.tr('Last update') + ': -');
        control.setAnonymous(true);
        this._add(control, { row: 3, column: 1, colSpan: 3 });
      break; }

      return control || this.base(arguments, id);
    },

    _applyIcon: function(value, old) {
      var icon = this.getChildControl('icon');
      icon.setSource(value);
    },

    _applyLabel: function(value, old) {
      var label = this.getChildControl('label');
      label.setValue(value);
    },

    _applyItemsNb: function(value, old) {
      var label = this.getChildControl('itemsNb');
      label.setValue(value.toString());
    },

    _applyItemsTotal: function(value, old) {
      if (value == 0) {
        this.getChildControl('itemsNb').setVisibility('excluded');
        this.getChildControl('itemsTotal').setVisibility('excluded');
        this.getChildControl('casesProgress').setVisibility('excluded');
        this.getChildControl('timeOccupance').setVisibility('excluded');
        this.getChildControl('timeOccupanceTotal').setVisibility('excluded');
        this.getChildControl('timeProgress').setVisibility('excluded');
      } else {
        this.getChildControl('itemsNb').setVisibility('visible');
        this.getChildControl('itemsTotal').setVisibility('visible');
        this.getChildControl('casesProgress').setVisibility('visible');
        this.getChildControl('timeOccupance').setVisibility('visible');
        this.getChildControl('timeOccupanceTotal').setVisibility('visible');
        this.getChildControl('timeProgress').setVisibility('visible');
      }

      var label = this.getChildControl('itemsTotal');
      label.setValue(value.toString());
    },

    _applyTimeOccupance: function(value, old) {
      var label = this.getChildControl('timeOccupance');
      label.setValue(value.toString());
    },

    _applyTimeOccupanceTotal: function(value, old) {
      var label = this.getChildControl('timeOccupanceTotal');
      label.setValue(value.toString());
    },

    _applyColor: function(value, old) {
      if (!this.__maxi) {
        var label = this.getChildControl('label');
        label.setBackgroundColor(value);
      }
    },

    _applyLastUpdate: function(value, old) {
      var label = this.getChildControl('lastUpdate');
      label.setValue(this.tr('Last update') + ': ' + value.toString());
    },

    _applySpecialState: function(value, old) {
      // if (value == 'BD')
      //   this.setBackgroundColor('#C40000');
      // else this.setBackgroundColor('highlight-shade');
    }
  }
});
