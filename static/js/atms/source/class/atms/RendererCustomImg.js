/**
*@asset(atms/*)
*@asset(qx/icon/${qx.icontheme}/*)
*/

qx.Class.define('atms.RendererCustomImg',
{
  extend: qx.ui.table.cellrenderer.Image,

  construct: function() {
    this.base(arguments);
  },

  statics: {
    icoMap: {
      android: 'atms/android.png',
      windows: 'atms/windows8.png',
      ios: 'atms/apple.png',
      lab: 'atms/meas.png',
      dut: 'atms/stb.png'
    }
  },

  members: {
    __icoMap: null,

    _identifyImage: function(cellInfo)
    {
      var imageHints =
            {
              imageWidth: this.__imageWidth,
              imageHeight: this.__imageHeight
            };
      if (cellInfo.value == '') {
        imageHints.url = null;
      } else {
        imageHints.url = qx.util.AliasManager.getInstance().resolve(
            atms.RendererCustomImg.icoMap[cellInfo.value]);
      }

      imageHints.tooltip = cellInfo.tooltip;

      return imageHints;
    }
  }
});
