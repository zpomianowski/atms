/**
*@asset(atms/*)
*@asset(qx/icon/${qx.icontheme}/*)
*/

qx.Class.define('atms.Chart',
{
  extend: qx.ui.container.Composite,

  construct: function(data) {
    this.data = data;
    this.filter = {};
    this.base(arguments);
    var layout = new qx.ui.layout.VBox();
    this.setLayout(layout);

    if ('layeredBars' in data) this._createChildControl('layeredBars');
  },

  properties: {
    loaded: {
      init: 0,
      apply: '_applyLoaded'
    }
  },

  events:
  {
    refreshChart: 'qx.event.type.Event'
  },

  members: {
    data: null,
    filter: null,
    __ctrl: null,
    __timer: null,

    _applyLoaded: function(value, old) {
      if (Object.keys(this.data['defaults']).length == value) {
        this.__ctrl.setModel(
          qx.data.marshal.Json.createModel(this.data['defaults']));
        this.fireEvent('refreshChart');
      }
    },

    _createChildControlImpl: function(id) {
      var control;
      switch (id) {
        case 'layeredBars':
          control = this.__barsFactory(id);
          this._add(control);
        break;
      }

      return control || this.base(arguments, id);
    },

    __toolbarFactory: function(id) {
      var toolbar = new qx.ui.container.Composite(new qx.ui.layout.Flow(
          4, 2));
      var params = this.data[id]['params'];

      this.__ctrl = new qx.data.controller.Object();

      this.data['defaults'] = {};
      for (var p in params) {
        this.data['defaults'][p] = params[p].value;
      }

      for (var p in params) {
        switch (params[p]['type']) {
          case 'select':
            var sbox = new atms.DynVirtualSelectBox(params[p]['filterp'],
                false, params[p]['multi'], p).set({
                  labelPath: params[p]['path']
                });
            sbox.setMinWidth(220);
            sbox.addListener('changeModel', function() {
              this.setLoaded(this.getLoaded() + 1);
            }, this);

            sbox.getChildControl('dropdown').addListener('click', function() {
              this.fireEvent('refreshChart');
            }, this);

            if (params[p]['multi']) {
              this.filter[p] = (function(sbox) {
                return function() {
                  var data = [];
                  sbox.getModel().forEach(function(item) {
                    if (item.getValue()) data.push(item.getId());
                  });

                  return data;
                };
              })(sbox);
            } else {
              this.filter[p] = (function(sbox) {
                return function() {
                  return sbox.getSelection().getItem(0).getId();
                };
              })(sbox);
            }

            if ('url' in params[p]) {
              var R = new qx.io.rest.Resource({
                get: { method: 'GET', url: params[p]['url'] }
              });
              var S = new qx.data.store.Rest(R, 'get', {
                manipulateData: function(data) {
                  data.content.forEach(function(item) {
                    item.value = false;
                  });

                  return data;
                }
              });
              S.bind('model.content', sbox, 'model');
              R.get();
            }

            if ('model' in params[p]) {
              sbox.setModel(
                qx.data.marshal.Json.createModel(params[p]['model']));
            }

            this.__ctrl.addTarget(sbox, 'defaultValue', p, true);
            if (params[p]['multi']) {
              toolbar.add(sbox);
            } else {
              var box = new qx.ui.container.Composite();
              box.setLayout(new qx.ui.layout.VBox(4));
              var label = new qx.ui.basic.Label(this.tr('Aggregation'));
              box.add(label);
              box.add(sbox);
              toolbar.add(box);
            }

          break;

          case 'date':
            var today = new Date();
            var d = new qx.ui.form.DateField();
            d.addListener('changeValue', function() {
              this.fireEvent('refreshChart');
            }, this);

            d.setValue(new Date(today.getFullYear(), today.getMonth(),
                today.getDate() + params[p]['value']));
            d.getChildControl('list').setWidth(300);
            this.filter[p] = (function(d) {
              return function() {
                var df = new qx.util.format.DateFormat('yyyy-MM-dd kk:mm:ss');
                return df.format(d.getValue());
              };
            })(d);

            this.setLoaded(this.getLoaded() + 1);
            toolbar.add(d);
          break;

          case 'datetime':
            var today = new Date();
            var d = new atms.DateField();
            d.addListener('changeValue', function() {
              this.fireEvent('refreshChart');
            }, this);

            d.setValue(new Date(today.getFullYear(), today.getMonth(),
                today.getDate() + params[p]['value'], 23, 59));
            d.getChildControl('list').setWidth(300);
            d.setMinWidth(200);
            this.filter[p] = (function(d) {
              return function() {
                var df = new qx.util.format.DateFormat('yyyy-MM-dd kk:mm:ss');
                return df.format(d.getValue());
              };
            })(d);

            this.setLoaded(this.getLoaded() + 1);
            toolbar.add(d);
          break;
        }
      }

      return toolbar;
    },

    __barsFactory: function(id) {
      var that = this;

      // containers
      var con = new qx.ui.container.Composite(new qx.ui.layout.VBox());
      var chart = new qx.ui.container.Composite(new qx.ui.layout.Grow());

      // toolbar
      var tools = this.__toolbarFactory(id);

      // chart type
      var chTypeCon = new qx.ui.container.Composite(
        new qx.ui.layout.Flow(4, 4));
      var chTypeB1 = new qx.ui.form.RadioButton(this.tr('Stacked'));
      var chTypeB2 = new qx.ui.form.RadioButton(this.tr('Grouped'));
      var chTypeB3 = new qx.ui.form.RadioButton(this.tr('Lines'));
      chTypeB1.setModel('stack');
      chTypeB2.setModel('group');
      chTypeB3.setModel('lines');
      var chTypeMgr =  new qx.ui.form.RadioGroup();
      chTypeMgr.add(chTypeB1, chTypeB2, chTypeB3);
      chTypeCon.add(chTypeB1);
      chTypeCon.add(chTypeB2);
      chTypeCon.add(chTypeB3);
      tools.add(chTypeCon);

      var chInterCon = new qx.ui.container.Composite(
        new qx.ui.layout.Flow(4, 4));
      var chInterBa = new qx.ui.form.RadioButton(this.tr('Linear'))
          .set({ model: 'linear' });
      var chInterBb = new qx.ui.form.RadioButton(this.tr('Step'))
          .set({ model: 'step' });
      var chInterBc = new qx.ui.form.RadioButton(this.tr('Basis'))
          .set({ model: 'basis' });
      var chInterMgr =  new qx.ui.form.RadioGroup();
      chInterMgr.add(chInterBa, chInterBb, chInterBc);
      chInterCon.add(chInterBa);
      chInterCon.add(chInterBb);
      chInterCon.add(chInterBc);
      tools.add(chInterCon);

      var args = this.data[id]['args'];
      if (args) {
        chInterMgr.setModelSelection([args[1]]);
        chTypeMgr.setModelSelection([args[0]]);
      }

      con.add(tools);
      con.add(chart, { flex: 1 });
      con.setMinHeight(500);

      // d3 init
      var d3Obj = new qxd3.Svg();
      var svgRoot = d3Obj.getD3SvgNode();
      var d3 = d3Obj.getD3();

      // css rules for d3
      d3Obj.addCssRule('.axis path, .axis line', {
          stroke: '#3D72C9',
          'stroke-width': '1'
        }
      );
      d3Obj.addCssRule('.axis .minor line', {
            fill: 'none',
            stroke: '#777',
            'stroke-dasharray': '2,2'
          }
      );
      d3Obj.addCssRule('.axis path', {
          display: 'none'
        }
      );
      d3Obj.addCssRule('.rectData:hover', {
          'stroke-width': '3',
          stroke: '#FFef00',
          fill: '#FFef00'
        }
      );

      var yGroupMax = 0;
      var yStackMax = 0;
      var legendRectSize = 18,
          legendSpacing = 4,
          legendGroupBy = 4,
          legendLabelSpacing = 250,
          legendBottomMargin = 20,
          legendHeight = 100;//Math.ceil(result.layers.length/legendGroupBy) * (legendRectSize + legendSpacing) + legendBottomMargin;
      var margin = { top: legendHeight + 10, right: 0, bottom: 160, left: 35 },
          width = 1000 - margin.left - margin.right,
          height = 500 - margin.top - margin.bottom;

      var emptyA = new qx.data.Array(10).toArray();
      var x = d3.scale.ordinal()
          .domain(['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K'])
          .rangeRoundBands([0, width], .09);

      var y = d3.scale.linear()
          .domain([0, 10])
          .range([height, 0]);

      var color = d3.scale.linear();

      var xAxis = d3.svg.axis()
          .scale(x)
          .tickSize(-height)
          .tickPadding(6)
          .orient('bottom');

      var yAxis = d3.svg.axis()
          .scale(y)
          .ticks(10)
          .tickSize(-width)
          .tickPadding(6)
          .orient('left');

      var svg = svgRoot.attr('viewBox', '0 0 ' +
            (width + margin.left + margin.right) + ' ' +
            (height + margin.top + margin.bottom))
          .attr('preserveAspectRatio', 'xMidYMid meet')
          .append('g')
              .attr('transform',
                    'translate(' + margin.left + ',' + margin.top + ')');

      var gx = svg.append('g')
          .attr('class', 'x axis')
          .attr('transform', 'translate(0,' + height + ')');
      gx.call(xAxis);

      var gy = svg.append('g')
          .attr('class', 'y axis');
      gy.call(yAxis);

      var transitionS, transitionG, transitionL, transitionH = function() {};

      // accuire data
      var result = null;
      var req = new qx.io.request.Xhr(this.data[id]['url'], 'POST');
      req.addListener('success', function(e) {
          var req = e.getTarget();
          result = req.getResponse().content;

          var n = result.layers.length, // number of layers
              m = result.layers[0].data.length, // number of samples per layer
              stack = d3.layout.stack(),
              layers = stack(result.layers.map(function(d) {
                return d.data.map(function(d, i) {
                  return { x: result.x[i], y: d.value, desc: d.desc };});})),

              yGroupMin = d3.min(layers, function(layer) {
                return d3.min(layer, function(d) {
                  return d.y; }); }),

              yGroupMax = d3.max(layers, function(layer) {
                return d3.max(layer, function(d) {
                  return d.y; }); }),

              yStackMin = d3.min(layers, function(layer) {
                return d3.min(layer, function(d) {
                  return d.y0 + d.y; }); });

          yStackMax = d3.max(layers, function(layer) {
            return d3.max(layer, function(d) {
              return d.y0 + d.y; }); });

          // console.log(result.layers[0]);

          x.domain(result.x);
          y.domain([0, yStackMax]);

          // color
          //     .domain([0, n-1])
          //     .range(result.layers.map(function(d, i) { return that.rainbow(n-1, i); }));
          // var color = this.shuffle(
          //   result.layers.map(function(d, i) { return that.rainbow(n, i); }));
          var color = result.layers.map(function(d, i) {
            if (d.hasOwnProperty('color')) {
              return d.color;
            }

            return that.rainbow(n, i);
          });

          xAxis.scale(x);
          yAxis.scale(y);

          var legend = svg.selectAll('.legend').selectAll('*').remove();
          var legend = svg.selectAll('.legend')
              .data(result.layers);

          legend.enter().append('g')
                  .attr('class', 'legend')
                  .on('click', this.tooltip)
                    .attr('transform', function(d, i) {
                      var height = legendRectSize + legendSpacing;
                      var offset =  legendHeight;
                      var horz = i % legendGroupBy * legendLabelSpacing;
                      var vert = parseInt(i / legendGroupBy) * height - offset;
                      return 'translate(' + horz + ',' + vert + ')';
                    });

          legend.append('rect')
              .attr('width', legendRectSize)
              .attr('height', legendRectSize)
              .style('fill', function(d, i) { return color[i]; })
              .style('stroke', '#3D72C9');

          legend.append('text')
              .attr('x', legendRectSize + legendSpacing)
              .attr('y', legendRectSize - legendSpacing)
              .text(function(d) { return d.label; });

          svg.selectAll('.layer').selectAll('*').remove();
          svg.selectAll('.layer').remove();
          var layer = svg.selectAll('.layer')
              .data(layers);

          var lineGen = d3.svg.line()
              .x(function(d) { return x(d.x) + x.rangeBand() / 2; })
              .y(function(d) { return y(d.y); })
              .interpolate('linear');

          layer.enter().append('g')
                  .attr('class', 'layer')
                  .style('fill', function(d, i) { return color[i]; })
                  .append('path')
                      .attr('d', function(d) { return lineGen(d); })
                      .attr('stroke-width', 0)
                      .attr('stroke', function(d, i) { return color[i]; })
                      .attr('fill', 'none');
          var lines = layer.selectAll('path');

          layer.selectAll('rect').selectAll('*').remove();
          var rect = layer.selectAll('g rect')
              .data(function(d) { return d; });

          rect.enter().append('rect')
                  .attr('class', 'rectData')
                  .attr('x', function(d) { return x(d.x); })
                  .attr('y', height)
                  .attr('width', x.rangeBand())
                  .attr('height', 0);

          rect.exit().remove();
          layer.exit().remove();
          legend.exit().remove();

          var text_angle = m * 3 + 15;
          if (text_angle > 90) text_angle = 90;

          // gx.call(xAxis)
          //     .selectAll('.tick text')
          //         .call(that.wrap, x.rangeBand());

          gx.call(xAxis)
              .selectAll('text')
                  .style('text-anchor', 'end')
                  .attr('dx', '-.8em')
                  .attr('dy', '.15em')
                    .attr('transform', function(d) {
                      return 'rotate(-' + text_angle + ')';
                    });

          function refreshY() {
            gy.transition().duration(1500).call(yAxis);
            gy.selectAll('g').filter(function(d) { return d; })
                .classed('minor', true);
          }

          gx.selectAll('line').filter(function(d) { return d; })
              .style('stroke-width', x.rangeBand())
              .style('stroke', '#f6f6ff');

          refreshY();

          function transitionGrouped() {
            transitionLinesHide();
            y.domain([0, yGroupMax]);
            refreshY();

            rect.transition()
              .duration(500)
              .ease('elastic')
              .delay(function(d, i) { return i * 10; })
              .attr('x', function(d, i, j) {
                return x(d.x) + x.rangeBand() / n * j; })
              .attr('width', x.rangeBand() / n)
            .transition()
              .attr('y', function(d) { return y(d.y); })
              .attr('height', function(d) { return height - y(d.y); });
          }

          function transitionStacked() {
            transitionLinesHide();
            y.domain([0, yStackMax]);
            refreshY();

            rect.transition()
              .duration(500)
              .ease('bounce')
              .delay(function(d, i) { return i * 10; })
              .attr('y', function(d) { return y(d.y0 + d.y); })
              .attr('height', function(d) { return y(d.y0) - y(d.y0 + d.y); })
            .transition()
              .attr('x', function(d) { return x(d.x); })
              .attr('width', x.rangeBand());
          }

          function transitionLines(interpolation) {
            y.domain([yGroupMin, yGroupMax]);
            refreshY();

            if (interpolation)
                lineGen.interpolate(interpolation);

            lines.transition()
                .duration(300)
                .delay(function(d, i) { return i * 10; })
                .ease('elastic')
                .attr('stroke-width', 4);
            lines.attr('d', function(d) { return lineGen(d); });
          }

          function transitionLinesHide() {
            lines.transition()
                .duration(300)
                .delay(function(d, i) { return i * 10; })
                .ease('elastic')
                .attr('stroke-width', 0);
          }

          function transitionHide() {
            rect
                .transition()
                    .duration(300)
                    .delay(function(d, i) { return i * 10; })
                    .ease('elastic')
                    .attr('y', function(d) { return y(d.y); })
                    .attr('height', function(d) { return 0; });

            lines.attr('stroke-width', 0);
          }

          transitionG = transitionGrouped;
          transitionS = transitionStacked;
          transitionH = transitionHide;
          transitionL = transitionLines;

          manageInterpolation(chInterMgr.getModelSelection().toArray()[0]);
          manageTransition(chTypeMgr.getModelSelection().toArray()[0]);
        }, this);

      function manageTransition(action) {
        switch (action) {
          case 'stack':
            transitionS();
          break;

          case 'group':
            transitionG();
          break;

          case 'lines':
            transitionH();
            transitionL();
          break;
        }
      }

      function manageInterpolation(action) {
        if (chTypeMgr.getModelSelection().toArray()[0] != 'lines')
            return;
        transitionH();
        transitionL(action);
      }

      chTypeMgr.addListener('changeSelection', function(e) {
        manageTransition(e.getData()[0].getModel());
      }, this);

      chInterMgr.addListener('changeSelection', function(e) {
        manageInterpolation(e.getData()[0].getModel());
      }, this);

      function reloadData() {
        that.changeRequestData(req);
        transitionH();
        setTimeout(function() {
          // req.setAsync(false);
          req.send();

          // manageTransition(chTypeMgr.getModelSelection().toArray()[0]);
        }, 500);
      }

      this.__timer = new qx.event.Timer(500);
      this.__timer.addListener('interval', function() {
        reloadData();
        this.stop();
      });

      this.addListener('refreshChart', function() {
        this.__timer.restart();
      });

      reloadData();
      chart.add(d3Obj);
      return con;
    },

    changeRequestData: function(r) {
      var data = {};
      for (var k in this.filter)
          data[k] = this.filter[k]();
      r.setRequestData(data);
    },

    tooltip: function(d, i) {
      var timeout = 500;
      var popup = new qx.ui.popup.Popup(new qx.ui.layout.Canvas()).set({
        backgroundColor: d.color,
        padding: [2, 4],
        offset: 3,
        position: 'top-right'
      });

      popup.add(new qx.ui.basic.Atom(
        'dupa ' + i, 'icon/32/apps/media-photo-album.png'));
      popup.placeToPoint({ left: d3.event.pageX, top: d3.event.pageY });
      popup.show();
      if (timeout)
            setTimeout(function() {
              popup.hide();
            }, timeout);
    },

    rainbow: function(numOfSteps, step) {
      var r, g, b;
      var h = step / numOfSteps;
      var i = ~~(h * 6);
      var f = h * 6 - i;
      var x = 1 - f;
      switch (i % 6){
        case 0: r = 1, g = f, b = 0; break;
        case 1: r = x, g = 1, b = 0; break;
        case 2: r = 0, g = 1, b = f; break;
        case 3: r = 0, g = x, b = 1; break;
        case 4: r = f, g = 0, b = 1; break;
        case 5: r = 1, g = 0, b = x; break;
      }

      function rgbToHsl(r, g, b) {
        var max = Math.max(r, g, b), min = Math.min(r, g, b);
        var h, s, l = (max + min) / 2;

        if (max == min) {
          h = s = 0; // achromatic
        }else {
          var d = max - min;
          s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
          switch (max){
            case r: h = (g - b) / d + (g < b ? 6 : 0); break;
            case g: h = (b - r) / d + 2; break;
            case b: h = (r - g) / d + 4; break;
          }
          h /= 6;
        }

        return [h, s / 1.6, l * 1.3];
      }

      function hslToRgb(h, s, l) {
        var r, g, b;

        if (s == 0) {
          r = g = b = l; // achromatic
        }else {
          function hue2rgb(p, x, t) {
            if (t < 0) t += 1;
            if (t > 1) t -= 1;
            if (t < 1 / 6) return p + (x - p) * 6 * t;
            if (t < 1 / 2) return x;
            if (t < 2 / 3) return p + (x - p) * (2 / 3 - t) * 6;
            return p;
          }

          var x = l < 0.5 ? l * (1 + s) : l + s - l * s;
          var p = 2 * l - x;
          r = hue2rgb(p, x, h + 1 / 3);
          g = hue2rgb(p, x, h);
          b = hue2rgb(p, x, h - 1 / 3);
        }

        return [r, g, b];
      }

      var h = rgbToHsl(r, g, b);
      var c = hslToRgb(h[0], h[1], h[2]);
      c = ('#' + ('00' + (~~(c[0] * 255)).toString(16)).slice(-2) +
        ('00' + (~~(c[1] * 255)).toString(16)).slice(-2) +
        ('00' + (~~(c[2] * 255)).toString(16)).slice(-2));
      return (c);
    },

    wrap: function(text, width) {
      text.each(function() {
        var text = d3.select(this),
            words = text.text().split(/\s+/).reverse(),
            word,
            line = [],
            lineNumber = 0,
            lineHeight = 1.1, // ems
            y = text.attr('y'),
            dy = parseFloat(text.attr('dy')),
            tspan = text.text(null)
              .append('tspan')
                .attr('x', 0)
                .attr('y', y)
                .attr('dy', dy + 'em');
        while (word = words.pop()) {
          line.push(word);
          tspan.text(line.join(' '));
          if (tspan.node().getComputedTextLength() > width) {
            line.pop();
            tspan.text(line.join(' '));
            line = [word];
            tspan = text.append('tspan')
              .attr('x', 0)
              .attr('y', y)
              .attr('dy', ++lineNumber * lineHeight + dy + 'em').text(word);
          }
        }
      });
    },

    shuffle: function(o) {
      for (var j, x, i = o.length; i; j = Math.floor(
        Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
      return o;
    }
  }
});
