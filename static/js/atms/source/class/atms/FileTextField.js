/**
*@asset(atms/*)
*@asset(qx/icon/${qx.icontheme}/*)
*/

qx.Class.define('atms.FileTextField',
{
  extend: qx.ui.form.TextField,

  properties: {
    id: {
      check: 'Integer',
      nullable: true,
      event: 'idChanged'
    }
  },

  construct: function(placeholder, extensions) {
    this.base(arguments);
    this.__exts = extensions || [];
    this.setPlaceholder(placeholder);
    this.setReadOnly(true);
    this.setDroppable(true);
    this.__setupResources();
    this.setInvalidMessage('dupa');

    this.addListener('idChanged', function() {
      if (this.getId())
        this.__attR.get({ id: this.getId() });
      else this.setValue('');
    }, this);

    this.addListener('dblclick', function() {
      var that = this;
      var caption = this.tr('Warning!');
      var desc = this.tr('Do you want to clear the field?');
      var dialog = new atms.WinConfirmation(
        caption,
        desc, function() {
          that.setId(null);
        }, null);

      this.getApplicationRoot().add(dialog);
      dialog.open();
    }, this);
  },

  members: {
    __exts: null,
    __attR: null,
    __attS: null,

    __setupResources: function() {
      var that = this;
      var attconf = {
        get: {
          method: 'GET',
          url: '/atms/manager/api/atms_attachments/id/{id}.json'
        }
      };
      this.__attR = new qx.io.rest.Resource(attconf);
      this.__attS = new qx.data.store.Rest(this.__attR, 'get');
      this.__attS.bind('model.content[0].title', this, 'value', {
        converter: function(data) {
          if (that.__exts.length > 0 && data) {
            var ext = data.split('.').pop();
            var incorrect = that.__exts.indexOf(ext) == -1;
            if (incorrect) {
              that.setInvalidMessage(
                that.tr('"%1" is not supported file type!', ext) +
                that.tr('Supported files: %1', that.__exts.join(', ')));
              that.setValid(false);
              return '';
            }
          }

          that.setValid(true);
          if (data) return data;
          else return '';
        }
      });
    }
  }
});
