/**
*@asset(atms/*)
*@asset(qx/icon/${qx.icontheme}/*)
*/

qx.Class.define('atms.QuizQ',
{
  extend: qx.ui.container.Composite,

  construct: function(mode, callback_ok) {
    this.__mode = mode;
    this.__callback_ok = callback_ok;

    this.base(arguments);
    this.setLayout(new qx.ui.layout.Grow());
    this.setMinWidth(500);
    var form = new qx.ui.form.Form();

    var category = this.__widgetFactory('text');
    var title = this.__widgetFactory('text');
    var questionbody = this.__widgetFactory('markitup');
    var optionlist = this.__widgetFactory('list');
    optionlist.setSelectionMode('multi');

    form.add(category, this.tr('Category'),
        null, 'category');
    form.add(title, this.tr('Question'),
        null, 'title');
    var text = 'text';
    if (mode == 'read') text = 'text_html';
    form.add(questionbody, this.tr('Details'),
        null, text);
    form.add(optionlist, this.tr('Options'),
        null, 'options', { flex: 1 });

    var that = this;
    var recalcH = function() {
      setTimeout(function() {
        var newHeight = 0;
        optionlist.getChildren().forEach(function(item) {
          newHeight += item.getSizeHint().height;
        }, that);

        optionlist.setHeight(newHeight + 20);
      }, 300);
    };

    optionlist.addListener('removeItem', recalcH);
    optionlist.addListener('addItem', recalcH);

    var controller = new qx.data.controller.Form(null, form);

    this.add(new qx.ui.form.renderer.Single(form));

    this.__ctrl = controller;
    this.__optionlist = optionlist;

    this.__setupResources();
    this.__setupStores();
    this.__setupBindings();

    if (mode == 'read') return;
    var saveButton = new qx.ui.form.Button(
        this.tr('Save'), 'atms/update-white.png');
    var createButton = new qx.ui.form.Button(
        this.tr('Create'), 'atms/update-white.png');
    var addOption = new qx.ui.form.Button(
        this.tr('Add option'), 'atms/update-white.png');

    form.addButton(addOption);
    form.addButton(saveButton);
    form.addButton(createButton);

    addOption.addListener('execute', function() {
      var o = this.__widgetFactory('option');
      optionlist.add(o);
    }, this);

    var prepareData = function() {
      var options = [];
      optionlist.getChildren().forEach(function(item) {
        if (that.__mode != 'read')
          options.push({
            id: item.getId(),
            correct: item.getChildControl('checkbox').getValue(),
            text: item.getChildControl('text').getValue()
          });
      });

      var model = controller.getModel();
      model.setOptions(
          qx.data.marshal.Json.createModel(options));
      return qx.util.Serializer.toJson(model);
    };

    createButton.addListener('execute', function() {
      this.__R.post(null, prepareData());
    }, this);

    saveButton.addListener('execute', function() {
      this.__R.update(null, prepareData());
    }, this);

    controller.bind('model.id', saveButton, 'enabled', {
      converter: function(value, model, source, target) {
        if (value) return true;
        return false;
      }
    });

    var model_skeleton = {
      id: null,
      category: 'ZADANIE',
      title: 'Oblicz',
      text: 'Treść zadania [...]',
      options: []
    };
    var model = controller.setModel(
        qx.data.marshal.Json.createModel(model_skeleton));
  },

  members: {
    __ctrl: null,
    __optionlist: null,
    __mode: null,
    __form: null,
    __R: null,
    __S: null,

    __callback_ok: null,

    load: function(id) {
      this.__R.get({
        id: id
      });
    },

    getResult: function() {
      var options = this.__optionlist.getChildren();
      var result = true;
      options.forEach(function(item) {
        var answear_result = item.getAnswear() == item.getChecked();
        item.setState(answear_result);
        result &= answear_result;
      });

      return {
        question: this.__ctrl.getModel().getId(),
        result: result
      };
    },

    callback_ok: function() {
      jQuery('.flash').html(this.tr('Operation succeed.').toString());
      jQuery('.flash').show();
      if (typeof (this.__callback_ok) == 'function')
          this.__callback_ok();
    },

    callback_error: function(msg) {
      var err_details = ' ';
      if (msg) err_details += msg;
      jQuery('.flash').html(this.tr(
          'Form not filled properly!')
          .toString() + err_details);
      jQuery('.flash').show();
    },

    setOptions: function(options) {
      this.__optionlist.removeAll();
      for (var i in options) {
        var o = this.__widgetFactory('option');
        o.setId(options[i].id);
        if (this.__mode != 'read') {
          o.setChecked(options[i].correct);
          o.setText(options[i].text);
        } else {
          o.setText(options[i].text_html);
        }

        o.setAnswear(options[i].correct);
        this.__optionlist.add(o);
      }
    },

    __setupResources: function() {
      var base_url = '/atms/manager/../quiz_manager/';
      this.__R = new qx.io.rest.Resource({
        get: {
            method: 'GET',
            url: base_url + 'api/{id}.json' },
        update: {
            method: 'PUT',
            url: base_url + 'api/' },
        post: {
            method: 'POST',
            url: base_url + 'api/' }
      });
      this.__R.addListener('updateSuccess', function() {
        this.callback_ok();
      }, this);

      this.__R.addListener('postSuccess', function() {
        this.callback_ok();
      }, this);

      this.__R.addListener('updateError', function(e) {
        var msg = e.getRequest().getResponse();
        this.callback_error(msg);
      }, this);

      this.__R.addListener('postError', function(e) {
        var msg = e.getRequest().getResponse();
        this.callback_error(msg);
      }, this);
    },

    __setupStores: function() {
      var that = this;
      this.__S = new qx.data.store.Rest(this.__R, 'get', {
        manipulateData: function(data) {
          that.setOptions(data.content[0].options);
          return data;
        }
      });
    },

    __setupBindings: function() {
      this.__S.bind('model.content[0]', this.__ctrl, 'model', true);
    },

    __widgetFactory: function(name) {
      var widget = null;

      switch (this.__mode + '_' + name) {
        case 'update_markitup':
        case 'create_markitup':
          widget = new markitup.Editor('/atms/manager/html_render.json').set({
            required: true,
            minWidth: 350,
            minHeight: 350
          });
        break;

        case 'read_markitup':
          widget = new atms.FormLabel().set({
            rich: true,
            minWidth: 350,
            maxWidth: 350
          });
        break;

        case 'create_list':
        case 'update_list':
        case 'read_list':
          widget = new qx.ui.form.List();
          widget.setHeight(0);
        break;

        case 'create_option':
        case 'update_option':
        case 'read_option':
          widget = new atms.QuizOptionView(this.__mode);
        break;

        break;

        case 'create_text':
        case 'update_text':
          widget =  new qx.ui.form.TextField();
          widget.setRequired(true);
        break;

        case 'read_text':
          widget = new atms.FormLabel();
        break;
      }
      return widget || new qx.ui.form.TextField();
    }
  }
});
