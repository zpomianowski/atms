/**
*@asset(atms/*)
*@asset(qx/icon/${qx.icontheme}/*)
*/

qx.Class.define('atms.ViewHTML',
{
  extend: qx.ui.core.Widget,

  construct: function(uri) {
    this.base(arguments);
    this.__setupResources(uri);
    this._setLayout(new qx.ui.layout.Grow());
    this.__html = new qx.ui.embed.Html().set({
      overflowY: 'auto'
    });
    this._add(this.__html);
  },

  members: {
    __html: null,
    __scroll: null,
    __R: null,

    __setupResources: function(uri) {
      this.__R = new qx.io.rest.Resource({
        get: {
            method: 'GET',
            url: uri }
      });
      this.__R.addListener('success', function(e) {
        this.__html.setHtml(e.getData());
      }, this);

      this.__R.get();
    }
  }
});
