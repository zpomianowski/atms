/**
*@asset(atms/*)
*@asset(qx/icon/${qx.icontheme}/*)
*/

qx.Class.define('atms.FormLabel',
{
  extend: qx.ui.basic.Label,
  include: qx.ui.form.MForm,
  implement: [qx.ui.form.IStringForm, qx.ui.form.IForm],

  construct: function() {
    this.base(arguments);
  }
});
