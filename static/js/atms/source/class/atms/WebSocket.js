/**
*@asset(atms/*)
*@asset(qx/icon/${qx.icontheme}/*)
*/

qx.Class.define('atms.WebSocket',
{
  extend: qx.core.Object,

  construct: function(protocol_host_port, url_path, onmessage, onopen, onclose) {
    if (protocol_host_port == null) {
      if (gWsServer !== null)
      {
        protocol_host_port = gWsServer + '/';
      } else {
        protocol_host_port = 'ws://127.0.0.1:8888/';
      }
    }

    this.base(arguments);
    if ('WebSocket' in window) {
      this.__ws = new WebSocket(gWsServer + url_path);
      this.__ws.onopen = onopen ? onopen : (function() {});

      this.__ws.onmessage = onmessage;
      this.__ws.onclose = onclose ? onclose : (function() {});
    } else throw 'WebSocket not supported';
  },

  members: {
    __ws: null,

    close: function() {
      this.__ws.close();
    },

    send: function(msg) {
      this.__ws.send(msg);
    }
  }
});
