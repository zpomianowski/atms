qx.Class.define('atms.ModalCellEditorFactory',
{
  extend: qx.core.Object,
  implement: qx.ui.table.ICellEditorFactory,
  include: [qx.locale.MTranslation],

  construct: function() {
    this.base(arguments);
    this.__df = new qx.util.format.DateFormat('yyyy-MM-dd kk:mm:ss');
  },

  members:
    {
      __df: null,

      createCellEditor: function(cellInfo) {
        var cellId = cellInfo.table.getTableModel().getColumnId(cellInfo.col);
        var cellEditor = new qx.ui.window.Window();
        cellEditor.setLayout(new qx.ui.layout.HBox(4));
        cellEditor.set(
            {
              padding: 3,
              showMaximize: false,
              showMinimize: false
            });
        cellEditor.setWidth(300);
        cellEditor.moveTo(300, 250);

        var titles = {
          start_time: this.tr('Edit start time'),
          stop_time: this.tr('Edit stop time'),
          next_run_time: this.tr('Next run time')
        };
        var title = titles[cellId] ? titles[cellId] : this.tr('Cell editor');
        switch (cellId)
        {
          case 'start_time':
          case 'stop_time':
          case 'next_run_time':
            cellEditor.__cellEditor = new atms.DateField();
            cellEditor.__cellEditor.setValue(new Date(cellInfo.value));
          break;

          default:
            cellEditor.__cellEditor = new qx.ui.form.TextArea(cellInfo.value + '');
            cellEditor.addListener('appear', function(e) {
              cellEditor.__cellEditor.focus();
              cellEditor.__cellEditor.setTextSelection(0, cellEditor.__cellEditor.getValue().length);
            });

          break;
        }
        cellEditor.__cellEditor.setAllowGrowY(true);
        cellEditor.setCaption(title);
        cellEditor.add(cellEditor.__cellEditor, { flex: 1 });

        var save = new qx.ui.form.Button(this.tr('Save'));
        save.addListener('execute', function(e) {
          cellEditor.close();
        });

        cellEditor.add(save);

        var command = new qx.ui.command.Command('Enter');
        command.addListener('execute', function(e) {
          save.execute();
          command.dispose();
          command = null;
        });

        return cellEditor;
      },

      getCellEditorValue: function(cellEditor) {
        var value = cellEditor.__cellEditor.getValue();
        if (value instanceof Date)
            value = this.__df.format(value);
        return value;
      }
    }
});
