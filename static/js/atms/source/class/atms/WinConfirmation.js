/**
*@asset(atms/*)
*@asset(qx/icon/${qx.icontheme}/*)
*/

qx.Class.define('atms.WinConfirmation',
{
  extend: qx.ui.window.Window,

  construct: function(title, msg, callback_success, callback_fail, valueMode, options) {
    this.__valueMode = valueMode || false;
    this.base(arguments, title, 'atms/warning-sign.png');
    this.setLayout(new qx.ui.layout.VBox(10));
    this.setModal(true);
    this.setShowClose(true);
    this.setShowMinimize(false);
    this.setShowMaximize(false);
    var width = this.__valueMode == true ? 300 : 600;
    this.setMinWidth(width);

    var mngr = new qx.ui.form.validation.Manager();

    if (!this.__valueMode) {
      var warn = new qx.ui.form.TextArea(msg);
      warn.setReadOnly(true);
    } else {
      var warn = new qx.ui.form.TextField();
      warn.setRequired(true);
      mngr.add(warn);
    }

    this.add(warn, { flex: 1 });

    var box = new qx.ui.container.Composite;
    box.setLayout(new qx.ui.layout.HBox(10, 'right'));
    this.add(box);

    var btn3 = new qx.ui.form.Button(this.tr('Yes'), 'atms/ok-sign.png');
    btn3.addListener('execute', function(e) {
      if (mngr.validate() || !this.__valueMode) {
        if (callback_success) callback_success(warn.getValue());
        this.close();
      }
    }, this);

    box.add(btn3);

    var btn4 = new qx.ui.form.Button(this.tr('No'), 'atms/remove-sign.png');
    btn4.addListener('execute', function(e) {
      if (callback_fail) callback_fail();
      this.close();
    }, this);

    box.add(btn4);

    if (options) {
      if (options['width']) this.setMinWidth(options['width']);
      if (options['height']) this.setMinHeight(options['height']);
      if (options['textStyle']) warn.setAppearance(options['textStyle']);
    }

    this.addListener('resize', this.center);

    // this._cmdMngr = new qx.ui.command.GroupManager();
    // var group1 = new qx.ui.command.Group();
    // var enterCommand = new qx.ui.command.Command("Enter");
    // var escCommand = new qx.ui.command.Command("Esc");
    // group1.add("enter", enterCommand);
    // group1.add("esc", escCommand);
    // group1.setActive(true);

    // btn3.setCommand(enterCommand);
    // btn4.setCommand(escCommand);
  },

  members: {
    _cmdMngr: null,
    __valueMode: null,

    __handleKey: function(e) {
      console.log(e);
    }
  }
});
