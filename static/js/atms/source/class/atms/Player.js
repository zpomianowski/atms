var qxversion = qx.core.Environment.get('qx.version');
if ({ '4.1': true }[qxversion]) {
  qx.bom.Event.$$stopPropagation_old = qx.bom.Event.stopPropagation;
  qx.bom.Event.stopPropagation = function(e) {
    if (typeof e.target.className !== 'object') {
      qx.bom.Event.$$stopPropagation_old(e);
    }
  };
}

qx.Class.define('atms.Player',
{
  extend: qx.ui.core.Widget,

  construct: function() {
    this.base(arguments);

    this.__addCss('<css_file>');
    var codeArr = [
        '<js_file_1>',
        '<js_file_2>'
    ];
    this.__loadScriptArr(codeArr, qx.lang.Function.bind(this.__loadElement, this));
    this.__setup();
  },

  statics: {
    INSTANCE_COUNTER: 0,
    LOADED: {},
    LOADING: {}
  },

  events: {
    scriptLoaded: 'qx.event.type.Event'
  },

  properties:
  {
    editor: {},

    isLoaded:
    {
      nullable: true,
      init: false,
      apply: '__isLoaded'
    }
  },

  members: {
    __loadScriptArr: function(codeArr, handler) {
      var that = this;
      var script = codeArr.shift();
      if (script) {
        if (qxcodemirror.Widget.LOADING[script]) {
          qxcodemirror.Widget.LOADING[script].addListenerOnce(
              'scriptLoaded', function() {
            this.__loadScriptArr(codeArr, handler);
          }, this);
        } else if (qxcodemirror.Widget.LOADED[script]) {
          this.__loadScriptArr(codeArr, handler);
        } else {
          qxcodemirror.Widget.LOADING[script] = this;
          var sl = new qx.bom.request.Script();
          var src = qx.util.ResourceManager.getInstance().toUri(script);
          sl.onload = function() {
            that.debug('Dynamically loaded ' + src + ': ' + status);
            that.__loadScriptArr(codeArr, handler);
            qxcodemirror.Widget.LOADED[script] = true;
            qxcodemirror.Widget.LOADING[script] = null;
            that.fireDataEvent('scriptLoaded', script);

          };

          sl.open('GET', src);
          sl.send();
        }
      } else {
        handler();
      }
    },

    __addCss: function(url) {
      if (!qxcodemirror.Widget.LOADED[url]) {
        qxcodemirror.Widget.LOADED[url] = true;
        var head = document.getElementsByTagName('head')[0];
        var el = document.createElement('link');
        el.type = 'text/css';
        el.rel = 'stylesheet';
        el.href = qx.util.ResourceManager.getInstance().toUri(url);
        setTimeout(function() {
          head.appendChild(el);
        }, 0);
      };
    },

    __loadElement: function() {
      var el = this.getContentElement().getDomElement();
      if (el == null) {
        this.addListenerOnce('appear', qx.lang.Function.bind(this.__loadElement, this), this);
      } else {
        // TODO
      }
    },

    __isLoaded: function(value, old, name) {
        // TODO
      },

    destroy: function() {
      if (this.__ws) this.__ws.close();
      arguments.callee.base.apply(this, arguments);
    }
  }
});
