/**
* @ignore(CKEDITOR*)
* @asset(cke/*)
*/

qx.Class.define('atms.Markmin',
{
  extend: qx.ui.container.Composite,

  construct: function() {
    this.base(arguments);

    var codeArr = [
        'js/markitup/jquery.markitup.js'
    ];
    this.__loadScriptArr(codeArr, qx.lang.Function.bind(
      this.__loadElement, this));
  },

  statics: {
    INSTANCE_COUNTER: 0,
    LOADED: {},
    LOADING: {}
  },

  events: {
    scriptLoaded: 'qx.event.type.Event'
  },

  properties:
    {
      value:
      {
        check: 'String',
        nullable: true,
        event: 'changeValue',
        init: ''
      },

      isLoaded:
      {
        nullable: true,
        init: false,
        apply: '__isLoaded'
      }
    },

  members: {
    __readOnly: null,
    __ckeditor: null,
    __div: null,

    __loadScriptArr: function(codeArr, handler) {
      var that = this;
      var script = codeArr.shift();
      if (script) {
        if (atms.Markmin.LOADING[script]) {
          atms.Markmin.LOADING[script].addListenerOnce(
                      'scriptLoaded', function() {
                        this.__loadScriptArr(codeArr, handler);
                      }, this);
        } else if (atms.Markmin.LOADED[script]) {
          this.__loadScriptArr(codeArr, handler);
        } else {
          atms.Markmin.LOADING[script] = this;
          var sl = new qx.bom.request.Script();
          var src = qx.util.ResourceManager.getInstance().toUri(
            '../static/' + script);
          sl.onload = function() {
            that.debug('Dynamically loaded ' + src + ': ' + status);
            that.__loadScriptArr(codeArr, handler);
            atms.Markmin.LOADED[script] = true;
            atms.Markmin.LOADING[script] = null;
            that.fireDataEvent('scriptLoaded', script);
          };

          sl.open('GET', src);
          sl.send();
        }
      } else {
        handler();
      }
    },

    __addCss: function(url) {
      if (!atms.Markmin.LOADED[url]) {
        atms.Markmin.LOADED[url] = true;
        var head = document.getElementsByTagName('head')[0];
        var el = document.createElement('link');
        el.type = 'text/css';
        el.rel = 'stylesheet';
        el.href = qx.util.ResourceManager.getInstance().toUri(
          '../' + url);
        setTimeout(function() {
          head.appendChild(el);
        }, 0);
      };
    },

    __loadElement: function() {

    },

    __isLoaded: function(value, old, name) {
      this.__ckeditor.setData(this.getValue());
      this.addListener('changeValue', function() {
        this.__ckeditor.setData(this.getValue());
      }, this);
    },

    __destroy: function() {
        },

    saveValue: function() {
      this.setValue(this.__ckeditor.getData());
    },

    setCkeEnabled: function(value) {
      this.__readOnly = value;
      this.setIsLoaded(true);
    }
  }
});
