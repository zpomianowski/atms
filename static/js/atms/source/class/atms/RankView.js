/**
*@asset(atms/*)
*@asset(qx/icon/${qx.icontheme}/*)
*/

qx.Class.define('atms.RankView',
{
  extend: qx.ui.core.Widget,
  include: [qx.ui.form.MModelProperty],

  construct: function() {
    this.base(arguments);

    var main_layout = new qx.ui.layout.Grid();

    // main_layout.setColumnFlex(0, 1);
    // main_layout.setColumnFlex(1, 1);
    main_layout.setColumnFlex(2, 1);
    main_layout.setColumnFlex(3, 1);

    // main_layout.setRowFlex(2, 1);

    main_layout.setColumnAlign(0, 'left', 'middle');

    this._setLayout(main_layout);

    this._createChildControl('user-place');
    this._createChildControl('user-label');
    this._createChildControl('user-rank');
    this._createChildControl('user-icon');
    this._createChildControl('user-awards');

    this.setAppearance('rank');
  },

  properties: {
    place: {
      check: 'Integer',
      apply: '_applyPlace'
    },

    label: {
      check: 'String',
      apply: '_applyLabel',
      nullable: true
    },

    icon: {
      check: 'String',
      apply: '_applyIcon',
      nullable: true
    },

    rank: {
      check: 'String',
      apply: '_applyRank'
    },

    awards: {
      check: 'qx.data.Array',
      apply: '_applyAwards'
    },

    color: {
      check: 'String',
      apply: '_applyColor',
      init: '#5583D0',
      nullable: true
    }
  },

  members: {
    _createChildControlImpl: function(id) {
      var control;
      var that = this;

      switch (id) {
        case 'user-place':
          control = new qx.ui.basic.Label();
          control.setAllowGrowY(true);
          control.setWidth(56);
          this._add(control, { row: 0, column: 0, rowSpan: 2, colSpan: 1 });
        break;

        case 'user-label':
          control = new qx.ui.basic.Label();
          this._add(control, { row: 0, column: 2, rowSpan: 1, colSpan: 1 });
        break;

        case 'user-rank':
          control = new qx.ui.basic.Label();
          this._add(control, { row: 1, column: 2, rowSpan: 1, colSpan: 1 });
        break;

        case 'user-icon':
          control = new qx.ui.basic.Image().set({ scale: true });
          control.setWidth(48);
          control.setHeight(48);
          this._add(control, { row: 0, column: 1, rowSpan: 2, colSpan: 1 });
        break;

        case 'user-awards':
          control = new qx.ui.container.Composite();
          control.setLayout(new qx.ui.layout.Flow(4, 4));
          this._add(control, { row: 2, column: 0, rowSpan: 1, colSpan: 3 });
        break;
      }
      return control || this.base(arguments, id);
    },

    _applyColor: function(value, old) {
      if (!value) return;
      this.getChildControl('user-place').setBackgroundColor(value);
    },

    _applyPlace: function(value, old) {
      var ctrl = this.getChildControl('user-place');
      ctrl.setValue(String(value));
    },

    _applyIcon: function(value, old) {
      var ctrl = this.getChildControl('user-icon');
      ctrl.setSource(value);
    },

    _applyLabel: function(value, old) {
      var ctrl = this.getChildControl('user-label');
      ctrl.setValue(value);
    },

    _applyRank: function(value, old) {
      var ctrl = this.getChildControl('user-rank');
      ctrl.setValue(value);
    },

    __awardFactory: function(count, icon, size) {
      var holder = this.getChildControl('user-awards');
      var award = new qx.ui.basic.Atom('x' + String(count), icon);
      var ico = award.getChildControl('icon');
      ico.setScale(true);
      ico.setWidth(size);
      ico.setHeight(size);
      holder.add(award);
      return award;
    },

    _applyAwards: function(value, old) {
      this.getChildControl('user-awards').removeAll();

      value.forEach(function(item) {
        var count = item.getCount();
        var name = item.getName();
        if (count <= 0) return;
        switch (name) {
          case 'badge1':
            var award = this.__awardFactory(count, 'atms/award_badge1.png', 16);
            award.setToolTip(new qx.ui.tooltip.ToolTip(
                this.tr('Bronze medal for experience.')));
          break;

          case 'badge2':
            var award = this.__awardFactory(count, 'atms/award_badge2.png', 24);
            award.setToolTip(new qx.ui.tooltip.ToolTip(
                this.tr('Silver medal for experience.')));
          break;

          case 'badge3':
            var award = this.__awardFactory(count, 'atms/award_badge3.png', 32);
            award.setToolTip(new qx.ui.tooltip.ToolTip(
                this.tr('Gold medal for experience!')));
          break;

          case 'week_best':
            var award = this.__awardFactory(count, 'atms/award_week_best.png', 32);
            award.setToolTip(new qx.ui.tooltip.ToolTip(
                this.tr('Best score of the week is yours!')));
          break;

          case 'month_best':
            var award = this.__awardFactory(count, 'atms/award_month_best.png', 48);
            award.setToolTip(new qx.ui.tooltip.ToolTip(
                this.tr('Best score of the month is yours!!!')));
          break;

          case 'month_worst':
            var award = this.__awardFactory(count, 'atms/award_worst.png', 48);
            award.setToolTip(new qx.ui.tooltip.ToolTip(
                this.tr('Oh no! You get penalty dick for the whole month!!!')));
          break;

          case 'week_worst':
            var award = this.__awardFactory(count, 'atms/award_worst.png', 32);
            award.setToolTip(new qx.ui.tooltip.ToolTip(
                this.tr('Oh no! You get penalty dick for the whole week!')));
          break;

          case 'week_best_day':
            var award = this.__awardFactory(count, 'atms/award_best_day.png', 32);
            award.setToolTip(new qx.ui.tooltip.ToolTip(
                this.tr('Most productive day for the week!')));
          break;

          case 'month_best_day':
            var award = this.__awardFactory(count, 'atms/award_best_day.png', 48);
            award.setToolTip(new qx.ui.tooltip.ToolTip(
                this.tr('Most productive day for the month!!!')));
          break;

          case 'week_best_wiki':
            var award = this.__awardFactory(count, 'atms/award_wiki_master.png', 32);
            award.setToolTip(new qx.ui.tooltip.ToolTip(
                this.tr('Most productive wiki editor for the week!')));
          break;

          case 'month_best_wiki':
            var award = this.__awardFactory(count, 'atms/award_wiki_master.png', 48);
            award.setToolTip(new qx.ui.tooltip.ToolTip(
                this.tr('Most productive wiki editor for the month!!!')));
          break;

          case 'week_best_eater':
            var award = this.__awardFactory(count, 'atms/award_best_eater.png', 32);
            award.setToolTip(new qx.ui.tooltip.ToolTip(
                this.tr('Bon appetit!')));
          break;

          case 'month_best_eater':
            var award = this.__awardFactory(count, 'atms/award_best_eater.png', 48);
            award.setToolTip(new qx.ui.tooltip.ToolTip(
                this.tr('Bon appetit fatyy!')));
          break;

          case 'week_best_tester':
            var award = this.__awardFactory(count, 'atms/award_test_terminator.png', 32);
            award.setToolTip(new qx.ui.tooltip.ToolTip(
                this.tr('Your title: Test Termiantor of the week!')));
          break;

          case 'month_best_tester':
            var award = this.__awardFactory(count, 'atms/award_test_terminator.png', 48);
            award.setToolTip(new qx.ui.tooltip.ToolTip(
                this.tr('Your title: Test Termiantor of the month!!!')));
          break;

          case 'week_best_coder':
            var award = this.__awardFactory(count, 'atms/award_best_coder.png', 32);
            award.setToolTip(new qx.ui.tooltip.ToolTip(
                this.tr('Best coder of the week!')));
          break;

          case 'month_best_coder':
            var award = this.__awardFactory(count, 'atms/award_best_coder.png', 48);
            award.setToolTip(new qx.ui.tooltip.ToolTip(
                this.tr('Best coder of the month!!!')));
          break;

          case 'week_best_news':
            var award = this.__awardFactory(count, 'atms/award_news_master.png', 32);
            award.setToolTip(new qx.ui.tooltip.ToolTip(
                this.tr('The most active news editor of the week!')));
          break;

          case 'month_best_news':
            var award = this.__awardFactory(count, 'atms/award_news_master.png', 48);
            award.setToolTip(new qx.ui.tooltip.ToolTip(
                this.tr('The most active news editor of the month!!!')));
          break;

          case 'week_best_manager':
            var award = this.__awardFactory(count, 'atms/award_best_manager.png', 32);
            award.setToolTip(new qx.ui.tooltip.ToolTip(
                this.tr('Test manager of the week!')));
          break;

          case 'month_best_manager':
            var award = this.__awardFactory(count, 'atms/award_best_manager.png', 48);
            award.setToolTip(new qx.ui.tooltip.ToolTip(
                this.tr('Test manager of the month!!!')));
          break;

          case 'week_best_timesheet':
            var award = this.__awardFactory(count, 'atms/award_best_timesheet.png', 32);
            award.setToolTip(new qx.ui.tooltip.ToolTip(
                this.tr('Timesheet of the week!')));
          break;

          case 'month_best_timesheet':
            var award = this.__awardFactory(count, 'atms/award_best_timesheet.png', 48);
            award.setToolTip(new qx.ui.tooltip.ToolTip(
                this.tr('Timesheet of the month!!!')));
          break;

          case 'week_above_avg':
            var award = this.__awardFactory(count, 'atms/award_above_avg.png', 32);
            award.setToolTip(new qx.ui.tooltip.ToolTip(
                this.tr("Good job! Higher than the week's average!")));
          break;

          case 'month_above_avg':
            var award = this.__awardFactory(count, 'atms/award_above_avg.png', 48);
            award.setToolTip(new qx.ui.tooltip.ToolTip(
                this.tr("Awesome job!!! Higher than the months's average!")));
          break;

          case 'week_sautis_satan':
            var award = this.__awardFactory(count, 'atms/award_sautis_satan.png', 32);
            award.setToolTip(new qx.ui.tooltip.ToolTip(
                this.tr('SAUTis tamer! You are so harsh for vendors!')));
          break;

          case 'month_sautis_satan':
            var award = this.__awardFactory(count, 'atms/award_sautis_satan.png', 48);
            award.setToolTip(new qx.ui.tooltip.ToolTip(
                this.tr('Flooder! The ruler of the SAUTis! Vendors are undoubtedly afraid of you!!!')));
          break;

          case 'week_chatterbox':
            var award = this.__awardFactory(count, 'atms/award_chatterbox.png', 32);
            award.setToolTip(new qx.ui.tooltip.ToolTip(
                this.tr('You talk a lot! Everyone hopes it does make sense! :)')));
          break;

          case 'month_chatterbox':
            var award = this.__awardFactory(count, 'atms/award_chatterbox.png', 48);
            award.setToolTip(new qx.ui.tooltip.ToolTip(
                this.tr('Damn! You chat so fucking much!')));
          break;

          case 'milord':
            var award = this.__awardFactory(count, 'atms/award_milord.png', 48);
            award.setToolTip(new qx.ui.tooltip.ToolTip(
                this.tr('Yes my Lord?')));
          break;
        }
      }, this);
    }
  }
});
