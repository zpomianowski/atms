/* ************************************************************************

   Copyright:
   2009 ACME Corporation -or- Your Name, http://www.example.com

   License:
   LGPL: http://www.gnu.org/licenses/lgpl.html
   EPL: http://www.eclipse.org/org/documents/epl-v10.php
   See the LICENSE file in the project's top-level directory for details.

   Authors:
   * Zbigniew Pomianowski

************************************************************************ */

/**
 * This is the main class of contribution 'atms'
 *
 * TODO: Replace the sample code of a custom button with the actual code of
 * your contribution.
 *
 * @asset(atms/*)
 */
qx.Class.define('atms.editor.FullEditor',
{
  extend: qx.ui.container.Composite,

  properties: {
    widget: {}
  },

  construct: function(resource_id, readOnly, getR, putR) {
    this.base(arguments);
    this.__resourceId = resource_id;
    this.__readOnly = readOnly || false;
    this.__get_route = getR || '/atms/manager/download/atms_attachments';
    this.__save_route = putR || '/atms/manager/file_api/atms_attachments';
    this.base(arguments);
    this.setLayout(new qx.ui.layout.VBox());

    // Setup Toolbars
    var bar = new qx.ui.toolbar.ToolBar();
    var part1 = new qx.ui.toolbar.Part();
    var part2 = new qx.ui.toolbar.Part();

    var undoButton = new qx.ui.toolbar.Button(this.tr('Undo'), 'atms/undo.png');
    var redoButton = new qx.ui.toolbar.Button(this.tr('Redo'), 'atms/redo.png');
    var saveButton = new qx.ui.toolbar.Button(this.tr('Save'), 'atms/save.png');
    saveButton.setEnabled(!readOnly);
    var refreshButton = new qx.ui.toolbar.Button(this.tr('Sync'), 'atms/refresh.png');
    var helpButton = new qx.ui.toolbar.Button('ATMS', 'atms/question.png');
    var apidocButton = new qx.ui.toolbar.Button('API', 'atms/question.png');
    var appiumButton = new qx.ui.toolbar.Button('Appium', 'atms/question.png');
    var pyAppiumButton = new qx.ui.toolbar.Button('Appium-Python', 'atms/question.png');
    part1.add(undoButton);
    part1.add(redoButton);
    part1.add(saveButton);
    part1.add(refreshButton);
    part2.add(helpButton);
    part2.add(apidocButton);
    part2.add(appiumButton);
    part2.add(pyAppiumButton);

    bar.add(part1);
    bar.add(part2);

    if (!this.__readOnly)
      this.add(bar);
    var w = new atms.editor.Editor();
    this.setWidget(w);
    atms.editor.Editor.loadAce(function() {
      w.init();
      w.setCode(this.tr('Please wait...'));
      w.setReadOnly(this.__readOnly);
      if (!this.__readOnly) {
        w.setSaveClbk(qx.lang.Function.bind(this.saveFile, this));
      }

      if (this.__resourceId) this.syncFile();
    }, this);

    this.add(this.getWidget(), { flex: 1 });

    undoButton.addListener('execute', function() {
      this.getWidget().getEditor().undo();
    }, this);

    redoButton.addListener('execute', function() {
      this.getWidget().getEditor().redo();
    }, this);

    saveButton.addListener('execute', function() {
      this.saveFile();
    }, this);

    refreshButton.addListener('execute', function() {
      this.syncFile();
    }, this);

    helpButton.addListener('execute', function() {
      qx.bom.Window.open('http://atms.lab.redefine.pl/atms/default/wiki/scripting-testnode');
    });

    apidocButton.addListener('execute', function() {
      qx.bom.Window.open('http://atms.lab.redefine.pl/atms/static/docs/tester/tester.lab.html');
    });

    appiumButton.addListener('execute', function() {
      qx.bom.Window.open('http://appium.io/slate/en/master/?python#');
    });

    pyAppiumButton.addListener('execute', function() {
      qx.bom.Window.open('https://github.com/appium/python-client');
    });

    this.__setup();
  },

  members: {
    __R: null,
    __ws: null,

    __setup: function() {
      var that = this;
      this.__R = new qx.io.rest.Resource({
        get: {
          method: 'GET',
          url: that.__get_route + '/{id}'
        },
        put: {
          method: 'PUT',
          url: that.__save_route + '/{id}'
        }
      });
      this.__R.configureRequest(function(req) {
        if (!that.__readOnly)
          req.setCache(false);
      }, this);

      this.__R.addListener('putSuccess', function(e) {
        var response = e.getData();
        if (response == 'None') {
          jQuery('.flash').html(this.tr('File saved').toString());
          jQuery('.flash').show();
        } else {
          response = JSON.parse(response);
          var msg = response.content.caption + '\n\n' +
              this.tr('Errors') + ': ' +
              response.content.errors_count + '\n' +
              this.tr('Warnings') + ': ' +
              response.content.warnings_count + '\n';
          var data = response.content.data;
          for (var k in data) {
            msg += String(data[k].line + new Array(15).join(' ')).slice(0, 15);
            msg += String(data[k].incident + new Array(15).join(' ')).slice(0, 15);
            msg += data[k].desc + '\n';
          }

          var dialog = new atms.WinConfirmation(
            this.tr('Syntax errors!'),
            msg, null, null, null, {
              width: 800,
              height: 600,
              textStyle: 'textarea-code'
            });
          this.getApplicationRoot().add(dialog);
          dialog.open();
          jQuery('.flash').html(this.tr('Cannot save the file!').toString());
          jQuery('.flash').show();
        }
      }, this);

      this.__R.addListener('getSuccess', function(e) {
        this.getWidget().setCode(e.getData());
      }, this);
    },

    syncFile: function() {
      this.__R.get({ id: this.__resourceId });
    },

    registerWsSource: function(channel) {
      var that = this;
      this.__ws = new atms.WebSocket(null, channel, function(e) {
        if (e.data)
            that.getWidget().setCode(e.data);
      });
    },

    saveFile: function() {
      var that = this;
      var caption = this.tr('Save changes?');
      var desc = this.tr('This action cannot be undone.' +
          'Changes will be applied for all cases and templates tied with this attachemnt.');
      var dialog = new atms.WinConfirmation(
        caption,
        desc,
        function() {
          that.__R.put({ id: that.__resourceId },
              that.getWidget().getCode());
        });

      this.getApplicationRoot().add(dialog);
      dialog.open();
    },

    destroy: function() {
      if (this.__ws) this.__ws.close();
      arguments.callee.base.apply(this, arguments);
    }
  }
});
