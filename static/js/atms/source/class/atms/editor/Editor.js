/* ************************************************************************

   qooxdoo - the new era of web development

   http://qooxdoo.org

   Copyright:
     2008 1&1 Internet AG, Germany, http://www.1und1.de

   License:
     MIT: https://opensource.org/licenses/MIT
     See the LICENSE file in the project's top-level directory for details.

   Authors:
     * Martin Wittemann (martinwittemann)

************************************************************************ */

/**
 * Container for the source code editor.
 *
 * @asset(atms/*)
 * @ignore(ace.*, require)
 */
qx.Class.define('atms.editor.Editor',
{
  extend: qx.ui.container.Composite,
  include: qx.ui.core.MBlocker,

  statics: {
    loadAce: function(clb, ctx) {
      var resource = [
        'atms/editor/ace.js',
        'atms/editor/theme-monokai.js',
        'atms/editor/mode-python.js',
        'atms/editor/ext-searchbox.js'
      ];
      var load = function(list) {
        if (list.length == 0) {
          clb.call(ctx);
          return;
        }

        var res = list.shift();
        var uri = qx.util.ResourceManager.getInstance().toUri(res);
        var loader = new qx.bom.request.Script();
        loader.onload = function() {
          load(list);
        };

        loader.open('GET', uri);
        loader.send();
      };

      load(resource);
    }
  },

  construct: function()
  {
    this.base(arguments);
  },

  events:
  {
    /**
     * Event for signaling that the highlighting could not be done by the editor.
     */
    disableHighlighting: 'qx.event.type.Event'
  },

  properties:
  {
    readOnly: {
      check: 'Boolean',
      init: false,
      apply: '_applyReadOnly',
      event: 'changeReadOnly'
    },

    value: {
      event: 'changeValue',
      apply: '_applyValue'
    },

    simple: {
      check: 'Boolean',
      init: false,
      apply: '_applySimple',
      event: 'changeSimple'
    }
  },

  members:
  {
    __textarea: null,
    __highlighted: null,
    __editor: null,
    __ace: null,
    __errorLabel: null,
    __save_clbk: null,

    /**
     * The constructor was spit up to make the included mixin available during
     * the init process.
     */
    init: function()
    {
      this.setBackgroundColor('white');

      // If widgets are added to the container, the zIndex of the editor blocker
      // is set to 100. This makes possible to resize the splitpanes
      this.addListener('addChildWidget', function() {
        this.getBlocker().getBlockerElement().setStyles({ zIndex: 100 });
      }, this);

      // layout stuff
      var layout = new qx.ui.layout.VBox();
      this.setLayout(layout);
      this.setDecorator('main');
      this.setMinHeight(250);

      // caption
      var dec = new qx.ui.decoration.Decorator().set({
        widthBottom: 1,
        colorBottom: 'border-separator'
      });
      var caption = new qx.ui.container.Composite().set({
        padding: 5,
        allowGrowX: true,
        allowGrowY: true,
        backgroundColor: 'white',
        decorator: dec
      });
      this.add(caption);

      // configure caption
      caption.setLayout(new qx.ui.layout.HBox(10));
      caption.add(new qx.ui.basic.Label(this.tr('Source Code')).set(
        { font: 'bold' }));
      this.__errorLabel = new qx.ui.basic.Label().set(
        { textColor: 'red' });
      caption.add(this.__errorLabel);

      // no use case for caption... for now, maybe in future
      caption.setVisibility('excluded');

      // plain text area
      this.__textarea = new qx.ui.form.TextArea().set({
        wrap: false,
        font: qx.bom.Font.fromString('14px monospace'),
        backgroundColor: 'white',
        padding: [0, 0, 0, 5],
        decorator: null
      });
      this.add(this.__textarea, { flex: 1 });

      this.__editor = new qx.ui.core.Widget();
      var highlightDisabled = false;
      var badIE = qx.core.Environment.get('engine.name') == 'mshtml';
      if (badIE) {
        badIE = parseFloat(qx.core.Environment.get('browser.version')) <= 8 ||
          qx.core.Environment.get('browser.documentmode') <= 8;
      }

      var opera = qx.core.Environment.get('engine.name') == 'opera';

      // FF2 does not have that...
      if (!document.createElement('div').getBoundingClientRect || badIE || opera || !window.ace) {
        this.fireEvent('disableHighlighting');
        highlightDisabled = true;
      } else {
        this.__editor.addListenerOnce('appear', function() {
          this.__onEditorAppear();
        }, this);
      }

      this.__editor.setVisibility('excluded');
      this.add(this.__editor, { flex: 1 });

      // override the focus border CSS of the editor
      qx.bom.Stylesheet.createElement(
        '.ace_editor {border: 0px solid #9F9F9F !important; font-size: 16px; }'
      );

      // chech the initial highlight state
      var shouldHighligth = qx.bom.Cookie.get('playgroundHighlight') !== 'false';
      this.useHighlight(!highlightDisabled && shouldHighligth);
    },

    /**
     * This code part uses the ajax.org code editor library to add a
     * syntax-highlighting editor as an textarea replacement
     *
     * @ignore(ace.edit, require)
     */
    __onEditorAppear: function() {
      // timout needed for chrome to not get the ACE layout wrong and show the
      // text on top of the gutter
      qx.event.Timer.once(function() {
        var container = this.__editor.getContentElement().getDomElement();

        // create the editor
        var editor = this.__ace = ace.edit(container);
        editor.setDisplayIndentGuides(true);
        editor.setTheme('ace/theme/monokai');

        // set python mode
        var PythonScriptMode = require('ace/mode/python').Mode;
        editor.getSession().setMode(new PythonScriptMode());

        // configure the editor
        var session = editor.getSession();
        session.setUseSoftTabs(true);
        session.setTabSize(4);

        // based on properties
        editor.renderer.setShowGutter(!this.getSimple());
        editor.setReadOnly(this.getReadOnly());

        // copy the inital value
        session.setValue(this.__textarea.getValue() || '');

        var that = this;

        // append resize listener
        this.__editor.addListener('resize', function() {
          // use a timeout to let the layout queue apply its changes to the dom
          window.setTimeout(function() {
            that.__ace.resize();
          }, 0);
        });

        // set special keys
        this.__ace.commands.addCommand({
          name: 'save',
          bindKey: { win: 'Ctrl-S', mac: 'Cmd-S' },
          exec: function(editor) {
            if (typeof (that.__save_clbk) == 'function')
              that.__save_clbk();
          }

        });

      }, this, 500);
    },

    _applyReadOnly: function(value, old) {
      if (!this.__ace) return;
      if (value) this.__ace.setReadOnly(true);
      else this.__ace.setReadOnly(false);
    },

    _applySimple: function(value, old) {
      if (!this.__ace) return;
      if (value) this.__ace.setShowGutter(false);
      else this.__ace.setReadOnly(true);
    },

    _applyValue: function(value, old) {
      if (this.__textarea) {
        this.setCode(value);
      } else {
        // property sync is faster than widget is ready :/
        // custom event TODO
        qx.event.Timer.once(function() {
          this.setCode(value);
        }, this, 1000);
      }
    },

    setSaveClbk: function(func) {
      this.__save_clbk = func;
    },

    getEditor: function() {
      return this.__ace;
    },

    /**
     * Returns the current set code of the editor.
     * @return {String} The current set text.
     */
    getCode: function() {
      if (this.__highlighted && this.__ace) {
        return this.__ace.getSession().getValue();
      } else {
        return this.__textarea.getValue();
      }
    },

    /**
     * Sets the given code to the editor.
     * @param code {String} The new code.
     */
    setCode: function(code) {
      if (this.__ace) {
        this.__ace.getSession().setValue(code);

        // move cursor to start to prevent scrolling to the bottom
        this.__ace.renderer.scrollToX(0);
        this.__ace.renderer.scrollToY(0);
        this.__ace.selection.moveCursorFileStart();
      }

      this.__textarea.setValue(code);
    },

    /**
     * Displays the given error in the caption bar.
     * @param ex {Exception} The exception to display.
     */
    setError: function(ex) {
      this.__errorLabel.setValue(ex ? ex.toString() : '');
    },

    /**
     * Switches between the ajax code editor editor and a plain textarea.
     * @param value {Boolean} True, if the code editor should be used.
     */
    useHighlight: function(value) {
      this.__highlighted = value;

      if (value) {
        // change the visibility
        this.__editor.setVisibility('visible');
        this.__textarea.setVisibility('excluded');

        // copy the value, if the editor already availabe
        if (this.__ace) {
          this.__ace.getSession().setValue(this.__textarea.getValue());
        }
      } else {
        // change the visibility
        this.__editor.setVisibility('excluded');
        this.__textarea.setVisibility('visible');

        // copy the value, if the editor already availabe
        if (this.__ace) {
          this.__textarea.setValue(this.__ace.getSession().getValue());
        }
      }
    }
  },

  /*
   *****************************************************************************
      DESTRUCTOR
   *****************************************************************************
   */

  destruct: function()
  {
    this._disposeObjects('__textarea');
    this.__ace = null;
  }
});
