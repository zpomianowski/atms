qx.Mixin.define('atms.MParent',
{
  properties:
  {
    parentNode:
    {
      init: null,
      nullable: true
    }
  }
});
