/**
*@asset(atms/*)
*@asset(qx/icon/${qx.icontheme}/*)
*/

qx.Class.define('atms.UserList',
{
  extend: qx.ui.container.Composite,

  construct: function(desktop) {
    this.base(arguments);
    this.__desktop = desktop;

    this.setLayout(new qx.ui.layout.VBox(4));
    this.__nf = new atms.TimeFormat();

    this.__uFilter = new qx.ui.form.TextField().set(
      {
        placeholder: this.tr('Filter users') 
      }
    );

    this.__list = new qx.ui.list.List();
    this.__list.setSelectable(true);
    this.__list.setItemHeight(68);
    this.add(this.__uFilter);
    this.add(this.__list, { flex: 1 });

    this.__uFilter.addListener('changeValue', function() {
      this.__list.refresh();
    }, this);

    // configuration
    this.__setupResources();
    this.__setupStores();
    this.__setupBindings();

    this.setDraggable(true);
    this.setDroppable(true);
    this.addListener('dragstart', this._handleDragStart, this);
    this.addListener('droprequest', this._handleDropRequest, this);
    this.addListener('drop', this._handleDrop, this);
    this.addListener('drag', this.__onDrag, this);
    this.addListener('dragend', this.__onDragEnd, this);

    this.setContextMenu(this.__getMenu());

    this.__list.addListener('dblclick', function(e) {
      this.__list.getSelection().forEach(function(item) {
        this.__openCasesForUser(item.getId(), item.getLabel());
      }, this);
    }, this);

    this.__dragFeedback = new atms.DragFeedback(this.tr('Assign users to test/case'));
    qx.core.Init.getApplication().getRoot().add(this.__dragFeedback);
  },

  events:
    {
      /** (Fired by {@link qx.ui.form.List}) */
      action: 'qx.event.type.Event'
    },

  members: {
    __nf:        null,
    __desktop:   null,
    __userR:     null,
    __usersR:    null,
    __userS:     null,
    __usersS:    null,
    __caseR:     null,
    __notifyR:   null,

    __uFilter:   null,
    __list:      null,

    __menu:         [],
    __contextMenu:  null,

    __dragFeedback: null,

    __setupResources: function() {
      this.__userR = new qx.io.rest.Resource({
        get: { method: 'GET',    url: '/atms/manager/uapi/user/{id}.json' },
        put: {
          method: 'PUT',
          url: '/atms/manager/uapi/user/{id}/{role}/{to_remove}.json' },
        del: {
          method: 'DELETE',
          url: '/atms/manager/uapi/user/{id}/{is_active}.json' }
      });
      this.__usersR = new qx.io.rest.Resource({
        get:   { method: 'GET',    url: '/atms/manager/uapi/users.json' },
        addTask: {
            method: 'POST',
            url: '/atms/manager/task_api/{task}?{params}' }
      });
      this.__caseR = new qx.io.rest.Resource({
        post:   { method: 'POST',    url: '/atms/manager/api/atms_cases{param}' },
        update:   { method: 'PUT',     url: '/atms/manager/api/atms_cases{param}' }
      });
      this.__notifyR = new qx.io.rest.Resource({
        get:   { method: 'GET',    url: '/atms/manager/notifyUser/{id}' }
      });
      this.__notifyR.addListener('getSuccess', function() {
        jQuery('.flash').html(this.tr('User notified').toString());
        jQuery('.flash').show();
      }, this);

      this.__usersR.addListener('addTaskSuccess', function() {
        this.__usersR.get();
        jQuery('.flash').html(this.tr('Task added').toString());
        jQuery('.flash').show();
      }, this);

      this.__userR.addListener('putSuccess', function() {
        this.__usersR.get();
        jQuery('.flash').html(this.tr('User roles changed!').toString());
        jQuery('.flash').show();
      }, this);

      this.__userR.addListener('delSuccess', function() {
        this.__usersR.get();
        jQuery('.flash').html(this.tr('User state changed!').toString());
        jQuery('.flash').show();
      }, this);
    },

    __setupStores: function() {
      this.__userS  = new qx.data.store.Rest(this.__userR, 'get');
      this.__usersS = new qx.data.store.Rest(this.__usersR, 'get', {
        manipulateData: function(data) {
          data.content.forEach(function(item) {
            item.icon = 'test';
          });

          return data;
        }
      });
    },

    __setupBindings: function() {
      var that = this;

      var userList   = this.__list;
      var userStore  = this.__userS;
      var usersStore = this.__usersS;

      userList.setDelegate({
        filter: function(user) {
          var test = that.__uFilter.getValue();
          if (!test) return true; 
          var test = test.toLowerCase().split(',');
          var fname = (user.getFirst_name() || '').toLowerCase();
          var lname = (user.getLast_name() || '').toLowerCase();
          var result = false;
          var tmp = [];
          test.forEach(function(item) {
              if (!item) return;
              item = item.trim();
              var r = (fname.indexOf(item) >= 0 || lname.indexOf(item) >= 0);
              result |= r;
              tmp.push(r);
          });
          return result;
        },

        createItem: function() {
          return new atms.UserView(that.__nf, true);
        },

        bindItem: function(controller, item, id) {
          controller.bindProperty('id', 'userID', null, item, id);
          controller.bindProperty('label', 'label', null, item, id);
          controller.bindProperty('identicon', 'icon', {
            converter: function(value, model) {
              if (value.substring(0, 5) == 'atms/')
                  return value;

              return '/atms/manager/../default/download/' + value;
            }
          }, item, id);
          controller.bindProperty('itemsNb', 'itemsNb', null, item, id);
          controller.bindProperty('itemsTotal', 'itemsTotal', null, item, id);
          controller.bindProperty('timeOccupance', 'timeOccupance', null, item, id);
          controller.bindProperty('timeOccupanceTotal', 'timeOccupanceTotal', null, item, id);
          controller.bindProperty('color', 'color', null, item, id);
          controller.bindProperty('lastUpdate', 'lastUpdate', null, item, id);
          controller.bindProperty('state', 'specialState', null, item, id);
        },

        configureItem: function(item) {
          item.getChildControl('icon').setWidth(50);
          item.getChildControl('icon').setHeight(50);
          item.getChildControl('icon').setScale(true);
          item.setDroppable(true);
          var anim = {
            duration: 100,
            keep: 100,
            keyFrames: {
              0:   { translate: ['0px', '0px'] },
              100: { translate: ['4px', '0px'] }
            },
            origin: '50% 50%',
            repeat: 1,
            timing: 'ease-out',
            alternate: false
          };
          item.addListener('pointerover', function(e) {
            var el = this.getContentElement().getDomElement();
            qx.bom.element.Animation.animate(el, anim);
          }, item);

          item.addListener('pointerout', function(e) {
            var el = this.getContentElement().getDomElement();
            qx.bom.element.Animation.animateReverse(el, anim);
          }, item);
        }
      });

      usersStore.bind('model.content', userList, 'model', {
        converter: function(value) {
          return value || new qx.data.Array();
        }
      });

      userList.setSelectionMode('multi');

      this.__usersR.get();
    },

    __getMenu: function() {
      var that = this;
      var menu = new qx.ui.menu.Menu();

      var refreshButton = new qx.ui.menu.Button(this.tr('Refresh'),
          'atms/refresh.png');
      var gameButton = new qx.ui.menu.Button('Play!',
          'atms/gamepad.png');
      var notifyUser = new qx.ui.menu.Button(this.tr('Notify user'),
          'atms/envelope.png');
      var notifyUsers = new qx.ui.menu.Button(this.tr('Notify users'),
          'atms/envelope.png');
      var talk = new qx.ui.menu.Button(this.tr('Talk'),
          'atms/bubbles.png');
      var newCase = new qx.ui.menu.Button(this.tr('New case'),
          'atms/sc.png');
      var userDetails = new qx.ui.menu.Button(this.tr('Show details'),
          'atms/details.png');
      var openCaseManagerButton = new qx.ui.menu.Button(this.tr('Open cases for this user'),
          'atms/cases.png');
      var openLooseCaseManagerButton = new qx.ui.menu.Button(this.tr('Open unassigned cases'),
          'atms/cases.png');
      var userMngr = new qx.ui.menu.Button(this.tr('Manage permissions'),
          'atms/key.png');
      var menu_ranks = new qx.ui.menu.Menu();
      var ranks_week = new qx.ui.menu.Button(this.tr('Last week'),
          'atms/trending-up.png');
      var ranks_month = new qx.ui.menu.Button(this.tr('Last month'),
          'atms/trending-up.png');
      var ranks_quarter = new qx.ui.menu.Button(this.tr('Last quarter'),
          'atms/trending-up.png');
      var ranks_halfyear = new qx.ui.menu.Button(this.tr('Last half year'),
          'atms/trending-up.png');
      var ranks_year = new qx.ui.menu.Button(this.tr('Last year'),
          'atms/trending-up.png');
      var ranks_global = new qx.ui.menu.Button(this.tr('All time'),
          'atms/trending-up.png');
      var ranks_recalc = new qx.ui.menu.Button(this.tr('Racalculate the ranking'),
          'atms/cog.png');
      menu_ranks.add(ranks_week);
      menu_ranks.add(ranks_month);
      menu_ranks.add(ranks_quarter);
      menu_ranks.add(ranks_halfyear);
      menu_ranks.add(ranks_year);
      menu_ranks.add(ranks_global);
      menu_ranks.add(new qx.ui.menu.Separator());
      menu_ranks.add(ranks_recalc);
      var ranks = new qx.ui.menu.Button(this.tr('Ranking'),
          'atms/trending-up.png', null, menu_ranks);

      notifyUser.addListener('click', function() {
        that.__list.getSelection().forEach(function(item) {
          that.__notifyR.get({ id: item.getId() });
        });
      }, this);

      notifyUsers.addListener('click', function() {
        this.__notifyR.get({ id: 0 });
      }, this);

      talk.addListener('click', function() {
        var chat_members = [];
        var chat_members_labels = [];
        that.__list.getSelection().forEach(function(item) {
          chat_members.push(item.getId());
          chat_members_labels.push(item.getLabel());
        });

        var widget = new atms.Comments('users', chat_members);
        var win = new atms.WinPanel(widget, chat_members_labels.join());
        that.__desktop.add(win);
        win.open();
      });

      newCase.addListener('click', function() {
        this.__addNewCase();
      }, this);

      userDetails.addListener('click', function() { this.__userDetails(); }, this);

      refreshButton.addListener('click', function() { this.__usersR.get(); }, this);

      gameButton.addListener('click', function() { this.__gameification(); }, this);

      openCaseManagerButton.addListener('click', function() {
        that.__list.getSelection().forEach(function(item) {
          that.__openCasesForUser(item.getId(), item.getLabel());
        });
      });

      openLooseCaseManagerButton.addListener('click', function() {
        that.__openCasesForUser('None');
      }, this);

      ranks_week.addListener('execute',
          function() { this.__openRanking('week'); }, this);

      ranks_month.addListener('execute',
          function() { this.__openRanking('month'); }, this);

      ranks_quarter.addListener('execute',
          function() { this.__openRanking('quarter'); }, this);

      ranks_halfyear.addListener('execute',
          function() { this.__openRanking('half-year'); }, this);

      ranks_year.addListener('execute',
          function() { this.__openRanking('year'); }, this);

      ranks_global.addListener('execute',
          function() { this.__openRanking('global'); }, this);

      ranks_recalc.addListener('execute', function() {
        this.__usersR.addTask({
          task: 'calculateAwards',
          params: 'group_name=main'
        });
      }, this);

      menu.add(notifyUser);
      menu.add(new qx.ui.menu.Separator());
      menu.add(talk);
      menu.add(new qx.ui.menu.Separator());
      menu.add(newCase);
      menu.add(new qx.ui.menu.Separator());
      menu.add(userDetails);
      menu.add(openCaseManagerButton);
      menu.add(openLooseCaseManagerButton);
      menu.add(new qx.ui.menu.Separator());
      menu.add(userMngr);
      menu.add(notifyUsers);
      menu.add(new qx.ui.menu.Separator());
      menu.add(ranks);
      menu.add(new qx.ui.menu.Separator());
      menu.add(gameButton);
      menu.add(new qx.ui.menu.Separator());
      menu.add(refreshButton);

      this.addListener('beforeContextmenuOpen', function() {
        userMngr.setEnabled(false);
        var that = this;
        var req = new qx.io.request.Xhr(
          '/atms/manager/get_basic_permissions.json');
        req.addListener('success', function(e) {
          var perms = e.getTarget().getResponse().content;
          if (!perms.users.del || this.__list.getSelection().length == 0)
            return;

          userMngr.setEnabled(true);
          var user_menu = new qx.ui.menu.Menu();

          var group_map = {
            atms_admin: this.tr('user manager'),
            atms_tester: this.tr('tester'),
            atms_manager: this.tr('manager'),
            TimesheetAdmin: this.tr('timesheet admin'),
            wiki_editor: this.tr('wiki_editor'),
            wiki_author: this.tr('wiki_author')
          };

          var groups = this.__list.getSelection().getItem(0).getGroups();
          for (var key in group_map) {
            var already_is = groups.indexOf(key) !== -1;
            var prefix = already_is ?
              this.tr('Remove role') : this.tr('Add role');
            var button = new qx.ui.menu.Button(
              prefix + ' ' + group_map[key],
              already_is ? 'atms/thumbs-o-down.png' : 'atms/thumbs-o-up.png');
            (function() {
              var k = key;
              var a = already_is;
              button.addListener('execute',
                function() { that.__updateUserGroup(k, a); });
            })();

            user_menu.add(button);
          }

          var is_active = this.__list.getSelection().getItem(0).getIs_active();
          var button = new qx.ui.menu.Button(
            is_active ? this.tr('Deactivate user') :
            this.tr('Activate user'), 'atms/warning.png');
          button.addListener('execute',
            function() { that.__toggleUserState(is_active); });

          user_menu.add(new qx.ui.menu.Separator());
          user_menu.add(button);

          userMngr.setMenu(user_menu);
        }, this);

        req.send();
      }, this);

      return menu;
    },

    __updateUserGroup: function(key, remove) {
      var user_id = this.__list.getSelection().getItem(0).getId();
      this.__userR.put({
        id: user_id,
        role: key,
        to_remove: remove
      });
    },

    __toggleUserState: function(is_active) {
      this.__userR.del({
        id: this.__list.getSelection().getItem(0).getId(),
        is_active: is_active });
    },

    __openRanking: function(type) {
      var widget = new atms.RankList(type);
      var caption = 'Ranking';
      switch (type) {
        case 'week':
          caption = this.tr('Ranking of the week');
        break;

        case 'month':
          caption = this.tr('Ranking of the month');
        break;

        case 'quarter':
          caption = this.tr('Ranking of the quarter');
        break;

        case 'half-year':
          caption = this.tr('Ranking of the half-year');
        break;

        case 'year':
          caption = this.tr('Ranking of the year');
        break;
      }
      var win = new atms.WinPanel(widget, caption);
      this.__desktop.add(win);
      win.open();
    },

    __userDetails: function() {
      var users = [];
      this.__list.getSelection().forEach(function(item) {
        users.push(item.getId());
      }, this);

      var scroll = new qx.ui.container.Scroll();
      var container = new qx.ui.container.Composite(new qx.ui.layout.VBox());
      var widget1 = new atms.Chart({
        layeredBars: {
          args: ['stack', 'linear'],
          url: '/atms/manager/../stats/call/json/user_exp',
          params: {
            user: {
              type: 'select',
              multi: true,
              url: '/atms/manager/uapi/users.json',
              path: 'label',
              filterp: 'Label',
              value: users
            },
            dfrom: {
              type: 'date',
              value: -7
            },
            dto: {
              type: 'date',
              value: 0
            }
          }
        }
      });
      var widget2 = new atms.Chart({
        layeredBars: {
          args: ['group', 'linear'],
          url: '/atms/manager/../stats/call/json/cases',
          params: {
            user: {
              type: 'select',
              multi: true,
              url: '/atms/manager/uapi/users.json',
              path: 'label',
              filterp: 'Label',
              value: users
            },
            test: {
              type: 'select',
              multi: true,
              url: '/atms/manager/api/atms_tests.json',
              path: 'name',
              filterp: 'Name',
              value: ['_all']
            },
            groupby: {
              type: 'select',
              multi: true,
              model: [
                  { id: 'atms_auth_user.id', value: false, label: this.tr('User') },
                  { id: 'atms_tests.name', value: false, label: this.tr('Test') },
                  { id: 'atms_cases.status', value: false, label: this.tr('Status') },
                  { id: 'atms_cases.weight', value: false, label: this.tr('Weight') }
              ],
              path: 'label',
              filterp: 'Label',
              value: ['atms_tests.name', 'atms_auth_user.id']
            },
            layer: {
              type: 'select',
              multi: true,
              model: [
                  { id: 'atms_auth_user.id', value: false, label: this.tr('User') },
                  { id: 'atms_tests.name', value: false, label: this.tr('Test') },
                  { id: 'atms_cases.status', value: false, label: this.tr('Status') },
                  { id: 'atms_cases.weight', value: false, label: this.tr('Weight') }
              ],
              path: 'label',
              filterp: 'Label',
              value: ['atms_cases.status']
            },
            status: {
              type: 'select',
              multi: true,
              model: [
                  { id: 'new', value: true, label: this.tr('New') },
                  { id: 'pending', value: true, label: this.tr('Pending') },
                  { id: 'aborted', value: true, label: this.tr('Aborted') },
                  { id: 'impossible', value: true, label: this.tr('Impossible') },
                  { id: 'failed', value: true, label: this.tr('Failed') },
                  { id: 'passed', value: true, label: this.tr('Passed') }
              ],
              path: 'label',
              filterp: 'Label',
              value: []
            },
            aggr: {
              type: 'select',
              multi: false,
              model: [
                  { id: 'COUNT(atms_cases.id)', value: false, label: this.tr('Count') },
                  { id: 'MAX(atms_cases.weight)', value: false, label: this.tr('Weight MAX') },
                  { id: 'MIN(atms_cases.weight)', value: false, label: this.tr('Weight MIN') },
                  { id: 'MAX(atms_cases.expected_time)', value: false, label: this.tr('Expected time MAX') },
                  { id: 'MIN(atms_cases.expected_time)', value: false, label: this.tr('Expected time MIN') },
                  { id: 'MAX(atms_cases.result_time)', value: false, label: this.tr('Result time MAX') },
                  { id: 'MIN(atms_cases.result_time)', value: false, label: this.tr('Result time MIN') }
              ],
              path: 'label',
              filterp: 'Label',
              value: ['COUNT(atms_cases.id)']
            }
          }
        }
      });
      container.add(widget1, { flex: 1 });
      container.add(widget2, { flex: 1 });
      scroll.add(container);
      scroll.classname = 'atms.Chart';
      var win = new atms.WinPanel(scroll);
      this.__desktop.add(win);
      win.open();
    },

    __addNewCase: function(data) {
      var widget = new atms.Form('atms_cases', {
        user: 1
      }, 'create',
          null, null);
      var win = new atms.WinPanel(widget);
      this.__desktop.add(win);
      win.open();
    },

    __gameification: function() {
      var game = new atms.Gamification();
      var caption = 'Gamification';
      var win  = new atms.WinPanel(game, caption);
      this.__desktop.add(win);
      win.open();
    },

    __openCasesForUser: function(user, userName) {
      var cases = new atms.table.TableCase(null, user);
      var caption = user == 'None' ?
          this.tr('Unassigned cases') :
          this.tr('Assigned cases to') + ' ' + userName;
      var win  = new atms.WinPanel(cases, caption);
      this.__desktop.add(win);
      win.open();
    },

    __resetContextMenu: function(e) {
      var currentList = qx.ui.core.FocusHandler.getInstance().getFocusedWidget();
      this.__menu['newuserButton'].setEnabled(false);
      this.__menu['newSubstanceButton'].setEnabled(false);
      this.__menu['newuserButton'].setEnabled(false);
      if (currentList === this.__pList) this.__menu['newuserButton'].setEnabled(true);
      if (currentList === this.__sList) this.__menu['newSubstanceButton'].setEnabled(true);
      if (currentList === this.__iList) this.__menu['newuserButton'].setEnabled(true);
    },

    _handleDragStart: function(e) {
      e.addType('sender');
      e.addType('target');
      e.addType('users');
      e.addAction('move');

      var that = this;
      var selection = this.__list.getSelection();
      if (selection.length === 0) {
        if (e.getDragTarget().classname !== 'atms.UserView') return;
        this.__list.getModel().forEach(function(item) {
          if (e.getDragTarget().getUserID() == item.getId())
            selection.push(item);
        }, this);
      }

      selection.forEach(function(item) {
        var itemWidget = new atms.UserView(that.__nf);
        itemWidget.set({
          height: 32,
          label: item.getLabel(),
          itemsNb: item.getItemsNb(),
          itemsTotal: item.getItemsTotal(),
          color: item.getColor()

          //icon: '/atms/manager/../default/download/' + item.getIdenticon()
        });

        // itemWidget.getChildControl('icon').setWidth(32);
        // itemWidget.getChildControl('icon').setHeight(32);
        // itemWidget.getChildControl("icon").setScale(true);
        that.__dragFeedback.addDragChild(itemWidget);
      });
    },

    __onDrag: function(e) {
      this.__dragFeedback.setDomPosition(e.getDocumentLeft() + 15, e.getDocumentTop() + 15);
    },

    __onDragEnd: function(e) {
      this.__dragFeedback.setDomPosition(-1000, -1000);
      this.__dragFeedback.purgeChildren();
    },

    _handleDropRequest: function(e) {
      e.addData('sender', this);
      e.addData('target', e.getRelatedTarget());
      e.addData('users', this.__list.getSelection());
    },

    _handleDrop: function(e) {
      var that = this;
      var sender = e.getData('sender');
      if (sender instanceof atms.Tree) {
        var user  = e.getData('target');
        var param = e.getData('tree-items-uri');
        var caption = this.tr('Create new cases for user') + ' ' + user.getLabel();
        var desc = this.tr('This action will create a set of cases based on templates and will assign them to this particular user.');
        var dialog = new atms.WinConfirmation(
            caption,
                    desc, function() {
                      param += '&user=' + user.getUserID();
                      var req = new qx.io.request.Xhr('/atms/manager/create_from_nodes?' + param);
                      req.setTimeout(2400000);
                      req.addListener('success', function(e) {
                        jQuery('.flash').html(that.tr('Cases created for user').toString() + ' ' + user.getLabel());
                        jQuery('.flash').show();
                      }, this);

                      req.send();
                    }, null);

        this.getApplicationRoot().add(dialog);
        dialog.open();
      }

      if (sender instanceof atms.table.TableTest) {
        var user  = e.getData('target');
        var tests = e.getData('tests');
        var caption = this.tr('Assign all cases from chosen tests to user') + ' ' + user.getLabel();
        var desc = this.tr('This action will assign all cases from chosen tests to this particular user.');
        var dialog = new atms.WinConfirmation(
            caption,
                    desc, function() {
                      tests.iterateSelection(function(index) {
                        param =  '&test=' + sender.getModel().getRowData(index).id;
                        param += '&user=' + user.getUserID();
                        var req = new qx.io.request.Xhr('/atms/manager/assign_test2user?' + param);
                        req.setTimeout(2400000);
                        req.addListener('success', function(e) {
                          jQuery('.flash').html(that.tr('Cases created for test').toString());
                          jQuery('.flash').show();
                        }, this);

                        req.send();
                      });
                    }, null);

        this.getApplicationRoot().add(dialog);
        dialog.open();
      }

      if (sender instanceof atms.table.TableCase) {
        var user  = e.getData('target').getUserID();
        var cases = e.getData('cases');
        cases.iterateSelection(function(index) {
          var item = sender.getModel().getRowData(index);
          sender._modifyCell(1, index, user, item.user, sender);
        });
      }
    }
  }
});
