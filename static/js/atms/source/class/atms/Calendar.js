/* ************************************************************************

   Copyright:
     2009 ACME Corporation -or- Your Name, http://www.example.com

   License:
     LGPL: http://www.gnu.org/licenses/lgpl.html
     EPL: http://www.eclipse.org/org/documents/epl-v10.php
     See the LICENSE file in the project's top-level directory for details.

   Authors:
     * Your Name (username)

************************************************************************ */

/**
 * This is the main class of contribution 'fullcalendar'
 *
 * TODO: Replace the sample code of a custom button with the actual code of
 * your contribution.
 *
 */
qx.Class.define('atms.Calendar',
{
  extend: dynloadfiles.Contribution,

  /*
  *****************************************************************************
     CONSTRUCTOR
  *****************************************************************************
  */

  /**
   * Create a new custom button
   *
   * @param label {String} Label to use
   * @param icon {String?null} Icon to use
   */
  construct: function(codeArr, cssArr, base_path)
  {
    codeArr = codeArr || [
      'js/bower_components/moment/min/moment.min.js',
      'js/bower_components/jquery/dist/jquery.min.js',
      'js/bower_components/fullcalendar/dist/fullcalendar.min.js',
      'js/bower_components/fullcalendar/dist/lang-all.js'];
    cssArr = cssArr || [
      'js/bower_components/fullcalendar/dist/fullcalendar.min.css'];
    var base_path = base_path || '/atms/manager/../static/';
    this.base(arguments, codeArr, cssArr, base_path);

    qx.bom.Stylesheet.createElement(
      '.fc-button { width: inherit;' +
      'background-color: white !important;' +
      'background-image: none !important; }'
    );

    this.__menu = {};
    this.__cEvent = Object();
    this.setContextMenu(this.__getMenu());
  },

  members: {
    __idx: null,
    __jqEl: null,
    __settings: null,
    __menu: null,
    __cEvent: null,

    __getMenu: function() {
      var menu = new qx.ui.menu.Menu();

      var calcButton = new qx.ui.menu.Button(this.tr('Calculate time'),
        'atms/steam.png');
      var setTimeButton = new qx.ui.menu.Button(this.tr('Edit/View'),
        'atms/edit.png');

      calcButton.addListener('click', function() {
        var req = new qx.io.request.Xhr('/atms/manager/calculate_test_duration', 'PUT');
        req.setRequestData({
          id: this.__cEvent.id
        });
        req.setTimeout(2400000);
        req.addListener('success', function(e) {
          this.__jqEl.fullCalendar('refetchEvents');
        }, this);

        req.send();
      }, this);

      this.__menu['calcButton'] = calcButton;

      menu.add(calcButton);

      return menu;
    },

    refresh: function() {
      this.__jqEl.fullCalendar('refetchEvents');
    },

    addEventSource: function(source) {
      jQuery(this.__idx).fullCalendar('addEventSource', source);
    },

    removeEventSource: function(source) {
      jQuery(this.__idx).fullCalendar('removeEventSource', source);
    },

    _onElementLoaded: function(el) {
      var idx = 'atms-cal-' + dynloadfiles.Contribution.INSTANCE_COUNTER;
      this.__idx = '#' + idx;
      el.setAttribute('id', idx);
      var that = this;
      this.__jqEl = jQuery(this.__idx).fullCalendar({
          lang: qx.locale.Manager.getInstance().getLanguage(),

          // minTime: '9:00:00',
          // maxTime: '17:00:00',
          // weekends: false,
          header:{
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
          },
          defaultView: 'agendaDay',
          eventSources: [
              {
                url: '/atms/manager/get_tests_events.json',
                type: 'GET',
                success: function(data) {
                  return data.content;
                },

                error: function() {
                  console.log('There was an error while fetching events!');
                },

                color: '#008A00'
              }
          ],
          eventDrop: function(event, jsEvent, ui, view) {
            var req = new qx.io.request.Xhr('/atms/manager/api/atms_tests', 'PUT');
            req.setRequestData({
              id: event.id,
              start: event.start.format('YYYY-MM-DD HH:mm:ss'),
              stop: event.end.format('YYYY-MM-DD HH:mm:ss')
            });
            req.setTimeout(2400000);
            req.send();
          },

          eventResize: function(event, delta, revertFunc, jsEvent, ui, view) {
            var req = new qx.io.request.Xhr('/atms/manager/api/atms_tests', 'PUT');
            req.setRequestData({
              id: event.id,
              stop: event.end.format('YYYY-MM-DD HH:mm:ss')
            });
            req.setTimeout(2400000);
            req.send();
          },

          eventRender: function(calEvent, element, icon) {
            element.find('.fc-event-inner').append(calEvent.cases);
            element.bind('mousedown', function(e) {
              if (e.which == 3) {
                that.__cEvent.id = calEvent.id;

              } else {
                that.__cEvent.id = null;
              }
            });
          }
        });

      this.addListener('resize', function(e) {
        var data = e.getTarget().getBounds();
        var w = data.width;
        var h = data.height;
        var aspectRatio = parseFloat(w) / h;

        jQuery(this.__idx).fullCalendar('option', 'height', h);
        jQuery(this.__idx).fullCalendar('render');
      }, this);
    },

    _destroy: function() {
      jQuery(this.__idx).fullCalendar('destroy');
    }
  }
});
