/**
*@asset(atms/*)
*@asset(qx/icon/${qx.icontheme}/*)
*/

qx.Class.define('atms.RankList',
{
  extend: qx.ui.container.Composite,

  construct: function(type) {
    this.base(arguments);

    this.setLayout(new qx.ui.layout.VBox());

    // general
    this.__list = new qx.ui.list.List();
    this.__list.setItemHeight(150);
    this.__list.syncWidget = function(jobs) {
      qx.log.Logger.debug('syncWidget');

      var firstRow = this._layer.getFirstRow();
      var rowSize = this._layer.getRowSizes().length;

      for (var row = firstRow; row < firstRow + rowSize; row++) {
        var widget = this._layer.getRenderedCellWidget(row, 0);
        if (widget != null) {
          var height = widget.getSizeHint().height;
          qx.log.Logger.debug('height: ' + height + ' row:' + row + ' widget: ' + widget.getDay());
          this.getPane().getRowConfig().setItemSize(row, height);
        }
      }
    };

    this.add(this.__list, { flex: 1 });

    // configuration
    this.__setupResources(type);
    this.__setupStores();
    this.__setupBindings();
    this.__R.get();
  },

  members: {
    __R: null,
    __S: null,

    __setupResources: function(type) {
      this.__R = new qx.io.rest.Resource({
        get: {
          method: 'GET',
          url: '/atms/manager/../stats/call/json/user_levels/' + type
        }
      });
    },

    __setupStores: function() {
      this.__S  = new qx.data.store.Rest(this.__R, 'get');
    },

    __setupBindings: function() {
      var that = this;

      var list   = this.__list;
      var store  = this.__S;

      list.setDelegate({
        createItem: function() {
          return new atms.RankView();
        },

        bindItem: function(controller, item, id) {
          controller.bindProperty('color', 'color', null, item, id);
          controller.bindProperty('place', 'place', null, item, id);
          controller.bindProperty('rank', 'rank', null, item, id);
          controller.bindProperty('label', 'label', null, item, id);
          controller.bindProperty('awards', 'awards', {
            converter: function(value) {
              return value || new qx.data.Array();
            }
          }, item, id);
          controller.bindProperty('icon', 'icon', {
            converter: function(value, model) {
              return '/atms/manager/../default/download/' + value;
            }
          }, item, id);
        }
      });

      store.bind('model.content', list, 'model', {
        converter: function(value) {
          return value || new qx.data.Array();
        }
      });
    }
  }
});
