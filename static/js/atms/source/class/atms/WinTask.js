/**
*@asset(atms/*)
*@asset(qx/icon/${qx.icontheme}/*)
*/

qx.Class.define('atms.WinTask',
{
  extend: qx.ui.container.Composite,

  statics: {
    reInfo:  /(.*?)\[(INFO)\]/g,
    reDebug: /(.*?)\[(DEBUG)\]/g,
    reWarn:  /(.*?)\[(WARN)\]/g,
    reError: /(.*?)\[(ERROR)\](.*)/g,
    reSum:   /(STB.*INFO<\/span>)(.*?)(;)/g
  },

  construct: function(task_id) {
    this.__taskID = task_id;
    this.base(arguments);
    var layout = new qx.ui.layout.VBox(5);
    this.setLayout(layout);

    this.__progress = new qx.ui.indicator.ProgressBar().set({
      allowGrowY: false,
      height: 10
    });

    this.__deps = new qx.ui.form.TextField();
    var depsLabel = new qx.ui.basic.Label(this.tr('Job dependencies'));

    this.__outputHolder = new qx.ui.container.Composite(
      new qx.ui.layout.VBox(4));
    this.__output = new qx.ui.embed.Html().set({
      overflowY: 'scroll'
    });
    var outputLabel = new qx.ui.basic.Label(this.tr('Output'));
    this.__outputHolder.add(outputLabel);
    this.__outputHolder.add(this.__output, { flex: 1 });

    this.__resultHolder = new qx.ui.container.Composite(
      new qx.ui.layout.VBox(4)).set({
        visibility: 'excluded'
      });
    this.__result = new qx.ui.form.TextArea();
    var resultLabel = new qx.ui.basic.Label(this.tr('Result'));
    this.__resultHolder.add(resultLabel);
    this.__resultHolder.add(this.__result, { flex: 1 });

    this.__tracebackHolder = new qx.ui.container.Composite(
      new qx.ui.layout.VBox(4)).set({
        visibility: 'excluded'
      });
    this.__traceback = new qx.ui.form.TextArea;
    var tracebackLabel = new qx.ui.basic.Label(this.tr('Traceback'));
    this.__tracebackHolder.add(tracebackLabel);
    this.__tracebackHolder.add(this.__traceback, { flex: 1 });

    this.add(this.__progress);

    this.add(depsLabel);
    this.add(this.__deps);

    var mainHolder = new qx.ui.container.Composite(new qx.ui.layout.HBox(4));
    mainHolder.add(this.__outputHolder, { flex: 1 });
    mainHolder.add(this.__tracebackHolder, { flex: 1 });
    this.add(mainHolder, { flex: 3 });

    this.add(this.__resultHolder, { flex: 1 });

    var actionHolder = new qx.ui.container.Composite();
    actionHolder.setLayout(new qx.ui.layout.HBox(5));

    var actionButton = new qx.ui.form.Button(this.tr('Save'));
    actionButton.addListener('execute', function() {
      var children = this.__deps.getValue().split(',');
      children.forEach(function(item, index) {
        children[index] = parseInt(item);
      });

      var model = qx.data.marshal.Json.createModel(
                {
                  id: task_id,
                  children: children
                });
      var uriParams = qx.util.Serializer.toUriParameter(model);
      this.__R.updateDeps({
        params: '?' + uriParams
      });

    }, this);

    actionHolder.add(new qx.ui.core.Spacer(), { flex: 1 });
    actionHolder.add(actionButton);

    this.add(actionHolder);

    this.__setupResources();
    this.__setupStores();
    this.__setupBindings();

    this.__R.getRun({ id: task_id });
    this.__R.getDeps({ id: task_id });

    // reload timer
    this.__timer = new qx.event.Timer(5000);
    this.__timer.addListener('interval', function() {
      this.__R.getRun({ id: task_id });
    }, this);

    this.__timer.start();
  },

  members: {
    __timer: null,

    __taskID: null,
    __progress: null,
    __output: null,
    __outputHolder: null,
    __result: null,
    __resultHolder: null,
    __traceback: null,
    __tracebackHolder: null,

    __R: null,
    __S: null,
    __Sdeps: null,

    destroy: function() {
      this.__timer.stop();
      arguments.callee.base.apply(this, arguments);
    },

    __setupResources: function() {
      this.__R = new qx.io.rest.Resource({
        getRun: {
          method: 'GET',
          url: '/atms/manager/api/scheduler_run/task_id/{id}.json'
        },
        getDeps: {
          method: 'GET',
          url: '/atms/manager/deps_api/{id}.json'
        },
        updateDeps: {
          method: 'PUT',
          url: '/atms/manager/deps_api{params}'
        }
      });
      this.__R.addListener('updateDepsSuccess', function() {
        jQuery('.flash').html(this.tr('Task dependencies updated').toString());
        jQuery('.flash').show();
        this.__R.getDeps({ id: this.__taskID });
      }, this);

      this.__R.addListener('updateDepsError', function() {
        this.__R.getDeps({ id: this.__taskID });
      }, this);
    },

    __setupStores: function() {
      this.__S  = new qx.data.store.Rest(this.__R, 'getRun');
      this.__S.addListener('changeModel', function(e) {
        if (e.getData().getContent().length == 0) {
          e.stopPropagation();
          e.preventDefault();
        } else {
          var status = e.getData().getContent().getItem(0).getStatus();
          if (['RUNNING', 'ASSIGNED', 'QUEUED'].indexOf(status) == -1) {
            this.__timer.stop();
            this.__progress.setValue(100);
          }
        }
      }, this);

      this.__Sdeps  = new qx.data.store.Rest(this.__R, 'getDeps');
    },

    __setupBindings: function() {
      var that = this;

      var infT = '$1<span style="color: #0031c6; font-weight: bold;">$2</span>';
      var debT = '$1<span style="color: #17a200; font-weight: bold;">$2</span>';
      var warT = '$1<span style="color: #e37400; font-weight: bold;">$2</span>';
      var errT = '$1<span style="color: #f00; font-weight: bold;"> $2 $3</span>';
      var sumT = '$1<span style="color: #000; font-weight: bold;">$2</span>$3';
      this.__S.bind('model.content[0].run_output', this.__output, 'html', {
        converter: function(data) {
          if (!data) return '';
          data = data.replace(atms.WinTask.reInfo, infT);
          data = data.replace(atms.WinTask.reDebug, debT);
          data = data.replace(atms.WinTask.reWarn, warT);
          data = data.replace(atms.WinTask.reError, errT);
          data = data.replace(atms.WinTask.reSum, sumT);
          var new_data = '';
          data.split('\n').reverse().forEach(function(row) {
            new_data += '\n' + row + '<br>';
          });

          return new_data;
        }
      });
      this.__S.bind('model.content[0].traceback', this.__traceback, 'value', {
        converter: function(data) {
          if (data)
            that.__tracebackHolder.setVisibility('visible');
          return data;
        }
      });
      this.__S.bind('model.content[0].run_result', this.__result, 'value', {
        converter: function(data) {
          if (data) {
            that.__resultHolder.setVisibility('visible');
            return JSON.parse(data).content;
          }
        }
      });
      this.__S.bind('model.content[0].run_output', this.__progress, 'value', {
        converter: function(data, model, source, target) {
          try {
            var lines = data.split('\n');
            var last = lines[lines.length - 2];
            return parseInt(last.slice(4, last.length - 1));
          } catch (err) {
            return that.__progress.getValue();
          }
        }
      });
      this.__Sdeps.bind('model.content', this.__deps, 'value', {
        converter: function(data, model, source, target) {
          var value = '';
          if (!data) return value;
          return data.join(', ');
        }
      }, this);
    }
  }
});
