/**
*@asset(atms/*)
*@asset(qx/icon/${qx.icontheme}/*)
*/

qx.Class.define('atms.Form',
{
  extend: qx.ui.container.Composite,
  include: [atms.MWBlocker],

  construct: function(data_type, ids, mod_type,
                      callback_success, callback_fail,
                      controller) {
    this.base(arguments);
    this.data_type = data_type;
    this.mod_type = mod_type;
    this.__callback_s = callback_success || null;
    this.__callback_f = callback_fail || null;
    this.__codeditors = [];
    this.__bctl = controller || 'manager';

    var href_els = window.location.href.split('/');
    var newurl = (href_els.slice(0, 6).join('/').replace('#', '') +
      '/' + data_type + '/' + mod_type + '/' + ids.id);
    window.history.replaceState({}, null, newurl);

    // layout
    var that = this;
    var layout = new qx.ui.layout.VBox(4);
    this.setLayout(layout);
    var mainContainer = new qx.ui.container.Composite(new qx.ui.layout.HBox(4));

    this.containers = {};

    // possible panels
    var panelHolders = [
      { name: 'main', flex: 3 },
      { name: 'atms.Devices', flex: 3 },
      { name: 'atms.Attachments', flex: 1 },
      { name: 'atms.Comments', flex: 1 },
    ];

    panelHolders.forEach(function(item) {
      var container = new qx.ui.container.Composite(
          new qx.ui.layout.Grow());
      container.setVisibility('excluded');
      container.setUserData('name', item.name);

      container.addListenerOnce('appear', function() {
        if (item['name'] == 'main')
            container.add(that.__widgetFactory(item['name']));
      }, this);

      mainContainer.add(container, { flex: item['flex'] });
      that.containers[item['name']] = container;
    });

    // toolbar
    this.add(this.__toolbarFactory(data_type));

    // add main container
    this.add(mainContainer, { flex: 1 });

    // set parent properties
    this.addListenerOnce('appear', function() {
      var parent = this.getLayoutParent().getLayoutParent();
      this.__parent = parent;
      this.__setDescription(data_type, ids.id, mod_type);

      this.tform = new atms.TForm(this.data_type, this.mod_type);
      this.vmngr = new qx.ui.form.validation.Manager();
      this.__formCtrl = new qx.data.controller.Object();
      this.__setupR(data_type);
      this.__setupStores();
      this.__S.setModel(this.tform.getModelTemplate(ids));
      this.__initForm();
      that.setIds(ids);

      if (mod_type !== 'create') this.getBlocker().block();
    }, this);
  },

  properties: {
    ids: {
      init: {},
      event: 'idsChanged',
      apply: '_applyIds'
    }
  },

  members: {
    __codeditors: null,
    data_type: null,
    mod_type: null,
    callback_success: null,
    callback_fail: null,

    tform: null,
    vmngr: null,
    __formCtrl: null,

    __parent: null,
    __R: null,
    __S: null,
    __P: null,
    __callback_s: null,
    __callback_f: null,

    containers: null,

    createB: null,
    updateB: null,
    refreshB: null,

    destroy: function() {
      for (var key in this.containers) {
        this.containers[key].getChildren().forEach(function(child) {
          child.destroy();
        });

        this.containers[key].destroy();
      }

      this.base(arguments);
    },

    __setupR: function(data_type) {
      var api = {
        get: {
          method: 'GET',
          url: '/atms/' + this.__bctl + '/api/' + data_type + '/id/{id}.json'
        },
        update: {
          method: 'PUT',
          url: '/atms/' + this.__bctl + '/api/' + data_type + '.json/?id={id}'
        },
        post: {
          method: 'POST',
          url: '/atms/' + this.__bctl + '/api/' + data_type + '.json'
        }
      };
      this.__R = new qx.io.rest.Resource(api);
      this.__R.configureRequest(function(req) {
        req.setRequestHeader('Content-Type', 'application/json');
        try {
          this.getBlocker().block();
        } catch (err) {}
      }, this);

      this.__R.addListener('success', function() {
        this.getBlocker().unblock();
      }, this);

      this.__R.addListener('postSuccess', function() {
        jQuery('.flash').html(this.tr('New item created').toString());
        jQuery('.flash').show();
        if (this.__callback_s)
            this.__callback_s();
        this.__parent.close();
      }, this);

      this.__R.addListener('postError', function() {
        if (this.__callback_f)
            this.__callback_f();
        jQuery('.flash').html(this.tr('Error pushing data!').toString());
        jQuery('.flash').show();
      }, this);

      this.__R.addListener('updateSuccess', function() {
        if (this.__callback_s)
            this.__callback_s();
        jQuery('.flash').html(this.tr('Changes saved').toString());
        jQuery('.flash').show();
      }, this);

      this.__R.addListener('updateError', function() {
        jQuery('.flash').html(this.tr('Error saving changes!').toString());
        jQuery('.flash').show();
      }, this);

      this.__R.addListener('getError', function(e) {
        jQuery('.flash').html(this.tr('Fetching data failed!').toString());
        jQuery('.flash').show();
      }, this);

      this.__R.addListener('error', function(e) {
        this.getBlocker().unblock();
        var status = e.getRequest().getStatus();

        var caption = this.tr('Could not process data');
        if (status == 401) {
          var msg = this.tr('Session timeout or lack of permission on the server side');
        } else if (status == 500) {
          var msg = this.tr('An error encountered on the server side :(');
        } else if (status == 404) {
          var msg = this.tr('Page does not exist :(');
        } else if (status == 503) {
          var msg = this.tr('Server is overloaded :(');
        }

        var dialog = new atms.WinConfirmation(caption, msg);

        this.getApplicationRoot().add(dialog);
        dialog.open();
      }, this);
    },

    __setupStores: function() {
      var that = this;

      this.__S = new qx.data.store.Rest(this.__R, 'get', {
        manipulateData: function(data) {
          that.__P = qx.data.marshal.Json.createModel(data.perm);
          var filtered = that.__filterData(data.content[0]);
          var hints = that.getIds();
          for (var k in hints)
            if (k != 'id') filtered[k] = hints[k];
          return filtered;
        }
      });
      this.__S.addListener('changeModel', function(e) {
        this.__formCtrl.setModel(e.getData());
      }, this);
    },

    __parseDate: function(dateString) {
      return new Date(dateString);
    },

    __setupBinding: function(widget, prop, prop_config) {
      if (!widget) return;
      var that = this;
      var ctrl = this.__formCtrl;
      switch (widget.classname) {
        case 'atms.DateField':
          ctrl.addTarget(widget, 'value', prop, true,
          {
            converter: function(data, model, source, target) {
              return that.__parseDate(data || new Date());
            }
          });
        break;

        case 'atms.editor.Editor':
          ctrl.addTarget(widget, 'value', prop, false,
          {
            converter: function(data, model, source, target) {
              var out = qx.util.Serializer.toNativeObject(data);
              return JSON.stringify(out[0], null, 4);
            }
          });
          this.__codeditors.push(
            { property: prop, widget: widget }
          );
        break;

        case 'qx.ui.form.CheckBox':
          ctrl.addTarget(widget, 'value', prop, true,
          {
            converter: function(data, model, source, target) {
              return Boolean(data);
            }
          });
        break;

        case 'atms.DynVirtualSelectBox':
          ctrl.addTarget(widget, 'defaultValue', prop, true);
        break;

        default:
          ctrl.addTarget(widget, 'value', prop, true);
        break;
      }
    },

    __initForm: function() {
      var config = this.tform.getConfigAll();
      for (var key in config) {
        var widget = this.__formFactory(key, config[key]);
        this.__setupBinding(widget, key, config[key]);
        var validator = config[key].validator;
        switch (validator) {
          case 'required':
            widget.setRequired(true);
            widget.setRequiredInvalidMessage(config[key]['invalidmsg']);
          break;
        }
        if (validator)
          this.vmngr.add(widget);
      }
    },

    __filterData: function(data, tform) {
      var map = this.tform.getConfigAll();
      var new_data = new Object();
      for (var index in data) {
        if (index in map)
          new_data[index] = data[index];
      }

      return new_data;
    },

    __formFactory: function(key, config) {
      if (!config.widget) return;
      var ctrl = this.__formCtrl;
      var main = this.containers['main'].getChildren()[0];

      var widget = null;
      var label = null;

      var hide_toolbar = main.getLayout().getCellWidget(10, 0);
      var submit_bar = main.getLayout().getCellWidget(100, 0);
      var switches_bar = main.getLayout().getCellWidget(12, 0);
      if (!hide_toolbar) {
        hide_toolbar = this.__toolbarFactory('hide_toolbar');
        submit_bar = this.__toolbarFactory('submit_bar');
        switches_bar = this.__toolbarFactory('switches_bar');
        main.getLayout().setRowFlex(11, 1);
        main.add(hide_toolbar, { row: 10, column: 0, colSpan: 4 });
        main.add(switches_bar, { row: 12, column: 0, colSpan: 4 });
        main.add(submit_bar, { row: 100, column: 0, colSpan: 4 });
        main.add(new qx.ui.container.Composite(new qx.ui.layout.HBox(4)),
            { row: 11, column: 0, colSpan: 4 });
      } else hide_toolbar = main.getLayout().getCellWidget(10, 0);
      var hide_part = hide_toolbar.getChildren()[0];
      var hide_container = main.getLayout().getCellWidget(11, 0);

      label = new qx.ui.basic.Label(config.label).set({ rich: true });
      switch (config.widget) {
        case 'textfield':
          widget = new qx.ui.form.TextField();
        break;

        case 'textarea':
          widget = new qx.ui.form.TextArea();
        break;

        case 'markitup':
          hide_toolbar.setVisibility('visible');
          var map = this.__widgetFactory('markitup');
          var button = map['button'];
          var widget = map['widget'];
          var container = map['container'];
          container.addBefore(label, widget);
          button.setLabel(config.label);
          button.setIcon(config.icon);
          hide_part.add(map['button']);
          hide_container.add(map['container'], { flex: 1 });
        break;

        case 'toggle':
          label = null;
          widget = new qx.ui.form.ToggleButton(config.label, 'atms/check_nok.png');
          widget.addListener('changeValue', function(e) {
            if (e.getData()) this.setIcon('atms/check_ok.png');
            else this.setIcon('atms/check_nok.png');
          });

          switches_bar.add(widget);
        break;

        case 'checkbox':
          widget = new qx.ui.form.CheckBox();
        break;

        case 'code':
          widget = new atms.editor.Editor().set({
            simple: true
          });

          atms.editor.Editor.loadAce(function() { widget.init(); });

          if (this.data_type == 'atms_devices') {
            var ml = this.containers['main'].getChildren()[0].getLayout();
            ml.setColumnFlex(1, 0);
            ml.setColumnWidth(1, 280);
          }

        break;

        case 'spinner':
          widget = new qx.ui.form.Spinner();
          widget.setMinimum(1);
          widget.setMaximum(5);
        break;

        case 'time':
          widget = this.__widgetFactory('time');
        break;

        case 'date':
          widget = this.__widgetFactory('date');
        break;

        case 'mselectb':
          widget = new atms.VirtualSelectBox(
            '/atms/' + this.__bctl + '/call/json/store/' +
            config.store_name + '/' + config.store_path,
            { null_option: config.nullv, group: false });

          widget.addListener('closed', function() {
            this.__formCtrl.getModel().set(
              key, widget.getValues().getItem(0).getId());
          }, this);

        break;
      }
      if (widget) {
        if (label) {
          label.setMargin(7, 4, 4, 4);
          label.setAlignX('right');
        }

        widget.setMargin(4, 4, 4, 4);
        if (config.row != null && config.col != null) {
          var colshift = 1;
          var rowshift = 0;
          if (config.vbox) {
            colshift = 0; rowshift = 1;
          }

          main.add(label,
              { row: config.row - rowshift, column: config.col - colshift });
          main.add(widget,
              { row: config.row, column: config.col,
                rowSpan: config.rowSpan, colSpan: config.colSpan });
        }
      }

      if (hide_container.getChildren() > 0) {
        hide_toolbar.setVisibility('visible');
        main.getLayout()._applyLayoutChange();
      }

      if (!config[this.mod_type + '_enabled'] && config.widget != 'markitup')
        widget.setEnabled(false);
      if (!config[this.mod_type + '_enabled'] && config.widget == 'markitup')
        widget.setReadOnlyMode(true);
      return widget;
    },

    __toolbarFactory: function(data_type) {
      var that = this;
      var toolbar;
      switch (data_type) {
        default:
          toolbar = new qx.ui.toolbar.ToolBar();
          toolbar.setAppearance('toolbar-alert');
          toolbar.addState('help');
          var part1 = new qx.ui.toolbar.Part();
          var toggles = new Array();

          var mainPanel = new qx.ui.toolbar.CheckBox(
            this.tr('Main form'), 'atms/details.png').set({
              value: true
            });
          mainPanel.setUserData('name', 'main');
          toggles.push(mainPanel);

          toggles.forEach(function(item) {
            var widget_type = item.getUserData('name');
            var container = that.containers[widget_type];
            item.bind('value', container, 'visibility', {
              converter: function(value) {
                if (value) return 'visible';
                else return 'excluded';
              }
            });
            item.addListener('changeValue', function(e) {
              var value = e.getData();
              if (value && !container.getChildren()[0])
                  container.add(that.__widgetFactory(widget_type));
            }, that);

            part1.add(item);
          });

          toolbar.add(part1);
        break;

        case 'atms_tcases':
        case 'atms_cases':
        case 'atms_tests':
          toolbar = new qx.ui.toolbar.ToolBar();
          toolbar.setAppearance('toolbar-alert');
          toolbar.addState('help');
          var part1 = new qx.ui.toolbar.Part();
          var toggles = [];

          var mainPanel = new qx.ui.toolbar.CheckBox(
                        this.tr('Main form'), 'atms/details.png').set({
                          value: true
                        });
          mainPanel.setUserData('name', 'main');
          toggles.push(mainPanel);

          var autoPanel = new qx.ui.toolbar.CheckBox(
              this.tr('Automation'), 'atms/android.png');
          autoPanel.setUserData('name', 'atms.Devices');
          that.bind('ids', autoPanel, 'enabled',
            that.getConfigMap('id2Enabled'));
          toggles.push(autoPanel);
          if (data_type == 'atms_tests') autoPanel.setVisibility('excluded');

          var attaPanel = new qx.ui.toolbar.CheckBox(
              this.tr('Attachments'), 'atms/dl.png');
          attaPanel.setUserData('name', 'atms.Attachments');
          that.bind('ids', attaPanel, 'enabled',
            that.getConfigMap('id2Enabled'));
          toggles.push(attaPanel);

          var commPanel = new qx.ui.toolbar.CheckBox(
              this.tr('Comments'), 'atms/bubbles.png');
          commPanel.setUserData('name', 'atms.Comments');
          that.bind('ids', commPanel, 'enabled', that.getConfigMap('id2Enabled'));
          toggles.push(commPanel);

          toggles.forEach(function(item) {
            var widget_type = item.getUserData('name');
            var container = that.containers[widget_type];
            item.bind('value', container, 'visibility', {
              converter: function(value) {
                if (container.getUserData('name') == 'atms.Devices' && value)
                  mainPanel.setValue(false);
                if (container.getUserData('name') == 'main' && value)
                  autoPanel.setValue(false);
                if (value) return 'visible';
                else return 'excluded';
              }
            });
            item.addListener('changeValue', function(e) {
              var value = e.getData();
              if (value && !container.getChildren()[0])
                  container.add(that.__widgetFactory(widget_type));
            }, that);

            part1.add(item);
          });

          toolbar.add(part1);
        break;

        case 'hide_toolbar':
          toolbar = new qx.ui.toolbar.ToolBar();
          toolbar.setAppearance('toolbar-alert');
          toolbar.addState('help');
          var part1 = new qx.ui.toolbar.Part();
          toolbar.add(part1);
          var part2 = new qx.ui.toolbar.Part();
          var help1 = new qx.ui.toolbar.Button('Help', 'atms/info-sign.png');
          help1.addListener('execute', function() {
            qx.bom.Window.open(
                '/atms/' + that.__bctl + '/../default/wiki/index');
          });

          help1.setToolTip(new qx.ui.tooltip.ToolTip(
            'Szybka ściągawka:<br> \
            S#1234 - ticket w SAUTisie<br> \
            IPLA#1234 - ticket w Redmine Redefine<br> \
            PP#1234 - ticket w Redmine DSiTT<br>'
          ).set({ rich: true }));
          part2.add(help1);
          toolbar.add(part2);
          toolbar.setVisibility('hidden');
        break;

        case 'submit_bar':
          toolbar = new qx.ui.container.Composite(
              new qx.ui.layout.Flow(0, 4, 'right'));
          this.createB = new qx.ui.form.Button(this.tr('Create'), 'atms/new-white.png');
          this.createB.addState('critical');
          this.updateB = new qx.ui.form.Button(this.tr('Save'), 'atms/update-white.png');
          this.refreshB = new qx.ui.form.Button(this.tr('Refresh'), 'atms/refresh-white.png');
          this.refreshB.addState('info');

          this.refreshB.addListener('execute', function() {
            this.__R.get({ id: this.getIds().id });
          }, this);

          this.updateB.addListener('execute', function() {
            if (this.vmngr.validate())
                this.__R.update(
                  { id: this.getIds().id },
                  { content: this.__serialize(this.__formCtrl.getModel()) });
          }, this);

          this.createB.addListener('execute', function() {
            if (!this.vmngr.validate()) return;
            if (this.mod_type == 'create') {
              this.__R.post(
                null,
                { content: this.__serialize(this.__formCtrl.getModel()) });
              return;
            }

            var caption = this.tr('Create new item?');
            var desc = this.tr('This action will create new item based on existing one.');
            var callback_ok = function() {
              that.__R.post(
                null,
                { content: that.__serialize(that.__formCtrl.getModel()) });
            };

            var dialog = new atms.WinConfirmation(
                caption,
                desc,
                callback_ok
            );
            this.getApplicationRoot().add(dialog);
            dialog.open();
          }, this);

          toolbar.add(this.createB);
          toolbar.add(this.refreshB);
          toolbar.add(this.updateB);

          var options = {
            converter: function(value) {
              if (value.id) return true;
              return false;
            }
          };
          this.bind('ids', this.updateB, 'enabled', options);
          this.bind('ids', this.refreshB, 'enabled', options);
        break;

        case 'switches_bar':
          toolbar = new qx.ui.container.Composite(
              new qx.ui.layout.Flow(0, 4, 'left'));
        break;
      }
      return toolbar;
    },

    __widgetFactory: function(widget_type) {
      var widget;
      switch (widget_type) {
        case 'main':
          widget = new qx.ui.container.Composite();
          var layout = new qx.ui.layout.Grid();
          layout.setColumnWidth(0, 150);
          layout.setColumnFlex(1, 1);
          layout.setColumnWidth(2, 150);
          layout.setColumnFlex(3, 1);
          widget.setLayout(layout);
        break;

        case 'atms.Devices':
          var _tcase = null;
          var _case = null;
          if (this.data_type == 'atms_cases') {
            _tcase = this.__S.getModel().getParent();
            _case = this.getIds().id;
          } else _tcase = this.getIds().id;
          widget = new atms.DevicesTaskMngr(_tcase, _case);
        break;

        case 'atms.Attachments':
          var _refCase = null;
          var _tcase = null;
          var _test = null;
          var _guard = [];
          if (this.data_type == 'atms_cases') {
            _tcase = this.__S.getModel().getParent();
            _refCase = this.getIds().id;
            _test = this.__S.getModel().getTest();
            _guard = ['tcase', 'test'];
          }

          if (this.data_type == 'atms_tcases') {
            _tcase = this.getIds().id;
            _guard = ['test'];
          }

          if (this.data_type == 'atms_tests') {
            _test = this.getIds().id;
            _guard = ['case', 'tcase'];
          }

          widget = new atms.Uploader('atms_attachments', {
            test: _test,
            tcase: _tcase,
            refcase: _refCase
          }, _guard);

          // console.log('_refCase', _refCase, '_tcase', _tcase, '_test', _test, '_guard', _guard);
        break;

        case 'atms.Comments':
          var channel = this.data_type + '_' + this.getIds().id;
          widget = new atms.Comments(channel);
        break;

        case 'time':
          var nf = new atms.TimeFormat();
          var setTimeValue = function(value) {
            var v  = nf.parse(value);
            var fv = nf.format(v);
            this.getContentElement().setValue(fv);
            this.fireDataEvent('changeValue', v);
            return v;
          };

          var getTimeValue = function() {
            var value = this.getContentElement().getValue();
            return nf.parse(value);
          };

          widget = new qx.ui.form.TextField();
          widget.setValue = setTimeValue;
          widget.getValue = getTimeValue;
        break;

        case 'date':
          var widget = new atms.DateField();
        break;

        case 'markitup':
          var w = new markitup.Editor('/atms/' + this.__bctl + '/html_render.json');
          var container = new qx.ui.container.Composite(
              new qx.ui.layout.VBox(4));
          container.add(w, { flex: 1 });

          var panel = new qx.ui.toolbar.CheckBox().set({
            value: true
          });
          panel.bind('value', container, 'visibility', {
            converter: function(value) {
              if (value) return 'visible';
              else return 'excluded';
            }
          });
          widget = {
            button: panel,
            widget: w,
            container: container
          };
        break;
      }
      return widget || new qx.ui.core.Widget();
    },

    __setDescription: function(data_type, dataId, mod_type) {
      var desc;
      var mode;
      switch (data_type) {
        case 'atms_cases':
          desc = this.tr('Test case');
          this.__parent.setIcon('atms/sc.png');
        break;

        case 'atms_nodes':
          desc = this.tr('Folder');
          this.__parent.setIcon('atms/folder-close.png');
        break;

        case 'atms_tcases':
          desc = this.tr('Test case template');
          this.__parent.setIcon('atms/tsc.png');
        break;

        case 'atms_ttests':
          desc = this.tr('Test template');
          this.__parent.setIcon('atms/calendar.png');
        break;

        case 'atms_devices':
          desc = this.tr('LAB device');
          this.__parent.setIcon('atms/code_blue.png');
        break;

        case 'atms_canteen':
          desc = this.tr('Canteen');
          this.__parent.setIcon('atms/spoon-knife.png');
        break;
      }
      switch (mod_type) {
        case 'create':
          mode = this.tr('creating');
        break;

        case 'update':
          mode = this.tr('editing');
        break;

        case 'do':
          mode = this.tr('performing');
        break;
      }
      if (dataId != 'template' && dataId != null)
          desc = '[' + dataId + '] ' + desc;
      desc += ' - ' + mode;
      this.__parent.setCaption(desc);
    },

    __serialize: function(model) {
      var serialized = true;
      this.__codeditors.forEach(function(item) {
        try {
          var value = qx.lang.Json.parse(
            item['widget'].getCode());
        } catch (e) {
          var dialog = new atms.WinConfirmation(
            this.tr('Json data not valid!'),
            this.tr('Entry not updated. Please fix the JSON data structure.'));
          this.getApplicationRoot().add(dialog);
          dialog.open();

          throw new qx.core.ValidationError('JSON data not valid!');
        }

        model.set(item['property'], [value]);
      }, this);

      var data = qx.util.Serializer.toNativeObject(
          model,
          null,
          new qx.util.format.DateFormat('yyyy-MM-dd kk:mm:ss'));
      return qx.lang.Json.stringify([data]);
    },

    _applyIds: function(value, old) {
      if (value.id != null)
          this.__R.get({ id: value.id });
    },

    getConfigMap: function(key) {
      var that = this;
      var config = {
        id2Enabled: {
          converter: function(value) {
            if (that.mod_type == 'create') return false;
            if (value != null && value != 'template')
                return true;
            else return false;
          }
        }
      };
      return config[key];
    }
  }
});
