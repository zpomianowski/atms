/**
*@asset(atms/*)
*@asset(qx/icon/${qx.icontheme}/*)
*/

qx.Class.define('atms.ViewHTMLueCap',
{
  extend: qx.ui.core.Widget,

  construct: function(id) {
    this.base(arguments);
    this.__setupResources(id);

    this._setLayout(new qx.ui.layout.VBox());

    // Filter panel
    var bandlist = new qx.data.Array([
      1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 17, 18,
      19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32,
      65, 66, 67, 68, 69, 70,
      252, 255,
      33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48
    ]);

    this.__filter = new qx.ui.groupbox.GroupBox(
      this.tr(
        'Filter bands'
        ), 'atms/filter.png').set({
          layout: new qx.ui.layout.Grid(2, 2),
          visibility: 'excluded'
        });

    var row = 0;
    var col = 0;
    bandlist.forEach(function(band) {
      var ch = new qx.ui.form.CheckBox('' + band).set({ minWidth: 64 });
      ch.setUserData('band', band);
      row = col % 7 == 0 ? ++row : row;
      col = col % 7 == 0 ? 0 : col;
      this.__filter.add(ch, { row: row, column: col });
      col++;
    }, this);

    var butFilterAny = new qx.ui.form.Button('\u2203b\u220AB:(b\u220ACA)');
    butFilterAny.setToolTip(new qx.ui.tooltip.ToolTip(
      this.tr('There exists b from B that also is within CA')));
    butFilterAny.addListener('execute', function() {
      this.__html.setHtml(this.tr('Please wait...'));
      this.__R.get(
        { band: this.getQueryAndSerialize(),
        filterType: 0 });
    }, this);

    var butFilterSub = new qx.ui.form.Button('CA\u2286B');
    butFilterSub.setToolTip(new qx.ui.tooltip.ToolTip(
      this.tr('CA is subset of B')));
    butFilterSub.addListener('execute', function() {
      this.__html.setHtml(this.tr('Please wait...'));
      this.__R.get(
        { band: this.getQueryAndSerialize(),
        filterType: 2 });
    }, this);

    var butFilterAll = new qx.ui.form.Button('B\u2286CA');
    butFilterAll.setToolTip(new qx.ui.tooltip.ToolTip(
      this.tr('B is subset of CA')));
    butFilterAll.addListener('execute', function() {
      this.__html.setHtml(this.tr('Please wait...'));
      this.__R.get(
        { band: this.getQueryAndSerialize(),
        filterType: 1 });
    }, this);

    var butPL = new qx.ui.form.Button('', 'atms/flag_poland.png');
    butPL.setToolTip(new qx.ui.tooltip.ToolTip(
      this.tr('Check all bands used in Poland')));
    butPL.addListener('execute', function() {
      var pl_bands = new qx.data.Array([1, 3, 7, 8, 20, 38]);
      this.__filter.getChildren().forEach(function(item) {
        if (item.classname != 'qx.ui.form.CheckBox') return;
        item.setValue(pl_bands.contains(item.getUserData('band')));
      });
    }, this);

    this.__filter.add(butFilterAny, { row: 10, column: 0, colSpan: 2 });
    this.__filter.add(butFilterSub, { row: 10, column: 2, colSpan: 2 });
    this.__filter.add(butFilterAll, { row: 10, column: 4, colSpan: 2 });
    this.__filter.add(butPL, { row: 10, column: 6 });
    this.__filter.add(new qx.ui.basic.Label(
      this.tr('b-band, B-set of checked bands, CA-subet of aggr bands')).set({
        appearance: 'attlistitem/size'
      }),
      { row: 11, column: 0, colSpan: 7 }
    );

    // Summary static panel
    this.__html = new qx.ui.embed.Html().set({
      overflowY: 'auto',
      allowGrowY: true
    });

    this._add(this.__filter);
    this._add(this.__html, { flex: 1 });

    this.setContextMenu(this._createMenu());
  },

  members: {
    __html: null,
    __scroll: null,
    __R: null,

    getQueryAndSerialize: function() {
      var bands2show = new qx.data.Array();
      this.__filter.getChildren().forEach(function(item) {
        if (item.classname != 'qx.ui.form.CheckBox') return;
        if (!item.getValue()) return;
        bands2show.push(item.getUserData('band'));
      });

      return bands2show.join('/');
    },

    _createMenu: function() {
      var menu = new qx.ui.menu.Menu();

      var buttonFilter = new qx.ui.menu.CheckBox(this.tr('Show filters'));
      buttonFilter.addListener('changeValue', function(e) {
        buttonFilter.bind('value', this.__filter, 'visibility', {
          converter: function(value) {
            return value == true ? 'visible' : 'excluded';
          }
        });
      }, this);

      menu.add(buttonFilter);
      return menu;
    },

    __setupResources: function(id) {
      this.__R = new qx.io.rest.Resource({
        get: {
            method: 'GET',
            url: '/atms/lipstein/summary.load/' + id +
                 '/{band}' + '?filterType={filterType=0}' }
      });
      this.__R.addListener('success', function(e) {
        this.__html.setHtml(e.getData());
      }, this);

      this.__R.get();
    }
  }
});
