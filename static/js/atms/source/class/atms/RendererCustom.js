/**
*@asset(atms/*)
*@asset(qx/icon/${qx.icontheme}/*)
*/

qx.Class.define('atms.RendererCustom',
{
  extend: qx.ui.table.cellrenderer.Replace,

  statics: {
    colorMap: {
      'adb console': '#FF1D77',
      init: '#AA40FF',
      downloading: '#7F6E94',
      pending: '#e3a21a',
      aborted: '#ADADAD',
      impossible: '#9f00a7',
      failed: '#EE1111',
      passed: '#3D72C9',
      idle: '#2672EC',
      rebooting: '#8C0095',
      busy: '#008A00',
      unknown: '#AA40FF',
      up: '#2672EC',
      down: '#000000',
      QUEUED: '#008A00',
      COMPLETED: '#2672EC',
      RUNNING: '#F3B200',
      FAILED: '#000000',
      TIMEOUT: '#000000',
      STOPPED: '#aaaaaa',
      EXPIRED: '#FF1D77',
      ASSIGNED: '#FE7C22'
    }
  },

  members: {
    construct: function() {
      this.base(arguments);
    },

    _getCellStyle: function(cellInfo) {
      cellInfo.styleHeight = 82;
      var tableModel = cellInfo.table.getTableModel();
      var i;
      var cond_test;
      var compareValue;

      var style =
            {
              margin: '0px 1px 0px 0px'
            };

      if (!cellInfo.value) cellInfo.value = '';
      if (cellInfo.value in atms.RendererCustom.colorMap) {
        style['background-color'] = atms.RendererCustom
          .colorMap[cellInfo.value];
        style['color'] = 'white';
      }

      var styleString = [];
      for (var key in style) {
        if (style[key]) {
          styleString.push(key, ':', style[key], ';');
        }
      }

      return styleString.join('');
    },

    _getCellSizeStyle: function(width, height, insetX, insetY) {
      var style = '';
      if (qx.core.Environment.get('css.boxmodel') == 'content') {
        width -= insetX;
        height -= insetY;
      }

      style += 'width:' + Math.max(width, 0) + 'px;';
      style += 'height:' + Math.max(height - 1, 0) + 'px;';

      return style;
    }
  }
});
