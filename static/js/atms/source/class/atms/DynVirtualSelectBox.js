/**
*@asset(atms/*)
*@asset(qx/icon/${qx.icontheme}/*)
*/

qx.Class.define('atms.DynVirtualSelectBox',
{
  extend: qx.ui.form.VirtualSelectBox,

  construct: function(filterProperty, groupped, multi, name) {
    var that = this;
    this.__name = name || 'Filter';
    this.__filterProperty = filterProperty || 'Name';
    this.__groupped = groupped;
    this.__sBuf = '';
    this.__multi = multi || false;

    this.base(arguments);
    this.__timer = new qx.event.Timer(1000);
    this.__timer.addListener('interval', function() {
      that.__sBuf = '';
      this.stop();
    });

    var delegate = {
      sorter: function(a, b) {
        var a = eval('a.get' + that.__filterProperty + '()');
        var b = eval('b.get' + that.__filterProperty + '()');
        return a > b ? 1 : a < b ? -1 : 0;
      },

      filter: function(item) {
              var a = eval(
                'item.get' + that.__filterProperty + '().toLowerCase()');
              var b = that.__sBuf.toLowerCase();
              if (qx.lang.String.contains(a, b)) return true;
              else return false;
            }
    };
    if (this.__groupped) delegate.group = function(model) {
      if (that.__sBuf != '') return that.tr('Filter is active');
      else return eval(
        'model.get' + that.__filterProperty + '().charAt(0).toUpperCase()');
    };

    this.addListener('keyinput', function(e) {
      this.__timer.restart();
      this.__sBuf += e.getChar();
      this.refresh();
    });

    this.addListener('focusout', function(e) {
      this.__sBuf = '';
      var that = this;
      setTimeout(function() {
        that.refresh();
      }, 0);
    });

    this.setLabelPath('label');

    this.getSelection().addListener('change', function(e) {
      var itemId = this.getSelection().getItem(0).getId();
      if (this.__state) this.removeState(this.__state);
      this.setDefaultValue(itemId);
      this.addState(itemId);
      this.__state = itemId;
      this.refresh();
    }, this);

    if (this.__multi) {
      var that = this;
      var dd = this.getChildControl('dropdown');
      dd.close = function() {};

      delegate.createItem = function() {
        var box = new qx.ui.form.CheckBox();
        return box;
      };

      delegate.bindItem = function(controller, item, index) {
        controller.bindProperty('', 'model', null, item, index);
        controller.bindProperty(
          that.getLabelPath(), 'label', that.getLabelOptions(), item, index);
        controller.bindProperty('value', 'value', null, item, index);
        controller.bindPropertyReverse('value', 'value', null, item, index);
      };

      this.addListener('disappear', function() {
        this.getChildControl('dropdown').hide();
        this.getChildControl('dropdown').dispose();
      }, this);
    }

    this.setDelegate(delegate);
  },

  properties: {
    defaultValue: {
      nullable: true,
      apply: '_applyDefaultValue',
      event: 'changeDefaultValue'
    }
  },

  members: {
    __state:            null,
    __name:             null,
    __groupped:         null,
    __sBuf:             null,
    __filterProperty:   null,
    __multi:            null,

    __timer:            null,

    _applyDefaultValue: function(value, old) {
      var content = this.getModel();
      content.forEach(function(el, i) {
        if (this.__multi) {
          if (value instanceof qx.data.Array) {
            value.forEach(function(d) {
              if (el.getId() == d) el.setValue(true);
              if (d == '_all') el.setValue(true);
            });
          }
        } else {
          if (value == el.getId()) {
            this.getSelection().push(el);
          }
        }
      }, this);

      return value;
    },

    _addBindings: function() {
      this.base(arguments);
      var that = this;
      var atom = this.getChildControl('atom');

      var custom = null;
      if (this.__multi) {
        var custom = {
          converter: function(data) {
            var counter = 0;
            var currentValue = '';
            that.getModel().forEach(function(d) {
              if (d.getValue()) {
                counter += 1;
                currentValue = d['get' + that.__filterProperty]();
              }
            });

            var value = counter == 1 ?
              currentValue : that.tr('selected') + ' ' + counter;
            var d = {
              special: that.tr('Special'),
              victim: that.tr('Victim'),
              action: that.tr('Action'),
              user: that.tr('Tester'),
              test: that.tr('Test'),
              status: that.tr('Status'),
              groupby: that.tr('Group by'),
              layer: that.tr('Layer'),
              aggr: that.tr('Aggregation')
            };
            return d[that.__name] + ' (' + value + ')';
          }
        };
      }

      var opts = this.getLabelOptions() || custom;
      var labelSourcePath = this._getBindPath('selection', this.getLabelPath());
      this.bind(labelSourcePath, atom, 'label', opts);
    },

    toggle: function() {
      var dropDown = this.getChildControl('dropdown');

      if (dropDown.isVisible()) {
        this.close();
        dropDown.hide();
      } else {
        this.open();
      }
    },

    close: function() {
      this.base(arguments);
    }
  }
});
