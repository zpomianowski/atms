/**
*@asset(atms/*)
*@asset(qx/icon/${qx.icontheme}/*)
*/

qx.Class.define('atms.FormWiFiCap',
{
  extend: qx.ui.container.Composite,

  properties: {
    id: {
      check: 'Integer',
      nullable: true,
      event: 'idChanged'
    }
  },

  construct: function(id, mod_type, callback_success, callback_fail) {
    this.base(arguments);
    this.setId(id);

    var href_els = window.location.href.split('/');
    var newurl = (href_els.slice(0, 6).join('/').replace('#', '') +
      '/wificap/' + mod_type + '/' + id);
    // window.history.replaceState({}, null, newurl);

    this.__callback_s = callback_success;
    this.__callback_f = callback_fail;

    this.setLayout(new qx.ui.layout.VBox());
    this.__holder = new qx.ui.container.Composite().set({
      layout: new qx.ui.layout.HBox()
    });

    var layout = new qx.ui.layout.Grid(4, 4);
    layout.setColumnWidth(0, 150);
    layout.setColumnFlex(1, 1);
    layout.setColumnWidth(2, 150);
    layout.setColumnFlex(3, 1);

    var tb = new qx.ui.toolbar.ToolBar();
    var part = new qx.ui.toolbar.Part();
    tb.add(part);

    var butForm = new qx.ui.toolbar.CheckBox(
      this.tr('Main form'), 'atms/details.png');
    var butAtt = new qx.ui.toolbar.CheckBox(
        this.tr('Attachments'), 'atms/dl.png').set({
          value: false,
          enabled: false
        });
    this.__butAtt = butAtt;

    part.add(butForm);
    part.add(butAtt);

    this.__form = new qx.ui.container.Composite();
    this.__form.setLayout(layout);

    this.add(tb);
    this.add(this.__holder, { flex: 1 });
    this.__holder.add(this.__form, { flex: 3 });

    this.addListener('appear', function(e) {
      var p = this.getLayoutParent().getLayoutParent();
      this.__parent = p;
      if (mod_type == 'create') {
        p.setCaption(this.tr('Creating new WiFi Capabilites entry'));
      }

      if (mod_type == 'update') {
        p.setCaption('[' + id + '] ' +
          this.tr('Updating WiFi Capabilites entry'));
      }

      this.__R.get({ id: id });
    }, this);

    this.__vmngr = new qx.ui.form.validation.Manager();
    this.__setupResources();

    // handle submit bar
    var toolbar = new qx.ui.container.Composite(
      new qx.ui.layout.Flow(0, 4, 'right'));

    var butSubmit = new qx.ui.form.Button();
    butSubmit.setLabel(
      mod_type == 'create' ? this.tr('Create') : this.tr('Save'));
    butSubmit.setIcon(
      mod_type == 'create' ? 'atms/new-white.png' : 'atms/update-white.png');
    butSubmit.addState(mod_type == 'create' ? 'critical' : 'info');

    butSubmit.addListener('execute', function() {
      if (!this.__vmngr.validate()) return;

      this.__S.getModel().setRef_cbs(
        (this.__widgets['ref_cbs'].getValues().getItem(0).getId()));

      this.__R.post(
        { id: this.getId() },
        { content: this.__serialize(this.__S.getModel()) });
    }, this);

    this.__butSubmit = butSubmit;

    toolbar.add(butSubmit);

    this.__form.add(toolbar, { row: 9, column: 0, colSpan: 4 });

    // handle events
    butForm.bind('value', this.__form, 'visibility', {
      converter: function(value) {
        return value ? 'visible' : 'excluded';
      }
    });
    butForm.setValue(true);
    this.__initialized = false;
  },

  members: {
    __parent: null,
    __holder: null,
    __butAtt: null,
    __form: null,
    __R: null,
    __S: null,
    __pcaps: null,
    __widgets: null,
    __vmngr: null,
    __callback_s: null,
    __callback_f: null,
    __butSubmit: null,
    __uploader: null,
    __initialized: null,

    __setupResources: function() {
      var api = {
        get: {
          method: 'GET',
          url: '/atms/lubelak/api/atms_wificap/id/{id}.json'
        },
        post: {
          method: 'POST',
          url: '/atms/lubelak/api/atms_wificap/{id}.json'
        },
        getDeviceModel: {
          method: 'GET',
          url: '/atms/lubelak/api/cbs_device/id/{id}/model/name.json'
        },
        getDeviceSN: {
          method: 'GET',
          url: '/atms/lubelak/api/cbs_device/id/{id}/sn.json'
        },
        getDeviceProducent: {
          method: 'GET',
          url: '/atms/lubelak/api/cbs_device/id/{id}/company/name.json'
        }
      };
      this.__R = new qx.io.rest.Resource(api);

      this.__R.addListener('getSuccess', function(e) {
        var fields = e.getData().content[0];

        if (!this.__initialized) {
          this.__widgets = {};
          for (var k in fields) {
            this.__widgets[k] = this.__widgetFactory(k);
          }
        }

        if (fields.hasOwnProperty('id') && !this.__uploader) {
          this.__butAtt.setEnabled(true);
          var upload = new atms.Uploader('atms_attachments', {
              refwificap: fields.id
            }, []);
          this.__uploader = upload;
          this.__holder.add(upload, { flex: 1 });
          this.__butAtt.bind('value', upload, 'visibility', {
            converter: function(value) {
              return value ? 'visible' : 'excluded';
            }
          });
        }

        this.__initialized = true;
      }, this);

      this.__R.addListener('postSuccess', function(e) {
        this.__butSubmit.setLabel(this.tr('Save'));
        this.__butSubmit.setIcon('atms/update-white.png');
        this.__butSubmit.addState('info');
        this.__butSubmit.removeState('critical');
        this.setId(parseInt(e.getData().content));
        this.__R.get({ id: this.getId() });
        if (this.__callback_s) this.__callback_s();
      }, this);

      this.__R.addListener('error', function(e) {
        if (this.__callback_f) this.__callback_f();
        this.__parent.setShowStatusbar(true);
        this.__parent.setStatus(
          this.tr('Operation failed!'));
      }, this);

      this.__S = new qx.data.store.Rest(this.__R, 'get', {
        manipulateData: function(data) {
          return data.content[0];
        }
      });

      // binding
      this.__S.addListenerOnce('changeModel', function(e) {
        for (var k in this.__widgets) {
          if (!this.__widgets[k]) continue;
          var cls = this.__widgets[k].classname;
          if (cls == 'atms.VirtualSelectBox') {
            qx.data.SingleValueBinding.bind(
              this.__S, 'model.' + k, this.__widgets[k], 'value');
          }

          if (cls == 'atms.FileTextField') {
            qx.data.SingleValueBinding.bind(
              this.__S, 'model.' + k, this.__widgets[k], 'id');
            qx.data.SingleValueBinding.bind(
              this.__widgets[k], 'id', this.__S, 'model.' + k);
          } else {
            qx.data.SingleValueBinding.bind(
              this.__S, 'model.' + k, this.__widgets[k], 'value', {
                converter: function(value) {
                  return value ? String(value) : '';
                }
              });
            qx.data.SingleValueBinding.bind(
              this.__widgets[k], 'value', this.__S, 'model.' + k);
          }
        }
      }, this);

      // bindings for helpers - model, producent
      var modelS = new qx.data.store.Rest(this.__R, 'getDeviceModel');
      modelS.addListenerOnce('changeModel', function() {
        qx.data.SingleValueBinding.bind(
          modelS, 'model.content[0].name',
          this.__widgets['f_model'], 'value');
      }, this);

      var producentS = new qx.data.store.Rest(this.__R, 'getDeviceProducent');
      producentS.addListenerOnce('changeModel', function() {
        qx.data.SingleValueBinding.bind(
          producentS, 'model.content[0].name',
          this.__widgets['f_company'], 'value');
      }, this);

      var snS = new qx.data.store.Rest(this.__R, 'getDeviceSN');
      producentS.addListenerOnce('changeModel', function() {
        qx.data.SingleValueBinding.bind(
          snS, 'model.content[0].f_sn',
          this.__widgets['f_sn'], 'value');
      }, this);
    },

    __widgetFactory: function(id) {
      var that = this;
      var rowFlex = 0;
      var widget = null;
      var validator = null;
      switch (id) {
        case 'id':
        case 'f_processed':
        case 'f_caps_json':
        case 'f_maxrate24':
        case 'f_maxrate5':
          return null;
        break;

        case 'ref_cbs':
          widget = new atms.VirtualSelectBox(
            '/atms/manager/call/json/store/atms_wificap/ref_cbs',
            { null_option: true });
          widget.addListener('closed', function() {
            var index = widget.getValues().getItem(0).getId();
            this.__R.getDeviceModel({ id: index });
            this.__R.getDeviceProducent({ id: index });
            this.__R.getDeviceSN({ id: index });
          }, this);

        break;

        case 'ref_pcap24':
        case 'ref_pcap5':
          widget = new atms.FileTextField(
            this.tr('Drop pcap file here'),
            ['pcap', 'pcapng']
          );
          widget.addListener('drop', this._handleDrop, this);
        break;

        case 'f_cat':
          widget = new qx.ui.form.TextField().set({ readOnly: true });
        break;

        default:
          widget = new qx.ui.form.TextField();
      }
      var label = id;

      var pos = {
        ref_cbs: 0,
        f_sn: 1,
        f_model: 3,
        f_company: 2,
        f_chipset: 4,
        f_sw_version: 5,
        ref_pcap24: 6,
        ref_pcap5: 7,
        f_cat: 8
      };

      var tr = {
        f_sn: this.tr('Serial No'),
        ref_cbs: this.tr('Ref CBS'),
        f_model: this.tr('Model'),
        f_company: this.tr('Company'),
        f_chipset: 'Chipset',
        f_cat: this.tr('WiFi category'),
        f_sw_version: this.tr('SW version'),
        ref_pcap24: 'PCAP 2.4GHz',
        ref_pcap5: 'PCAP 5GHz'
      };
      this.__form.add(
        new qx.ui.basic.Label().set({ value: tr[id] }),
        { row: pos[id], column: 0 });
      this.__form.add(widget, { row: pos[id], column: 1, colSpan: 3 });
      this.__form.getLayout().setRowFlex(pos[id], rowFlex);

      if (validator) {
        this.__vmngr.add(widget, validator);
      }

      return widget;
    },

    __serialize: function(model) {
      return qx.util.Serializer.toJson(
          model,
          null,
          new qx.util.format.DateFormat('yyyy-MM-dd kk:mm:ss'));
    },

    _handleDrop: function(e) {
      var sender = e.getData('sender');
      var target = e.getData('target');
      if (sender instanceof atms.Uploader) {
        var modelSkeleton = new Object();
        var att_id = e.getData('atts').getItem(0).getAtms_attachments_id();
        target.setId(att_id);
      }
    }
  }
});
