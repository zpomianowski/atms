/**
*@asset(atms/*)
*@asset(qx/icon/${qx.icontheme}/*)
*/

qx.Class.define('atms.CommentRooms',
{
  extend: qx.ui.container.Composite,

  construct: function(data) {
    this.base(arguments);
    this.setLayout(new qx.ui.layout.Grow());

    var list = new qx.ui.list.List().set({
      selectionMode: 'single',
      itemHeight: 48
    });
    var that = this;
    var delegate = {
      createItem: function() {
        return new atms.CommentRoomItem();
      },

      bindItem: function(controller, item, id) {
        controller.bindProperty('channel', 'channel', null, item, id);
        controller.bindProperty('caption', 'caption', null, item, id);
        controller.bindProperty('count', 'count', null, item, id);
      }
    };
    list.setDelegate(delegate);
    this.add(list);

    var model = qx.data.marshal.Json.createModel(data);
    list.setModel(model);

    list.addListener('dblclick', function() {
      var item = list.getSelection().getItem(0);
      var widget = null;
      switch (item.getChannel()) {
        case 'users':
        case 'atms_manager':
        case 'atms_tester':
        case 'wiki_editor':
          widget = new atms.Comments(item.getChannel(), item.getMembers().toArray());
        break;

        default:
          var re = /(.*?)_?(\d+)?$/g;
          var m = re.exec(item.getChannel());
          var widget = new atms.Form(
            m[1],
            { id: m[2] },
            'update', null, null);
        break;
      };
      if (!widget) return;
      var win  = new atms.WinPanel(widget, item.getCaption());
      qx.core.Init.getApplication().gDesk1.add(win);
      win.open();
    }, this);
  },

  members: {

  }
});
