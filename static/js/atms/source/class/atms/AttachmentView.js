/**
*@asset(atms/*)
*@asset(qx/icon/${qx.icontheme}/*)
*/

qx.Class.define('atms.AttachmentView',
{
  extend: qx.ui.core.Widget,
  include: [qx.ui.form.MModelProperty],

  properties: {
    appearance:
    {
      refine: true,
      init: 'attlistitem'
    },

    attID:
    {
      check: 'Integer',
      nullable: true
    },

    protCase:
    {
      init: false,
      check: Boolean,
      nullable: true,
      apply: '_applyProtection'
    },

    protTCase:
    {
      init: false,
      check: Boolean,
      nullable: true,
      apply: '_applyProtection'
    },

    protTest:
    {
      init: false,
      check: Boolean,
      nullable: true,
      apply: '_applyProtection'
    },

    icon:
    {
      init: '-',
      check: 'String',
      apply: '_applyIcon',
      nullable: true
    },

    label: {
      init: '-',
      check: 'String',
      apply: '_applyLabel',
      nullable: true
    },

    progress: {
      init: 0,
      check: 'Integer',
      apply: '_applyProgress',
      event: 'progressChanged'
    },

    size: {
      init: 0,
      check: 'String',
      apply: '_applySize'
    },

    created_by: {
      init: '-',
      check: 'String',
      apply: '_applyCreatedBy'
    },

    created_on: {
      init: '-',
      check: 'String',
      apply: '_applyCreatedOn'
    },

    stored: {
      init: false,
      check: 'Boolean',
      apply: '_applyStored'
    }
  },

  construct: function() {
    this.base(arguments);

    var layout = new qx.ui.layout.Grid(3, 3);
    layout.setColumnFlex(1, 2);
    this._setLayout(layout);
    this._createChildControl('icon');
    this._createChildControl('label');
    this._createChildControl('progress');
    this._createChildControl('size');
    this._createChildControl('created_by');
    this._createChildControl('created_on');
    this._createChildControl('protection');
  },

  members: {
    _createChildControlImpl: function(id) {
      var control;
      var that = this;

      switch (id)
      {
        case 'icon':
          control = new qx.ui.basic.Image();
          control.setAnonymous(true);
          this._add(control, { row: 1, column: 0, rowSpan: 2 });
        break;

        case 'label':
          control = new qx.ui.basic.Label(this.getLabel());
          control.set({ allowGrowX: true });
          control.setTextAlign('left');
          control.setAnonymous(true);
          this._add(control, { row: 1, column: 1, colSpan: 1 });
        break;

        case 'progress':
          control = new qx.ui.indicator.ProgressBar(0, 100);
          control.setAnonymous(true);
          this._add(control, { row: 0, column: 0, colSpan: 3 });
        break;

        case 'protection':
          control = new qx.ui.basic.Image('atms/lock.png');
          this._add(control, { row: 1, column: 4, colSpan: 1 });
        break;

        case 'size':
          control = new qx.ui.basic.Label(this.getLabel());
          control.set({ allowGrowX: true });
          control.setAnonymous(true);
          control.setTextAlign('right');
          this._add(control, { row: 1, column: 3, colSpan: 1 });
        break;

        case 'created_by':
          control = new qx.ui.basic.Label(this.getLabel());
          control.set({ allowGrowX: true });
          control.setAnonymous(true);
          this._add(control, { row: 2, column: 1, colSpan: 1 });
        break;

        case 'created_on':
          control = new qx.ui.basic.Label(this.getLabel());
          control.set({ allowGrowX: true });
          control.setAnonymous(true);
          control.setTextAlign('right');
          this._add(control, { row: 2, column: 3, colSpan: 1 });
        break;
      }
      return control || this.base(arguments, id);
    },

    _applyIcon: function(value, old) {
      var icon = this.getChildControl('icon');
      icon.setSource(value);
    },

    _applyProgress: function(value, old)
    {
      var label = this.getChildControl('progress');
      label.setValue(value);
    },

    _applyLabel: function(value, old) {
      var label = this.getChildControl('label');
      label.setValue(value.toString());
    },

    _applySize: function(value, old)
    {
      var result = '';
      var M = 1048576;
      var K = 1024;
      if (value > M) result = (value / M).toFixed(2).toString() + ' MB';
      else if (value > K) result = (value / K).toFixed(2).toString() + ' KB';
      else result = value.toString() + ' B';
      var label = this.getChildControl('size');
      label.setValue(result);
    },

    _applyCreatedOn: function(value, old) {
      var label = this.getChildControl('created_on');
      label.setValue(value.toString());
    },

    _applyCreatedBy: function(value, old) {
      var label = this.getChildControl('created_by');
      label.setValue(value.toString());
    },

    _applyStored: function(value, old) {
      var progress = this.getChildControl('progress');
      if (value) progress.setVisibility('hidden');
      else progress.setVisibility('visible');
    },

    _applyProtection: function(value, old) {
      var prot = this.getProtCase() || this.getProtTCase() || this.getProtTest();
      var lock = this.getChildControl('protection');
      if (prot) {
        this.getChildControl('protection').setVisibility('visible');
      } else this.getChildControl('protection').setVisibility('excluded');
    }
  }
});
