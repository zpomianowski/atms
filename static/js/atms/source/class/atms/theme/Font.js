/* ************************************************************************

   Copyright:

   License:

   Authors:

************************************************************************ */

qx.Theme.define("atms.theme.Font",
{
  extend : qx.theme.indigo.Font,

  fonts :
  {
    "big" : {
      family : [ "sans-serif" ],
      size : 32,
      lineHeight: 1
    },

    "mild" : {
      family : [ "sans-serif" ],
      size : 14,
      bold: true,
      lineHeight: 1
    },

    "mild-grey" : {
      family : [ "sans-serif" ],
      size : 12,
      lineHeight: 1
    },

  	"ministats" : {
  		family : [ "sans-serif" ],
  		size : 9,
      lineHeight: 1,
      color: "#F0F"
  	},

  	"userlabel" : {
  		family : [ "sans-serif" ],
  		size : 12,
  		bold : true
  	},

    "filterFont" : {
      family : [ "sans-serif" ],
      size : 10
    },

    "menuFont": {
      family : [ "sans-serif" ],
      lineHeight: -10,
      size : 12
    }
  }
});