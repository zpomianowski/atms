/* ************************************************************************

   qooxdoo - the new era of web development

   http://qooxdoo.org

   Copyright:
     2004-2011 1&1 Internet AG, Germany, http://www.1und1.de

   License:
     LGPL: http://www.gnu.org/licenses/lgpl.html
     EPL: http://www.eclipse.org/org/documents/epl-v10.php
     See the LICENSE file in the project's top-level directory for details.

   Authors:
   * Martin Wittemann (martinwittemann)

************************************************************************* */

/* ************************************************************************


************************************************************************* */

/**
 * The simple qooxdoo appearance theme.
 *
 * @asset(atms/*)
 * @asset(qx/icon/Tango/16/apps/office-calendar.png)
 * @asset(qx/icon/Tango/16/places/folder-open.png)
 * @asset(qx/icon/Tango/16/places/folder.png)
 * @asset(qx/icon/Tango/16/mimetypes/text-plain.png)
 * @asset(qx/icon/Tango/16/actions/view-refresh.png)
 * @asset(qx/icon/Tango/16/actions/window-close.png)
 * @asset(qx/icon/Tango/16/actions/dialog-cancel.png)
 * @asset(qx/icon/Tango/16/actions/dialog-ok.png)
 */
qx.Theme.define("atms.theme.Appearance",
{
  extend : qx.theme.indigo.Appearance,

  appearances : {
    "ca-view-header": {
      style: function(states) {
        var bC = "highlight-shade";
        if (states.selected) bC = "#FF8000";
        else {
          if (states.intraband) bC = "#DC572E";
          if (states.interband) bC = "#BF1E4B";
          if (states.intrabandc) bC = "#00A600";
        }
        return {
          backgroundColor: bC,
          textColor: "white"
        }
      }
    },

    "chat" : {
      style: function(states) {
        var bC = "highlight-shade";
        if (states.selected) bC = "#FF8000";
        else {
          if (states.wiki_editor) bC = "#DC572E";
          if (states.atms_tester) bC = "#BF1E4B";
          if (states.atms_manager) bC = "#00A600";
          if (states.atms_cases) bC = "highlight";
          if (states.users) bC = "#505050";
        }
        return {
          backgroundColor: bC,
          padding: [4, 4, 4, 0],
          textColor: "white",
          decorator: "comment"
        }
      }
    },

    "chat/caption": "attlistitem/size",
    "chat/channel": {
      style: function(states) {
        return {
          font: "userlabel"
        }
      }
    },
    "chat/count": "chat/channel",

    "post/txt" : {
      style: function(states) {
        var dec = "comment";
        if (states.not_read)
          dec = "comment_not_read"
        return {
          backgroundColor: "#f6f6f6",
          padding: [0,8,8,8],
          marginRight: 4,
          decorator: dec
        }
      }
    },

    "post/label" : {
      style: function(states) {
        return {
          font: "userlabel"
        }
      }
    },

    "post/delButton" : {
      include: "button",

      style: function(states) {
        return {
          font: "ministats"
        }
      }
    },

    "virtual-selectbox" : {
      include: "selectbox",

      style: function(states) {
        var bC = undefined;
        var txtC = undefined;
        if (states.hovered) { txtC = "#FFFFFF"; bC = "background-selected"; }
        else if (states.unknown) { txtC = "#FFFFFF"; bC = "state-unknown"; }
        else if (states['new']) { txtC = "#FFFFFF"; bC = "state-new"; }
        else if (states.pending) { txtC = "#FFFFFF"; bC = "state-pending"; }
        else if (states.aborted) { txtC = "#FFFFFF"; bC = "state-aborted"; }
        else if (states.impossible) { txtC = "#FFFFFF"; bC = "state-impossible"; }
        else if (states.failed) { txtC = "#FFFFFF"; bC = "state-failed"; }
        else if (states.passed) { txtC = "#FFFFFF"; bC = "state-passed"; }

        return {
          height: 27,
          textColor: txtC,
          backgroundColor: bC
        }
      }
    },
    "virtual-selectbox/arrow": "selectbox/arrow",
    "virtual-selectbox/atom": {
      alias: "selectbox/atom",

      style: function() {
        return {
          padding: [0,0,0,0]
        }
      }
    },

    "markitup" : {
      alias: "tabview",

      style: function(states) {
        var d = undefined;
        if (states.invalid)
          d = "markitup-invalid";
        return {
          decorator: d
        }
      }
    },

    "submit-button" : {
      include: "button-frame",

      style: function(states) {
        var backC = '#3D72C9';
        if (states.hovered && states.safe)
          backC = '#99b433';
        else if (states.hovered && states.critical)
          backC = '#da532c';
        else if (states.hovered && states.normal)
          backC = '#e3a21a'
        return {
          backgroundColor: backC,
          textColor: '#FFFFFF'
        }
      }
    },

    "tooltip" : {
      style: function(states) {
        return {
          textColor: "#fff",
          backgroundColor: "#3D72C9",
          padding: [8, 8, 8, 8]
        }
      }
    },

    "rank/user-place" : {
      style : function(states) {
        return {
          padding: [8, 8, 8, 8],
          font: "big",
          textColor: "#fff",
          backgroundColor: "#3D72C9"
        }
      }
    },

    "rank/user-awards" : {
      style : function(states) {
        return {
          padding: [4, 0, 4, 0],
          backgroundColor: "#5583D0",
          textColor: "#fff"
        }
      }
    },

    "rank/user-rank" : {
      style : function(states) {
        return {
          padding: [0, 0, 0, 10],
          font: "mild-grey",
          textColor: "#707070"
        }
      }
    },

    "rank/user-label" : {
      style : function(states) {
        return {
          padding: [0, 0, 0, 10],
          font: "mild"
        }
      }
    },

    "cketextarea" : {
      style : function(states) {
        return {
          wordWrap: "break-word"
        }
      }
    },

    "textarea-code" : {
      alias: "textarea",

      style : function(states) {
        return {
          padding: [6,6,6,6],
          backgroundColor: "#272822",
          textColor: "#f8f8f2",
          font: "monospace"
        }
      }
    },

    "selectbox-toolbar" : {
      style : function(states) {
        return {}
      }
    },
    "selectbox-toolbar/arrow": "selectbox/arrow",

    "toolbar-alert" :
    {
      alias : "toolbar",

      style : function(states)
      {
        var c = '#FF2E12';
        if (states.alert) c = '#FF2E12';
        if (states.info) c = "#008A00";
        if (states.help) {
          c = "#FFFFFF";
        }
        return {
          backgroundColor : c
        }
      }
    },

    "toolbar-button" :
    {
      alias : "atom",

      style : function(states)
      {
        var decorator = "button-box";
        var textC = undefined;
        var bC = undefined;

        if (states.disabled) {
          decorator = "button-box";
        } else if (states.hovered && !states.pressed && !states.checked) {
          decorator = "button-box-hovered";
          textC = "white";
        } else if (states.hovered && (states.pressed || states.checked)) {
          decorator = "button-box-pressed-hovered";
          textC = "white";
        } else if (states.pressed || states.checked) {
          decorator = "button-box-pressed";
        }

        if (states.notifications) {
          bC = 'background-selected';
          textC = "white";
        }

        // set the right left and right decoratos
        if (states.left) {
          decorator += "-left";
        } else if (states.right) {
          decorator += "-right";
        } else if (states.middle) {
          decorator += "-middle";
        }

        // set the margin
        var margin = [7, 10];
        if (states.left || states.middle || states.right) {
          margin = [7, 0];
        }

        return {
          backgroundColor: bC,
          textColor : textC,
          cursor  : states.disabled ? undefined : "pointer",
          decorator : decorator,
          margin : margin,
          padding: [3, 5],
          font: "menuFont"
        };
      }
    },

    "filter-tag" : {
      style: function(states)
      {
        var bgC = "#a4a4a4";
        if (states.status) bgC = "#DC572E";
        else if (states.user) bgC = "highlight";
        else if (states.test) bgC = "#008A00";
        else if (states.path) bgC = "#008299";
        else if (states.name) bgC = "#8C0095";
        else if (states.modified_on) bgC = "#C27D4F";
        if (states.hovered) bgC = "background-selected";
        return {
          backgroundColor: bgC,
          font: "filterFont",
          margin: [0, 1, 1, 1],
          padding: [0, 2, 1, 3],
          decorator: "filter-tag",
          textColor: "white"
        }
      }
    },

    "attlistitem" : {
      style: function(states) {
        var backgroundC = undefined;
        var fc = "#000000";
        if (states.selected) {
          backgroundC = "#FF8000";
          fc = "#FFFFFF";
        }
        else if (states.tcase) {
          backgroundC = "#cccccc";
          fc = "#FFFFFF";
        }
        return {
          backgroundColor: backgroundC,
          textColor: states.selected ? "#FFFFFF" : "#000000"
        }
      }
    },

    "attlistitem/label" : {
      style: function(states) {
        return {
          minWidth: 30,
          font: "userlabel"
        }
      }
    },
    "attlistitem/size" : {
      alias: "attlistitem/label",

      style : function() {
        return {
          paddingRight: 4,
          minHeight: 10,
          font: "ministats"
        }
      }
    },
    "attlistitem/created_by": "attlistitem/size",
    "attlistitem/created_on": "attlistitem/size",

    "attlistitem/progress" : {
      style : function() {
        return {
          padding: 0,
          backgroundColor: "#EEE",
          minHeight: 3,
          maxHeight: 3
        }
      }
    },

    "attlistitem/progress/progress":
    {
      style: function(states) {
        return {
          minHeight: 3,
          maxHeight: 3,
          backgroundColor: states.overflowed ?
            "#00d" :
            "#7ebc0e"
        }
      }
    },

    // userlistitem
    "userlistitem" : {
      style : function(states) {
        var backgroundC = "highlight-shade";
        if (states.selected) backgroundC = "#FF8000";
        return {
          backgroundColor: backgroundC,
          decorator: "comment"
        }
      }
    },
    "userlistitem/icon" : {
      include: "icon",
      style : function(states) {
        return { margin: [5,5,5,5] }
      }
    },

    "userlistitem/label" : {
      include: "label",
      style : function(states) {
        return {
          marginTop: 5,
          backgroundColor: "highlight",
          padding: [0,5,0,5],
          //marginLeft: 34,
          textColor: "white",
          font : "userlabel"
        }
      }
    },

    "userlistitem/itemsNb" : {
      style : function() {
          return {
            height: 8,
            minWidth: 30,
            font: "ministats",
            textColor: "white"
          }
      }
    },
    "userlistitem/itemsTotal" : "userlistitem/itemsNb",
    "userlistitem/timeOccupance" : "userlistitem/itemsNb",
    "userlistitem/timeOccupanceTotal" : "userlistitem/itemsNb",
    "userlistitem/lastUpdate" : {
      include: "userlistitem/itemsNb",
      style : function() { return { marginLeft: 34, marginBottom: 5 }}
    },

    "userlistitem/casesProgress" : {
      style : function() {
        return {
          padding: 0,
          backgroundColor: "#EEE",
          height: 3
        }
      }
    },

    "userlistitem/casesProgress/progress":
    {
      style: function(states) {
        return {
          height: 3,
          backgroundColor: states.overflowed ?
            "#d00" :
            "#7ebc0e"
        }
      }
    },

    "userlistitem/timeProgress" : "userlistitem/casesProgress",

    "list" : {
      alias: "scrollarea"
    },

    "scrollbar" : {},
    "scrollbar/slider" : {},

    "scrollbar/slider/knob" :
    {
      style : function(states)
      {
        var decorator = "scroll-knob";

        if (!states.disabled) {
          if (states.hovered && !states.pressed && !states.checked) {
            decorator = "scroll-knob-hovered";
          } else if (states.hovered && (states.pressed || states.checked)) {
            decorator = "scroll-knob-pressed-hovered";
          } else if (states.pressed || states.checked) {
            decorator = "scroll-knob-pressed";
          }
        }

        return {
          height : 4,
          width : 4,
          backgroundColor: "highlight",
          cursor : states.disabled ? undefined : "pointer",
          decorator : decorator,
          minHeight : states.horizontal ? undefined : 4,
          minWidth : states.horizontal ? 4 : undefined
        };
      }
    },


    "scrollbar/button" :
    {
      style : function(states)
      {
        var styles = {};
        // styles.padding = 0;

        // var icon = "";
        // if (states.left) {
        //   //icon = "left";
        //   styles.marginRight = 2;
        // } else if (states.right) {
        //   //icon += "right";
        //   styles.marginLeft = 2;
        // } else if (states.up) {
        //   //icon += "up";
        //   styles.marginBottom = 2;
        // } else {
        //   //icon += "down";
        //   styles.marginTop = 2;
        // }
        // styles.height = 0;

        // styles.icon = qx.theme.simple.Image.URLS["arrow-" + icon];

        // styles.cursor = "pointer";
        // styles.decorator = "button-box";
        return styles;
      }
    },

    "scrollbar/button-begin" : "scrollbar/button",
    "scrollbar/button-end" : "scrollbar/button",



    "label" :
    {
      style : function(states)
      {
        var _textColor;
        // if (states.hovered && !states.selected) {
        //   _textColor = "text-hovered";
        // } else _textColor = undefined;
        return {
          textColor : states.disabled ? "text-disabled" : _textColor
        };
      }
    },

    "group-item" :
    {
      include : "label",
      alias : "label",

      style : function(states)
      {
        return {
          padding : 4,
          backgroundColor : "highlight",
          textColor : "white",
          font: "bold"
        };
      }
    },

    "button-frame" :
    {
      alias : "atom",

      style : function(states)
      {
        var decorator = "button-box";
        var _textColor;
        var _backC = undefined;
        if (states.hovered && !states.focused) {
          _textColor = "text-selected";
        } else _textColor = undefined;

        if (!states.disabled) {
          if (states.hovered && !states.pressed && !states.checked) {
            if (states.critical)
              _backC = "state-critical";
            else if (states.safe)
              _backC = "state-safe";
            else if (states.info)
              _backC = "state-info";
            decorator = "button-box-hovered";
          } else if (states.hovered && (states.pressed || states.checked)) {
            decorator = "button-box-pressed-hovered";
          } else if (states.pressed || states.checked) {
            decorator = "button-box-pressed";
          }
        }

        if (states.invalid && !states.disabled) {
          decorator += "-invalid";
        } else if (states.focused) {
          decorator += "-focused";
        }

        return {
          backgroundColor: _backC,
          decorator : decorator,
          padding : [3, 8],
          cursor: states.disabled ? undefined : "pointer",
          minWidth: 5,
          minHeight: 5,
          textColor : states.disabled ? "text-disabled" : _textColor
        };
      }
    },

    "treevirtual-folder" :
    {
      style : function(states)
      {
        return {
          icon : (states.opened
                  ? "atms/folder-open.png"
                  : "atms/folder-close.png")
        }
      }
    },

    "tree-folder" :
    {
      style : function(states)
      {
        var backgroundColor;
        if (states.selected) {
          backgroundColor = "background-selected";
          if (states.disabled) {
            backgroundColor += "-disabled";
          }
        }
        return {
          padding : [2, 8, 2, 5],
          icon : states.opened ? "atms/folder-open.png" : "atms/folder-close.png",
          backgroundColor : backgroundColor,
          iconOpened : "atms/folder-open.png"
        };
      }
    },

    "dragdrop-cursor" :
    {
      style : function(states)
      {
        var icon = "sad";

        if (states.copy) {
          icon = "wink";
        } else if (states.move) {
          icon = "wink";
        } else if (states.alias) {
          icon = "wink";
        }

        return {
          source : "atms/" + icon + ".png",
          position : "right-top",
          offset : [ 18, 0, 0, 18 ]
        };
      }
    },

     /*
    ---------------------------------------------------------------------------
      FILTER
    ---------------------------------------------------------------------------
    */

    "atms/atms" : {
      style : function(states) {
        return {
          backgroundColor : "highlight-shade"
        }
      }
    },

    "atms-filterBox/filter-status" : {
      style : function(states) {
        return {
          font: "filterFont"
        }
      }
    },

    "atms-filterBox/filter-status/legend" : {
      alias: "check-groupbox/legend",
      include: "check-groupbox/legend",

      style : function(states) {
        return {
          textColor: "white",
          backgroundColor : "highlight-shade",
          margin : 0,
          font: "filterFont"
        }
      }
    },

    "atms-filterBox/filter-status/frame" : {

      style : function(states) {
        return {
          backgroundColor : "background",
          padding : [6, 9],
          margin: [18, 2, 2, 2],
          decorator  : "white-box"
        };
      }
    },

    "atms-filterBox/filter-user" : "atms-filterBox/filter-status",
    "atms-filterBox/filter-test" : "atms-filterBox/filter-status",
    "atms-filterBox/filter-qualificator" : "atms-filterBox/filter-status",
    "atms-filterBox/filter-name" : "atms-filterBox/filter-status",
    "atms-filterBox/filter-modified_on" : "atms-filterBox/filter-status",

    "atms-filterBox/filter-user/check-groupbox/widget/list" : {
      style : function(states) {
        return {
          textColor: "red",
          backgroundColor : "highlight"
        }
      }
    },

    "filter-item" : {
      style : function(states) {
        return {
          backgroundColor : "highlight-shade"
        }
      }
    },

    /*
    ---------------------------------------------------------------------------
      TABVIEW
    ---------------------------------------------------------------------------
    */

    "tabview" : {},

    "tabview/bar" :
    {
      alias : "slidebar",

      style : function(states)
      {
        var marginTop=0, marginRight=0, marginBottom=0, marginLeft=0;

        if (states.barTop) {
          marginBottom -= 1;
        } else if (states.barBottom) {
          marginTop -= 1;
        } else if (states.barRight) {
          marginLeft -= 1;
        } else {
          marginRight -= 1;
        }

        return {
          marginBottom : marginBottom,
          marginTop : marginTop,
          marginLeft : marginLeft,
          marginRight : marginRight
        };
      }
    },


    "tabview/bar/button-forward" :
    {
      include : "slidebar/button-forward",
      alias : "slidebar/button-forward",

      style : function(states)
      {
        if (states.barTop) {
          return {
            marginTop : 4,
            marginBottom: 2,
            decorator : null
          }
        } else if (states.barBottom) {
          return {
            marginTop : 2,
            marginBottom: 4,
            decorator : null
          }
        } else if (states.barLeft) {
          return {
            marginLeft : 4,
            marginRight : 2,
            decorator : null
          }
        } else {
          return {
            marginLeft : 2,
            marginRight : 4,
            decorator : null
          }
        }
      }
    },

    "tabview/bar/button-backward" :
    {
      include : "slidebar/button-backward",
      alias : "slidebar/button-backward",

      style : function(states)
      {
        if (states.barTop) {
          return {
            marginTop : 4,
            marginBottom: 2,
            decorator : null
          }
        } else if (states.barBottom) {
          return {
            marginTop : 2,
            marginBottom: 4,
            decorator : null
          }
        } else if (states.barLeft) {
          return {
            marginLeft : 4,
            marginRight : 2,
            decorator : null
          }
        } else {
          return {
            marginLeft : 2,
            marginRight : 4,
            decorator : null
          }
        }
      }
    },

    "tabview/pane" :
    {
      style : function(states)
      {
        return {
          backgroundColor : "background",
          padding : 0
        };
      }
    },

    "tabview-page" : "widget",

    "tabview-page/button" :
    {
      style : function(states)
      {
        var decorator;

        // default padding
        if (states.barTop || states.barBottom) {
          decorator = "tabview-page-button-bottom-atms";
          var padding = [4, 4, 4, 4];
        } else {
          var padding = [8, 4, 8, 4];
        }

        // // decorator
        // if (states.checked) {
        //   if (states.barTop) {
        //     decorator = "tabview-page-button-top";
        //   } else if (states.barBottom) {
        //     decorator = "tabview-page-button-bottom"
        //   } else if (states.barRight) {
        //     decorator = "tabview-page-button-right";
        //   } else if (states.barLeft) {
        //     decorator = "tabview-page-button-left";
        //   }
        // } else {
        //   for (var i=0; i < padding.length; i++) {
        //     //padding[i] += 1;
        //   }
        //   // reduce the size by 1 because we have different decorator border width
        //   if (states.barTop) {
        //     padding[2] -= 1;
        //   } else if (states.barBottom) {
        //     padding[0] -= 1;
        //   } else if (states.barRight) {
        //     padding[3] -= 1;
        //   } else if (states.barLeft) {
        //     padding[1] -= 1;
        //   }
        // }

        return {
          zIndex : states.checked ? 10 : 5,
          decorator : states.checked ? decorator + '-selected' : decorator,
          textColor : states.disabled ? "text-disabled" : states.checked ? null : null,
          font: "filterFont",
          padding : padding,
          cursor: "pointer"
        };
      }
    },

    "tabview-page/button/label" :
    {
      alias : "label",

      style : function(states)
      {
        return {
          padding : [0, 1, 0, 1]
        };
      }
    },

    "tabview-page/button/icon" : "image",
    "tabview-page/button/close-button" :
    {
      alias : "atom",
      style : function(states)
      {
        return {
          cursor : states.disabled ? undefined : "pointer",
          icon : qx.theme.simple.Image.URLS["tabview-close"]
        };
      }
    },

    "upload-widget" : {
      base: true,
      include: "widget",
      alias: "widget",

      style : function(states, styles)
      {
        return {
          backgroundColor : states.upload ? "highlight-shade" : undefined,
          decorator: states.helpImage ? "dragndrop-upload" : undefined
        }
      }
    },

    "under-construction" : {
      style: function(states, styles)
      {
        return {
          decorator: "hard-work"
        }
      }
    },

    'datefield/textfield' : {
      style : function(styles)
      {
        return {
          decorator : 'no-border'
        }
      }
    },

    'datechooser/time-label' : {
      style : function(styles)
      {
        return {
          backgroundColor: "#323335",
          textColor: "white",
          font: 'bold',
          paddingLeft: 10
        }
      }
    },

    'datechooser/time-hour' : 'spinner',
    'datechooser/time-minutes' : 'spinner',

    'layout-win' : {
      style : function(styles)
      {
        return {
          backgroundColor: "#000000",
          padding: [10, 10, 10, 10]
        }
      }
    },

    'label-warning': {
      include: 'label',

      style: function(styles)
      {
        return {
          backgroundColor: "#FF2E12",
          textColor: "white",
          font: 'bold',
          padding: [4, 10, 4, 10]
        }
      }
    },
     /**
    * COSMETOSAFE
    */
    "textfield-no-border" :
    {
      include: "textfield",
      style: function(states)
      {
        return {
          decorator: null
        }
      }
    },

    "cs-gbFilterBox" : {
      alias: "check-groupbox",
      style : function(styles)
      {
        return {
          backgroundColor: "highlight"
        }
      }
    },
    "cs-gbFilterBox/legend" :
    {
      alias : "checkbox",
      include : "checkbox",

      style : function(states)
      {
        return {
          textColor : states.invalid ? "invalid" : "white",
          padding : 2,
          margin : 2,
          font: 'filterFont'
        };
      }
    },

    "cs-MinMaxFilter" : "widget",
    "cs-MinMaxFilter/min" : "spinner",
    "cs-MinMaxFilter/max" : "spinner",
    "cs-MinMaxFilter/min-date" : "datefield",
    "cs-MinMaxFilter/max-date" : "datefield",
    "cs-MinMaxFilter/min-datetime" : "datefield",
    "cs-MinMaxFilter/max-datetime" : "datefield",
    "cs-MinMaxFilter/min-chbox" : "checkbox",
    "cs-MinMaxFilter/max-chbox" : "checkbox",

    token: "textfield-no-border",
    "token/textfield": {
      include: "textfield-no-border",
      style: function(states) {
        return {
          minWidth: 48,
          decorator: null
        }
      }
    },
    tokenitem:
    {
      include: 'listitem',
      style: function(states) {
        return {
          font: 'userlabel',
          decorator: 'no-border',
          backgroundColor: states.hovered ? 'background-selected' : 'highlight',
          textColor: 'white',
          height: 24,
          padding: [1, 6, 1, 6],
          margin: [0, 0, 0, 1],
          icon: 'atms/remove-sign.png'
        };
      }
    }
  }
});
