/* ************************************************************************

   Copyright:

   License:

   Authors:

************************************************************************ */

qx.Theme.define("atms.theme.Theme",
{
  meta :
  {
    color : atms.theme.Color,
    decoration : atms.theme.Decoration,
    font : atms.theme.Font,
    icon : qx.theme.icon.Tango,
    appearance : atms.theme.Appearance
  }
});