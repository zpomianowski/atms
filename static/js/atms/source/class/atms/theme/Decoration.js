/* ************************************************************************

   Copyright:

   License:

   Authors:

************************************************************************ */

qx.Theme.define("atms.theme.Decoration",
{
  extend : qx.theme.indigo.Decoration,

  decorations :
  {
    "ca-view-left" : {
      style:
      {
        width: [1, 1, 0, 1],
        color: "#e3e3e3"
      }
    },

    "ca-view" : {
      style:
      {
        width: [1, 1, 0, 0],
        color: "#e3e3e3"
      }
    },

    "comment" :
    {
      style:
      {
        radius: [4,4,4,4],
        width: 2,
        color: "#e6e6e6"
      }
    },

    "comment_not_read" :
    {
      style:
      {
        radius: [4,4,4,4],
        width: 2,
        color: ["#e6e6e6", "#FF8000", "#e6e6e6", "#e6e6e6"]
      }
    },

    "progressbar" :
    {
      style:
      {
        backgroundColor: "#242424",
        width: 0,
        color: "border-separator"
      }
    },

    /*
    ---------------------------------------------------------------------------
      TEXT FIELD
    ---------------------------------------------------------------------------
    */
    "inset" :
    {
      style :
      {
        width : 1,
        color : [ "transparent", "transparent", "border-light", "border-light" ]
      }
    },

    "focused-inset" :
    {
      style :
      {
        width : 2,
        color : "background-selected"
      }
    },

    "border-invalid" :
    {
      style :
      {
        width : 2,
        color : "invalid"
      }
    },

    "markitup-invalid" :
    {
      style :
      {
        width : [0, 0, 2, 0],
        color : "invalid"
      }
    },

    /*
    ---------------------------------------------------------------------------
      SCROLL KNOB
    ---------------------------------------------------------------------------
    */
    "scroll-knob" :
    {
      style :
      {
        radius : 0,
        width : 0,
        color : "button-border",
        backgroundColor : "scrollbar-bright"
      }
    },

    "scroll-knob-pressed" :
    {
      include : "scroll-knob",

      style :
      {
        backgroundColor : "scrollbar-dark"
      }
    },

    "scroll-knob-hovered" :
    {
      include: "scroll-knob",

      style :
      {
        color : "button-border-hovered"
      }
    },

    "scroll-knob-pressed-hovered" :
    {
      include: "scroll-knob-pressed",

      style :
      {
        color : "button-border-hovered"
      }
    },

        /*
    ---------------------------------------------------------------------------
      WINDOW
    ---------------------------------------------------------------------------
    */
    "window" :
    {
      style :
      {
        width : 0,
        color : "window-border",
        shadowLength : 0,
        shadowBlurRadius : 2,
        shadowColor : "shadow",
        backgroundColor : "background",
        radius: 0
      }
    },

    "window-active" :
    {
      include : "window",

      style :
      {
        shadowLength : 0,
        shadowBlurRadius : 10
      }
    },

    "window-caption" : {
      style :
      {
        radius: [3, 3, 0, 0],
        color: "window-border",
        widthBottom: 1
      }
    },

    "window-caption-active" : {
      style :
      {
        radius: [3, 3, 0, 0],
        color: "highlight",
        widthBottom: 3
      }
    },

  	    /*
    ---------------------------------------------------------------------------
      BUTTON
    ---------------------------------------------------------------------------
    */
    "button-box" :
    {
      style :
      {
        //radius : 3,
        width : 1,
        //color : "button-border",
        //gradientStart : ["button-box-bright", 40],
        //gradientEnd : ["button-box-dark", 70],
        backgroundColor : "button-box-bright"
      }
    },

    "button-box-pressed" :
    {
      include : "button-box",

      style :
      {
      	backgroundColor : "#eeeeee"
        // gradientStart : ["button-box-bright-pressed", 40],
        // gradientEnd : ["button-box-dark-pressed", 70],
        // backgroundColor : "button-box-bright-pressed"
      }
    },

    "button-box-pressed-hovered" :
    {
      include : "button-box-pressed",

      style :
      {
      	backgroundColor : "#ff8000"
        //color : "button-border-hovered"
      }
    },

    "button-box-hovered" :
    {
      include : "button-box",

      style :
      {
      	backgroundColor : "#ff8000"
        //color : "button-border-hovered"
      }
    },


    /*
    ---------------------------------------------------------------------------
      BUTTON INVALID
    ---------------------------------------------------------------------------
    */
    "button-box-invalid" :
    {
      include : "button-box",

      style :
      {
        color : "invalid"
      }
    },

    "button-box-pressed-invalid" :
    {
      include : "button-box-pressed",

      style :
      {
        color : "invalid"
      }
    },

    "button-box-hovered-invalid" : {include: "button-box-invalid"},

    "button-box-pressed-hovered-invalid" : {include: "button-box-pressed-invalid"},


    /*
    ---------------------------------------------------------------------------
      BUTTON FOCUSED
    ---------------------------------------------------------------------------
    */
    "button-box-focused" :
    {
      include : "button-box",

      style :
      {
        //color : "background-selected"
      }
    },

    "button-box-pressed-focused" :
    {
      include : "button-box-pressed",

      style :
      {
        //color : "background-selected"
      }
    },

    "button-box-hovered-focused" : {include: "button-box-focused"},

    "button-box-pressed-hovered-focused" : {include: "button-box-pressed-focused"},


    /*
    ---------------------------------------------------------------------------
      BUTTON RIGHT
    ---------------------------------------------------------------------------
    */
    "button-box-right" :
    {
      include : "button-box",

      style :
      {
        radius : [0, 3, 3, 0]
      }
    },

    "button-box-pressed-right" :
    {
      include : "button-box-pressed",

      style :
      {
        radius : [0, 3, 3, 0]
      }
    },

    "button-box-pressed-hovered-right" :
    {
      include : "button-box-pressed-hovered",

      style :
      {
        radius : [0, 3, 3, 0]
      }
    },

    "button-box-hovered-right" :
    {
      include : "button-box-hovered",

      style :
      {
        radius : [0, 3, 3, 0]
      }
    },

    "button-box-focused-right" :
    {
      include : "button-box-focused",

      style :
      {
        radius : [0, 3, 3, 0]
      }
    },

    "button-box-hovered-focused-right" :
    {
      include : "button-box-hovered-focused",

      style :
      {
        radius : [0, 3, 3, 0]
      }
    },

    "button-box-pressed-focused-right" :
    {
      include : "button-box-pressed-focused",

      style :
      {
        radius : [0, 3, 3, 0]
      }
    },

    "button-box-pressed-hovered-focused-right" :
    {
      include : "button-box-pressed-hovered-focused",

      style :
      {
        radius : [0, 3, 3, 0]
      }
    },


    /*
    ---------------------------------------------------------------------------
      BUTTON BORDERLESS RIGHT
    ---------------------------------------------------------------------------
    */
    "button-box-right-borderless" :
    {
      include : "button-box",

      style :
      {
        radius : [0, 3, 3, 0],
        width: [1, 1, 1, 0]
      }
    },

    "button-box-pressed-right-borderless" :
    {
      include : "button-box-pressed",

      style :
      {
        radius : [0, 3, 3, 0],
        width: [1, 1, 1, 0]
      }
    },

    "button-box-pressed-hovered-right-borderless" :
    {
      include : "button-box-pressed-hovered",

      style :
      {
        radius : [0, 3, 3, 0],
        width: [1, 1, 1, 0]
      }
    },

    "button-box-hovered-right-borderless" :
    {
      include : "button-box-hovered",

      style :
      {
        radius : [0, 3, 3, 0],
        width: [1, 1, 1, 0]
      }
    },


    /*
    ---------------------------------------------------------------------------
      BUTTON TOP RIGHT
    ---------------------------------------------------------------------------
    */
    "button-box-top-right" :
    {
      include : "button-box",

      style :
      {
        radius : [0, 3, 0, 0],
        width: [1, 1, 1, 0]
      }
    },

    "button-box-pressed-top-right" :
    {
      include : "button-box-pressed",

      style :
      {
        radius : [0, 3, 0, 0],
        width: [1, 1, 1, 0]
      }
    },

    "button-box-pressed-hovered-top-right" :
    {
      include : "button-box-pressed-hovered",

      style :
      {
        radius : [0, 3, 0, 0],
        width: [1, 1, 1, 0]
      }
    },

    "button-box-hovered-top-right" :
    {
      include : "button-box-hovered",

      style :
      {
        radius : [0, 3, 0, 0],
        width: [1, 1, 1, 0]
      }
    },


    /*
    ---------------------------------------------------------------------------
      BUTTON BOTOM RIGHT
    ---------------------------------------------------------------------------
    */
    "button-box-bottom-right" :
    {
      include : "button-box",

      style :
      {
        radius : [0, 0, 3, 0],
        width : [0, 1, 1, 0]
      }
    },

    "button-box-pressed-bottom-right" :
    {
      include : "button-box-pressed",

      style :
      {
        radius : [0, 0, 3, 0],
        width : [0, 1, 1, 0]
      }
    },

    "button-box-pressed-hovered-bottom-right" :
    {
      include : "button-box-pressed-hovered",

      style :
      {
        radius : [0, 0, 3, 0],
        width : [0, 1, 1, 0]
      }
    },

    "button-box-hovered-bottom-right" :
    {
      include : "button-box-hovered",

      style :
      {
        radius : [0, 0, 3, 0],
        width : [0, 1, 1, 0]
      }
    },


    /*
    ---------------------------------------------------------------------------
      BUTTON BOTOM LEFT
    ---------------------------------------------------------------------------
    */
    "button-box-bottom-left" :
    {
      include : "button-box",

      style :
      {
        radius : [0, 0, 0, 3],
        width : [0, 0, 1, 1]
      }
    },

    "button-box-pressed-bottom-left" :
    {
      include : "button-box-pressed",

      style :
      {
        radius : [0, 0, 0, 3],
        width : [0, 0, 1, 1]
      }
    },

    "button-box-pressed-hovered-bottom-left" :
    {
      include : "button-box-pressed-hovered",

      style :
      {
        radius : [0, 0, 0, 3],
        width : [0, 0, 1, 1]
      }
    },

    "button-box-hovered-bottom-left" :
    {
      include : "button-box-hovered",

      style :
      {
        radius : [0, 0, 0, 3],
        width : [0, 0, 1, 1]
      }
    },


    /*
    ---------------------------------------------------------------------------
      BUTTON TOP LEFT
    ---------------------------------------------------------------------------
    */
    "button-box-top-left" :
    {
      include : "button-box",

      style :
      {
        radius : [3, 0, 0, 0],
        width : [1, 0, 0, 1]
      }
    },

    "button-box-pressed-top-left" :
    {
      include : "button-box-pressed",

      style :
      {
        radius : [3, 0, 0, 0],
        width : [1, 0, 0, 1]
      }
    },

    "button-box-pressed-hovered-top-left" :
    {
      include : "button-box-pressed-hovered",

      style :
      {
        radius : [3, 0, 0, 0],
        width : [1, 0, 0, 1]
      }
    },

    "button-box-hovered-top-left" :
    {
      include : "button-box-hovered",

      style :
      {
        radius : [3, 0, 0, 0],
        width : [1, 0, 0, 1]
      }
    },


    /*
    ---------------------------------------------------------------------------
      BUTTON MIDDLE
    ---------------------------------------------------------------------------
    */
    "button-box-middle" :
    {
      include : "button-box",

      style :
      {
        radius : 0,
        width : [1, 0, 1, 1]
      }
    },

    "button-box-pressed-middle" :
    {
      include : "button-box-pressed",

      style :
      {
        radius : 0,
        width : [1, 0, 1, 1]
      }
    },

    "button-box-pressed-hovered-middle" :
    {
      include : "button-box-pressed-hovered",

      style :
      {
        radius : 0,
        width : [1, 0, 1, 1]
      }
    },

    "button-box-hovered-middle" :
    {
      include : "button-box-hovered",

      style :
      {
        radius : 0,
        width : [1, 0, 1, 1]
      }
    },


    /*
    ---------------------------------------------------------------------------
      BUTTON LEFT
    ---------------------------------------------------------------------------
    */
    "button-box-left" :
    {
      include : "button-box",

      style :
      {
        radius : [3, 0, 0, 3],
        width : [1, 0, 1, 1]
      }
    },

    "button-box-pressed-left" :
    {
      include : "button-box-pressed",

      style :
      {
        radius : [3, 0, 0, 3],
        width : [1, 0, 1, 1]
      }
    },

    "button-box-pressed-hovered-left" :
    {
      include : "button-box-pressed-hovered",

      style :
      {
        radius : [3, 0, 0, 3],
        width : [1, 0, 1, 1]
      }
    },

    "button-box-hovered-left" :
    {
      include : "button-box-hovered",

      style :
      {
        radius : [3, 0, 0, 3],
        width : [1, 0, 1, 1]
      }
    },

    "button-box-focused-left" :
    {
      include : "button-box-focused",

      style :
      {
        radius : [3, 0, 0, 3],
        width : [1, 0, 1, 1]
      }
    },

    "button-box-hovered-focused-left" :
    {
      include : "button-box-hovered-focused",

      style :
      {
        radius : [3, 0, 0, 3],
        width : [1, 0, 1, 1]
      }
    },

    "button-box-pressed-hovered-focused-left" :
    {
      include : "button-box-pressed-hovered-focused",

      style :
      {
        radius : [3, 0, 0, 3],
        width : [1, 0, 1, 1]
      }
    },

    "button-box-pressed-focused-left" :
    {
      include : "button-box-pressed-focused",

      style :
      {
        radius : [3, 0, 0, 3],
        width : [1, 0, 1, 1]
      }
    },

    /*
    ---------------------------------------------------------------------------
      CUSTOM
    ---------------------------------------------------------------------------
    */

    "filter-tag" :
    {
      style :
      {
        radius : [6, 0, 0, 6],
        width : [0, 4, 0, 0],
        color: "background-selected"
      }
    },

    "tabview-page-button-bottom-atms":
    {
      style :
      {
        width : [1, 0, 0, 0],
        color : "highlight"
      }
    },

    "tabview-page-button-bottom-atms-selected":
    {
      style :
      {
        width: [3, 0, 0, 0],
        color : "highlight"
      }
    },

    "dragndrop-upload" :
    {
      style:
      {
        backgroundImage: "atms/uploader_bckgr.png",
        backgroundRepeat: "no-repeat",
        backgroundPosition: ['center', 'center']
      }
    },

    "hard-work" :
    {
      style:
      {
        backgroundImage: "atms/hard_work.png",
        backgroundRepeat: "no-repeat",
        backgroundPosition: ['center', 'center']
      }
    },

    "no-border" :
    {
      style :
      {
        width : [0, 0, 0, 0],
        color : "background-selected"
      }
    }
  }
});
