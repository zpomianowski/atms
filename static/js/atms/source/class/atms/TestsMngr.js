/**
*@asset(atms/*)
*@asset(qx/icon/${qx.icontheme}/*)
*/

qx.Class.define('atms.TestsMngr',
{
  extend: qx.ui.container.Composite,

  construct: function() {
    this.base(arguments);
    this.setLayout(new qx.ui.layout.VBox(5));

    // calendar management
    var calButton = new qx.ui.form.ToggleButton(this.tr('Manage tests'));
    calButton.addListener('changeValue', function(e) {
      var value = e.getData();
      if (value) {
        var cal = new atms.Calendar();
        var win = new atms.WinPanel(cal);
        qx.core.Init.getApplication().gDesk1.add(win);
        win.open();
        win.maximize();

        win.addListener('close', function() {
          calButton.setValue(false);
        }, this);

        qx.data.SingleValueBinding.bind(calButton, 'value', win, 'alive');
      }
    }, this);

    // project choosment
    // var container1 = new qx.ui.container.Composite(new qx.ui.layout.HBox(5));
    // var label = new qx.ui.basic.Label(this.tr('Project'));
    // this.__activeProject = new qx.ui.form.VirtualSelectBox();
    // container1.add(label);
    // container1.add(this.__activeProject, { flex: 1 });

    // table with projects
    this.__table = new atms.table.TableTest();

    // build ui
    this.add(calButton);
    // this.add(container1);
    this.add(this.__table, { flex: 1 });

    this.__setupResources();
  },

  members: {
    __table: null,
    __activeProject: null,

    __setupResources: function() {
      // var that = this;
      // var chooser = this.__activeProject;
      // this.__R = new qx.io.rest.Resource({
      //   get: { method: 'GET',    url: '/atms/manager/sautis_api/projects.json' }
      // });
      // var store = new qx.data.store.Rest(this.__R, 'get', {
      //   manipulateData: function(data) {
      //     data.content.splice(0, 0, { id: null, name: that.tr('<None>') });
      //     return data;
      //   }
      // });
      // store.bind('model.content', chooser, 'model');
      // chooser.setLabelPath('name');
      //
      // chooser.getSelection().addListener('change', function(e) {
      //   var value = chooser.getSelection().getItem(0).getId();
      //   if (value)
      //     this.__table.setHiddenQuery({
      //       project: value });
      //   else this.__table.setHiddenQuery({});
      // }, this);
      //
      // this.__R.get();
    }
  }
});
