/**
*@asset(atms/*)
*@asset(qx/icon/${qx.icontheme}/*)
*/

qx.Class.define('atms.TForm',
{
  extend: qx.core.Object,
  include: [qx.locale.MTranslation],

  construct: function(data_type, mode) {
    this.base(arguments);
    this.__data_type = data_type;
    this.__mode = mode;
    this.__map = {
      atms_cases: {
        parent: { widget: null },
        user: {
          widget: 'mselectb',
          row: 2,
          col: 1,
          label: this.tr('Assigned'),
          store_name: 'atms_cases',
          store_path: 'user',
          do_pass: false
        },
        test: {
          widget: 'mselectb',
          row: 2,
          col: 3,
          label: this.tr('Test'),
          store_name: 'atms_cases',
          store_path: 'test',
          do_pass: false
        },
        name: {
          row: 0,
          col: 1,
          colSpan: 3,
          label: this.tr('Name'),
          validator: 'required',
          do_enabled: false
        },
        path: {
          row: 1,
          col: 1,
          colSpan: 3,
          label: this.tr('Qualificator'),
          do_pass: false
        },
        procedure_text: {
          widget: 'markitup',
          label: this.tr('Procedure'),
          icon: 'atms/details.png',
          do_enabled: false
        },
        expected_result: {
          widget: 'markitup',
          label: this.tr('Expected result'),
          do_enabled: false
        },
        result_notes: {
          widget: 'markitup',
          label: this.tr('Result notes'),
          icon: 'atms/warning-sign.png'
        },
        expected_time: {
          widget: 'time',
          row: 20,
          col: 1,
          label: this.tr('Expected time'),
          do_pass: false,
          default_value: 15
        },
        result_time: {
          widget: 'time',
          row: 21,
          col: 1,
          label: this.tr('Result time')
        },
        status: {
          widget: 'mselectb',
          row: 21,
          col: 3,
          store_name: 'atms_cases',
          store_path: 'status',
          nullv: false,
          label: this.tr('Status'),
          default_value: 'new'
        },
        weight: {
          widget: 'spinner',
          row: 20,
          col: 3,
          label: this.tr('Impact factor'),
          do_pass: false,
          default_value: 1
        }
      },
      atms_tcases: {
        node: { widget: null },
        name: {
          row: 0,
          col: 1,
          colSpan: 3,
          label: this.tr('Name'),
          validator: 'required',
          do_enabled: false
        },
        procedure_text: {
          widget: 'markitup',
          label: this.tr('Procedure'),
          icon: 'atms/details.png',
          do_enabled: false
        },
        expected_result: {
          widget: 'markitup',
          label: this.tr('Expected result'),
          do_enabled: false
        },
        expected_time: {
          widget: 'time',
          row: 20,
          col: 1,
          label: this.tr('Expected time'),
          do_pass: false,
          default_value: 15
        },
        weight: {
          widget: 'spinner',
          row: 20,
          col: 3,
          label: this.tr('Impact factor'),
          do_pass: false,
          default_value: 1
        }
      },
      atms_nodes: {
        node: { widget: null },
        name: {
          row: 0,
          col: 1,
          colSpan: 3,
          label: this.tr('Name'),
          validator: 'required'
        },
        description: {
          widget: 'markitup',
          label: this.tr('Description'),
          icon: 'atms/details.png'
        }
      },
      atms_ttests: {
        name: {
          row: 0,
          col: 1,
          colSpan: 3,
          label: this.tr('Name'),
          validator: 'required'
        }
      },
      atms_tests: {
        project: {
          widget: 'atms.list',
          row: 1,
          col: 1,
          label: this.tr('Active project'),
          store_name: 'projects'
        },
        ttest: {
          widget: 'atms.list',
          row: 2,
          col: 1,
          label: this.tr('Copy from'),
          store_name: 'ttests'
        },
        name: {
          row: 0,
          col: 1,
          colSpan: 3,
          label: this.tr('Name'),
          validator: 'required'
        },
        description: {
          widget: 'markitup',
          label: this.tr('Description'),
          icon: 'atms/details.png'
        },
        summary: {
          widget: 'markitup',
          label: this.tr('Summary'),
          icon: 'atms/details.png'
        },
        start: {
          widget: 'date',
          label: this.tr('From'),
          row: 3,
          col: 1
        },
        stop: {
          widget: 'date',
          label: this.tr('To'),
          row: 3,
          col: 3
        }
      },
      atms_uecap: {
        f_model: {
          row: 0,
          col: 1,
          colSpan: 3,
          label: this.tr('Model'),
          validator: 'required'
        },
        f_company: {
          row: 1,
          col: 1,
          colSpan: 3,
          label: this.tr('Company'),
          validator: 'required'
        },
        f_sw_version: {
          row: 1,
          col: 1,
          colSpan: 3,
          label: this.tr('SW version'),
          validator: 'required'
        }
      },
      atms_devices: {
        settings: {
          widget: 'code',
          vbox: true,
          row: 1,
          rowSpan: 10,
          col: 2,
          colSpan: 2,
          label: this.tr('Settings')
        },
        type: {
          widget: 'mselectb',
          store_name: 'atms_devices',
          store_path: 'type',
          row: 0,
          col: 1,
          colSpan: 1,
          label: this.tr('Type'),
          validator: 'required'
        },
        host: {
          row: 1,
          col: 1,
          colSpan: 1,
          label: this.tr('Host'),
          validator: 'required'
        },
        subtype: {
          row: 2,
          col: 1,
          colSpan: 1,
          label: this.tr('Subtype')
        },
        model: {
          row: 3,
          col: 1,
          colSpan: 1,
          label: this.tr('Model'),
          validator: 'required'
        },
        manufacturer: {
          row: 4,
          col: 1,
          colSpan: 1,
          label: this.tr('Manufacturer'),
          validator: 'required'
        },
        udid: {
          row: 5,
          col: 1,
          colSpan: 1,
          label: 'UDID/SN',
          validator: 'required'
        },
        android_version: {
          row: 6,
          col: 1,
          colSpan: 1,
          label: this.tr('Android version')
        },
        sdk: {
          row: 7,
          col: 1,
          colSpan: 1,
          label: 'SDK'
        },
        state: {
          widget: 'mselectb',
          store_name: 'atms_devices',
          store_path: 'state',
          row: 8,
          col: 1,
          colSpan: 1,
          label: this.tr('State')
        },
        connected: {
          widget: 'checkbox',
          row: 9,
          col: 1,
          colSpan: 1,
          label: this.tr('Connected')
        }
      },
      atms_canteen: {
        f_name: {
          row: 0,
          col: 1,
          colSpan: 3,
          label: this.tr('Name'),
          validator: 'required'
        },
        f_contact: {
          row: 1,
          col: 1,
          colSpan: 3,
          label: this.tr('Contact'),
          validator: 'required'
        },
        f_desc: {
          widget: 'textarea',
          row: 2,
          col: 1,
          rowSpan: 10,
          colSpan: 3,
          label: this.tr('Notes')
        },
        f_atplace: {
          widget: 'checkbox',
          row: 13,
          col: 1,
          colSpan: 1,
          label: this.tr('At place<br>(no janitor)')
        }
      }
    };
  },

  members: {
    __mode: null,
    __data_type: null,
    __map: null,

    getConfigAll: function() {
      var map = {};
      for (var key in this.__map[this.__data_type]) {
        var data = this.getConfig(key);
        if (data[this.__mode + '_pass'] == false) data.widget = null;
        map[key] = data;
      }

      return map;
    },

    getConfig: function(key) {
      return this.__getKeyConfig(
        this.__map[this.__data_type][key]);
    },

    setDefault: function(key, value) {
      this.__map[this.__data_type][key]['default_value'] = value;
    },

    getModelTemplate: function(ids) {
      var ids = ids || {};
      var data = {};
      for (var key in this.__map[this.__data_type]) {
        if (this.getConfig(key)[this.__mode + '_pass'] < 0) continue;
        data[key] = this.getConfig(key).default_value;
      }

      for (var key in ids) {
        if (key in data) data[key] = ids[key];
      }

      return qx.data.marshal.Json.createModel(data);
    },

    __getKeyConfig: function(arg) {
      var map = new Object();

      map.do_pass = arg.do_pass === undefined ? true : arg.do_pass;
      map.update_pass = arg.update_pass === undefined ? true : arg.update_pass;
      map.create_enabled = arg.create_enabled === undefined ? true : arg.create_enabled;
      map.update_enabled = arg.update_enabled === undefined ? true : arg.update_enabled;
      map.do_enabled = arg.do_enabled === undefined ? true : arg.do_enabled;
      map.vbox = arg.vbox === undefined ? false : arg.vbox;

      map.default_value = (arg.hasOwnProperty('default_value')
        ? arg.default_value : '');
      map.store_name = arg.store_name || null;
      map.store_path = arg.store_path || 'name';
      map.nullv = arg.nullv === undefined ? true : false;

      map.label = arg.label || this.tr('Label');
      map.widget = arg.widget === undefined ? 'textfield' : arg.widget;
      map.row = arg.row != null ? arg.row : null;
      map.col = arg.col != null ? arg.col : null;
      map.colSpan = arg.colSpan || 1;
      map.rowSpan = arg.rowSpan || 1;
      map.icon = arg.icon || 'atms/bolt.png';

      map.validator = arg.validator || null;
      map.invalidmsg = arg.invalidmsg || this.tr('Cannot be empty!');
      return map;
    }
  }
});
