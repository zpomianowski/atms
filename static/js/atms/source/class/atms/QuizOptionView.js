/**
*@asset(atms/*)
*@asset(qx/icon/${qx.icontheme}/*)
*/

qx.Class.define('atms.QuizOptionView',
{
  extend: qx.ui.core.Widget,
  include: [qx.ui.form.MModelProperty],

  properties: {
    id:
    {
      check: 'Integer',
      nullable: true
    },

    checked:
    {
      init: false,
      apply: '_applyChecked'
    },

    answear:
    {
      init: false,
      check: 'Boolean'
    },

    text:
    {
      init: '',
      check: 'String',
      apply: '_applyText'
    },

    state:
    {
      init: null,
      nullable: true,
      check: 'Boolean',
      apply: '_applyState'
    }
  },

  construct: function(mode) {
    this.__mode = mode;

    this.base(arguments);

    var layout = new qx.ui.layout.Grid(4, 4);
    layout.setColumnFlex(1, 1);
    this._setLayout(layout);

    this._createChildControl('checkbox');
    this._createChildControl('text');
    if (this.__mode != 'read')
        this._createChildControl('delete');
  },

  members: {
    __mode: null,

    _createChildControlImpl: function(id) {
      var control;
      switch (this.__mode + '_' + id)
      {
        case 'create_checkbox':
        case 'update_checkbox':
        case 'read_checkbox':
          var that = this;
          control = new qx.ui.form.ToggleButton(
              null, 'atms/check_nok.png');
          control.addListener('changeValue', function(e) {
            if (e.getData()) this.setIcon('atms/check_ok.png');
            else this.setIcon('atms/check_nok.png');
            that.setChecked(e.getData());
          });

          this._add(control, { row: 0, column: 0 });
        break;

        case 'create_text':
        case 'update_text':
          control = new markitup.Editor('/atms/manager/html_render.json');
          control.set({ allowGrowX: true });
          this._add(control, { row: 0, column: 1 });
        break;

        case 'read_text':
          control = new qx.ui.basic.Label().set({
            rich: true
          });
          this._add(control, { row: 0, column: 1 });
        break;

        case 'create_delete':
        case 'update_delete':
          control = new qx.ui.form.Button(
              this.tr('Delete'));
          this._add(control, { row: 0, column: 2 });
          control.addListener('execute', function() {
            this.getLayoutParent().remove(this);
          }, this);

        break;
      }
      return control || this.base(arguments, id);
    },

    _applyChecked: function(value, old) {
      var chbox = this.getChildControl('checkbox');
      chbox.setValue(value);
    },

    _applyText: function(value, old) {
      var txt = this.getChildControl('text');
      txt.setValue(value);
    },

    _applyState: function(value, old) {
      if (value == false)
          this.getChildControl('checkbox')
              .setBackgroundColor('state-critical');
    }
  }
});
