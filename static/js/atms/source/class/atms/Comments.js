/**
*@asset(atms/*)
*@asset(qx/icon/${qx.icontheme}/*)
*/

qx.Class.define('atms.Comments',
{
  extend: qx.ui.container.Composite,
  include: [atms.MWBlocker],

  construct: function(channel, members) {
    this.base(arguments);
    this.setMinWidth(300);
    this.setChannel(channel);
    members = members || [];
    if (members.length == 0)
        this.setMembers('_');
    else this.setMembers(members);

    this.setLayout(new qx.ui.layout.VBox(5));
    this.__msgs_panel = new qx.ui.container.Composite(
            new qx.ui.layout.VBox(5, 'bottom').set({
              reversed: true
            }));
    this.__msg_panel = new markitup.Editor('/atms/manager/html_render.json', true);
    this.__msg_panel.setMinHeight(100);
    this.__msg_panel.addListener('keypress', function(e) {
      var key = e.getKeyIdentifier();
      if (key == 'Enter' && e.isShiftPressed()) {
        var v = this.__msg_panel.getEditor().getValue();
        this.__msg_panel.setValue(v);
        sendB.fireEvent('execute');
        e.preventDefault();
      }
    }, this);

    var hintLabel = new qx.ui.basic.Label(
            this.tr('Shift+Enter sends the message')).set({
              alignX: 'right'
            });
    hintLabel.setAppearance('attlistitem/size');
    var sendB = new qx.ui.form.Button(this.tr('Send'));
    sendB.addListener('execute', function() {
      this.getBlocker().block();
      if (!this.__msg_panel.getValue()) return;
      this.__R.post('', {
        channel: this.getChannel(),
        msg: this.__msg_panel.getValue(),
        members: this.getMembers()
      });
    }, this);

    var refreshB = new qx.ui.form.Button(null, 'atms/refresh.png');
    refreshB.addListener('execute', function() {
      this.__R.getNew({
        channel: this.getChannel(),
        last_id: this.getNewestComment(),
        members: this.getMembers()
      });
    }, this);

    var scroll = new qx.ui.container.Scroll();
    scroll.addListener('mousewheel', function() {
      var h = this.__msgs_panel.getInnerSize().height;
      if (h - 500 < scroll.getScrollY())
          this.__sticky = true;
      else
          this.__sticky = false;
      if (scroll.getScrollY() == 0) {
        if (this.__whileOldProcessingLock) return;
        this.__whileOldProcessingLock = true;
        this.__R.getOld({
          channel: this.getChannel(),
          last_id: this.getOldestComment(),
          members: this.getMembers()
        });
      }
    }, this);

    scroll.add(this.__msgs_panel);
    this.add(scroll, { flex: 5 });
    this.__scroll = scroll;
    this.add(this.__msg_panel, { flex: 1 });
    var buttonHolder = new qx.ui.container.Composite(
        new qx.ui.layout.HBox());
    buttonHolder.add(sendB, { flex: 1 });
    buttonHolder.add(refreshB);
    this.add(buttonHolder);
    this.add(hintLabel);

    this.__setupResources();

    var that = this;

    // reload timer
    try {
      this.__newTimer = new qx.event.Timer(50);
      this.__newTimer.addListener('interval', function() {
        this.__R.getNew({
          channel: this.getChannel(),
          last_id: this.getNewestComment(),
          members: this.getMembers()
        });
      }, this);

      this.__ws = new atms.WebSocket(null, 'realtime/' + channel, function(e) {
        that.__newTimer.start();
      });

      this.__newTimer.addListener('interval', function() { this.__newTimer.stop(); }, this);
    } catch (err) {
      this.__newTimer.setInterval(5000);
      this.__newTimer.start();
    }

    this.__R.getNew({
      channel: this.getChannel(),
      last_id: this.getNewestComment(),
      members: this.getMembers()
    });
    this.__whileOldProcessingLock = false;
    this.__sticky = true;
  },

  properties: {
    channel: {
      init: '',
      event: 'channelChanged'

      //apply: "_applyChannel"
    },

    newestComment: {
      init: 0,
      check: 'Integer',
      event: 'newestCommentChanged',
      apply: '_applyNewestComment'
    },

    oldestComment: {
      init: 1e99,
      check: 'Integer',
      event: 'oldestCommentChanged',
      apply: '_applyOldestComment'
    },

    members: {
      event: 'membersChanged'
    }
  },

  members: {
    __scroll: null,
    __msgs_panel: null,
    __msg_panel: null,

    __ws: null,
    __newTimer: null,
    __whileOldProcessingLock: null,
    __sticky: null,

    __R: null,

    destroy: function() {
      if (this.__ws) this.__ws.close();
      this.__newTimer.stop();
      arguments.callee.base.apply(this, arguments);
    },

    __setupResources: function() {
      var api = {
        getNew: {
          method: 'GET',
          url: '/atms/manager/../chat/api/{channel}/{members}/0/{last_id}.json'
        },
        getOld: {
          method: 'GET',
          url: '/atms/manager/../chat/api/{channel}/{members}/1/{last_id}.json'
        },
        post: {
          method: 'POST',
          url: '/atms/manager/../chat/api'
        },
        del: {
          method: 'DELETE',
          url: '/atms/manager/../chat/api/{id}'
        }
      };
      this.__R = new qx.io.rest.Resource(api);
      this.__R.configureRequest(function(req) {
        req.setRequestHeader('Content-Type', 'application/json');
      });

      var processData = function(e, context) {
        var data = e.getData().content;
        var reversed = e.getData().reverse;
        var empty = !context.__msgs_panel.hasChildren();
        for (var el in data) {
          var item = data[el];
          if (item.id > context.getNewestComment())
              context.setNewestComment(item.id);
          if (item.id < context.getOldestComment())
              context.setOldestComment(item.id);
          var msg = new atms.CommentPost(context);
          msg.setId(item.id);
          msg.setUser(item.user);
          msg.setTxt(item.msg || '');
          msg.setIcon(item.icon);
          msg.setDeletable(item.deletable);
          msg.setTimestamp(item.timestamp);
          msg.setRead(item.read);
          if (empty || reversed)
              context.__msgs_panel.add(msg);
          else {
            context.__msgs_panel.addBefore(
                msg,
                context.__msgs_panel.getChildren()[0]);
          }
        }

        context.__whileOldProcessingLock = false;
        if (context.__sticky)
            context.__scroll.scrollToY(1e99);
      };

      this.__R.addListener('getNewSuccess', function(e) {
        processData(e, this);
      }, this);

      this.__R.addListener('getOldSuccess', function(e) {
        processData(e, this);
      }, this);

      this.__R.addListener('postSuccess', function(e) {
        this.__msg_panel.setValue('');
        this.getBlocker().unblock();
      }, this);
    },

    deletePost: function(id, callback) {
      this.__R.addListenerOnce('delSuccess', function() {
        callback();
      });

      this.__R.del({
        id: id
      });
    },

    _applyNewestComment: function(value, old) {
      if (value == old) return;
      this.__R.getNew({
        channel: this.getChannel(),
        last_id: this.getNewestComment(),
        members: this.getMembers()
      });
    },

    _applyOldestComment: function(value, old) {}
  }
});
