/**
*@asset(atms/*)
*@asset(qx/icon/${qx.icontheme}/*)
*/
qx.Class.define('atms.TimeFormat',
{
  extend: qx.util.format.NumberFormat,
  include: [qx.locale.MTranslation],

  construct: function() {
    this.base(arguments);

    this.__fM = 'M';
    this.__fw = 'w';
    this.__fd = 'd';
    this.__fh = 'h';
    this.__fm = 'm';

    this.setMode(false);
  },

  properties: {
    mode: {
      init: false,
      check: 'Boolean',
      apply: '_applyMode'
    }
  },

  members: {
    __M: null,
    __w: null,
    __d: null,
    __h: null,
    __m: null,

    __fM: null,
    __fw: null,
    __fd: null,
    __fh: null,

    __Mi: null,
    __wi: null,
    __di: null,
    __hi: null,
    __mi: null,

    _applyMode: function(value) {
      // machine mode
      if (value) {
        this.__Mi = 43800;
        this.__wi = 10080;
        this.__di = 1440;
        this.__hi = 60;
      } else {
        this.__Mi = 14600;
        this.__wi = 2400;
        this.__di = 480;
        this.__hi = 60;
      }
    },

    parse: function(string) {
      var string = String(string);
      var string = string.trim();
      var number = 0;
      var arr = string.split(' ');
      for (var i in arr) {
        var l     = arr[i].length - 1;
        var quant = arr[i][l];

        var parsed = parseInt(arr[i]);
        if (isNaN(parsed)) continue;
        switch (quant) {
          case this.__fM:
            number += parsed * this.__Mi;
          break;

          case this.__fw:
            number += parsed * this.__wi;
          break;

          case this.__fd:
            number += parsed * this.__di;
          break;

          case this.__fh:
            number += parsed * this.__hi;
          break;

          case this.__fm:
            number += parsed;
          break;

          default:
            number += parsed;
        }
      }

      return number;
    },

    format: function(number) {
      var number = number;
      this.__M = parseInt(number / this.__Mi);
      number = number % this.__Mi;
      this.__w = parseInt(number / this.__wi);
      number = number % this.__wi;
      this.__d = parseInt(number / this.__di);
      number = number % this.__di;
      this.__h = parseInt(number / this.__hi);
      number = number % this.__hi;
      this.__m = number;

      var s = '';
      if (this.__M) s += this.__M + this.__fM + ' ';
      if (this.__w) s += this.__w + this.__fw + ' ';
      if (this.__d) s += this.__d + this.__fd + ' ';
      if (this.__h) s += this.__h + this.__fh + ' ';
      if (this.__m) s += this.__m + this.__fm + ' ';

      return s;
    }
  }
});
