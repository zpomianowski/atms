/**
*@asset(atms/*)
*@asset(qx/icon/${qx.icontheme}/*)
*/

qx.Class.define('atms.DateTimeChooser',
{
  extend: qx.ui.control.DateChooser,

  construct: function() {
    this.base(arguments);
    this.setWidth(300);
    this.__nf = new qx.util.format.NumberFormat();
    this.__nf.setMinimumIntegerDigits(2);

    this._createChildControl('time-label');
    this._createChildControl('time-pane');
  },

  members: {
    __nf: null,

    _createChildControlImpl: function(id, hash) {
      var control = null;
      switch (id)
      {
        case 'time-label':
          control = new qx.ui.basic.Label(this.tr('Time'));
          control.setAllowGrowX(true);
          this._add(control, { flex: 1 });
          break;

        case 'time-pane':
          control = new qx.ui.container.Composite(new qx.ui.layout.HBox());
          control.add(new qx.ui.core.Spacer(20));
          control.add(this.getChildControl('time-hour'));
          control.add(new qx.ui.basic.Label(':'));
          control.add(this.getChildControl('time-minutes'));
          this._add(control);
          break;

        case 'time-hour':
          control = new qx.ui.form.Spinner();
          control.set({
            maximum: 24,
            minimum: -1
          });
          control.addListener('changeValue', function() {
            this.updateWidget();
          }, this);

          break;

        case 'time-minutes':
          control = new qx.ui.form.Spinner();
          control.set({
            maximum: 60,
            minimum: -1
          });
          control.addListener('changeValue', function() {
            this.updateWidget();
          }, this);

          control.setNumberFormat(this.__nf);
          break;

      }
      return control || arguments.callee.base.apply(this, arguments);
    },

    _onDayTap: function(evt) {
      var time = evt.getCurrentTarget().dateTime;
      this.updateWidget(new Date(time));
    },

    _applyValue: function(value, old) {
      arguments.callee.base.apply(this, arguments);
      if (value != null) {
        this.getChildControl('time-hour').setValue(value.getHours());
        this.getChildControl('time-minutes').setValue(value.getMinutes());
      }
    },

    updateWidget: function(date) {
      var date = date || this.getValue();

      var control = this.getChildControl('time-hour');
      var value = control.getValue();
      if (value == control.getMaximum()) {
        control.setValue(control.getMinimum() + 1);
        date.setHours(control.getMinimum() + 1);
      }

      if (value == control.getMinimum()) {
        control.setValue(control.getMaximum() - 1);
        date.setHours(control.getMaximum() - 1);
      } else {
        date.setHours(value);
      }

      var control = this.getChildControl('time-minutes');
      var value = control.getValue();
      if (value == control.getMaximum()) {
        control.setValue(control.getMinimum() + 1);
        date.setMinutes(control.getMinimum() + 1);
      }

      if (value == control.getMinimum()) {
        control.setValue(control.getMaximum() - 1);
        date.setMinutes(control.getMaximum() - 1);
      } else {
        date.setMinutes(value);
      }

      this.setValue(new Date(date.getTime()));
      this.execute();
    }
  }
});
