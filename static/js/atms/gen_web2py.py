import os
import shutil
import time

if __name__ == "__main__":
    os.system('python generate.py build')

    # copy compiled app to js folder in web2py app
    apppath = '../../web2py/applications/atms/static/js/qxatms'
    devappproject = '../../web2py/applications/atms/static/js/qxatms-dev'
    if os.path.isdir(apppath):
        shutil.rmtree(apppath)
    shutil.copytree('build', apppath)

    # copy dev project
    if os.path.isdir(devappproject):
        shutil.rmtree(devappproject)
    shutil.copytree('.', devappproject)
