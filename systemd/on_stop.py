#!/bin/python
# -*- coding: utf-8 -*-
from websocket_messaging import websocket_send

db = current.db
row = db(db.atms_servers.hostname == settings.host).select().first()
row.status = 'down'
row.update_record()
db(db.atms_devices.host == settings.host).update(
    state='offline', connected=False)
db.commit()

websocket_send(settings.ws_server, '', settings.security_key, 'atms_servers')
websocket_send(settings.ws_server, '', settings.security_key, 'atms_devices')
