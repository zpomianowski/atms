### ATMS - more advanced features
Needed for STB video tests.


#### RTMP/HLS/MPEG-DASH streaming via nginx with rtmp module
We have to compile the _nginx_ with _rtmp-module_:
```bash
cd /tmp
sudo apt update
sudo apt-get install libav-tools git build-essential libpcre3 libpcre3-dev libssl-dev htop
wget https://nginx.org/download/nginx-1.12.0.tar.gz
tar -zxvf nginx-1.12.0.tar.gz
git clone https://github.com/sergey-dryabzhinsky/nginx-rtmp-module.git
cd nginx-1.12.0
./configure --with-http_ssl_module --add-module=../nginx-rtmp-module
make
sudo make install
```

Make sure the _vod_ user is created:
```bash
sudo adduser vod
su vod
cd ~
mkdir video
cd video
wget http://atms-dev.lab.redefine.pl/vod/owca_720.flv
cp /tmp/rpi/dashplayer.html ..
```

Setup the rtmp environment and video test sources:

```bash
cd <ATMS dir>
sudo cp systemd/rtmp/nginx-rtmp.service /lib/systemd/system
sudo cp systemd/rtmp/hls.service /lib/systemd/system
sudo cp systemd/rtmp/dash.service /lib/systemd/system
sudo cp systemd/rtmp/nginx.conf /usr/local/nginx/conf/
```
