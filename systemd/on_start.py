#!/bin/python
# -*- coding: utf-8 -*-
import tester.mobile.tasks as mt
from tester.lab import task_scanlab


task_reportServerStatus()
# if settings.host_master == settings.host:
#     queue_task_dep(
#         'reportServerStatus',
#         group_name='main',
#         timeout=90)
if not settings.host_master == settings.host:
    task_restartAdb()
    mt.task_listAndroidDevices()
    task_scanlab()
#     queue_task_dep(
#         'restartAdb',
#         group_name=settings.host,
#         timeout=300)
#     queue_task_dep(
#         'scanLab',
#         group_name=settings.host,
#         timeout=300)
# db.commit()
