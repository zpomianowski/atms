#!/usr/bin/python
import time
from threading import Timer
from watchdog.observers import Observer
from watchdog.events import PatternMatchingEventHandler

# idle time - delay time in sec after device connection
# after that time ATMS refreshes the device database
IDLETIME = 15.0


class MyHandler(PatternMatchingEventHandler):
    def __init__(self):
        PatternMatchingEventHandler.__init__(self, ignore_directories=True)
        self.t = Timer(IDLETIME, None)

    def on_modified(self, event):
        def __handleEvent(event):
            db._adapter.reconnect()
            scheduler.queue_task(
                'listAndroidDevices',
                group_name=settings.host,
                timeout=180,
                sync_output=5)
            scheduler.queue_task(
                'scanLab',
                group_name=settings.host,
                timeout=300,
                sync_output=5)
            db.commit()

        self.t.cancel()
        self.t = Timer(IDLETIME, __handleEvent, [event])
        self.t.start()


if __name__ == "__main__":
    event_handler = MyHandler()
    observer = Observer()
    observer.schedule(event_handler, path='/dev/bus/usb', recursive=True)
    observer.start()

    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()
    observer.join()
