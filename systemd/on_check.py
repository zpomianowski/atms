#!/bin/python
# -*- coding: utf-8 -*-
from subprocess import Popen, PIPE
from websocket_messaging import websocket_send

p = Popen('systemctl status atms | grep Active', shell=True, stdout=PIPE)
out = p.communicate()[0]

if '(running)' not in out:
    db = current.db
    row = db(db.atms_servers.hostname == settings.host).select().first()
    row.status = 'down'
    row.update_record()
    db(db.atms_devices.host == settings.host).update(
        connected=False,
        state='offline')
    db.commit()

    websocket_send(
        settings.ws_server, '', settings.security_key, 'atms_servers')
    websocket_send(
        settings.ws_server, '', settings.security_key, 'atms_devices')
else:
    # ATMS is operating
    tasks2do = db(
        (db.scheduler_task.group_name == settings.host) &
        (db.scheduler_task.status.contains(
            ['QUEUED', 'ASSIGNED', 'RUNNING']))).count()

    last_test_item = db(
        (db.scheduler_task.group_name == settings.host) &
        (db.scheduler_task.task_name == 'runTest')).select(
        db.scheduler_task.task_name,
        db.scheduler_task.start_time,
        db.scheduler_task.timeout,
        orderby=db.scheduler_task.start_time
    ).last()

    if last_test_item:
        last_test_item = (datetime.datetime.now() >
                          last_test_item.start_time +
                          datetime.timedelta(seconds=last_test_item.timeout))
    else:
        last_test_item = True

    if tasks2do == 0 and last_test_item:
        scheduler.queue_task(
            'listAndroidDevices',
            group_name=settings.host,
            timeout=180,
            sync_output=5)
        scheduler.queue_task(
            'scanLab',
            group_name=settings.host,
            timeout=300,
            sync_output=5)
        db.commit()
