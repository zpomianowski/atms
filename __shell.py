import IPython
from traitlets import config
c = config.get_config()
c.InteractiveShell.autoindent = True
c.InteractiveShell.confirm_exit = False
c.InteractiveShell.deep_reload = True
c.TerminalIPythonApp.display_banner = False
c.InteractiveShell.editor = 'vi'
c.InteractiveShellApp.exec_lines = [
    'from time import sleep',
    'from datetime import datetime, timedelta',
    'from gluon import current',
    'from gluon.storage import Storage',
    'from IPython.utils.coloransi import TermColors as color',
    'from tester.lab import LabFactory',
    'db = current.db',
    'f = LabFactory()',
    'print color.LightGreen',
    'print "from time import sleep"',
    'print "from datetime import datetime, timedelta"',
    'print "from tester.lab import LabFactory"',
    'print "f = LabFactory()"',
    'print color.LightCyan',
    'print "Example: dev = f(model=\'" + color.Yellow + "wkl" + color.LightCyan + "\')"',
    'print "         dev.temp = 45"',
    'print "         dev.humi_control = True"',
    'print "         dev.humi = 55"',
    'print "         dev.on()"',
    'print "Help: " + color.Yellow + "http://atms.lab.redefine.pl/atms/static/docs/tester/tester.lab.html" + color.Normal'
    ]
IPython.start_ipython(config=c)
