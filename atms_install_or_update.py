#!/bin/python
# -*- coding: utf-8 -*-
import os
import optparse
from subprocess import call, Popen, PIPE

INSTALL_PATH = None
USER = None
BRANCH = None

NODEV = '6.11.1'
APPIUMV = '1.6.5'
WEB2PYV = '2.14.6'


def mpath(path):
    if not os.path.exists(path):
        call('mkdir -p %s' % path, shell=True)
    else:
        call('rm -R %s' % path, shell=True)
        call('mkdir -p %s' % path, shell=True)
    os.chdir(path)


def install(layer):
    print 'INSTALLING LAYER: %s' % layer

    if layer == 'basics':
        call('dpkg --add-architecture i386', shell=True)
        call('apt-get -y install software-properties-common', shell=True)
        call('add-apt-repository ppa:wireshark-dev/stable -y', shell=True)
        call('add-apt-repository ppa:ubuntu-toolchain-r/test -y', shell=True)
        call('apt-get update -y', shell=True)
        call('apt-get dist-upgrade -y', shell=True)
        call('apt-get -y install \
             fping \
             mpg321 \
             nmap \
             xvfb \
             beep \
             unzip \
             wget \
             git \
             htop \
             dos2unix \
             python-pip \
             python-dev \
             python-gi \
             pep8 \
             libssl-dev \
             libffi-dev \
             wireshark-common \
             gstreamer1.0 \
             tshark \
             libmysqlclient-dev \
             python-mysqldb \
             libttspico0 libttspico-utils libttspico-data \
             virtualenv', shell=True)
        call('setcap cap_net_raw,cap_net_admin,' +
             'cap_net_bind_service+eip /usr/bin/nmap', shell=True)
    if layer == 'appium':
        version = "v%s" % NODEV
        n_path = "%(v)s/node-%(v)s-linux-x64" % dict(v=version)
        call('rm -R nodejs', shell=True)

        cmd = 'tar -xvf %(p)s.tar.gz && \
            rm %(p)s.tar.gz && \
            mv %(p)s nodejs' % dict(p=os.path.basename(n_path))
        call('wget http://nodejs.org/dist/%(p)s.tar.gz' % dict(p=n_path),
             shell=True)
        call(cmd, shell=True)
        call('nodejs/bin/npm install -g appium@>=%s' % APPIUMV, shell=True)
    if layer == 'android-sdk':
        version = "sdk-tools-linux-3859397"
        call('rm -R android-sdk-linux', shell=True)

        call('apt-get -y install libgl1-mesa-dev:i386', shell=True)
        call('wget https://dl.google.com/android/repository/%s.zip' % version,
             shell=True)
        call('unzip %(sdk)s.zip -d android-sdk-linux && \
             rm %(sdk)s.zip && \
             apt-get install openjdk-8-jdk -y' % dict(sdk=version),
             shell=True)
        install('adb')
    if layer == 'python+':
        call('sudo apt-get -y install build-essential git cmake \
             libqt4-dev libphonon-dev python2.7-dev libxml2-dev \
             libxslt1-dev libqtwebkit-dev libssl-dev',
             shell=True)
        call('apt-get -y build-dep python-imaging && \
            apt-get -y install libjpeg62 libjpeg62-dev && \
            ln -s /usr/lib/`uname -i`-linux-gnu/libz.so /usr/lib/libz.so && \
            ln -s /usr/lib/`uname -i`-linux-gnu/libjpeg.so \
            /usr/lib/libjpeg.so && \
            ln -s /usr/lib/`uname -i`-linux-gnu/libfreetype.so \
            /usr/lib/libfreetype.so && \
            apt-get -y install libxml2-dev libxslt1-dev', shell=True)
    # after 'web2py', 'atms' has to be installed
    if layer == 'web2py':
        mpath('web2py_venv')
        call('virtualenv --no-site-packages --distribute venv', shell=True)
        call('wget http://www.web2py.com/examples/static/%s/web2py_src.zip' %
             WEB2PYV, shell=True)
        call('unzip web2py_src.zip && rm web2py_src.zip', shell=True)
    if layer == 'atms':
        repo = 'https://saut:cpwifi1313@bitbucket.org/zpomianowski/atms.git'
        mpath(INSTALL_PATH + '/web2py_venv/web2py/applications/atms')
        os.chdir(os.pardir)
        if not DEV_MODE:
            call('git clone %s -b %s --single-branch --depth 1' % (
                repo, BRANCH), shell=True)
        else:
            import shutil
            shutil.rmtree('atms', ignore_errors=True)
            shutil.copytree('/home/%s/web2py/applications/atms' % USER,
                            'atms', symlinks=True)
        call('cp /appconfig.ini atms/private/', shell=True)
        call('chown -R %s:%s %s *' % (USER, USER, 'atms'), shell=True)
        call('cp atms/atms_install_or_update.py /atms_install_or_update.py',
             shell=True)
        call('chmod 700 /atms_install_or_update.py', shell=True)
        call('chmod 777 atms/extras/set_wallpaper.sh', shell=True)
        call(INSTALL_PATH + '/web2py_venv/venv/bin/pip install --upgrade pip',
             shell=True)
        call(INSTALL_PATH + '/web2py_venv/venv/bin/pip install -r ' +
             INSTALL_PATH + '/web2py_venv/web2py/applications/atms/' +
             'requirements/prod.txt --upgrade',
             shell=True)

        os.chdir('atms/systemd')
        if INSTALL_PATH == "/":
            atms_path = ""
        else:
            atms_path = INSTALL_PATH.replace("/", "\/")
        call('sed -i \"s/{{INSTALL}}/%s/g\" atms.env'
             % atms_path, shell=True)
        call('cp atms.env ../private/', shell=True)

        # temp - need to be setup at venv stage
        path = INSTALL_PATH + '/web2py_venv/venv/lib/python2.7/dist-packages'
        try:
            os.makedirs(path)
        except:
            pass
        os.chdir(path)
        try:
            call('ln -s /usr/lib/python2.7/dist-packages/gi',
                 shell=True)
        except:
            pass
        try:
            call('ln -s /usr/local/lib/python2.7/dist-packages/cv2.so',
                 shell=True)
        except:
            pass

        # update cron
        os.chdir(INSTALL_PATH + '/web2py_venv/web2py/applications/atms/cron')
        if INSTALL_PATH == "/":
            atms_path = ""
        else:
            atms_path = INSTALL_PATH.replace("/", "\/")
        call('sed -i \"s/{{INSTALL}}/%s/g\" crontab'
             % atms_path, shell=True)
        call('crontab crontab -u %s && /etc/init.d/cron reload' % USER,
             shell=True)

        install('adb')

    if layer == 'systemd':
        os.chdir(INSTALL_PATH +
                 '/web2py_venv/web2py/applications/atms/systemd')
        if INSTALL_PATH == "/":
            atms_path = ""
        else:
            atms_path = INSTALL_PATH.replace("/", "\/")
        call('sed -i \"s/{{INSTALL}}/%s/g\" atms.service'
             % atms_path, shell=True)
        call('sed -i \"s/{{INSTALL}}/%s/g\" atmsusbwd.service'
             % atms_path, shell=True)
        call('sed -i \"s/{{INSTALL}}/%s/g\" atmsinit.service'
             % atms_path, shell=True)
        call('sed -i s/{{USER}}/%s/ atms.service'
             % USER, shell=True)
        call('sed -i s/{{USER}}/%s/ atmsusbwd.service'
             % USER, shell=True)
        call('sed -i s/{{USER}}/%s/ atmsinit.service'
             % USER, shell=True)
        call('cp atms.service /lib/systemd/system/ && \
              cp atmsusbwd.service /lib/systemd/system/ && \
              cp atmsinit.service /lib/systemd/system/', shell=True)
        call('systemctl enable atmsinit atms atmsusbwd', shell=True)
        if DEV_MODE:
            call('systemctl disable atmsinit atms atmsusbwd', shell=True)

    if layer == 'systemd-rtmp':
        # TODO - details in systemd/rtmp
        pass

    if layer == 'adb':
        # to check: android-sdk-linux/tools/android list sdk
        call('touch /root/.android/repositories.cfg', shell=True)
        cmd = '( sleep 5 && while [ 1 ]; do sleep 1; echo y || break; done ) \
            | sdkmanager "platform-tools"'
        call(cmd, shell=True)
        cmd = '( sleep 5 && while [ 1 ]; do sleep 1; echo y || break; done ) \
            | sdkmanager update'
        call(cmd, shell=True)
        os.chdir(INSTALL_PATH + "/android-sdk-linux/platform-tools")
        call('chown root:%s adb && chmod 4750 adb' % USER, shell=True)

    if layer == 'opencv':
        call('apt-get -y install ' +
             'build-essential cmake git pkg-config ' +
             'libjpeg8-dev libtiff-dev libjasper-dev libpng12-dev ' +
             'libgtk2.0-dev libavcodec-dev libavformat-dev libswscale-dev ' +
             'libv4l-dev libatlas-base-dev gfortran ' +
             'python-numpy libdc1394-22-dev libtbb2 libtbb-dev', shell=True)

        version = '3.2.0'
        mpath('/tmp/opencv_tmp')

        # get openCV
        call('wget https://github.com/opencv/opencv/archive/%s.zip' % version +
             ' -O opencv.zip', shell=True)
        call('wget https://github.com/opencv/opencv_contrib/archive/%s.zip' %
             version + ' -O opencv_contrib.zip', shell=True)

        call('unzip opencv.zip' % dict(v=version), shell=True)
        call('unzip opencv_contrib.zip' % dict(v=version), shell=True)

        call('mv opencv-%(v)s opencv' % dict(v=version), shell=True)
        call('mv opencv_contrib-%(v)s opencv_contrib' %
             dict(v=version), shell=True)

        os.chdir('opencv')
        os.mkdir('release')
        os.chdir('release')

        call('cmake -D CMAKE_BUILD_TYPE=RELEASE '
             '-DOPENCV_EXTRA_MODULES_PATH=../../opencv_contrib/modules '
             '-D CMAKE_INSTALL_PREFIX=/usr/local ..', shell=True)
        call('make -j5 && make install', shell=True)

        # lib/cv2.so -> virtualenv/lib/cv2.so -> in atms layer

    if layer == 'videocap':
        version = '3589'
        mpath(INSTALL_PATH + '/magewell')
        call(
            'wget http://www.magewell.com/files/ProCaptureForLinux_%s.tar.gz' %
             version + ' -O ProCaptureForLinux.tar.gz', shell=True)
        call('mkdir ProCaptureForLinux && ' +
             'tar -xf ProCaptureForLinux.tar.gz -C ProCaptureForLinux ' +
             '--strip-components 1',
             shell=True)
        os.chdir(INSTALL_PATH + '/magewell/ProCaptureForLinux')
        call('./install.sh', shell=True)

    if layer == 'desktop':
        from socket import gethostname
        os.chdir('/usr/share/applications')
        call('rm atmsPrev*', shell=True)
        # call('rm magewellPrev*', shell=True)

        os.chdir(INSTALL_PATH +
                 '/web2py_venv/web2py/applications/atms/extras')
        call('chmod 777 atmsPrev.py', shell=True)
        call('cp atmsPrev.desktop ' +
             '/usr/share/applications/atmsPrev.desktop', shell=True)

        os.chdir('/usr/share/applications')

        call('sed -i s:{{INSTALL}}:%s: atmsPrev.desktop'
             % INSTALL_PATH, shell=True)
        call('sed -i s/{{HOSTNAME}}/%s/ atmsPrev.desktop'
             % gethostname(), shell=True)
        call('chmod 777 atmsPrev.desktop', shell=True)

        os.chdir(INSTALL_PATH +
                 '/web2py_venv/web2py/applications/atms/extras')
        call('cp atmsShell.desktop ' +
             '/usr/share/applications/atmsShell.desktop', shell=True)

        os.chdir('/usr/share/applications')

        call('sed -i s:{{INSTALL}}:%s: atmsShell.desktop'
             % INSTALL_PATH, shell=True)
        call('chmod 777 atmsShell.desktop', shell=True)


if __name__ == '__main__':
    usage = __doc__
    version = ""
    parser = optparse.OptionParser(usage, None, optparse.Option, version)
    parser.add_option('-p',
                      '--path',
                      default='/atms',
                      dest='path',
                      help='Where to install ATMS')
    parser.add_option('-u',
                      '--user',
                      default='root',
                      dest='user',
                      help='Run ATMS as <user>')
    parser.add_option('-b',
                      '--branch',
                      default='master',
                      dest='branch',
                      help='Choose branch in git repo')
    choices = ['all', 'basics', 'appium', 'android-sdk',
               'python+', 'web2py', 'atms', 'adb', 'systemd',
               'opencv', 'videocap', 'desktop']
    parser.add_option('-l',
                      '--layer',
                      default='all',
                      dest='layer',
                      choices=choices,
                      help='Layer to install: %s' % ', '.join(choices))
    parser.add_option('-d',
                      '--dev',
                      default=False,
                      action='store_true',
                      dest='dev',
                      help='Only for developers!')
    (options, args) = parser.parse_args()

    INSTALL_PATH = os.path.abspath(options.path)
    USER = options.user
    LAYER = options.layer
    BRANCH = options.branch
    DEV_MODE = options.dev

    os.environ["PATH"] = os.environ["PATH"] + (
        ':%(h)s/nodejs/bin/:%(h)s/android-sdk-linux/tools' +
        ':%(h)s/android-sdk-linux/tools/bin' +
        ':%(h)s/android-sdk-linux/platform-tools') % dict(
        h=INSTALL_PATH)
    print os.environ['PATH']
    os.environ["JAVA_HOME"] = "/usr/lib/jvm/java-8-openjdk-amd64"
    os.environ["ANDROID_HOME"] = "%(h)s/android-sdk-linux" % dict(
        h=INSTALL_PATH)

    call('mkdir -p %s' % INSTALL_PATH, shell=True)
    os.chdir(INSTALL_PATH)
    install_all = False
    if LAYER == 'all':
        # internet access needed
        install('basics')
        # approx 22 minuts
        install('appium')
        install('android-sdk')
        install('python+')
        install('web2py')
        install('atms')
        install('systemd')
        install('opencv')
        install('videocap')
        if not DEV_MODE:
            call('reboot', shell=True)
    else:
        # only access to saut.test needed
        install(LAYER)
        if LAYER == 'atms':
            try:
                install('desktop')
            except:
                pass
    if not DEV_MODE:
        call('sed -i \'s/^blacklist pcspkr/#blacklist pcspkr/g\' ' +
             '/etc/modprobe.d/blacklist.conf', shell=True)
        call('modprobe pcspkr', shell=True)
    call('usermod -a -G dialout %s' % USER, shell=True)
    call('chown -R %s:%s %s' % (USER, USER, INSTALL_PATH), shell=True)

# copy this file and config.ini paste:
# sudo nano atms_install_or_update.py
# sudo nano appconfig.ini
