import unittest
import sys
import os

from applications.atms.tests.controllers import default, timesheet

execfile("applications/atms/controllers/default.py", globals())
execfile("applications/atms/controllers/timesheet.py", globals())
execfile("applications/atms/controllers/lipstein.py", globals())

suite = unittest.TestSuite()
suite.addTest(unittest.makeSuite(default.TestDefault))
#suite.addTest(unittest.makeSuite(timesheet.TestTimesheet))
#suite.addTest(unittest.makeSuite(timesheet.TestLipstein))
unittest.TextTestRunner(verbosity=1).run(suite)
