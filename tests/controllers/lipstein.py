import unittest
from gluon.globals import Request
from gluon.storage import Storage
import gluon.contrib.simplejson as json

execfile("applications/atms/controllers/lipstein.py", globals())

#db(db.game.id>0).delete()  # Clear the database
#db.commit()

class TestLipstein(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        self.request = Storage()
        auth.login_bare('pszklanko@cyfrowypolsat.pl',
                        myconf.take('dev.test_user_password'))

    #def testTapiGet(self):
    #    request.env.request_method = "GET"

    #def testApiGet(self):
    #    request.env.request_method = "GET"

    #def testApiPost(self):
    #    request.env.request_method = "POST"

    #def testApiDelete(self):
    #    request.env.request_method = "DELETE"

    #def testCaps(self):
    #    response = caps(2)
    #    print response

    #def testCa(self):
    #    response = ca(2)

    #def testSummary(self):
    #    request.args = [2]
    #    response = summary()
    #    self.assertEquals('content' in response, True)

    def testTxt_apiGet(self):
        request.env.request_method = "GET"
        request.args = ['ref_cbs', 2]
        request.vars = Storage({'field': 'ref_cbs', 'id': 2})
        response = txt_api()
        print response

suite = unittest.TestSuite()
suite.addTest(unittest.makeSuite(TestLipstein))
unittest.TextTestRunner(verbosity=2).run(suite)
