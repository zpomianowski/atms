import unittest
from gluon.globals import Request
from gluon.storage import Storage
import gluon.contrib.simplejson as json

execfile("applications/atms/controllers/default.py", globals())

#db(db.game.id>0).delete()  # Clear the database
#db.commit()

class TestDefault(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        request = Storage()
        auth.login_bare('pszklanko@cyfrowypolsat.pl', myconf.take('dev.test_user_password'))


    def testAddNews(self):
        # Set variables for the test function
        request._vars['table'] = 'atms_news'
        request._vars['data'] = {"description": "opis testowego testu",
                                     "title": "testTestowy",
                                     "link": ["www.google.pl"]}

        response = addRecord()
        self.assertGreater(response['resp'], 0)

    def testAddNewsNoTable(self):
        # Set variables for the test function
        del request._vars['table']
        request._vars['data'] = {"description": "opis testowego testu",
                                     "title": "testTestowy",
                                     "link": ["www.google.pl"]}

        response = addRecord()
        self.assertEquals(response['resp'], -1)

    def testAddNewsWrongFieldName(self):
        # Set variables for the test function
        request._vars['table'] = 'atms_news'
        request._vars['data'] = {"desScRripTtion": "opis testowego testu",
                                     "title": "testTestowy",
                                     "link": ["www.google.pl"]}

        response = addRecord()
        self.assertEquals(response['resp'], -1)

    def testEditNews(self):
        request._vars['table'] = 'atms_news'
        request._vars['id'] = db().select(orderby=~db.atms_news.id,limitby=(0,1))[0].id
        request._vars['data'] = {"description": "zmieniony opis testowego testu",
                                     "title": "testTestowy",
                                     "link": ["www.google.pl"]}

        response = editRecord()
        self.assertEquals(response['resp'], 1)

    def testEditNewsBadRequest(self):
        # no id
        request._vars['table'] = 'atms_news'
        request._vars['data'] = {"description": "zmieniony opis testowego testu",
                                     "title": "testTestowy",
                                     "link": ["www.google.pl"]}

        response = editRecord()
        self.assertEquals(response['resp'], 0)

    def testDeleteNews(self):
        request._vars['table'] = 'atms_news'
        request._vars['id'] = db().select(orderby=~db.atms_news.id,limitby=(0,1))[0].id
        response = delRecord()
        self.assertEquals(response['resp'], 1)

    def testDeleteNewsBadRequest(self):
        request._vars['table'] = ''
        request._vars['id'] = db().select(orderby=~db.atms_news.id,limitby=(0,1))[0].id
        response = delRecord()
        self.assertEquals(response['resp'], -1)

    #def testSearch(self):
    #    request._vars['keywords'] = ['test', 'testowy']
    #    request._body = request._vars
    #    response = search()
    #    self.assertGreater(len(response['news_items']), 0)

    #def testGetNews(self):
    #    request.env.request_method = "GET"






suite = unittest.TestSuite()
suite.addTest(unittest.makeSuite(TestDefault))
unittest.TextTestRunner(verbosity=2).run(suite)
