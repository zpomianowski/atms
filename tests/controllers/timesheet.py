import unittest
from gluon.globals import Request
from gluon.storage import Storage
import gluon.contrib.simplejson as json

#execfile("applications/atms/controllers/timesheet.py", globals())

#db(db.game.id>0).delete()  # Clear the database
#db.commit()

class TestTimesheet(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        self.request = Storage()
        auth.login_bare('pszklanko@cyfrowypolsat.pl',
                        myconf.take('dev.test_user_password'))

    def test00GetUserInfo(self):
        response = getUserInfo()
        self.assertEquals('id' and 'email' and 'firstname' and
                          'lastname' and 'username' and 'privileges'
                          in response['data'], True)

    def test01GetProjects(self):
        response = getProjects()
        self.assertGreater(len(response['data']), 0)
        self.assertEquals('id' and 'name' and 'description' and
                          'visible' and 'status' and 'color' and 'background'
                          in response['data'][0], True)

    def test02GetTaskType(self):
        response = getTaskType()
        self.assertGreater(len(response['data']), 0)
        self.assertEquals('id' and 'name' and 'description' and
                          'visible' in response['data'][0], True)

    def test03GetProjectTaskType(self):
        response = getProjectTaskType()
        self.assertGreater(len(response['data']), 0)
        self.assertEquals('project_id' and 'id' and 'task_type_id' and
                          'visible' in response['data'][0], True)

    def test04GetEvents(self):
        response = getEvents()
        self.assertGreater(len(response['data']), 0)
        self.assertEquals('project_id' and 'visible' and 'user_id' and 'id' and
                          'task_type_id' and 'descritpion' and 'start' and
                          'duration' and 'end' and 'visible'
                          in response['data'][0], True)

    def test05GetUsers(self):
        response = getUsers()
        self.assertGreater(len(response['data']), 0)
        self.assertEquals('id' and 'first_name' and 'last_name' and
                          'email' and 'username' and 'is_active'
                          in response['data'][0], True)

    def test06AddRecord(self):
        request._vars['table'] = 'atms_timesheet_task'
        request._vars['data'] = {'user_id': 30,
                                     'description': 'dsds',
                                     'visible': True,
                                     'stop': '2016-11-15 12:00:00',
                                     'start': '2016-11-15 09:00:00',
                                     'task_type_id': 3,
                                     'duration': '3',
                                     'project_id': 10}
        response = addRecord()
        self.assertGreater(response['data'], 0)

    def test07EditRecord(self):
        request._vars['table'] = 'atms_timesheet_task'
        request._vars['id'] = db().select(orderby=~db.atms_timesheet_task.id,limitby=(0,1))[0].id
        request._vars['data'] = {'description': 'zmieniony event',
                                 'stop': '2016-11-14 12:00:00',
                                 'start': '2016-11-14 09:00:00',
                                 'duration': '3'}

        response = editRecord()
        self.assertEquals(response['data'], 1)

    def test08DelRecord(self):
        request._vars['table'] = 'atms_timesheet_task'
        request._vars['id'] = db().select(orderby=~db.atms_timesheet_task.id,limitby=(0,1))[0].id
        response = delRecord()
        self.assertEquals(response['data'], 1)

    def test09ResetRelations(self):
        pass

    def test10SetRelations(self):
        pass


suite = unittest.TestSuite()
suite.addTest(unittest.makeSuite(TestTimesheet))
unittest.TextTestRunner(verbosity=2).run(suite)
